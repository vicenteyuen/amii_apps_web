### 短信服务([leanCloud](https://leancloud.cn/docs/rest_api.html#数据查询_API))
### 消息推送([leanCloud](https://leancloud.cn/docs/rest_api.html#Push_通知))

### 配置图片服务器地址.env中IMAGE_HOST


## 目录准备

* 确保 laravel\storage 目录有可写权限
* 确保 laravel\uploads 目录有可写权限
* 确保 laravel\uploads\deepdraw 目录存在并可写


### Cache使用
##### Cache使用file驱动
##### 使用Cache的地方
> [首页推广]    
> [首页banner]    
> [商品详情(不包含库存)]    

### 开启队列

```
php artisan queue:listen
```

### 开启cron任务

```
EDITOR=/usr/bin/vim crontab -e
```

```
* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```


## 配置环境：

1. 根据 https://gitlab.cngump.com/ganguo_web/wiki 安装PHP、composer

2. brew安装node.js 以及 npm

## clone代码目录之后：

1. 切换分支为wechat

2. 更新代码

3. 进入项目—>laravel目录 `composer install`

4. `php artisan key:generate`

5. 进入项目文件有个.env.example 复制与.env.example 一样的配置并命名为.env

6. 配置.env 复制以下几段
```
DB_CONNECTION=mysql
DB_HOST=dev.ganguo.hk
DB_PORT=3306
DB_DATABASE=amii
DB_USERNAME=phpadmin
DB_PASSWORD=MP+yXe++pPktqhS/GWQFY3
```

7. `npm install`

8. `npm run watch`

9. `php artisan serve` / `php artisan serve --host=0.0.0.0:8000` (`npm run watch` 不能暂停!!)

10. 访问 http://{url}:8000/wechat#/mall 到首页
