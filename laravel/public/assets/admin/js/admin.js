"use strict";
$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $.cookie('XSRF-TOKEN')
    }
  });
  $(document).ajaxError(function (event, jqXHR, settings, thrownError) {
    if (jqXHR.responseJSON && jqXHR.responseJSON.message) {
      layer.msg(jqXHR.responseJSON.message);
      return;
    }
    switch (jqXHR.status) {
      case 403:
        layer.msg('未授权该操作！');
        break;
      case 404:
        layer.msg('链接错误！');
        break;
      case 500:
        layer.msg('服务器错误！');
        break;
    }
  });
  // http://stackoverflow.com/a/3955096/7340930
  if(!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(what, i) {
      i = i || 0;
      var L = this.length;
      while (i < L) {
        if(this[i] === what) return i;
        ++i;
      }
      return -1;
    };
  }
  Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
      what = a[--L];
      while ((ax = this.indexOf(what)) !== -1) {
        this.splice(ax, 1);
      }
    }
    return this;
  };
  $('#modal-image-select').on('click', '.images-select', function (e) {
    var obj = $(this).prev('img');
    var key = obj.attr('data-id');
    if (! $(this).hasClass('images-selected')) {
      $.each($('.images-selected'),function(){
          $(this).removeClass('images-selected');
      })
      imageSelected = obj.attr('src');
    } else{
      imageSelected = undefined;
    }
    $(this).toggleClass('images-selected');
  });
  Helper.init();
});
var imageSelected;
var Helper = {
  init: function () {
    // this.setDefaultImg();
  },
  readURL: function(o, selecter) {
    if (o.files && o.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        if (selecter) {
          $(selecter).attr('src', e.target.result);
        } else {
          $('#uploadImg').attr('src', e.target.result);
        }
      }
      reader.readAsDataURL(o.files[0]);
    }
  },
  modalToggle: function(obj) {
    obj.modal('toggle');
  },
  ajax: function () {
    var ret = [];
    $.each(["post", "put", "delete"], function (i, method) {
      ret[method] = function (url, data, callback, type) {
        // Shift arguments if data argument was omitted
        if (jQuery.isFunction(data)) {
          type = type || callback;
          callback = data;
          data = undefined;
        }
        // The url can be an options object (which then must have .url)
        return jQuery.ajax(jQuery.extend({
          url: url,
          type: method,
          dataType: type,
          data: data,
          async: true,
          success: callback
        }, jQuery.isPlainObject(url) && url));
      };
    });
    return ret;
  },
  setDefaultImg: function (obj, url) {
    if (! obj) {
      var obj = $('#uploadImg');
    }
    if (! url) {
      var url = 'http://7xw4yp.com1.z0.glb.clouddn.com/1459309578804.jpg';
    }
    obj.attr('src', url);
  },
  initFun: {
    setLeftSidebarActiveItem: function () {
      var sidebarItem = $('#main-menu li[class!="gui-folder"]');
      if (!window.sessionStorage) {
        return false;
      }
      if (sidebarItem.length == 0) {
        sessionStorage.sidebarItem = 0;
        return false;
      }
      sidebarItem.each(function (k, v) {
        $(this).click(function (e) {
          sessionStorage.sidebarItem = k;
        });
      });
      var cur;
      if (sessionStorage.sidebarItem != undefined) {
        cur = sidebarItem.eq(sessionStorage.sidebarItem);
      } else {
        cur = sidebarItem.eq(1);
      }
      cur.find('a').addClass('active');
    },
  },
};
Helper.initFun.setLeftSidebarActiveItem();
