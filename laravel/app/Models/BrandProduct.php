<?php

namespace App\Models;

class BrandProduct extends Model
{
    protected $table = 'brand_products';


    protected $fillable = [
        'product_id',
        'brand_id',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
    ];

    public function products()
    {
        return $this->belongsTo('Ecommerce\Models\Product', 'product_id', 'id');
    }

    public function product()
    {
        return $this->hasOne('Ecommerce\Models\Product', 'id', 'product_id');
    }
}
