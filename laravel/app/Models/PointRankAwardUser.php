<?php

namespace App\Models;

class PointRankAwardUser extends Model
{
    protected $table = 'point_rank_award_users';

    protected $fillable = [
        'placed',
        'point_rank_award_id',
        'user_id',
        'points'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
