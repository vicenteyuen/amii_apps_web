<?php

namespace App\Models;

class OrderPackage extends Model
{
    protected $table = 'order_packages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'shipping_status',
        'com',
        'shipping_sn',
        'products',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

    /**
     * 物流信息
     */
    public function express()
    {
        return $this->belongsTo('App\Models\ExpressModels\ExpressLog', 'id', 'order_package_id');
    }
}
