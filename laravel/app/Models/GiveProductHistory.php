<?php

namespace App\Models;

class GiveProductHistory extends Model
{
    protected $table = 'give_product_histories';

    protected $fillable = [
        'sell_product_id',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function sellProduct()
    {
        return $this->belongsTo('App\Models\SellProduct', 'sell_product_id', 'id');
    }
}
