<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class AttributeValue extends Model
{
    protected $table = 'attribute_values';

    protected $fillable = [
        'attribute_id',
        'value',
        'status',
        'weight',
        'image',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
        'deleted_at',
        'deleted_at_millisecond',
    ];
    // 属性值
    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id');
    }

    // 属性值
    public function attr()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id');
    }
   
    // get img link
    public function getImageAttribute()
    {
        if (!$this->attributes['image']) {
            return null;
        }
        return ImageHelper::getImageUrl($this->attributes['image'], 'product', 'small');
    }
    public function sellAttributeValue()
    {
        return $this->hasOne('App\Models\SellAttributeValue', 'attribute_value_id', 'id');
    }
}
