<?php

namespace App\Models;

class VideoLike extends Model
{
    protected $table = 'video_likes';

    protected $fillable = [
        'id',
        'video_id',
        'user_id',
    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'created_at',
        'updated_at',
    ];
}
