<?php

namespace App\Models;

class Size extends Model
{
    protected $table = 'sizes';

    protected $fillable = [
        'name',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
