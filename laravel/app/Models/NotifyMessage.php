<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class NotifyMessage extends Model
{
    use SoftDeletes;

    protected $table = 'notify_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
        'title',
        'content',
        'user_id',
        'status',
        'type',
        'resource',
        'provider',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'order',
        'order_image',
        'order_name',
        'express_sn',
        'order_sn',
        'deleted_at_millisecond',
        'deleted_at',
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
        'userNotifyMessage',
    ];

    protected $appends = [
        'order_image',
        'order_name',
        'express_sn',
        'order_sn',
    ];

    /**
     * 修饰创建时间
     */
    public function getCreatedAtAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->toDateString();
        }
        return $value;
    }

    /**
     * 获取消息状态
     */
    public function getStatusAttribute($value)
    {
        if ($this->attributes['provider'] == 'notify') {
            if ($value == 2) {
                return $value;
            }
            $userId = auth()->id();

            if ($userId && $this->userNotifyMessage && $this->userNotifyMessage->user_id == $userId) {
                return 1;
            }
            return 0;
        }
        return $value;
    }

    /**
     *  获取快递单号
     */
    public function getExpressSnAttribute()
    {
        $order = $this->load('order.packages');

        if (! $order) {
            return '';
        }
        if (!$order->order || $order->order->packages->isEmpty()) {
            return '';
        }
        return $order->order->packages[0]->shipping_sn;
    }

    /**
     *  获取订单ID
     */
    public function getOrderSnAttribute()
    {
        $order = $this->order;
        if (! $order) {
            return '';
        }
        return $order->order_sn;
    }

    /**
     *  获取物流消息展示图
     */
    public function getOrderImageAttribute()
    {
        $order = $this->load('order.orderProducts.sellProduct.product');
        if (! $order) {
            return '';
        }
        if (!$order->order || $order->order->orderProducts->isEmpty()) {
            return '';
        }
        return $order->order->orderProducts[0]->sellProduct->product->main_image;
    }

    /**
     * 获取物流消息标题
     */
    public function getOrderNameAttribute()
    {
        $order = $this->load('order.orderProducts.sellProduct.product');
        if (! $order) {
            return '';
        }
        if (!$order->order || $order->order->orderProducts->isEmpty()) {
            return '';
        }
        return $order->order->orderProducts[0]->sellProduct->product->name;
    }

    /**
     * 获取有效通知消息
     */
    public function scopeStatus($query)
    {
        return $query->whereIn('status', [0, 1]);
    }

    /**
     * 获取通知类型消息
     */
    public function scopeNotify($query)
    {
        return $query->where('provider', 'notify');
    }

    /**
     * 获取物流类型消息
     */
    public function scopeExpress($query)
    {
        return $query->where('provider', 'express');
    }
    // 与订单建立关联
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'resource', 'id');
    }

    // 系统消息用户已读状态
    public function userNotifyMessage()
    {
        return $this->hasOne('App\Models\UserNotifyMessage', 'notify_messages_id', 'id')
        ->where('user_id', auth()->id());
    }

    // 与用户建立关联
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
