<?php

namespace App\Models;

use App\Helpers\AlipayAopHelper;
use App\Helpers\EasyWechatHelper;
use App\Models\Events\WithdrawRecordEvent;

class WithdrawRecord extends Model
{
    protected $fillable = [
        'amount',
        'state',
        'channel_id',
    ];

    protected static function boot()
    {
        parent::boot();
        static::observe(new WithdrawRecordEvent());
    }

    public function channel()
    {
        return $this->belongsTo(WithdrawChannel::class, 'channel_id');
    }

    /**
     * 发起支付
     * @return null
     */
    public function paySend()
    {
        if ($this->state != 1) return null;
        $channel = $this->channel;
        if (!$channel->account_number) return null;

        if (in_array($channel->type, ['wechat', 'weweb', 'wemp', 'weapp'])) {
            $application = null;
            switch ($channel->type) {
                case 'weweb':
                case 'wechat':
                    $application = EasyWechatHelper::easywechatWechat();
                    break;
                case 'wemp':
                    $application = EasyWechatHelper::easywechatWemp();
                    break;
                case 'weapp':
                    $application = EasyWechatHelper::easywechatWeapp();
                    break;
            }
            if ($application) {
                \Log::info('微信发起企业支付：' . $channel->type);
                $res = EasyWechatHelper::merchantPaySend(
                    $application,
                    $this->trade_no,
                    $channel->account_number,
                    $this->amount,
                    '余额提现'
                );
                if ($res->return_code == 'SUCCESS' && $res->result_code == 'SUCCESS') {
                    $this->state = 2;
                    $this->save();
                }
                return ['status' => false, 'msg' => '微信转账失败：' . $res->return_msg];
            }
            return null;
        }
        if (in_array($channel->type, ['alipay'])) {
            $result = AlipayAopHelper::getInstance('withdraw')->fundTransToaccountTransfer(
                $this->trade_no,
                $channel->account_number,
                $this->amount,
                $channel->account_name,
                '余额提现'
            );
            if ($result->code != 10000) {
                return ['status' => false, 'msg' => '支付宝转账失败：' . $result->sub_msg];
            }
            $this->state = 2;
            $this->save();
        }
        return $this;
    }
}
