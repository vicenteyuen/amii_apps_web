<?php

namespace App\Models;

use App\Helpers\ImageHelper;
use App\Helpers\SummernoteHelper;

class PhysicalStore extends Model
{
    protected $table = 'physical_stores';

    protected $fillable = [
        'name',
        'name_initial',
        'image',
        'phone',
        'address',
        'detail_address',
        'longitude',
        'latitude',
        'city_name',
        'introduction',
        'weight',

    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'created_at',
        'updated_at',

    ];

    protected $appends = [
        'url',
    ];

    public function getUrlAttribute()
    {
        return action('Apih5\PhysicalStoreController@show', ['id' => $this->attributes['id']]);
    }
    public function getImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'physicalStore', 'large');
    }

    public function getIntroductionAttribute()
    {
        return SummernoteHelper::getImgUrl($this->attributes['introduction'], 'physicalStore');
    }
}
