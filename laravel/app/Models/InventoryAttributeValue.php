<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryAttributeValue extends Model
{
    use SoftDeletes;

    protected $table = 'inventories_attribute_values';
    protected $dates = ['deleted_at'];

    protected $fillable =[
        'attribute_value_id',
        'inventory_id',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
        'deleted_at',
        'deleted_at_millisecond',
    ];


    /**
     * 属性
     */
    public function skuValue()
    {
        return $this->belongsTo('App\Models\AttributeValue', 'attribute_value_id', 'id');
    }
}
