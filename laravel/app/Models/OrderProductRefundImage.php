<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class OrderProductRefundImage extends Model
{
    protected $table = 'order_refund_log_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * get image attribute
     */
    public function getValueAttribute($value)
    {
        return ImageHelper::getImageUrl($value, 'refund');
    }

    public function refundLog()
    {
        return $this->belongsTo(OrderProductRefundLog::class, 'order_refund_log_id');
    }
}
