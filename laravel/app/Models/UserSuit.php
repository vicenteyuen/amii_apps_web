<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class UserSuit extends Model
{
    
    protected $table = 'user_suits';

    protected $fillable = [
        'user_id',
        'name',
        'height',
        'weight',
        'bust',
        'hips',
        'waist',
        'status',
        'skin',
        'hair',
        'shoulder_width',
        'thigh_width',
        'front_photo',
        'side_photo',
        'leg_width',
        'gender',
    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'created_at',
        'updated_at',
    ];

    public function getFrontPhotoAttribute()
    {
        if ($this->attributes['front_photo']) {
            return ImageHelper::getImageUrl($this->attributes['front_photo'], 'avatar');
        }
        return null;
    }

    public function getSidePhotoAttribute()
    {
        if ($this->attributes['side_photo']) {
            return ImageHelper::getImageUrl($this->attributes['side_photo'], 'avatar');
        }
        return null;
    }
    public function question()
    {
        return $this->hasMany('App\Models\UserAnswer', 'user_suit_id', 'id');
    }

    public function size()
    {
        return $this->belongsTo('App\Models\Size', 'size_id', 'id');
    }
}
