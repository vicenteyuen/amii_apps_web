<?php

namespace App\Models\TaobaoOpen;

use App\Models\Model;

class TaobaoTopInventory extends Model
{
    protected $table = 'taobao_top_inventorys';

    protected $fillable = [
        'merchant',
        'taobao_outer_id',
        'value',
        'is_handle'
    ];

    protected $casts = [
        'value' => 'array',
    ];

    /**
     * 获取未处理条目
     */
    public function scopeUnHandle($query)
    {
        return $query->where('is_handle', 0);
    }

    /**
     * 获取已处理条目
     */
    public function scopeHandled($query)
    {
        return $query->where('is_handle', 1);
    }
}
