<?php

namespace App\Models\TaobaoOpen;

use App\Models\Model;

class TaobaoTopSyncLog extends Model
{
    protected $table = 'taobao_top_sync_logs';

    protected $fillable = [
        'merchant',
        'start_sync_time',
        'end_sync_time',
        'request_start_sync_time',
        'request_end_sync_time',
        'value',
    ];

    protected $casts = [
        'value' => 'array',
    ];

    public function scopeMerchant($query, $merchant)
    {
        return $query->where('merchant', $merchant);
    }
}
