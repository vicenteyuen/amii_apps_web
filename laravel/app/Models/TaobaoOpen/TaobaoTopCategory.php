<?php

namespace App\Models\TaobaoOpen;

use App\Models\Model;

class TaobaoTopCategory extends Model
{
    protected $table = 'taobao_top_categorys';

    protected $fillable = [
        'cid',
        'parent_cid',
        'name',
        'category_id'
    ];

    public function parent()
    {
        return $this->belongsTo('\App\Models\TaobaoOpen\TaobaoTopCategory', 'parent_cid', 'cid');
    }

    // category
    public function category()
    {
        return $this->hasOne('EcommerceManage\Models\Category', 'id', 'category_id');
    }
}
