<?php

namespace App\Models\TaobaoOpen;

use App\Models\Model;

class TaobaoTopProductHandle extends Model
{
    protected $table = 'taobao_top_product_handles';

    protected $fillable = [
        'taobao_top_inventory_id',
        'merchant',
        'taobao_outer_id',
        'taobao_product_id',
        'is_publish',
        'is_sync_update',
        'sync_update_start_time',
        'sync_update_end_time',
        'value',
        'handle_picture_status',
        'handle_picture',
        'handle_wap_vision_status',
        'handle_wap_vision',
        'check_skus',
        'check_cate'
    ];

    protected $casts = [
        'value' => 'array',
        'handle_picture' => 'array',
        'handle_wap_vision' => 'array',
    ];

    /**
     * 未发布条目查询
     */
    public function scopeUnPublish($query)
    {
        return $query->where('is_publish', 0);
    }

    /**
     * 待发布条目查询
     */
    public function scopeDuringPublish($query)
    {
        return $query->where('is_publish', 0)
            ->where('handle_picture_status', 2)
            ->where('handle_wap_vision_status', 2)
            ->where('check_skus', 1)
            ->where('check_cate', '>', 0);
    }

    /**
     * 获取待更新条目
     */
    public function scopeDuringUpdate($query)
    {
        return $query->where('is_publish', 0)
            ->where('handle_picture_status', 2)
            ->where('handle_wap_vision_status', 2);
    }

    /**
     * 未处理条目查询
     */
    public function scopeUnHandle($query)
    {
        return $query->where('is_publish', 0)
            ->where('handle_picture_status', 0)
            ->where('handle_wap_vision_status', 0);
    }

    /**
     * 图片信息未处理查询
     */
    public function scopeUnHandlePicture($query)
    {
        return $query->where('handle_picture_status', 0);
    }

    /**
     * 图片信息正在处理中查询
     */
    public function scopeHandlingPicture($query)
    {
        return $query->where('handle_picture_status', 1);
    }

    /**
     * 图片信息处理完成查询
     */
    public function scopeHandledPicture($query)
    {
        return $query->where('handle_picture_status', 2);
    }

    /**
     * 图片信息不需要处理查询
     */
    public function scopeNoHandlePicture($query)
    {
        return $query->where('handle_picture_status', 3);
    }

    /**
     * 无线详情信息未处理查询
     */
    public function scopeUnHandleWapVision($query)
    {
        return $query->where('handle_picture_status', 0);
    }

    /**
     * 无线详情信息正在处理中查询
     */
    public function scopeHandlingWapVision($query)
    {
        return $query->where('handle_picture_status', 1);
    }

    /**
     * 无线详情信息处理完成查询
     */
    public function scopeHandledWapVision($query)
    {
        return $query->where('handle_picture_status', 2);
    }

    /**
     * 无线详情信息不需要处理查询
     */
    public function scopeNoHandleWapVision($query)
    {
        return $query->where('handle_picture_status', 3);
    }
}
