<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class ProductImage extends Model
{
    protected $table = 'product_images';

    protected $fillable = [
        'product_id',
        'image',
        'name',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    // get attribute
    public function getImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'product');
    }
    public function product()
    {
        return $this->hasOne('Ecommerce\Models\Product', 'id', 'product_id');
    }
}
