<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserScanLog extends Model
{
    use SoftDeletes;

    protected $table = 'user_scan_logs';
    protected $dates = [
        'deleted_at',
    ];

    
    protected $fillable = [
        'user_id',
        'sell_product_id',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
        'deleted_at_millisecond',
        'deleted_at',
    ];

    protected $appends = [
        'date',
    ];
    
    public function getDateAttribute()
    {
        return date('Y-m-d H:i:s', strtotime($this->attributes['updated_at']));
    }
    public function sellProduct()
    {
        return $this->hasOne('App\Models\SellProduct', 'id', 'sell_product_id')
            ->where('status', 1);
    }
}
