<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserComment extends Model
{
    use SoftDeletes;
    protected $table = 'user_comments';

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'inventory_id',
        'content',
        'product_sell_id',
        'status',
        'comment_rank_id',
    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
        'deleted_at_millisecond',
        'deleted_at',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function inventory()
    {
        return $this->hasOne('Ecommerce\Models\Inventory', 'id', 'inventory_id');
    }

    public function image()
    {
        return $this->hasMany('App\Models\CommentImage', 'comment_id', 'id');
    }

    public function sellProduct()
    {
         return $this->belongsTo('App\Models\SellProduct', 'product_sell_id', 'id');
    }
}
