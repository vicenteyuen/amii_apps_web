<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserCoupon extends Model
{
    use SoftDeletes;

    protected $table = 'user_coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'coupon_id',
        'user_id',
        'code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'coupon_id',
        'deleted_at_millisecond',
        'deleted_at',
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    protected $attributes = [
        'status' => 0
    ];

    /**
     * 获取可使用优惠
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }

    /**
     * 优惠券信息
     */
    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id', 'id');
    }
}
