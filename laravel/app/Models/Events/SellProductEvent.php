<?php

namespace App\Models\Events;

use Carbon\Carbon;
use App\Jobs\SellProductNumberListenJob;

class SellProductEvent implements EventInterface
{
    public function creating($model)
    {
        //
    }
    public function created($model)
    {
        if ($model->number == 0) {
            $sellProductId = $model->id;
            $time = Carbon::now()->toDateTimeString();
            $data = compact('sellProductId', 'time');
            $job = new SellProductNumberListenJob('created', $data);
            dispatch($job);
        }
    }
    public function updating($model)
    {
        //
    }
    public function updated($model)
    {
        $time = Carbon::now()->toDateTimeString();
        $sellProductId = $model->id;

        if ($model->number == 0) {
            $type = 'set';
        } else {
            $type = 'del';
        }

        $data = compact('sellProductId', 'time', 'type');
        $job = new SellProductNumberListenJob('updated', $data);
        dispatch($job);
    }
    public function saving($model)
    {
        //
    }
    public function saved($model)
    {
        //
    }
    public function deleting($model)
    {
        //
    }
    public function deleted($model)
    {
        //
    }
    public function restoring($model)
    {
        //
    }
    public function restored($model)
    {
        //
    }
}
