<?php

namespace App\Models\Events;

/**
 * Class Event
 * static::observe(new Event());
 * @package App\Models\Events
 */
class WithdrawRecordEvent implements EventInterface
{
    public function creating($model)
    {
        if(!$model->trade_no) $model->trade_no = str_random().date('YmdHis');
    }

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model|\App\Models\WithdrawRecord $model
     * @return mixed|void
     */
    public function created($model)
    {
        if(!($model->state == 1 && $model->channel && $model->channel->user))return;
        $model->channel->user->decrement('cumulate_profit', $model->amount);
    }

    public function updating($model)
    {
    }

    public function updated($model)
    {
        if(!($model->channel && $model->channel->user))return;
        /**
         * @var \App\Models\User $user
         */
        $user = $model->channel->user;
        switch ($model->state){
            case 1:
                $model->channel->user->decrement('cumulate_profit', $model->amount);
                break;
            case 2:
                $user->balances()->create([
                    'value' => -$model->amount,
                    'type' => 3,
                ]);
                break;
            case 3:
                $user->increment('cumulate_profit', $model->amount);
                break;
        }
    }

    public function saving($model)
    {
    }

    public function saved($model)
    {
    }

    public function deleting($model)
    {
    }

    public function deleted($model)
    {
    }

    public function restoring($model)
    {
    }

    public function restored($model)
    {
    }
}
