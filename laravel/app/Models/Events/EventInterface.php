<?php

namespace App\Models\Events;

interface EventInterface
{
    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function creating($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function created($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function updating($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function updated($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function saving($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function saved($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function deleting($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function deleted($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function restoring($model);

    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model $model
     * @return mixed|bool
     */
    public function restored($model);
}