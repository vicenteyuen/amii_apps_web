<?php

namespace App\Models\Events;

/**
 * Class Event
 * static::observe(new Event());
 * @package App\Models\Events
 */
class WithdrawChannelEvent implements EventInterface
{
    /**
     * @param \Eloquent|\Illuminate\Database\Eloquent\Model|\App\Models\WithdrawChannel $model
     * @return bool
     */
    public function creating($model)
    {
        $allow_channels = ['wechat', 'weweb', 'wemp', 'weapp', 'alipay'];
        // $allow_channels = ['alipay'];
        if(!in_array($model->type, $allow_channels))return false;
    }

    public function created($model)
    {
    }

    public function updating($model)
    {
    }

    public function updated($model)
    {
    }

    public function saving($model)
    {
    }

    public function saved($model)
    {
    }

    public function deleting($model)
    {
    }

    public function deleted($model)
    {
    }

    public function restoring($model)
    {
    }

    public function restored($model)
    {
    }


}
