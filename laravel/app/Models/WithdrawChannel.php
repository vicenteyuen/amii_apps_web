<?php

namespace App\Models;

use App\Models\Events\WithdrawChannelEvent;

class WithdrawChannel extends Model
{
    protected $fillable = [
        'type',
        'account_number',
        'account_name'
    ];

    protected static function boot()
    {
        parent::boot();
        static::observe(new WithdrawChannelEvent());
    }

    /**
     * 修改渠道名
     */
    public function getTypeAttribute($value)
    {
        if (in_array($value, ['wechat', 'weweb', 'wemp', 'weapp'])) {
            $value = 'wechat';
        }
        return $value;
    }

    public function record()
    {
        return $this->hasMany(WithdrawRecord::class, 'channel_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
