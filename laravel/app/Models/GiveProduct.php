<?php

namespace App\Models;

class GiveProduct extends Model
{
    protected $table = 'give_products';

    protected $fillable = [
        'sell_product_id',
        'user_id',
        'order_id',
        'status',
        'activity_id'
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
        'status'
    ];

    // sellProduct
    public function sellProduct()
    {
        return $this->belongsTo('App\Models\SellProduct', 'sell_product_id', 'id');
    }

    // activity
    public function activity()
    {
        return $this->belongsTo('\App\Models\Activity', 'activity_id', 'id');
    }
}
