<?php

namespace App\Models;

class UserGameAdvice extends Model
{
    protected $table = 'user_game_advices';

    protected $fillable = [
        'user_id',
        'status',
        'provider',
        'text',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
