<?php

namespace App\Models;

class Gain extends Model
{
    protected $table = 'gains';
    
    protected $fillable = [
        'user_id',
        'p_user_id',
        'income',
        'total_orders',
        'balance_id',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
    ];
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function balance()
    {
        return $this->hasOne('App\Models\Balance', 'id', 'balance_id');
    }
}
