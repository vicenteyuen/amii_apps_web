<?php

namespace App\Models;

class Balance extends Model
{
    protected $table = 'balance';

    protected $fillable = [
        'user_id',
        'value',
        'type',
        'come_user_id',
        'order_id',
        'status',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
    protected $appends = [
        'type_str',
        'admin_created_at',
    ];

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function getAdminCreatedAtAttribute()
    {
        return date('Y-m-d H:i:s', strtotime($this->attributes['created_at']));
    }

    public function getTypeStrAttribute()
    {
        switch ($this->attributes['type']) {
            case 1:
                return '部落盈利';
                break;

            case 2:
                return '消费';
                break;

            case 3:
                return '提现';
                break;
            case 4:
                return '退货退款扣除佣金';
                break;
            case 5:
                return '退还垫付运费';
                break;
            case 6:
                return '退回余额';
                break;
            
            default:
                # code...
                break;
        }
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'come_user_id');
    }
    public function order()
    {
        return $this->hasOne('App\Models\Order', 'id', 'order_id');
    }
}
