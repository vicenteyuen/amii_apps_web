<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class BrandCategory extends Model
{
    protected $table = 'brand_categorys';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'category_id',
        'image',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];

    public function brand()
    {
        return $this->hasOne('EcommerceManage\Models\Brand', 'id', 'brand_id');
    }
    public function category()
    {
        return $this->hasOne('EcommerceManage\Models\Category', 'id', 'category_id');
    }

    public function productBrand()
    {
        return $this->hasMany('App\Models\BrandProduct', 'brand_id', 'brand_id');
    }

     // 获取品牌分类图片url
    public function getImageAttribute()
    {
        $data =  ImageHelper::getImageUrl($this->attributes['image'], 'brandCateHeard', 'source');
        return  $data;

    }
}
