<?php

namespace App\Models;

class Channel extends Model
{
    protected $table = 'channels';

    protected $fillable = [
        'name',
        'code',
        'downloads',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
