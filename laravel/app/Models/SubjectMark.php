<?php

namespace App\Models;

class SubjectMark extends Model
{
    protected $table = 'subject_marks';

    protected $fillable = [
        'user_id',
        'subject_id',
    ];
}
