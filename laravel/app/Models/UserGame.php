<?php

namespace App\Models;

use Carbon\Carbon;

class UserGame extends Model
{
    protected $table = 'user_games';

    protected $fillable = [
        'user_id',
        'sell_product_id',
        'count',
        'finished_number',
        'expired',
        'success',
        'order_id',
        'expired_time',
        'order_remain_time',
        'gaming_end_time',
        'row',
        'col',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
    ];

    protected $appends =[
        'status',
        'remain_time',
        'has_inventory',
        'can_commit',
    ];

    //是否可合成游戏商品
    public function getCanCommitAttribute()
    {
        if ($this->attributes['success'] == 1) {
            $check = app('amii.game.order')->checkFinishedGame();
            if ($check) {
                return 1;
            }
            return 0;
        }
        return 1;

    }

    // 游戏过期,订单过期再次发起游戏是否有库存
    public function getHasInventoryAttribute()
    {
        if ($this->attributes['expired'] == 0 || $this->attributes['order_status'] == 2) {
            $number = SellProduct::where('id', $this->attributes['sell_product_id'])
                ->value('number');
            if ($number > 0) {
                return 1;
            }
            return 0;
        }
        return 1;
    }

    // 游戏剩余时间
    public function getRemainTimeAttribute()
    {
        if ($this->attributes['expired'] == 1) {
            return 0;
        }
        if ($this->attributes['gaming_end_time']) {
            $gameTime = Carbon::parse($this->attributes['gaming_end_time'])->timestamp - Carbon::now()->timestamp;
        } else {
            // 设置 gaming_end_time
            if ($this->attributes['created_at'] >= Carbon::parse('2017-06-20 12:00:00') && $this->attributes['created_at'] < Carbon::parse('2017-06-21 12:00:00')) {
                $gamingEndTime = Carbon::parse($this->attributes['created_at'])->addHours(60)->toDateTimeString();
            } else {
                $gamingEndTime = Carbon::parse($this->attributes['created_at'])->addHours(24)->toDateTimeString();
            }
            UserGame::where('id', $this->attributes['id'])
                ->update([
                    'gaming_end_time' => $gamingEndTime
                ]);
            $gameTime = Carbon::parse($this->attributes['created_at'])->addDays(1)->timestamp - Carbon::now()->timestamp;
        }
        if ($gameTime <=0) {
            $gameTime = 0;
            //手动更新状态
            if ($this->attributes['expired'] == 0 && $this->attributes['success'] == 0) {
                app('amii.user.game')->update($this->attributes['id'], ['expired' => 1]);
                Order::where('id', $this->attributes['order_id'])
                        ->update(['order_status' => 3]);
            }
        }
        return $gameTime;
    }

    //订单合成剩余时间
    public function getOrderRemainTimeAttribute($value)
    {
        if ($this->attributes['success'] == 1 && $this->attributes['order_status'] == 0) {
            $orderRemainTime = 24 * 60 * 60 * 5 + $value - time();
            if ($orderRemainTime <= 0) {
                $orderRemainTime = 0;
                //手动更新状态
                if ($this->attributes['order_status'] != 2) {
                    app('amii.user.game')->update($this->attributes['id'], ['order_status' => 2]);
                    Order::where('id', $this->attributes['order_id'])
                        ->update(['order_status' => 3]);
                }
            }
            return $orderRemainTime;
        }
        return 0;
    }

    // 游戏状态
    public function getStatusAttribute()
    {
        if ($this->attributes['success'] == 1 && $this->attributes['order_status'] == 0) {
            //success 未提交订单
            return 1;
        }
        if ($this->attributes['success'] == 0 && $this->attributes['expired'] == 1) {
            //expired
            return 2;
        }
        if ($this->attributes['success'] == 0 && $this->attributes['expired'] == 0) {
            //playing
            return 3;
        }
        if ($this->attributes['success'] == 1 && $this->attributes['order_status'] == 1) {
            //success 提交订单(订单完成)
            return 4;
        }
        if ($this->attributes['success'] == 1 && $this->attributes['order_status'] == 2) {
            //success 订单过期
            return 5;
        }

    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function sellProduct()
    {
        return $this->belongsTo('App\Models\SellProduct', 'sell_product_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }
}
