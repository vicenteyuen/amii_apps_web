<?php

namespace App\Models;

class UserNotifyMessage extends Model
{

    protected $table = 'user_notify_messages';

    protected $hidden = [
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    // 与user建立一对多点关系
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
