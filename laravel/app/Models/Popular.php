<?php

namespace App\Models;

use App\Helpers\ImageHelper;
use App\Libraries\Nestedset\NodeTrait;

class Popular extends Model
{
    use NodeTrait;

    protected $table = 'populars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'title',
        'image',
        'provider',
        'parent_id',
        'resource',
        'descript',
        'is_click',
        'image',
        'weapp_image',
        'weight',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'status',
        'orderby',
        '_lft',
        '_rgt',
        'provider_value',
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];


    /**
     * get provider attribute
     */
    public function getProviderAttribute($value)
    {
        switch ($value) {
            case '1':
                $provider = 'category';
                break;
            case '2':
                $provider = 'brand';
                break;
            case '3':
                $provider = 'product';
                break;
            case '4':
                $provider = 'html';
                break;
            case '5':
                $provider = 'brandcate';
                break;

            default:
                if (! $this->attributes['is_click']) {
                    $provider = 'unclick';
                } else {
                    $provider = 'undefine';
                }
                break;
        }

        return $provider;
    }

    /**
     * get provider value
     */
    public function getProviderValueAttribute()
    {
        switch ($this->provider) {
            case 'category':
                $value = 1;
                break;
            case 'brand':
                $value = 2;
                break;
            case 'product':
                $value = 3;
                break;
            case 'html':
                $value = 4;
                break;
             case 'brandcate':
                $value = 5;
                break;
            default:
                $value = 0;
                break;
        }

        return $value;
    }

    /**
     * get image attribute
     */
    public function getImageAttribute($value)
    {
        if ($this->attributes['parent_id'] == 0) {
            return ImageHelper::getImageUrl($value, 'popular');
        }
        return $value;
    }

    /**
     * get weapp image attribute
     */
    public function getWeappImageAttribute($value)
    {
        if ($this->attributes['parent_id'] == 0) {
            return ImageHelper::getImageUrl($value, 'popularWeapp');
        }
        return $value;
    }
}
