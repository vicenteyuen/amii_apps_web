<?php

namespace App\Models;

use App\Models\Model;

class CouponGrant extends Model
{
    protected $table = 'coupon_grants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'coupon_id',
        'code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'coupon_id',
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    /**
     * 优惠券可领用状态
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 0);
    }

    /**
     * 优惠券
     */
    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id', 'id');
    }
}
