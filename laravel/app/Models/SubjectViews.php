<?php

namespace App\Models;

class SubjectViews extends Model
{
    protected $table = 'subject_views';

    protected $fillable = [
        'id',
        'subject_id',
        'user_id',
    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'created_at',
        'updated_at',
    ];
}
