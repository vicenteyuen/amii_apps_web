<?php

namespace App\Models;

use App\Helpers\ImageHelper;
use App\Helpers\GamePuzzlesHelper;
use App\Models\Events\SellProductEvent;

class SellProduct extends Model
{

    protected $table = 'sell_products';

    protected $fillable = [
        'product_id',
        'number',
        'image',
        'base_price',
        'status',
        'provider',
        'product_point',
        'puzzles',
        'market_price',
        'inventory_number',
        'number_zero_time'
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
        'default_order_by',
    ];

    protected $appends = [
        'all_image',
        'share_url',
        'game_puzzles',
    ];

    public static $allImageEmpty = false;

    // 注册服务
    protected static function boot()
    {
        parent::boot();
        static::observe(new SellProductEvent());
    }

    // 拼图块数
    public function getGamePuzzlesAttribute()
    {
        if ($this->attributes['provider'] == 1 || $this->attributes['provider'] == 3) {
            if (isset($this->attributes['puzzles']) && $this->attributes['puzzles']) {
                return $this->attributes['puzzles'];
            }
            if (!isset($this->attributes['market_price'])) {
                return null;
            }
            $puzzles =  GamePuzzlesHelper::countPuzzles($this->attributes['market_price']);
            if ($puzzles) {
                return $puzzles;
            }
            return null;
        }
        return null;

    }

    // 获取商品分享链接
    public function getShareUrlAttribute()
    {
        if ($this->product) {
            return url('/wechat/mall/product/' . $this->product->id);
        }
        return;
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            return ImageHelper::getImageUrl($this->attributes['image'], 'product');
        }
        $image = app('sellproduct')->image($this->attributes['product_id']);
        if ($image) {
            SellProduct::where('id', $this->attributes['id'])
                ->update(['image' => basename($image[0])]);
            return $image[0];
        }
        return null;
    }



    public function getSmallImageAttribute()
    {
        if ($this->attributes['image']) {
            return ImageHelper::getImageUrl($this->attributes['image'], 'product', 'small');
        }

        return null;
    }


    public function getAllImageAttribute()
    {
        if (static::$allImageEmpty) {
            return [];
        }
        return app('sellproduct')->images($this->attributes['id']);
    }

    public function product()
    {
        return $this->hasOne('Ecommerce\Models\Product', 'id', 'product_id');
    }

    // oms product
    public function omsProduct()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    // 库存
    public function inventories()
    {
        return $this->hasMany('Ecommerce\Models\Inventory', 'product_sell_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'product_id');
    }

    public function collection()
    {
        return $this->hasOne('App\Models\UserCollection', 'sell_product_id', 'id');
    }

    // 品牌
    public function brand()
    {
        return $this->belongsToMany('EcommerceManage\Models\Brand', 'brand_sell_products', 'sell_product_id', 'brand_id');
    }
    public function userGame()
    {
        return $this->hasMany('App\Models\UserGame', 'sell_product_id', 'id');
    }

    public function game()
    {
        return $this->hasOne('App\Models\UserGame', 'sell_product_id', 'id');
    }

    public function firstPurchase()
    {
        return $this->hasOne('App\Models\FirstPurchase', 'sell_product_id', 'id');
    }

    public function GiveProductHistory()
    {
        return $this->hasOne('App\Models\GiveProductHistory', 'sell_product_id', 'id');
    }

    public function userComment()
    {
        return $this->hasMany('App\Models\UserComment', 'product_sell_id', 'id');
    }

    public function productBrand()
    {
        return $this->hasMany('App\Models\BrandProduct', 'product_id', 'id');
    }

    public function sellAttribute()
    {
        return $this->hasMany('App\Models\sellAttribute', 'product_sell_attribute_id', 'id');
    }

    // 远程一对多
    public function sellAttributeValue()
    {
        return $this->hasManyThrough('App\Models\SellAttributeValue', 'App\Models\SellAttribute', 'product_sell_attribute_id', 'sell_attribute_id', 'id');
    }
}
