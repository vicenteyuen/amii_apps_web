<?php

namespace App\Models;

class OrderProduct extends Model
{
    protected $table = 'order_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'order_refund_id'
    ];

    /**
     * 添加attribute
     */
    protected $appends = [
        'refund_status',
    ];

    public $timestamps = false;

    /**
     * 获取订单商品售后状态
     */
    public function getRefundStatusAttribute()
    {
        return app('amii.order.refund')->isRefund($this->attributes['id']);
    }

    /**
     * 获取商品详细信息 关联关系
     */
    public function sellProduct()
    {
        return $this->belongsTo('App\Models\SellProduct', 'sell_product_id', 'id');
    }

    /**
     * 获取商品库存
     */
    public function inventory()
    {
        return $this->belongsTo('Ecommerce\Models\Inventory', 'inventory_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|OrderProductRefund
     */
    public function refund()
    {
        return $this->hasOne(OrderProductRefund::class);
    }
}
