<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes;

    protected $table = 'feedbacks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'user_id',
        'content',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'deleted_at_millisecond',
        'deleted_at',
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
