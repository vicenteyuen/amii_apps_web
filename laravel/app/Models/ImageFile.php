<?php

namespace App\Models;

class ImageFile extends Model
{
    protected $table = 'image_files';

    protected $fillable =[
        'name',
        'desc',
    ];

    protected $hidden =[
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function image()
    {
        return $this->hasMany('App\Models\Image', 'image_file_id', 'id');
    }
}
