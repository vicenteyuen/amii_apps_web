<?php

namespace App\Models;

class BrandSellProduct extends Model
{
    protected $table = 'brand_sell_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sell_product_id',
        'brand_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];
}
