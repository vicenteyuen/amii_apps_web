<?php

namespace App\Models;

class SellAttribute extends Model
{
    protected $table = 'sell_attributes';

    protected $fillable = [
        'attribute_id',
        'product_sell_attribute_id',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function attribute()
    {
        return $this->hasOne('App\Models\Attribute', 'id', 'attribute_id');
    }

    public function sellAttributeValue()
    {
        return $this->hasMany('App\Models\SellAttributeValue', 'sell_attribute_id', 'id');
    }
}
