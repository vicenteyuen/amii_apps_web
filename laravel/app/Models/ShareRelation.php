<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShareRelation extends Model
{
    protected $table = 'share_relations';

    protected $fillable = [
        'share_id',
        'sell_product_id',
        'subject_id',
        'video_id',
        'code_id',
        'tribe_id',
        'activity_id',
        'puzzle_game_id',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    // share
    public function share()
    {
        return $this->belongsTo('App\Models\Share', 'share_id', 'id');
    }
}
