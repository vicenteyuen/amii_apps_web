<?php

namespace App\Models;

class UserQuestion extends Model
{
    protected $table = 'user_questions';
    protected $fillable = [
        'question',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];
    public function option()
    {
        return $this->hasMany('App\Models\SuitSize', 'question_id', 'id');
    }
}
