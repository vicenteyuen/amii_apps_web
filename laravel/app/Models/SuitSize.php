<?php

namespace App\Models;

class SuitSize extends Model
{
    protected $table = 'suit_sizes';
    protected $fillable = [
        'type',
        'kinds',
        'question_id',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
