<?php

namespace App\Models;

class HotSearch extends Model
{
    protected $table = 'hot_search';

    protected $fillable = [
        'name',
        'number',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
