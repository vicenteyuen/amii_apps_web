<?php

namespace App\Models;

use Carbon\Carbon;

class Token extends Model
{
    protected $table = 'tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_id',
        'type',
        'code',
        'expires',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'type',
        'expires',
    ];

    // 校验token是否有效
    public function getIsExpiresAttribute()
    {
        if ($this->attributes['updated_at'] > Carbon::now()->subSeconds($this->attributes['expires'])) {
            return true;
        }
        return false;
    }

    // token user
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'ref_id', 'id');
    }
}
