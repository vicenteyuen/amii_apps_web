<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserCollection extends Model
{
    use SoftDeletes;

    protected $table = 'user_collections';
    protected $dates = [
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'sell_product_id',
        'provider',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
        'deleted_at_millisecond',
        'deleted_at',
    ];
    public function sellProduct()
    {
        return $this->hasOne('App\Models\SellProduct', 'id', 'sell_product_id');
    }
}
