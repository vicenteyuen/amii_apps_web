<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{
    use SoftDeletes;

    protected $table = 'user_address';

    protected $dates = [
        'deleted_at',
    ];
    
    protected $fillable = [
        'realname',
        'phone',
        'region_id',
        'detail',
        'user_id',
        'status',
        'post_code',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
        'deleted_at_millisecond',
        'deleted_at',
    ];
}
