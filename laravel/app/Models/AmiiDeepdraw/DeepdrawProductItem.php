<?php

namespace App\Models\AmiiDeepdraw;

use App\Models\Model;

class DeepdrawProductItem extends Model
{
    protected $table = 'deepdraw_product_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_publish',
        'deepdraw_product_id',
        'merchant_id',
        'basic',
        'basic_status',
        'picture',
        'picture_status',
        'picture_handle',
        'picture_handle_status',
        'vision',
        'vision_status',
        'vision_handle',
        'vision_handle_status',
        'sku',
        'sku_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    /**
     * 更改列
     */
    protected $casts = [
        'basic' => 'array',
        'picture' => 'array',
        'picture_handle' => 'array',
        'vision' => 'array',
        'vision_handle' => 'array',
        'sku' => 'array',
    ];

    /**
     * 待处理数据
     */
    public function scopeStatus($query)
    {
        return $query->where('basic_status', 1)
            ->where('picture_status', 1)
            ->where('vision_status', 1)
            ->where('sku_status', 1)
            ->where('picture_handle_status', 1)
            ->where('vision_handle_status', 1);
        ;
    }

    /**
     * 未发布商品
     */
    public function scopePublishing($query)
    {
        return $query->where('is_publish', 0);
    }

    /**
     * 获取未处理过数据的条目
     */
    public function scopeUnHandle($query)
    {
        return $query->where(function($q) {
            return $q->where('picture_status', 1)
                ->where('picture_handle_queue_status', 0);
        })
        ->orWhere(function($q) {
            return $q->where('vision_status', 1)
                ->where('vision_handle_queue_status', 0);
        });
    }

    /**
     * 获取处理过的条目
     */
    public function scopeHandle($query)
    {
        return $query->where('picture_handle_status', 1)
            ->where('vision_handle_status', 1);
    }

    /**
     * 数据不完整的
     */
    public function scopeUnWhole($query)
    {
        return $query->where('basic_status', 0)
            ->orWhere('picture_status', 0)
            ->orWhere('vision_status', 0)
            ->orWhere('sku_status', 0);
    }
}
