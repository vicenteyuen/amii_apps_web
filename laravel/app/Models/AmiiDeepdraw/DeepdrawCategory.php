<?php

namespace App\Models\AmiiDeepdraw;

use App\Models\Model;

class DeepdrawCategory extends Model
{

    protected $table = 'deepdraw_categorys';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'deepdraw_category_id',
        'data',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    /**
     * 更改列
     */
    protected $casts = [
        'data' => 'array',
    ];
}
