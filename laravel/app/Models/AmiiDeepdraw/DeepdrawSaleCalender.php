<?php

namespace App\Models\AmiiDeepdraw;

use App\Models\Model;

class DeepdrawSaleCalender extends Model
{
    protected $table = 'deepdraw_sale_calenders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id',
        'sale_id',
        'day',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    /**
     * 未处理的上货期数
     */
    public function scopeUndo($query)
    {
        return $query->where('status', 0);
    }
}
