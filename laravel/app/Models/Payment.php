<?php

namespace App\Models;

class Payment extends Model
{
    protected $table = 'payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'provider',
        'trade_no',
        'total_fee',
        'subject',
        'paid_time',
        'refund',
        'refund_time',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * 获取支付完成
     */
    public function paid($query)
    {
        return $query->where('status', 1)
            ->where('refund', 0);
    }
}
