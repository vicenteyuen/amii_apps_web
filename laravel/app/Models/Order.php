<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\OrderHelper;

/**
 * @property OrderProductRefund $orderProductRefunds
 */
class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider',
        'user_id',
        'order_sn',
        'order_status',
        'invoice',
        'shipping_status',
        'pay_status',
        'is_appraise',
        'can_refund',
        'refund_time',
        'is_refund',
        'is_refund_product',
        'shipping_region',
        'shipping_address',
        'best_time',
        'product_amount',
        'remark',
        'best_time',
        'order_start_time',
        'order_end_time',
        'pay_start_time',
        'pay_end_time',
        'cart_ids',
        'refunded_balance',
        'refunded_point',
    ];

    public static $payTime = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at_millisecond',
        'deleted_at',
        'updated_at_millisecond',
        'updated_at',
    ];

    /**
     * 类型转换
     */
    protected $casts = [
        'shipping_address' => 'array',
        'cart_ids' => 'array',
    ];

    /**
     * 添加attribute
     */
    protected $appends = [
        'status_str',
        'paid_time',
        'order_status_str',
    ];

    /**
     * 需付金额区别格式
     */
    protected function getOrderFeeAttribute($value)
    {
        if ($this->attributes['provider'] == 'standard') {
            return $value;
        } else if ($this->attributes['provider'] == 'point') {
            return intval($value);
        } else {
            return $value;
        }
    }

    /**
     * 获取未确认订单
     */
    public function scopeUnconfirm($query)
    {
        return $query->where('order_status', 0);
    }

    /**
     * 获取正常订单
     */
    public function scopeConfirm($query)
    {
        return $query->whereIn('order_status', [1, 2]);
    }

    /**
     * 获取已取消订单
     */
    public function scopeCancel($query)
    {
        return $query->where('order_status', 2);
    }

    /**
     * 获取待支付订单
     */
    public function scopePaying($query)
    {
        return $query->where('order_status', 1)
            ->where('pay_status', 0)
            ->where('provider', '!=', 'game');
    }

    /**
     * 获取已支付订单
     */
    public function scopePaid($query)
    {
        return $query->where('pay_status', 1);
    }

    /**
     * 获取待发货订单
     */
    public function scopeSending($query)
    {
        return $query->where('order_status', 1)
            ->where('pay_status', 1)
            ->where('shipping_status', 0);
    }

    /**
     * 获取待收货订单
     */
    public function scopeReceiving($query)
    {
        return $query->where('order_status', 1)
            ->where('pay_status', 1)
            ->where('shipping_status', 1);
    }

    /**
     * 获取待评价订单
     */
    public function scopeAppraising($query)
    {
        return $query->where('order_status', 1)
            ->where('pay_status', 1)
            ->where('shipping_status', 2)
            ->where('is_appraise', 0);
    }

    /**
     * 获取可申请售后
     */
    public function scopeCanRefund($query)
    {
        return $query->where('can_refund', 1)
            ->where('is_refund', 0);
    }

    /**
     * 获取可开启售后订单
     */
    public function scopeCanOpenRefund($query)
    {
        return $query->where('order_status', 1)
            ->where('pay_status', 1)
            ->where('shipping_status', '>', 0)
            ->where('can_refund', 0);
    }

    /**
     * 可关闭售后
     */
    public function scopeCanCloseRefund($query)
    {
        return $query->where('can_refund', 1);
    }

    /**
     * 获取售后中订单
     */
    public function scopeRefunding($query)
    {
        return $query->has('orderProducts.refund');
    }

    /**
     * 获取售后中的商品数量
     */
    public function scopeRefunds($query)
    {
        return $query->whereHas('orderProducts', function ($q1) {
            return $q1->whereHas('refund', function ($q2) {
                return $q2->whereIn('refund_status', [0,1,2]);
            });
        });
    }

    /**
     * 可删除订单
     */
    public function scopeCanDelete($query)
    {
        return $query->where(function ($q) {
            return $q->where('order_status', 2)
                ->orWhere(function ($q1) {
                    return $q1->where('order_status', 1)//                        ->where('is_appraise', 1)
                        ;
                });
        });
    }

    /**
     * 获取已完成订单
     */
    public function scopeSuccess($query)
    {
        return $query->where('is_appraise', 1);
    }

    /**
     * 订单展示状态字符串
     */
    public function getStatusStrAttribute()
    {
        return OrderHelper::orderStatus($this->attributes);
    }

    /**
     * 订单状态字符串
     */
    public function getOrderStatusStrAttribute()
    {
        return OrderHelper::orderStatusStr($this->attributes);
    }

    /**
     * 订单支付完成时间
     */
    public function getPaidTimeAttribute()
    {
        if (static::$payTime) {
            return null;
        }
        $paidPayment = Payment::where('order_id', $this->attributes['id'])
            ->where('status', 1)
            ->orderBy('paid_time', 'desc')
            ->get();
        if ($paidPayment->isEmpty()) {
            if ($this->attributes['pay_status'] == 1) {
                return $this->attributes['pay_end_time'];
            }
            return null;
        }
        return $paidPayment[0]->paid_time;
    }

    public function incrementVolume()
    {
        $this->orderProducts->each(function ($product) {
            $product->sellProduct->increment('volume');
            $product->sellProduct->increment('month_volume');
        });
    }

    /**
     * 订单关联商品
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|OrderProduct
     */
    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough|OrderProductRefund
     */
    public function orderProductRefunds()
    {
        return $this->hasManyThrough(OrderProductRefund::class, OrderProduct::class, 'order_id', 'order_product_id');
    }

    /**
     * 订单支付纪录
     */
    public function payments()
    {
        return $this->hasMany('App\Models\Payment', 'order_id', 'id');
    }

    /**
     * 快递
     **/
    public function packages()
    {
        return $this->hasMany('App\Models\OrderPackage', 'order_id', 'id');
    }

    /**
     * 首单活动
     */
    public function giveProduct()
    {
        return $this->hasOne('\App\Models\GiveProduct', 'order_id', 'id');
    }

    /**
     * 订单用户
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function game()
    {
        return $this->hasOne('App\Models\UserGame', 'order_id', 'id');
    }

    /**
     * 检查售后状态，改变订单状态
     */
    public function checkRefundChangeStatus()
    {
        $count_products = $this->orderProducts()->count();

        $orderProductRefunds = $this->orderProductRefunds()->where('status', 1)->get();
        $count_complete_refund = $orderProductRefunds->count();
        $count_pay_refund = $orderProductRefunds
            ->where('provider', 'pay')
            ->count();

        if ($count_complete_refund == $count_products
            && !($this->shipping_status == 2 && $count_pay_refund > 0)
        ) {
            $this->order_status = 2;
            $this->save();
        }
    }
    
    public function getCanRefundAttribute($value)
    {
        if ($this->attributes['provider'] == 'game') {
            return 0;
        }
        return $value;
    }
}
