<?php

namespace App\Models;

class UserAnswer extends Model
{
    protected $table = 'user_answer';
    protected $fillable = [
        'question_id',
        'user_id',
        'user_suit_id',
        'option_id',
        'answer',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];

    public function question()
    {
        return $this->hasOne('App\Models\UserQuestion', 'id', 'question_id');
    }
}
