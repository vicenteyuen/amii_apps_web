<?php

namespace App\Models;

class LevelEnable extends Model
{
    protected $table = 'level_enables';

    protected $fillable = [
        'level_id',
        'enable_id',
        'get',
        'describe',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
    public function enable()
    {
        return $this->belongsTo('App\Models\Enable', 'enable_id', 'id');
    }
}
