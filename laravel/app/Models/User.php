<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use App\Models\Model;
use Carbon\Carbon;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Helpers\ImageHelper;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'password',
        'gender',
        'uuid',
        'points',
        'sum_points',
        'avatar',
        'status',
        'nickname',
        'realname',
        'email',
        'orders',
        'level',
        'purchase_price',
        'return_money',
        'user_game_id',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'payment_password',
        'remember_token',
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
        'last_log_time_millisecond',
        'last_log_ip',
        'birthday_millisecond',
    ];

    protected $appends = [
        'join_day',
        'age',
        'has_payment_password',
        'is_weixin',
        'is_weapp',
        'is_qq',
        'is_weibo',
        'is_alipay',
    ];

    public static $withoutAppends = false;
    public static $calcPurchasePrice = false;

    protected function getArrayableAppends()
    {
        if (self::$withoutAppends) {
            return [];
        }
        return parent::getArrayableAppends();
    }

    public function getNicknameAttribute($value)
    {
        $user = auth()->user();
        if (preg_match('/^1(3[0-9]|4[0-9]|5[012356789]|8[0-9]|7[0678])\d{8}$/', $value)) {
            if ($user && $user->id == $this->attributes['id']) {
                return $value;
            }
            // 隐藏手机号
            $head = substr($value, 0, 3);
            $rear = substr($value, -4);
            return $head.'****'.$rear;
        }

        if (preg_match('/(.*)amii(.*)/i', $value) || preg_match('/(.*)西伍(.*)/i', $value)) {
            if (isset($this->attributes['phone']) && $this->attributes['phone']) {
                $head = substr($this->attributes['phone'], 0, 3);
                $rear = substr($this->attributes['phone'], -4);
                return $head.'****'.$rear;
            } else {
                return '无';
            }
        }
        return $value;
    }

    /**
     * is weixin
     */
    public function getIsWeixinAttribute()
    {
        return app('socialite')->isWeixin($this->attributes['id']);
    }

    /**
     * 是否微信小程序登录过
     */
    public function getIsWeappAttribute()
    {
        return app('socialite')->isWeapp($this->attributes['id']);
    }

    /**
     * is qq
     */
    public function getIsQqAttribute()
    {
        return app('socialite')->isQq($this->attributes['id']);
    }

    /**
     * is weibo
     */
    public function getIsWeiboAttribute()
    {
        return app('socialite')->isWeibo($this->attributes['id']);
    }

    /**
     * is alipay
     */
    public function getIsAlipayAttribute()
    {
        if (!$this->withdrawChannels->isEmpty()) {
            foreach ($this->withdrawChannels as $withdrawChannel) {
                if ($withdrawChannel->type == 'alipay') {
                    return 1;
                }
            }
        }
        return 0;
    }


    public function getAgeAttribute()
    {
        if (!$this->attributes['birthday']) {
            return null;
        }
        return Carbon::now()->year - Carbon::parse($this->attributes['birthday'])->year;
    }

    public function getAvatarAttribute()
    {
        if (!$this->attributes['avatar']) {
            return null;
        }
        return ImageHelper::getImageUrl($this->attributes['avatar'], 'avatar');
    }

    public function setPaymentPasswordAttribute($value)
    {
        $this->attributes['payment_password'] = bcrypt($value);
    }

    public function getHasPaymentPasswordAttribute()
    {
        return !!$this->payment_password;
    }

    public function checkPaymentPassword($password)
    {
        return \Hash::check($password, $this->payment_password);
    }

    //订单数
    public function getOrdresAttribute()
    {
        return app('order')->userOrders($this->attributes['id']);
    }

    //消费总额
    public function getPurchasePriceAttribute($value)
    {
        if (self::$calcPurchasePrice) {
            return app('order')->userConsumer($this->attributes['id'])?:'0.00';
        } else {
            return "";
        }
        
    }

    /**
     * check password
     * @var $password
     * @return bool
     */
    public function checkPassword($password)
    {
        return \Hash::check($password, $this->attributes['password']);
    }

    public function enables()
    {
        return $this->hasMany('App\Models\UserEnable', 'user_id', 'id');
    }

    // 加入天数
    public function getJoinDayAttribute()
    {
        return Carbon::now()->diffInDays(Carbon::parse($this->attributes['created_at'])) + 1;
    }


    public function address()
    {
        return $this->hasMany('App\Models\UserAddress', 'id', 'user_id');
    }

    public function token()
    {
        return $this->hasMany('App\Models\Token', 'ref_id', 'id');
    }

    /**
     * 用户三方登录
     */
    public function socialites()
    {
        return $this->hasMany('App\Models\Socialite', 'user_id', 'id');
    }

    public function level()
    {
        return $this->hasOne('App\Models\PointLevel', 'id', 'level');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|WithdrawChannel
     */
    public function withdrawChannels()
    {
        return $this->hasMany(WithdrawChannel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough|WithdrawRecord
     */
    public function withdrawRecords()
    {
        return $this->hasManyThrough(WithdrawRecord::class, WithdrawChannel::class, 'user_id', 'channel_id');
    }

    public function balances()
    {
        return $this->hasMany(Balance::class);
    }
}
