<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class Version extends Model
{
    protected $table = 'versions';

    protected $fillable = [

        'name',
        'description',
        'is_newest',
        'version_number',
        'version_url',
        'channel',
        'version_type',
        'version_code',
        'is_uploadApk',

    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
    ];

    public function getVersionUrlAttribute()
    {
        return ImageHelper::getPackageUrl($this->attributes['version_url']);
    }


}
