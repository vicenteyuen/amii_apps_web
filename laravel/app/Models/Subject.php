<?php

namespace App\Models;

use App\Helpers\ImageHelper;
use App\Helpers\SummernoteHelper;

class Subject extends Model
{
    protected $table = 'subjects';

    protected $fillable = [
        'title',
        'subtitle',
        'scan',
        'content',
        'product_sell_id',
        'image',
        'weight',
        'marks',

    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'created_at',
        'updated_at',

    ];

    protected $appends = [
        'url',
        'share_url',
    ];

    public function getUrlAttribute()
    {
        return action('Apih5\SubjectController@show', ['id' => $this->attributes['id']]);
    }

    public function getShareUrlAttribute()
    {
         return url('/wechat/community/topic/'.$this->attributes['id']);
    }

    public function getImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'subject');
    }


    public function getSmallImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'subject', 'small');
    }
    public function getMediumImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'subject', 'medium');
    }
    public function getLargeImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'subject', 'large');
    }


    public function getContentAttribute()
    {
        return SummernoteHelper::getImgUrl($this->attributes['content'], 'subject');
    }

    // subject products
    public function products()
    {
        return $this->hasMany('\App\Models\SubjectProduct', 'subject_id', 'id');
    }
}
