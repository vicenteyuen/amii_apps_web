<?php

namespace App\Models;

class SubjectProduct extends Model
{
    protected $table = 'subject_products';

    protected $fillable = [
        'id',
        'subject_id',
        'product_id',
    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'created_at',
        'updated_at',
        
    ];
}
