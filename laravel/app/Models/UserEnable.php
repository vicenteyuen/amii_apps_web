<?php

namespace App\Models;

class UserEnable extends Model
{
    protected $table = 'user_enables';

    protected $fillable = [
        'user_id',
        'enable_id',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
