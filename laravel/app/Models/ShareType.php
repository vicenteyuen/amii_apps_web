<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShareType extends Model
{
    protected $table = 'share_types';

    protected $fillable = [
        'provider_id',
        'desc',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

}
