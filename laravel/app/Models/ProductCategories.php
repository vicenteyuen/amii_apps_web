<?php

namespace App\Models;

class ProductCategories extends Model
{
    protected $table = 'product_categories';

    protected $fillable = [
        'product_id',
        'category_id',
        'brand_id',
    ];
    public function product()
    {
         return $this->hasOne('Ecommerce\Models\Product', 'id', 'product_id');
    }
    public function brand()
    {
        return $this->belongsTo('EcommerceManage\Models\Brand', 'brand_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo('EcommerceManage\Models\Category', 'category_id', 'id');
    }

    // product category
    public function productCategory()
    {
        return $this->belongsTo('App\Models\BrandCategory', 'category_id', 'category_id');
    }

    // product brand
    public function productBrand()
    {
        return $this->belongsTo('App\Models\BrandCategory', 'brand_id', 'brand_id');
    }
}
