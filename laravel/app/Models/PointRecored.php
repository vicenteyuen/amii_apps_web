<?php

namespace App\Models;

class PointRecored extends Model
{
    protected $table = 'point_recoreds';

    protected $fillable = [
        'user_id',
        'type',
        'points',
        'provider',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
    protected $appends = [
        'date',
        'type_str',
    ];

    public function getTypeStrAttribute()
    {
        switch ($this->attributes['type']) {
            case 0:
                return '积分订单';
                break;
            case 1:
                return '消费';
                break;
            case 2:
                return '签到';
                break;
            case 3:
                # code...
                break;
            
            default:
                return '暂无';
                break;
        }
    }
    
    public function getDateAttribute()
    {
        return date('Y-m-d', strtotime($this->attributes['created_at']));
    }
}
