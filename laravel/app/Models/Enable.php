<?php

namespace App\Models;

class Enable extends Model
{
    protected $table = 'enables';

    protected $fillable = [
        'name',
        'image',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
    public function levelEnable()
    {
        return $this->hasOne('App\Models\LevelEnable', 'enable_id', 'id');
    }
}
