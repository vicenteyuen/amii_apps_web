<?php

namespace App\Models;

use App\Helpers\AlipayAopHelper;
use App\Helpers\EasyWechatHelper;

/**
 * @property Order $order
 */
class OrderProductRefund extends Model
{
    protected $table = 'order_refunds';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider',
        'pay',
        'express_fee',
        'applied_data',
        'status',
        'refund_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'applied_data' => 'array',
        'express_fee' => 'float',
        'pay' => 'float',
    ];


    public function product()
    {
        return $this->belongsTo(OrderProduct::class, 'order_product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|OrderProductRefundLog
     */
    public function logs()
    {
        return $this->hasMany(OrderProductRefundLog::class, 'order_refund_id');
    }

    public function getOrderAttribute()
    {
        return $this->product->order;
    }

    /**
     * 发起退款操作
     */
    public function launchRefund()
    {
        $order = $this->order;
        if (!$order) {
            return;
        }

        if ($this->express_fee) {
            \App\Services\BalanceService::rebackExpress($order->user_id, $this->express_fee, 5);
            $this->logs()->create([
                'title' => '退垫付运费成功',
                'content' => '退垫付运费：￥' . $this->express_fee . "\n" . '退回用户余额',
            ]);
        }

        if ($this->status == 1 && $this->refund_status ==3) {
            return;
        }

        // 未发货订单全部退完需要退运费
        $shouldRefund = $this->pay;
        $isRefundShippingFee = true;
        $orderProducts = $order->orderProducts;
        $orderProductNum = $orderProducts->count();
        $calcNum = 0;
        if ($order->provider == 'standard' && $order->pay_status == 1 && $order->shipping_status == 0) {
            foreach ($orderProducts as $orderProduct) {
                if ($orderProduct->refund) {
                    $calcNum++;
                    if ($orderProduct->refund->id != $this->id && !($orderProduct->refund->status == 1 && $orderProduct->refund->refund_status == 3)) {
                        $isRefundShippingFee = false;
                    }
                }
            }
        } else {
            $isRefundShippingFee = false;
        }
        if ($isRefundShippingFee && $orderProductNum == $calcNum) {
            $shouldRefund = $this->pay + $order->shipping_fee;
        }

        if (! $shouldRefund) {
            return;
        }
      
        if ($order->provider == 'point') {
            if ($this->pay) {
                $this->logs()->create([
                    'title' => '退积分给买家 ' . $this->pay . '积分',
                ]);
                app('pointReceived')->userAddPoint('reback', 'points', $this->pay, $order->user_id);
                $this->logs()->create([
                    'title' => '退积分成功',
                    'content' => '退积分数量：' . $this->pay . "积分\n" . '退回用户积分账户',
                ]);
            }
            $this->toComplete();
            return;
        }
        // 普通订单退积分退余额
        if ($order->provider == 'standard') {
            $sumPoints = $order->point_discount * 100;
            $refundedPoints = $order->refunded_point * 100;
            $remainRefundPoints = $sumPoints - $refundedPoints;
            $sumBalance = $order->balance_discount;
            $refundedBlance = $order->refunded_balance;
            $remainRefundBalance = number_format($sumBalance - $refundedBlance, 2, '.', '');

            // 优先退积分
            $shouldRefundPoint = $shouldRefund * 100;
            if ($shouldRefundPoint <= $remainRefundPoints) {
                // 积分可足额退回
                // 记录已退积分
                $order->update([
                    'refunded_point' => number_format($order->refunded_point + $shouldRefund, 2, '.', '')
                ]);
                // 退回积分
                app('pointReceived')->userAddPoint('reback', 'points', $shouldRefundPoint, $order->user_id);
                $this->logs()->create([
                    'title' => '退积分给买家 ' . $shouldRefundPoint . '积分',
                ]);
                $this->toComplete();
                return;
            } else {
                if ($remainRefundPoints) {
                    // 记录已退积分
                    $order->update([
                        'refunded_point' => $order->point_discount
                    ]);
                    // 退回积分
                    app('pointReceived')->userAddPoint('reback', 'points', $remainRefundPoints, $order->user_id);
                    // 退回积分后剩余需要退回额度
                    $shouldRefund = number_format($shouldRefund - $remainRefundPoints / 100, 2, '.', '');
                    $this->logs()->create([
                        'title' => '退积分给买家 ' . $remainRefundPoints . '积分',
                    ]);
                }
            }

            // 其次退回余额
            $shouldRefundBalance = $shouldRefund;
            if ($shouldRefundBalance <= $remainRefundBalance) {
                $order->user->cumulate_profit = $order->user->cumulate_profit + $shouldRefundBalance;
                // 退回余额记录
                app('balance')->storeBalance([
                    'value'     => $shouldRefundBalance,
                    'type'      => 6,
                    'status'    => 1,
                    'user_id'   => $order->user_id,
                ]);
                // 记录已退余额
                $order->update([
                    'refunded_balance' => number_format($order->refunded_balance + $shouldRefundBalance, 2, '.', '')
                ]);
                $order->user->save();
                $this->logs()->create([
                    'title' => '退余额给买家 ' . $shouldRefundBalance . '余额',
                ]);
                $this->toComplete();
                return;
            } else {
                if ($remainRefundBalance != '0.00') {
                    $order->user->cumulate_profit = number_format($order->user->cumulate_profit + $remainRefundBalance, 2, '.', '');
                    // 退回余额记录
                    app('balance')->storeBalance([
                        'value'     => $remainRefundBalance,
                        'type'      => 6,
                        'status'    => 1,
                        'user_id'   => $order->user_id,
                    ]);
                    // 记录已退余额
                    $order->update([
                        'refunded_balance' => number_format($order->balance_discount, 2, '.', '')
                    ]);
                    $order->user->save();
                    // 退回余额后剩余需要退回额度
                    $shouldRefund = number_format($shouldRefund - $remainRefundBalance, 2, '.', '');
                    $this->logs()->create([
                        'title' => '退余额给买家 ' . $remainRefundBalance . '余额',
                    ]);
                }
            }
        }
        $payment = $order->payments()->where('status', 1)->first();
        if (!$payment) {
            return;
        }

        $wechat_app = null;
        $alipay_app = null;
        switch ($payment->provider) {
            case 'weweb':
            case 'wechat':
                $wechat_app = EasyWechatHelper::easywechatWechat();
                break;
            case 'weapp':
                $wechat_app = EasyWechatHelper::easywechatWeapp();
                break;
            case 'wemp':
                $wechat_app = EasyWechatHelper::easywechatWemp();
                break;
            case 'alipay':
            case 'alipayweb':
                $alipay_app = AlipayAopHelper::getInstance($payment->provider);
                break;
        }

        if (!$this->out_refund_no) {
            $this->out_refund_no = $this->id . $this->created_at->format('YmdHis');
            $this->save();
            $this->logs()->create([
                'title' => '退款给买家 ¥' . $shouldRefund,
            ]);
        }

        if ($wechat_app) {
            $order_fee = $order->order_fee;
            if (env('APP_DEBUG')) {
                $order_fee = 0.01;
            }
            $result = \App\Helpers\EasyWechatHelper::refund(
                $wechat_app,
                $order->order_sn,
                $this->out_refund_no,
                $order_fee,
                $shouldRefund
            );
            //退款失败
            if ($result->result_code == 'FAIL' && in_array($result->err_code, ['ERROR', 'USER_ACCOUNT_ABNORMAL'])) {
            }
            //退款成功
            if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS') {
                $this->logs()->create([
                    'title' => '退款成功',
                    'content' => '退款金额：￥' . $shouldRefund . "\n" . '退回微信',
                ]);
                $this->toComplete();
                return;
            }
            \Log::info(self::class . ':WechatLibrary.refund-微信退款' . $shouldRefund . '失败', $result->toArray());
            throw new \Exception('微信退款失败：' . $result->err_code_des);
        }
        if ($alipay_app) {
            $result = $alipay_app->tradeRefund($order->order_sn, $shouldRefund, $this->out_refund_no);
            if ($result->code != 10000) {
                throw new \Exception('支付宝退款' . $shouldRefund . '失败：' . $result->sub_code);
            }
            $this->logs()->create([
                'title' => '退款成功',
                'content' => '退款金额：￥' . $shouldRefund . "\n" . '退回支付宝',
            ]);
            $this->toComplete();
            return;
        }
        throw new \Exception('退款失败');
    }

    private function toComplete()
    {
        $this->refund_status = 3;
        $this->status = 1;
        $this->save();
        $this->order->checkRefundChangeStatus();
        // 重新计算用户等级
        app('pointLevel')->points($this->order->user->sum_points, $this->order->user_id);
        return $this;
    }
}
