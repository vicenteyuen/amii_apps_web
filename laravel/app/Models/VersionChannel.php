<?php

namespace App\Models;

class VersionChannel extends Model
{
    protected $table = 'version_channels';

    protected $fillable = [
        'channel_id',
        'desc',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

}
