<?php

namespace App\Models;

class FirstPurchase extends Model
{
    protected $table = 'first_purchases';

    protected $fillable = [
        'sell_product_id',
        'status',
        'money',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function sellProduct()
    {
        return $this->belongsTo('App\Models\SellProduct', 'sell_product_id', 'id');
    }
}
