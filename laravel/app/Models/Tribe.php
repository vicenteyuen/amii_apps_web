<?php

namespace App\Models;

class Tribe extends Model
{
    protected $table = 'tribes';
    protected $fillable = [
        'pid',
        'user_id',
        'income',
        'week_rank',
        'month_rank',
        'last_week_rank',
        'last_month_rank',
        'week_income',
        'month_rank',
        'user_last_rank',
        'user_current_rank',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];

    protected $appends =[
        'week_change',
        'month_change',
        'user_contribute_change',
    ];


    protected function getRankAttribute()
    {
        if ($this->attributes['rank'] == 0) {
            return "";
        }
        return $this->attributes['rank'];
    }

    protected function getWeekRankAttribute()
    {
        if ($this->attributes['week_rank'] == 0) {
            return "";
        }
        return $this->attributes['week_rank'];
    }

    protected function getMonthRankAttribute()
    {
        if ($this->attributes['month_rank'] == 0) {
            return "";
        }
        return $this->attributes['month_rank'];

    }

    public function tribe()
    {
        return $this->hasMany('App\Models\Tribe', 'id', 'parent_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function getWeekChangeAttribute()
    {
        if ($this->attributes['last_week_rank'] == 0) {
            return  0;
        }
        if ($this->attributes['week_rank'] == 0) {
            return - $this->attributes['last_week_rank'];
        }
        return $this->attributes['last_week_rank'] - $this->attributes['week_rank'] ;
    }
    public function getMonthChangeAttribute()
    {
        if ($this->attributes['last_month_rank'] == 0) {
            return  0;
        }
        if ($this->attributes['month_rank'] == 0) {
            return - $this->attributes['last_month_rank'];
        }
        return $this->attributes['last_month_rank'] - $this->attributes['month_rank'] ;
    }

    public function getUserContributeChangeAttribute()
    {
        if ($this->attributes['user_last_rank'] == 0) {
            return  0;
        }
        if ($this->attributes['user_current_rank'] == 0) {
            return - $this->attributes['user_last_rank'];
        }
        return $this->attributes['user_last_rank'] - $this->attributes['user_current_rank'] ;
    }
}
