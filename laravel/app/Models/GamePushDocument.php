<?php

namespace App\Models;

class GamePushDocument extends Model
{
    protected $table = 'game_push_documents';

    protected $fillable = [
        'hours',
        'desc',
        'provider',
        'redundant',
        'status',
 
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
