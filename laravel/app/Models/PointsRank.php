<?php

namespace App\Models;

class PointsRank extends Model
{
    protected $table = 'points_rank';
    protected $fillable = [
        'pid',
        'user_id',
        'year_rank',
        'month_rank',
        'last_year_rank',
        'last_month_rank',
        'year_income',
        'month_income',
 
    ];
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];

    protected $appends =[
        'month_change',
        'year_change',
    ];

    protected function getMonthRankAttribute()
    {
        if ($this->attributes['month_rank'] == 0) {
            return "";
        }
        return $this->attributes['month_rank'];
    }

    protected function getYearRankAttribute()
    {
        if ($this->attributes['year_rank'] == 0) {
            return "";
        }
        return $this->attributes['year_rank'];
    }

    public function getYearChangeAttribute()
    {
        if ($this->attributes['last_year_rank'] == 0) {
            return  0;
        }
        if ($this->attributes['year_rank'] == 0) {
            return - $this->attributes['last_year_rank'];
        }
        return $this->attributes['last_year_rank'] - $this->attributes['year_rank'] ;
    }
    public function getMonthChangeAttribute()
    {
        if ($this->attributes['last_month_rank'] == 0) {
            return  0;
        }
        if ($this->attributes['month_rank'] == 0) {
            return - $this->attributes['last_month_rank'];
        }
        return $this->attributes['last_month_rank'] - $this->attributes['month_rank'] ;
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
