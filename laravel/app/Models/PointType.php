<?php

namespace App\Models;

class PointType extends Model
{
    protected $table = 'point_types';

    protected $fillable = [
        'name',
        'description',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
