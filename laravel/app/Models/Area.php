<?php

namespace App\Models;

class Area extends Model
{
    protected $table = 'areas';

    protected $fillable = [
        'name',
        'area_id',
        'city_id',
        'lat',
        'lng',
 
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
