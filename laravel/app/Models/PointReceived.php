<?php

namespace App\Models;

class PointReceived extends Model
{
    protected $table = 'point_received';

    protected $fillable = [
        'user_id',
        'points',
        'type',
        'type_child',
        'resource',
        'status',
    ];

    protected $appends = [
        'type_str',
        'date',
        'createdtime',
    ];

    public function getCreatedtimeAttribute()
    {
        return strtotime($this->attributes['created_at']);
    }

    public function getdateAttribute()
    {
        return date("Y-m-d", strtotime($this->attributes['created_at']));
    }

    public function getPointsAttribute()
    {
        if ($this->attributes['type'] == 'consumer' && $this->attributes['type_child'] == 'consumer') {
            return  "-".$this->attributes['points'];
        }
        return "+".$this->attributes['points'];
    }

    public function getTypeStrAttribute()
    {
        switch ($this->attributes['type']) {
            //注册
            case 'register':
                if ($this->attributes['type_child'] == 'register') {
                    return '注册积分';
                }
                return '';

            //个人资料更新
            case 'update':
                if (in_array($this->attributes['type_child'], ['phone','birthday','address','gender'], true)) {
                    return  '个人信息积分';
                } else if ($this->attributes['type_child'] == 'wechat') {
                    return '绑定微信积分';
                } else if ($this->attributes['type_child'] == 'qq') {
                    return '绑定QQ积分';
                } else if ($this->attributes['type_child'] == 'weibo') {
                    return '绑定微博积分';
                } else {
                    return '';
                }

            //形象管理
            case 'looking':
                switch ($this->attributes['type_child']) {
                    case 'height':
                        return '完善身高数据积分';
                    case 'weight':
                        return '完善体重数据积分';
                    case 'bust':
                        return '完善胸围数据积分';
                    case 'waist':
                        return '完善腰围数据积分';
                    case 'hips':
                        return '完善臀围数据积分';
                    case 'shoulder_width':
                        return '完善肩宽数据积分';
                    case 'thigh_width':
                        return '完善大腿围数据积分';
                    case 'leg_width':
                        return '完善小腿围数据积分';
                    case 'skin':
                        return '完善肤色数据积分';
                    case 'hair':
                        return '完善发色数据积分';
                    case 'up_size':
                        return '回答AMII上装问题积分';
                    case 'lower_size':
                        return '回答AMII下装问题积分';
                    case 'front_photo':
                        return '上传正面照积分';
                    case 'side_photo':
                        return '上传侧面照积分';
                        break;
                    default:
                        return '';
                }
            //邀请
            case 'invite':
                if ($this->attributes['type_child'] == 'invite') {
                    return '邀请积分';
                }
                return '';

            //购物
            case 'shopping':
                if ($this->attributes['type_child'] == 'shopping') {
                    return '购物积分';
                }
                return '';

            //天数达标
            case 'received_day':
                if ($this->attributes['type_child'] == 'received_day') {
                    return '签到积分';
                }
                return '';

            //评论
            case 'appraising':
                if ($this->attributes['type_child'] == 'normal') {
                    return '普通评论积分';
                } else if ($this->attributes['type_child'] == 'image') {
                    return '图片评论积分';
                } else {
                    return ;
                }

            //图文专区参与
            case 'image_appraising':
                if ($this->attributes['type_child'] == 'mark') {
                    return '点赞积分';
                } else if ($this->attributes['type_child'] == 'comment') {
                    return '评论积分';
                } else {
                    return '';
                }

            //分享
            case 'share':
                if ($this->attributes['type_child'] == 'product') {
                    return '商品分享';
                } else if ($this->attributes['type_child'] == 'puzzle') {
                    return '游戏分享';
                } else if ($this->attributes['type_child'] == 'activity') {
                     return '活动分享';
                } else if ($this->attributes['type_child'] == 'person_comment') {
                    return '买家秀';
                } else if ($this->attributes['type_child'] == 'subject') {
                    return '专题分享';
                } else if ($this->attributes['type_child'] == 'video') {
                    return '视频分享';
                } else if ($this->attributes['type_child'] == 'activity') {
                    return '首单七折购分享';
                } else if ($this->attributes['type_child'] == 'tribe') {
                    return '分佣规则分享';
                } else if ($this->attributes['type_child'] == 'puzzle_index') {
                    return '游戏首页分享';
                } else if ($this->attributes['type_child'] == 'subject_mark') {
                    return '专题点赞';
                } else if ($this->attributes['type_child'] == 'home') {
                    return '首页分享';
                } else {
                    return '';
                }


            //活动 二期
            case 'activity':
                if ($this->attributes['type_child'] == 'lottery') {
                    return '';
                } else if (in_array($this->attributes['type_child'], ['show','collocation'], true)) {
                    return '';
                } else if ($this->attributes['type_child'] == 'game') {
                    return $this->handPoint($select, $points);
                } else {
                    return '';
                }

           //用户反馈
            case 'advice':
                if ($this->attributes['type_child'] == 'advice') {
                    return '用户反馈采纳积分';
                } else if ($this->attributes['type_child'] == 'game') {
                     return '游戏反馈采纳积分';
                } else if ($this->attributes['type_child'] == 'useradvice') {
                    return '用户反馈积分';
                } else if ($this->attributes['type_child'] == 'gameadvice') {
                    return '游戏反馈积分';
                }
                return '';

            //消费花去积分
            case 'consumer':
                if ($this->attributes['type_child'] == 'consumer') {
                    return '消费积分';
                }
                return '';
            //退回积分
            case 'reback':
                if ($this->attributes['type_child'] == 'points') {
                    return '取消订单或售后退回积分';
                }
                return '';

            default:
                return '';
        }
    }


    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'resource', 'order_sn');
    }
}
