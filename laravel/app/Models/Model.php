<?php

namespace App\Models;

use XsBaseDatabase\BaseModel;

/**
 * Class Model
 * @package App\Models
 * @mixin \Eloquent|\Illuminate\Database\Eloquent\Model
 */
abstract class Model extends BaseModel
{
    //
}
