<?php

namespace App\Models;

class PointLevel extends Model
{
    protected $table = 'point_levels';

    protected $fillable = [
        'name',
        'points',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    protected $appends = [
        'validate_phone',
        'require_money',
        'require_purchase_time',
        'validate_user',
    ];

    public function levelEnable()
    {
        return $this->hasMany('App\Models\LevelEnable', 'level_id', 'id');
    }

    public function getValidatePhoneAttribute()
    {
        switch ($this->attributes['id']) {
            case '1':
                return 0;
            case '2':
                return 1;
            case '3':
                return 1;
            case '4':
                return 1;
            case '5':
                return 1;
            case '6':
                return 1;
            case '7':
                return 1;
            case '8':
                return 1;
            case '9':
                return 1;
            case '10':
                return 1;
            case '11':
                return 1;

            default:
                # code...
                break;
        }
    }

    public function getRequireMoneyAttribute()
    {
        switch ($this->attributes['id']) {
            case '1':
                return null;
            case '2':
                return null;
            case '3':
                return null;
            case '4':
                return 100;
            case '5':
                return 300;
            case '6':
                return 500;
            case '7':
                return 1000;
            case '8':
                return 2000;
            case '9':
                return 5000;
            case '10':
                return 10000;
            case '11':
                return 20000;

            default:
                # code...
                break;
        }
    }

    public function getRequirePurchaseTimeAttribute()
    {
        switch ($this->attributes['id']) {
            case '1':
                return null;
            case '2':
                return null;
            case '3':
                return null;
            case '4':
                return 1;
            case '5':
                return 3;
            case '6':
                return 5;
            case '7':
                return 10;
            case '8':
                return 15;
            case '9':
                return 30;
            case '10':
                return 50;
            case '11':
                return 100;
            default:
                # code...
                break;
        }
    }

    public function getValidateUserAttribute()
    {
        switch ($this->attributes['id']) {
            case '1':
                return 0;
            case '2':
                return 1;
            case '3':
                return 1;
            case '4':
                return 1;
            case '5':
                return 1;
            case '6':
                return 1;
            case '7':
                return 1;
            case '8':
                return 1;
            case '9':
                return 1;
            case '9':
                return 1;
            case '10':
                return 1;
            case '11':
                return 1;
            
            default:
                # code...
                break;
        }
    }
}
