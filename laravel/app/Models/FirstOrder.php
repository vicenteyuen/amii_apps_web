<?php

namespace App\Models;

class FirstOrder extends Model
{
    protected $table = 'first_order_records';

    protected $fillable = [
        'user_id',
        'value',
        'order_id',
    ];
}
