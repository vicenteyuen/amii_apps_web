<?php

namespace App\Models;

use App\Models\Model;

use Carbon\Carbon;
use App\Helpers\ImageHelper;

class Coupon extends Model
{
    protected $table = 'coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cid',
        'status',
        'title',
        'image',
        'number',
        'remain',
        'provider',
        'value',
        'lowest',
        'start',
        'end',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'orderby',
        'status',
        'start_millisecond',
        'end_millisecond',
        'deleted_at_millisecond',
        'deleted_at',
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    /**
     * 添加attribute
     */
    protected $appends = [
        'coupon_status',
    ];

    /**
     * 获取现金券
     */
    public function scopeCash($query)
    {
        return $query->where('provider', 1);
    }

    /**
     * 获取满减券
     */
    public function scopeFullcut($query)
    {
        return $query->where('provider', 2);
    }

    /**
     * 获取折扣券
     */
    public function scopeRebate($query)
    {
        return $query->where('provider', 3);
    }

    /**
     * 获取优惠券状态
     */
    public function getCouponStatusAttribute()
    {
        $str = '';
        if ($this->attributes['end'] <= Carbon::now()) {
            // 优惠券过期
            $str = 'overdata';
        } elseif ($this->attributes['start'] >= Carbon::now()) {
            $str = 'unbegin';
        } else {
            if ($this->attributes['status']) {
                // 优惠券可用
                $str = 'useful';
            } else {
                // 优惠券不可用
                $str = 'unavailable';
            }
        }
        return $str;
    }

    /**
     * get image attribute
     */
    public function getImageAttribute($value)
    {
        return ImageHelper::getImageUrl($value, 'coupon');
    }

    /**
     * get Start attribute
     */
    public function getStartAttribute($value)
    {
        if ($value) {
            return date('Y-m-d', strtotime($value));
        }
        return $value;
    }

    /**
     * get End attribute
     */
    public function getEndAttribute($value)
    {
        if ($value) {
            return date('Y-m-d', strtotime($value));
        }
        return $value;
    }

    /**
     * get Value attribute
     */
    public function getValueAttribute($value)
    {
        return intval($value);
    }

    /**
     * get Value attribute
     */
    public function getLowestAttribute($value)
    {
        return intval($value);
    }



    /**
     * get provider attribute
     */
    public function getProviderAttribute($value)
    {
        switch ($value) {
            case '1':
                // 现金券
                $provider = 'cash';
                break;
            case '2':
                // 满减券
                $provider = 'fullcut';
                break;
            case '3':
                // 折扣券
                $provider = 'rebate';
                break;
            default:
                $provider = 'undefine';
                break;
        }
        return $provider;
    }

    /**
     * 获取时间可用优惠券
     */
    public function scopeTimeUseful($query)
    {
        return $query->where('start', '<=', Carbon::now())
            ->where('end', '>', Carbon::now());
    }

    /**
     * scope for unend coupon
     */
    public function scopeUnend($query)
    {
        return $query->where('end', '>', Carbon::now());
    }

    /**
     * 优惠券处于正常状态
     */
    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }

    /**
     * 优惠券发行
     */
    public function couponGrant()
    {
        return $this->hasMany('App\Models\CouponGrant', 'coupon_id', 'id');
    }
}
