<?php

namespace App\Models;

use App\Helpers\ImageHelper;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $table = 'shares';

    protected $fillable = [
        'title',
        'image',
        'provider',
        'content',
        'is_default',
        'is_defaultTitle',
        'is_defaultContent',
        'is_defaultImg',
        'is_shareAll',
    ];

    protected $hidden = [
        'provider_value',
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    /**
    *   get provider attibute
    */
    protected function getProviderAttribute($value)
    {
        switch ($value) {
            case '1':
                $provider = 'product';
                break;
            case '2':
                $provider = 'subject';
                break;
            case '3':
                $provider = 'video';
                break;
            case '4':
                $provider = 'code';
                break;
            case '5':
                $provider = 'tribe';
                break;
            case '6':
                $provider = 'activity';
                break;
            case '7':
                $provider = 'puzzle';
                break;
            case '8':
                $provider = 'puzzle_index';
                break;
            case '9';
                $provider = 'red_packet';
                break;
            case '10';
                $provider = 'home';
                break;
            default:
                $provider = 'undefine';
                break;
        }
        return $provider;
    }

    /**
    * get provider_value attribute
    */
    protected function getProviderValueAttribute()
    {
        switch ($this->provider) {
            case 'product':
                $value = '1';
                break;
            case 'subject':
                $value = '2';
                break;
            case 'video':
                $value = '3';
                break;
            case 'code':
                $value = '4';
                break;
            case 'tribe':
                $value = '5';
                break;
            case 'activity':
                $value = '6';
                break;
            case 'puzzle':
                $value = '7';
                break;
            case 'puzzle_index':
                $value = '8';
                break;
            case 'red_packet':
                $value = '9';
                break;
            case 'home':
                $value = '10';
                break;
            default:
                $value = false;
                break;
        }
        return $value;
    }

    /**
     * get image attribute
     */
    public function getImageAttribute($value)
    {
        return ImageHelper::getImageUrl($value, 'share');
    }

    // shareRelation
    public function shareRelation()
    {
        return $this->hasMany('App\Models\ShareRelation', 'share_id', 'id');
    }
}
