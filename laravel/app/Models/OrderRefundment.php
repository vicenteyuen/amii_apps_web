<?php

namespace App\Models;

class OrderRefundment extends Model
{
    protected $table = 'order_refundments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
