<?php

namespace App\Models;

class City extends Model
{
    protected $table = 'citys';

    protected $fillable = [
        'name',
        'fullname',
        'city_id',
        'province_id',
        'lat',
        'lng',
 
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
    
    public function Area()
    {
        return $this->hasMany('App\Models\Area', 'city_id', 'city_id');
    }
}
