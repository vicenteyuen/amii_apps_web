<?php

namespace App\Models;

class PointsReward extends Model
{
    protected $table = 'points_reward';

    protected $fillable = [
        'id',
        'user_id',
        'month_income',
        'month_rank',
        'year',
        'month',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
