<?php

namespace App\Models;

class PointRankAward extends Model
{
    protected $table = 'point_rank_awards';

    protected $fillable = [
        'name',
        'status',
        'is_publish',
        'number',
        'provider',
        'start_time',
        'end_time',
    ];

    protected $appends = [
        'url'
    ];

    /**
     * 已发布不显示
     */
    public function scopePublishedUndisplay($query)
    {
        return $query->where('is_publish', 2);
    }

    /**
     * 已发布并显示的
     */
    public function scopePublishedDisplay($query)
    {
        return $query->where('is_publish', 1);
    }

    public function getUrlAttribute()
    {
        return url('/wechat/points/award?resource=' . $this->attributes['id']);
    }

    /**
     * 获奖人员
     */
    public function users()
    {
        return $this->hasMany(PointRankAwardUser::class, 'point_rank_award_id', 'id');
    }
}
