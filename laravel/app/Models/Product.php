<?php

namespace App\Models;

use Ecommerce\Models\Product as BaseProduct;

class Product extends BaseProduct
{
    protected $hidden = [];
}
