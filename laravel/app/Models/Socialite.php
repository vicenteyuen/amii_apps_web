<?php

namespace App\Models;

class Socialite extends Model
{
    protected $table = 'socialites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'provider',
        'openid',
        'oauth_token',
        'oauth_token_secret',
        'unionid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'oauth_token',
        'oauth_token_secret',
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    /**
     * 用户
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
