<?php

namespace App\Models;

class ProductAttribute extends Model
{
    protected $table = 'product_attributes';

    protected $fillable = [
        'product_id',
        'attribute_id',
    ];

    public function attribute()
    {
         return $this->belongsTo('App\Models\Attribute');
    }
}
