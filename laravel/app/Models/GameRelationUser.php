<?php

namespace App\Models;

class GameRelationUser extends Model
{
    protected $table = 'game_relation_users';

    protected $fillable = [
        'user_games_id',
        'user_id',
    ];
    protected $hidden = [
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d H:i', strtotime($value));
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function game()
    {
         return $this->belongsTo('App\Models\UserGame', 'user_games_id', 'id');
    }
}
