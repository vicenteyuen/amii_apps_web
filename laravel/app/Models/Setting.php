<?php

namespace App\Models;

class Setting extends Model
{
    protected $table = 'settings';
    public $timestamps = false;

    protected $fillable = [
        'key',
        'value',
    ];
}
