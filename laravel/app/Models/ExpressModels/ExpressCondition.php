<?php

namespace App\Models\ExpressModels;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Model;

class ExpressCondition extends Model
{
    use SoftDeletes;
    protected $table = 'express_conditions';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'express_fee_id',
        'express_template_id',
        'region_code',
        'default_fee',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    public function province()
    {
        return $this->hasOne('App\Models\Province', 'province_id', 'region_code');
    }
}
