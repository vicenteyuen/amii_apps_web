<?php

namespace App\Models\ExpressModels;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Model;

class ExpressTemplate extends Model
{
    use SoftDeletes;
    protected $table = 'express_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'free',
        'assign',
        'calc_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
        'updated_at',
        'deleted_at',
    ];

    public function expressFee()
    {
        return $this->hasOne('App\Models\ExpressModels\ExpressFee', 'express_template_id', 'id');
    }

    public function expressCondition()
    {
        return $this->hasMany('App\Models\ExpressModels\ExpressCondition', 'express_template_id', 'id');
    }
}
