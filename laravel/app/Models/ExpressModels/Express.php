<?php

namespace App\Models\ExpressModels;

use App\Models\Model;

class Express extends Model
{
    protected $table = 'express';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'com',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'status',
        'orderby',
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at'
    ];

    /**
     * 可用快递
     */
    public function scopeUseful($query)
    {
        return $query->where('status', 1);
    }
}
