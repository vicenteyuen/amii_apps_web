<?php

namespace App\Models\ExpressModels;

use App\Models\Model;

class ExpressLog extends Model
{
    protected $table = 'express_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_package_id',
        'info',
        'is_poll',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'is_poll',
        'order_package_id',
    ];

    /**
     * 类型转换
     */
    protected $casts = [
        'info' => 'array',
    ];
}
