<?php

namespace App\Models\ExpressModels;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Model;

class ExpressFee extends Model
{
    use SoftDeletes;
    protected $table = 'express_fees';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'express_template_id',
        'default_fee',
        'free_money',
        'free_fee',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];

    public function expressCondition()
    {
        return $this->hasMany('App\Models\ExpressModels\ExpressCondition', 'express_fee_id', 'id');
    }
}
