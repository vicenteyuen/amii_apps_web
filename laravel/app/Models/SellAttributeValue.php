<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class SellAttributeValue extends Model
{
    protected $table = 'sell_attribute_values';
    protected $fillable = [
        'sell_attribute_id',
        'attribute_value_id',
        'attribute_id',
        'sell_product_attribute_value_image',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
        'product_image',
    ];

    // 获取主图
    public function getSellProductAttributeValueImageAttribute()
    {
        if (!$this->attributes['sell_product_attribute_value_image']) {
            return null;
        }
        return ImageHelper::getImageUrl($this->attributes['sell_product_attribute_value_image'], 'product');
    }

    public function attributeValue()
    {
        return $this->hasOne('App\Models\AttributeValue', 'id', 'attribute_value_id');
    }

    public function sellAttributes()
    {
         return $this->hasOne('App\Models\SellAttribute', 'id', 'sell_attribute_id');
    }

    public function attribute()
    {
         return $this->hasOne('App\Models\Attribute', 'id', 'attribute_id');
    }
}
