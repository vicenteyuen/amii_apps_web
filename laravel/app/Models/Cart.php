<?php

namespace App\Models;

class Cart extends Model
{
    protected $table = 'carts';

    protected $fillable = [
        'number',
        'inventory_id',
        'sell_product_id',
        'user_id',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function inventory()
    {
        return $this->hasOne('Ecommerce\Models\Inventory', 'id', 'inventory_id');
    }

    public function sellProduct()
    {
        return $this->hasOne('App\Models\SellProduct', 'id', 'sell_product_id');
    }
}
