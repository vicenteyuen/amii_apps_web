<?php

namespace App\Models;
use App\Helpers\ImageHelper;

class TodayTask extends Model
{
    protected $table = 'today_tasks';

    protected $casts = [
        'task_points' => 'array',
    ];

    protected $fillable = [
        'type',
        'title',
        'content',
        'image',
        'task_points',
        'jump_url',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    public function getImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image']);
    }

    public function getJumpUrlAttribute()
    {
        if ($this->attributes['type'] == 2) {
            $uuid = '';
            if (auth()->user()) {
                $uuid = auth()->user()->uuid;
            }
            return url($this->attributes['jump_url']. $uuid);
        }
        return url($this->attributes['jump_url']);
    }
}
