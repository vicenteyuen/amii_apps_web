<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductService extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at_millisecond',
        'deleted_at',
        'updated_at_millisecond',
        'updated_at',
        'created_at_millisecond',
        'created_at',
    ];
}
