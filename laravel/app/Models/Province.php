<?php

namespace App\Models;

class Province extends Model
{
    protected $table = 'provinces';

    protected $fillable = [
        'name',
        'province_id',
        'lat',
        'lng',
 
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
    
    public function City()
    {
        return $this->hasMany('App\Models\City', 'province_id', 'province_id');
    }
}
