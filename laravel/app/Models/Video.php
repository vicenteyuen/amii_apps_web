<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class Video extends Model
{
    protected $table = 'videos';

    protected $fillable = [
        'image',
        'title',
        'description',
        'video',
        'likes',
        'weight',

    ];

    protected $hidden = [
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',

    ];
    protected $appends = [
        'share_url'
    ];

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function getShareUrlAttribute()
    {
        return url('/wechat/community/video/'.$this->attributes['id']);
    }

    public function getImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'video', 'small');
    }

    public function getVideoAttribute()
    {
        return ImageHelper::getVideoUrl($this->attributes['video']);
    }

    public function getOriginalVideoAttribute()
    {
        return $this->attributes['video'];
    }
}
