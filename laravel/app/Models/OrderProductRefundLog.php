<?php

namespace App\Models;

class OrderProductRefundLog extends Model
{
    protected $table = 'order_refund_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'is_user_record',
        'is_express',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function images()
    {
        return $this->hasMany(OrderProductRefundImage::class, 'order_refund_log_id');
    }
}
