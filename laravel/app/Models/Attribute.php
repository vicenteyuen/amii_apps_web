<?php

namespace App\Models;

class Attribute extends Model
{
    protected $table = 'attributes';

    protected $fillable = [
        'name',
        'has_img',
        'is_multi',
        'weight',
        'status',
        'type',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
        'deleted_at',
        'deleted_at_millisecond',
    ];
    public function attributeValue()
    {
        return $this->hasMany('App\Models\AttributeValue', 'attribute_id', 'id');
    }
}
