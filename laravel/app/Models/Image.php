<?php

namespace App\Models;

use App\Helpers\ImageHelper;

class Image extends Model
{
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider',
        'value',
        'image_file_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'provider',
    ];

    /**
     * The attributes that append
     */
    protected $appends = [
        'url',
    ];

    // get attribute
    public function getUrlAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['value'], $this->attributes['provider'], 'source');
    }

    public function File()
    {
        return $this->belognsTo('App\Models\ImageFile', 'image_file_id', 'id');
    }
}
