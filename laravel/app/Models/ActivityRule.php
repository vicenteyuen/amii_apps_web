<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\ImageHelper;
use App\Helpers\SummernoteHelper;

class ActivityRule extends Model
{
    use SoftDeletes;
    protected $table = 'activity_rules';

    protected $fillable = [
        'image',
        'content',
        'status',
        'weight',
        'is_addLink',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'content',
    ];

    protected $appends = [
        'h5url',
    ];

    /**
     * 修改图片属性
     */
    public function getImageAttribute($value)
    {
        return ImageHelper::getImageUrl($value, 'activityRule');
    }

    public function getMediumImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'activityRule', 'medium');
    }

    public function getLargeImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'activityRule', 'large');
    }

    public function getContentAttribute()
    {
        return SummernoteHelper::getImgUrl($this->attributes['content'], 'activityRule');
    }

    /**
     * 添加获取h5链接
     * @return [type] [description]
     */
    public function getH5urlAttribute()
    {
        // 如果有添加链接，返回链接
        if($this->attributes['is_addLink'] == 1){
            return $this->attributes['content'];
        }
        return action('\App\Http\Controllers\Apih5\ActivityRuleController@show', $this->attributes['id']);
    }
}
