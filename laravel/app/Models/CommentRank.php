<?php

namespace App\Models;

class CommentRank extends Model
{
    protected $table = 'comment_ranks';

    protected $fillable = [
        'rank',
        'desc',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
