<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\ImageHelper;

class CommentImage extends Model
{
    use SoftDeletes;

    protected $table = 'comment_images';

    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'image',
        'comment_id',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at_millisecond',
        'updated_at',
        'deleted_at_millisecond',
        'deleted_at',
    ];
    public function getImageAttribute()
    {
        return ImageHelper::getImageUrl($this->attributes['image'], 'product', 'small');
    }
}
