<?php

namespace App\Models;

class FirstPurchaseText extends Model
{
    protected $table = 'first_purchase_texts';

    protected $fillable  = [
        'text',
    ];

    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
