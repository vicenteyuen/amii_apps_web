<?php

namespace App\Models;

class Activity extends Model
{
    protected $table = 'activities';

    protected $fillable = [
        'title',
        'provider',
        'order_description',
        'isPopup',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
}
