<?php

namespace App\Http\Controllers\Oath2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\SocialiteOath2\SocialiteManager;
use App\Helpers\EasyWechatHelper;

class LoginController extends Controller
{
    protected $socialite;

    protected $provider;

    public function __construct()
    {
        parent::__construct();
        $this->socialite = new SocialiteManager(config('oath2'));
    }


    /**
     * 微信公众号获取配置信息
     */
    public function wempConfig()
    {
        $config = EasyWechatHelper::wempConfig();

        return response()->json([
            'data' => $config,
            'status' => 'success'
        ]);
    }

    public function sign(Request $request)
    {
        $driver = $request->provider;
        // dd(config('oath2'), $driver);

        if (! isset(config('oath2')[$driver])) {
            if ($request->ajax()) {
                return $this->app['jsend']->error('请求三方登录错误');
            }
            dd('三方登录错误');
            return false;
        }

        if ($request->return_url) {
            session()->put('wechat_return_url', base64_decode($request->return_url));
        }

// dd($this->socialite->driver($driver));
        $response = $this->socialite->driver($driver)
            ->redirect();
            // dd($response);
        return $response;
    }

    public function login(Request $request)
    {
        if ($request->platform) {
            $driver = $request->provider . $request->platform;
        } else {
            $driver = $request->provider;
        }

        if (! in_array($driver, array_keys(config('oath2')))) {
            return $this->app['jsend']->error('当前登录方式不许可');
        }

        $user = $this->socialite->driver($driver)->user();
        if (! $user) {
            return $this->app['jsend']->error('当前登录方式失败');
        }

        // store user data
        $checkUser = $this->app['socialite']->findOrCreateByProvider($driver, $user);
        if (! $checkUser['status']) {
            return $this->app['jsend']->error($checkUser['message']);
        }
        auth()->loginUsingId($checkUser['data']->id);
        // 生成token
        $token = $this->app['token']->createAndExpires($checkUser['data']->id);

        $isPhone = $checkUser['data']->phone ? true : false;

        if ($driver == 'wemp') {
            return redirect(session()->pull('wechat_return_url', '/wechat/mall'));
        }

        return $this->app['jsend']->success(compact('token', 'isPhone'));
    }

    /**
     * 三方登录账号绑定
     */
    public function bund(Request $request)
    {
        $driver = $request->provider;

        if (! in_array($driver, array_keys(config('oath2')))) {
            return $this->app['jsend']->error('当前绑定登录方式不许可');
        }

        $auth_user = auth()->user();
        $userId = auth()->id();

        // 校验当前方式是否已绑定
        $isBund = $this->app['socialite']->isBund($userId, $driver);
        if ($isBund) {
            return $this->app['jsend']->error('当前绑定方式已绑定过账号');
        }

        $user = $this->socialite->driver($driver)->user();

        // store user data
        $checkData = $this->app['socialite']->bundFindOrCreateByProvider($auth_user, $driver, $user);
        if ($checkData['status']) {
            //新增积分
            $childStr = '';
            if (in_array($driver, ['wechat', 'weweb', 'wemp', 'weapp'])) {
                // 微信
                $childStr = 'wechat';
            } elseif (in_array($driver, ['qqios', 'qqandroid'])) {
                // qq
                $childStr = 'qq';
            } elseif (in_array($driver, ['weiboapp'])) {
                // 微博
                $childStr = 'weibo';
            }
            app('pointReceived')->userAddPoint('update', $childStr);
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($checkData['message']);
    }

    /**
     * 获取当前微信用户openid不存储用户信息
     */
    public function openid(Request $request)
    {
        $driver = $request->provider;

        if (! in_array($driver, array_keys(config('oath2')))) {
            return $this->app['jsend']->error('当前绑定登录方式不许可');
        }

        $user = $this->socialite->driver($driver)->user();

        $openid = $user->getId();

        return $this->app['jsend']->success(['openid' => $openid]);
    }
}
