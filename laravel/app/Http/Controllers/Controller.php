<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Session\Session;

class Controller extends BaseController
{
    protected $app;

    protected $isAjax = false;

    public function __construct()
    {
        $this->app = app();

        $request = $this->createDefaultRequest();
        if ($request->ajax() || $request->wantsJson()) {
            $this->isAjax = true;
        }
    }

    /**
     * Create default request instance.
     */
    protected function createDefaultRequest()
    {
        return Request::createFromBase(SymfonyRequest::createFromGlobals());
    }
}
