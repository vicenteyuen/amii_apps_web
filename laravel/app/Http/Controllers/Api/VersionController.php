<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class VersionController extends Controller
{
    // 获取最新系统版本
    public function getNewest(Request $request){

        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('type不能为空且必须为string类型！');
        }

        $type = $request->type;

        if (!$request->channel) {
            $channel = 'mobileApp';
        } else {
            $channel = $request->channel;
        }

        $newest = $this->app['version']->getNewest($type, $channel);

        if (!$newest) {
            return $this->app['jsend']->success();
        }

        $arr = [
            'id'      => $newest->id,
            'name'    => $newest->name,
            'channel' => $newest->channel,
            'code'    => $newest->version_code,
            'version' => $newest->version_number,
            'url'     => $newest->version_url,
            'description' => $newest->description,
        ];

        return $this->app['jsend']->success($arr);
    }
}
