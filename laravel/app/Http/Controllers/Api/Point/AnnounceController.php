<?php

namespace App\Http\Controllers\Api\Point;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AnnounceController extends Controller
{
    public function show($id)
    {
        $announces = $this->app['point.rank.award']->show($id);
        if (! $announces) {
            return $this->app['jsend']->error('奖励已下线');
        }
        return $this->app['jsend']->success(['announces' => $announces]);
    }
}
