<?php

namespace App\Http\Controllers\Api\Point;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('api.auth');
    }
    /**
     * 提交订单
     */
    public function store(Request $request)
    {
        $store = $this->app['amii.point.order']->store(auth()->id(), $request);

        return $store;
    }
}
