<?php

namespace App\Http\Controllers\Api\Point;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class PayController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('api.auth');
    }
    /**
     * 获取提交兑换数据
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'  => 'required'
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        $userId = auth()->id();

        $order = $this->app['order']->getOrder2Pay($userId, $request->orderSn, 'point');
        if (! $order) {
            return $this->app['jsend']->error('订单不在可支付状态');
        }

        // 获取默认地址
        $addr = $this->app['address']->default($userId);

        // 需使用积分
        $point = $this->app['amii.point.order']->orderNeedPoint($order);

        // 校验用户积分是否可以兑换
        $userPoint = $this->app['user']->userRemainPoint($userId);
        if ($point > $userPoint) {
            return $this->app['jsend']->error('积分不足');
        }

        return $this->app['jsend']->success(compact('addr', 'point', 'order'));
    }

    /**
     * 提交订单兑换
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'       => 'required',
            'addrId'        => 'required|integer',
            'bestTime'      => '',
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        $userId = auth()->id();

        DB::beginTransaction();
        $order = $this->app['order']->getOrder2Pay($userId, $request->orderSn, 'point');
        if (! $order) {
            return $this->app['jsend']->error('订单不在可支付状态');
        }

        // 需使用积分
        $point = $this->app['amii.point.order']->orderNeedPoint($order);

        // 校验用户积分是否可以兑换
        $userPoint = $this->app['user']->userRemainPoint($userId);
        if ($point > $userPoint) {
            return $this->app['jsend']->error('积分不足');
        }

        $requestData = $request->only([
            'orderSn',
            'addrId',
            'bestTime',
        ]);

        $requestData['point'] = $point;

        // 存储订单数据
        $storeOrderData = $this->app['pay']->storeOrderData($order, $requestData, 'point');
        if (! $storeOrderData['status']) {
            DB::rollBack();
            return app('jsend')->error($storeOrderData['message']);
        }
        if (! $storeOrderData['data']) {
            DB::rollBack();
            return app('jsend')->error('存储订单数据失败,请重试');
        }

        DB::commit();
        return $this->app['jsend']->success();
    }
}
