<?php

namespace App\Http\Controllers\Api\Point;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class PointProductController extends Controller
{
    /**
     * 积分商品列表
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        $data = $this->app['product']->pointProduct($request, 2, $request->sort);
        if ($data) {
            $score = $this->app['product']->pointScore();
            return $this->app['jsend']->success(['score' => $score,'products' => $data]);
        }
        return $this->app['jsend']->error('获取商品失败');
    }

    /**
     * 积分商品详情
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $id)
    {
        $data = $this->app['product']->productDetail($id, 2);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('获取商品详情失败', 30001);
    }
}
