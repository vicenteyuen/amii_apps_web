<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class SellProductController extends Controller
{
    /**
     * 商品详情
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $id)
    {
        $data = $this->app['sellproduct']->detail($id);

        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('获取商品失败');
        
    }
      //分享商品增加积分
    public function shareProduct($provider)
    {
        $data = app('pointReceived')->userAddPoint('share', $provider);
        if ($data) {
            switch ($provider) {
                case 'product':
                    $message = '分享商品增加积分成功';
                    break;

                case 'puzzle_index':
                    $message = '分享游戏首页积分成功';
                    break;

                case 'puzzle':
                    $message = '分享游戏链接增加积分成功';
                    break;
                    
                case 'subject':
                    $message = '专题分享增加积分成功';
                    break;
                case 'video':
                    $message = '视频分享增加积分成功';
                    break;
                case 'tribe':
                    $message = '分佣规则分享增加积分成功';
                    break;
                case 'activity':
                    $message = '首单七折购分享增加积分成功';
                    break;
                case 'home':
                    $message = '首页分享增加积分成功';
                    break;
             
                default:
                    $message = 'fail';
                    break;
            }
            return $this->app['jsend']->success(['status' => '1','message' => $message,'points' => $data]);
        }
        return $this->app['jsend']->success();
    }

    //添加
    public function addGameInventory($sku, $number)
    {
        $data = $this->app['sellproduct']->skuAddGameProduct($sku, $number);
        if ($data['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($data['message']);
    }
}
