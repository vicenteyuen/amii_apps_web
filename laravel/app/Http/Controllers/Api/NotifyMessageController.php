<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class NotifyMessageController extends Controller
{
    /**
     * 通知消息列表
     */
    public function index($provider = 'notify')
    {
        $messages = $this->app['amii.notify.message']->index(auth()->id(), $provider);
        return $this->app['jsend']->success(['messages' => $messages]);
    }


    /**
     * 通知消息主面板
     */
    public function home()
    {
        $home = $this->app['amii.notify.message']->home(auth()->id());
        return $this->app['jsend']->success(['home' => $home]);
    }

    /**
     * 已读消息
     */
    public function read($provider)
    {
        $read = $this->app['amii.notify.message']->read(auth()->id(), $provider);

        if (! $read) {
            return $this->app['jsend']->error('已读失败');
        }
        return $this->app['jsend']->success();
    }

    /**
     * 删除消息
     */
    public function destroy($id)
    {
        $destroy = $this->app['amii.notify.message']->destroy(auth()->id(), $id);
        if ($destroy) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');
    }

    /**
     * 批量删除
     */
    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids'  => 'required|array',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数错误');
        }

        $deletes = $this->app['amii.notify.message']->deletes(auth()->id(), $request->ids);

        if ($deletes) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');
    }
}
