<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PurchaseTextController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('api.not.check.auth', ['only' => [
            'index',
        ]]);
    }
    
    public function index()
    {
        $texts = $this->app['amii.purchase.text']->apiIndex();
        return $this->app['jsend']->success($texts);
    }
}
