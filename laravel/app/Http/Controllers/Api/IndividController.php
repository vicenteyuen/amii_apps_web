<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndividController extends Controller
{
    /**
     * 获取推荐商品列表
     */
    public function index(Request $request, $provider)
    {
        $index = $this->app['amii.individ']->index($provider, $request->sign);

        return $this->app['jsend']->success(['products' => $index]);
    }
}
