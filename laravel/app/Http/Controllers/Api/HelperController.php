<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class HelperController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('api.not.check.auth', [
            'only' => [
                'notifyNumber',
                'init',
            ],
        ]);
    }

    // 获取所有h5链接
    public function h5Urls()
    {
        // 商品详情
        $product_page = action('Apih5\ProductController@show');
        // 专题详情
        $subject_page = action('Apih5\SubjectController@show');
        // 实体店详情
        $physical_store_page = action('Apih5\PhysicalStoreController@show');
        // 积分规则
        $point_rule_page = action('Apih5\PointController@rule');
        // 会员规则
        $member_rule_page = action('Apih5\MemberController@rule');
        // 关于我们
        $about_page = action('Apih5\AboutController@show');
        // 部落规则页
        $tribe_rule_page = url('/wechat/mall/sales-home');
        // 用户规则页
        $register_agreement_page = action('Apih5\MemberController@policy');
        // 游戏分享页
        $share_red_packet_page = url('/wechat/my/money');
        // 积分奖品页
        $point_award_page = action('Apih5\PointController@award');
        //我的拼图
        $exit = $this->app['amii.user.game']->checkUserPlaying();
        if ($exit) {
            $user_game_page = url('/wechat/game/puzzle');
        } else {
            $user_game_page = url('/wechat/game/heartbeat');
        }
       

        return $this->app['jsend']->success(compact(
            'product_page',
            'subject_page',
            'physical_store_page',
            'point_rule_page',
            'member_rule_page',
            'about_page',
            'tribe_rule_page',
            'register_agreement_page',
            'share_red_packet_page',
            'point_award_page',
            'user_game_page'
        ));
    }

    // 获取公钥
    public function xpublic()
    {
        $public = '1';
        return $this->app['jsend']->success(['public' => $public]);
    }

    // 获取通知数目
    public function notifyNumber()
    {
        $userId = auth()->id();
        $cart = $this->app['cart']->number($userId);
        $order = $this->app['order']->statusCount($userId);
        $notify = $this->app['amii.notify.message']->number($userId);
        $is_login = auth()->check();

        return $this->app['jsend']->success(compact('cart', 'order', 'notify', 'is_login'));
    }

    /**
     * init
     */
    public function init()
    {
        // 客服电话
        $customer_service_phone = '4006-839-776';

        // 积分商城顶部背景图
        $point_top_background_img = $this->app['amii.init']->getPointTopBackgroundImg();

        // 部落顶部背景图
        $tribe_top_background_img = $this->app['amii.init']->getTribeTopBackgroundImg();

        // 资产顶部背景图
        $asset_top_background_img = $this->app['amii.init']->getAssetTopBackgroundImg();

        return $this->app['jsend']->success(
            compact(
                'customer_service_phone',
                'point_top_background_img',
                'tribe_top_background_img',
                'asset_top_background_img'
            )
        );
    }
}
