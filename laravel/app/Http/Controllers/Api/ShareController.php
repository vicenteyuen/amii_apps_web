<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ShareController extends Controller
{
    private $token;

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->token = $request->header('token');
    }

    // 获取分享文案
    public function show(Request $request, $provider)
    {

        switch ($provider) {
             //获取单品页分享文案
            case 'product':
                $sell_product_id = $request->id;
                if (!$sell_product_id) {
                    return $this->app['jsend']->error('参数有误');
                }
                $data = $this->app['share']->getPoductShare($sell_product_id, $this->token);
                break;
            //获取社区专题分享文案
            case 'subject':
                $subject_id = $request->id;
                if (!$subject_id) {
                    return $this->app['jsend']->error('参数有误');
                }
                $data = $this->app['share']->getSubjectShare($subject_id, $this->token);
                break;
            //获取社区视频分享文案
            case 'video':
                $video_id = $request->id;
                if (!$video_id) {
                    return $this->app['jsend']->error('参数有误');
                }
                $data = $this->app['share']->getVideoShare($video_id, $this->token);
                break;
            //获取二维码分享文案
            case 'code':
                $data = $this->app['share']->getCodeShare($this->token);
                break;
            //获取部落分享文案
            case 'tribe':
                $data = $this->app['share']->getTribeShare($this->token);
                break;
            case 'activity':
                $data = $this->app['share']->getActivityShare($this->token);
                break;
            //获取拼图分享文案
            case 'puzzle':
                $puzzle_game_id = $request->id;
                if (!$puzzle_game_id) {
                    return $this->app['jsend']->error('参数有误');
                }
                $data = $this->app['share']->getPuzzleShare($puzzle_game_id, $this->token);
                break;
            case 'puzzle_index':
                $data = $this->app['share']->getPuzzleListShare($this->token);
                break;
            case 'red_packet':
                $data = $this->app['share']->getRedPacketShare($this->token);
                break;
            case 'home':
                $data = $this->app['share']->getHomeShare($this->token);
                break;

            default:
                return $this->app['jsend']->error('分享端口参数有误');
        }

        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->success();
    }
}
