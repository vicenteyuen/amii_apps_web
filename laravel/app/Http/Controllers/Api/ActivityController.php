<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($provider)
    {
        $activity = $this->app['amii.activity']->getActivity($provider);
        return $this->app['jsend']->success(['activity' => $activity]);
    }

    public function list()
    {
        $items = $this->app['amii.activity.rule']->index();
        return $this->app['jsend']->success(['activity_list' => $items]);
    }
}
