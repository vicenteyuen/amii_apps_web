<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class PointRecoredController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }

    public function index()
    {
        $datas = $this->app['pointRecored']->index($this->user_id);
        if ($datas) {
            return $this->app['jsend']->success(['points' =>$datas]);
        }
        return $this->app['jsend']->error('暂无收益明细');
    }
}
