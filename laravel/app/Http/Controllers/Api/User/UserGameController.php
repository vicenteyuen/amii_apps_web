<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use Auth;

class UserGameController extends Controller
{
    //用户参加游戏
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sell_product_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }
        $arr = [
            'sell_product_id' => $request->sell_product_id,
            'user_id'         => auth()->id(),
        ];

        $id =   $this->app['amii.user.game']->store($arr);
        if ($id) {
            return $this->app['jsend']->success(['id' => $id]);
        }
        return $this->app['jsend']->error('您已完成拼图或正在拼图');
    }


    //用户激活
    public function activate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_games_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }
        $arr = [
            'user_games_id' => $request->user_games_id,
            'user_id'       => auth()->id(),
        ];
        $data = $this->app['amii.user.game']->relationUserAdd($arr);
        if ($data['status']) {
            $this->app['pointReceived']->userAddPoint('share', 'game_url', null, $data['data']->user_id);
            return $this->app['jsend']->success(['finished_number' => $data['data']->finished_number,'success_time' => $data['data']->success_time,'new' => $data['new']]);
        }
        return $this->app['jsend']->error($data['message']);
    }

    // 游戏列表
    public function index($type = null)
    {
        $data = $this->app['amii.user.game']->index($type);
        $data = $data->toArray();
        $data['can_join_game'] = app('amii.user.game')->checkedUserJoinGame();
        return $this->app['jsend']->success($data);
    }

    //是否可参加游戏
    public function canJoinGame()
    {
        $data['can_join_game'] = app('amii.user.game')->checkedUserJoinGame();
        return $this->app['jsend']->success($data);
    }

    //我的拼图
    public function userPlay()
    {
        $data = $this->app['amii.user.game']->userPlay(auth()->id());
        return $this->app['jsend']->success($data);

    }

    // 用户当前拼图
    public function userGame($id)
    {
        $data = $this->app['amii.user.game']->userGame($id, auth()->id());
        return $this->app['jsend']->success($data);
    }

    //游戏进程
    public function playingGame(Request $request)
    {
        $data = $this->app['amii.user.game']->playingGame($request->user_game_id);
        return $this->app['jsend']->success($data);
    }

    // 用户正在进行中的游戏
    public function userPlayingGame()
    {
        $data = $this->app['amii.user.game']->userPlayingGame();
        $remainTime = $this->app['amii.game.order']->checkTimesLessThree(auth()->id());
        return $this->app['jsend']->success(['game' => $data,'time_to_in'=>$remainTime]);
    }

    // 用户过期游戏
    public function expireGame()
    {
        $data = $this->app['amii.user.game']->expireGame();
        return $this->app['jsend']->success(['game' => $data]);
    }

    //参加人数
    public function joinUsers()
    {
        $sendNumber = 165;
        $joinNumber = 1132;
        $join = $this->app['amii.user.game']->joinUsers();
        $send = $this->app['amii.user.game']->sendProductNumber() + 165;
        $playing = $this->app['amii.user.game']->checkUserPlaying();
        $remainNumber = $this->app['amii.user.game']->remainNumber();
        return $this->app['jsend']->success(['join_number' => $join,'send_number' => $send,'playing' => $playing,'remain_number' =>$remainNumber]);
    }

    // 通过sku计算正在进行的游戏
    public function countPlayingBySkuCode($skueCode)
    {
        $number = $this->app['amii.user.game']->countPlayingBySkuCode($skueCode);
        return $this->app['jsend']->success(['number' => $number]);
    }

    // 用户登陆添加游戏id
    public function addUserGameId(Request $request)
    {
        if (!$request->user_game_id) {
             return $this->app['jsend']->error('参数出错');
        }
        $data = $this->app['user']->addUserGameId(auth()->id(), $request->user_game_id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }

    //激活用户
    public function activiting(Request $request)
    {
        $user = User::find(auth()->id());
        if (!$user || $user->user_game_id == 0) {
            return $this->app['jsend']->success();
        }
        $arr = [
            'user_id' => auth()->id(),
            'user_games_id' => $user->user_game_id,
        ];

        $data = app('amii.user.game')->relationUserAdd($arr);
        if ($data['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->success();
    }
}
