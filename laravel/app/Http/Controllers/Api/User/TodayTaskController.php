<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TodayTaskController extends Controller
{
    protected $userId;

    public function __construct()
    {
        parent::__construct();
        $this->userId = auth()->id();
    }

    //今日任务列表
    public function index()
    {
       $data = $this->app['todayTask']->getIndex($this->userId);
       if ($data) {
            return $this->app['jsend']->success($data);
       }
       return $this->app['jsend']->success();
    }
}
