<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PointsRankController extends Controller
{
    // 本月本年排名
    public function show($provider)
    {
        $data = $this->app['pointRank']->show($provider);
        return $this->app['jsend']->success($data);
    }

    // 上月排名
    public function lastShow()
    {
        $data = $this->app['pointRank']->lastMonthReward();
        return $this->app['jsend']->success(['last_month' => $data]);
    }
}
