<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BalanceController extends Controller
{
    public function index(Request $request)
    {
        $user = $this->app['balance']->index(auth()->id(), $request);
        if ($user) {
             return $this->app['jsend']->success(compact('user'));
        }
        return $this->app['jsend']->error('数据获取失败');
    }
}
