<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class OrderController extends Controller
{
    /**
     * 订单列表
     */
    public function index(Request $request)
    {
        $orders = $this->app['order']->index(auth()->id(), $request);
        return $this->app['jsend']->success(compact('orders'));
    }

    /**
     * 获取最近购买商品
     */
    public function products()
    {
        $orderProducts = $this->app['order']->orderProducts(auth()->id());

        return $this->app['jsend']->success(['orderProducts' => $orderProducts]);
    }

    /**
     * 提交订单
     */
    public function store(Request $request, $provider = 'buy')
    {
        $userId = auth()->id();

        $returnData = null;
        if ($provider == 'buy') {
            $returnData = $this->app['order']->buyNow($userId, $request);
        } elseif ($provider == 'cart') {
            $returnData = $this->app['order']->buyCart($userId, $request);
        }

        if ($returnData) {
            return $returnData;
        }
        return $this->app['jsend']->error('创建订单失败');
    }

    /**
     * 订单详情
     */
    public function show($orderSn)
    {
        $order = $this->app['order']->show(auth()->id(), $orderSn);
        if (! $order) {
            return $this->app['jsend']->error('订单不存在');
        }

        return $this->app['jsend']->success(compact('order'));
    }

    /**
     * 删除订单
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'  => 'required'
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        $order = $this->app['order']->destroy(auth()->id(), $request->orderSn);
        if ($order) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败,或订单不在可删除状态');
    }

    /**
     * order status count
     */
    public function statusCount()
    {
        $userId = auth()->id();
        $counts = $this->app['order']->statusCount($userId);

        return $this->app['jsend']->success(['counts' => $counts]);
    }

    /**
     * cancel order
     */
    public function cancel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'  => 'required'
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        $cancel = $this->app['order']->cancel(auth()->id(), $request->orderSn);
        if ($cancel) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('取消订单失败或订单当前不可取消');
    }

    /**
     * 确认收货
     */
    public function confirm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'  => 'required'
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        $confirm = $this->app['order']->orderReceive(auth()->id(), $request->orderSn);
        if ($confirm) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('确认收货失败,或已确认收货');
    }


    /*
    快递信息
     */
    public function express(Request $request, $orderSn)
    {
        $data = $this->app['express']->getExpress($orderSn);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('查询失败');
    }

    /**
    获取可评价订单
    */
    public function orderAppraising($orderSn)
    {
        $data = $this->app['order']->orderAppraising($orderSn);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('查询失败');
    }
}
