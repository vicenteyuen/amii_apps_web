<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class GainController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }
    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'income'    =>  'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('10001');
        }

        $arr = [
            'user_id'    => $this->user_id,
            'income'     => $request->income,
        ];

        $data = $this->app['gain']->store($arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('新增失败');
    }

    public function gainShow()
    {
        $data['tribe'] = $this->app['gain']->userGain(auth()->id());
        $data['recently'] = $this->app['gain']->halfMonth(auth()->id());
        return $this->app['jsend']->success($data);
    }
}
