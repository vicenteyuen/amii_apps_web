<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserQuestionController extends Controller
{
    public function index()
    {
        $data = $this->app['question']->index();
        return $this->app['jsend']->success($data);
    }
}
