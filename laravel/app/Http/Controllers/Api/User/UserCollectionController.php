<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\PointReceived;

class UserCollectionController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }

    /**
     * 收藏列表
     * @return json
     */
    public function index()
    {
        $datas = $this->app['collection']->show($this->user_id);
        if ($datas) {
            return $this->app['jsend']->success(['collection' =>$datas]);
        }
        return $this->app['jsend']->error('暂无收藏商品');
    }

    /**
     * 增加收藏
     * @param  Request $request [description]
     * @return json
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'sell_product_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'sell_product_id' => $request->sell_product_id,
            'user_id'    => $this->user_id,
        ];

        if (isset($request->provider)) {
            $arr['provider'] = 2;
        }
        $data = $this->app['collection']->store($arr);
        if ($data) {
            return $this->app['jsend']->success(['id'=>$data->id]);
        }
        return $this->app['jsend']->error('新增收藏失败');
    }

    /**
     * 批量收藏
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function stores(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $data = $this->app['collection']->stores($request->ids, $this->user_id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('批量收藏失败');
    }

    /**
     * 批量删除
     * @return [type] [description]
     */
    public function deleteAll(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $data = $this->app['collection']->deleteAll($request->ids);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');

    }

    /**
     * 删除收藏
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $delete = $this->app['collection']->delete($id);
        if ($delete) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除收藏失败');

    }

    public function gameIndex()
    {
        $datas = $this->app['collection']->gameIndex($this->user_id);
        if ($datas) {
            return $this->app['jsend']->success(['collection' =>$datas]);
        }
        return $this->app['jsend']->error('暂无收藏商品');
    }
}
