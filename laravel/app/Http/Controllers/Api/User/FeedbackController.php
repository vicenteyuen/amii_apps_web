<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class FeedbackController extends Controller
{
    /**
     * 获取新建意见反馈数据
     */
    public function create()
    {
        $data = [
            'support' => '中国区客服热线：4006-839-776',
            'description' => '请输入您的问题/反馈,如被采纳送200积分',
        ];

        return $this->app['jsend']->success(['feedback_data' => $data]);
    }

    /**
     * 新增意见反馈
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content'  => 'required'
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        $storeData = [
            'user_id' => auth()->id(),
            'content' => trim($request->get('content'))
        ];

        $store = $this->app['amii.feedback']->store($storeData);
        if (! $store) {
            return $this->app['jsend']->error('添加意见反馈失败');
        }
        return $this->app['jsend']->success();
    }
}
