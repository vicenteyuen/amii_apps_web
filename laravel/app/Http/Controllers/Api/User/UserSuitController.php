<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Gimage\Gimage;
use App\Helpers\ImageHelper;
use App\Helpers\SizeHelper;

class UserSuitController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }

     /**
     * 身材数据列表
     * @return json
     */
    public function index()
    {

        $datas = $this->app['suit']->show($this->user_id);
        if ($datas) {
            return $this->app['jsend']->success($datas);
        }
        return $this->app['jsend']->error('身材数据获取失败');
    }

    /**
     * 增加身材数据
     * @param  Request $request [description]
     * @return json
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name'              => 'required|string',
            'height'            => 'string',
            'weight'            => 'string',
            'shoulder_width'    => 'string',
            'thigh_width'       => 'string',
            'bust'              => 'string',
            'waist'             => 'string',
            'hips'              => 'string',
            'status'            => 'integer',
            'skin'              => 'integer',
            'hair'              => 'integer',
            'front_photo'       => 'string',
            'side_photo'        => 'string',
            'leg_width'         => 'string',
            "answer"            => "array",
            "gender"            => 'integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'name'              => $request->name,
            'height'            => $request->height,
            'weight'            => $request->weight,
            'bust'              => $request->bust,
            'waist'             => $request->waist,
            'hips'              => $request->hips,
            'shoulder_width'    => $request->shoulder_width,
            'thigh_width'       => $request->thigh_width,
            'skin'              => $request->skin,
            'hair'              => $request->hair,
            'front_photo'       => $request->front_photo,
            'side_photo'        => $request->side_photo,
            'user_id'           => $this->user_id,
            'status'            => $request->status ? 1 : 0,
            'leg_width'         => $request->leg_width,
            'gender'            => $request->gender ? : 0,
        ];
        $answer = $request->answer;
        $data = $this->app['suit']->store($arr);
        if ($data) {
            $this->app['suit']->addlookingPoint($arr);
            if (!empty($answer)) {
                foreach ($answer as $k => &$v) {
                    $v['user_suit_id'] = $data->id;
                    $v['user_id']      =  $this->user_id;
                    if ($v['question_id'] == 1) {
                        $typeChild = 'up_size';
                    } elseif ($v['question_id'] == 2) {
                        $typeChild = 'lower_size';
                    } else {
                        $typeChild = 'none';
                    }
                    app('pointReceived')->userAddPoint('looking', $typeChild);
                }
                //回答问题
                app('answer')->insert($answer);
                // 推荐尺寸
                if (!empty($arr['height'])) {
                    $sizeId = SizeHelper::recommendSize($arr['height']);
                    if ($sizeId) {
                        $data->size_id = $sizeId;
                        $data->save();
                    }
                }
            }
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('新增身材数据失败');
    }

    /**
     * 编辑身材数据
     * @param  Request $request [description]
     * @return json
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name'              => 'required|string',
            'height'            => 'string',
            'weight'            => 'string',
            'shoulder_width'    => 'string',
            'thigh_width'       => 'string',
            'bust'              => 'string',
            'waist'             => 'string',
            'hips'              => 'string',
            'status'            => 'integer',
            'skin'              => 'integer',
            'hair'              => 'integer',
            'front_photo'       => 'string',
            'side_photo'        => 'string',
            'leg_width'         => 'string',
            "answer"            => "array",
            "gender"            => 'integer',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'name'              => $request->name,
            'height'            => $request->height,
            'weight'            => $request->weight,
            'bust'              => $request->bust,
            'waist'             => $request->waist,
            'hips'              => $request->hips,
            'shoulder_width'    => $request->shoulder_width,
            'thigh_width'       => $request->thigh_width,
            'skin'              => $request->skin,
            'hair'              => $request->hair,
            'front_photo'       => $request->front_photo,
            'side_photo'        => $request->side_photo,
            'user_id'           => $this->user_id,
            'status'            => $request->status ? 1 : 0,
            "user_id"           => $this->user_id,
            'leg_width'         => $request->leg_width,
            'gender'           => $request->gender ? : 0,
        ];

        $answer = $request->answer;
        if (!empty($answer)) {
            foreach ($answer as $k => &$v) {
                $v['user_suit_id'] = $id;
                $v['user_id']      =  $this->user_id;
                if ($v['question_id'] == 1) {
                    $typeChild = 'up_size';
                } elseif ($v['question_id'] == 2) {
                    $typeChild = 'lower_size';
                } else {
                    $typeChild = 'none';
                }
                app('pointReceived')->userAddPoint('looking', $typeChild);
            }
        }
        $data = $this->app['suit']->update($arr, $id, $answer);
        if ($data) {
            $this->app['suit']->addlookingPoint($arr);
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('保存身材数据失败');
    }

    /**
     * 编辑获取数据
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show(Request $request)
    {
        $data = $this->app['suit']->detail($request->suit);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('身材数据获取失败');
    }

    /**
     * 默认身材数据
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function status(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'      => 'required|integer',
            'status'  => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $data = $this->app['suit']->status($this->user_id, $request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('设置默认身材数据失败');
    }

    /**
     * 删除身材数据
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $delete = $this->app['suit']->delete($id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除身材数据失败');
    }

    /**
     * 上下装 肤色发色接口
     * @return [type] [description]
     */
    public function size()
    {
        $size = $this->app['suit']->size();
        return $this->app['jsend']->success($size);
    }

    public function defaults($id)
    {
        $default = $this->app['suit']->defaults($this->user_id, $id);
        return $this->app['jsend']->success($default);
    }

    /**
     * 上传图片
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function image(Request $request)
    {
        $file = $request->file('image');
        if (!$file) {
            return $this->app['jsend']->error('fail');
        }
        $storeFile = Gimage::uploadImage($file, 'avatar', 'images');
        if ($storeFile instanceof \Exception) {
            return response()->json(0);
        }
        $url = ImageHelper::getImageUrl($storeFile, 'avatar');
        return $this->app['jsend']->success(['image' => $storeFile,'url' => $url]);
    }
}
