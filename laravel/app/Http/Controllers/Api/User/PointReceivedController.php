<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PointReceivedController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->middleware('api.not.check.auth', [
            'only' => [
                'received',
            ]
        ]);
    }
    public function index()
    {
        $data = $this->app['pointReceived']->index();
        return $this->app['jsend']->success(['points'=>$data]);
    }

    //签到增加
    public function received()
    {
        $id = auth()->id();
        if (!$id) {
            return $this->app['jsend']->success();
        }
        if (!auth()->user()->phone) {
            return $this->app['jsend']->success();
        }
        $data =  $this->app['pointReceived']->userAddPoint('received_day', 'received_day');
        if ($data) {
            $day = \App\Services\PointReceivedService::continuteSign($id);
            $message = '连续登录'.$day.'天';
            return $this->app['jsend']->success(['status' => '1','message' =>  $message,'points' => $data]);
        }
        return $this->app['jsend']->success();
    }

    // 订单查询积分
    public function receivedOrder($orderSn)
    {
        $data = $this->app['pointReceived']->receivedOrder($orderSn);
        if ($data) {
            return $this->app['jsend']->success(['status' => '1','message' => '支付成功','points' => $data]);
        }
        return $this->app['jsend']->success();
    }
}
