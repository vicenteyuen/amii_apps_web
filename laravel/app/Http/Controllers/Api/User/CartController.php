<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class CartController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }

     /**
     * 购物车列表
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        
        $data = $this->app['cart']->index($this->user_id);
        if ($data !== false) {
            return $this->app['jsend']->success(['carts' =>$data['cart'],'activityMessage' => $data['activityMessage']]);
        }
        return $this->app['jsend']->error('获取列表失败');
    }

    /**
     * 新增商品到购物车
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inventory_id'        => 'required|integer',
            'number'              => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $arr = [
            'inventory_id'        => $request->inventory_id,
            'number'              => $request->number,
            'user_id'             => $this->user_id,
        ];

        $data = $this->app['cart']->store($arr);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('新增失败');

    }

    /**
     * 批量新增商品到购物车
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function stores(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'carts'        => 'required|array',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $data = $this->app['cart']->stores($request->carts, $this->user_id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('批量新增失败');
    }

    /**
     * 获取编辑页面的信息
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show(Request $request)
    {
        $data = $this->app['cart']->show($request->sell_product_id);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('编辑信息获取失败');
    }


    /**
     * 编辑购物车的商品属性
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inventory_id'        => 'required|integer',
            'number'              => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $arr = [
            'inventory_id'        => $request->inventory_id,
            'number'              => $request->number,
        ];

        $data = $this->app['cart']->update($request->cart, $arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('编辑失败');
    }

    /**
     * 删除购物车中的商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request)
    {
        $data = $this->app['cart']->delete($request->cart);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');

    }

    /**
     * 批量删除
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deletes(Request $request)
    {
        $data = $this->app['cart']->delete($request->ids);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');
    }
}
