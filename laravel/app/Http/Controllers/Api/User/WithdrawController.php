<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

use App\Http\Controllers\Controller;

class WithdrawController extends Controller
{
    private $auth;
    /**
     * @var \App\Models\User
     */
    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->auth = auth();
        $this->user = $this->auth->user();
    }

    /**
     * 获取提现页面数据
     * @return mixed
     */
    public function getIndex()
    {
        $channel = $this->user->withdrawChannels()
            ->where('type', 'alipay')
            ->orderBy('type', 'asc')->first();
        $wallet_balance = $this->user->cumulate_profit;
        $has_password = $this->user->has_payment_password;
        return $this->app['jsend']->success(compact('channel', 'wallet_balance', 'has_password'));
    }

    /**
     * 获取提现数据
     * @return mixed
     */
    public function getApply()
    {
        return $this->app['jsend']->success($this->user->withdrawChannels()
            ->where('type', 'alipay')
            ->get());
    }

    /**
     * 申请提现校验是否可以获取短信验证码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postApply(Request $request)
    {
        Validator::extend('payment_password_check', function ($attribute, $value, $parameters) {
            if (!$this->user) abort(401, 'Unauthorized');
            return $this->user->checkPaymentPassword($value);
        }, '提现密码错误');
        $cumulate_profit = (float)$this->user->cumulate_profit;
        $rules = [
            'channel_type' => 'required|in:alipay,wechat',
            'amount' => 'required|numeric|min:0.1|max:'.$cumulate_profit,
            'password' => 'required|payment_password_check',
        ];
        $customAttributes = [
            'amount' => '提现金额'
        ];
        $validator = Validator::make($request->all(), $rules, [], $customAttributes);

        if ($validator->fails()) {
            return app('jsend')->error($validator->errors()->first(), null, $validator->errors());
        }

        if ($cumulate_profit < $request->amount) {
            return app('jsend')->error('可提现余额不足此次提现');
        }

        $channel = $this->user->withdrawChannels->where('type', $request->channel_type)->first();
        if (!$channel) {
            return app('jsend')->error('申请失败，无此渠道');
        }
        // 一天提现一次
        $withdrawRecords = $channel->record()
            ->orderBy('id', 'desc')
            ->get();
        if (! $withdrawRecords->isEmpty()) {
            $dtDateRecord = Carbon::parse(Carbon::parse($withdrawRecords[0]->created_at)->toDateString());
            $dtDateNow = Carbon::parse(Carbon::now()->toDateString());
            if ($dtDateRecord->diffInDays($dtDateNow) < 1) {
                return app('jsend')->error('当前限制一天只能提现一次');
            }
        }
        return $this->app['jsend']->success();
    }

    /**
     * 提交提现申请
     */
    public function postCodeApply(Request $request)
    {
        Validator::extend('payment_password_check', function ($attribute, $value, $parameters) {
            if (!$this->user) abort(401, 'Unauthorized');
            return $this->user->checkPaymentPassword($value);
        }, '提现密码错误');
        $cumulate_profit = (float)$this->user->cumulate_profit;
        $rules = [
            'channel_type' => 'required|in:alipay,wechat',
            'amount' => 'required|numeric|min:0.1|max:'.$cumulate_profit,
            'password' => 'required|payment_password_check',
            'code' => 'required',
        ];
        $customAttributes = [
            'amount' => '提现金额'
        ];
        $validator = Validator::make($request->all(), $rules, [], $customAttributes);

        if ($validator->fails()) {
            return app('jsend')->error($validator->errors()->first(), null, $validator->errors());
        }

        // 手机验证码校验
        if (! $this->app->make('leancloud', ['sms'])->verifySmsCode(auth()->user()->phone, $request->code)) {
            return $this->app['jsend']->error('验证码错误');
        }

        if ($cumulate_profit < $request->amount) {
            return app('jsend')->error('可提现余额不足此次提现');
        }

        $channel = $this->user->withdrawChannels->where('type', $request->channel_type)->first();
        if (!$channel) {
            return app('jsend')->error('申请失败，无此渠道');
        }
        // 一天提现一次
        $withdrawRecords = $channel->record()
            ->orderBy('id', 'desc')
            ->get();
        if (! $withdrawRecords->isEmpty()) {
            $dtDateRecord = Carbon::parse(Carbon::parse($withdrawRecords[0]->created_at)->toDateString());
            $dtDateNow = Carbon::parse(Carbon::now()->toDateString());
            if ($dtDateRecord->diffInDays($dtDateNow) < 1) {
                return app('jsend')->error('当前限制一天只能提现一次');
            }
        }

        \DB::beginTransaction();
        /**
         * @var \App\Models\WithdrawRecord $record
         */
        $record = $channel->record()->create([
            'amount' => $request->amount,
        ]);
        // 1000以上额度需要财务审核
        if ($request->amount < 1000) {
            $record->update([
                'state' => 1
            ]);
           $res = $record->paySend();
            if (isset($res['status']) && $res['status'] == false) {
                \DB::rollback();
                return $this->app['jsend']->error($res['msg'], 60600);
            }
        }
        \DB::commit();
        return $this->app['jsend']->success();
    }

    /**
     * 获取渠道
     * @return mixed
     */
    public function getChannels()
    {
        $data = [
            $this->user->withdrawChannels->where('type', 'alipay')->first(),
            // $this->user->withdrawChannels->where('type', 'wechat')->first(),
        ];
        return $this->app['jsend']->success(array_values(array_filter($data)));
    }

    /**
     * 添加渠道
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChannels(Request $request)
    {
        $rules = [
            'type' => 'required|in:alipay',
            'account_number' => '',
            'account_name' => ''
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return app('jsend')->error($validator->errors()->first(), null, $validator->errors());
        }

        $data = $this->user->withdrawChannels()->updateOrCreate($request->only(['type']), $request->only(array_keys($rules)));

        return $this->app['jsend']->success($data);
    }
}
