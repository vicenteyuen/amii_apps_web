<?php

namespace App\Http\Controllers\Api\User;

use App\Jobs\OrderRefund;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageHelper;
use Gimage\Gimage;

use Validator;

class OrderProductRefundController extends Controller
{
    public function index($order_product_id)
    {
        $refund = $this->app['amii.order.refund']->index($order_product_id);

        return $this->app['jsend']->success($refund);
    }

    /**
     * 申请售后
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request, $order_product_id)
    {
        $validator = Validator::make($request->all(), [
            'provider'       => 'required|in:pay,product',
            'images'         => 'array',
            'images.*.value' => 'required',
            'options_str'    => '',
            'express_fee'    => '',
            'money'          => 'required_without_all:express_fee',
            'content'        => '',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误', null, $errors);
        }
        $isProduct = $request->provider == 'product';

        $store = $this->app['amii.order.refund']->applyRefund($order_product_id, $request, $isProduct);

        if ($store) {
            return $this->index($order_product_id);
        }
        return $this->app['jsend']->error('申请售后失败, 当前订单产品不可申请售后');
    }

    /**
     * 取消售后
     * @param $order_product_id
     * @param $product_refund_id
     * @return mixed
     */
    public function destroy($order_product_id, $product_refund_id)
    {
        $product = OrderProduct::has('order')
            ->has('refund')
            ->find($order_product_id);

        if (!$product) {
            return $this->app['jsend']->error('无售后信息');
        }

        $refund = $product->refund;

        if ($refund->id == $product_refund_id) {
            $refund->delete();
//            $updateOrderRefund = app('order')->updateOrderRefund($product->order_id);
            return $this->app['jsend']->success();
        }

        return $this->app['jsend']->error('无售后信息');
    }

    /**
     * 填写售后快递信息
     * @param Request $request
     * @param $order_product_id
     */
    public function express(Request $request, $order_product_id)
    {
        $validator = Validator::make($request->all(), [
            'courier_company' => 'required',
            'courier_number' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误', null, $errors);
        }

        $data = $this->app['amii.order.refund']->express($order_product_id, $request);

        if ($data) {
            return $this->index($order_product_id);
        }
        return $this->app['jsend']->error('填写快递失败, 当前订单产品不可填写快递');
    }

    /**
     * 售后图片上传
     */
    public function image(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误');
        }

        $file = $request->file('image');
        $storeFile = Gimage::uploadImage($file, 'refund', 'images');
        $imageUrl = ImageHelper::getImageUrl($storeFile, 'refund');
        if ($storeFile instanceof \Exception) {
            return $this->app['jsend']->error('上传图片失败');
        }
        return $this->app['jsend']->success(['value' => $storeFile,'image_url' =>$imageUrl]);
    }

    /**
     * 获取售后原因选项
     * @param $order_product_id
     * @return mixed
     */
    public function Copywriting($order_product_id)
    {
        $order_product = OrderProduct::findOrFail($order_product_id);
        $refundable_amount = (float)$order_product->refundable_amount;
        $data = [];
        $type_str = '款';
        if ($order_product->order->provider == 'point') {
            $type_str = '积分';
        }
        switch ($order_product->order->order_status_str) {
            case 'sending':
                $data = [
                    [
                        'title' => '申请退' . $type_str,
                        'name' => '仅退' . $type_str,
                        'provider' => 'pay',
                        'children_title' => '退' . $type_str . '原因',
                        'children' => [
                            [
                                'name' => '拍错/多拍/不想要',
                                'max_amount' => $refundable_amount,
                            ], [
                                'name' => '协商一致退款',
                                'max_amount' => $refundable_amount,
                            ], [
                                'name' => '缺货',
                                'max_amount' => $refundable_amount,
                            ], [
                                'name' => '未按约定时间发货',
                                'max_amount' => $refundable_amount,
                            ], [
                                'name' => '其他',
                                'max_amount' => $refundable_amount,
                                'is_other' => true,
                            ],
                        ],
                    ],
                ];
                break;
            case 'receiving':
                $data = [
                    [
                        'title' => '申请退' . $type_str,
                        'name' => '仅退' . $type_str,
                        'provider' => 'pay',
                        'children_title' => '退' . $type_str . '原因',
                        'children' => [
                            [
                                'name' => '已拒收包裹',
                                'max_amount' => $refundable_amount,
                            ], [
                                'name' => '快递一直不派送',
                                'max_amount' => $refundable_amount,
                            ],
                        ],
                    ],
                ];
                break;
            case 'appraising':
            case 'success':
                $data = [
                    [
                        'title' => '申请退' . $type_str,
                        'name' => '仅退' . $type_str,
                        'provider' => 'pay',
                        'children_title' => '商品状态',
                        'children' => [
                            [
                                'name' => '已经收到商品了',
                                'children_title' => '退' . $type_str . '类型',
                                'children' => [
                                    [
                                        'max_shipping_amount' => 20,
                                        'name' => '退运费',
                                    ], [
                                        'name' => '补差价',
                                        'max_amount' => $refundable_amount,
                                    ], [
                                        'name' => '退补偿',
                                        'max_amount' => round($refundable_amount * 0.3, 2),
                                    ],
                                ],
                            ], [
                                'name' => '没有收到商品',
                                'children_title' => '退' . $type_str . '原因',
                                'children' => [
                                    [
                                        'name' => '实际没有收到包裹',
                                        'max_amount' => $refundable_amount,
                                    ], [
                                        'name' => '收到包裹是空的',
                                        'max_amount' => $refundable_amount,
                                    ], [
                                        'name' => '商品少了',
                                        'max_amount' => $refundable_amount,
                                    ],
                                ],
                            ],
                        ],
                    ], [
                        'title' => '申请退货',
                        'name' => '退货退' . $type_str,
                        'provider' => 'product',
                        'children_title' => '商品状态',
                        'children' => [
                            [
                                'name' => '质量没有问题',
                                'max_amount' => $refundable_amount,
                                'children_title' => '退' . $type_str . '原因',
                                'children' => [
                                    [
                                        'name' => '穿着效果不好',
                                        'children' => [
                                            [
                                                'name' => '不是想象中的效果',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '款式不适合我',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '版式设计有问题',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '其他',
                                                'max_amount' => $refundable_amount,
                                                'is_other' => true,
                                            ],
                                        ],
                                    ],
                                    [
                                        'name' => '尺码不合适',
                                        'children' => [
                                            [
                                                'name' => '尺码偏大',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '尺码偏小',
                                                'max_amount' => $refundable_amount,
                                            ],
                                        ],
                                    ],
                                    [
                                        'name' => '面料不喜欢',
                                        'children' => [
                                            [
                                                'name' => '不是我想要的面料',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '面料与页面描述不符',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '面料太厚',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '面料太薄',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '面料太硬/太扎',
                                                'max_amount' => $refundable_amount,
                                            ], [
                                                'name' => '其他',
                                                'max_amount' => $refundable_amount,
                                                'is_other' => true,
                                            ],
                                        ],
                                    ],
                                ],
                            ], [
                                'name' => '质量有问题',
                                'children_title' => '退' . $type_str . '原因',
                                'children' => [
                                    [
                                        'name' => '做工问题',
                                        'max_shipping_amount' => 20,
                                        'max_amount' => $refundable_amount,
                                    ], [
                                        'name' => '面料问题',
                                        'max_shipping_amount' => 20,
                                        'max_amount' => $refundable_amount,
                                    ], [
                                        'name' => '其他',
                                        'max_shipping_amount' => 20,
                                        'max_amount' => $refundable_amount,
                                        'is_other' => true,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ];
                break;
        }
        return $this->app['jsend']->success($data);
    }
}
