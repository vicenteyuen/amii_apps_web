<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserCouponController extends Controller
{
    /**
     * 获取用户优惠券
     */
    public function index(Request $request)
    {
        $userId = auth()->id();
        if (in_array($request->status, ['valid', 'expired', 'used'])) {
            $type = $request->status;
        } else {
            $type = 'all';
        }

        $coupons = $this->app['amii.manger.user.coupon']->index($userId, $type);

        return $this->app['jsend']->success(['coupons' => $coupons]);
    }

    /**
     * 用户新增优惠券
     */
    public function store(Request $request)
    {
        $userId = auth()->id();
        $store = $this->app['amii.manger.user.coupon']->store($userId, $request->cid);

        return $store;
    }

    /**
     * 删除优惠券
     */
    public function destroy($id)
    {
        $code = $id;
        $delete = $this->app['amii.manger.user.coupon']->delete($code);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败或已删除');
    }
}
