<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class EnableController extends Controller
{
    public function index()
    {
        $enable = $this->app['enable']->index();
        return $this->app['jsend']->success($enable);
    }
}
