<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class UserAddressController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }

    /**
     * 返回中国所有地址数据
     * @return [type] [description]
     */
    public function address()
    {

        $contents = $this->app['address']->addressData();
        return $this->app['jsend']->success($contents);
    }

    /**
     *  地址列表
     * @return [type] [description]
     */
    public function index(Request $request)
    {
        $datas = $this->app['address']->show($this->user_id);
        if ($datas !== false) {
            return $this->app['jsend']->success($datas);
        }
        return $this->app['jsend']->error('获取地址失败');
    }

    /**
     * 获取单个地址的详细信息
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show($id)
    {

        $validator = Validator::make(['id'=>$id], [
            'id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $data = $this->app['address']->index($id);
        if ($data) {
             return $this->app['jsend']->success($data);
        }
         return $this->app['jsend']->error('获取地址失败');

    }

    /**
    * 新增地址
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'realname'  => 'required|string',
        'phone'     => 'required|phone170',
        'region_id' => 'required|integer',
        'detail'    => 'required|string',
        'status'    => 'integer',
        'post_code' => ['string','regex:/^[0-9]\d{5}$/'],

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
        'realname'  => $request->realname,
        'phone'     => $request->phone,
        'region_id' => $request->region_id,
        'detail'    => $request->detail,
        'status'    => $request->status ? 1 : 0,
        'user_id'   => $this->user_id,
        'post_code' => $request->post_code ? $request->post_code:'',
        ];

        $data = $this->app['address']->store($arr);
        if ($data) {
            return $this->app['jsend']->success();
        }

        return $this->app['jsend']->error('新增地址失败');

    }
    /**
     * 设置默认地址
     * @param Request $request [description]
     */
    public function status(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $update = $this->app['address']->updateStatus($request->id, $this->user_id, ['status' => 1]);

        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('设置默认地址失败');
    }

    /**
     * 修改地址
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        'realname'  => 'required|string',
        'phone'     => 'required|phone',
        'region_id' => 'required|integer',
        'detail'    => 'required|string',
        'status'    => 'string',
        'post_code' => ['string','regex:/^[0-9]\d{5}$/'],

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
        'realname'  => $request->realname,
        'phone'     => $request->phone,
        'region_id' => $request->region_id,
        'detail'    => $request->detail,
        'post_code' => $request->post_code ? $request->post_code : '',
        ];
        if ($request->status) {
            $arr['status'] = $request->status;
        }
        $edit = $this->app['address']->update($arr, $id, $this->user_id);
        if ($edit) {
            return $this->app['jsend']->success();
        }

        return $this->app['jsend']->error('编辑地址失败');
    }
    /**
     * 删除地址
     * @param  Request $require [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request, $id)
    {
        $delete = $this->app['address']->delete($id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除地址失败');

    }
    /**
     * 获取默认地址的信息
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function detail(Request $request)
    {
        $data = $this->app['address']->statusDetail($this->user_id);
        if ($data !== false) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('删除地址失败');
    }
}
