<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Validator;
use App\Models\UserGame;

class PayController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'  => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', null, $errors);
        }

        $userId = auth()->id();

        $gameOrder = Order::where('order_sn', $request->orderSn)
                ->with('game')
                ->first();
        if (!$gameOrder) {
             return $this->app['jsend']->error('订单不在可支付状态');
        }

        $order = $this->app['order']->getOrder2Pay($userId, $request->orderSn, null, $gameOrder->game);
        if (! $order) {
            return $this->app['jsend']->error('订单不在可支付状态');
        }

        // 获取默认地址
        $addr = $this->app['address']->default($userId);

        // 可用优惠券
        $coupons = $this->app['order']->getCoupon4Order($order);

        $calcPayFee = $this->app['pay']->calcPayFee($request->orderSn, $userId, $addr ? $addr->id : null);

        if ($calcPayFee[0] !== 0) {
            $calc_pay_fee = [];
            $is_show_first_order_product = 0;
        } else {
            $calc_pay_fee = $calcPayFee[1];
            $is_show_first_order_product = $this->app['pay']->isShowFirstOrderProduct($calc_pay_fee);
        }

        // 实付金额
        if ($calc_pay_fee) {
            $orderPay = $calc_pay_fee['payfee']
                + $calc_pay_fee['point_discount']
                + $calc_pay_fee['balance_discount'];
        } else {
            $orderPay = 0;
        }
        // 首单购数据
        if (! $is_show_first_order_product) {
            $first_buy = null;
        } else {
            $first_buy = $this->app['amii.give.product']->checkFirstProduct($orderPay, $userId, $order->id);
            if (! $first_buy || $first_buy === true) {
                $first_buy = null;
            }
        }
        $order->give_product = $first_buy;
        if (! $first_buy) {
            $is_show_first_order_product = 0;
        }

        $game = UserGame::where('order_id', $order->id)
            ->first();
        if ($game) {
            $calc_pay_fee['payfee'] = '0.00';
        }
        //游戏商品格数
        $puzzles = $this->app['amii.user.game']->returnPuzzles($order->id);
        $order->puzzles = $puzzles;

        // 检查是否是禁用户
        $checkUser = app('user')->checkUser($order->user_id);
        if (!$checkUser) {
            $calc_pay_fee['can_use_point'] = 0;
            $calc_pay_fee['can_use_point_discount'] = '0.00';
            $calc_pay_fee['use_point'] = 0;
            $calc_pay_fee['point_discount'] = '0.00';
        }

        // 首单限制使用优惠
        $isFirstOrder = $this->app['order']->isFirstOrder($userId, $order->id);
        if ($isFirstOrder) {
            $coupons = [];
            $calc_pay_fee['can_use_point'] = 0;
            $calc_pay_fee['can_use_point_discount'] = '0.00';
            $calc_pay_fee['use_point'] = 0;
            $calc_pay_fee['point_discount'] = '0.00';
            $calc_pay_fee['can_use_balance'] = '0.00';
            $calc_pay_fee['balance_discount'] = '0.00';
        }

        return $this->app['jsend']->success(compact('calc_pay_fee', 'is_show_first_order_product', 'addr', 'coupons', 'order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnData = $this->app['pay']->store(auth()->id(), $request);
        return $returnData;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $orderSn = $id;

        // 继续支付
        $returnData = $this->app['pay']->pay($orderSn, $request);

        return $returnData;
    }

    /**
     * 获取订单支付金额
     */
    public function payFee(Request $request, $orderSn)
    {
        $validator = Validator::make($request->all(), [
            'addrId'        => '',
            'couponCode'    => '',
            'point'         => 'numeric|max:2147483647',
            'balance'       => '',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', null, $errors);
        }
        $userId = auth()->id();
        $order = app('order')->getOrder2Pay($userId, $request->orderSn);

        if (! $order) {
            return $this->app['jsend']->error('订单不在可支付状态');
        }

        $calcPayFee = $this->app['pay']->calcPayFee($orderSn, $userId, $request->addrId, $request->couponCode, $request->point, $request->balance);
        if ($calcPayFee[0] !== 0) {
            return $this->app['jsend']->error($calcPayFee[1] ? : null);
        } else {
            $calc_pay_fee = $calcPayFee[1];
            $is_show_first_order_product = $this->app['pay']->isShowFirstOrderProduct($calc_pay_fee);
        }
        // 实付金额
        if ($calc_pay_fee) {
            $orderPay = $calc_pay_fee['payfee']
                + $calc_pay_fee['point_discount']
                + $calc_pay_fee['balance_discount'];
        } else {
            $orderPay = 0;
        }
        // 首单购数据
        $first_buy = $this->app['amii.give.product']->checkFirstProduct($orderPay, $userId, $order->id);
        if (! $first_buy || $first_buy === true) {
            $first_buy = null;
        }
        $order->give_product = $first_buy;
        if (! $first_buy) {
            $is_show_first_order_product = 0;
        }

        // 检查是否是禁用户
        $checkUser = app('user')->checkUser($order->user_id);
        if (!$checkUser) {
            $calc_pay_fee['can_use_point'] = 0;
            $calc_pay_fee['can_use_point_discount'] = '0.00';
            $calc_pay_fee['use_point'] = 0;
            $calc_pay_fee['point_discount'] = '0.00';
        }

        // 首单限制使用优惠
        $isFirstOrder = $this->app['order']->isFirstOrder($userId, $order->id);
        if ($isFirstOrder) {
            $calc_pay_fee['can_use_point'] = 0;
            $calc_pay_fee['can_use_point_discount'] = '0.00';
            $calc_pay_fee['use_point'] = 0;
            $calc_pay_fee['point_discount'] = '0.00';
            $calc_pay_fee['can_use_balance'] = '0.00';
            $calc_pay_fee['balance_discount'] = '0.00';
        }
        return $this->app['jsend']->success([
            'calc_pay_fee' => $calc_pay_fee,
            'is_show_first_order_product' => $is_show_first_order_product
        ]);
    }
}
