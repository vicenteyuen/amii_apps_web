<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class TribeController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }


    /**
     * 部落详情
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index($provider, Request $request)
    {
        $data = $this->app['tribe']->index($provider, $this->user_id);
        $tip = '*邀请好友成为自己一级部落成员,可获得好友消费金额15%返佣！好友再邀请的用户将成为自己的二级部落成员，可获得二级部落成员消费金额5%返佣！
2017年7月31日前消费的用户还可额外获得年度奖励一级部落5%，二级部落5%！';
        if ($data !== false) {
            return $this->app['jsend']->success(['tip' => $tip,'tribe' => $data]);
        }
        return $this->app['jsend']->error('数据获取失败');
    }

    /**
     * 收益明细
     * @return [type] [description]
     */
    public function detail()
    {
        $data = $this->app['tribe']->detail($this->user_id);
        if ($data) {
            $cumulateProfit = $this->app['tribe']->profit($this->user_id);
            return $this->app['jsend']->success(['detail' => $data,'cumulate_profit' => $cumulateProfit ?:'0.00']);
        }
        return $this->app['jsend']->error('数据获取失败');
    }

    /**
     * 部落首页
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show(Request $request)
    {
        $data = $this->app['tribe']->show($this->user_id);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('数据获取失败');
    }
    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pid' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('10001');
        }

        $arr = [
            'pid'        => $request->pid,
            'user_id'    => $this->user_id,
            'income'     => 0
        ];

        $data = $this->app['tribe']->store($arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('新增失败');

    }
}
