<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class UserScanLogController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }

     /**
     * 浏览列表
     * @return json
     */
    public function index()
    {
        
        $datas = $this->app['scan']->show($this->user_id);
        if ($datas) {
             return $this->app['jsend']->success(['scan' =>$datas]);
        }
        return $this->app['jsend']->error('暂无浏览记录');
    }

    /**
     * 增加浏览记录
     * @param  Request $request [description]
     * @return json
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sell_product_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'sell_product_id' => $request->sell_product_id,
            'user_id'    => $this->user_id,
        ];

        $data = $this->app['scan']->store($arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('新增浏览记录失败');
    }

    /**
     * 批量删除
     * @return [type] [description]
     */
    public function deleteAll(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $data = $this->app['scan']->deleteAll($request->ids);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');

    }
    /**
     * 删除浏览记录
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $delete = $this->app['scan']->delete($id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除浏览记录失败');

    }
}
