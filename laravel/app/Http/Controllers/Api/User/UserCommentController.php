<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Gimage\Gimage;
use App\Helpers\ImageHelper;

class UserCommentController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('api.not.check.auth', ['only' => 'index']);
    }

    /**
     * 获取评价添加页数据
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }

        // 获取订单商品信息
        $data = $this->app['order']->order4Comment(auth()->id(), $request->orderSn);
        if (! $data) {
            return $this->app['jsend']->error('获取订单数据失败');
        }
        return $this->app['jsend']->success(['products' => $data]);
    }

    /**
     * 评论列表
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request, $id)
    {
        $validator = Validator::make(['id' => $id], [
            'id'   => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $data = $this->app['userComment']->index($id);

        if ($data !== false) {
            return $this->app['jsend']->success(['comment'=>$data]);
        }
        return $this->app['jsend']->error('获取评论列表失败');
    }

    /**
     * 新增评论
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'  => 'required',
            'comments' => 'required|array',
            'comments.*.inventoryId' => 'required',
            'comments.*.images' => 'array',
            'comments.*.content' => '',
            'comments.*.comment_rank_id' => 'required',

        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        $store = $this->app['userComment']->store(auth()->id(), $request);

        if ($store['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($store['message']);
    }

    /**
     * 上传评价图片
     * @return [type] [description]
     */
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment_image'       => 'required|image',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $file  = $request->file('comment_image');
        $storeFile = Gimage::uploadImage($file, 'product', 'images');
        if ($storeFile instanceof \Exception) {
            return $this->app['jsend']->error('上传图片失败');
        }
        $data['image_name'] = $storeFile;
        $data['image_url'] =  ImageHelper::getImageUrl($data['image_name'], 'product');
        return $this->app['jsend']->success($data);
    }
    /**
     * 删除评论
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request, $id)
    {
        $delete = $this->app['userComment']->destroy($id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除评论失败');
    }

    // 获取弹出评论
    public function popup($id)
    {
        $validator = Validator::make(['id' => $id], [
            'id'   => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $data = $this->app['userComment']->popup($id);

        if ($data !== false) {
            return $this->app['jsend']->success(['comment'=>$data]);
        }
        return $this->app['jsend']->error('获取弹出评论列表失败');
    }

}
