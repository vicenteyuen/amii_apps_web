<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class PointLevelController extends Controller
{
    public function index()
    {
        $levels = $this->app['pointLevel']->index();
        return $this->app['jsend']->success($levels);
    }

    public function level()
    {
        $userId = auth()->id();
        for ($i=2; $i <= 11; $i++) {
            $data[] = $this->app['user']->userLevelEnable($userId, $i);
        }
        return $this->app['jsend']->success($data);
    }
}
