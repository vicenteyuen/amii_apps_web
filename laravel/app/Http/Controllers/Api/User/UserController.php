<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Gimage\Gimage;
use Validator;
use Cache;
use App\Helpers\SensitiveLimitHelper;

class UserController extends Controller
{
    protected $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->user_id = auth()->id();
    }

    /**
     * 返回个人基本信息
     * @return [type] json
     */
    public function index()
    {
        $detail = $this->app['user']->show($this->user_id);
        if (!is_null($detail)) {
            $detail->default_address = app('address')->default($detail->id);
            $detail->playing_game = app('amii.user.game')->checkUserPlaying();
            return $this->app['jsend']->success($detail);
        }
        return $this->app['jsend']->error('基本信息查询出错');

    }

    /**
     * 修改头像
     * @return [type] [description]
     */
    public function avatar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar'       => 'required|image',
        ], [
            'required' => ':attribute不能为空！',
            'image' => ':attribute格式不正确',
        ], [
            'avatar' => '图片',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $file  = $request->file('avatar');
        $avatar = Gimage::uploadImage($file, 'avatar', 'images');
        if ($avatar instanceof \Exception) {
            return $this->app['jsend']->error('上传图片失败');
        }

        $update = $this->app['user']->update(compact('avatar'), $this->user_id);
        if ($update) {
            return $this->app['jsend']->success($update['data']);
        }
        return $this->app['jsend']->error($update['msg']);
    }

    /**
     * 修改密码
     * @param  Request $request [description]
     * @return json           [description]
     */
    public function password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'oldPassword'       =>   'required|string',
            'newPassword'       =>   'required|string',
            'confirmPassword'   =>   'required|string|min:6|max:20',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $oldPassword     = $request->oldPassword;
        $newPassword     = $request->newPassword;
        $confirmPassword = $request->confirmPassword;

        $id              =  auth()->user()->id;
        $password        =  auth()->user()->password;
        $id              = $request->id;
        $password        = $request->password;

        if ($confirmPassword !== $newPassword) {
            return $this->app['jsend']->error('两次输入的密码不一致');
        }

        if (!password_verify($oldPassword, $password)) {
             return $this->app['jsend']->error('密码输入错误');
        }


        $arr = [
            'password' => bcrypt($newPassword),
        ];

        $update = $this->app['user']->update($arr, $id);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('修改密码失败');
    }

    /**
     * 基本信息修改
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nickname'  => 'string',
            'gender'    => 'integer',
            'birthday'  => 'string',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        if ($request->nickname) {
            $limit = SensitiveLimitHelper::limit($request->nickname);
            if (!$limit) {
                 return $this->app['jsend']->error('昵称只允许输入中文、英文字母和数字且不能包括与本公司相关的汉字英文');
            }
            $arr['nickname'] = $request->nickname;
        } elseif (isset($request->gender)) {
            $arr['gender'] = $request->gender;
        } else if ($request->birthday) {
            $arr['birthday'] = $request->birthday;
        } else {
            return $this->app['jsend']->error('修改基本信息失败');
        }
        $update = $this->app['user']->update($arr, $this->user_id);
        if ($update['status']) {
            return $this->app['jsend']->success($update['data']);
        }
        return $this->app['jsend']->error($update['msg']);

    }

    /**
     * 获取二维码
     * @return [type] [description]
     */
    public function qrcode(Request $request)
    {
        $qrcode = $this->app['user']->qrcode($request->provider, $this->user_id);
        if ($qrcode) {
            $user = $this->app['user']->member($this->user_id);
            // 分享链接
            $shareUrl = url('/wechat') . '/my/share?code=' . $user->uuid;
            return $this->app['jsend']->success(compact('qrcode', 'user', 'shareUrl'));
        }
        return $this->app['jsend']->error('获取二维码失败');
    }
    
    /**
     * 会员中心首页数据
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function member(Request $request)
    {
        $data = $this->app['user']->member($this->user_id, $request);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('获取信息失败');
    }

    /**
     * 更换手机号校验旧手机验证码
     */
    public function checkCurrentPhoneCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'code'      => 'required',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数错误');
        }

        $phone = (string)$request->phone;
        $code = (string)$request->code;

        $user = $this->app['user']
            ->findWhere([
                'phone' => $phone,
            ]);
        if (! $user) {
            return $this->app['jsend']->error('手机号未注册', 500001);
        }
        // 校验验证码
        if (! $this->app->make('leancloud', ['sms'])->verifySmsCode($phone, $code)) {
            return $this->app['jsend']->error('验证码错误');
        }

        $encryptData = $phone . $code;
        $digest = hash_hmac("sha512", $encryptData, $code, false);
        $cacheKey = 'update_phone_code' . $phone;
        $cacheValue = substr($digest, 0, 32);
        Cache::put($cacheKey, $cacheValue, 10);

        return $this->app['jsend']->success();
    }

    /**
     * 更换手机
     */
    public function updatePhone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'oldPhone'  => 'required|phone',
            'oldCode'   => 'required',
            'code'      => 'required',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数错误');
        }

        $userId = auth()->id();
        $oldPhone = (string)$request->oldPhone;
        $newPhone = (string)$request->phone;
        $oldCode = (string)$request->oldCode;
        $code = (string)$request->code;

        // 检证手机是否已注册过
        $exits = $this->app['user']->findUserByPhone($newPhone);
        if ($exits) {
            return $this->app['jsend']->error('当前手机已注册，请更换未注册的手机');
        }
        // 校验
        $encryptData = $oldPhone . $oldCode;
        $digest = hash_hmac("sha512", $encryptData, $oldCode, false);
        $cacheKey = 'update_phone_code' . $oldPhone;
        $checkValue = substr($digest, 0, 32);
        if (Cache::get($cacheKey) != $checkValue) {
            return $this->app['jsend']->error('验证失败');
        } else {
            Cache::forget($cacheKey);
        }

        // 校验验证码
        if (! $this->app->make('leancloud', ['sms'])->verifySmsCode($newPhone, $code)) {
            return $this->app['jsend']->error('验证码错误');
        }

        $user = $this->app['user']
            ->findWhere([
                'id' => $userId,
                'phone' => $oldPhone
            ]);
        if (! $user) {
            return $this->app['jsend']->error('当前登录用户错误');
        }

        $updatePhone = $this->app['user']->updatePhone($user, $newPhone);
        if (! $updatePhone) {
            return $this->app['jsend']->error('修改手机号失败');
        }
        return $this->app['jsend']->success();
    }

    /**
     * 三方登录绑定手机
     */
    public function bundPhone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'code'      => 'required',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数错误');
        }

        $userId = auth()->id();
        $phone = (string)$request->phone;
        $code = (string)$request->code;

        // 校验手机验证码
        if (! $this->app->make('leancloud', ['sms'])->verifySmsCode($phone, $code)) {
            return $this->app['jsend']->error('验证码错误');
        }

        $bundPhone = $this->app['user']->bundPhone($userId, $phone);
        if (! $bundPhone['status']) {
            return $this->app['jsend']->error($bundPhone['message']);
        }
        return $this->app['jsend']->success(['token' => $bundPhone['data']]);
    }

    /**
     * 获取余额
     * @return [type] [description]
     */
    public function balance()
    {
        $user = $this->app['user']->show($this->user_id);
        if (! $user) {
            return $this->app['jsend']->error('余额获取失败');
        }
        $cumulateProfit = $user->cumulate_profit;
        $returnMoney =  $user->return_money;
        return $this->app['jsend']->success(compact('cumulateProfit', 'returnMoney'));
    }

    // 通过uuid 查找user
    public function findByUuid($uuid)
    {
        $user = $this->app['user']->findByUuid($uuid);
        return $this->app['jsend']->success($user);
    }
}
