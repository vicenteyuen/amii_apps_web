<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use Validator;

use App\Http\Controllers\Controller;

class PaymentPasswordController extends Controller
{
    /**
     * 校验短信验证码
     */
    public function postCode(Request $request)
    {
        $user = auth()->user();

        Validator::extend('sms_code_check', function ($attribute, $value, $parameters) use ($user) {
            return $this->app->make('leancloud', ['sms'])->verifySmsCode($user->phone, $value);
        }, '短信验证码错误');

        $phone = $user->phone;
        $code = $request->code;

        $encryptData = $phone . $code;
        $digest = hash_hmac("sha512", $encryptData, $code, false);
        $cacheKey = 'update_payment_check' . $phone;
        $cacheValue = substr($digest, 0, 32);
        \Cache::put($cacheKey, $cacheValue, 10);

        return $this->app['jsend']->success();
    }

    public function postReset(Request $request)
    {
        $user = auth()->user();
        Validator::extend('payment_password_check', function ($attribute, $value, $parameters) use ($user) {
            if (!$user) abort(401, 'Unauthorized');
            return $user->checkPaymentPassword($value);
        }, '旧提现密码错误');

        $rules = [
            'new_password' => 'required',
        ];
        if ($user->payment_password) {
            $rules = array_merge($rules, [
                'old_password' => 'required_without:code|payment_password_check',
                'code' => 'required_without:old_password',

            ]);
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return app('jsend')->error($validator->errors()->first());
        }

        if ($request->code) {

            $phone = (string)$user->phone;
            $code = (string)$request->code;

            // 校验
            $encryptData = $phone . $code;
            $digest = hash_hmac("sha512", $encryptData, $code, false);
            $cacheKey = 'update_payment_check' . $phone;
            $checkValue = substr($digest, 0, 32);
            if(!env('APP_DEBUG')){
                if (\Cache::get($cacheKey) != $checkValue) {
                    return $this->app['jsend']->error('验证失败');
                } else {
                    \Cache::forget($cacheKey);
                }
            }
        }

        $user->payment_password = $request->new_password;
        $user->save();

        return $this->app['jsend']->success();
    }
}
