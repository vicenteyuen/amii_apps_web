<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HotSearchController extends Controller
{
    /**
     * 热门搜索
     * @return [type] [description]
     */
    public function hot()
    {
        $data = $this->app['hotSearch']->hot();
        return $this->app['jsend']->success($data);
    }
}
