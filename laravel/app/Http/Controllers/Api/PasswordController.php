<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;
use Cache;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    /**
     * check forget password code
     */
    public function forgetPsdCodeCheck(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'code'      => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;
        $code = (string)$request->code;

        $user = $this->app['user']
            ->findWhere([
                'phone' => $phone,
            ]);
        if (! $user) {
            return $this->app['jsend']->error('手机号未注册', 500001);
        }
        // 校验验证码
        if (! $this->app->make('leancloud', ['sms'])->verifySmsCode($phone, $code)) {
            return $this->app['jsend']->error('验证码错误');
        }

        $time = substr(time(), -6);
        $cacheKey = $phone . $code . $time;
        Cache::put($cacheKey, bcrypt($code), 10);

        return $this->app['jsend']->success(['check' => $time]);
    }

    /**
     * forget password
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'code'      => 'required',
            'check'     => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;
        $code = (string)$request->code;
        $password = (string)$request->password;

        // 校验
        $time = (string)$request->check;
        $cacheKey = $phone . $code . $time;
        if (! \Hash::check($request->code, Cache::get($cacheKey))) {
            return $this->app['jsend']->error('验证失败');
        }

        $user = $this->app['user']
            ->findWhere([
                'phone' => $phone,
            ]);
        if (! $user) {
            return $this->app['jsend']->error('手机号未注册');
        }

        if ($this->app['user']->updatePassword($user, $password)) {
            // 重置token
            if ($token = $this->app['token']->resetAllToken($user->id)) {
                // logout
                \Auth::logout();
                return $this->app['jsend']->success(compact('token'));
            }
        }
        return $this->app['jsend']->error('重置密码失败');
    }

    /**
     * reset password
     */
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'newpsd'    => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误', '10000', $errors);
        }

        $password = (string)$request->password;
        $user = auth()->user();
        // 校验密码
        if (! $user->checkPassword($password)) {
            return $this->app['jsend']->error('原密码错误');
        }
        // 是否大于6位
        if (strlen($request->newpsd) < 6) {
            return $this->app['jsend']->error('密码长度不能小于6位');
        }
         // 是否包含有数字和字母
        if (!preg_match('/[0-9]+/', $request->newpsd) || !preg_match('/[A-Za-z]+/', $request->newpsd)) {
             return $this->app['jsend']->error('密码应该包含数字和字母');
        }
        if ($this->app['user']->updatePassword($user, $request->newpsd)) {
            // 重置token
            if ($token = $this->app['token']->resetAllToken($user->id)) {
                // logout
                auth('web')->logout();
                return $this->app['jsend']->success(compact('token'));
            }
        }
        return $this->app['jsend']->error('修改密码失败');
    }
}
