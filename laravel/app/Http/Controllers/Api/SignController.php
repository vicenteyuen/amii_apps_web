<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;

use App\Http\Controllers\Controller;

class SignController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'code'      => 'required',
            'password'  => 'required'
        ], [
            'code.required'  => ':attribute不能为空！',
            'password.required'  => ':attribute不能为空！',
        ], [
            'code' => '验证码',
            'password' => '密码'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;
        $code = (string)$request->code;

        // 校验手机验证码
        $checkSmsCode = $this->app->make('leancloud', ['sms'])->verifySmsCode($phone, $code);
        if (! $checkSmsCode) {
            return $this->app['jsend']->error('短信验证码错误');
        }

        // 校验用户是否注册
        $checkUser = $this->app['user']
            ->findWhere(['phone' => $phone]);
        if ($checkUser) {
            return app('jsend')->error('用户已注册', '10001');
        }

        // store data
        $store = $this->app['user']
            ->store([
                'phone' => $phone,
                'password'  => bcrypt((string)$request->password)
            ]);

        if (! $store) {
            return app('jsend')->error('新建用户失败', '10002');
        }
        auth()->loginUsingId($store->id);
        // create token
        $token = $this->app['token']->createAndExpires($store->id);
        // 更新积分等级
        $this->app['pointReceived']->userAddPoint('register', 'register', null, $store->id);

        $uuid = $request->uuid;
        if (!$uuid && \Session::has('share_uuid')) {
            $uuid = \Session::get('share_uuid');
        }
        if ($uuid) {
             // 通过邀请加积分
            app('user')->addPointByInvente($uuid);
            // 通过邀请绑定用户
            app('gain')->binding($uuid, $store->id);
        }
        return app('jsend')->success(compact('token'));
    }

    // 游戏注册
    public function gameSign(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'code'      => 'required',
        ], [
            'code.required'  => ':attribute不能为空！',
        ], [
            'code' => '验证码',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;
        $code = (string)$request->code;

        // 校验手机验证码
        $checkSmsCode = $this->app->make('leancloud', ['sms'])->verifySmsCode($phone, $code);
        if (! $checkSmsCode) {
            return $this->app['jsend']->error('短信验证码错误');
        }

        // 校验用户是否注册
        $checkUser = $this->app['user']
            ->findWhere(['phone' => $phone]);
        if ($checkUser) {
            $store = $checkUser;
            $store->user_game_id = $request->user_game_id;
            $store->save();
        } else {
        // 新建用户
            $store = $this->app['user']
            ->store([
                'phone' => $phone,
                'user_game_id' => $request->user_game_id,
            ]);
        }
        if (! $store) {
            return app('jsend')->error('新建用户失败', '10002');
        }
        auth()->loginUsingId($store->id);
        // create token
        $token = $this->app['token']->createAndExpires($store->id);
        // 更新积分等级
        $this->app['pointReceived']->userAddPoint('register', 'register', null, $store->id);
        if ($request->uuid && !$checkUser) {
             // 送优惠卷
            app('coupon')->invateCoupon($request->uuid, $store->id);
            app('user')->addPointByInvente($request->uuid);  //通过邀请加积分
            app('gain')->binding($request->uuid, $store->id);
        }
        if (!$checkUser) {
            app('user')->createPassword($store->id);//发送密码
        }
        return app('jsend')->success(compact('token'));
    }

    // 邀请注册
    public function inviteSign(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'code'      => 'required',
        ], [
            'code.required'  => ':attribute不能为空！',
        ], [
            'code' => '验证码',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;
        $code = (string)$request->code;

        // 校验用户是否注册
        $checkUser = $this->app['user']
            ->findWhere(['phone' => $phone]);
        if ($checkUser) {
            return app('jsend')->error('用户已注册', '10001');
        }

         // 校验手机验证码
        $checkSmsCode = $this->app->make('leancloud', ['sms'])->verifySmsCode($phone, $code);
        if (! $checkSmsCode) {
            return $this->app['jsend']->error('短信验证码错误');
        }

        if (!$request->password) {
            return app('jsend')->error('密码不能为空', '10000');
        }
        // store data
        $store = $this->app['user']
            ->store([
                'phone' => $phone,
                'password' => bcrypt($request->password),
            ]);

        if (! $store) {
            return app('jsend')->error('新建用户失败', '10002');
        }
        auth()->loginUsingId($store->id);
        // create token
        $token = $this->app['token']->createAndExpires($store->id);
        // 更新积分等级
        $this->app['pointReceived']->userAddPoint('register', 'register', null, $store->id);

        $uuid = $request->uuid;
        if ($uuid) {
            // 通过邀请加积分
            app('user')->addPointByInvente($uuid);
            // 通过邀请绑定用户
            app('gain')->binding($uuid, $store->id);
            // 送优惠卷
            $coupon = app('coupon')->invateCoupon($uuid, $store->id);
            // 向新注册用户推送消息
            $data = [
                'user_id' => $store->id,
                'coupon_desc' => $coupon['invitedUserCouponTitle'],
            ];
            app('amii.notify.message')->sendNotifyMsg('notify', 'coupons', $data);
            // 向邀请的用户推送消息
            $userId = app('user')->getUserId($uuid);
            $data = [
                'user_id' => $userId,
                'coupon_desc' => $coupon['inviteUserCouponTitle'],
            ];
            app('amii.notify.message')->sendNotifyMsg('notify', 'coupons', $data);
        }
        // app('user')->createPassword($store->id);//发送密码
        return app('jsend')->success(compact('token'));
    }
}
