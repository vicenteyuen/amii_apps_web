<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class SmsController extends Controller
{
    /**
     * sms provider
     */
    private static $sms = [
        // 注册
        'register',
        // 随机密码登录
        'login',
        // 忘记密码重置密码
        'forget',
    ];

    /**
     * config auth sms provider
     */
    private static $authSms = [
        // 更换手机 旧手机获取
        'updateOld',
        // 更换手机 新手机获取
        'updateNew',
        // 重置支付密码获取验证码
        'paymentPassword',
        // 提现验证码
        'withdraw',
    ];

    /**
     * config auth third sms provider
     */
    private static $thirdSms = [
        // 获取绑定手机号验证码
        'bund',
    ];

    /**
     * send sms code not check auth
     */
    public function sendCode(Request $request)
    {
        $provider = (string)$request->provider;
        if (! in_array($provider, static::$sms)) {
            return $this->app['jsend']->error('非开放请求');
        }

        return self::validatorSendCode($request);
    }

    /**
     * send sms code check auth
     */
    public function authSendCode(Request $request)
    {
        $provider = (string)$request->provider;
        if (! in_array($provider, static::$authSms)) {
            return $this->app['jsend']->error('非开放请求');
        }

        return self::validatorSendCode($request);
    }

    /**
     * send sms code check auth
     */
    public function thirdSendCode(Request $request)
    {
        $provider = (string)$request->provider;
        if (! in_array($provider, static::$thirdSms)) {
            return $this->app['jsend']->error('非开放请求');
        }

        return self::validatorSendCode($request);
    }

    /**
     * validator and send sms code
     */
    private function validatorSendCode(Request $request)
    {
        if (! in_array($request->provider, ['withdraw'])) {
            $validator = Validator::make($request->all(), [
                'phone'     => 'required|phone'
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                $errorMsg = '参数错误';

                if (preg_match('/^170/', $request->phone)) {
                    $errorMsg = '暂不支持170字段的手机号码注册';
                }
                if (preg_match('/^171/', $request->phone)) {
                    $errorMsg = '暂不支持171字段的手机号码注册';
                }
                if (preg_match('/^152/', $request->phone)) {
                    $errorMsg = '暂不支持152字段的手机号码注册';
                }
                if (preg_match('/^156/', $request->phone)) {
                    $errorMsg = '暂不支持156字段的手机号码注册';
                }

                return app('jsend')->error($errorMsg, '10000', $errors);
            }
            $phone = (string)$request->phone;
        } else {
            $phone = auth()->user()->phone;
            if (! $phone) {
                return app('jsend')->error('请先绑定手机号');
            }
        }

        //notCheckUser 不用检查手机号是否注册
        $response = self::provider($phone, $request->provider, $request->notCheckUser);

        if ($response) {
            return $response;
        }

        if (self::sendSmsCode($phone)) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('发送验证码失败');
    }

    /**
     * sendSmsCode
     */
    private function sendSmsCode($phone = '')
    {
        return $this->app->make('leancloud', ['sms'])->requestSmsCode($phone);
    }

    /**
     * send sms code provider
     */
    private function provider($phone, $provider, $notCheckUser = null)
    {
        $response = null;
        switch ($provider) {
            case 'register':
                // 校验手机号是否注册
                $checkUser = self::checkUser($phone);
                //获取验证码不用验证手机号
                if ($notCheckUser) {
                    break;
                }
                if ($checkUser) {
                    $response = app('jsend')->error('手机号已注册');
                }
                break;
            case 'login':
                // 校验手机号是否注册
                $checkUser = self::checkUser($phone);
                if (! $checkUser) {
                    $response = app('jsend')->error('手机号未注册', 500001);
                }
                break;
            case 'forget':
                // 校验手机号是否注册
                $checkUser = self::checkUser($phone);
                if (! $checkUser) {
                    $response = app('jsend')->error('手机号未注册', 500001);
                }
                break;
            case 'updateOld':
                // 更换手机号 旧手机号获取验证码
                $checkUser = self::checkUser($phone);
                if (! $checkUser) {
                    $response = app('jsend')->error('手机号未注册', 500001);
                }
                break;
            case 'updateNew':
                // 更换手机号 旧手机号获取验证码
                $checkUser = self::checkUser($phone);
                if ($checkUser) {
                    $response = app('jsend')->error('手机号已注册');
                }
                break;
            case 'bund':
                // 三方登录绑定手机号 不验证手机号是否注册 若已注册，则绑定当前三方账号到此手机号
                $response = null;
                break;
            case 'paymentPassword':
                // 重置支付密码获取验证码,判断手机号码是否当前号码
                if (auth()->user()->phone != $phone) {
                    $response = app('jsend')->error('手机号不正确');
                }
                break;
            case 'withdraw':
                // 提现
                $response = null;
                break;

            default:
                # code...
                break;
        }
        return $response;
    }

    /**
     * check user
     */
    private function checkUser($phone)
    {
        return $this->app['user']
            ->findWhere([
                'phone' => $phone,
            ]);
    }
}
