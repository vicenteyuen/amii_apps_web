<?php

namespace App\Http\Controllers\Api\Community;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhysicalStoreController extends Controller
{
    public function index(Request $request)
    {
        if ($request->cityName && $request->longitude && $request->latitude) {
            $stores = $this->app['physicalStore']->indexOrderByDistance(
                $request->cityName,
                $request->latitude,
                $request->longitude,
                $request->is_paginate,
                $request->is_all
            );
        } else if ($request->cityName) {
            $stores =  $this->app['physicalStore']->indexOrderCityName($request->cityName);
        } else {
            $stores = $this->app['physicalStore']->indexOrderByInitial();
        }

        return $this->app['jsend']->success(compact('stores'));
    }

    public function show($id)
    {
        $store = $this->app['physicalStore']->find($id);

        if ($store) {
            return $this->app['jsend']->success($store);
        } else {
            return $this->app['jsend']->error('获取实体店信息失败，请刷新重试');
        }
    }
}
