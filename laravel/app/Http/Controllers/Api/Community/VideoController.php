<?php

namespace App\Http\Controllers\Api\Community;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('api.not.check.auth', ['only' => [
            'show',
            'addLike',
            'removeLike',
        ]]);
    }

    /**
     * 首页
     */
    public function home()
    {
        $videos = $this->app['video']->homeIndex();

        return $this->app['jsend']->success($videos);
    }

    /**
     * 视频列表
     */
    public function index(Request $request)
    {
        // hot new
        $type = $request->type;
        switch ($type) {
            case 'new':
                $data = $this->app['video']->newIndex();
                break;
            default;
                $data = $this->app['video']->hotIndex();
                break;
        }

        return $this->app['jsend']->success($data);
    }

    public function show($id)
    {
        $video = $this->app['video']->find($id);

        if (auth()->check()) {
            $video->is_like = $this->app['video']->relateLike($id) ? 1 : 0;
        }

        return $this->app['jsend']->success($video);
    }

    public function addLike($video_id)
    {
        $this->app['video']->addLike($video_id);

        return $this->app['jsend']->success();
    }

    public function removeLike($video_id)
    {
        $this->app['video']->removeLike($video_id);

        return $this->app['jsend']->success();
    }
}
