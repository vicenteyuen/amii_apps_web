<?php

namespace App\Http\Controllers\Api\Community;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class SubjectController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('api.not.check.auth', ['only' => [
            'show',
        ]]);
        $this->middleware('api.auth', ['only' => [
            'mark',
        ]]);
    }

    public function index()
    {
        $subjects = $this->app['subject']->index();
        return $this->app['jsend']->success(compact('subjects'));
    }

    public function show($id)
    {
        $this->app['subject']->addScan($id, auth()->id());
        $subject = $this->app['subject']->find($id);
        $subject->hadMark = $this->app['subject']->hadMark($id);
        //TODO::获取猜你喜欢商品
        if ($subject) {
            return $this->app['jsend']->success($subject);
        } else {
            return $this->app['jsend']->error('获取专题信息失败，请刷新重试');
        }
    }

    // 点赞或取消点赞
    public function mark(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject_id'         => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $subject = $this->app['subject']->mark($request->subject_id, auth()->id());
        if ($subject) {
            $subject = $this->app['subject']->find($request->subject_id);
            $subject->hadMark = $this->app['subject']->hadMark($request->subject_id);
            return $this->app['jsend']->success(['subject' => $subject]);
        } else {
            return $this->app['jsend']->error('fail');
        }
    }
}
