<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('api.not.check.auth', ['show','gameProductDetail']);
    }

    /**
     * 商品详情
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $id)
    {
        $provider = $request->provider ? :1;
        $data = $this->app['product']->productDetail($id, $provider);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('获取商品失败', 30001);
    }

    public function gameProductDetail($id)
    {
        $data = $this->app['product']->productGameDetail($id, 3);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('获取商品失败', 30001);
    }

    /**
     * product index
     */
    public function index(Request $request)
    {
        // 商品数据
        $products = $this->app['product']->index($request);

        // 筛选
        $filterData = $this->app['popular']->filterData($request->provider, $request->resource, $request->filter);
        if ($filterData) {
            $filter_image = $filterData['filter_image'];
            $filters = $filterData['filters'];
        } else {
            $filter_image = '';
            $filters = null;
        }

        return $this->app['jsend']->success(compact('filter_image', 'filters', 'products'));
    }
}
