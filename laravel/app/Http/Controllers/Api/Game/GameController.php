<?php

namespace App\Http\Controllers\Api\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class GameController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('api.not.check.auth', ['only' => [
            'activity',
        ]]);
    }
    // 创建订单
    public function store(Request $request)
    {
        $store = $this->app['amii.game.order']->store(auth()->id(), $request);
        return $store;
    }

    // 提交订单
    public function gameCommitOrder(Request $request)
    {
        $commit = $this->app['amii.game.order']->commitOrder(auth()->id(), $request);
        return $commit;
    }

    //已完成的订单
    public function gameOrder()
    {
        $orders = $this->app['amii.game.order']->gameOrder();
        return $this->app['jsend']->success(['orders' => $orders]);
    }

    // 首单信息
    public function activity()
    {
        $activity = \App\Services\GiveProductService::checkUserFirstProduct();
        return $this->app['jsend']->success($activity);
    }
}
