<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'password'  => 'required'
        ], [
            'password.required'  => ':attribute不能为空！',
        ], [
            'password' => '密码'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;
        $password = (string)$request->password;

        // 校验手机号
        $checkUser = $this->app['user']
            ->findWhere([
                'phone' => $phone,
            ]);
        if (! $checkUser) {
            return $this->app['jsend']->error('手机号未注册', 500001);
        }
        // 校验密码
        if (! $checkUser->checkPassword($password)) {
            return $this->app['jsend']->error('密码错误');
        }
        auth()->login($checkUser);

        // 更新token
        $token = $this->app['token']->createAndExpires($checkUser->id);

        // 是否微信小程序登录过
        $isWeapp = $this->app['socialite']->isWeapp($checkUser->id);

        // 注册用户激活游戏
        // $userGameId = $request->user_game_id;
        // $activite = 0;
        // if ($userGameId) {
        //     $data = [
        //         'user_id' => auth()->id(),
        //         'user_games_id' => $userGameId,
        //     ];
        //     $activite = app('amii.user.game')->relationUserAdd($data);
        //     $activite = $activite['status'];
        // }
        // 返回token
        return $this->app['jsend']->success(['token' => $token, 'is_weapp' => $isWeapp]);
        
    }

    /**
     * 随机密码登录
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function code(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|phone',
            'code'   => 'required'
        ], [
            'phone.required' => ':attribute不能为空！',
            'phone.phone'    => ':attribute格式不正确！',
            'code.required'  => ':attribute不能为空！',
        ], [
            'phone' => '手机号码',
            'code' => '验证码',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;
        $code = (string)$request->code;

        // 校验手机号是否注册
        $checkUser = $this->app['user']
            ->findWhere([
                'phone' => $phone,
            ]);
        if (! $checkUser) {
            return $this->app['jsend']->error('用户未注册', 500001);
        }

        // 校验短信验证码
        $checkSmsCode = $this->app->make('leancloud', ['sms'])->verifySmsCode($phone, $code);
        if (! $checkSmsCode) {
            return $this->app['jsend']->error('短信验证码错误');
        }

        // 更新token
        $token = $this->app['token']->createAndExpires($checkUser->id);

        // 是否微信小程序登录过
        $isWeapp = $this->app['socialite']->isWeapp($checkUser->id);

        // 返回token
        return $this->app['jsend']->success(['token' => $token, 'is_weapp' => $isWeapp]);
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        auth()->logout();

        return $this->app['jsend']->success();
    }
}
