<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * home
     */
    public function home()
    {
        $banners = $this->app['banner']->index();
        $populars = $this->app['popular']->index();
        return $this->app['jsend']->success(compact('banners', 'populars'));
    }
}
