<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QrcodeController extends Controller
{
    /**
     * 二维码扫描
     */
    public function show(Request $request)
    {
        $value = $request->value;
        if (! $value) {
            return $this->app['jsend']->error('未知');
        }

        // 数据处理
        if (strlen($value) == 13) {
            $value = substr($value, 0, 8);
        }

        $returnData = $this->app['amii.qrcode']->show($value);

        if ($returnData[0] !== 0) {
            $errorMsg = '暂无当前商品';
            if (isset($returnData[1])) {
                $errorMsg = $returnData[1];
            }
            return $this->app['jsend']->error($errorMsg);
        }
        return $this->app['jsend']->success(['qrcode' => $returnData[1]]);
    }
}
