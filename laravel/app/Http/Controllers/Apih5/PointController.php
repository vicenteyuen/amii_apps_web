<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{
    /**
     * 积分规则
     */
    public function rule()
    {
        return view('apih5.point.rule');
    }

    /**
     * 积分奖励
     */

    public function award()
    {
        return view('apih5.point.award');
    }
}
