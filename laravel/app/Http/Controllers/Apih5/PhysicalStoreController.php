<?php

namespace App\Http\Controllers\Apih5;

use App\Http\Controllers\Controller;

class PhysicalStoreController extends Controller
{
    public function show($id)
    {
        $physical_store = $this->app['physicalStore']->find($id);

        return view('apih5.physical_store.show', compact('physical_store'));
    }
}
