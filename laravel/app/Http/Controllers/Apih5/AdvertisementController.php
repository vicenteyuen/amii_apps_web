<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvertisementController extends Controller
{
    /**
     * 首单购详情
     */
    public function show()
    {
        return view('apih5.advertisement.advertisement');
    }
}
