<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalesController extends Controller
{
    /**
     * 分佣规则
     */
    public function rule()
    {
        return view('apih5.sales.rule');
    }
    /**
     * 分佣首页
     */
    public function home()
    {
        return view('apih5.sales.home');
    }
}
