<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    /**
     * 积分规则
     */
    public function rule()
    {
        return view('apih5.member.rule');
    }

    /**
     * 用户协议
     */
    public function policy()
    {
        return view('apih5.member.policy');
    }
}
