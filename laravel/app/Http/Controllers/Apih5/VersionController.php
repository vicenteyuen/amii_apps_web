<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VersionController extends Controller
{
    //二维码下载
    public function download($provider, Request $request)
    {
        // 更新下载量
        app('channel')->update($provider);
        $url = 'http://app.amii.com/?provider=' . $provider;
        return redirect($url);
    }

    //更新默认渠道二维码下载量
    public function updateDefaultDownload(Request $request)
    {
        // 更新下载量
        app('channel')->update('app.amii.com');
    }
}
