<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MoneyController extends Controller
{
    /**
     * 红包详情
     */
    public function detail()
    {
        return view('apih5.money.detail');
    }
}
