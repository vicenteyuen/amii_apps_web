<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    // 商品详情页
    public function show($id)
    {
        $productInfo = $this->app['product']->productInfo($id);

        return view('apih5.product.info', ['productInfo' => $productInfo]);
    }
}
