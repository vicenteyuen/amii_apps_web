<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use Validator;
use App\Models\Zhaoshang;
use App\Http\Controllers\Controller;

class ZhaoshangController extends Controller
{
    //存储用户加盟申请
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'phone' => 'required|phone',
            'email' => 'email',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数有误', '10000', $errors);
        }

        $data = [
            'name'  =>  $request->name,
            'phone' => (string)$request->phone,
            'email' =>  $request->email,
        ];

        Zhaoshang::Create($data);

        return app('jsend')->success(['加盟申请提交成功！']);
    }

}
