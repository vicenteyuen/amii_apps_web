<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityRuleController extends Controller
{
    public function show($id)
    {
        $data = $this->app['amii.activity.rule']->show($id);
        return view('apih5.sales.rule');
    }
}
