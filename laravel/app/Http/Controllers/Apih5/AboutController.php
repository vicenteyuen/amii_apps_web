<?php

namespace App\Http\Controllers\Apih5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
     * 关于我们
     */
    public function show()
    {
        return view('apih5.about.show');
    }
}
