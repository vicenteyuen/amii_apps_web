<?php

namespace App\Http\Controllers\Apih5;

use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    //专题详情
    public function show($id)
    {
        $subject = $this->app['subject']->find($id);
        return view('apih5.subject.content', compact('subject'));
    }
}
