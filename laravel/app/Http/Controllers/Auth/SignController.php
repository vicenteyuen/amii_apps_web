<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\BaseController as Controller;
use Validator;

class SignController extends Controller
{
    /**
     * get sign view
     */
    public function create()
    {
        switch ($this->guard) {
            case 'web':
                $view = 'web.account.sign';
                break;
            case 'mobile':
                $view = 'mobile.account.sign';
                break;
            case 'admin':
                $view = null;
                break;

            default:
                $view = null;
                break;
        }

        if ($view) {
            return view($view);
        }
        return;
    }

    /**
     * post sign data
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'     => 'required|phone',
            'password'  => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->authResponseError('参数错误', '10000', $errors);
        }

        $phone = (string)$request->phone;

        // 校验手机验证码

        // 校验用户是否注册
        $checkUser = $this->app['user']
            ->findWhere(['phone' => $phone])
            ->first();
        if ($checkUser) {
            return $this->authResponseError('用户已注册', '10001');
        }

        // store data
        $store = $this->app['user']
            ->create([
                'phone' => $phone,
                'password'  => bcrypt((string)$request->password)
            ]);

        if (! $store) {
            return $this->authResponseError('新建用户失败', '10002');
        }

        if ($this->isAjax) {
            return $this->app['jsend']->success();
        }
        return ;
    }
}
