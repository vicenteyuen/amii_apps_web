<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Auth\BaseController as Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Illuminate\Foundation\Auth\ThrottlesLogins;

class LoginController extends Controller
{
    // use AuthenticatesUsers, ThrottlesLogins;
    use AuthenticatesUsers;

    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->middleware('guest', ['except' => 'logout']);
    }

    // get login view
    public function create(Request $request)
    {
        switch ($request->provider) {
            case 'mobile':
                $view = 'web.accout.login';
                break;
            case 'api':
                $view = null;
                break;
            case 'admin':
                $view = 'admin.accout.login';
                break;

            default:
                $view = 'web.accout.login';
                break;
        }

        if ($view) {
            // return view($view);
            return;
        }
        return;
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = $this->validateLogin($request);
        if (! $validator) {
            return '验证失败';
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            // login too many
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (\Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            // login
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $rules = [];
        switch ($this->guard) {
            case 'admin':
                $rules = [
                    'email'     => 'required',
                    'password'  => 'required',
                ];
                break;

            default:
                $rules = [
                    'phone'     => 'required',
                    'password'  => 'required',
                ];
                break;
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    // logout
    public function logout(Request $request)
    {
        dd('post logout user');
        return;
    }

    protected function getGuard()
    {
        return $this->guard;
    }
}
