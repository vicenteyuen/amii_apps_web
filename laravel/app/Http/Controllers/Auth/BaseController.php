<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

abstract class BaseController extends Controller
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * auth guard
     */
    protected $guard;

    public function __construct(Request $request)
    {
        parent::__construct();

        switch ($request->provider) {
            case 'admin':
                $this->setAuthDefaultDriver('admin');
                break;

            case 'mobile':
                $this->setAuthDefaultDriver('mobile');
                break;

            case 'api':
                $this->setAuthDefaultDriver('api');
                break;

            default:
                $this->setAuthDefaultDriver('web');
                break;
        }
    }

    /**
     * set auth default driver and guard
     * @param string $provider
     */
    protected function setAuthDefaultDriver($provider = 'web')
    {
        \Auth::setDefaultDriver($provider);
        $this->guard = $provider;
    }

    /**
     * auth response
     * @param string $errorMsg
     * @param int $errorCode,
     */
    protected function authResponseError($errorMsg, $errorCode = null, $errorData = null)
    {
        if ($this->guard == 'api') {
            return app('jsend')->error($errorMsg, $errorCode, $errorData);
        }

        return back();
    }
}
