<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderProductRefundImageController extends Controller
{
    public function show($id)
    {
        $data = $this->app['logImage']->show($id);
        return "<img src='$data'>";
    }
}
