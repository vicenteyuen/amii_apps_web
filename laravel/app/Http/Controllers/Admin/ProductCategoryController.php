<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ProductCategoryController extends Controller
{
    /**
     * 分类品牌商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function categoryShow(Request $request)
    {
        $category_id = $request->category_id;
        $brand_id = $request->brand_id;
        $brand_category = ['category_id' => $category_id,  'brand_id' => $brand_id];
        //判断显示制定品牌分类
        if ($category_id &&  $brand_id) {
            $is_showOne = 1;
        }
        $brand   = $this->app['brand']->showAll();

        return view('admin.category.create', compact('category', 'brand', 'brand_category', 'is_showOne'));
    }

    /**
     * 分类商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function categoryProductShow(Request $request)
    {
        $category = $this->app['category']->index();
        return view('admin.category.create-category-product', compact('category'));
    }

    /**
     * 搜索品牌分类商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function search(Request $request)
    {
        $categoryId = $request->category_id;
        $brandId = $request->brand_id;
        // 判断是否是搜寻指定品牌分类
        if ($request->is_showOne) {
            $brand_category = ['category_id' => $categoryId,  'brand_id' => $brandId];
            $is_showOne = 1;
        }

        $brand   = $this->app['brand']->showAll();
        $products = $this->app['product']->search($request->key, $brandId, $categoryId, $request, 100);
        return view('admin.category.create', compact('products', 'brandId', 'categoryId', 'brand', 'brand_category', 'is_showOne'));
    }

    /**
     * 搜索分类商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function searchCategoryProduct(Request $request)
    {
        $categorySelected = $request->category_id;
        $products = $this->app['productCategory']->searchCategoryProduct($request->key, $request->category_id, $request, 100);
        $category = $this->app['category']->index();

        return view('admin.category.create-category-product', compact('products', 'category', 'categorySelected'));
    }

    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id'   => 'required|integer',
            'category_id'  => 'required|integer',
            'brand_id'     => 'required|integer',
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $arr =[
            'product_id'  => $request->product_id,
            'category_id' => $request->category_id,
            'brand_id'    => $request->brand_id,
        ];

        $arr1 =[
            'product_id'  => $request->product_id,
            'category_id' => $request->category_id,
            'brand_id'    => 0,
        ];

        $data = $this->app['productCategory']->store($arr, $arr1);
        if ($data['status']) {
             return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error($data['msg']);
    }

    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function storeWithoutBrand(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id'   => 'required|integer',
            'category_id'  => 'required|integer',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $arr =[
            'product_id'  => $request->product_id,
            'category_id' => $request->category_id,
            'brand_id'    => 0,
        ];

        $data = $this->app['productCategory']->storeWithoutBrand($arr);
        if ($data) {
             return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('添加失败');
    }

    /**
     * 删除
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        $data = $this->app['productCategory']->delete($request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');

    }

    public function deleteAll(Request $request)
    {
        $data = $this->app['productCategory']->deleteAll($request->ids);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');

    }

    // 分类商品
    public function categoryProduct($id)
    {
        $products = $this->app['productCategory']->index($id);
        return view('admin.category.category-products', compact('products', 'id'));
    }

    // 插入全部或批量
    public function insertAll(Request $request)
    {
        $data = $this->app['productCategory']->insertAll($request->brand_id, $request->category_id, $request->productIds);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
