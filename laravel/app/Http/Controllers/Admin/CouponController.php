<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Helpers\AesHelper;

class CouponController extends Controller
{
    /**
     * get coupon index
     */
    public function index()
    {
        $coupons = $this->app['coupon']->adminIndex();
        return view('admin.coupon.index', compact('coupons'));
    }

    /**
     * get coupon create view
     */
    public function create()
    {
        $providers = [
            '1' => '现金券',
            '2' => '满减券',
        ];
        return view('admin.coupon.create', compact('providers'));
    }

    /**
     * store coupon
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'boolean',
            'title' => 'required|max:64',
            'provider' => 'required',
            'value' => 'required',
            'start' => 'date',
            'end' => 'date',
            'number' => 'required|integer',
            'lowest' => '',
            'discount' => 'integer|max:100|min:1',
            'image' => 'image',
        ], [
            'title.required' => '标题必填',
            'provider.required' => '必须选择优惠券类型',
            'value.required' => '必须填写金额',
            'number.required' => '必须填写优惠券数量',
            'discount' => '折扣必须在1-99之间',
            'image' => '图片类型错误'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

        $arr['status'] = $request->status ? 1 : 0;
        $arr['title'] = $request->title;
        $arr['provider'] = $request->provider;
        $arr['value'] = $request->value;
        $arr['start'] = $request->start;
        $arr['end'] = $request->end;
        $arr['number'] = $request->number;
        $arr['remain'] = $request->number;
        $arr['lowest'] = $request->lowest;
        $arr['discount'] = $request->discount;
        $arr['cid'] = AesHelper::createKey(10);

        // app image
        if ($request->image) {
            $uploadImage = \Gimage::uploadImage($request->image, 'coupon', 'images');
            if ($uploadImage instanceof \Exception) {
                $error['image'] = 1;
            } else {
                $arr['image'] = $uploadImage;
            }
        }

        $store = $this->app['coupon']->store($arr);

        if ($store) {
            return redirect(action('Admin\CouponController@index'));
        }

        return back();
    }

     /**
     * status coupon
     * 优惠卷停用，更改其状态
     */
    public function status(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'id'     => 'required|integer',
          'status' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $status = $this->app['coupon']->updateStatus(['status' => $request->status], $request->id);

        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('失败');

    }

    /**
     * show coupon
     * 显示优惠券券码列表
     */
    public function show(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return back()->with(compact('errors'));
        }

        $coupon_id = $request->id;
        $couponGrants = $this->app['coupon']->getCouponGrants($request->id);
        // dd($couponGrants);
        return view('admin.coupon.show', compact('couponGrants', 'coupon_id'));
    }
}
