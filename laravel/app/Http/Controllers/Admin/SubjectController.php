<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\SummernoteHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Gimage\Gimage;

class SubjectController extends Controller
{
    /**
     * 专题列表
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        $subjects = $this->app['subject']->adminIndex();
        return view('admin.subject.index', compact('subjects'));
    }

    public function create()
    {
        return view('admin.subject.create_edit');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|string',
            'subtitle'    => 'string',
            'image'       => 'required|file|image',
            'content'     => 'required|string',
            'product_ids' => 'array',
            'weight'      => 'numeric',

        ], [
            'title.required'   => '标题不能为空',
            'image.required'   => '图片为空或格式出错',
            'content.required' => '专题内容不能为空',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return redirect()->back()->withInput()->with(compact('errors'));
        }
        $arr = [
            'title' => $request['title'],
            'weight' => $request->weight,
            'subtitle' => empty($request['subtitle']) ? '' : $request['subtitle'],
        ];

        $file = $request->file('image');

        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'subject', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['image'] = $storeFile;
        }

        $arr['content'] = SummernoteHelper::getImgName($request->get('content'), 'subject');
        $subject = $this->app['subject']->store($arr);
        if ($subject) {
            if (count($request->product_ids) > 0) {
                $this->app['subjectProduct']->store($subject->id, $request->product_ids);
            }

            return redirect(action('Admin\SubjectController@index'));
        }

        return back();
    }

    /**
     * 编辑获取数据
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $subject = $this->app['subject']->find($id);
        $subjectProducts = $this->app['subjectProduct']->getProducts($id);
        if ($subject) {
            return view('admin.subject.create_edit', compact('subject', 'subjectProducts'));
        }

        return back();
    }

    /**
     * 编辑
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|string',
            'subtitle'    => 'string',
            'image'       => 'image',
            'content'     => 'required|string',
            'product_ids' => 'array',
            'weight'      => 'numeric',

        ], [
            'title.required'   => '标题不能为空',
            'image.image'      => '图片格式出错',
            'content.required' => '专题内容不能为空',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }
        $arr = [
            'title' => $request->title,
            'weight' => $request->weight,
            'subtitle' => empty($request->subtitle) ? '' : $request->subtitle,
        ];

        if ($request->editIden == -1) {
            $file = $request->file('image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'subject', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['image'] = $storeFile;
            }
        }
        $arr['content'] = SummernoteHelper::getImgName($request->get('content'), 'subject');

        $subject = $this->app['subject']->update($arr, $id);
        if ($subject) {
            //删除关联商品，添加新关联
            $this->app['subjectProduct']->delete($id);
            if (count($request->product_ids) > 0) {
                $this->app['subjectProduct']->store($id, $request->product_ids);
            }

            return redirect(action('Admin\SubjectController@index'));
        }

        return back();
    }

    public function destroy($id)
    {
        $result = $this->app['subject']->delete($id);
        if ($result) {
            //删除关联商品
            $this->app['subjectProduct']->delete($id);

            return $this->app['jsend']->success();
        } else {
            return  $this->app['jsend']->error();
        }
    }

    public function status(Request $request)
    {
        $status = $this->app['subject']->update(['status' => $request->status], $request->id);
        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
