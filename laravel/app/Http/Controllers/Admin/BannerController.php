<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BannerController extends Controller
{
    /**
     * get banner index
     */
    public function index()
    {
        $banners = $this->app['banner']->adminIndex();

        return view('admin.banner.index', compact('banners'));
    }

    /**
     * get banner create view
     */
    public function create(Request $request)
    {
        $brands = $this->app['brand']->showAll();
        // 添加推广下级的当前推广id
        $parent_id = $request->id;
        foreach ($brands as $key => $value) {
           $brandData[$value->name] = $value->id;
        }
        $categories = $this->app['category']->index();
        return view('admin.banner.create' , compact( 'brandData', 'categories', 'parent_id'));
    }

    /**
     * store banner
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required',
            'appImage'    => 'image',
            'mobileImage' => 'image',
            'weappImage'  => 'image',
            'weight'      => 'numeric',

        ],[
            'title.required'    => '标题不能为空',
            'appImage.image'    => '图片格式出错',
            'mobileImage.image' => '图片格式出错',
            'weappImage.image'  => '图片格式出错',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->withInput()->with(compact('errors'));
        }

        $arr['title'] = $request->title;
        $arr['status'] = $request->status ? 1 : 0;
        $arr['provider'] = $request->provider ? : '';
        $arr['resource'] = $request->resource ? : '';
        $arr['weight']   = $request->weight ? : 0;

        // app image
        if ($request->appImage) {
            $uploadAppImage = \Gimage::uploadImage($request->appImage, 'bannerApp', 'images');
            if ($uploadAppImage instanceof \Exception) {
                $error['appImage'] = 1;
            } else {
                $arr['app_image'] = $uploadAppImage;
            }
        }

        // mobile image
        if ($request->mobileImage) {
            $uploadMobileImage = \Gimage::uploadImage($request->mobileImage, 'bannerMobile', 'images');
            if ($uploadMobileImage instanceof \Exception) {
                $error['mobileImage'] = 1;
            } else {
                $arr['mobile_image'] = $uploadMobileImage;
            }
        }

        // weapp image
        if ($request->weappImage) {
            $uploadWeappImage = \Gimage::uploadImage($request->weappImage, 'bannerWeapp', 'images');
            if ($uploadWeappImage instanceof \Exception) {
                $error['weappImage'] = 1;
            } else {
                $arr['weapp_image'] = $uploadWeappImage;
            }
        }
        // dd($arr);
        $store = $this->app['banner']->store($arr);

        if ($store) {
            return redirect(action('Admin\BannerController@index'));
        }

        return back();
    }

     /**
     * 获取轮播详情
     */
    public function showDetail(Request $request)
    {
        $id = $request->id;
        if (isset($id)) {
            $validator = Validator::make(['id' => $id], [
                'id' => 'required|integer',
            ]);
            if ($validator->fails()) {
                return back()->with(compact('参数有误！')) ;
            }

            $showBanner = $this->app['banner']->showBanner($id);
            if (!$showBanner) {

                return back()->with(compact('该轮播不存在！'));
            }
        }
        $brands = $this->app['brand']->showAll();
        foreach ($brands as $key => $value) {
           $brandData[$value->name] = $value->id;
        }
        $categories = $this->app['category']->index();
        return view('admin.banner.create', compact('showBanner', 'brands','brandData', 'categories'));
    }

    /**
     * status banner
     * 更新轮播状态
     */
    public function status(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'     => 'required|integer',
            'status' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $status = $this->app['banner']->updateStatus(['status' => $request->status], $request->id );

        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('失败');

    }

    /**
     * edits
     * 编辑推广状态
     */
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'          => 'required|integer',
            'title'       => 'required',
            'appImage'    => 'image',
            'mobileImage' => 'image',
            'weappImage'  => 'image',
            'weight'      => 'numeric',

        ],[
            'title.required'    => '标题不能为空',
            'appImage.image'    => '图片格式出错',
            'mobileImage.image' => '图片格式出错',
            'weappImage.image'  => '图片格式出错',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->withInput()->with(compact('errors'));
        }

        $arr['id']       = $request->id;
        $arr['title']    = $request->title;
        $arr['status']   = $request->status ? 1 : 0;
        $arr['provider'] = $request->provider ? : '';
        $arr['resource'] = $request->resource ? : '';
        $arr['weight']   = $request->weight ? : 0;


        // app image
        if ($request->appImage) {
            $uploadAppImage = \Gimage::uploadImage($request->appImage, 'bannerApp', 'images');
            if ($uploadAppImage instanceof \Exception) {
                $error['appImage'] = 1;
            } else {
                $arr['app_image'] = $uploadAppImage;
            }
        }

        // mobile image
        if ($request->mobileImage) {
            $uploadMobileImage = \Gimage::uploadImage($request->mobileImage, 'bannerMobile', 'images');
            if ($uploadMobileImage instanceof \Exception) {
                $error['mobileImage'] = 1;
            } else {
                $arr['mobile_image'] = $uploadMobileImage;
            }
        }

        // weapp image
        if ($request->weappImage) {
            $uploadWeappImage = \Gimage::uploadImage($request->weappImage, 'bannerWeapp', 'images');
            if ($uploadWeappImage instanceof \Exception) {
                $error['weappImage'] = 1;
            } else {
                $arr['weapp_image'] = $uploadWeappImage;
            }
        }

        $update = $this->app['banner']->updateStatus($arr, $request->id);
        if ($update) {
            return redirect(action('Admin\BannerController@index'));
        }

        return back();
    }

    /**
     * delete
     * 删除首页轮播
     */
    public function delete(Request $request)
    {
        $data = $this->app['banner']->delete($request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }

     /**
    * search
    * 搜索商品
    */
    public function search(Request $request)
    {
        $data = $this->app['popular']->search($request->key);
        return $this->app['jsend']->success($data);
    }

    /**
     * 品牌类别
     * @return [type] [description]
     */
    public function brandCategory(Request $request)
    {
        $data = $this->app['brand']->brandCategory($request->brand_id);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error();
    }
}
