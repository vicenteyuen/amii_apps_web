<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PopularController extends Controller
{
    /**
     * 获取不同popular管理首页
     */
    public function index(Request $request)
    {
        $populars = $this->app['popular']->adminIndex();
        return view('admin.popular.index')->with(compact('populars'));
    }

    /**
     * 获取对应popular所有子
     */
    public function children(Request $request, $id = 0)
    {
        $populars = $this->app['popular']->adminIndex($id);
        return view('admin.popular.index')->with(compact('populars'));
    }

    /**
     * 获取不同popular管理view
     */
    public function create(Request $request)
    {
        // 判断是否为推广下级添加下级
        if ($request->is_childCreate) {
            $is_childCreate = $request->is_childCreate;
        }
        $parents = $this->app['popular']->parents();
        $brands = $this->app['brand']->showAll();
        // 添加推广下级的当前推广id
        $parent_id = $request->id;
        foreach ($brands as $key => $value) {
            $brandData[$value->name] = $value->id;
        }
        $categories = $this->app['category']->index();
        return view('admin.popular.create', compact('parents', 'brandData', 'categories', 'parent_id', 'is_childCreate'));
    }

     /**
     * 获取推广详情
     */
    public function showDetail(Request $request)
    {
        $id = $request->id;
        if (isset($id)) {
            $validator = Validator::make(['id' => $id], [
                'id' => 'required|integer',
            ]);
            if ($validator->fails()) {
                return back()->with(compact('参数有误！')) ;
            }
            $showPopular = $this->app['popular']->show($id);
            if (!$showPopular) {
                return back()->with(compact('该推广不存在！'));
            }
        }

        // 判断是否为推广下级
        if ($request->is_child) {
            $is_child = $request->is_child;
        }

        $parents = $this->app['popular']->parents();
        $childs  = $this->app['popular']->getMyChild($request->id);
        $brands = $this->app['brand']->showAll();
        foreach ($brands as $key => $value) {
            $brandData[$value->name] = $value->id;
        }
        $categories = $this->app['category']->index();
        return view('admin.popular.create', compact('parents', 'showPopular', 'childs', 'brandData', 'categories', 'is_child'));
    }


    /**
     * store popular
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status'      => 'boolean',
            'title'       => 'max:64',
            'descript'    => 'max:512',
            'parentId'    => 'integer',
            'isClick'     => 'boolean',
            'image'       => 'image',
            'weapp_image' => 'image',
            'weight'      => 'numeric',

        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

        $arr['status']    = $request->status ? 1 : 0;
        $arr['title']     = $request->title;
        $arr['descript']  = $request->descript;
        $arr['parent_id'] = $request->parentId ? : 0;
        $arr['is_click']  = $request->isClick ? : 0;
        $arr['weight']    = $request->weight ? : 0;


        if ($arr['is_click'] == 1) {
            $arr['provider']  = $request->provider ? : '';
            $arr['resource']  = $request->resource ? : '';
        } else {
            $arr['provider']  = '';
            $arr['resource']  = '';
        }

        // app image
        if ($request->image) {
            $uploadImage = \Gimage::uploadImage($request->image, 'popular', 'images');
            if ($uploadImage instanceof \Exception) {
                $error['image'] = 1;
            } else {
                if ($arr['parent_id'] == 0) {
                    $arr['image'] = $uploadImage;
                } else {
                    $arr['image'] = '';
                }
            }
        }
        // 小程序 weapp_image
        if ($request->weapp_image) {
            $uploadImage = \Gimage::uploadImage($request->weapp_image, 'popularWeapp', 'images');
            if ($uploadImage instanceof \Exception) {
                $error['weapp_image'] = 1;
            } else {
                if ($arr['parent_id'] == 0) {
                    $arr['weapp_image'] = $uploadImage;
                } else {
                    $arr['weapp_image'] = '';
                }
            }
        }

        $store = $this->app['popular']->store($arr);

        if ($store) {
            if ($arr['parent_id'] == 0) {
                return redirect(action('Admin\PopularController@index'));
            }
            return redirect(action('Admin\PopularController@showDetail', ['id' => $arr['parent_id']]));
        }
        return back();
    }

    /**
     * status popular
     * 更新推广状态
     */
    public function status(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'     => 'required|integer',
            'status' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $status = $this->app['popular']->updateStatus(['status' => $request->status], $request->id);

        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('失败');

    }

    /**
     * edits popular
     * 编辑推广状态
     */
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'       => 'required|string',
            'status'   => 'boolean',
            'title'    => 'max:64',
            'descript' => 'max:512',
            'parentId' => 'integer',
            'isClick'  => 'boolean',
            'image'    => 'image',
            'weapp_image' => 'image',
            'weight'      => 'numeric',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

        $arr['id']        = $request->id;
        $arr['status']    = $request->status ? 1 : 0;
        $arr['title']     = $request->title;
        $arr['descript']  = $request->descript;
        $arr['parent_id'] = $request->parentId ? : 0;
        $arr['is_click']  = $request->isClick ? : 0;
        $arr['weight']    = $request->weight ? : 0;


        if ($arr['is_click'] == 1) {
            $arr['provider']  = $request->provider ? : '';
            $arr['resource']  = $request->resource ? : '';
        } else {
            $arr['provider']  = '';
            $arr['resource']  = '';
        }
        // app image
        if ($request->image) {
            $uploadImage = \Gimage::uploadImage($request->image, 'popular', 'images');
            if ($uploadImage instanceof \Exception) {
                $error['image'] = 1;
            } else {
                if ($arr['parent_id'] == 0) {
                    $arr['image'] = $uploadImage;
                } else {
                    $arr['image'] = '';
                }
            }
        }
        // 小程序 weapp_image
        if ($request->weapp_image) {
            $uploadImage = \Gimage::uploadImage($request->weapp_image, 'popularWeapp', 'images');

            if ($uploadImage instanceof \Exception) {
                $error['weapp_image'] = 1;
            } else {
                if ($arr['parent_id'] == 0) {
                    $arr['weapp_image'] = $uploadImage;
                } else {
                    $arr['weapp_image'] = '';
                }
            }
        }
        $update = $this->app['popular']->update($request->id, $arr);

        if ($update) {
            if ($arr['parent_id'] == 0) {
                return redirect(action('Admin\PopularController@index'));
            }
            return redirect(action('Admin\PopularController@showDetail', ['id' => $arr['parent_id']]));
        }
        $errors = ['指定父级失败,不能指定自身或子级为父级'];
        return back()->with(compact('errors'));
    }

    /**
     * delete
     * 删除推广
     */
    public function delete(Request $request)
    {
        $data = $this->app['popular']->delete($request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }

    public function deleteAll(Request $request)
    {
        $data = $this->app['popular']->deleteAll($request->ids);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }

    /**
    * search
    * 搜索商品
    */
    public function search(Request $request)
    {
        $data = $this->app['popular']->search($request->key);
        return $this->app['jsend']->success($data);
    }

    /**
     * 品牌分类
     * @return [type] [description]
     */
    public function brandCategory(Request $request)
    {
        $data = $this->app['brand']->brandCategory($request->brand_id);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error();
    }
}
