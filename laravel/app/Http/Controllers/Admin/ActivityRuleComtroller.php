<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Helpers\SummernoteHelper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Gimage\Gimage;

class ActivityRuleComtroller extends Controller
{
    //活动列表 index
    public function index(Request $request)
    {
        $activityRules = $this->app['amii.activity.rule']->adminIndex();
        return view('admin.activity_rule.index', compact('activityRules'));
    }

    public function create()
    {
        return view('admin.activity_rule.create_edit');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image'       => 'required|file|image',
            'content'     => 'required|string',
            'weight'      => 'numeric',
            'is_addLink'     => 'boolean',
        ], [
            'image.required'   => '图片为空或格式出错',
            'content.required' => '活动内容不能为空',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return redirect()->back()->withInput()->with(compact('errors'));
        }
        $arr = [
            'weight' => $request->weight,
            'is_addLink' => $request->is_addLink,
        ];
        $file = $request->file('image');

        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'activityRule', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['image'] = $storeFile;
        }
        // 判断是否是添加活动链接
        if ($request->is_addLink == 1) {
            $arr['content'] = $request->content;
        } else {
            $arr['content'] = SummernoteHelper::getImgName($request->get('content'), 'activityRule');
        }

        $activityRule = $this->app['amii.activity.rule']->store($arr);
        if ($activityRule) {
            return redirect(action('Admin\ActivityRuleComtroller@index'));
        }

        return back();
    }

    /**
     * 编辑获取数据
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $activityRule = $this->app['amii.activity.rule']->find($id);

        if ($activityRule) {
            return view('admin.activity_rule.create_edit', compact('activityRule'));
        }
        return back();
    }

    /**
     * 编辑
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'image'       => 'image',
            'content'     => 'required|string',
            'weight'      => 'numeric',
        ], [
            'image.image'      => '图片格式出错',
            'content.required' => '活动内容不能为空',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }
        $arr = [
            'weight' => $request->weight,
            'is_addLink' => $request->is_addLink,
        ];

        if ($request->editIden == -1) {
            $file = $request->file('image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'activityRule', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['image'] = $storeFile;
            }
        }
        // 判断是否是添加活动链接
        if ($request->is_addLink == 1) {
            $arr['content'] = $request->content;
        } else {
            $arr['content'] = SummernoteHelper::getImgName($request->get('content'), 'activityRule');
        }
        // dd(  $arr);
        $activityRule = $this->app['amii.activity.rule']->update($arr, $id);
        if ($activityRule) {
            return redirect(action('Admin\ActivityRuleComtroller@index'));
        }
        return back();
    }

    public function status(Request $request)
    {
        $status = $this->app['amii.activity.rule']->update(['status' => $request->status], $request->id);
        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }

    public function destroy($id)
    {
        $result = $this->app['amii.activity.rule']->delete($id);
        if ($result) {
            return $this->app['jsend']->success();
        } else {
            return  $this->app['jsend']->error();
        }
    }
}
