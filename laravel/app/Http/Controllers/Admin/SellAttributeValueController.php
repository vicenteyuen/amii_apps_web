<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Gimage\Gimage;

class SellAttributeValueController extends Controller
{

    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sell_attribute_id'   => 'required|integer',
            'attribute_value_id'  => 'required|integer',
            'attribute_id'        => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('新增属性值出错');
        }

        $arr = [
            'sell_attribute_id'   => $request->sell_attribute_id,
            'attribute_value_id'  => $request->attribute_value_id,
            'attribute_id'        => $request->attribute_id,
        ];

        $data = $this->app['sellAttributeValue']->store($arr);
        if ($data['status']) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error($data['msg']);
    }

    /**
     * 更新图片
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'   => 'required|integer',
            'sell_product_attribute_value_image'  => 'required|image'
        ], ['sell_product_attribute_value_image.required' => '图片不能为空']);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'id'   => $request->id,
        ];

        $file = $request->file('sell_product_attribute_value_image');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'product', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['sell_product_attribute_value_image'] = $storeFile;
        }
        $data = $this->app['sellAttributeValue']->update($arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fails');
    }

    /**
     * 获取商某个属性的属性值
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        $data = $this->app['sellAttributeValue']->attributeValues($request->sell_attribute_id);
        if (!$data->isEmpty()) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('获取商品属性值失败');
    }

    /**
     * 删除商品属性值
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        $sell_attribute_id = $request->sell_attribute_id;
        // 删除属性
        if ($sell_attribute_id) {
            $data = $this->app['sellAttributeValue']->deleteAttribute($sell_attribute_id);
        } else {
            // 删除属性值
            $data = $this->app['sellAttributeValue']->delete($request->id);
        }
        if ($data['status']) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('删除失败,请刷新重试');
    }

    /**
     * 生成库存
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function makeInventory(Request $request)
    {
        $data = $this->app['sellAttributeValue']->inventory($request->id);
        $inventory = $this->app['inventory']->inventory($request->id);
        if ($data) {
            return $this->app['jsend']->success(['create' => $data,'inventory' => $inventory]);
        }
        return $this->app['jsend']->error('获取库存数据失败');
    }

    /**
     * 生成属性值
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function makeSellAttributeValue(Request $request)
    {
        $data = $this->app['sellAttributeValue']->makeSellAttributeValue($request->id);
        return $this->app['jsend']->success($data);
    }
}
