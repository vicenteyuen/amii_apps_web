<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gimage\Gimage;

class HelperController extends Controller
{
    /**
     * summerote图片上传处理
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function summernoteImageUpload(Request $request)
    {
        $file = $request->file('file');
        // 上传图片的类型
        $provider = $request->provider;
        $storeFile = Gimage::uploadImage($file, $provider, 'images');
        if ($storeFile instanceof \Exception) {
            return response()->json(0);
        }
        // 富文本图片获取原图
        $imgUrl = ImageHelper::getImageUrl($storeFile, $provider, 'source');
        echo $imgUrl;
    }
}
