<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Gimage\Gimage;

class CategoryController extends Controller
{
    /**
     * 查看类别
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index()
    {
        $categories = $this->app['category']->getCategory();
        return view('admin.category.index', compact('categories'));

    }
    /**
     * 添加类别页面
     * @return [type] [description]
     */
    public function shows()
    {
        $categories = $this->app['category']->getCategory();
        return view('admin.category.edit', compact('categories'));
    }

    /**
     * 新增分类
     */
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'parent_id'    => 'required|integer',
            'name'         => 'required|string',
            'weight'       => 'numeric',
            'status'       => 'required|integer',
            'img'          => 'file|image',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数格式出错');
        }

        $arr = [
            'parent_id' => $request->parent_id,
            'name'      => trim($request->name),
            'cate_desc' => $request->cate_desc?:'',
            'status'    => $request->status,
            'weight'    => $request->weight,
        ];

        $file = $request->file('img');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'category', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['img'] = $storeFile;
        }

        if ($this->app['category']->checkAdd($arr)) {
            return $this->app['jsend']->error('该分类已存在');
        }

        $data = $this->app['category']->store($arr);

        if ($data) {
            return $this->app['jsend']->success();
        }

        return $this->app['jsend']->error('添加分类失败');

    }

     /**
     * 修改品牌分类头图
     */
    public function updateBrandCateImg(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brandCateId'  => 'required|integer',
            'files'        => 'file|image',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数格式出错');
        }

        $file = $request->file('files');

        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'brandCateHeard', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $brandCateHeardImages = $storeFile;
        }

        $data = $this->app['category']->updateBrandCateImg($request->brandCateId, $brandCateHeardImages);
        if ($data) {
            return $this->app['jsend']->success();
        }

        return $this->app['jsend']->error('添加图片失败');

    }

    /**
     * 新增类别(无父级)
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $count = 0 ;//添加失败的次数
        $this->recursion($request->categories, $count);

        if (!$count) {
            DB::commit();
            return $this->app['jsend']->success();
        } else {
            DB::rollback();
            return $this->app['jsend']->error('添加分类失败');
        }

    }

    /**
     * 递归新增无限分类
     * @param  [type] $sub       [description]
     * @param  [type] $parent_id [description]
     * @return [type]            [description]
     */
    public function recursion($sub, &$count, $parent_id = '')
    {
        foreach ($sub as $k => $val) {
            if (empty($val['name'])|| empty($val['cate_desc'])) {
                 $count ++;
                 return false;
            }

            $arr = [
                'parent_id' => $parent_id,
                'name'      => $val['name'],
                'cate_desc' => $val['cate_desc']
            ];

            $insertId = $this->app['category']->store($arr);
            if (!$insertId) {
                $count ++;
                return false;
            }

            if (array_key_exists('sub', $val) && !empty($val['sub'])) {
                $this->recursion($val['sub'], $count, $insertId);
            }
        }
    }

    /**
     * 编辑子级分类
     * @param Request $request [description]
     */
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status'       => 'required|integer',
            'img'          => 'file|image',
            'weight'       => 'numeric',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $arr = [
            'cate_desc' => $request->cate_desc?:'',
            'status'    => $request->status,
            'weight'    => $request->weight,
        ];

        $file = $request->file('img');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'category', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['img'] = $storeFile;
        }

        $data = $this->app['category']->update($request->id, $arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('保存分类失败');
    }

    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showData(Request $request)
    {
        $id = $request->id;
        //获取分类品牌
        $data = $this->app['category']->showCategoryData($id);
        return $this->app['jsend']->success($data);
        
    }

    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showCateProduct(Request $request)
    {
        $categoryId = $request->id;
        $brandId = $request->brand_id;
        $products = $this->app['category']->showDetail($categoryId, $brandId, $request);
        return view('admin.category.category-brand-product', compact('products'));
    }

    /**
     * 删除分类商品
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteCateProduct(Request $request)
    {
        $id = $request->product_id;

        $data = $this->app['product']->delCateProduct($id);

        if ($data) {
             return $this->app['jsend']->success();
        } else {
            return  $this->app['jsend']->error();
        }

    }
    /**
     * 删除分类
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        // 批量删除分类列表分类
        $categories = $request->categories;
        if ($id) {
            $data = $this->app['category']->delete($id);
        } else {
            $data = $this->app['category']->deleteCategories($categories);
        }
        if ($data) {
             return $this->app['jsend']->success();
        } else {
            return  $this->app['jsend']->error();
        }

    }
}
