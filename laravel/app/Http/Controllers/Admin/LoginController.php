<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest:admin', ['only' => 'create']);
    }

    // 获取admin登录页
    public function create()
    {
        return view('admin.account.login');
    }

    // admin login
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect('/admin/login')->with(['errors' => $errors]);
        }

        $authCheck = Auth('admin')->attempt(
            [
                'email'     => $request->email,
                'password'  => $request->password
            ],
            $request->remember
        );

        if ($authCheck) {
            return redirect('/admin');
        }
        return redirect('/admin/login')->with(['errors' => ['帐号或密码错误']]);
    }

    // admin logout
    public function logout(Request $request)
    {
        Auth('admin')->logout();
        if ($this->isAjax) {
            return response()->json(1);
        }
        return redirect('/admin/login');
    }
}
