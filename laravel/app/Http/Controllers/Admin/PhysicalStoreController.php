<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\SummernoteHelper;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gimage\Gimage;

class PhysicalStoreController extends Controller
{
    public function index()
    {
        $stores = $this->app['physicalStore']->adminIndex();
        return view('admin.physical_store.index', compact('stores'));
    }

    public function create()
    {
        return view('admin.physical_store.create_edit');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image'          => 'required|file|image',
            'name'           => 'required|string',
            'name_initial'   => 'string',
            'phone'          => 'string',
            'address'        => 'required|string',
            'detail_address' => 'string',
            'longitude'      => 'numeric',
            'latitude'       => 'numeric',
            'city_name'      => 'string',
            'introduction'   => 'string',
            'weight'         => 'numeric',

        ],[
            'image.required'   => '配图不能为空',
            'name.required'    => '名称不能为空',
            'address.required' => '地址不能为空',
            'image.required'   => '配图不能为空',
            'detail_address'   => '补充地址有误',
            'city_name'        => '城市名称有误',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->withInput()->with(compact('errors'));
        }

        $arr = [
            'name'           => $request->name,
            'name_initial'   => $request->name_initial,
            'phone'          => $request->phone,
            'address'        => $request->address,
            'detail_address' => $request->detail_address,
            'longitude'      => $request->longitude,
            'latitude'       => $request->latitude,
            'city_name'      => $request->city_name,
            'weight'         => $request->weight,

        ];

        $file = $request->file('image');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'physicalStore', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['image'] = $storeFile;
        }

        $arr['introduction'] = SummernoteHelper::getImgName($request->introduction, 'physicalStore');
        $store = $this->app['physicalStore']->store($arr);

        if ($store) {
            return redirect(action('Admin\PhysicalStoreController@index'));
        }

        return back();
    }

    public function edit($id)
    {
        $store = $this->app['physicalStore']->find($id);
        if ($store) {
            return view('admin.physical_store.create_edit', compact('store'));
        }

        return back();
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'image'          => 'image',
            'name'           => 'required|string',
            'name_initial'   => 'string',
            'phone'          => 'string',
            'address'        => 'required|string',
            'detail_address' => 'string',
            'longitude'      => 'numeric',
            'latitude'       => 'numeric',
            'city_name'      => 'string',
            'introduction'   => 'string',
            'weight'         => 'numeric',

        ],[
            'image.image'      => '配图格式错误',
            'name.required'    => '名称不能为空',
            'address.required' => '地址不能为空',
            'image.required'   => '配图不能为空',
            'detail_address'   => '补充地址有误',
            'city_name'        => '城市名称有误',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

        $arr = [
            'name'           => $request->name,
            'name_initial'   => $request->name_initial,
            'phone'          => $request->phone,
            'address'        => $request->address,
            'detail_address' => $request->detail_address,
            'longitude'      => $request->longitude,
            'latitude'       => $request->latitude,
            'city_name'      => $request->city_name,
            'weight'         => $request->weight,

        ];

        if ($request->editIden == -1) {
            $file = $request->file('image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'physicalStore', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['image'] = $storeFile;
            }
        }

        $arr['introduction'] = SummernoteHelper::getImgName($request->introduction, 'physicalStore');
        $store = $this->app['physicalStore']->update($arr, $id);
        if ($store) {
            return redirect(action('Admin\PhysicalStoreController@index'));
        }

        return back();
    }

    public function destroy($id)
    {
        $result = $this->app['physicalStore']->delete($id);

        if ($result) {
            return $this->app['jsend']->success();
        } else {
            return  $this->app['jsend']->error();
        }
    }
    public function status(Request $request)
    {
        $status = $this->app['physicalStore']->update(['status' => $request->status], $request->id);
        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
