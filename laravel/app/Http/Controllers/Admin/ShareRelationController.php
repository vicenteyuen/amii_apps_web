<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ShareRelationController extends Controller
{


    // 获取选择分享点数据
    public function getSelected(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sell_product_id'  => 'required|integer',
            'provider'   => 'required|integer',
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $data = $this->app['shareRelation']->getSelected($request->sell_product_id, $request->provider);
        if ($data) {
             return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('获取失败');
    }

    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id'   => 'required|integer',
            'provider_id'  => 'required|integer',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $arr =[
            'product_id'  => $request->product_id,
            'provider_id' => $request->provider_id,
        ];


        $data = $this->app['shareRelation']->storeShareRelation($arr);
        if ($data) {
             return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('添加失败');
    }


}
