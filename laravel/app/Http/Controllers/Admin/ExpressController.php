<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ExpressController extends Controller
{

    public function create()
    {
        return view('admin.express.create-express');
    }
    public function index()
    {
        $expresses = $this->app['express']->index();
        return view('admin.express.index-express', compact('expresses'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name'      => 'required',
                'com'      => 'required',
        ]);

        $arr = array_filter(array_only($request->all(), ['name','com']));
        if ($validator->fails()) {
             $errors = $validator->errors()->toArray();
             return back()->with(compact('errors'));
        }
        $template = $this->app['express']->add($arr);
        if ($template) {
            return redirect(url('/admin/company/express'));
        }
        $errors = ['保存失败'];
        return back()->with(compact('errors'));
    }

    public function save(Request $request)
    {
         $status = $this->app['express']->update($request->id, ['status' => $request->status]);
        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
