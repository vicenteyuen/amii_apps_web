<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TaobaoSyncController extends Controller
{
    /**
     * 获取淘宝同步商品页
     */
    public function index()
    {
        return view('admin.taobao_sync.index');
    }

    /**
     * 同步淘宝商品
     */
    public function sync(Request $request)
    {
        $provider = $request->provider;
        if (in_array($provider, ['amii', 'amiiR', 'amiiM', 'amiiC'])) {
            $this->app['amii.taobao.open.sync.log']->start($provider);
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('请联系管理员');
    }

    /**
     * 获取商户搜索页面
     */
    public function searchView(Request $request)
    {
        $provider = $request->provider;
        if (in_array($provider, ['amii', 'amiiR', 'amiiM', 'amiiC'])) {
            return view('admin.taobao_sync.search')->with(['merchant' => $provider]);
        }
        return $this->app['jsend']->error('请联系管理员');
    }

    /**
     * 搜索商户商品
     */
    public function search(Request $request)
    {
        $provider = $request->provider;
        $merchant = $request->merchant;
        $search = $request->search;

        if (! in_array($merchant, ['amii', 'amiiR', 'amiiM', 'amiiC'])) {
            return $this->app['jsend']->error('请联系管理员');
        }

        switch ($provider) {
            case 'onSale':
                $searchDataArr = $this->app['amii.taobao.open.sync.log']->search($merchant, $provider, $search);
                break;
            case 'inventory':
                $searchDataArr = $this->app['amii.taobao.open.sync.log']->search($merchant, $provider, $search);
                break;

            default:
                return $this->app['jsend']->error('当前商户不可用');
                break;
        }
        if ($searchDataArr[0] !== 0) {
            return $this->app['jsend']->error('搜索失败');
        }
        $items = $searchDataArr[1];
        return $this->app['jsend']->success(['items' => $items]);
    }

    /**
     * 同步商户商品
     */
    public function syncSearch(Request $request)
    {
        $numIid = $request->numIid;
        $merchant = $request->merchant;

        if (! ($numIid && $merchant)) {
            return $this->app['jsend']->error('参数错误');
        }
        if (! in_array($merchant, ['amii', 'amiiR', 'amiiM', 'amiiC'])) {
            return $this->app['jsend']->error('未知商户');
        }

        $syncRes = $this->app['amii.taobao.open.product']->syncTaobaoItemSellerGet($merchant, $numIid);
        if ($syncRes[0] !== 0) {
            return $this->app['jsend']->error($syncRes[1]);
        }
        return $this->app['jsend']->success();
    }

    /**
     * 淘宝分类映射系统分类
     */
    public function category()
    {
        $items = $this->app['taobao.open.category.amii']->index();
        $categorys = $this->app['category']->index()
            ->pluck('name', 'id');
            // dd(json_encode($categorys));
        return view('admin.taobao_sync.category', compact('items', 'categorys'));
    }

    /**
     * 映射分类
     */
    public function mapCate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'taobaoCateId'   => 'required|integer',
            'cateId' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('请求错误');
        }

        $map = $this->app['taobao.open.category.amii']->mapCate($request->taobaoCateId, $request->cateId);

        if (! $map) {
            return $this->app['jsend']->error('映射失败');
        }
        return $this->app['jsend']->success();
    }
}
