<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    //update
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rule'       => 'integer|required',
            'provider'   => 'string|required'

        ], [
            'rule.required' => '数量限制不能为空',
            'rule.integer'  => '数量限制必须为数字',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }
        $arr = [
            'rule' => $request->rule,
        ];
        $result = $this->app['amii.activity']->update($request->provider, $arr);
        if ($result) {
            return $this->app['jsend']->success();
        } else {
            return  $this->app['jsend']->error();
        }
    }
}
