<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ChannelController extends Controller
{
    //index
    public function index(Request $request)
    {
        $channels = $this->app['channel']->adminIndex();
        // 生成默认二维码
        foreach ($channels as $key => $channel) {
            if($channel->code == 'app.amii.com'){
                // 生成二维码
                $url = 'http://app.amii.com/';
                // 保存二维码到本地，并返回二维码
                $qrcode = $this->app['channel']->qrcode($url);
                $dir_path = '../public/channelQrcode/';
                $fileName = 'app.amii.com.png';
                $qrcode = base64_decode( $qrcode);
                // 保存到本地,如果不存在文件，则创建新的
                $this->app['channel']->saveChannelQrcode($qrcode, $dir_path, $fileName);
            }
        }
        return view('admin.channel.index', compact('channels'));
    }

    // 新增
    public function create(Request $request)
    {
        return view('admin.channel.create');
    }

    // 编辑
    public function showEdits(Request $request)
    {
        $channel_id = $request->id;
        $channel = $this->app['channel']->getChannelDeitail($channel_id);
        if ($channel) {
            return view('admin.channel.create', compact('channel'));
        }
        return back();
    }

    // 保存新增
     public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:channels',
        ],[
            'name:unique' => ':attribute 已经存在！',
            'name:required' => ':attribute 不能为空！',
        ],[
            'name' => '渠道名称',
        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
           return back()->with(compact('errors'));
        }
        // 生成随机唯一码
        $code = $this->app['channel']->generateCode();

        $arr = [
            'name' => $request->name,
            'code' => $code,
        ];

        // 生成二维码
        $url = action('Apih5\\VersionController@download', ['provider' => $code]);
        // 保存二维码到本地，并返回二维码
        $qrcode = $this->app['channel']->qrcode($url);
        $dir_path = '../public/channelQrcode/';
        $fileName = $code.'.png';
        $qrcode = base64_decode( $qrcode);
        // 保存到本地,如果不存在文件，则创建新的
        $this->app['channel']->saveChannelQrcode($qrcode, $dir_path, $fileName);

        $data = $this->app['channel']->store($arr);
        if ($data) {
            return redirect(action('Admin\ChannelController@index'));
        }
        return back();
    }

     // 编辑
     public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:channels',
            'id'  => 'required|integer',
        ],[
            'name:unique' => ':attribute 已经存在！',
            'name:required' => ':attribute 不能为空！',
            'id:required' => ':attribute 不能为空！',
        ],[
            'name' => '渠道名称',
            'id' => '渠道编号',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
           return back()->with(compact('errors'));
        }

        $arr = [
            'name' => $request->name,
        ];
        $data = $this->app['channel']->updateChannel($request->id, $arr);
        if ($data) {
            return redirect(action('Admin\ChannelController@index'));
        }
        return back();
    }

    // 保存新增
     public function delete(Request $request)
    {
        $result = $this->app['channel']->delete($request->id);
        if ($result) {
            return $this->app['jsend']->success();
        } else {
            return  $this->app['jsend']->error();
        }
    }

    // 二维码下载
    public function downloadQrcode(Request $request)
    {
        $provider = $request->provider;

        // 下载二维码
        $contenttype = 'image/jpeg';
        $dir_path = '../public/channelQrcode/';
        $fileName = $provider.'.png';
        $fileurl = $dir_path.$fileName;

        header("Cache-control: private");
        header("Content-type: $contenttype"); //设置要下载的文件类型
        header("Content-Length:" . filesize($fileurl)); //设置要下载文件的文件大小
        header("Content-Disposition: attachment; filename=" . urldecode($fileName)); //设置要下载文件的文件名

        readfile($fileurl);
    }


}
