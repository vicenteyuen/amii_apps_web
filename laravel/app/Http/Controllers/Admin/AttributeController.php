<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class AttributeController extends Controller
{

    /**
     * 属性列表
     * @return [type] [description]
     */
    public function index()
    {
        $attributes = $this->app['attribute']->index();
        return view('admin.attribute.index', compact('attributes'));

    }
    public function addShow(Request $request)
    {
         return view('admin.attribute.edit');

    }

    public function editShow(Request $request)
    {
        $attribute = $this->app['attribute']->find($request->id);
        return view('admin.attribute.edit', compact('attribute'));

    }
    
    /**
     * 新增属性
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
            $validator = Validator::make($request->all(), [
            'name'     => 'required|string',
            'has_img'  => 'required|integer',
            'status'   => 'required|integer',
            'weight'   => 'integer',
            'is_multi' => 'integer',
            ]);

            if ($validator->fails()) {
                return $this->app['jsend']->error('数据格式出错');
            }

            $arr = [
                'name'      => trim($request->name),
                'has_img'   => $request->has_img,
                'status'    => $request->status,
                'is_multi'  => $request->is_multi ?: 0,
                'weight'    => $request->weight ?: 0,
            ];

            $data = $this->app['attribute']->store($arr);

            if ($data) {
                return $this->app['jsend']->success();
            }
            return $this->app['jsend']->error('新增属性失败,请检查该属性是否已存在');
        
    }

    /**
     * 编辑属性
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function edits(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'id'       => 'required|integer',
                'name'     => 'required|string',
                'has_img'  => 'required|integer',
                'status'   => 'required|integer',
                'weight'   => 'integer',
                'is_multi' => 'integer',
            ]);

            if ($validator->fails()) {
                return $this->app['jsend']->error('数据格式出错');
            }

            $arr = [
                'name'      => trim($request->name),
                'has_img'   => $request->has_img,
                'status'    => $request->status,
                'is_multi'  => $request->is_multi ?: 0,
                'weight'    => $request->weight ?: 0,
            ];
            $data = $this->app['attribute']->update($arr, $request->id);
            if ($data) {
                return $this->app['jsend']->success();
            }
            return $this->app['jsend']->error('编辑属性失败,请检查该属性是否已存在');
    }
    
    /**
     * 删除属性
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        $delete = $this->app['attribute']->delete($request->id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除属性失败');
    }

    /**
     *获取所有属性
     * @return [type] [description]
     */
    public function get()
    {
        return $this->app['attribute']->get();
    }
}
