<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class FirstPurchaseTextController extends Controller
{

    public function __contruct()
    {
         parent::__construct();
    }
    // 添加页面
    public function create()
    {
        $url = action('Admin\FirstPurchaseTextController@store');
        return view('admin.purchase.purchase-text-create', compact('url'));
    }

    // 编辑页面
    public function show($id)
    {
        
        $url = action('Admin\FirstPurchaseTextController@update');
        $purchaseText = $this->app['amii.purchase.text']->show($id);
        $activity     = $this->app['amii.purchase.text']->getActivity();
        return view('admin.purchase.purchase-text-create', compact('id', 'url', 'purchaseText', 'activity'));
    }

    //列表
    public function index()
    {
        $purchaseText = $this->app['amii.purchase.text']->index();
        $activity     = $this->app['amii.purchase.text']->getActivity();
        return view('admin.purchase.purchase-text-index', compact('purchaseText', 'activity'));

    }

    //新增
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'text'  => 'required|string',
                'title' => 'required|string',
                'order_description' => 'required|string',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

        $data = $this->app['amii.purchase.text']->store(['text' => $request->text], $request->title, $request->order_description);
        if ($data) {
            return redirect(action('Admin\FirstPurchaseTextController@index'));
        }
        $errors = ['新增出错'];
        return back()->with(compact('errors'));
    }

    // 更新
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'id'   => 'required|integer',
                'text' => 'required|string',
                'title'=> 'required|string',
                'order_description' => 'required|string',
        ]);

        if ($validator->fails()) {
              $errors = $validator->errors()->toArray();
              return back()->with(compact('errors'));
        }
        $arr = array_only($request->all(), ['id','text']);
        $data = $this->app['amii.purchase.text']->update($arr, $request->title, $request->order_description);
        if ($data) {
            return redirect(action('Admin\FirstPurchaseTextController@index'));
        }
        $errors = ['编辑失败,请刷新重试'];
        return back()->with(compact('errors'));
    }

    public function delete(Request $request)
    {
        $delete = $this->app['amii.purchase.text']->delete($request->id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
