<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Excel;

class ReportController extends Controller
{
    /**
     * get view refund order
     */
    public function refundOrder()
    {
        $items = $this->app['order']->refundOrder();
        return view('admin.report.refund-order', ['items' => $items]);
    }

    /**
     * 导出有售后订单数据
     */
    public function exportRefundOrder()
    {
        $items = $this->app['order']->refundOrder(true);
        $cellData = [
            ['订单号', '商品编号']
        ];

        foreach ($items as $item) {
            foreach ($item->orderProducts as $orderProduct) {
                $tmp = [
                    "'" . $item->order_sn . "'",
                    "'" . $orderProduct->inventory->sku_code . "'"
                ];
                $cellData[] = $tmp;
            }
        }

        Excel::create('售后订单纪录',function($excel) use ($cellData){
            $excel->sheet('order', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->export('xlsx');
    }
}
