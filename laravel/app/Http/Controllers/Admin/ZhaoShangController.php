<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ZhaoShangController extends Controller
{
    /**
     * get zhaoshang index
     */
    public function index(Request $request)
    {
        $zhaoshangs = $this->app['Zhaoshang']->adminIndex();
        return view('admin.zhaoshang.index', compact('zhaoshangs'));
    }

    /**
     * 显示详情
     *@param  Request $request [description]
     *@return [type]           [description]
    */
    public function showDetail(Request $request)
    {
        $user_id = $request->user_id;
        $validator = Validator::make(['id' => $user_id], [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $userInfo = $this->app['Zhaoshang']->show($user_id);
        if ($userInfo) {

            return view('admin.zhaoshang.detail', compact('userInfo'));
        }
        return $this->app['jsend']->error('查看详情失败');

    }

    /**
     * 编辑信息处理
     *@param  Request $request [description]
     *@return [type]           [description]
    */
    public function edits(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id'         => 'required|integer',
            'name'       => 'required',
            'phone'      => 'required|phone',
            'email'      => 'email',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'name'       => $request->name,
            'phone'      => $request->phone,
            'email'      => $request->email,
            // 将商家更改为已处理状态
            'status'     => 1,
        ];
        // return ['status' => 0, 'msg' => '信息处理失败！'];
        $updateInfo = $this->app['Zhaoshang']->update($arr, $request->id);


        if ($updateInfo['status']) {

            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($updateInfo['msg']);

    }
}
