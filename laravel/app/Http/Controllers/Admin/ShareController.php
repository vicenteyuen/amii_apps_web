<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ShareController extends Controller
{
    //获取分享文案列表
    public function index()
    {
        $shareDatas = $this->app['share']->adminShareIndex();
        // 获取分享端口类型
        $shareTypes = $this->app['share']->getShareType();
        return view('admin.share.index', compact('shareDatas', 'shareTypes'));
    }

    // 新增文案
    public function create()
    {
        return view('admin.share.create');
    }

    // 获取文案详情
    public function showDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'  => 'required|integer',
            'type' => 'required|integer',
        ],[
            'required' => ':attribute 不能为空！',
            'integer' => ':attribute 格式不正确！',
        ],[
            'id' => '参数',
            'type' => '参数类型',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }
        $showDetail = $this->app['share']->getShareDetail($request->id);

        if (!$showDetail) {
            $error = ['文案不存在！'];
            return back()->with(compact('error'));
        }
        // 选择显示视图
        if ($request->type != 1) {
            //编辑页
            return view('admin.share.create', compact('showDetail'));
        }
        return view('admin.share.detail', compact('showDetail'));
    }

    // 保存文案
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'       => 'required|max:64',
            'content'     => 'required|max:512',
            'provider'    => 'integer',
            'image'       => 'image',
            'is_defaultTitle'  => 'boolean',
            'is_defaultContent' => 'boolean',
            'is_defaultImg' => 'boolean',
            'is_shareAll' => 'boolean',
        ],[
            'title.required' => '标题不能为空！',
            'content.required' => '内容不能为空！',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->withInput()->with(compact('errors'));
        }

        $arr['title'] = $request->title != 2 ? $request->title : 2;
        $arr['content']  = $request->content != 2 ? $request->content : 2;
        $arr['provider'] = $request->provider;
        $arr['is_defaultTitle'] = $request->is_defaultTitle;
        $arr['is_defaultContent'] = $request->is_defaultContent;
        $arr['is_defaultImg'] = $request->is_defaultImg;
        $arr['is_shareAll'] = $request->is_shareAll;


        if ($request->shareSelectIds && $request->is_shareAll != 1) {
            $shareSelectIds = explode(',', $request->shareSelectIds);
        } else {
            $shareSelectIds = '';
        };

        // 文案 image
        if ( $request->is_defaultImg == 1) {
            $arr['image'] = '';
        } else {
            if($request->image) {
                $uploadImage = \Gimage::uploadImage($request->image, 'share' , 'images');
                if ($uploadImage instanceof \Exception) {
                    $error['image'] = 1;
                } else {
                    $arr['image'] = $uploadImage;
                }
            }
        }

        $store = $this->app['share']->store($arr, $shareSelectIds);
        if ($store) {
            return redirect(action('Admin\ShareController@index'));
        }
        return back();
    }

    // 编辑文案
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'  => 'required|integer',
            'title'       => 'required|max:64',
            'content'     => 'required|max:512',
            'hiddenProvider'    => 'integer',
            'image'       => 'image',
            'is_defaultTitle'  => 'boolean',
            'is_defaultContent' => 'boolean',
            'is_defaultImg' => 'boolean',
            'is_shareAll' => 'boolean',
        ],[
            'title.required' => '标题不能为空！',
            'content.required' => '内容不能为空！',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

       if ($request->shareSelectIds && $request->is_shareAll != 1) {
            $shareSelectIds = explode(',', $request->shareSelectIds);
        } else {
            $shareSelectIds = '';
        };

        $arr['id']  = $request->id;
        $arr['title'] = $request->title != 2 ? $request->title : 2;
        $arr['content']  = $request->content != 2 ? $request->content : 2;
        $arr['provider'] = $request->hiddenProvider;
        $arr['is_defaultTitle'] = $request->is_defaultTitle;
        $arr['is_defaultContent'] = $request->is_defaultContent;
        $arr['is_defaultImg'] = $request->is_defaultImg;
        $arr['is_shareAll'] = $request->is_shareAll ;

        // 文案 image
        if( $request->is_defaultImg == 1){
            $arr['image'] = '';
        } else {
            if ($request->image) {
                $uploadImage = \Gimage::uploadImage($request->image, 'share' , 'images');
                if ($uploadImage instanceof \Exception) {
                    $error['image'] = 1;
                } else {
                    $arr['image'] = $uploadImage;
                }
            }
        }
        // dd($arr);

        $store = $this->app['share']->update($request->id, $arr, $shareSelectIds);
        if ($store) {
            return redirect(action('Admin\ShareController@index'));
        }
        return back();

    }

    // 删除文案
    public function delete(Request $request)
    {
        $data = $this->app['share']->delete($request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }

    // 搜索商品数据
    public function search(Request $request)
    {
        $provider_id = $request->provider_id;
        $data = $this->app['share']->search($request->key, $provider_id);
        return $this->app['jsend']->success($data);
    }
}
