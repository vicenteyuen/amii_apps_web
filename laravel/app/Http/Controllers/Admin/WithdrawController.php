<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\WithdrawRecord;

class WithdrawController extends Controller
{
    /**
     * 获取需要审核提现单
     */
    public function index(Request $request)
    {
        $items = new WithdrawRecord;
        if ($request->status !== null) {
            $items = $items->where('state', $request->status);
        }
        $items = $items->with('channel.user')
            ->paginate(config('amii.adminPaginate'));

        return view('admin.withdraw.index')->with(['items' => $items, 'request' => $request]);
    }

    /*!
     * 支付用户申请提现
     * @param  Request $request
     */
    public function agree(Request $request)
    {
        \DB::beginTransaction();
        $record = WithdrawRecord::where('id', $request->id)
            ->first();
        if (! $record) {
            return $this->app['jsend']->error();
        }
        // 校验余额是否满足提现额度
        if (! ($record->channel && $record->channel->user && $record->channel->user->cumulate_profit < $record->amount)) {
            return $this->app['jsend']->error();
        }
        $record->update([
                'state' => 1
            ]);
        $pay = $record->paySend();
        if ($pay) {
            \DB::commit();
            return $this->app['jsend']->success();
        }
        \DB::rollback();
        return $this->app['jsend']->error();
    }

    /*!
     * 拒绝用户提现申请
     * @param  Request $request
     */
    public function refuse(Request $request)
    {
        WithdrawRecord::where('id', $request->id)
            ->update([
                'state' => 3
            ]);
        return $this->app['jsend']->success();
    }
}
