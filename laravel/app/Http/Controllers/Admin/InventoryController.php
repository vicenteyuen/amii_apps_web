<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\AmiiHelper;
use Validator;
use DB;
use Gimage\Gimage;

class InventoryController extends Controller
{

    /**
     * 获取无属性商品库取
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        $arr = [
            'product_id' => $request->product_id,
            'product_sell_id' => $request->id,
        ];
        $data = $this->app['inventory']->show($arr);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('失败');

    }

    /**
     * 新增库存页面
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request)
    {
        $attributes = $this->app['attribute']->index();
        $name = '商品1';
        return view('admin.inventory.create', compact('name', 'attributes'));

    }

    /**
     * 获取库存数据
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function showData(Request $request)
    {
        $arr = [
            'value_key'         => $request->value_key,
            'product_sell_id'   => $request->sellProduct_id,
        ];
        $inventory = $this->app['inventory']->show($arr);
        if ($inventory) {
             return $this->app['jsend']->success($inventory);
        }
        return $this->app['jsend']->error('该属性值没有库存');
    }

    /**
     * 获取库存数据
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function inventory(Request $request, $id)
    {
        $inventory = $this->app['inventory']->inventory($id);
        if ($inventory) {
             return $this->app['jsend']->success($inventory);
        }
        return $this->app['jsend']->error('该属性值没有库存');
    }



     /**
     * 新增库存
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_id'        => 'required|integer',
            'product_sell_id'   => 'required|integer',
            'value_key'         => 'required|string',
            'mark_price'        => 'required|numeric',
            'number'            => 'required|integer',
            'sku_code'          => 'string',
            'image'             => 'image'
        ], [
            'mark_price.required' => ':attribute 不能为空',
            'number.required'     => ':attribute 不能为空',
        ], [
            'mark_price' => '积分',
            'number'     => '库存数量',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $arr = [
            'product_id'            => $request->product_id,
            'product_sell_id'       => $request->product_sell_id,
            'sku_code'              => $request->sku_code,
            'mark_price'            => $request->mark_price,
            'value_key'             => $request->value_key,
            'warn_number'           => $request->warn_number,
            'number'                => $request->number,
        ];
        // 商品编号
        if (empty($arr['sku_code'])) {
            $arr['sku_code'] =  AmiiHelper::generateProductSn();
        }

        // 库存图片
        $file = $request->file('image');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'product', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['image'] = $storeFile;
        }
        $inventory = $this->app['inventory']->store($arr);
        if ($inventory) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('新增库存失败');
    }

    /**
     * 编辑库存
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id'        => 'required|integer',
            'product_sell_id'   => 'required|integer',
            'value_key'         => 'required|string',
            'mark_price'        => 'required|numeric',
            'number'            => 'required|integer',
            'sku_code'          => 'string',
            'image'             => 'image',
        ], [
            'mark_price.required' => ':attribute 不能为空',
            'number.required'     => ':attribute 不能为空',
        ], [
            'mark_price' => '积分',
            'number'     => '库存数量',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $arr = [
            'sku_code'              => $request->sku_code,
            'mark_price'            => $request->mark_price,
            'warn_number'           => $request->warn_number,
            'number'                => $request->number,
            'product_id'            => $request->product_id,
        ];

        $where = [
            'value_key'             => $request->value_key,
            'product_sell_id'       => $request->product_sell_id,
        ];
         // 商品编号
        if (empty($arr['sku_code'])) {
            $arr['sku_code'] =  AmiiHelper::generateProductSn();
        }

        // 库存图片
        $file = $request->file('image');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'product', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['image'] = $storeFile;
        }
        $inventory = $this->app['inventory']->update($arr, $where);
        if ($inventory) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('编辑库存失败，请刷新重试');
    }
}
