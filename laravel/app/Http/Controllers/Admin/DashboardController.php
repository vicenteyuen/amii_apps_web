<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProductRefund;
use App\Models\User;
use Carbon\Carbon;
use Cache;

class DashboardController extends Controller
{
    public function getIndex()
    {
        $labels = [];
        $start_datetime = Carbon::createFromTimestamp(strtotime('-12 months'))
            ->day(1)
            ->subDay(1)
            ->hour(23)
            ->minute(59)
            ->second(59);

        $for_datetime = $start_datetime;
        $labels[] = $for_datetime->format('Y-m');
        for ($i = 1; $i <= 12; $i++) {
            $for_datetime = $for_datetime
                ->addMonth();

            $labels[] = $for_datetime->format('Y-m');
        }

        $registration = User::selectRaw('count(id) as count, date_format(created_at,"%Y-%m") as date')
            ->where('created_at', '>', $start_datetime->toDateTimeString())
            ->groupBy(\DB::raw('date_format(created_at,"%Y-%m")'))
            ->get()
            ->each(function ($user) {
                $user->setVisible(['count', 'date']);
            });

        $arr = [];
        foreach ($labels as $v) {
            $date = $registration->where('date', $v)->first();
            $arr[] = $date ? $date['count'] : 0;
        }
        $registration = $arr;

        $orders = Order::withTrashed()
            ->selectRaw('count(id) as count, date_format(created_at,"%Y-%m") as date')
            ->where('created_at', '>', $start_datetime->toDateTimeString())
            ->groupBy(\DB::raw('date_format(created_at,"%Y-%m")'))
            ->get()
            ->each(function ($user) {
                $user->setVisible(['count', 'date']);
            });

        $arr = [];
        foreach ($labels as $v) {
            $date = $orders->where('date', $v)->first();
            $arr[] = $date ? $date['count'] : 0;
        }
        $orders = $arr;

        $history = compact('registration', 'orders','labels');

        // add cache
        $userCount = Cache::get('dashboard:userCount');
        if (empty($userCount) ) {
            $userCount = User::count();
            Cache::put('dashboard:userCount',  $userCount , 2);;
        }

        $orderProductRefund = Cache::get('dashboard:orderProductRefund');
        if (empty($orderProductRefund) ) {
            $orderProductRefund =OrderProductRefund::count();
            Cache::put('dashboard:orderProductRefund',  $orderProductRefund , 2);;
        }

        $ordersCount = Cache::get('dashboard:ordersCount');
        if (empty($ordersCount) ) {
            $ordersCount = Order::withTrashed()->paid()->count();
            Cache::put('dashboard:ordersCount',  $ordersCount , 2);;
        }

        $ordersAmount = Cache::get('dashboard:ordersAmount');
        if (empty($ordersAmount) ) {
            $ordersAmount = Order::withTrashed()
                                    ->paid()
                                    ->where('provider', 'standard')
                                    ->sum(\DB::raw('order_fee + balance_discount'));
            Cache::put('dashboard:ordersAmount',  $ordersAmount , 2);
        }



        $numbers = new \stdClass();
        $numbers->orders_count = $ordersCount;
        $numbers->orders_total_amount = $ordersAmount;
        $numbers->refund_count = $orderProductRefund;
        $numbers->user_count = $userCount;

        return view('admin.dashboard.index', compact('numbers', 'history'));
    }
}
