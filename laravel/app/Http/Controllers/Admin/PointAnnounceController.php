<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PointAnnounceController extends Controller
{
    public function index()
    {
        $items = $this->app['point.rank.award']->adminIndex();
        return view('admin.announce.point.index', ['items' => $items]);
    }

    /**
     * 创建公告
     */
    public function createOrShow(Request $request)
    {
        $item = $this->app['point.rank.award']->newObject($request->id);
        return view('admin.announce.point.show', ['item' => $item]);
    }

    /**
     * 修改公告
     */
    public function update(Request $request)
    {
        if ($request->id) {
            return back();
        } else {
            // 创建公告
            $validator = Validator::make($request->all(), [
                'name'  => 'required',
                'number'  => 'required',
                'provider'  => 'required',
                'start'  => 'required',
                'end'  => 'required',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return back();
            }

            $arr = [
                'name' => $request->name,
                'number' => $request->number,
                'provider' => $request->provider,
                'start_time' => $request->start,
                'end_time' => $request->end,
            ];
            $create = $this->app['point.rank.award']->create($arr);
            if ($create) {
                return redirect(action('Admin\PointAnnounceController@index'));
            }
            return back();
        }
    }

    /**
     * 公示公告
     */
    public function publish(Request $request)
    {
        $publish = $this->app['point.rank.award']->publish($request->id);
        return redirect(action('Admin\PointAnnounceController@index'));
    }

    /**
     * 下架公告
     */
    public function unPublish(Request $request)
    {
        $unPublish = $this->app['point.rank.award']->unPublish($request->id);
        return redirect(action('Admin\PointAnnounceController@index'));
    }
}
