<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PointReceivedController extends Controller
{
    public function show($id)
    {
        $points = $this->app['pointReceived']->userIndex($id);
        return view('admin.member.point', compact('points'));
    }
}
