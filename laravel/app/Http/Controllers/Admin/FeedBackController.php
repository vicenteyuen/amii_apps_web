<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;

class FeedBackController extends Controller
{
    // 用户反馈列表
    public function index(Request $request)
    {
        $feedbacks = $this->app['amii.feedback']->adminIndex();
        return view('admin.feedback.index', compact('feedbacks'));
    }

    /**
     * 显示反馈详情
     *@param  Request $request [description]
     *@return [type]           [description]
    */
    public function showDetail(Request $request)
    {
        $id = $request->id;

        $validator = Validator::make(['id' => $id], [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $feedbacksInfo = $this->app['amii.feedback']->show($id);
        if ($feedbacksInfo) {
            return view('admin.feedback.detail', compact('feedbacksInfo'));
        }
        return $this->app['jsend']->error('查看详情失败');

    }

    /**
     * 编辑反馈处理
     *@param  Request $request [description]
     *@return [type]           [description]
    */
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'         => 'required|integer',
            'nickname'   => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            // 将反馈状态更改为已处理状态
            'status'     => $request->status,
        ];

        $updateInfo = $this->app['amii.feedback']->update($arr, $request->nickname, $request->id);

        if ($updateInfo['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($updateInfo['msg']);

    }
}
