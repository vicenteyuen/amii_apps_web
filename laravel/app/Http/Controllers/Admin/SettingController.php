<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function update(Request $request)
    {
        $update = $this->app['setting']->update($request->key, $request->value);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }

    public function index()
    {
        $setting = $this->app['setting']->community();
        return view('admin.setting.index', compact('setting'));
    }
}
