<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Gimage\Gimage;

class ProductImageController extends Controller
{
     /**
     * store image
     */
    public function store(Request $request)
    {

        $file = $request->file;
        if (! $file) {
            return response()->json(0);
        }
        $storeFile = Gimage::uploadImage($file, 'product', 'images');
        if ($storeFile instanceof \Exception) {
            return response()->json(0);
        }

        $arr = [
            'image'          => $storeFile,
            'product_id'     => $request->product_id,
        ];
        $data = $this->app['productImage']->store($arr);
        if ($data) {
             return response()->json($data);
        }
        return response()->json(0);
    }
    /**
     * 删除商品图片
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        $data = $this->app['productImage']->delete($request->id);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除商品图片失败');

    }

    // 更换图片
    public function change(Request $request)
    {
        $productId = $request->product_image_id;
        $sellProductId = $request->sell_product_id;
        $data = $this->app['productImage']->change($productId, $sellProductId);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
