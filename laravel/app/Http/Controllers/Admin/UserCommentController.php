<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserCommentController extends Controller
{
    //商品评论列表
    public function index(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_id'   => 'integer',
            'provider'     => 'integer',
            'rank'         => 'integer',
            'search'       => 'string',
            'start'        => 'date',
            'end'          => 'date',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return back()->with(compact('errors'));
        }

        isset($request->search) ? $request->search : '';
        isset($request->start) ? $startTime = $request->start: $startTime ='';
        isset($request->end) ? $endTime = $request->end : $endTime ='';
        $startTime && $endTime ? $searchTime = 1 : $searchTime = '';

        $product_id      = $request->product_id;
        $provider        = $request->provider;
        $comment_rank_id = $request->rank;
        $search          = $request->search;
        // 用户判断是否开启搜索
        if (!$provider) {
            $searchUp = 1;
        }
        $commentLists   = $this->app['userComment']->getCommentInfo($product_id, $provider, $comment_rank_id, $search, $request,  $searchTime,  $startTime, $endTime);
        $ranks  = $this->app['userComment']->getCommentRanks();
        return view('admin.usercomment.index', compact('commentLists', 'ranks', 'product_id', 'provider', 'searchUp', 'comment_rank_id'));
    }

    // 查看商品评论详情
    public function showDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sell_product_id'     => 'required|integer',
            'comment_id'     => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return back()->with(compact('errors'));
        }

        //获取评论内容
        $commentDetail = $this->app['userComment']->showCommentContent($request->sell_product_id, $request->comment_id);
        //获取评论图片
        $commentImages = $this->app['userComment']->getCommentImg($request->comment_id);

        return view('admin.usercomment.detail', compact('commentDetail', 'commentImages'));
    }

    // 商品评论状态更改
    public function status(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'     => 'required|integer',
            'status' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }
        $status =  $this->app['userComment']->updateStatus(['comment_status' => $request->status], $request->id);

        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('失败');

    }
}
