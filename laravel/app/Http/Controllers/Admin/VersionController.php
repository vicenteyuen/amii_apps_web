<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

class VersionController extends Controller
{
    public function index(Request $request)
    {
        isset($request->search) ? $request->search : '';

        $versions = $this->app['version']->adminIndex($request->search);
       // 获取渠道名称
        $channels = $this->app['version']->getVersionChannel();
        return view('admin.version.index', compact('versions', 'channels'));
    }

    // 新系统版本添加页
    public function create()
    {
        return view('admin.version.create');
    }

    // 编辑/查看系统版本详情
    public function showDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'   => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('参数错误！');
        }

        isset($request->type) ? $request->type : 'show';
        $type = $request->type;
        $versions = $this->app['version']->detail($request->id);
        return view('admin.version.create', compact('versions', 'type'));
    }

    // 添加新系统版本
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'           => 'required',
            'is_newest'      => 'required|boolean',
            'is_uploadApk'   => 'required|boolean',
            'version_code'   => 'required|integer',
            'version_type'   => 'required',
            'version_url'    => 'required|string',
            'version_number' => 'required|string',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
           return $this->app['jsend']->error($errors);
        }

        $arr = [
            'name'           =>  $request->name,
            'channel'        =>  $request->channel,
            'version_code'   =>  $request->version_code,
            'version_type'   =>  $request->version_type,
            'version_number' =>  $request->version_number,
            'version_url'    =>  $request->version_url,
            'is_newest'      =>  $request->is_newest,
            'description'    =>  $request->description,
            'is_uploadApk'   =>  $request->is_uploadApk,
        ];

        $data = $this->app['version']->store($arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($data['message']);
    }

    // 编辑指定版本信息
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'             => 'required|integer',
            'name'           => 'required',
            'is_newest'      => 'required|boolean',
            'is_uploadApk'   => 'required|boolean',
            'version_type'   => 'required',
            'channel'        => 'required',
            'version_code'   => 'required|integer',
            'version_url'    => 'required|string',
            'version_number' => 'required|string',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'name'           =>  $request->name,
            'is_newest'      =>  $request->is_newest,
            'version_type'   =>  $request->version_type,
            'channel'        =>  $request->channel,
            'version_url'    =>  $request->version_url,
            'version_number' =>  $request->version_number,
            'version_code'   =>  $request->version_code,
            'description'    =>  $request->description,
            'is_uploadApk'   =>  $request->is_uploadApk,
        ];

        $data = $this->app['version']->update($arr, $request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($data['message']);
    }

    // 上传安装包
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = $request->name;

        $file_path = storage_path('temp/').$file_name;
        $file_part_path = $file_path . '.part';

        $in = @fopen($file, 'rb');
        $out = @fopen($file_part_path, 'ab');
        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }
        @fclose($out);
        @fclose($in);

        $chunk = (int) $request->get('chunk', false);
        $chunks = (int) $request->get('chunks', false);
        if ($chunk == $chunks - 1) {
            rename($file_part_path, $file_path);
            $file = new UploadedFile($file_path, $file_name, 'blob', sizeof($file_path), UPLOAD_ERR_OK, true);
            $file_name = $this->app['fileUpload']->upload($file_path, $file, 'packages');
            @unlink($file_path);

            if ($file_name) {
                return $this->app['jsend']->success(compact('file_name'));
            } else {
                return $this->app['jsend']->error('上传失败，请重试');
            }
        }
    }

    public function delete(Request $request)
    {
        $version = $this->app['version']->delete($request->id);
        if ($version) {
            return $this->app['jsend']->success();
        } else {
            return $this->app['jsend']->error();
        }
    }
}
