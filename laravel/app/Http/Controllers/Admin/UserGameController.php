<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class UserGameController extends Controller
{
    /**
     * 统计数据
     * @return [type] [description]
     */
    public function manager(Request $request)
    {

        $data['joinGamePerson']         = $this->app['amii.user.game']->joinGame($request); //发起人数
        $data['gameCollectionPreson']   = $this->app['amii.user.game']->gameCollection($request);//点亮人数
        $data['takePartIn']             = $this->app['amii.user.game']->joinUsers($request);//总参与人数
        $data['joinGameTime']           = $this->app['amii.user.game']->joinGameTime($request);//发起次数
        $data['personPerTime']          = $this->app['amii.user.game']->personPerTime($request);//人均发起次数
        $data['sendGameTime']           = $this->app['amii.user.game']->sendGameTime($request);//送出件数
        $data['sendPerson']             = $this->app['amii.user.game']->sendPerson($request);//送出人数
        $data['personPerGetTime']       = $this->app['amii.user.game']->personPerGetTime($request);//人均获得件数
        $data['unCommit']               = $this->app['amii.user.game']->waitCommit($request);//待合成次数
        $perData                        = $this->app['amii.user.game']->trend($request->date);//趋势图
        return view('admin.game.dataManage', compact('data', 'perData'));

    }

    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id'    => 'required|integer',
            'market_price'  => 'required|numeric',
            'base_price'    => 'required|numeric',
            'puzzles'       => 'required|integer|in:4,6,9,12,16,20,25'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $arr = array_only($request->all(), ['product_id','market_price','base_price','puzzles']);
        $data = $this->app['sellproduct']->addProductToGame($arr, 'YS001');
        if ($data['status']) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error($data['message']);
    }

    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function storeByGetMethod(Request $request)
    {
        $arr = [
            'product_id' => $request->product_id,
            'market_price' => $request->market_price,
            'base_price'  => $request->market_price,
            'puzzles'  => $request->puzzles,
        ];
        $data = $this->app['sellproduct']->addProductToGame($arr, 'YS001');
        if ($data['status'] == 1) {
            return redirect($data['url']);
        }
        $errors = ['添加游戏库存出错'];
        return back()->with(compact('errors'));
    }

    /**
     * 添加游戏页面
     * @return [type] [description]
     */
    public function create(Request $request)
    {
        $products = $this->app['product']->gameProductSearch(trim($request->key));
        return view('admin.game.create', compact('products'));
    }

    /**
     * 列表
     * @return [type] [description]
     */
    public function index(Request $request)
    {
        $sellProducts = $this->app['sellproduct']->adminGameProduct(trim($request->search));
        $activity     = $this->app['amii.activity']->getActivityByProvider('game');
        return view('admin.game.index', compact('sellProducts', 'activity'));
    }

    /**
     * 搜索商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function search(Request $request)
    {
        $data = $this->app['product']->gameProductSearch($request->key);
        if (!$data->isEmpty()) {
             return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('查询不到该品牌下的商品');

    }
}
