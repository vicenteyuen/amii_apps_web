<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SellController extends Controller
{
    /**
     * 库存属性
     * @param  Request $request    [description]
     * @param  [type]  $product_id [description]
     * @param  [type]  $sell_id    [description]
     * @return [type]              [description]
     */
    public function index(Request $request)
    {
        if ($request->provider == 3 || $request->provider == 4) {
            $adminHtml = 'admin.game.sell'; // 游戏页面,礼品页面
        } else {
            $adminHtml = 'admin.sell.create';
        }
        $attributes = $this->app['attribute']->get();
        // 刷新新增页面
        $sellPro  = $this->app['sellproduct']->addSellType($request->product_id, $request->provider, 2);
        $arr = ['id' => $sellPro->id];
        $hasInventory   = $this->app['sellproduct']->hasInventory($sellPro->id);
        $sellStatus  = $this->app['sellproduct']->detail($sellPro->id)->status;

        // 判断是否有属性和属性值，
        $hasSellAttributes = $this->app['sellAttribute']->hasSellAttributes($sellPro->id);
        if ($hasSellAttributes) {
            $hasSellAttr = 1;
        }
        if ($hasInventory) {
            return $this->edits($request);
        }
        return view($adminHtml, compact('attributes', 'sellStatus', 'sellPro', 'hasSellAttr'));
    }

    /**
     * 库存管理(新增 编辑)
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function edits(Request $request)
    {
        if ($request->provider == 3 || $request->provider == 4) {
            $adminHtml = 'admin.game.sell'; // 游戏页面
        } else {
            $adminHtml = 'admin.sell.create';
        }
        $edit     = 1;
        $sellPro  = $this->app['sellproduct']->addSellType($request->product_id, $request->provider, 2);
        $hasInventory = $this->app['sellproduct']->hasInventory(['id' => $sellPro->id]);

        // 新增
        if (!$hasInventory) {
            return $this->index($request);
        }

        // 编辑
        $where    = [
            'product_id'      => $request->product_id,
            'sell_product_id' => $sellPro->id
        ];

        $attributes    = $this->app['attribute']->get();                                // 属性
        $productImages = $this->app['productImage']->index($where);                     // 库存图片
        $sellProduct   = $this->app['sellproduct']->detail($sellPro->id);               // 销售形式
        $sellStatus    = $sellProduct->status;                                          // 商品状态(上下架)
        $sellAttributeValue = $this->app['sellAttributeValue']->getSellAttributeValue($sellPro->id); //属性值
        return view($adminHtml, compact('attributes', 'edit', 'productImages', 'sellProduct', 'sellStatus', 'sellPro', 'sellAttributeValue'));
    }
}
