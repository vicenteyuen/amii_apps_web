<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    /**
     * 会员列表
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        if ($request->sort && $request->order) {
            //排序
            $where = ['sort' => $request->sort, 'order' => $request->order];
            $members = $this->app['user']->index($where);
        } elseif ($request->search || $request->start || $request->end) {
            //search 查找
            $members = $this->app['user']->index([], $request);
        } else {
            $members = $this->app['user']->index(['sort' => 'created_at', 'order' => 'desc']);
        }

        return view('admin.member.index', compact('members'));
    }

    /**
     * 会员详情
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function show($id)
    {
        $user = $this->app['user']->getDetail($id);
        $addresses    = $this->app['address']->show($user->id); // 获取用户地址
        $firstTribes  = $this->app['user']->getTribes('first', $user->id); // 获取一级部落信息
        $secondTribes = $this->app['user']->getTribes('secode', $user->id); // 获取二级部落信息
        $parentTribe  = $this->app['tribe']->parentTribe($user->id);
        $balances     = $this->app['balance']->balanceUserRecord($user->id); //余额记录
        return view('admin.member.show', compact('user', 'addresses', 'firstTribes', 'secondTribes', 'parentTribe', 'balances'));
    }

    /**
     * 修改会员生日
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function changeBirthday(Request $request)
    {
        $user_id = $request->userId;
        $birthday = $request->birthday;
        $res = $this->app['user']->updateBirthday($user_id, $birthday);

        if (!$res) {
            return $this->app['jsend']->error('修改失败');
        }
        return $this->app['jsend']->success();
    }

    // 更新用户信息
    public function updateUser(Request $request)
    {
        $arr = ['status' => $request->status];
        $data = $this->app['user']->updateUser($request->id, $arr);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
