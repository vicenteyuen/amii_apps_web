<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ProductAttributeController extends Controller
{

    public function index(Request $request)
    {
        $data = $this->app['productAttribute']->index($request->product_id);
        if ($data->isEmpty()) {
            return $this->app['jsend']->error('获取属性失败');
        }
        return $this->app['jsend']->success($data);

    }
    /**
     * 新增商品属性
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id'   => 'required|integer',
            'attribute_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('新增属性出错');
        }

        $arr = [
            'attribute_id' => $request->attribute_id,
            'product_id'   => $request->product_id,
        ];
        $data = $this->app['productAttribute']->store($arr);
        if ($data['status']) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error($data['msg']);
    }
}
