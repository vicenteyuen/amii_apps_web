<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BrandProductController extends Controller
{
    public function store(Request $request)
    {
        $arr= [
            'brand_id' => $request->brand_id,
            'product_id' => $request->product_id,
        ];
        $data = $this->app['brandProduct']->store($arr);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error();
    }

    // 插入全部或批量
    public function insertAll(Request $request)
    {
        $data = $this->app['brandProduct']->insertBrandProAll($request->brand_id,  $request->productIds);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }

}
