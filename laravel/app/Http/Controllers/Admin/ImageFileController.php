<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ImageFileController extends Controller
{
    //index
    public function index()
    {
        $imageFiles = $this->app['imageFile']->index();
        return view('admin.image.index-file', compact('imageFiles'));
    }

    //create
    public function create()
    {
        $url = action('Admin\ImageFileController@store');
        return view('admin.image.create-file', compact('url'));
    }

    //show
    public function showEdit($id)
    {
        $url = action('Admin\ImageFileController@updates');
        $file = $this->app['imageFile']->find($id);
        return view('admin.image.create-file', compact('url', 'file'));
    }

    //store
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name'  => 'required|string',
        ]);
        if ($validator->fails()) {
            $errors = ['文件名不能为空'];
            return back()->with(compact('errors'));
        }
        $data = $this->app['imageFile']->store(['name' => $request->name,'desc' => $request->desc?:'']);
        if ($data) {
            return redirect(action('Admin\ImageFileController@index'));
        }
        $errors = ['新增出错'];
        return back()->with(compact('errors'));

    }

    //update
    public function updates(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name'  => 'required|string',
                'id'    => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = ['文件名不能为空'];
            return back()->with(compact('errors'));
        }
        $data = $this->app['imageFile']->update($request->id, ['name' => $request->name,'desc' => $request->desc?:'']);
        if ($data) {
            return redirect(action('Admin\ImageFileController@index'));
        }
        $errors = ['编辑出错'];
        return back()->with(compact('errors'));
    }

    //show
    public function showFileImage()
    {
        $imageFiles = $this->app['imageFile']->index();
        if (!$imageFiles->isEmpty()) {
            $fileId = $imageFiles[0]->id;
            $images = $this->app['imagestore']->fileImage($fileId);
        } else {
            $fileId = 0;
            $images = [];
        }
        $index = $fileId;
        return view('admin.image.file-image', compact('imageFiles', 'images', 'index'));
    }

    // 选中文件夹
    public function selectFile($id)
    {
        $index = $id;
        $imageFiles = $this->app['imageFile']->index();
        $images = $this->app['imagestore']->fileImage($id);
        return view('admin.image.file-image', compact('imageFiles', 'images', 'index'));
    }

    public function delete(Request $request)
    {
        $delete = $this->app['imageFile']->delete($request->id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');
    }
}
