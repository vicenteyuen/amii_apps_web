<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class ExpressTemplateController extends Controller
{
    //新增页面
    public function create(Request $request)
    {
        $provice = app('address')->province();
        return view('admin.express.create-template', compact('provice'));

    }

    // 列表
    public function index()
    {
        $templates = $this->app['template']->index();
        return view('admin.express.index-template', compact('templates'));
    }

    // 新增
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name'          => 'required',
                'free'          => 'required|integer',
                'default_fee'   => 'required_if:free,0|numeric',
                'free_money'    => 'numeric',
                'free_fee'      => 'integer',
        ], [
                'name.required' => '模版名称不能为空',
                'default_fee.required_if' =>  '默认邮费不能为空',
                'default_fee.numeric'     =>  '默认邮费必须是数字',
                'free_fee.integer'    =>  '包邮条件必须是整数',
                'free_money.numeric'  =>  '包邮金额必须是数字',

        ]);

        if ($validator->fails()) {
             $errors = $validator->errors()->toArray();
             return back()->withInput()->with(compact('errors'));
        }
        $arr = array_filter(array_only($request->all(), ['name','free','default_fee','free_fee','free_money']));
        if (!array_key_exists('free', $arr)) {
            $arr['free'] = 0;
        }
        $defaultFee = $request->default_fees ? array_filter($request->default_fees):null;
        $province   = $request->province?array_filter($request->province):null;
        $template = $this->app['template']->store($arr, $defaultFee, $province);
        if ($template) {
            return redirect(url('/admin/express'));
        }
        $errors = ['保存失败'];
        return back()->with(compact('errors'));
    }

    // 编辑页面
    public function showEdit($id)
    {
        $provice = app('address')->province();
        $template = $this->app['template']->show($id);
        return view('admin.express.create-template', compact('provice', 'template', 'id'));
    }

    // 编辑
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name'          => 'required',
                'free'          => 'required|integer',
                'default_fee'   => 'required_if:free,0|numeric',
                'free_money'    => 'numeric',
                'free_fee'      => 'integer',
        ], [
                'name.required' => '模版名称不能为空',
                'default_fee.required_if' =>  '默认邮费不能为空',
                'default_fee.numeric'     =>  '默认邮费必须是数字',
                'free_fee.integer'    =>  '包邮条件必须是整数',
                'free_money.numeric'  =>  '包邮金额必须是数字',

        ]);

        if ($validator->fails()) {
             $errors = $validator->errors()->toArray();
             return back()->with(compact('errors'));
        }
        $arr = array_filter(array_only($request->all(), ['name','free','default_fee','free_fee','free_money']));
        if (!array_key_exists('free', $arr)) {
            $arr['free'] = 0;
        }
        $defaultFee = $request->default_fees ? array_filter($request->default_fees):null;
        $province   = $request->province?array_filter($request->province):null;
        $template = $this->app['template']->save($request->id, $arr, $defaultFee, $province);
        if ($template) {
            return redirect(url('/admin/express'));
        }
        $errors = ['保存失败'];
        return back()->with(compact('errors'));
    }

    // 删除
    public function delete(Request $request)
    {
        $delete = $this->app['template']->delete($request->id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
    
    // 详情
    public function show($id, Request $request)
    {
        $template = $this->app['template']->show($id);
       
        return view('admin.express.detail-template', compact('template', 'products'));
    }

    //运费模版商品列表
    public function templateProductIndex(Request $request)
    {
        $products = $this->app['template']->showTemplateProduct($request->id);
        $template = $this->app['template']->show($request->id);
        return view('admin.express.index-product-template', compact('products', 'template'));
    }

    // 添加运费模版商品
    public function storeTemplateProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'productIds'    => 'required|array',
                'templateId'    => 'required',

        ], [
                'productIds.required' => '未选中商品',
                'productIds.array' =>  '商品提交格式出错',
                'templateId.required' => '运费模版出错',

        ]);
        if ($validator->fails()) {
             $errors = $validator->errors()->toArray();
             return back()->with(compact('errors'));
        }
        $data = $this->app['template']->storeProduct($request->templateId, $request->productIds);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');


    }

    // 添加运费模版页面/商品search
    public function createTemplateProduct(Request $request)
    {
        $template = $this->app['template']->show($request->id);
        $products = $this->app['product']->productSearch(trim($request->key), $request);
        return view('admin.express.add-product-template', compact('template', 'products'));
    }

    // 一键添加运费模版
    public function setAllProductExpressTemplate(Request $request)
    {
        $data = $this->app['template']->setAllProductExpressTemplate($request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
