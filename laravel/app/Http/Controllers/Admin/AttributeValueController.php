<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Gimage\Gimage;

class AttributeValueController extends Controller
{

    /**
     * 商品属性列表
     * @return [type] [description]
     */
    public function show(Request $request, $id)
    {
        $datas = $this->app['attributeValue']->show($id);
        if (!$datas->isEmpty()) {
            return $this->app['jsend']->success($datas);
        }
        return $this->app['jsend']->error('暂无属性');

    }
    /**
     * 属性值
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function editShow(Request $request)
    {
        $attributeValue = $this->app['attributeValue']->find($request->id);
        $attributeName = $attributeValue->attr->name;
        return view('admin.attribute.attributeValue', compact('attributeValue', 'attributeName'));


    }

    /**
     * 属性值
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function addShow(Request $request)
    {
        $attributeName = $this->app['attribute']->find($request->id);
        $attributeName = $attributeName->name;
        return view('admin.attribute.attributeValue', compact('attributeName'));

    }

    /**
     * 新增属性值
     * @param Request $request [description]
     */
    public function add(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'attribute_id' => 'required|integer',
                'value'        => 'required|string',
                'status'       => 'required|integer',
                'weight'       => 'integer',
                'image'        => 'image',
            ]);

            if ($validator->fails()) {
                return $this->app['jsend']->error('数据格式出错');
            }

            $arr = [
                'status'       => $request->status,
                'attribute_id' => $request->attribute_id,
                'weight'       => $request->weight ?: 0,
                'value'        => trim($request->value),
            ];

            $file = $request->file('image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'product', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['image'] = $storeFile;
            }

            $data = $this->app['attributeValue']->create($arr);
            if ($data['status']) {
                return $this->app['jsend']->success($data);
            }
            return $this->app['jsend']->error($data['msg']);
    }

   /**
    * 编辑属性值
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function edits(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'attribute_id' => 'required|integer',
                'value'        => 'required|string',
                'status'       => 'required|integer',
                'weight'       => 'integer',
                'image'        => 'image',
            ]);

            if ($validator->fails()) {
                return $this->app['jsend']->error('数据格式出错');
            }

            $arr = [
                'status'       => $request->status,
                'weight'       => $request->weight ?: 0,
                'value'        => trim($request->value),
            ];

            $file = $request->file('image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'product', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['image'] = $storeFile;
            }

            $id = $request->attribute_id;
            $data = $this->app['attributeValue']->update($id, $arr);
            if ($data) {
                return $this->app['jsend']->success();
            }
            return $this->app['jsend']->error('编辑失败,请刷新重试');
    }

    public function delete(Request $request)
    {
        $data = $this->app['attributeValue']->delete($request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败,请刷新重试');
    }
}
