<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Gimage\Gimage;

class VideoController extends Controller
{
    public function index()
    {
        $videos = $this->app['video']->adminIndex();
        return view('admin.video.index', compact('videos'));
    }

    public function create()
    {
        return view('admin.video.create_edit');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image'       => 'required|file|image',
            'title'       => 'required|string',
            'video'       => 'required|string',
            'description' => 'required|string',
            'weight'      => 'numeric',

        ], [
            'image.required'       => '图片不能为空',
            'image.image'          => '图片格式出错',
            'title.required'       => '标题不能为空',
            'description.required' => '描述不能为空',
            'video.required'       => '视频不能为空',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return redirect()->back()->withInput()->with(compact('errors'));
        }

        $arr = [
            'title'       => $request->title,
            'description' => $request->description,
            'video'       => $request->video,
            'weight'      => $request->weight,
        ];
        $file = $request->file('image');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'video', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
            $arr['image'] = $storeFile;
        }
        $subject = $this->app['video']->store($arr);
        if ($subject) {
            return redirect(action('Admin\VideoController@index'));
        }

        return back();
    }

    public function edit($id)
    {
        $video = $this->app['video']->find($id);
        return view('admin.video.create_edit', compact('video'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image',
            'title' => 'required|string',
            'description' => 'required|string',
            'video' => 'required|string',
             'weight'      => 'numeric',
        ], [
            'image.image'          => '图片格式出错',
            'title.required'       => '标题不能为空',
            'description.required' => '描述不能为空',
            'video.required'       => '视频不能为空',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();

            return back()->with(compact('errors'));
        }

        $arr = [
            'title'       => $request->title,
            'description' => $request->description,
            'video'       => $request->video,
            'weight'      => $request->weight,

        ];

        if ($request->editIden == -1) {
            $file = $request->file('image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'video', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['image'] = $storeFile;
            }
        }
        $video = $this->app['video']->update($id, $arr);
        if ($video) {
            return redirect(action('Admin\VideoController@index'));
        }

        return back();
    }

    public function destroy($id)
    {
        $video = $this->app['video']->delete($id);

        if ($video) {
            return $this->app['jsend']->success();
        } else {
            return $this->app['jsend']->error();
        }
    }

    /**
     * 视频上传
     *
     * @param Request $request
     * @return mixed
     */
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = $request->name;

        $file_path = storage_path('temp/').$file_name;
        $file_part_path = $file_path . '.part';

        $in = @fopen($file, 'rb');
        $out = @fopen($file_part_path, 'ab');
        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }
        @fclose($out);
        @fclose($in);

        $chunk = (int) $request->get('chunk', false);
        $chunks = (int) $request->get('chunks', false);
        if ($chunk == $chunks - 1) {
            rename($file_part_path, $file_path);
            $file = new UploadedFile($file_path, $file_name, 'blob', sizeof($file_path), UPLOAD_ERR_OK, true);
            $file_name = $this->app['fileUpload']->upload($file_path, $file, 'videos');
            @unlink($file_path);

            if ($file_name) {
                return $this->app['jsend']->success(compact('file_name'));
            } else {
                return $this->app['jsend']->error('上传失败，请重试');
            }
        }
    }

    public function status(Request $request)
    {
        $status = $this->app['video']->update($request->id, ['status' => $request->status]);
        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
