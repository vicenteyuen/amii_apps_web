<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gimage;
use Validator;

class SellProductController extends Controller
{


    /**
     * 新增商品与销售形式的关联
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
            'sell_id'    => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }

        $arr = [
            'product_id'   => $request->product_id,
            'sell_id'      => $request->sell_id,
        ];

        $create = $this->app['sellproduct']->store($arr);
        if ($create) {
            return $this->app['jsend']->success(['id' => $create->id]);
        }
        return $this->app['jsend']->error('新增失败');
    }

     /**
     * 添加 普通库存 积分库存
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function addSellType(Request $request)
    {
        $sellProduct = $this->app['sellproduct']->addSellType($request->product_id, $request->provider);
        if ($sellProduct) {
            return $this->app['jsend']->success($sellProduct);
        }
        return $this->app['jsend']->error('添加库存类别失败');
    }


    /**
     * 新增库存
     * @param Request $request [description]
     */
    public function addInventory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('库存必须为数字');
        }
        $arr = [
            'number'            => $request->number,
            'product_id'        => $request->product_id,
            'product_sell_id'    => $request->id,
        ];
        $update = $this->app['sellproduct']->update($arr, $request->number);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('新增库存失败');

    }

    /**
     * 上传主图
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function upload(Request $request)
    {
        if ($request->uploadType == 'imagesFolder') {
            //从图片列表中选择图片
            $storeFile = $request->selectImage;
        } else if ($request->uploadType == 'inputFile') {
            $file = $request->file('main_image');
            // 上传图片
            if (!$file) {
                return $this->app['jsend']->error('上传图片失败,请选择图片再上传');
            }
            $storeFile = Gimage::uploadImage($file, 'product', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }
        } else {
            return $this->app['jsend']->error('上传图片失败,请选择图片再上传');
        }
        $arr = [
            'id' => $request->id,
            'image' => $storeFile,
        ];
        $saveImage = $this->app['sellproduct']->uploadImage($arr);
        if ($saveImage) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('上传图片失败');
    }

     /**
     * 保存商品卖点
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function saveSellPoint(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'                => 'required|integer',
            'sell_point'        => 'required',
        ]);
        $arr = [
            'sell_point' => $request->sell_point,
        ];

        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }

        $update = $this->app['sellproduct']->saveSellPoint($request->id, $arr);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('保存失败');
    }


    /**
     * 保存商品基本价格 商品积分
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function savePrice(Request $request)
    {
        switch ($request->provider) {
            case '1':    //普通库存基本价格
                $validator = Validator::make($request->all(), [
                    'id'                => 'required|integer',
                    'base_price'        => 'required|numeric',
                    'base_mark_price'   => 'required|numeric',
                ]);
                $arr = [
                    'base_price' => $request->base_price,
                    'market_price' =>$request->base_mark_price,
                ];
                break;
            
            case '2':    //积分库存
                $validator = Validator::make($request->all(), [
                  'id'                => 'required|integer',
                  'product_point'     => 'required|integer',
                  'base_mark_price'   => 'required|numeric',
                ]);
                $arr = [
                  'product_point' => $request->product_point,
                   'market_price' =>$request->base_mark_price,
                ];
                break;

            default:
                return $this->app['jsend']->error('参数出错');
        }
    
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }

        
        $update = $this->app['sellproduct']->save($request->id, $arr);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('保存失败');
    }

    // 更改游戏块数
    public function savePuzzles(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'             => 'required|integer',
            'puzzles'        => 'required|integer|in:4,6,9,12,16,20,25',
        ]);

        $arr = [
           'puzzles' => $request->puzzles,
        ];
        if ($validator->fails()) {
            return $this->app['jsend']->error('拼图块数不在格数范围内');
        }
        $update = $this->app['sellproduct']->save($request->id, $arr);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('保存失败');
    }

    /**
     * 商品上架或下架
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function status(Request $request)
    {
        $arr = ['status' => $request->status];
        $update = $this->app['sellproduct']->updataStatus($request->id, $arr);
        if ($update['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($update['msg']);

    }

    /**
     * 删除关联
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request, $id)
    {
        $delete = $this->app['sellproduct']->delete($id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');

    }

    /**
     * 游戏商品上下架
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function gameStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'     => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }

        $arr = [
            'id'    => $request->id,
            'status' => $request->status ? 1 : 0,
        ];
        $update = $this->app['sellproduct']->gameSellproductDownOrUp($arr);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('操作失败');
    }

    public function muliGameStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids'     => 'required|array',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }
        $update = $this->app['sellproduct']->muliGameSellproductDownOrUp($request->ids, $request->status);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('操作失败');
    }


    // 更新商品状态
    public function sellProductStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'        => 'required',
            'status'    => 'required',
            'provider'  => 'required'
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }

        $update = $this->app['sellproduct']->SellproductDownOrUp($request->id, $request->provider, $request->status);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('操作失败');
    }


    // 批量更新商品状态
    public function muliSellproductStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ids'       => 'required|array',
            'status'    => 'required',
            'provider'  => 'required'
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('参数出错');
        }
        $update = $this->app['sellproduct']->muliSellproductDownOrUp($request->ids, $request->provider, $request->status);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('操作失败');
    }
}
