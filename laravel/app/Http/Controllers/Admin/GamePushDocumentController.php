<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class GamePushDocumentController extends Controller
{
    public function index()
    {
        $documents =  $this->app['game.push.document']->index();
        return view('admin.game.push-document', compact('documents'));
    }

    public function showEdit($id)
    {
        $document = $this->app['game.push.document']->show($id);
        return view('admin.game.push-document-edit', compact('document'));

    }

    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'desc'  => 'required',
                'hours' => 'required_if:provider,2,3|numeric',
        ], [
                'desc.required'     => '文案不能为空',
                'hours.required_if' => '倒计时不能为空',
                'hours.numeric'     => '倒计时格式出错',
        ]);

        if ($validator->fails()) {
             $errors = $validator->errors()->toArray();
             return back()->withInput()->with(compact('errors'));
        }

        $arr = [
            'desc' => $request->desc,
        ];

        if ($request->hours) {
            $arr['hours'] = $request->hours;
        }
        $template = $this->app['game.push.document']->update($request->id, $arr);
        if ($template) {
            return redirect(url('/admin/game/push/document'));
        }
        $errors = ['保存失败'];
        return back()->with(compact('errors'));
    }
}
