<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\SummernoteHelper;
use Illuminate\Http\Request;
use App\Helpers\AmiiHelper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Gimage\Gimage;

class ProductController extends Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 获取新增商品页
     */
    public function create()
    {
        $brand = $this->app['brand']->showAll();
        $expressTemplates = $this->app['template']->index();
        return view('admin.product.create', compact('brand', 'expressTemplates'));
    }

    /**
     * 商品列表
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        if (empty($request->page_number)) {
            $request->page_number = 20;
        }

        $products = $this->app['product']->adminIndex(trim($request->search), $request, $request->page_number);
        $expressTemplates = $this->app['template']->index();
        $brands = $this->app['brand']->get();
        $providers = [
            ['id' => 1,'name' =>'普通商品'],
            ['id' => 2,'name' =>'积分商品'],
            ['id' => 3,'name' =>'游戏商品'],
            ['id' => 4,'name' =>'礼品商品'],
        ];

        $mainImage = [
            ['id' => 1, 'name' => '有主图'],
            ['id' => 2, 'name' => '无主图'],
        ];
        $status = [2,1,0];
        $pages = [20,50,100,200];
        $sorts = ['asc','desc'];
        return view('admin.product.index', compact('products', 'expressTemplates', 'providers', 'request', 'status', 'sorts', 'brands', 'mainImage', 'pages'));
    }

    /**
     * 获取商品详情
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error($errors);
        }
        $data = $this->app['product']->show($id);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('查询商品详情出错');

    }


    /**
     * 新增商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        switch ($request->type) {
            case '2':
                return $this->detail($request);
                break;

            default:
                # code...
                break;
        }
        $validator = Validator::make($request->all(), [
            'name'          => 'required|string',
            'brief'         => 'string',
            'main_image'    => 'image',
            'status'        => 'required|integer',
            'unit'          => 'string',
            'snumber'       => 'string',
            'express_template_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return $this->app['jsend']->error('商品信息填写不完整或格式出错');
        }
        $arr = [
            'name'          => $request->name,
            'brief'         => $request->brief,
            'status'        => $request->status,
            'unit'          => $request->unit,
            'snumber'       => $request->snumber ?: 0,
            'express_template_id' => $request->express_template_id,
        ];
        if ($request->uploadType == 'imagesFolder') {
            //从图片列表中选择图片
            $storeFile = $request->selectImage;
            $arr['main_image'] = $storeFile;
        } else if ($request->uploadType == 'inputFile') {
            // 上传图片
            $file = $request->file('main_image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'product', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['main_image'] = $storeFile;
            }
        }

        if (!$request->snumber) {
            $arr['snumber'] = AmiiHelper::generateProductSn();
        }
        $data = $this->app['product']->store($arr);
        if ($data) {
            return $this->app['jsend']->success(['id' => $data->id, 'type' => 2]);
        }

        return $this->app['jsend']->error('新增商品失败');
    }

    public function showEdit(Request $request)
    {
        $product      = $this->app['product']->detail($request->product_id);
        $productImage = $this->app['productImage']->productShow($request->product_id);
        $brand = $this->app['brand']->showAll();
        $expressTemplates = $this->app['template']->index();
        return view('admin.product.edit', compact('product', 'productImage', 'brand', 'expressTemplates'));

    }

    /**
     * 编辑商品
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required|string',
            'brief'         => 'string',
            'status'        => 'required|integer',
            'main_image'    => 'image',
            'unit'          => 'string',
            'snumber'       => 'string',
            'express_template_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return $this->app['jsend']->error('商品信息填写不完整或格式出错');
        }

        if (!trim($request->detail)) {
            $detail = '';
        } else {
            $detail = SummernoteHelper::getImgName($request->detail, 'product');
        }
        $arr = [
            'name'          => $request->name,
            'brief'         => $request->brief,
            'status'        => $request->status,
            'unit'          => $request->unit,
            'snumber'       => $request->snumber ?: 0,
            'express_template_id' => $request->express_template_id,
            'detail'        => $detail,
        ];
        if ($request->uploadType == 'imagesFolder') {
            //从图片列表中选择图片
            $storeFile = $request->selectImage;
            $arr['main_image'] = $storeFile;
        } else if ($request->uploadType == 'inputFile') {
            // 上传图片
            $file = $request->file('main_image');
            if ($file) {
                $storeFile = Gimage::uploadImage($file, 'product', 'images');
                if ($storeFile instanceof \Exception) {
                    return response()->json(0);
                }
                $arr['main_image'] = $storeFile;
            }
        }

        if (!$request->snumber) {
            $arr['snumber'] = AmiiHelper::generateProductSn();
        }
        $id = $this->app['product']->update($arr, $request->product_id);
        if ($id) {
            return $this->app['jsend']->success(['id' => $id, 'type' => 2]);
        }

        return $this->app['jsend']->error('编辑商品失败');
    }

    /**
     * 新增详情
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function detail(Request $request)
    {
        $requests = ['product_id' => $request->product_id];
        $validator = Validator::make($requests, [
            'product_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        if (!trim($request->detail)) {
            return $this->app['jsend']->success(['id' => $request->product_id, 'type' => 3]);
        }

        $detail = SummernoteHelper::getImgName($request->detail, 'product');

        $arr = ['detail' => $detail];

        $data = $this->app['product']->updateDetail($arr, $request->product_id);
        if ($data) {
            return $this->app['jsend']->success(['id' => $request->product_id, 'type' => 3]);
        }
        return $this->app['jsend']->error('编辑详情失败');
    }

    /**
     * [delete description]
     * @param  Request $request [description]
     * @param  [type]  $id        [description]
     * @param  boolean $isTrashed [description]
     * @return [type]             [description]
     */
    public function status(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'        => 'required|integer',
            'status'    => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('参数出错');
        }

        $status = $this->app['product']->updateStatus(['status' => $request->status], $request->id);
        if ($status) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('失败');
    }

    /**
     * upload product images
     */
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $storeFile = Gimage::uploadImage($file, 'product', 'images');
        if ($storeFile instanceof \Exception) {
            return response()->json(0);
        }
        echo 'uploads/images/product/' . $storeFile;
    }

    /**
     * 搜索商品
     *
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $products = $this->app['product']->search($keyword);
        if ($products) {
            return $this->app['jsend']->success($products);
        }
        return $this->app['jsend']->error('搜索商品失败');
    }


    /**
     * 商品详情预览
     */
    public function preview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'        => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['jsend']->error('商品ID不能为空');
        }

        $data = $this->app['product']->detail($request->id);
        if (empty($data)) {
            return '空';
        } else {
            return ($data->detail);
        }
    }

    // 预览轮播图
    public function previewImage($id)
    {
        $images = $this->app['sellproduct']->previewImage($id);
        $previewImage = '';
        foreach ($images as $key => $value) {
            $previewImage.='<div ><img width="100%" src='.$value.'></div>';
        }
        if (!$previewImage) {
            return '空';
        } else {
            return $previewImage;
        }
    }

    //serach
    public function productSearch(Request $request)
    {
        $data = $this->app['product']->productSearch($request->key);
        if (!$data->isEmpty()) {
             return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error('查询不到商品');

    }

    //删除主图
    public function deleteMainImage(Request $request)
    {
        $data = $this->app['product']->deleteMainImage($request->id);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');

    }

    //更新运费模版
    public function updateTemplate(Request $request)
    {
        $data = $this->app['product']->updateTemplate($request->ids, $request->templateId);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('批量增加失败');
    }

    // 更换图片
    public function change(Request $request)
    {
        $productId = $request->product_id;
        $sellProductId = $request->sell_product_id;
        $data = $this->app['product']->change($productId, $sellProductId);
        if ($data) {
             return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
