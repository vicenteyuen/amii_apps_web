<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Gimage\Gimage;

class BrandController extends Controller
{
    public function index()
    {
        $brands = $this->app['brand']->index();
        return view('admin.brand.index', compact('brands'));
    }

    // 品牌新增页
    public function create()
    {
        $categories = $this->app['category']->index();
        return view('admin.brand.create', compact('categories'));
    }

    // 添加商品
    public function add(Request $request)
    {
        $brand_id = $request->id;
        $brand = $this->app['brand']->brandCategory($request->id);
        return view('admin.brand.create-product', compact('brand', 'brand_id'));

    }

    // 品牌编辑
    public function showEdit(Request $request)
    {
        $categories = $this->app['category']->index();
        $brand = $this->app['brand']->detail($request->id);
        $brandCategory = $brand->brandCategory->pluck('category_id')->toArray();
        return view('admin.brand.create', compact('brand', 'categories', 'brandCategory'));
    }

    // 商品列表
    public function productShow(Request $request)
    {
        $products = $this->app['brandProduct']->index($request->id, $request);
        return view('admin.brand.brand-product', compact('products'));
    }

    //serach
    public function brandSearch(Request $request)
    {
        $brand_id = $request->brand_id;
        $brand = $this->app['brand']->detail($request->brand_id);
        $products  = $this->app['product']->searchBrandProduct($request->key, $request);
        return view('admin.brand.create-product', compact('products', 'brand'));

    }

    //新增品牌
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string',
            'hot'       => 'integer',
            'categories'=> 'array',
            'logo'      => 'required|image',
            'recommend'    => 'integer',
        ]);

        if ($validator->fails()) {
              return $this->app['jsend']->error('品牌信息填写不完整或格式出错');
        }

        $arr = [
            'name'          => trim($request->name),
            'hot'           => $request->hot ?: 0,
            'recommend'     => $request->recommend,
        ];

        // 上传图片
        $file = $request->file('logo');

        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'brand', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }

            $arr['logo'] = $storeFile;
        }
        $data = $this->app['brand']->store($arr, $request->categories);
        if ($data['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($data['msg']);
    }

     //编辑品牌
    public function edits(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string',
            'hot'       => 'integer',
            'categories'=> 'array',
            'logo'      => 'image',
            'recommend' => 'integer',

        ]);

        if ($validator->fails()) {
              return $this->app['jsend']->error('品牌信息填写不完整或格式出错');
        }

        $arr = [
            'name'          => trim($request->name),
            'hot'           => $request->hot ?: 0,
            'recommend'     => $request->recommend,
        ];

        // 上传图片
        $file = $request->file('logo');
        if ($file) {
            $storeFile = Gimage::uploadImage($file, 'brand', 'images');
            if ($storeFile instanceof \Exception) {
                return response()->json(0);
            }

            $arr['logo'] = $storeFile;
        }
        $data = $this->app['brand']->update($arr, $request->categories, $request->id);
        if ($data['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($data['msg']);
    }

    /**
     * 删除品牌
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        $data = $this->app['brand']->delete($request->id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }

    /**
     * 品牌类别
     * @return [type] [description]
     */
    public function brandCategory(Request $request)
    {
        $data = $this->app['brand']->brandCategory($request->brand_id);
        if ($data) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error();
    }

    /**
     * 移除品牌商品
     * @return [type] [description]
     */
    public function deleteBrand(Request $request)
    {
        $data = $this->app['brand']->deleteBrand($request->brand_id, $request->product_id);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }

    public function deleteBrandProduct(Request $request)
    {
        $data = $this->app['brand']->deleteBrandProduct($request->brand_id, $request->ids);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error();
    }
}
