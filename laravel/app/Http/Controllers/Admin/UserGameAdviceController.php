<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class UserGameAdviceController extends Controller
{
    /**
    * 用户游戏反馈列表
    * @return [type] [description]
    */
    public function index(Request $request)
    {
        $UserGameAdvices = $this->app['gameAdvice']->getUserAdvices();
        return view('admin.game_advice.index', compact('UserGameAdvices'));
    }
    /**
    * 用户游戏反馈列表
    * @return [type] [description]
    */
    public function details(Request $request)
    {
        $advice_id = $request->adviceId;
        $validator = Validator::make($request->all(), [
            'adviceId' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return back()->with(compact('errors'));
        }
        $UserGameAdvicesDetail = $this->app['gameAdvice']->getUserAdvicesDetail($advice_id);
        return view('admin.game_advice.detail', compact('UserGameAdvicesDetail'));
    }

    /**
    * 更改反馈采纳状态
    * @return [type] [description]
    */
    public function gameAdviceStatus(Request $request)
    {
        $advice_id = $request->id;
        $arr = [
            'status' => $request->status,
        ];
        $res = $this->app['gameAdvice']->updateAdvicesStatus($advice_id, $arr);
        if ($res) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('采纳失败');
    }
}
