<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class SellAttributeController extends Controller
{

    /**
     * 新增
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_sell_attribute_id'   => 'required|integer',
            'attribute_id'                => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->app['jsend']->error('新增属性出错');
        }

        $arr = [
            'product_sell_attribute_id'   => $request->product_sell_attribute_id,
             'attribute_id'                => $request->attribute_id,
        ];

        $data = $this->app['sellAttribute']->store($arr);
        if ($data['status']) {
            return $this->app['jsend']->success($data);
        }
        return $this->app['jsend']->error($data['msg']);
    }

    public function index(Request $request)
    {
        $data = $this->app['sellAttribute']->attributeValue($request->id);
        if (!$data->isEmpty()) {
            return $this->app['jsend']->success($data);
        }
        return ;
        // return $this->app['jsend']->error('获取属性值列表出错');
    }
}
