<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class FirstPurchaseController extends Controller
{
    public function create()
    {
        $money = $this->app['amii.fisrt.product']->purchaseMoney();
        return view('admin.purchase.purchase-first-create', compact('money'));
    }

    //搜索
    public function search(Request $request)
    {
        $data = $this->app['product']->firstPurchaseSearch($request->key);
        return $this->app['jsend']->success($data);
    }

    //当前赠品
    public function index()
    {
        $purchase = $this->app['amii.fisrt.product']->product();
        return view('admin.purchase.purchase-first-index', compact('purchase'));
    }

    public function store(Request $request)
    {
        $data = $this->app['amii.fisrt.product']->store(['sell_product_id' =>$request->sell_product_id]);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('添加首单商品失败');
    }

    public function saveMoney(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'money' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return $this->app['jsend']->error('fail');
        }
        $data = $this->app['amii.fisrt.product']->saveMoney(['money' =>$request->money]);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }


    public function update(Request $request)
    {
        $arr = array_only($request->all(), ['id','status']);
        $data = $this->app['amii.fisrt.product']->update($arr);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
    public function history()
    {
        $purchases = $this->app['amii.give.history.product']->index();
        return view('admin.purchase.purchase-history-index', compact('purchases'));
    }
}
