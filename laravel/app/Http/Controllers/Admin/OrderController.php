<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class OrderController extends Controller
{
    //订单列表
    public function index(Request $request)
    {
        $orders = $this->app['order']->adminIndex($request);
        $status = ['all','paying','sending','receiving','appraising','refunding','cancel','success'];
        $providers = ['standard','point','game'];
        return view('admin.order.index', compact('orders', 'status', 'request', 'providers'));
    }

    // 用户订单
    public function userOrder(Request $request)
    {
        $orders = $this->app['order']->adminUserOrderIndex($request->user_id);
        return view('admin.member.order-index', compact('orders'));
    }

    //用户订单详情
    public function userOrderDetail($orderSn)
    {
        $order = $this->app['order']->adminShow($orderSn);
        // 物流信息
        $express = $this->app['express']->getExpress($orderSn);

        // 快递公司列表
        $expressIndex = $this->app['express']->index();
        return view('admin.member.order-detail', compact('order', 'express', 'orderSn', 'expressIndex'));
    }

    //订单详情
    public function show(Request $request, $orderSn)
    {
        $order = $this->app['order']->adminShow($orderSn);
        // 物流信息
        $express = $this->app['express']->getExpress($orderSn);
        // 快递公司列表
        $expressIndex = $this->app['express']->index();

        return view('admin.order.detail', compact('order', 'express', 'orderSn', 'expressIndex'));
    }

    // 开启/关闭售后
    public function canRefund($orderSn, $can_refund)
    {
        $refund = $this->app['order']->openRefund($orderSn, $can_refund);
        if ($refund) {
            return back();
        }
        $errors = ['error' => '开启售后失败,请刷新重试'];
        return back()->with(compact('errors'));
    }

    // 后台发货
    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'express_id'  => 'required',
            'shipping_sn' => 'required',
            'orderSn'     => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            $errors = array_shift($errors);
            return back()->with(compact('errors'));
        }

        $data['com']  = $request->express_id;
        $data['shipping_sn'] = $request->shipping_sn;
        $data['orderSn']     = $request->orderSn;
        // 发货
        $send = $this->app['order']->adminOrderSend($data);
        if ($send) {
            return redirect('/admin/order');
        }
        $errors = ['error' => '发货失败,请刷新重试'];
        return back()->with(compact('errors'));
    }
}
