<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\OrderRefund;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gimage\Gimage;

use Validator;

class OrderProductRefundController extends Controller
{
   
    /**
     * 同意售后
     * @param  [type] $order_product_id [description]
     * @return [type]                   [description]
     */
    public function agree(Request $request)
    {
        $data = $this->app['amii.order.refund']->adminAgreeRefund($request->id, $request->content);
        if ($data['status']) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error($data['message']);
    }

    /**
     * 拒绝申请
     * @param  [type] $order_refund [description]
     * @return [type]               [description]
     */
    public function refuse(Request $request)
    {
        if (empty($request->reason)) {
            return $this->app['jsend']->error('拒绝失败，拒绝原因不能为空');
        }
        $data = $this->app['amii.order.refund']->refuse($request->id, $request->reason);
        if ($data) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('拒绝申请失败，请刷新重试');
    }

    /**
     * 确认收货
     * @return [type] [description]
     */
    public function receive(Request $request)
    {
        $data = $this->app['amii.order.refund']->received($request->id);
        if ($data) {
            return  $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('确认收货失败，请刷新重试');
    }
}
