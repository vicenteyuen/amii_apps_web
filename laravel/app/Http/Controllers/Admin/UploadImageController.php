<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UploadImageController extends Controller
{
    /**
     * 上传图片
     * @param  [type] $file [description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    public static function upload($file, $path)
    {
        $extension = $file->getClientOriginalExtension();
        $fileName = md5(time()).random_int(5, 5).".".$extension;
        $file->move($path, $fileName);
        return $fileName;

    }
}
