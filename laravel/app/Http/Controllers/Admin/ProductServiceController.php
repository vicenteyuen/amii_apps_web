<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductServiceController extends Controller
{
    public function index()
    {
        $services = $this->app['amii.product.service']->adminIndex();
        return view('admin.product_service.index', compact('services'));
    }

    public function update(Request $request)
    {
        $update = $this->app['amii.product.service']->update($request->id, $request->status);
        if ($update) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('fail');
    }
}
