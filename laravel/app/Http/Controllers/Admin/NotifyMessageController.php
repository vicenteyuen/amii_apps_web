<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class NotifyMessageController extends Controller
{
    //显示消息推送记录
    public function index(Request $request){

        if ($request->search) {

            $notifymessages = $this->app['amii.notify.message']->search($request->search);
        } else {

            $notifymessages = $this->app['amii.notify.message']->adminIndex();
        }

        return view('admin.notifymessage.index', compact('notifymessages'));
    }

    //编辑系统消息
    public function editNotify(Request $request)
    {
        return view('admin.notifymessage.edit_notify');
    }

    //系统消息发送
    public function notifyPlush(Request $request)
    {
        $validators = Validator::make($request->all(), [
            'title'   => 'required',
            'content' => 'required'
        ]);
        if ($validators->fails()) {
            return $this->app['jsend']->error('消息不完整！');
        }

        $data['title']      = $request->title;
        $data['content']    = $request->content;
        $data['notifyData'] = ['action' => 'notify'];

        //系统消息推送
        $res = $this->app->make('leancloud', ['push'])->globalPush($data);

        if ($res) {
            $arr['title']     = $request->title;
            $arr['content']   = $request->content;
            $arr['user_id']   = 0;
            $arr['provider']  = 'notify';

            $res = $this->app['amii.notify.message']->store($arr);
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('发送失败！');
    }

    //系统消息列表
    public function notifyIndex(Request $request)
    {
        $notifymessages = $this->app['amii.notify.message']->notifyList();
        return view('admin.notifymessage.notify_index', compact('notifymessages'));
    }

    //查看系统消息详情
    public function showNotify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

        $notifymessage = $this->app['amii.notify.message']->showNotify($request->id);
        return view('admin.notifymessage.show_notify', compact('notifymessage'));
    }

    //查看物流消息详情
    public function showDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return back()->with(compact('errors'));
        }

        $notifymessage = $this->app['amii.notify.message']->showExpress($request->id);
        // 信息状态替换
        if ($notifymessage->status == 0 ){
            $notifymessage->status = '未读';
        }elseif($notifymessage->status == 1){
            $notifymessage->status = '已读';
        }else{
            $notifymessage->status = '失效';
        }

        return view('admin.notifymessage.show_detail', compact('notifymessage'));
    }
}
