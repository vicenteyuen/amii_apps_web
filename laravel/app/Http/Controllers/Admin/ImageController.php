<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

use Gimage\Gimage;

class ImageController extends Controller
{
    /**
     * get images
     */
    public function index(Request $request)
    {
        // return view('admin.image.test');
        $images = $this->app['imagestore']->adminIndex();
        $files = $this->app['imageFile']->index();
        if ($this->isAjax) {
            return Response::json($images);
        }
        return view('admin.image.index', compact('images', 'files'));
    }

    public function pageImage(Request $request)
    {
        $provider = $request->provider;
        $images = $this->app['imagestore']->pageImage($provider, $request->page);
        if ($this->isAjax) {
            return Response::json($images);
        }
    }

    /**
     * store image
     */
    public function store(Request $request, $provider = null)
    {
        $arr = explode('_', $provider);
        $provider = $arr[0];
        $imageFileId = $arr[1];
        if (! in_array($provider, config('amii.imageUploadProvider'))) {
            return response()->json(0);
        }
        $file = $request->file;
        if (! $file) {
            return response()->json(0);
        }
        $storeFile = Gimage::uploadImage($file, $provider, 'images');
        if ($storeFile instanceof \Exception) {
            return response()->json(0);
        }
        $this->app['imagestore']->store([
            'provider' => $provider,
            'value' => $storeFile,
            'image_file_id' => $imageFileId,
        ]);
        return response()->json(1);
    }

     /**
     * 删除图片
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        $delete = $this->app['imagestore']->delete($request->id);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除属性失败');
    }

    public function deleteAll(Request $request)
    {
        $delete = $this->app['imagestore']->deleteAll($request->imageIds);
        if ($delete) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('删除失败');
    }
}
