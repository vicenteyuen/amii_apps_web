<?php

namespace App\Http\Controllers\Kuaidi100;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PollController extends Controller
{
    /**
     * 快递订阅回调
     */
    public function callback(Request $request)
    {
        if (empty($request->param)) {
            return false;
        }
        if (env('APP_DEBUG')) {
            $param = '{
				"status":"polling",
				"lastResult":{	
				"ischeck":"1",
				"state":"3",
				"nu":"V030344422",
				"data":[
						{

							"context":"上海分拨中心/装件入车扫描 ", 
							"time":"2012-08-28 16:33:19",           
							"ftime":"2012-08-28 16:33:19"
						},
						{
							"context":"上海分拨中心/下车扫描 ",     
							"time":"2012-08-27 23:22:42",          
							"ftime":"2012-08-27 23:22:42"
						},
						{
							"context":"上海分拨中心/下车扫描 ",     
							"time":"2012-08-27 23:22:42",          
							"ftime":"2012-08-27 23:22:42"
						}
					]          
				}
			}';
            $request->param = json_decode($param);
        }
        app('kuaidi100')->handleExpressInfo($request->param);
        return json_encode(array("result" => true,"returnCode" => "200","message" => "成功"));
    }
}
