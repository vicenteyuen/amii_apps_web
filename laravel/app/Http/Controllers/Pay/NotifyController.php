<?php

namespace App\Http\Controllers\Pay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\PaymentHelper;

class NotifyController extends Controller
{
    public function store($provider, Request $request)
    {
        $return = PaymentHelper::notify($provider, $request);

        return $return;
    }
}
