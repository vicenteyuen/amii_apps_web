<?php

namespace App\Http\Controllers\Wechat;

use App\Helpers\EasyWechatHelper;
use App\Libraries\SocialiteOath2\Providers\WempProvider;
use App\Libraries\SocialiteOath2\SocialiteManager;

class DefaultController extends Controller
{
    private $exception = [];

    /**
     * @var WempProvider
     */
    protected $socialite;

    public function __construct()
    {
        parent::__construct();

        $this->socialite = (new SocialiteManager(config('oath2')))
            ->driver('wemp')
            ->scopes(['snsapi_base']);
    }

    public function missingMethod($parameters = [])
    {
        $hash = implode('/', $parameters);
        foreach ($this->exception as $k => $v) {
            if ($hash == $v) {
                return parent::missingMethod($parameters);
                break;
            }
        }
        if (request()->method() == 'GET') {
//            return redirect()->action('Wechat\DefaultController@getIndex', '#' . $hash);
            return $this->getIndex();
        }
        return parent::missingMethod($parameters);
    }

    public function getIndex()
    {
        if (request()->has('uuid')) {
            \Session::set('share_uuid', request('uuid'));
        }
        $wechat_config = null;
        if (strpos(request()->header('user-agent'), 'MicroMessenger') && !strstr(request()->getHost(), '192.168')) {
            if (!session('wemp_pay_open_id')) {
                if (request('code') && request('state')) {
                    session(['wemp_pay_open_id' => $this->socialite->user()->getId()]);

                } else {
                    return $this->socialite->redirect(request()->fullUrl());
                }
            }
            $wechat_config = EasyWechatHelper::wempConfig();
            $wechat_config['debug'] = false;
        }
//        if(!strpos(request()->getRequestUri(), '?')){
//            return redirect()->action('Wechat\DefaultController@getIndex', '#');
//        }
        return view('wechat', compact('wechat_config'));
    }
}
