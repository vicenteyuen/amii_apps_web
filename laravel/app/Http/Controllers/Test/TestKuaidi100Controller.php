<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestKuaidi100Controller extends Controller
{
    /**
     * 订阅快递
     */
    public function poll()
    {
        $poll = $this->app['kuaidi100']->poll('3900409082001');
        if ($poll) {
            return $this->app['jsend']->success();
        }
        return $this->app['jsend']->error('订阅失败');
    }

    /**
     * 查询快递
     */
    public function express()
    {
        if (true) {
            $com = 'zhongtong';
            $nu = '719719752052';
        } else {
            $com = 'yunda';
            $nu = '3900409082001';
        }
        $express = $this->app['kuaidi100']->express($com, $nu);

        return $this->app['jsend']->success(['express' => $express]);
    }
}
