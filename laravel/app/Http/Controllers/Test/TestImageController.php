<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Gimage\Gimage;

class TestImageController extends Controller
{
    public function store(Request $request)
    {
        $file = $request->file;
        $storeFile = Gimage::uploadImage($file, 'product', 'images');
        if ($storeFile instanceof \Exception) {
            return false;
        }
        return $storeFile;
    }
}
