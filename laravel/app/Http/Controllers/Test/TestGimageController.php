<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Gimage\Gimage;


class TestGimageController extends Controller
{


    public function index(Request $request)
    {

        // dd(Gimage::getName());

        // dd('in index');
        // echo "";
        return view('test.gimage.index');


    }



    public function store(Request $request)
    {
        $file = $request->file;
        $storeFile = Gimage::uploadImage($file, 'product', 'images');
        if ($storeFile instanceof \Exception) {
            return false;
        }
        return $storeFile;
    }
}
