<?php

namespace App\Http\Controllers\Test\TaobaoOpen;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Helpers\TaobaoTopHelper;

class TestTaobaoOpenProductController extends Controller
{
    public function getIndex()
    {
        $test = 0;
        if ($test) {
            $merchant = 'amii';
            $startModified = null;
            $endModified = null;
            $pageNo = 1;

            $sessionKey = TaobaoTopHelper::setSessionKey($merchant);

            $c = TaobaoTopHelper::newClass('TopClient');
            $req = TaobaoTopHelper::newClass('ItemsInventoryGetRequest');
            $req->setFields("approve_status,num_iid,title,nick,type,cid,pic_url,num,props,valid_thru,list_time,price,has_discount,has_invoice,has_warranty,has_showcase,modified,delist_time,postage_id,seller_cids,outer_id");
            $req = TaobaoTopHelper::setPageConf($req, $pageNo);
            $req->setOrderBy('modified:asc');
            if ($startModified) {
                $req->setStartModified($startModified);
            }
            if ($endModified) {
                $req->setEndModified($endModified);
            }

            // 请求并格式化数据
            $resp = $c->execute($req, $sessionKey);
            $formatRes = TaobaoTopHelper::formatRes($resp);

            // 存储数据
            if ($formatRes[0] === 0) {
                return $this->app['jsend']->success(['testdata' => $formatRes[1]]);
                app('amii.taobao.open.sync.log')->store($formatRes[1]);
            }
            return $this->app['jsend']->error('错误');
        }

        $this->app['amii.taobao.open.sync.log']->start('amii');
    }
}
