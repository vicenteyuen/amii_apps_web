<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestOrderController extends Controller
{
    /**
     * 支付完成订单
     */
    public function paid($orderSn)
    {
        $paid = app('order')->paid($orderSn);
        if ($paid) {
            dd('支付完成');
        }
        dd('支付失败');
    }

    /**
     * 发货
     */
    public function send($orderSn)
    {
        $send = app('order')->orderSend($orderSn);
        if ($send) {
            dd('发货成功');
        }
        dd('发货失败');
    }

    /**
     * 开启售后订单
     */
    public function openRefund($orderSn)
    {
        //
    }

    /**
     * 售后状态更新
     */
    public function apply($orderId)
    {
        $update = app('order')->updateOrderRefund($orderId);
        if ($update) {
            dd('更新成功');
        }
        dd('更新失败');
    }
}
