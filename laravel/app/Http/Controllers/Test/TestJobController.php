<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Jobs\OrderRebackInventory;

class TestJobController extends Controller
{
    public function index($provider)
    {
        switch ($provider) {
            case 'order':
                $this->testOrder();
                break;

            default:
                # code...
                break;
        }
    }

    private function testOrder()
    {
        $job = (new OrderRebackInventory('order', '123432211'))->delay(60);
        dispatch($job);
    }
}
