<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestLeancloudController extends Controller
{

    // 消息推送
    public function getIndex(Request $request)
    {
        $userId = '';
        $data   = [
            'title'   => '测试消息',
            'content' => '测试的啦！收到请mattermost@下我回复。',
            'notifyData' => [
                "status" => "success",
                "msg"    => "json数据"
            ],
        ];

        $res = $this->app->make('leancloud', ['push'])->globalPush($data);
        // $res = $this->app->make('leancloud', ['push'])->push($userId, $data);
        if ($res) {

            return $this->app['jsend']->success([$res]);
        }
        return $this->app['jsend']->error('消息推送失败');
    }

    // 频道推送
    public function getShow(Request $request)
    {
        $channel = 1;
        $data    = [
            'name' => '尊敬的用户：你好！',
            'msg'   => '短信收到了么？',
        ];
        $res = $this->app->make('leancloud', ['push'])->pushChannel($channel, $data);

        if ($res) {
            return $this->app['jsend']->success($res);
        }
        return $this->app['jsend']->error('消息推送失败');

    }

}
