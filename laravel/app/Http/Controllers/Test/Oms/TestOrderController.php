<?php

namespace App\Http\Controllers\Test\Oms;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestOrderController extends Controller
{
    /**
     * 订单查询
     * @param $pageSize string 每页数据
     * @return array
     */
    public function index(Request $request)
    {
        $orders = $this->app['amii.oms.order']->index($request->all());

        return $orders;
    }

    /**
     * 订单发货
     */
    public function send(Request $request)
    {
        //
    }
}
