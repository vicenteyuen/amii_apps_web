<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestOrderRefundController extends Controller
{
    /**
     * 商户开启用户售后
     */
    public function postOpen(Request $request)
    {
        $orderSn = $request->orderSn;
        // 订单已发货 开启售后
        $open = $this->app['order']->openRefund($orderSn);

        if ($open) {
            dd('开启售后成功');
        }
        dd('开启售后失败');
    }

    /**
     * 用户发起售后
     */
    public function postApply(Request $request)
    {
        $orderSn = $request->orderSn;
        // $apply = $this->app['amii.order.refund']->applyRefund($orderSn, $request);
        $apply = $this->app['amii.order.refund']->applyRefund($orderSn, $request, true);

        if ($apply) {
            dd('申请成功');
        }
        dd('申请失败');
    }

    /**
     * 商户同意售后
     */
    public function postAgree($orderSn)
    {
        $agree = $this->app['order']->agreeRefund($orderSn);


        if ($agree) {
            dd('成功');
        }
        dd('失败');
    }

    /**
     * 商户填写售后收货地址
     */
    public function postShippingAddr($data)
    {
        //
    }

    /**
     * 买家填写售后快递信息
     */
    public function postExpress($data)
    {
        //
    }

    /**
     * 商户收到快递
     */
    public function postReceive($orderSn)
    {
        //
    }

    /**
     * 商户退款
     */
    public function postPay($orderSn)
    {
        //
    }

    /**
     * 退款信息
     */
    public function getPayStatus($orderSn)
    {
        //
    }
}
