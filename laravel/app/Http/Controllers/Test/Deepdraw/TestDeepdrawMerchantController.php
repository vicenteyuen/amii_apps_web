<?php

namespace App\Http\Controllers\Test\Deepdraw;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestDeepdrawMerchantController extends Controller
{
    /**
     * 获取商户信息
     */
    public function getShow($id)
    {
        $merchant = app('amii.deepdraw.merchant')->show($id);

        if (! $merchant) {
            return '获取商户信息失败';
        }
        return '获取商品信息成功';
    }

    /**
     * 查询商户信息
     */
    public function getSearch(Request $request)
    {
        $name = $request->name;
        $merchants = app('amii.deepdraw.merchant')->search($name);

        if (! $merchants) {
            return '获取商户信息失败';
        }
        return '获取商户信息成功';
    }

    /**
     * 查询商户上货期数
     */
    public function getSaleCalender($id)
    {
        $saleCalender = app('amii.deepdraw.merchant')->saleCalender($id);

        if (! $saleCalender) {
            return '查询失败';
        }
        return '查询成功';
    }

    /**
     * 商户上货期数商品查询
     */
    public function getSaleCalenderProduct($id, $day)
    {
        $products = app('amii.deepdraw.merchant')->calenderProducts($id, $day);

        if (! $products) {
            return '查询失败';
        }
        return '查询成功';
    }
}
