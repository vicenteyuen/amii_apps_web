<?php

namespace App\Http\Controllers\Test\Deepdraw;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestDeepdrawProductController extends Controller
{
    // 获取商品信息并保存
    public function getShow($id)
    {
        $getAndStore = app('amii.deepdraw.product')->show($id);

        if (! $getAndStore) {
            return '获取商品信息并保存失败';
        }
        return '获取商品并保存成功';
    }
}
