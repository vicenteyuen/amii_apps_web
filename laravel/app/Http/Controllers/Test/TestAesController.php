<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestAesController extends Controller
{
    /**
     * 获取新publickey
     */
    public function newPublic()
    {
        $publicKey = app('amii.oms.aes')->newPublic();
        dd($publicKey);
    }

    /**
     * 获取新 key
     */
    public function newKey()
    {
        $key = app('amii.oms.aes')->newKey();
        dd($key);
    }

    /**
     * 加密数据
     */
    public function encrypt(Request $request)
    {
        // 加密
        $data = $request->all();
        $result = app('amii.oms.aes')->aesEncode($data, $iv, $encryptedData);

        if ($result != 0) {
            dd($result);
            dd('加密失败');
        }
        $iv = base64_encode($iv);
        dd(compact('iv', 'encryptedData'));
    }

    /**
     * 解密数据
     */
    public function decrypt(Request $request)
    {
        // 数据需经urlencode
        // 解密
        $data = $request->data;
        $iv = $request->iv;
// $data = '9e7SuBYo6vPhzuorfBmSmgTF5id+1P70dr59zEUOZStvLbsxjSenV75v1kHgrrxoetKb7yO4hxCxYC8oOJdBlETSobNMCa+/CkOzQ2UAlyZl8UjcT8BAT4cuaobfCFbl';
// $iv = 'YWdsSk5PUFdZWjAyNkAoXQ==';
        $result = app('amii.oms.aes')->aesDecode($data, $iv);
        if ($result[0] != 0) {
            dd($result);
            dd('解密失败');
        }

        dd($result[1]);
    }
}
