<?php

namespace App\Http\Controllers\Oms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class InventoryController extends Controller
{
    public function inventory(Request $request)
    {
        $requestData = $request->get('ganguoEncryptedData');

        $validator = Validator::make($requestData, [
            'skuId' => 'required',
            'quantity' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->app['amii.oms.response']->error(null, null, $errors);
        }

        // 更新商品库存
        $update = $this->app['inventory']->omsUpdate($requestData['skuId'], $requestData['quantity']);

        if ($update[0] !== 0) {
            return $this->app['amii.oms.response']->error($update[0]);
        }

        return $this->app['amii.oms.response']->success();
    }
}
