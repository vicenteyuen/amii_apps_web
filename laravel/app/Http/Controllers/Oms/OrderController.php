<?php

namespace App\Http\Controllers\Oms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class OrderController extends Controller
{
    /**
     * 订单发货
     */
    public function send(Request $request)
    {
        $requestData = $request->get('ganguoEncryptedData');

        $send = $this->app['amii.oms.order']->send($requestData);

        if ($send) {
            return $this->app['amii.oms.response']->success();
        }

        return $this->app['amii.oms.response']->error('处理失败');
    }

    /**
     * 订单列表
     */
    public function index(Request $request)
    {
        $requestData = $request->get('ganguoEncryptedData');

        $pageNo = isset($requestData['pageNo']) ? $requestData['pageNo'] : '';
        if ($pageNo) {
            $request->request->add(['page' => $pageNo]);
        }

        // 获取订单列表
        $index = $this->app['amii.oms.order']->index($requestData);

        return $this->app['amii.oms.response']->success($index);
    }
}
