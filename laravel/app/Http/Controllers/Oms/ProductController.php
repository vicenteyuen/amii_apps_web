<?php

namespace App\Http\Controllers\Oms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * 商品列表
     */
    public function index(Request $request)
    {
        $requestData = $request->get('ganguoEncryptedData');

        $pageNo = isset($requestData['page']) ? $requestData['page'] : '';
        if ($pageNo) {
            $request->request->add(['page' => $pageNo]);
        }

        $index = $this->app['amii.oms.product']->index($requestData);

        return $this->app['amii.oms.response']->success($index);
    }
}
