<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:120,1',
            'print.sql',
        ],

        'oms' => [
            'aes',
            // 'throttle:1200,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

        'hmac'      => \App\Http\Middleware\Hmac::class,
        'wechat'    => \App\Http\Middleware\IsWeChat::class,

        'auth.provider'         => \App\Http\Middleware\AuthProvider::class,
        'api.auth'              => \App\Http\Middleware\ApiAuthenticate::class,
        'api.not.check.auth'    => \App\Http\Middleware\ApiUserNotCheckAuth::class,
        'api.third.auth'        => \App\Http\Middleware\ApiThirdAuth::class,

        'admin.auth'    => \App\Http\Middleware\AdminAuthenticate::class,
        'api.community.auth' => \App\Http\Middleware\CommunityMiddleware::class,

        // oms
        'aes' => \App\Http\Middleware\OmsAes::class,

        // print sql
        'print.sql' => \App\Http\Middleware\PrintSqlQuery::class,
    ];
}
