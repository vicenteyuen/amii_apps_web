<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Setting;

class CommunityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $key)
    {
        $setting = app('setting')->find($key);
        if ($setting->value == 0) {
             return app('jsend')->error('网络错误');
        }
        return $next($request);
    }
}
