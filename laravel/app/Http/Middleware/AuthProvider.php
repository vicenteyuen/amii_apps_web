<?php

namespace App\Http\Middleware;

use Closure;

class AuthProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        switch ($request->provider) {
            case 'admin':
                \Auth::setDefaultDriver('admin');
                break;

            case 'mobile':
                \Auth::setDefaultDriver('mobile');
                break;

            case 'api':
                \Auth::setDefaultDriver('api');
                break;

            default:
                # code...
                break;
        }
        return $next($request);
    }
}
