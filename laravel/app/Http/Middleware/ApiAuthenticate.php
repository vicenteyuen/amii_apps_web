<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\ApiUserService;
use Auth;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');

        if ($token && $user = ApiUserService::user($token)) {
            Auth::setDefaultDriver('api');
            Auth::setUser($user);
            // 校验用户可登陆状态
        } else {
            if (Auth::guard()->check()) {
                $user = Auth::user();
                Auth::setDefaultDriver('web');
            } else {
                return app('jsend')->error('请检查用户登录状态', '10003');
            }
        }

        $method = $request->method();
        if (in_array($method, ['POST', 'PUT']) && ! $user->phone) {
            return app('jsend')->error('请绑定手机号', '10066');
        }
        return $next($request);
    }
}
