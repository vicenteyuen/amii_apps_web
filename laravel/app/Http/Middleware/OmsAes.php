<?php

namespace App\Http\Middleware;

use Closure;

class OmsAes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $iv = $request->iv;
        $encryptedData = $request->encryptedData;

        $path = $request->path();

        if (config('amii.oms.log_request_data')) {
            // log request data
            app('amii.oms.request')->omsLog($path, $request->all());
        } else if (config('amii.oms.log')) {
            // log request path
            app('amii.oms.request')->omsLog($path);
        }

        if (! ($iv && $encryptedData)) {
            if (config('amii.oms.log')) {
                app('amii.oms.request')->omsLog($path, $request->all(), 'error');
            }
            return app('amii.oms.response')->error('请求数据空');
        }

        $decrypt = app('amii.oms.aes')->aesDecode($encryptedData, $iv);
        if ($decrypt[0] != 0) {
            if (config('amii.oms.log')) {
                app('amii.oms.request')->omsLog($path, '解密错误: ' . $decrypt[0], 'error');
            }
            return app('amii.oms.response')->error('数据解密失败', $decrypt[0]);
        }

        $request->attributes->add(['ganguoEncryptedData' => $decrypt[1]]);

        if (config('amii.oms.log_request_data')) {
            // log request data
            app('amii.oms.request')->omsLog($path, $decrypt[1]);
        }

        return $next($request);
    }
}
