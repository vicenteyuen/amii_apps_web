<?php

namespace App\Http\Middleware;

use Closure;

class Hmac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $method = $request->method();

        if (! in_array($method, ['POST', 'PUT'])) {
            return app('jsend')->error('method not allowed', 405);
        }

        $publicHash = $request->header('X-Public');
        $contentHash = $request->header('X-Hash');

        $keys = json_decode(config('amii.hmackey'), true);
        if ($publicHash && $contentHash && array_key_exists($publicHash, $keys)) {
            $privateHash = $keys[$publicHash];
        } else {
            return app('jsend')->error('public not found', 401);
        }

        $hash = hash_hmac('sha512', json_encode(empty($request->all()) ? '' : $request->all()), $privateHash);

        if ($hash != $contentHash) {
            $response = app('jsend')->error('hash not match', 401);
            if (config('app.debug')) {
                $response = $response->header('hash', $hash);
            }

            return $response;
        }

        return $next($request);
    }
}
