<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Services\ApiUserService;

class ApiUserNotCheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'api')
    {
        $token = $request->header('token');

        if ($token && $user = ApiUserService::user($token)) {
            auth()->setUser($user);
        }
        return $next($request);
    }
}
