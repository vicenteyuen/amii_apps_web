<?php

namespace App\Http\Middleware;

use Closure;

class IsWeChat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('amii.produce')) {
            if (! \Utiles::isWeChat()) {
                if ($request->ajax() || $request->wantsJson()) {
                    return response()->json('请在微信中打开');
                } else {
                    return response()->make('view for wechat open');
                }
            }
        }
        return $next($request);
    }
}
