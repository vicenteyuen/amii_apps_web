<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// oath2 login
Route::group([
    'namespace' => 'Oath2'
], function () {
    Route::get('oath2/sign/{provider}', 'LoginController@sign');
    Route::get('oath2/login/{provider}', 'LoginController@login');
});

// 二维码下载
Route::get('/app/{provider}/download', 'Apih5\VersionController@download');
Route::get('/app/defaultChannelQrcode', 'Apih5\VersionController@updateDefaultDownload');

// Auth
// Route::group([
//     'namespace' => 'Auth',
//     'middleware' => 'auth.provider',
// ], function () {
//     Route::get('{provider}/sign', 'SignController@create');
//     Route::post('{provider}/sign', 'SignController@store');
//     Route::get('{provider}/login', 'LoginController@create');
//     Route::post('{provider}/login', 'LoginController@login');

//     Route::get('sign', 'SignController@create');
//     Route::post('sign', 'SignController@store');
//     Route::get('login', 'LoginController@create');
//     Route::post('login', 'LoginController@login');
// });

Route::group([
    'namespace' => 'Wechat',
    'prefix' => 'wechat',
    'middleware' => ['wechat', 'cors'],
], function () {
    Route::controller('/', 'DefaultController');
});

// 接受三方服务器通知
// notify/alipay 支付宝付款异步通知
// notify/wechat 微信app支付异步通知
// notify/weapp  微信小程序支付异步通知
// notify/wemp   微信公众号支付异步通知
Route::post('notify/{provider}', 'Pay\NotifyController@store');

// 快递100
// 订阅回调
Route::post('kuaidi100/poll', 'Kuaidi100\PollController@callback');

// web
Route::group([
    'namespace' => 'Web',
    'middleware' => 'web',
], function () {
    //
});

Route::any('/', function () {
    return view('errors.404');
});
