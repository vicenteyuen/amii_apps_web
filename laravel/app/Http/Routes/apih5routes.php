<?php

/*
|--------------------------------------------------------------------------
| Application Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application api.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// api h5
Route::group([
    'namespace' => 'Apih5',
    'prefix' => 'apih5',
], function () {
    // 商品详情
    Route::get('/product/{id?}', 'ProductController@show');
    Route::get('/subject/{id?}', 'SubjectController@show');
    Route::get('/physical-store/{id?}', 'PhysicalStoreController@show');
    // 积分规则
    Route::get('/point/rule', 'PointController@rule');
    // 积分奖励
    Route::get('/point/award', 'PointController@award');
    // 分销规则
    Route::get('/sales/rule', 'SalesController@rule');
    // 分销首页
    Route::get('/sales/home', 'SalesController@home');
    // 会员规则
    Route::get('/member/rule', 'MemberController@rule');
    // 用户协议
    Route::get('/member/policy', 'MemberController@policy');
    // 红包详情
    Route::get('/money/detail', 'MoneyController@detail');
    // 关于我们
    Route::get('/about', 'AboutController@show');
    // 首单购
    Route::get('/advertisement', 'AdvertisementController@show');
    // 招商加盟
    Route::post('/zhaoshang', 'ZhaoShangController@store');
    // 活动规则
    Route::get('activity/{id}/rule', 'ActivityRuleController@show');
});
