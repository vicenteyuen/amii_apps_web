<?php

// Test
if (config('app.debug')) {
    Route::group([
        'prefix' => 'test',
        'namespace' => 'Test',
    ], function () {
        Route::resource('image', 'TestImageController');
        Route::resource('job/{provider}/', 'TestJobController');
        Route::controller('order/refund', 'TestOrderRefundController');
        // leancloud测试
        Route::controller('leancloud', 'TestLeancloudController');

        // 订单支付
        Route::get('/order/paid/{orderSn}', 'TestOrderController@paid');
        // 订单发货
        Route::get('/order/send/{orderSn}', 'TestOrderController@send');
        // 订单售后状态更新
        Route::get('/order/apply/{orderId}', 'TestOrderController@apply');

        // 添加用户余额
        Route::get('/user/addGain', 'TestUserController@gain');

        // 快递100
        # 订阅
        Route::get('/kuaidi100/poll', 'TestKuaidi100Controller@poll');
        # 查询
        Route::get('/kuaidi100/express', 'TestKuaidi100Controller@express');

        // aes
        Route::get('/aes/encrypt', 'TestAesController@encrypt');
        Route::get('/aes/decrypt', 'TestAesController@decrypt');
        Route::get('aes/public', 'TestAesController@newPublic');
        Route::get('aes/key', 'TestAesController@newKey');

        // Gimage
        Route::get('/gimage', 'TestGimageController@index');
        Route::post('/gimage', 'TestGimageController@store');


        // oms
        Route::group([
            'prefix' => 'oms',
            'namespace' => 'Oms'
        ], function () {
            Route::resource('order', 'TestOrderController');
        });

        // deepdraw
        Route::group([
            'prefix' => 'deepdraw',
            'namespace' => 'Deepdraw',
        ], function () {
            // 商户
            Route::controller('merchant', 'TestDeepdrawMerchantController');
            // 商品
            Route::controller('product', 'TestDeepdrawProductController');
        });

        // taobao open
        Route::group([
            'prefix' => 'taobao/open',
            'namespace' => 'TaobaoOpen',
        ], function () {
            Route::controller('product', 'TestTaobaoOpenProductController');
        });
    });
}
