<?php

/*
|--------------------------------------------------------------------------
| Application Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application admin.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// admin
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
], function () {

    Route::get('login', 'LoginController@create');
    Route::post('login', 'LoginController@store');

    Route::group([
        'middleware' => 'admin.auth',
    ], function () {

        // product
        Route::get('product/search', 'ProductController@productSearch');//搜索所有商品
        Route::post('member/birthday', 'MemberController@changeBirthday');
        Route::post('member/status', 'MemberController@updateUser');
        Route::resource('member', 'MemberController');
        Route::resource('sellAttribute', 'SellAttributeController');

        // sellProduct
        Route::group(['prefix' => 'sellProduct'], function () {
            Route::post('updateNumber', 'SellProductController@addInventory');
            Route::post('uploadImage', 'SellProductController@upload');
            Route::post('saveSellPoint', 'SellProductController@saveSellPoint');
            Route::post('savePrice', 'SellProductController@savePrice');
            Route::post('saveStatus', 'SellProductController@status');
            Route::post('addSellType', 'SellProductController@addSellType'); //普通库存，积分库存
            Route::post('game/status', 'SellProductController@gameStatus');  //上下架游戏商品
            Route::post('game/mulity/status', 'SellProductController@muliGameStatus');  //批量上下架游戏商品
            Route::post('product/status', 'SellProductController@sellProductStatus');  //上下架商品
            Route::post('product/mulity/status', 'SellProductController@muliSellproductStatus');  //批量上下架商品
            Route::post('savePuzzles', 'SellProductController@savePuzzles'); //更改拼图块数
            Route::resource('/', 'SellProductController');

        });

       // image
        Route::group(['prefix' => 'image'], function () {
            Route::post('delete', 'ImageController@delete');     //删除图片
            Route::post('/{provider}', 'ImageController@store'); //新增图片
            Route::post('delete/all', 'ImageController@deleteAll');   //删除所有图片
            Route::resource('/', 'ImageController');

        });

        //fileImage
         Route::group(['prefix' => 'file'], function () {
            Route::get('showEdit/{id}', 'ImageFileController@showEdit');
            Route::get('select/{id}', 'ImageFileController@selectFile');
            Route::get('image', 'ImageFileController@showFileImage');
            Route::post('update/file', 'ImageFileController@updates');
            Route::post('delete/{id}', 'ImageFileController@delete');
            Route::resource('/', 'ImageFileController');
         });

        // attributevalue 属性值
            Route::group(['prefix' => 'attributevalue'], function () {
                Route::get('editshow', 'AttributeValueController@editShow');//添加属性值
                Route::get('addShow', 'AttributeValueController@addShow');//添加属性值
                Route::post('add', 'AttributeValueController@add');//添加属性值
                Route::post('edit', 'AttributeValueController@edits');//编辑属性值
                Route::post('delete', 'AttributeValueController@delete');//删除属性值
                Route::post('create', 'AttributeValueController@create'); //商品新增属性值
                Route::resource('/', 'AttributeValueController');

            });

        // product 商品
            Route::group(['prefix' => 'product'], function () {
                Route::post('detail', 'ProductController@detail'); // 新增编辑详情
                Route::post('reate', 'ProductController@store'); //商品基本属性
                Route::post('image', 'ProductController@upload'); //summernote 上传图片
                Route::get('show', 'ProductController@showEdit'); // 编辑商品
                Route::post('update', 'ProductController@edits'); // 编辑商品
                Route::post('status', 'ProductController@status'); // 上下架商品
                Route::post('search', 'ProductController@search'); //搜索商品
                Route::post('change', 'ProductController@change'); // 更换图片
                Route::post('update_template', 'ProductController@updateTemplate');//批量更新商品运费模版
                Route::get('preview', 'ProductController@preview'); //预览商品详情
                Route::get('previewImage/{id}', 'ProductController@previewImage'); //商品预览图
                Route::post('deleteMainImage', 'ProductController@deleteMainImage');//删除主图
                Route::resource('/', 'ProductController');

            });

        // brand 品牌
            Route::group(['prefix' => 'brand'], function () {
                Route::post('/delete', 'BrandController@delete');
                Route::get('show', 'BrandController@showEdit');
                Route::get('add', 'BrandController@add'); //添加商品
                Route::get('product', 'BrandController@productShow'); //品牌商品列表
                Route::get('/category', 'BrandController@brandCategory');
                Route::post('delete/brand', 'BrandController@deleteBrand');
                Route::post('deleteAll/brand', 'BrandController@deleteBrandProduct');
                Route::post('edit', 'BrandController@edits');
                Route::get('brand/serach', 'BrandController@brandSearch');//search
                Route::resource('/', 'BrandController');

            });

        // category 分类
            Route::group(['prefix' => 'category'], function () {
                Route::get('show', 'CategoryController@showData'); //分类详情
                Route::get('CateProduct', 'CategoryController@showCateProduct'); //分类详情
                Route::get('addShow', 'CategoryController@shows'); //分类详情
                Route::post('delete', 'CategoryController@delete'); //删除分类
                Route::post('updateBrandCateImg', 'CategoryController@updateBrandCateImg'); //修改品牌分类头图
                Route::post('delCateProduct', 'CategoryController@deleteCateProduct'); //删除分类商品
                Route::post('save', 'CategoryController@save'); //保存分类
                Route::post('add', 'CategoryController@add'); //新增分类
                Route::post('child', 'CategoryController@addChild'); //新增子级分类
                Route::resource('/', 'CategoryController');
            });

        // productCategory 分类商品
            Route::group(['prefix' => 'productCategory'], function () {
                Route::get('show', 'ProductCategoryController@categoryShow'); //分类品牌商品详情
                Route::get('showProduct', 'ProductCategoryController@categoryProductShow'); //分类商品详情
                Route::get('search', 'ProductCategoryController@search'); //搜索品牌商品
                Route::get('searchCategoryProduct', 'ProductCategoryController@searchCategoryProduct'); //搜索分类商品
                Route::post('delete', 'ProductCategoryController@delete'); //移除商品
                Route::post('deleteAll', 'ProductCategoryController@deleteAll'); //移除商品
                Route::post('store/product', 'ProductCategoryController@storeWithoutBrand'); //添加分类商品
                Route::post('product/insertAll', 'ProductCategoryController@insertAll'); //插入全部
                Route::resource('/', 'ProductCategoryController');
            });

        // attribute 属性
            Route::group(['prefix' => 'attribute'], function () {
                Route::get('editshow', 'AttributeController@editShow');//添加属性值
                Route::get('addShow', 'AttributeController@addShow');//添加属性值
                Route::post('add', 'AttributeController@add'); //新增属性
                Route::post('edit', 'AttributeController@edits'); //编辑属性
                Route::post('delete', 'AttributeController@delete'); //删除属性
                Route::resource('/', 'AttributeController');
            });

        // sellAttributeValue 商品属性表-属性值中间表
            Route::group(['prefix' => 'sellvalue'], function () {
                Route::post('delete', 'SellAttributeValueController@delete');      //删除商品属性值
                Route::get('inventory', 'SellAttributeValueController@makeInventory'); //生成库存数据
                Route::get('sell-attribute-value', 'SellAttributeValueController@makeSellAttributeValue');//生成库存属性值
                  Route::post('sell-attribute-image', 'SellAttributeValueController@updateImage');//生成库存属性值
                Route::resource('/', 'SellAttributeValueController');
            });

        // sell 销售形式
            Route::group(['prefix' => 'sell'], function () {
                Route::get('edit', 'SellController@edits');  //编辑库存属性
                Route::resource('/', 'SellController');
            });

        // productAttribute 商品属性
            Route::group(['prefix' => 'productattribute'], function () {
                Route::post('create', 'ProductAttributeController@store'); //商品新增属性
                Route::resource('productAttribute', 'ProductAttributeController');
            });

        // inventory 商品库存
            Route::group(['prefix' => 'inventory'], function () {
                Route::post('show', 'InventoryController@showData');  //库存数据
                Route::post('edit', 'InventoryController@edits');  //编辑库存数据
                Route::get('showData/{$id}', 'InventoryController@inventory');
                Route::resource('/', 'InventoryController');
            });

        // productImage 商品图片
            Route::group(['prefix' => 'productimages'], function () {
                Route::post('create', 'ProductImageController@store');
                Route::post('delete', 'ProductImageController@delete');
                Route::post('change', 'ProductImageController@change');
                Route::resource('/', 'ProductImageController');
            });

        // video 视频
            Route::post('upload', 'VideoController@upload');
            Route::post('video/status', 'VideoController@status');
            Route::resource('video', 'VideoController');

        // subject 专题
            Route::post('subject/status', 'SubjectController@status');
            Route::resource('subject', 'SubjectController');

        // activityRule 活动列表
            Route::post('activity-rule/status', 'ActivityRuleComtroller@status');
            Route::resource('activity-rule', 'ActivityRuleComtroller');

            Route::post('activity/rule', 'ActivityController@update');

        // physicalStore 实体店
            Route::post('physical-store/status', 'PhysicalStoreController@status');
            Route::resource('physical-store', 'PhysicalStoreController');

        # banner
            Route::group([
            'prefix' => 'banner',
            ], function () {
                Route::get('/', 'BannerController@index');
                Route::get('create', 'BannerController@create');
                Route::get('show', 'BannerController@showDetail');
                Route::post('/', 'BannerController@store');
                Route::post('edit', 'BannerController@edits');
                Route::post('delete', 'BannerController@delete');
                Route::post('status', 'BannerController@status');
            });

        # 推广
            Route::group([
            'prefix' => 'popular',
            ], function () {
                Route::get('/', 'PopularController@index');
                Route::get('{id}/children', 'PopularController@children');
                Route::get('create', 'PopularController@create');
                Route::get('show', 'PopularController@showDetail');
                Route::get('search', 'PopularController@search');          //搜索
                Route::get('brandCategory', 'PopularController@brandCategory');          //获取品牌分类
                Route::post('/', 'PopularController@store');
                Route::post('edit', 'PopularController@edits');
                Route::post('delete', 'PopularController@delete');
                Route::post('deleteAll', 'PopularController@deleteAll');
                Route::post('status', 'PopularController@status');
            });

        # 优惠券
            Route::group([
            'prefix' => 'coupon',
            ], function () {
                Route::get('/', 'CouponController@index');
                Route::get('create', 'CouponController@create');
                Route::post('/', 'CouponController@store');
                Route::post('status', 'CouponController@status');
                Route::get('show', 'CouponController@show');
            });

        // 招商加盟
            Route::group(['prefix' => 'zhaoshang',], function () {
                Route::get('/', 'ZhaoShangController@index');
                Route::get('showDetail', 'ZhaoShangController@showDetail');
                Route::post('edits', 'ZhaoShangController@edits');

            });

        # 订单
            Route::post('order/send', 'OrderController@send');//发货
            Route::get('order/refund/{orderSn}/can_refund/{can_refund}', 'OrderController@canRefund');    //开启售后
            Route::get('order/user/{user_id}/index', 'OrderController@userOrder');//用户订单
            Route::get('order/user/{order_sn}/detail', 'OrderController@userOrderDetail'); //用户订单详情
            Route::resource('order', 'OrderController');

        # Helper
            Route::group([
            'prefix' => 'helper',
            ], function () {
                Route::post('summermote-image-upload', 'HelperController@summernoteImageUpload');
            });

            Route::get('memberShow/{id}', 'MemberController@show');//会员详情

            Route::get('logout', 'LoginController@logout');

        // 售后
            Route::post('refund/agree', 'OrderProductRefundController@agree');    //同意申请
            Route::post('refund/refuse', 'OrderProductRefundController@refuse');    //拒绝申请
            Route::post('refund/receive', 'OrderProductRefundController@receive'); //确认收货

        // 游戏
            Route::get('game/search', 'UserGameController@search');
            Route::get('game/store', 'UserGameController@storeByGetMethod');//新增
            Route::get('game/show/data', 'UserGameController@manager');//展示数据
            Route::resource('game', 'UserGameController'); //游戏

        // 用户游戏反馈
            Route::get('gameAdvice/detail', 'UserGameAdviceController@details');
            Route::post('gameAdvice/status', 'UserGameAdviceController@gameAdviceStatus');
            Route::resource('gameAdvice', 'UserGameAdviceController');

            Route::get('purchase/search', 'FirstPurchaseController@search');          //搜索
            Route::post('purchase/money', 'FirstPurchaseController@saveMoney');       //首单金额
            Route::get('purchase/history', 'FirstPurchaseController@history');        //首单历史商品
            Route::resource('purchase', 'FirstPurchaseController');                   //首单购
            Route::post('purchase/update', 'FirstPurchaseController@update');         //更新
            Route::resource('product/text', 'FirstPurchaseTextController');           //首单购文本
            Route::post('product/text/delete', 'FirstPurchaseTextController@delete'); //删除首单购文本
            Route::post('product/text/update', 'FirstPurchaseTextController@update');//更新

        // 商品星级评论
            Route::group(['prefix' => 'comment'], function () {
                Route::get('/', 'UserCommentController@index');
                Route::get('show', 'UserCommentController@showDetail');//显示评论详情
                Route::post('status', 'UserCommentController@status'); // 商品星级评论
            });

        // 用户反馈
            Route::group(['prefix' => 'feedback'], function () {
                Route::get('/', 'FeedBackController@index');
                Route::get('showDetail', 'FeedBackController@showDetail');//用户反馈详情
                Route::post('edits', 'FeedBackController@edits');
                // 反馈状态处理
            });

        // 文案分享
            Route::group(['prefix' => 'share'], function () {
                Route::get('/', 'ShareController@index');//获取文案分享列表
                Route::get('create', 'ShareController@create');//添加文案
                Route::get('detail', 'ShareController@showDetail');//查看文案内容
                Route::get('search', 'ShareController@search');//搜索商品数据
                Route::post('store', 'ShareController@store');//保存新文案
                Route::post('edit', 'ShareController@edits');//编辑新文案
                Route::post('delete', 'ShareController@delete');//获取文案分享列表
            });

        // 分享关联表
            Route::group(['prefix' => 'shareRelation'], function () {
                Route::get('/', 'ShareRelationController@index');//获取文案分享列表
                Route::get('create', 'ShareRelationController@create');//添加文案
                Route::get('selected', 'ShareRelationController@getSelected');//查看文案内容
                Route::get('search', 'ShareRelationController@search');//搜索商品数据
                Route::post('store', 'ShareRelationController@store');//保存新文案
                Route::post('edit', 'ShareRelationController@edits');//编辑新文案
                Route::post('delete', 'ShareRelationController@delete');//获取文案分享列表
            });

        // 通知消息
            Route::group(['prefix' => 'notifyMessage'], function () {
                Route::get('/', 'NotifyMessageController@index');
                Route::get('editNotify', 'NotifyMessageController@editNotify');//编辑系统消息视图
                Route::post('notifyPlush', 'NotifyMessageController@notifyPlush');//发送系统消息
                Route::get('showNotify', 'NotifyMessageController@showNotify');//系统消息详情
                Route::get('notifyIndex', 'NotifyMessageController@notifyIndex');//系统消息列表
                Route::get('showDetail', 'NotifyMessageController@showDetail');//通知消息详情

            });

        // 品牌商品
            Route::post('addAll', 'BrandProductController@insertAll'); //添加批量或全部
            Route::resource('/', 'BrandProductController');

        // 运费模版
            Route::resource('express', 'ExpressTemplateController'); //运费模版
            Route::post('set/all/product', 'ExpressTemplateController@setAllProductExpressTemplate');//一键添加运费模版

            Route::get('template/product/create', 'ExpressTemplateController@createTemplateProduct'); //添加运费模版商品页面
            Route::get('template/product/{id}', 'ExpressTemplateController@templateProductIndex'); // 运费模版商品列表
            Route::get('template/{id}', 'ExpressTemplateController@showEdit');//编辑运费模版页面
            Route::post('template/save', 'ExpressTemplateController@save');//编辑运费模版
            Route::post('template/product/store', 'ExpressTemplateController@storeTemplateProduct'); //添加运费模版商品
            Route::post('delete', 'ExpressTemplateController@delete');
            Route::post('company/express/update', 'ExpressController@save');
            Route::resource('company/express', 'ExpressController');

        // 关于我们
            Route::get('about', 'AboutController@index');
        // 版本管理
            Route::group(['prefix' => 'version'], function () {
                Route::get('show', 'VersionController@showDetail');//查看版本
                Route::post('delete', 'VersionController@delete');//编辑版本
                Route::post('upload', 'VersionController@upload');//上传版本安装包
                Route::post('edit', 'VersionController@edits');//编辑版本
                Route::resource('/', 'VersionController');
            });
            Route::post('setting/update', 'SettingController@update'); //社区开关
            Route::get('setting/show', 'SettingController@index'); //社区开关
            Route::get('product/service/index', 'ProductServiceController@index');//列表
            Route::post('product/service/update', 'ProductServiceController@update');//更新

            Route::resource('log/image', 'OrderProductRefundImageController');//售后图片

            Route::get('category_brand/product/{id}', 'ProductCategoryController@categoryProduct');
            Route::get('image/page', 'ImageController@pageImage'); //图片分页

        // 渠道管理
            Route::get('channel/downloadQrcode', 'ChannelController@downloadQrcode');
            Route::get('channel/showEdit', 'ChannelController@showEdits');
            Route::post('channel/edit', 'ChannelController@edits');
            Route::post('channel/delete', 'ChannelController@delete');
            Route::resource('channel', 'ChannelController');//渠道管理


        // 游戏方案
            Route::get('game/push/document', 'GamePushDocumentController@index');//文案首页
            Route::get('game/document/edit/{id}', 'GamePushDocumentController@showEdit');//文案编辑
            Route::post('game/push/document', 'GamePushDocumentController@edit');//文案首页
            // 同步淘宝商品
            Route::get('taobao/sync/index', 'TaobaoSyncController@index');
            Route::post('taobao/sync', 'TaobaoSyncController@sync');
            Route::get('taobao/sync/search', 'TaobaoSyncController@searchView');
            Route::post('taobao/sync/search', 'TaobaoSyncController@search');
            Route::post('taobao/sync/product', 'TaobaoSyncController@syncSearch');
            Route::get('taobao/sync/category', 'TaobaoSyncController@category');
            Route::post('taobao/sync/map', 'TaobaoSyncController@mapCate');

        // 提现
            Route::post('withdraw/agree', 'WithdrawController@agree');
            Route::post('withdraw/refuse', 'WithdrawController@refuse');
            Route::resource('withdraw', 'WithdrawController');

            // 报表下载
            Route::group([
                'prefix' => 'report',
            ], function () {
                Route::get('refundOrder', 'ReportController@refundOrder');
                Route::get('refundOrder/export', 'ReportController@exportRefundOrder');
            });
        // 公告
        Route::get('announce/point', 'PointAnnounceController@index');
        Route::get('announce/point/create', 'PointAnnounceController@createOrShow');
        Route::post('announce/point', 'PointAnnounceController@update');
        Route::get('announce/point/publish', 'PointAnnounceController@publish');
        Route::get('announce/point/unPublish', 'PointAnnounceController@unPublish');

        // 用户积分明细
            Route::resource('user/points', 'PointReceivedController');
        // 最底部路由
            Route::controller('/', 'DashboardController');

    });
});
