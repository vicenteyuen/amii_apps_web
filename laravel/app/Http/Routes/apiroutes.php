<?php

/*
|--------------------------------------------------------------------------
| Application Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application api.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// api
Route::group([
    'prefix' => 'api',
    // 测试环境不限制api请求
    // 'middleware' => ['api', 'cors'],
    'middleware' => ['cors'],
], function () {
    // oath2 login
    Route::group([
        'prefix' => 'oath2/{provider}'
    ], function () {
        Route::get('config', 'Oath2\LoginController@wempConfig');
        Route::get('sign', 'Oath2\LoginController@sign');
        Route::get('login/{platform?}', 'Oath2\LoginController@login');
    });
    // api third auth
    Route::group([
        'namespace' => 'Api',
        'middleware' => [
            'api.third.auth',
        ],
    ], function () {
        // 获取绑定手机号验证码
        Route::post('codeThird/{provider}', 'SmsController@thirdSendCode')->middleware('hmac');
        // 绑定手机号
        Route::post('user/phone/bund', 'User\UserController@bundPhone');
    });
    // api auth
    Route::group([
        'middleware' => 'api.auth',
    ], function () {
        // oath2 绑定帐号
        Route::group([
            'prefix' => 'oath2/{provider}',
        ], function () {
            Route::get('bund', 'Oath2\LoginController@bund');
            Route::get('openid', 'Oath2\LoginController@openid');
        });
        // api
        Route::group([
            'namespace' => 'Api',
        ], function () {
            Route::post('sing/password', 'SignController@createPassword');//获取系统分配密码
            Route::group(['namespace' => 'User'], function () {
                //user 用户
                // 更换手机校验旧手机验证码
                Route::post('user/phone/code', 'UserController@checkCurrentPhoneCode');
                // 更换手机号
                Route::get('user/balance', 'UserController@balance'); //余额
                Route::post('/user/phone', 'UserController@updatePhone');
                Route::resource('user', 'UserController');
                Route::post('user/password', 'UserController@password');
                Route::post('user/update', 'UserController@update');
                Route::post('user/avatar', 'UserController@avatar'); //修改头像
                Route::get('member', 'UserController@member'); //会员中心
                Route::get('qrcode/{provider?}', 'UserController@qrcode'); //会员二维码
                //地址
                Route::post('address/status', 'UserAddressController@status'); //设置默认地址
                Route::get('address/status/default', 'UserAddressController@detail');  //获取默认地址信息
                Route::resource('address', 'UserAddressController');
                //collection 收藏
                Route::resource('collection', 'UserCollectionController');
                Route::post('collection/delete', 'UserCollectionController@deleteAll'); //删除所有的
                Route::post('collection/stores', 'UserCollectionController@stores'); //批量新增收藏
                Route::get('game/collection', 'UserCollectionController@gameIndex'); //游戏商品
                //scan 浏览
                Route::resource('scan', 'UserScanLogController');
                Route::post('scan/delete', 'UserScanLogController@deleteAll'); //批量删除
                Route::resource('suit', 'UserSuitController');
                Route::post('suit/status', 'UserSuitController@status');   //设置身材数据默认地址
                Route::post('suit/image', 'UserSuitController@image');     //身材图片

                // 优惠券
                Route::get('coupon', 'UserCouponController@index');
                Route::resource('coupon', 'UserCouponController');

                Route::get('today-task', 'TodayTaskController@index'); //今日任务列表

                //comment 评论
                Route::post('comment/upload', 'UserCommentController@upload');
                Route::resource('comment', 'UserCommentController');

                //shoppingCart 购物车
                Route::post('cart/deletes', 'CartController@deletes'); //批量删除
                Route::post('cart/stores', 'CartController@stores'); //批量新增
                Route::resource('cart', 'CartController');
                //tribe 部落
                Route::get('tribe/index', 'TribeController@show'); //部落首页
                Route::get('tribe/{provider}', 'TribeController@index');
                Route::get('tribe/income/detail', 'TribeController@detail'); //收益明细
                Route::resource('tribe', 'TribeController');
                //gain 收益
                Route::resource('gain', 'GainController');
                Route::get('user/gain/show', 'GainController@gainShow');//部落首页
                Route::resource('level', 'PointLevelController');
                Route::resource('enable', 'EnableController');
                //pointReceived 积分明细记录
                Route::resource('point/recored', 'PointReceivedController');
                // order
                // 不同状态订单数量
                Route::get('order/status/count', 'OrderController@statusCount');
                // 取消订单
                Route::post('order/cancel', 'OrderController@cancel');
                // 确认收货
                Route::post('order/confirm', 'OrderController@confirm');
                // 删除订单
                Route::post('order/destroy', 'OrderController@destroy');
                // 提交订单支付
                Route::post('order/{provider}', 'OrderController@store');
                // 订单列表
                Route::get('order', 'OrderController@index');
                // 订单最近购买商品
                Route::get('order/products', 'OrderController@products');
                // 订单详情
                Route::get('order/{orderSn}', 'OrderController@show');
                // pay
                Route::get('payfee/{orderSn}', 'PayController@payFee');
                Route::resource('pay', 'PayController');
                // 快递信息
                Route::get('order/{orderSn}/express', 'OrderController@express');
                // order refund
                Route::resource('order-product/{id}/refund', 'OrderProductRefundController');
                Route::post('order-product/{id}/refund-express', 'OrderProductRefundController@express');
                Route::get('order-product/{id}/copywriting', 'OrderProductRefundController@Copywriting');//获取售后选项
                Route::post('order-product/image', 'OrderProductRefundController@image'); //售后图片上传
                // 意见反馈
                Route::post('feedback', 'FeedbackController@store');
                Route::resource('balance', 'BalanceController');

                //重置支付密码
                Route::controller('payment_password', 'PaymentPasswordController');

                Route::controller('withdraw', 'WithdrawController');

                Route::resource('question', 'UserQuestionController');
                Route::post('game/activate', 'UserGameController@activate');    // 用户激活
                Route::get('game/play/user', 'UserGameController@userPlay');    // 用户列表
                Route::post('game', 'UserGameController@store');                   // 游戏
                Route::get('game/user/{id}', 'UserGameController@userGame');       // 当前拼图
                Route::post('game/add', 'UserGameController@add');
                Route::get('game/expired/user', 'UserGameController@expireGame'); //用户游戏过期
                Route::get('order/received/{orderSn}', 'PointReceivedController@receivedOrder');//增加订单推送通知
                Route::get('order/can/appraising/{orderSn}', 'OrderController@orderAppraising');//订单可评价商品
                Route::get('userlevel/enable', 'PointLevelController@level'); //当前用户等级所具有的特权
                Route::resource('game/advice', 'UserGameAdviceController');//游戏建议
                Route::get('user/game/playing', 'UserGameController@userPlayingGame'); // 正在玩(多个)
                Route::post('user/add/userGameId', 'UserGameController@addUserGameId');// 用户登陆添加游戏id
                Route::post('user/activate/game', 'UserGameController@activiting');// 用户登陆添加游戏id
            });

            Route::group([
                    'namespace' => 'Game'
                 ], function () {
                    Route::post('game/order', 'GameController@store'); //创建订单
                    Route::post('game/commit', 'GameController@gameCommitOrder'); //提交订单
                    Route::get('user/game/order', 'GameController@gameOrder'); //用户已完成的订单
                 });
            // password
            Route::post('password/reset', 'PasswordController@reset');
            // notify
            // 通知消息主面板
            Route::get('notify/home', 'NotifyMessageController@home');
            // 通知消息列表
            Route::get('notify/{provider?}', 'NotifyMessageController@index');
            // 批量删除通知消息
            Route::post('notify/destroy', 'NotifyMessageController@delete');
            // 已读通知消息
            Route::post('notify/{provider}', 'NotifyMessageController@read');
            Route::resource('notify', 'NotifyMessageController');
            Route::get('activity-share/{provider}', 'SellProductController@shareProduct'); //分享商品增加积分
        });
    });
    // api not check auth
    Route::group([
        'namespace' => 'Api',
    ], function () {
        Route::get('home', 'HomeController@home');
        // 获取随机登录手机验证码
        Route::group([
            'middleware' => 'hmac',
        ], function () {
            // check auth
            Route::group([
                'middleware' => 'api.auth',
            ], function () {
                Route::post('codeAuth/{provider}', 'SmsController@authSendCode');
            });
            Route::post('code/{provider}', 'SmsController@sendCode');
        });

        //社区
        Route::group([
            'namespace' => 'Community'
        ], function () {

            //subject
            Route::resource('subject', 'SubjectController');
            Route::post('subject/mark', 'SubjectController@mark');//点赞或取消点赞
            // video
            Route::get('video/home', 'VideoController@home');
            Route::post('video/add-like/{video_id}', 'VideoController@addLike');
            Route::post('video/remove-like/{video_id}', 'VideoController@removeLike');
            Route::resource('video', 'VideoController');
            //physicalStore
            Route::resource('physical-store', 'PhysicalStoreController');

        });

        //文案分享 share
        Route::get('share/{provider}', 'ShareController@show');

        Route::get('cart/product/{sell_product_id}', 'User\CartController@show');

        // 商品列表
        Route::get('{provider}/{resource}/product', 'ProductController@index');

        //游戏商品详情
        Route::get('game/product/{id}', 'ProductController@gameProductDetail');
        Route::resource('product', 'ProductController');
        Route::get('comment/index/{id}', 'User\UserCommentController@index'); //获取评价列表
        Route::get('comment-pop/{id}', 'User\UserCommentController@popup');          //获取弹出评价列表
        Route::get('address/info/list', 'User\UserAddressController@address'); //省市区地址数据
        // point
        Route::group([
            'namespace' => 'Point'
        ], function () {
            // product
            Route::resource('point/product', 'PointProductController');
            // order
            Route::post('point/order', 'OrderController@store');
            // pay
            Route::get('point/pay', 'PayController@create');
            Route::post('point/pay', 'PayController@store');
            // announce
            Route::get('point/announce/{id}', 'AnnounceController@show');
        });

        Route::post('sign', 'SignController@store');
        Route::post('gameSign', 'SignController@gameSign'); //游戏注册
        Route::post('inviteSign', 'SignController@inviteSign');//邀请注册
        Route::post('login', 'LoginController@store');
        Route::post('codeLogin', 'LoginController@code');
        Route::get('logout', 'LoginController@logout');
        // password
        Route::post('password/code', 'PasswordController@forgetPsdCodeCheck');
        Route::post('password', 'PasswordController@store');
        // 商品热门搜索
        Route::get('hot/search', 'HotSearchController@hot');
        // h5 page
        Route::get('helper/h5-urls', 'HelperController@h5Urls')->middleware('api.not.check.auth');
        // 获取Hmac公钥
        Route::get('helper/xpublic', 'HelperController@xpublic');
        // 获取系统初始化配置
        Route::get('helper/init', 'HelperController@init');
        // 获取各消息数
        Route::get('helper/notify/number', 'HelperController@notifyNumber');
        // 二维码扫描
        Route::get('scanQrcode', 'QrcodeController@show');
        // 上下装 肤色 发色 数据
        Route::get('suit/user/size', 'User\UserSuitController@size');
        Route::get('feedback/create', 'User\FeedbackController@create');
        //默认身材数据
        Route::get('suit/default/{sell_product_id}', 'User\UserSuitController@defaults');
        // 商品推荐
        Route::get('individ/{provider}', 'IndividController@index');
        Route::resource('purchase/text', 'PurchaseTextController'); //文本列表
        Route::resource('comment-rank', 'User\CommentRankController'); //评论星级
        Route::get('game/{type?}', 'User\UserGameController@index')->middleware('api.not.check.auth');                   // 游戏
        Route::get('game/join/user', 'User\UserGameController@canJoinGame')->middleware('api.not.check.auth');//用户是否可参加游戏
        Route::get('version/newest', 'VersionController@getNewest');                   // 游戏

        Route::get('received', 'User\PointReceivedController@received'); //签到增加积分
        Route::get('game/{user_game_id}/user', 'User\UserGameController@playingGame')->middleware('api.not.check.auth'); // 游戏进程
        Route::get('activity/message', 'Game\GameController@activity'); //首单

        Route::get('user/join/number', 'User\UserGameController@joinUsers')->middleware('api.not.check.auth');
         //拼图游戏参与人数


        // 活动
        Route::get('activity/list', 'ActivityController@list')->middleware('api.not.check.auth');
        Route::get('activity/{provider}', 'ActivityController@index')->middleware('api.not.check.auth');
        Route::post('add-game-product/{sku}/{number}', 'SellProductController@addGameInventory'); //接口增加游戏商品
        Route::get('user/info/{uuid}', 'User\UserController@findByUuid');//通过uuid找用户信息
        Route::get('user/point/rank/{provider}', 'User\PointsRankController@show')->middleware('api.not.check.auth');//积分排名
        Route::get('user/last/month/rank', 'User\PointsRankController@lastShow');//上月积分排名
        Route::get('game/sku_code/{skuCode}', 'User\UserGameController@countPlayingBySkuCode');       //当前sku正在玩的游戏数量
    });
});
