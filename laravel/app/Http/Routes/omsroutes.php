<?php

/*
|--------------------------------------------------------------------------
| Application Oms Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application api.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// api h5
Route::group([
    'namespace' => 'Oms',
    'prefix' => 'oms',
], function () {
    // aes加密解密
    Route::group([
        'middleware' => ['oms'],
    ], function () {
        // 更新商品库存
        Route::post('inventory', 'InventoryController@inventory');
        // 商品列表
        Route::post('product/search', 'ProductController@index');
        // 订单
        Route::group([
            'prefix' => 'order',
        ], function () {
            // 列表
            Route::post('index', 'OrderController@index');
            // 发货
            Route::post('send', 'OrderController@send');
        });
    });
});
