<?php

namespace App\Services;

use App\Models\Balance;
use App\Models\User;

class BalanceService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        return Balance::create($arr);
    }

    // 资产管理
    public function index($userId, $request)
    {
        if (!$request->year) {
            $date = strtotime(date('Y-m', time()));
            $year = date('Y', time());
            $month = date('m', time());
        } else {
            $date  = strtotime($request->year.'-'.$request->month);
            $year  = $request->year;
            $month = $request->month;
        }

        $user = User::find($userId);
        $coupons = app('amii.manger.user.coupon')->index($userId);
        $balanceData = [
            'year'              => $year,
            'month'             => $month,
            'cumulate_profit'   => $user->cumulate_profit,
            'return_money'      => $user->return_money,
            'coupons'           => count($coupons),
            'points'            => $user->points,
        ];

        $balanceData['total_profit'] = Balance::whereIn('type', [1,5,6])
            ->where('user_id', $userId)
            ->where('status', 1)
            ->whereBetween(\DB::raw('unix_timestamp(created_at)'), [$date,$date +24 * 60 * 60 * 30 - 1])
            ->sum('value') ?: "0.00";

        $balanceData['purchase_price'] = Balance::whereIn('type', [2,3,4])
            ->where('user_id', $userId)
            ->whereBetween(\DB::raw('unix_timestamp(created_at)'), [$date,$date +24 * 60 * 60 * 30 - 1])
            ->sum('value');

        if ($balanceData['purchase_price'] < 0) {
             $balanceData['purchase_price'] = - $balanceData['purchase_price'];
        } else {
             $balanceData['purchase_price'] = "0.00";
        }
       
        $balanceData['income'] = Balance::whereIn('type', [1,5,6])
            ->where('user_id', $userId)
            ->where('status', 1)
            ->whereBetween(\DB::raw('unix_timestamp(created_at)'), [$date,$date +24 * 60 * 60 * 30 - 1])
            ->orderBy('created_at', 'desc')
            ->get();

        $balanceData['purchase'] = Balance::whereIn('type', [2,3,4])
            ->where('user_id', $userId)
            ->whereBetween(\DB::raw('unix_timestamp(created_at)'), [$date,$date +24 * 60 * 60 * 30 - 1])
            ->orderBy('created_at', 'desc')
            ->get();

        return $balanceData;
    }

    public static function rebackExpress($userId, $value, $type)
    {
        $arr = [
            'user_id' => $userId,
            'type'    => $type,
            'value'   => $value
        ];
        Balance::create($arr);
        $user = User::find($userId);
        $user->increment('cumulate_profit', $value);
        $user->save();
    }

    //用户余额记录 admin
    public function balanceUserRecord($userId)
    {
        return Balance::where('user_id', $userId)
            ->with('user')
            ->with('order')
            ->paginate(config('amii.adminPaginate'));
    }

    public function storeBalance($arr)
    {
        return Balance::create($arr);
    }
}
