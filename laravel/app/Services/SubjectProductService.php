<?php

namespace App\Services;

use App\Models\SubjectProduct;
use Ecommerce\Models\Product;

class SubjectProductService
{
    public function getProducts($subject_id)
    {
        $product_ids = SubjectProduct::where('subject_id', $subject_id)
            ->pluck('product_id')
            ->toArray();
        if (count($product_ids) > 0) {
            return Product::find($product_ids);
        }

        return false;

    }

    public function store($subject_id, array $product_ids)
    {
        $arr = [];
        foreach ($product_ids as $product_id) {
            $arr[] = [
                'subject_id' => $subject_id,
                'product_id' => $product_id
            ];
        }

        return SubjectProduct::insert($arr);
    }

    public function delete($subject_id)
    {
        return SubjectProduct::where('subject_id', $subject_id)
            ->delete();
    }
}
