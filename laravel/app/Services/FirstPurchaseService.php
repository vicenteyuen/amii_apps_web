<?php
namespace App\Services;

use App\Models\FirstPurchase;
use App\Models\GiveProduct;
use App\Models\GiveProductHistory;
use DB;

class FirstPurchaseService
{
    /**
     * 添加赠品
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($sellProductId)
    {
        DB::beginTransaction();
        $product = FirstPurchase::updateOrCreate(['id' => 1], $sellProductId);
        $history = GiveProductHistory::updateOrCreate($sellProductId);
        if ($product && $history) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }


    // 保存金额
    public function saveMoney($money)
    {
        return FirstPurchase::updateOrCreate(['id' => 1], $money);
    }


    /**
     * 当前赠品
     * @return [type] [description]
     */
    public function product()
    {
        return FirstPurchase::where('id', 1)
            ->with('sellProduct.product')
            ->first();
    }

    public function update($arr)
    {
        return FirstPurchase::where('id', $arr['id'])
            ->update(['status' => $arr['status']]);
    }

    //首单金额
    public function purchaseMoney()
    {
        return FirstPurchase::where('id', 1)
            ->value('money');
    }
}
