<?php
namespace App\Services;

use App\Models\Version;
use App\Helpers\QrcodeHelper;
use App\Models\VersionChannel;
use DB;
class VersionService
{
    /**
     * Version index for admin
     */
    public function adminIndex($search = null)
    {
        $query = new Version;

        $versionChannel =  VersionChannel::orderBy('id')
            ->where('desc', $search)
            ->first();
        if ($versionChannel) {
            $channel = $versionChannel->channel;
        } else {
            $channel = '';
        }
        $query = $query->when($search, function ($query) use($search, $channel) {
                return $query->where('name', 'like', '%'.trim($search).'%')
                    ->orWhere('version_number', 'like', '%'.trim($search).'%')
                    ->orWhere('channel', 'like', '%'.trim($channel).'%');
            })
            ->orderBy('created_at', 'desc')
            ->orderBy('is_newest')
            ->paginate(config('amii.adminPaginate'));
        return $query;
    }

    /**
    *   获取渠道名称
    */
    public function getVersionChannel()
    {
         $channels = VersionChannel:: orderBy('id')
            ->select('channel', 'desc')
            ->get();
        return $channels;
    }

    /**
    *保存新添版本信息
    *@param   $data array
    *@return  type  [description]
    */
    public function store($data)
    {
        return  Version::create($data);
    }

    /**
    *获取指定版本详情
    *@param   $id   指定版本的id
    *@return  type  [description]
    */
    public function detail($id)
    {
        $data = Version::where('id',  $id)
            ->first();
        return $data;
    }

    /**
    * 更新版本信息
    *@param   $arr       [description]
    *@param   $id        int
    *@return  type       [description]
    */
    public function update($arr, $id)
    {
        $exist = Version::where('id', $id)
            ->first();

        if ($exist && $id != $exist->id ) {
            return ['status' => 0, 'message' => '信息处理失败！'];
        }

        $update = Version::where('id', $id)
            ->update($arr);
        if (!$update) {
            return ['status' => 0, 'message' => '信息处理失败！'];
        }
        return ['status' => 1];
    }


    /**
    * api获取最新版
    *@param   $type     android/ios
    *@param   $channel  [description]
    *@return  type      [description]
    */
    public function getNewest($type, $channel)
    {
        $data = Version::where('is_newest', 1)
            ->where('version_type', $type)
            ->where('channel', $channel)
            ->orderBy('version_number', 'desc')
            ->first();
        return $data;
    }

    /**
    *去掉最新版
    *@return  type  [description]
    */
    protected function newestDown()
    {
        $count = Version::count();
        if ($count) {
            Version::where('is_newest', 1)->update(['is_newest' =>  0]);
        }
    }
    // 删除
    public function delete($id)
    {
        return Version::where('id', $id)->delete();
    }


}
