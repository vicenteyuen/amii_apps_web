<?php
namespace App\Services;

use App\Models\SellAttribute;
use App\Models\Attribute;

class SellAttributeService
{
    /**
     * 新增
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {

        $hasExists = SellAttribute::where($arr)
                        ->first();

        if (!is_null($hasExists)) {
            return ['status' =>false,'msg'=>'该属性已存在'];
        }
        $data = SellAttribute::create($arr);
        if ($data) {
            return ['status' => true,'id' => $data->id];
        }
        return ['status' => false,'msg' => '新增属性失败'];
    }
    /**
     * 删除
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        return SellAttribute::destroy($id);

    }

    /**
     * 获取属性值
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function attributeValue($id)
    {
        $data =  SellAttribute::where('product_sell_attribute_id', $id)
                        ->with('attribute.attributeValue')
                        ->get();
        return $data;
    }

    /**
     * 在没有库存情况下，获取属性和属性值
     * @param  [type] $sellProductId [description]
     * @return [type]                [description]
    */
    public function hasSellAttributes($sellProductId)
    {
        $data = SellAttribute::where('product_sell_attribute_id', $sellProductId)
            ->with('sellAttributeValue')
            ->get();
        return $data;
    }
}
