<?php
namespace App\Services;

use App\Models\AttributeValue;
use DB;

class AttributeValueService
{
    
    /**
     * 新增属性
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function create($arr)
    {
        $hasExists = AttributeValue::where('attribute_id', $arr['attribute_id'])
            ->where('value', $arr['value'])
            ->first();
        if (!is_null($hasExists)) {
            return ['status' =>false,'msg'=>'该属性值已存在'];
        }
        $save = AttributeValue::create($arr);
        if ($save) {
            return ['status' => true];
        }
        return ['status'=> false ,'msg' => '添加属性值失败'];

    }

    public function update($id, $arr)
    {
        return AttributeValue::where('id', $id)
            ->update($arr);
    }

    /**
     * 查找一条属性值对应该的属性
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function find($id)
    {
        return AttributeValue::where('id', $id)
            ->with('attr')
            ->first();
    }

    
    /**
     * 查询商品属性列表
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($id)
    {
        return AttributeValue::where('product_id', $id)
            ->get();
    }

    /** 删除属性值 */
    public function delete($id)
    {
        return AttributeValue::destroy($id);
    }
}
