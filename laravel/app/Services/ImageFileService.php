<?php

namespace App\Services;

use App\Models\ImageFile;

class ImageFileService
{
    /**
     * 图片列表
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public function index()
    {
        return ImageFile::get();
    }

    public function find($id)
    {
        return ImageFile::find($id);
    }

    // store imageFile
    public function store($arr)
    {
        return ImageFile::create($arr);
    }

    /**
     * 删除图片
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return ImageFile::destroy($id);
    }

    public function update($id, $arr)
    {
        return ImageFile::where('id', $id)
            ->update($arr);
    }
}
