<?php

namespace App\Services;

use App\Models\PointsRank;
use App\Models\PointReceived;
use App\Models\PointsReward;
use App\Models\User;
use Carbon\Carbon;
use DB;

class PointsRankService
{
    /**
     * 积分
     * @return [type] [description]
     */
    public function show($provider)
    {
        // $this->currentRank('month');
        // $this->currentRank('year');
        $data['month_rank']   = $this->rank('month', $provider);
        $data['year_rank']    = $this->rank('year', $provider);
        $user = null;
        $monthUser = null;
        $yearUser = null;
        $cAnounce = [];
        if ($provider == 'serval') {
            $cAnounce = app('point.rank.award')->cAnounce();
        }
        if (auth()->user()) {
            $user = User::where('id', auth()->id())
                ->select('id', 'nickname', 'avatar', 'phone')
                ->first();
            $userId = $user->id;

            // 前100名数据
            $userMon = PointsRank::where('user_id', $userId)
            ->with(['user' => function ($q1) {
                $q1->select('id', 'nickname', 'avatar', 'phone');
            }])
            ->where('month_rank', '<=', 100)
            ->first();

            $userYea = PointsRank::where('user_id', $userId)
            ->with(['user' => function ($q1) {
                $q1->select('id', 'nickname', 'avatar', 'phone');
            }])
            ->where('year_rank', '<=', 100)
            ->first();

            $monthRankAll = $this->rank('month', 'all');
            $yearRankAll  = $this->rank('year', 'all');

            $monthIds = $monthRankAll->pluck('user_id')->toArray();
            $yearIds = $yearRankAll->pluck('user_id')->toArray();

            $userRank = [
                'user_id'      => $user->id,
                'month_income' => $this->userIncome($userId, 'month'),
                'month_rank'   => '99+',
                'year_income'  => $this->userIncome($userId, 'year'),
                'year_rank'    => '99+',
                'user'         =>  $user,
            ];

            if (in_array($userId, $monthIds)) {
                $monthUser = $userMon;
            } else {
                $monthUser = $userRank;
            }

            if (in_array($userId, $yearIds)) {
                $yearUser =$userYea;
            } else {
                $yearUser = $userRank;
            }
        }

        $data['month_user'] = $monthUser;
        $data['year_user'] = $yearUser;

        // 积分轮播公告
        $data['c_anounce'] = $cAnounce;
        return $data;
    }

    // 上月排名
    public function lastMonthReward()
    {
        \App\Models\User::$withoutAppends = true;
        $year = Carbon::now()->subMonth()->year;
        $month = Carbon::now()->subMonth()->month;
        return $pointRanks = PointsReward::orderBy('month_rank', 'asc')
            ->where('month_rank', '>', 0)
            ->where('year', $year)
            ->where('month', $month)
            ->with(['user' => function ($q1) {
                $q1->select('id', 'nickname', 'avatar', 'phone');
            }])
            ->get();
    }

    /**
     * 排名调用方法
     * @param  array $arr 区间
     * @return [type]      [description]
     */
    public function rank($type, $provider)
    {
        \App\Models\User::$withoutAppends = true;
        switch ($type) {
            case 'month':
                // 本月排名
                $pointRanks = PointsRank::orderBy('month_rank', 'asc')
                ->where('month_rank', '>', 0)
                ->with(['user' => function ($q1) {
                    $q1->select('id', 'nickname', 'avatar', 'phone');
                }]);

                if ($provider == 'all') {
                    $pointRanks = $pointRanks->where('month_rank', '<=', 100)
                    ->get();
                } else if ($provider == 'serval') {
                    $pointRanks = $pointRanks->offset(0)
                    ->limit(3)
                    ->get();
                }
                return $pointRanks;
            case 'year':
                // 累积排名
                $pointRanks = PointsRank::orderBy('year_rank', 'asc')
                  ->where('year_rank', '>', 0)
                  ->with(['user' => function ($q1) {
                    $q1->select('id', 'nickname', 'avatar', 'phone');
                  }]);
                if ($provider == 'all') {
                    $pointRanks = $pointRanks->where('year_rank', '<=', 100)
                    ->get();
                } else if ($provider == 'serval') {
                    $pointRanks = $pointRanks->offset(0)
                    ->limit(3)
                    ->get();
                }
                return $pointRanks;
            default:
                return null;
            break;
        }
    }

     /**
     * 上个月范围
     * @return array [description]
     */
    public function lastMonthAmong()
    {
        $thisMonthStart =  date('Y-m-01', time());                            // 本月第一天
        $lastMonthStart =  mktime(0, 0, 0, date("m")-1, 1, date("Y"));        //上月开始日期
        $lastMonthEnd   =  strtotime($thisMonthStart)- 1;                     //上月结束日期
        return [$lastMonthStart,$lastMonthEnd];
    }

    /**
     * 本月时间范围
     * @return [type] [description]
     */
    public function monthAmong()
    {
        $thisMonthStart =  strtotime(date('Y-m-01', time()));                             // 本月第一天
        $lastMonthStart =  mktime(0, 0, 0, date("m")+1, 1, date("Y"))-1;       // 本月最后一天
        return [ $thisMonthStart, $lastMonthStart];
    }

    /**
     * 上一年时间范围
     * @return [type] [description]
     */
    public function lastYearAmong()
    {
        $lastYearStart = strtotime((date('Y', time())-1).'-01-01');
        $lastYearEnd = strtotime(date('Y', time()).'-01-01')-1;
        return[$lastYearStart,$lastYearEnd];
    }

    /**
     * 当前年份时间范围
     */
    public function YearAmong()
    {
        $thisYearStart = strtotime(date('Y', time()).'-01-01');
        $thisYearEnd = strtotime((date('Y', time()) + 1).'-01-01')-1;
        return [$thisYearStart,$thisYearEnd];
    }

    /**
     * 本年排名,本月排名
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function currentRank($type)
    {
        if (strtolower($type) == 'year') {
            //本年排名
            $limits = $this->YearAmong();
            $rank   = 'year_rank';
            $income = 'year_income';
        } else if (strtolower($type) == 'month') {
            //本月排名
            $limits = $this->monthAmong();
            $rank   = 'month_rank';
            $income = 'month_income';
        }
            $pointRanks = self::getRankBy20($limits);
            dispatch(new \App\Jobs\PointRank($rank, $income, $pointRanks));
            return true;
    }

    /**
     * 上年排名,上月排名
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function lastRank($type)
    {
        if (strtolower($type) == 'year') {
            //本年排名
            $limits = $this->lastYearAmong();
            $rank   = 'last_year_rank';
        } else if (strtolower($type) == 'month') {
            //本月排名
            $limits = $this->lastMonthAmong();
            $rank   = 'last_month_rank';
        }
            $pointRanks = self::getRankBy20($limits);
            DB::beginTransaction();
        foreach ($pointRanks as $k => $v) {
            dispatch(new \App\Jobs\LastPointRank($rank, $v));
        }
            DB::commit();
            return true;
    }

     //获取前20条的排名
    public function getRankBy20($limits)
    {
        return DB::select("
            select s.*,@rank := @rank + 1 as rank from (
                select user_id,sum(point_received.points) as total_points from point_received join users
                on point_received.user_id = users.id
                where unix_timestamp(point_received.created_at) between $limits[0] and $limits[1]
                and type != 'consumer' and type != 'reback' and users.phone <> '' and users.status = 1
                group by user_id
            ) s , (select @rank := 0) init
                order by total_points desc
                limit 100
        ");
    }

    // 用户积分值
    public function userIncome($userId, $limit)
    {
        if ($limit == 'month') {
            $limits = $this->monthAmong();
        } elseif ($limit == 'year') {
            $limits = $this->YearAmong();
        }
        return PointReceived::where('user_id', $userId)
            ->where('type', '<>', 'consumer')
            ->where('type', '<>', 'reback')
            ->whereBetween(\DB::raw('unix_timestamp(created_at)'), [$limits[0],$limits[1]])
            ->sum('points');
    }

    // 上月排名
    public function lastMonthRewardJob()
    {
        $LastPointRank = PointReceived::selectRaw("user_id, sum(points) as sum_points, max(created_at) as ctime")
            ->where('type', '!=', 'consumer')
            ->where('type', '!=', 'reback')
            ->where('created_at', '>=', Carbon::now()->subMonth()->toDateTimeString())
            ->where('created_at', '<', (new Carbon('first day of this month'))->toDateTimeString())
            ->groupBy('user_id')
            ->orderBy('sum_points', 'desc')
            ->orderBy('ctime')
            ->whereHas('user', function ($q) {
                return $q->where('status', 1);
            })
            ->limit(100)
            ->get();
        if (empty($LastPointRank)) {
            return false;
        }
        $year = Carbon::now()->subMonth()->year;
        $month = Carbon::now()->subMonth()->month;
        dispatch(new \App\Jobs\LastMonthRewardJob($month, $year, $LastPointRank));
    }
}
