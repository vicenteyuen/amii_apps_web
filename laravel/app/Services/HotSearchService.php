<?php

namespace App\Services;

use App\Models\HotSearch;

class HotSearchService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        $itemsQuery = HotSearch::whereIn('name', $arr);
        // 增加次数
        $itemsQuery->increment('number');
        // 已有搜索
        $itemsNameArr = $itemsQuery->pluck('name')
            ->toArray();

        $unInItemsArr = array_diff($arr, $itemsNameArr);
        $createData = array_map(function ($name) {
            return ['name' => $name];
        }, $unInItemsArr);
        sort($createData);

        if (! empty($createData)) {
            HotSearch::insert($createData);
        }

        return true;
    }

    public function hot()
    {
         return HotSearch::orderBy('number', 'desc')
            ->take(config('amii.hotSearchPaginate'))
            ->get();
    }
}
