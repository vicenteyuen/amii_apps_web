<?php

namespace App\Services\Deepdraw;

use App\Libraries\Deepdraw\Deepdraw;

class CategoryService
{
    const PROVIDER = '分类';
    /**
     * 根据分类id获取所有子分类
     */
    public function category($id)
    {
        $id=2;
        // id为1 是最顶级分类
        $uri = '/trades;id=' . $id . '/subs';
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log(CategoryService::PROVIDER . '子分类', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }

        $deepdraw->log( CategoryService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );

        $responseData = json_decode($response->getBody(), true);

        $data = [
            'deepdrawCategoryId' => $id,
            'deepdrawCategorys' => $responseData
        ];

        app('amii.amiiDeepdraw.category')->storeCategorys($data);
    }
}
