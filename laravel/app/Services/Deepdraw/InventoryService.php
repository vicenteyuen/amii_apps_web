<?php

namespace App\Services\Deepdraw;

use App\Libraries\Deepdraw\Deepdraw;

class InventoryService
{
    const PROVIDER = '库存';
    /**
     * 根据商品id获取sku库存
     */
    public function inventorys($id)
    {
        $uri = '/products;id=' . $id . '/sku';
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log(InventoryService::PROVIDER . '列表', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }

        $deepdraw->log( InventoryService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );

        $responseData = json_decode($response->getBody(), true);

        app('amii.amiiDeepdraw.product.item')->sku($id, $responseData);
    }
}
