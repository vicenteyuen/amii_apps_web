<?php

namespace App\Services\Deepdraw;

use App\Libraries\Deepdraw\Deepdraw;

class ProductService
{
    const PROVIDER = '商品';
    /**
     * 根据商品id获取商品信息
     */
    public function show($id)
    {
        $uri = '/products;id=' . $id;
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log(ProductService::PROVIDER . '信息', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }
        $deepdraw->log( ProductService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );

        $responseData = json_decode($response->getBody(), true);

        app('amii.amiiDeepdraw.product.item')->base($id, $responseData);
    }

    /**
     * 根据商品id获取商品图片
     */
    public function productImage($id)
    {
        $uri = '/products;id=' . $id . '/pictures';
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log(ProductService::PROVIDER . '图片', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }
        $deepdraw->log( ProductService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );

        $responseData = json_decode($response->getBody(), true);

        app('amii.amiiDeepdraw.product.item')->picture($id, $responseData);
    }

    /**
     * 根据商品id获取商品详情
     */
    public function productDetail($id)
    {
        $uri = '/products;id=' . $id . '/visionResource';
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log(ProductService::PROVIDER . '详情', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }
        $deepdraw->log( ProductService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );

        $responseData = json_decode($response->getBody(), true);

        app('amii.amiiDeepdraw.product.item')->vision($id, $responseData);
    }
}
