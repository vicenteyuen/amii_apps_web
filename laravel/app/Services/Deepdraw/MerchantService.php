<?php

namespace App\Services\Deepdraw;

use App\Libraries\Deepdraw\Deepdraw;

class MerchantService
{
    const PROVIDER = '商户';

    /**
     * 获取商户信息
     */
    public function show($id)
    {
        $uri = '/merchants;id=' . $id;
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);


        if ($response->getStatusCode() != 200) {
            $deepdraw->log( MerchantService::PROVIDER . '信息', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }

        $deepdraw->log( MerchantService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );


        $responseData = json_decode($response->getBody(), true);

        $store = app('amii.amiiDeepdraw.merchant')->store($responseData);

        if (! $store) {
            return false;
        }
        return true;
    }

    /**
     * 按名称模糊查询商户
     */
    public function search($name)
    {
        $uri = '/merchants;name=' . $name;
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log( MerchantService::PROVIDER . '查询信息', $name, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }

        $deepdraw->log( MerchantService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );

        $responseData = json_decode($response->getBody(), true);

        $store = app('amii.amiiDeepdraw.merchant')->search($responseData);

        if (! $store) {
            return false;
        }
        return true;
    }

    /**
     * 获取商户上货期数
     */
    public function saleCalender($id)
    {
        $page = 1;

        PAGE:
        $uri = '/merchants;id=' . $id . '/saleCalender?pageNo=' . $page;
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log( MerchantService::PROVIDER . '上货期数', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }

        $deepdraw->log( MerchantService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );

        $responseData = json_decode($response->getBody(), true);
        $data = [
            'saleCalenderData' => $responseData,
            'merchantId' => $id
        ];

        app('amii.amiiDeepdraw.merchant')->saleCalender($data);


        ## TODO: NEED review
        if ($page > 50) {
            return;
        }

        $page = $page + 1;

        if (! empty($responseData) ) {
            goto PAGE;
        }
    }

    /**
     * 获取商户上货日期上货期数的商品
     */
    public function calenderProducts($id, $day)
    {
        $uri = '/merchants;id=' . $id . '/saleCalender;day=' . $day . '/listProduct';
        $deepdraw = new Deepdraw();
        $response = $deepdraw->request($uri);
        if ($response->getStatusCode() != 200) {
            $deepdraw->log( MerchantService::PROVIDER . '上货日期', $id, 'url: ' . $uri . 'error: ' . $response->getHeaders());
            return false;
        }

        $deepdraw->log( MerchantService::PROVIDER , $id, 'url: ' . $uri . ' response: ' . $response->getBody() , 'info' );


        $responseData = json_decode($response->getBody(), true);

        $arr = [
            'merchantId' => $id,
            'data' => $responseData
        ];

        app('amii.amiiDeepdraw.merchant')->calenderProducts($arr);
    }
}
