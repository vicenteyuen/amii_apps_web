<?php

namespace App\Services;

use App\Models\Enable;
use App\Models\User;
use App\Models\LevelEnable;

class EnableService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        return Enable::create($arr);
    }

    public function index()
    {
        return Enable::get();
    }

    /**
     * 删除
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return Enable::destroy($id);
    }

    /**
     * 会员折扣
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function discount($userId)
    {
        $level = User::where('id', $userId)
            ->value('level');
        return LevelEnable::where('level_id', $level)
            ->where('enable_id', 1)
            ->value('get');
    }
}
