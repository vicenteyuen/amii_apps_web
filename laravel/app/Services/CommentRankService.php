<?php

namespace App\Services;

use App\Models\CommentRank;

class CommentRankService
{
    public function index()
    {
        return CommentRank::get();
    }
}
