<?php

namespace App\Services;

use App\Models\OrderProductRefundImage;

class OrderRefundImageService
{
    /**
     * 售后图片保存
     */
    public function store(array $data)
    {
        return OrderProductRefundImage::insert($data);
    }
}
