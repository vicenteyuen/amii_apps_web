<?php

namespace App\Services;

use App\Models\UserAddress;
use App\Models\Province;
use App\Models\Area;
use App\Models\City;
use DB;

class UserAddressService
{
    /**
    * 新增地址
    * @param $arr array
    * @return bool
    */
    public function store(array $arr)
    {
        //第一条设为默认地址
        $collection = UserAddress::where('user_id', $arr['user_id'])
            ->where('status', '1')
            ->first();

         // 该地址是否存在
        if (! $this->checkAddress($arr['region_id'])) {
            return false;
        }
        //新增积分
        app('pointReceived')->userAddPoint('update', 'address');

        if (is_null($collection)) {
            $arr['status'] = 1;

            return UserAddress::create($arr);
        }

        // 没有设为默认地址并且不是第一条
        if (! $arr['status']) {
            return UserAddress::create($arr);
        }

        // 新增地址并设为默认地址
        DB::beginTransaction();

        $updateStatus = UserAddress::where('user_id', $arr['user_id'])
            ->update(['status' => 0]);

        $createModel = UserAddress::create($arr);

        if (! $updateStatus || ! $createModel) {
            DB::rollback();

            return false;
        }

        DB::commit();

        return true;
    }

    /**
     * 更新地址数据
     * @param   $id  int
     * @param   $arr array
     * @return  bool
     */
    public function update(array $arr, $id, $user_id)
    {
        // 该地址是否存在
        if (! $this->checkAddress($arr['region_id'])) {
            return false;
        }

        if (isset($arr['status'])) {
            DB::beginTransaction();

            $statusDefault = UserAddress::where('user_id', $user_id)
                ->update(['status' => 0]);

            $setStatus = UserAddress::where('id', $id)
                ->update($arr);

            if (! $statusDefault || ! $setStatus) {
                DB::rollback();

                return false;
            }
            DB::commit();

            return true;
        }

        return UserAddress::where('id', $id)
            ->update($arr);
    }

    /**
     * 设置默认地址
     * @param  [type] $id
     * @param  [type] $status array
     * @return [type] bool
     */
    public function updateStatus($id, $user_id, $status)
    {
        $collection = UserAddress::where('user_id', $user_id)
            ->where('status', 1)
            ->first();

         // 原先未设置默地址
        if (is_null($collection)) {
            return UserAddress::where('id', $id)
                ->update($status);
        }

        DB::beginTransaction();

        $statusDefault = UserAddress::where('user_id', $user_id)
            ->update(['status' => 0]);

        $setStatus = UserAddress::where('id', $id)
            ->update($status);

        if (! $statusDefault || ! $setStatus) {
            DB::rollback();

            return false;
        }
        DB::commit();

        return true;
    }

    /**
     * 删除地址
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        $address = UserAddress::find($id);

        // 删除地址是默认地址
        if ($address && $address->status == 1) {
            $update = UserAddress::where('user_id', $address->user_id)
                ->where('id', '<>', $id)
                ->orderBy('created_at', 'desc')
                ->first();

            // 删除默认地址并且该用户没有其他地址
            if (! $update) {
                return UserAddress::destroy($id);
            }
            DB::beginTransaction();
            $delete = UserAddress::destroy($id);
            $update->status = 1;
            $save = $update->save();
            if ($save && $save) {
                DB::commit();

                return true;
            }

            DB::rollback();

            return false;
        }

        return UserAddress::destroy($id);
    }

    /**
     * 单条地址详情
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function index($id)
    {
        $data = UserAddress::find($id);
        if (! $data) {
            return null;
        }
        $data['address'] = $this->address($data['region_id']);

        return $data;
    }

    /**
     * 默认地址
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function statusDetail($id)
    {
        $arr = [
            'user_id' => $id,
            'status' => 1,
        ];
        $data = UserAddress::where($arr)
            ->first();

        if (! $data) {
            return null;
        }
        $data['address'] = $this->address($data['region_id']);

        return $data;
    }

    /**
     * 获取地址信息
     * @return collection
     */
    public function show($user_id)
    {
        $data = UserAddress::where('user_id', $user_id)
            ->orderBy('status', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();

        if ($data->isEmpty()) {
            return null;
        }
        $data = $data->toArray();
        foreach ($data as $k => &$v) {
            $v['address'] = $this->address($v['region_id']);
        }

        return $data;
    }

    /**
     * 检查添加的地址是否存在
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    protected function checkAddress($code)
    {
        return Area::where('area_id', $code)
            ->first() || City::where('city_id', $code)->first();
    }

    /**
     * 返回中国所有地址数据
     * @return [type] [description]
     */
    public function addressData()
    {
        return Province::with('City.Area')
            ->get();
    }

    /**
     * 查询时获取省市区数据
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    protected function address($code)
    {
        $area = Area::where('area_id', $code)
        ->first();

        $city = City::where('city_id', $area['city_id'])
        ->first();

        if (! $area) {
            $city = City::where('city_id', $code)
            ->first();
        }

        $province = Province::where('province_id', $city['province_id'])
        ->first();
        $address['province'] = $province;
        $address['city'] = $city;
        $address['area'] = $area;

        return $address;
    }

    /**
     * 获取用户默认地址
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function default($userId)
    {
        $addr = UserAddress::where('user_id', $userId)
            ->where('status', 1)
            ->first();
        if (! $addr) {
            return null;
        }
        $addr['address'] = $this->address($addr['region_id']);

        return $addr;
    }

    /**
     * 获取用户地址详情
     */
    public function addrOne($userId, $addrId)
    {
        $addr = UserAddress::where('id', $addrId)
        ->where('user_id', $userId)
        ->first();
        if (! $addr) {
            return null;
        }
        $addr->address = self::address($addr->region_id);

        return $addr;
    }

    public function province()
    {
        return Province::get();
    }
}
