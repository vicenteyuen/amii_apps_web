<?php

namespace App\Services;

use App\Models\PointLevel;
use App\Models\User;
use App\Models\Order;

use App\Jobs\CheckUserLevelJob;

class PointLevelService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        return PointLevel::create($arr);
    }

    public function index()
    {
        return PointLevel::get();
    }

    /**
     * 删除
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return PointLevel::destroy($id);
    }

    /**
     * 积分分数所属等级
     * @return [type] [description]
     */
    public function points($scores, $userId = null)
    {
        $levelPoints = $this->index();
        $len = count($levelPoints);

        for ($i=$len - 1; $i >= 0; $i--) {
            if ($scores >= $levelPoints[$i]['points']) {
                return $this->level($i+1, $userId);
            }
        }
        return false;
    }

    // 积分加上购物金额
    public function level($level, $userId)
    {
        // 未验证手机号
        if (!$this->checkPhone($userId)) {
            return 1;
        }
        //未身份验证
        if (!$this->checkWeiXin($userId)) {
            return 1;
        }

        // 购物次数等级
        $purchaseLevel = $this->checkPurchaseTime($userId);
        if ($purchaseLevel == -1) {
            if ($level <= 3) {
                return $level;
            }
            return 3;
        }

        $priceLevel = $this->purchaseLevel($userId);
        if ($priceLevel == -1) {
            // 不限制购物金额只针对vip2及以下等级
            if ($level <= 3) {
                return $level;
            }
            return 3;
        }
        return min($level, $priceLevel, $purchaseLevel);
    }

    public function purchaseLevel($userId)
    {
        $purchasePrice = app('order')->userConsumer($userId);
        if ($purchasePrice < 100 && $purchasePrice >= 0) {
            return -1;
        } elseif ($purchasePrice < 300 && $purchasePrice >= 100) {
            return 4;
        } elseif ($purchasePrice < 500 && $purchasePrice >= 300) {
            return 5;
        } elseif ($purchasePrice < 1000 && $purchasePrice >= 500) {
            return 6;
        } elseif ($purchasePrice < 2000 && $purchasePrice >= 1000) {
            return 7;
        } elseif ($purchasePrice < 5000 && $purchasePrice >= 2000) {
            return 8;
        } elseif ($purchasePrice < 10000 && $purchasePrice >= 5000) {
            return 9;
        } elseif ($purchasePrice < 20000 && $purchasePrice >= 10000) {
            return 10;
        } elseif ($purchasePrice >= 20000) {
            return 11;
        }
        return 1;
    }

    public function checkLevel($userId = 0)
    {
        $checkUser = User::where('id', '>', $userId)
            ->orderBy('id')
            ->first();
        if ($checkUser) {
            $checkUserId = $checkUser->id;
            $level = $this->points($checkUser->sum_points, $checkUserId);
            $checkUser->level = $level;
            $checkUser->save();

            // 加入队列校验下一个等级
            $job = new CheckUserLevelJob($checkUserId);
            dispatch($job);
        }
        return;
    }

    //验证手机
    public function checkPhone($userId)
    {
        $user = User::where('id', $userId)
            ->first();
        if ($user->phone) {
            return true;
        }
        return false;
    }

    //验证是否绑定微信
    public function checkWeiXin($userId)
    {
        return app('socialite')->isWeixin($userId);
    }

    //购物次数
    public function checkPurchaseTime($userId)
    {
        $count = Order::where('user_id', $userId)
            ->withTrashed()
            ->where('provider', 'standard')
            ->where('pay_status', 1)
            ->where('order_status', 1)
            ->count();
        if ($count == 0) {
            return -1;
        } elseif ($count < 3 && $count >= 1) {
            return 4;
        } elseif ($count < 5 && $count >= 3) {
            return 5;
        } elseif ($count < 10 && $count >= 5) {
            return 6;
        } elseif ($count < 15 && $count >= 10) {
            return 7;
        } elseif ($count < 30 && $count >= 15) {
            return 8;
        } elseif ($count < 40 && $count >= 30) {
            return 9;
        } elseif ($count < 50 && $count >= 40) {
            return 10;
        } elseif ($count >=50) {
            return 11;
        }
        return 1;
    }
}
