<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\UserGame;
use App\Models\GameRelationUser;
use App\Models\SellProduct;
use App\Models\UserGameAdvice;
use App\Models\User;
use App\Helpers\GamePuzzlesHelper;
use App\Jobs\UserGameExpire;
use App\Jobs\GameOrderExpiredAndRebackInventory;
use App\Jobs\NewGameOrder;
use App\Jobs\pushGameDocument;
use App\Models\GamePushDocument as PushDocument;
use App\Models\UserCollection;
use Ecommerce\Models\Inventory;
use App\Models\OrderProduct;
use Carbon\Carbon;
use App\Models\Activity;
use DB;

class UserGameService
{

    protected $time;
    public function __construct()
    {
        $this->time = date('Y-m-d H:i:s', time());
    }
    /**
     * 参加游戏调用
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function store($sellProduct)
    {
        $data['user_id'] = auth()->id();
        $data['sell_product_id'] = $sellProduct->id;
        $data['finished_number'] = 1;
        $data['count'] = $sellProduct->game_puzzles;
        $rowCol = GamePuzzlesHelper::countRowCol($data['count']);
        if (!$rowCol) {
            return false;
        }
        $data['row'] = $rowCol['row'];
        $data['col'] = $rowCol['col'];
        $create =  UserGame::create($data);
        if (!$create) {
            return false;
        }
        $gameRelation['user_games_id'] = $create->id;
        $gameRelation['user_id']      = $data['user_id'];
        $relationUser = GameRelationUser::create($gameRelation);
        // 玩游戏总数加一
        $sellProduct->game_number++;
        $save = $sellProduct->save();
        if ($create && $relationUser && $save) {
            dispatch((new UserGameExpire($create->id))->delay(60 * 60 * 24));

            //推送
            $hours = PushDocument::where('provider', 2)
                ->value('hours');
            $gameDelay = 24 * 60 * 60 - $hours * 60 * 60;

            if ($gameDelay <= 0) {
                $gameDelay = 0;
            }
            $jobGame = (new pushGameDocument(2, $create->id, $sellProduct->product->name, $sellProduct->product->snumber))->delay($gameDelay);
            dispatch($jobGame);

            return $create;
        }
        return false;
    }

    /**
     * 其他用户激活
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function relationUserAdd(array $data)
    {
        DB::beginTransaction();
        $success = true;
        $message = ['status' => 0,'message'=> '','data' =>'','new' => 0];

        $HasActivated = GameRelationUser::where('user_id', $data['user_id'])
            ->whereHas('game', function ($q1) {
                return $q1->where('order_status', 1)
                      ->orWhere(function ($q2) {
                        return $q2->where('expired', 0)
                            ->where('success', 0);
                      })->orWhere(function ($q3) {
                        return $q3->where('success', 1)
                            ->where('order_status', 0);
                      });
            })
            ->lockForUpdate()
            ->first();
        // 已被激活
        if ($HasActivated) {
            $message['message'] = '一个用户只能激活一次';
            return $message;
        }

        $userGame = UserGame::find($data['user_games_id']);
        if (!$userGame) {
            $message['message'] = '激活失败,当前游戏或已下架';
            return $message;
        }
        $sellProduct = SellProduct::find($userGame->sell_product_id);

        $successorExpired = UserGame::where('id', $data['user_games_id'])
            ->where(function ($q1) {
                return $q1->where('success', 1)
                    ->orWhere(function ($q2) {
                        return $q2->where('success', 0)
                            ->where('expired', 1);
                    });

            })
            ->first();

        if ($successorExpired) {
            $message['message'] = '拼图已完成,请刷新页面';
            return $message;
        }

        $userGame->lockForUpdate();
        // 完成拼图
        if ($userGame->finished_number + 1 == $userGame->count) {
            $userGame->success = 1;
            $userGame->expired_time = time() + 24 * 60 * 60 * 5; //过期时间
            $userGame->order_remain_time = time(); //成功时间
            $userGame->success_time = time() - strtotime($userGame->created_at);
            $success = $sellProduct->save();

            // 完成推送
            $sellProduct = SellProduct::where('id', $userGame->sell_product_id)
                ->first();
            \App\Helpers\GamePushDocumentHelper::pushGameDocument($userGame->user_id, 4, $sellProduct->product->name, $sellProduct->product->snumber);

            //未合成倒计时推送
            $hours = PushDocument::where('provider', 3)
                ->value('hours');
            $gameDelay = 24 * 60 * 60 - $hours * 60 * 60;
            if ($gameDelay <= 0) {
                $gameDelay = 0;
            }
            $jobGame = (new pushGameDocument(3, $userGame->id, $sellProduct->product->name, $sellProduct->product->snumber))->delay($gameDelay);
            dispatch($jobGame);

            // 添加队列，120小时内未合成过期
            $delay = 24 * 60 * 60 * 5;
            $job = (new NewGameOrder($userGame->id))->delay($delay);
            dispatch($job);

            // 添加队列，72小时内未合成过期
            // $delay = time() + 24 * 60 * 60 * 3;
            // $job = (new GameOrderExpiredAndRebackInventory($userGame->id))->delay($delay);
            // dispatch($job);
        }
        $userGame->finished_number++;
        $save = $userGame->save();
        $user = User::find($data['user_id']);
        $user->user_game_id = 0;
        $saveUser = $user->save();
        $create = GameRelationUser::create($data);
        if ($save && $create && $success && $saveUser) {
            DB::commit();
            $message['status'] = 1;
            $message['data'] = $userGame;
            //用户激活推送
            if ($userGame->count != $userGame->finished_number) {
                 $activeUser = User::find($data['user_id']);
                \App\Helpers\GamePushDocumentHelper::pushGameDocument($userGame->user_id, 1, '', '', $activeUser->nickname, $userGame->count - $userGame->finished_number);
            }

            $message['new'] = $this->checkNewActivate($data['user_id']);
            return $message;
        }
        DB::rollback();
        $message['message'] = '操作失败,请刷新页面';
        return $message;
    }

    // 判断刚激活的用户是否是刚注册获取优惠卷的
    public function checkNewActivate($id)
    {
        $check = User::where('id', $id)
            ->where('created_at', '>=', Carbon::now()->subMinute())
            ->first();
        if ($check) {
            return true;
        }
        return false;

    }

    /**
     * 我的拼图
     * @return [type] [description]
     */
    public function userPlay($userId)
    {
        return UserGame::where('user_id', $userId)
            ->with('sellProduct.product')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * 拼图列表(首页)
     * @return [type] [description]
     */
    public function index($type)
    {
        if ($type) {
            $compare = '=';
        } else {
            $compare = '!=';
            $type = 0;
        }
        $userId = auth()->id();
        SellProduct::$allImageEmpty = true;
        return SellProduct::where('provider', 3)
            ->where('status', 1)
            ->where('puzzles', $compare, $type)
            ->with('product')
            ->with(['collection' => function ($q1) use ($userId) {
                return $q1->where('user_id', $userId);
            }])
            ->paginate(config('amii.apiPaginate'));
    }

    /**
     * 当前拼图
     * @param  [type] $id     [description]
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function userGame($id, $userId)
    {
         $data =  UserGame::where('id', $id)
            ->where('user_id', $userId)
            ->with('sellProduct.product')
            ->with('order')
            ->first();
        if ($data) {
            $data->time_to_in = app('amii.game.order')->checkTimesLessThree($userId);
        }
        return $data;
    }

    /**
     * 游戏进程
     * @param  [type] $sell_product_id [description]
     * @return [type]                  [description]
     */
    public function playingGame($userGameId)
    {
        $game = GameRelationUser::where('user_games_id', $userGameId)
            ->with('user')
            ->with('game')
            ->orderBy('created_at', 'desc')
            ->get();
        $position = count($game);
        foreach ($game as $key => &$value) {
            $value->position = $position--;
        }
        return $game;
    }

    //用户进行中的拼图
    public function userPlayingGame()
    {
        return UserGame::where('user_id', auth()->id())
            ->where('success', 0)
            ->where('expired', 0)
            ->orWhere(function ($q1) {
                return $q1->where('success', 1)
                    ->where('order_status', 0)
                    ->where('user_id', auth()->id());
            })
            ->with('order')
            ->with('sellProduct.product')
            ->paginate(config('amii.apiPaginate'));
    }

    //过期游戏
    public function expireGame()
    {
        return UserGame::where('user_id', auth()->id())
            ->where('expired', 1)
            ->orWhere(function ($q1) {
                return $q1->where('success', 1)
                    ->where('order_status', 2)
                    ->where('user_id', auth()->id());
            })
            ->with('sellProduct.product')
            ->paginate(config('amii.apiPaginate'));
    }

    //参加人数
    public function joinUsers()
    {
        return GameRelationUser::count();
        // return self::joinGame() + self::gameCollection();
    }

    //送出的游戏个数
    public function sendProductNumber()
    {
        $send = UserGame::where('order_status', 1)
            ->get();
        return count($send);
    }
    //送出的当天剩余个数
    public function remainNumber()
    {
        $dt = Carbon::now();
        $start = $dt->startOfDay()->toDateTimeString();
        $number = UserGame::where('success', 1)
            ->where('order_status', 1)
            ->where('updated_at', '>=', $start)
            ->count();
        $sendNumber = Activity::where('provider', 'game')
            ->value('rule')?:300;
        $remainNumber = $sendNumber - $number;
        if ($remainNumber < 0) {
            $remainNumber = 0;
        }
        return $remainNumber;
    }


    //返回格数
    public function returnPuzzles($orderId)
    {
        return UserGame::where('order_id', $orderId)
            ->value('count');
    }

    //发起人数
    public function joinGame($request = null)
    {
        $this->setTime($request);
        $userGames = UserGame::groupBy('user_id')
            ->where('created_at', '<=', $this->time)
            ->get();
        return count($userGames);
    }

    //点亮人数
    public function gameCollection($request = null)
    {
        $this->setTime($request);
        // $collections = UserCollection::where('provider', 2)
        //     ->groupBy('user_id')
        //     ->where('created_at', '<=', $this->time)
        //     ->get();
        // return count($collections);
        return GameRelationUser::count();
    }

    //发起次数
    public function joinGameTime($request = null)
    {
        $this->setTime($request);
        $times = UserGame::where('created_at', '<=', $this->time)
            ->get();
        return count($times);
    }

    //人均发起次数
    public function personPerTime($request)
    {
        $joinGame = self::joinGame();
        if (!$joinGame) {
            return 0;
        }
        $average = self::joinGameTime()/$joinGame;
        return number_format($average, 2, '.', '');
    }

    //送出人数
    public function sendPerson($request = null)
    {
        $this->setTime($request);
        $personNumber = UserGame::where('order_status', 1)
            ->where('updated_at', '<=', $this->time)
            ->groupBy('user_id')
            ->get();
        return count($personNumber);
    }

    //送出件数
    public function sendGameTime($request = null)
    {
        $this->setTime($request);
        $sendProductTime = UserGame::where('order_status', 1)
            ->where('updated_at', '<=', $this->time)
            ->get();
        return count($sendProductTime);
    }

    //人均获取件数
    public function personPerGetTime($request)
    {
        $sendPerson = self::sendPerson();
        if (!$sendPerson) {
            return 0;
        }
        $average = self::sendGameTime()/$sendPerson;
        return number_format($average, 2, '.', '');
    }

    //待合成次数
    public function waitCommit($request)
    {
        $this->setTime($request);
        $uncommit = UserGame::where('success', 1)
            ->where('order_status', 0)
            ->where('created_at', '<=', $this->time)
            ->get();
        return count($uncommit);
    }

    //趋势 每日发起次数，每日发起人数，每日送出件数
    public function trend($date = null)
    {
        $labels = [];
        //统计这个月
        if (!$date) {
            $for_datetime = Carbon::now()->subDay(29);
        } else {
            $for_datetime = Carbon::parse($date)->subDay(29);
        }

        $start_datetime = clone $for_datetime;
        $labels[] = $for_datetime->format('Y-m-d');
        for ($i=0; $i < 29; $i++) {
            $for_datetime = $for_datetime
                ->addDay();
            $labels[] = $for_datetime->format('Y-m-d');
        }
        //每日发起次数
        $perTime = UserGame::selectRaw('*,count(id) as count, date_format(created_at,"%Y-%m-%d") as date')
            ->where('created_at', '>=', $start_datetime->toDateString())
            ->where('created_at', '<=', $for_datetime->addDay()->toDateString())
            ->groupBy(DB::raw('date_format(created_at,"%Y-%m-%d")'))
            ->get()
            ->each(function ($user) {
                $user->setVisible(['count', 'date']);
            });
        $arr = [];
        foreach ($labels as $v) {
            $date = $perTime->where('date', $v)->first();
            $arr[] = $date ? $date['count'] : 0;
        }
        $data['perTime'] = $arr;

        //每日发起人数
        $perPerson = UserGame::selectRaw('*,count(id) as count, date_format(created_at,"%Y-%m-%d") as date')
            ->where('created_at', '>=', $start_datetime->toDateString())
            ->where('created_at', '<=', $for_datetime->addDay()->toDateString())
            ->groupBy('user_id')
            ->groupBy(DB::raw('date_format(created_at,"%Y-%m-%d")'))
            ->get()
            ->each(function ($user) {
                $user->setVisible(['count', 'date']);
            });
        $arr = [];
        foreach ($labels as $v) {
            $date = $perPerson->where('date', $v)->first();
            $arr[] = $date ? $date['count'] : 0;
        }
        $data['perPerson'] = $arr;

        //每日送出件数
        $sendProduct = UserGame::selectRaw('*,count(id) as count, date_format(updated_at,"%Y-%m-%d") as date')
            ->where('order_status', 2)
            ->where('updated_at', '>=', $start_datetime->toDateString())
            ->where('updated_at', '<=', $for_datetime->addDay()->toDateString())
            ->groupBy(DB::raw('date_format(updated_at,"%Y-%m-%d")'))
            ->get()
            ->each(function ($user) {
                $user->setVisible(['count', 'date']);
            });
        $arr = [];
        foreach ($labels as $v) {
            $date = $sendProduct->where('date', $v)->first();
            $arr[] = $date ? $date['count'] : 0;
        }
        $data['sendProduct'] = $arr;
        $data['labels'] = $labels;
        return $data;
    }

    //设置筛选时间
    public function setTime($request)
    {
        if ($request && $request->date) {
            $this->time = $request->date;
        }
    }

    // 判断用户是否有正在进行的游戏
    public function checkUserPlaying()
    {
        if ($userId = auth()->id()) {
            $exits = UserGame::where('user_id', $userId)
                ->where('expired', 0)
                ->where('success', 0)
                ->first();
            if ($exits) {
                return true;
            }
            return false;
        }
        return false;
    }

    //更新状态
    public function update($id, $arr)
    {
        return UserGame::where('id', $id)
            ->update($arr);
    }

    //用户是否可以参加游戏
    public function checkedUserJoinGame()
    {
        $userId = auth()->id();
        if (!$userId) {
            return true;
        }
         // 一个用户一天最多发起三次
        $lessThree = app('amii.game.order')->checkTimesLessThree($userId);
        if ($lessThree <= 0) {
            return false;
        }
        //检查当前是否有正在进行的拼图
        $exitsPlaying = app('amii.game.order')->checkUserPlaying($userId);
        if (!$exitsPlaying) {
            return false;
        }
        return true;
    }

    // 通过sku计算正在进行的游戏
    public function countPlayingBySkuCode($skueCode)
    {
        $inventory = Inventory::where('sku_code', $skueCode)
            ->first();
        if (!$inventory) {
            return 0;
        }
        $inventoryId = $inventory->id;

        return OrderProduct::where('inventory_id', $inventoryId)
            ->whereHas('order', function ($query) use ($inventoryId) {
                return $query->whereHas('game', function ($q) use ($inventoryId) {
                    return $q->where('expired', 0)
                        ->where('order_status', 0);
                });
            })
            ->groupBy('inventory_id')
            ->sum('number') ?:0;
    }
}
