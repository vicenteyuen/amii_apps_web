<?php
namespace App\Services;

use App\Models\SellAttributeValue;
use App\Models\SellAttribute;
use DB;

class SellAttributeValueService
{
    /**
     * 新增
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {
        $hasExists = SellAttributeValue::where($arr)
                        ->first();

        if (!is_null($hasExists)) {
            return ['status' =>false,'msg'=>'该属性值已存在'];
        }
        $data = SellAttributeValue::create($arr);
        if ($data) {
            return ['status' => true,'id' => $data->id];
        }
        return ['status' => false,'msg' => '新增属性值失败'];
    }

    public function update($arr)
    {
        return SellAttributeValue::where('id', $arr['id'])
            ->update(['sell_product_attribute_value_image' => $arr['sell_product_attribute_value_image']]);
    }

    /**
    *   删除属性
    */
    public function deleteAttribute($sellAttributes_id)
    {
        $attribute = DB::table('sell_attributes')->where('id', $sellAttributes_id)
                ->first();
        if (!$attribute) {
            return ['status' => 0];
        }
        $res = DB::table('sell_attributes')
            ->where('id', $sellAttributes_id)
            ->delete();
        if ($res) {
            return ['status' => 1 ,'attribute' => 1];
        }

        return ['status' => 0];

    }

    /**
     * 删除
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        DB::beginTransaction();

        $attribute = SellAttributeValue::where('id', $id)
            ->with('sellAttributes')
            ->first();

        $delete = SellAttributeValue::destroy($id);

        // 该属性是否有还有属性值
        $attributeHasValue = SellAttributeValue::where('sell_attribute_id', $attribute['sell_attribute_id'])
            ->first();

        if ($attributeHasValue) {
             DB::commit();
            if ($delete) {
                return ['status' => 1 ,'attribute' => 0];
            }
            return ['status' => 0];
        } else {
            // 删除属性(没有属性值)
            $attributeDelete = DB::table('sell_attributes')->where('id', $attribute['sellAttributes']['id'])
                ->delete();
        }

        if ($delete && $attributeDelete) {
            DB::commit();
            return ['status' => 1 ,'attribute' => 1];
        }

        DB::rollback();
         return ['status' => 0];


    }

    /**
     * 获取单个属性的所有属性值
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function attributeValues($id)
    {
        return SellAttributeValue::where('sell_attribute_id', $id)
            ->with('attributeValue')
            ->get();

    }

    /**
     * 生成库存
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function inventory($id)
    {
        $sellAttributes =  SellAttribute::where('product_sell_attribute_id', $id)
             ->with('attribute.attributeValue')
             ->get();

        $sellAttributeValues =  SellAttributeValue::whereIn('sell_attribute_id', $sellAttributes->pluck('id'))
            ->lists('attribute_value_id');

        if ($sellAttributes->isEmpty()) {
            return false;
        }
        $sellAttributes =  $sellAttributes->toArray();

        foreach ($sellAttributes as $k1 => &$v1) {
            foreach ($v1['attribute']['attribute_value'] as $k2 => &$v2) {
                if (!in_array($v2['id'], $sellAttributeValues->toArray())) {
                    unset($v1['attribute']['attribute_value'][$k2]);
                }
            }
            $v1['attribute']['attribute_value'] = array_values($v1['attribute']['attribute_value']);
        }
        return $sellAttributes;
    }

    /**
     * 库存属性值
     * @param  [type] $sellProductId [description]
     * @return [type]                [description]
     */
    public function makeSellAttributeValue($sellProductId)
    {
        return SellAttribute::where('product_sell_attribute_id', $sellProductId)
            ->with('sellAttributeValue.attributeValue')
            ->get();
    }

    /**
     * 获取商品属性值
     * @param  [type] $sellProductId [description]
     * @return [type]                [description]
     */
    public function getSellAttributeValue($sellProductId)
    {
        $ids = SellAttribute::where('product_sell_attribute_id', $sellProductId)
            ->pluck('id')
            ->toArray();

        return SellAttributeValue::whereIn('sell_attribute_id', $ids)
            ->with('attributeValue')
            ->get();
    }
}
