<?php

namespace App\Services;

use DB;
use App\Models\Socialite;
use App\Models\User;

class SocialiteService
{
    /**
     * 校验是否绑定过微信
     */
    public function isWeixin($userId)
    {
        $socialites = Socialite::where('user_id', $userId)
            ->whereIn('provider', ['wechat', 'weweb', 'weapp'])
            ->get();
        if ($socialites->isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * 校验是否绑定qq
     */
    public function isQq($userId)
    {
        $socialites = Socialite::where('user_id', $userId)
            ->whereIn('provider', ['qqios', 'qqandroid'])
            ->get();
        if ($socialites->isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * 校验是否绑定微博
     */
    public function isWeibo($userId)
    {
        $socialites = Socialite::where('user_id', $userId)
            ->where('provider', 'weiboapp')
            ->get();
        if ($socialites->isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * 校验登录方式是否绑定
     */
    public function isBund($userId, $provider)
    {
        $socialite = Socialite::where('user_id', $userId)
            ->where(function ($query) use ($provider) {
                if (in_array($provider, ['wechat', 'weweb', 'wemp', 'weapp'])) {
                    return $query->whereIn('provider', ['wechat', 'weweb', 'wemp', 'weapp']);
                } elseif (in_array($provider, ['qqios', 'qqandroid'])) {
                    return $query->whereIn('provider', ['qqios', 'qqandroid']);
                } else {
                    return $query->where('provider', $provider);
                }
            })
            ->get();
        if ($socialite->isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * bund find or create socialite
     * @param \App\Models\User $user
     * @param $provider
     * @param $socialite
     * @return array
     */
    public function bundFindOrCreateByProvider($user, $provider, $socialite)
    {
        $userId = $user->id;
        $returnData = [
            'status' => false
        ];

        $data = self::getStoreData($provider, $socialite);
        if (! $data) {
            $returnData['message'] = '获取绑定账号数据失败';
            return $returnData;
        }
// dd($socialite, $data);
        $socialiteData = $data['socialiteData'];
        $unionid = isset($socialiteData['unionid']) ? $socialiteData['unionid'] : null;
        $userData = $data['user'];
        $openid = $socialiteData['openid'];

        $socialiteCollection = Socialite::where(function ($query) use ($provider, $openid) {
            return $query->where(function ($q1) use ($provider) {
                if (! in_array($provider, ['qqios', 'qqandroid'])) {
                    return $q1->where('provider', $provider);
                }
            })
            ->where('openid', $openid);
            return $query->where('provider', $provider)
                ->where('openid', $openid);
        })
        ->orWhere(function ($query) use ($unionid) {
            // 微信登录方式校验
            return $query->whereIn('provider', ['wechat', 'weweb', 'wemp', 'weapp'])
                ->where('unionid', $unionid);
        });
        $socialites = $socialiteCollection->get();

        if (! $socialites->isEmpty()) {
            // 校验登录方式是否绑定手机
            if (! (isset($socialites[0]) && $socialites[0]->user)) {
                $returnData['message'] = '网络错误';
                return $returnData;
            }
            if ($socialites[0]->user->phone) {
                $returnData['message'] = '绑定账号已绑定其它账号';
                return $returnData;
            }


            // 获取三方登录的用户的信息
            $userBundInfo = User::find($socialites[0]->user->id);
            //当前用户的信息
            $userInfo = User::find($userId);
            $updateInfo = [];
            if (!$userInfo->nickname && !empty($userBundInfo->nickname)) {
                $updateInfo['nickname'] = $userBundInfo->nickname;
            }
            if (!$userInfo->avatar && !empty($userBundInfo->avatar)) {
                $updateInfo['avatar'] = $userBundInfo->avatar;
            }
            // 更新用户昵称头像
            if (!empty($updateInfo)) {
                User::where('id', $userId)
                    ->update($updateInfo);
            }

            // 清除当前三方登录账号用户绑定到当前用户
            User::where('id', $socialites[0]->user->id)
                ->delete();
            $socialiteCollection->update([
                'user_id' => $userId
            ]);
            # 绑定微信提现渠道
            $user->withdrawChannels()
                ->updateOrCreate([
                    'type' => $provider
                ], [
                    'type' => $provider,
                    'account_number' => $socialiteData['openid'],
                    'account_name' => $userData['nickname']
                ]);
            $returnData['status'] = true;
            return $returnData;
        }
        // 当前绑定方式未登录过
        DB::beginTransaction();
        # 新增socialite
        Socialite::create(array_merge($socialiteData, ['user_id' => $userId]));

        \Log::info(self::class.'（'.$userId.'）第三方绑定:'.$provider, request()->all());

        # 绑定微信提现渠道
        $user->withdrawChannels()
            ->updateOrCreate([
                'type' => $provider
            ], [
                'type' => $provider,
                'account_number' => $socialiteData['openid'],
                'account_name' => $userData['nickname']
            ]);

        DB::commit();
        $returnData['status'] = true;
        return $returnData;
    }

    /**
     * find or create socialite
     */
    public function findOrCreateByProvider($provider, $socialite)
    {
        $returnData = [
            'status' => false,
        ];
        $data = self::getStoreData($provider, $socialite);
        if (! $data) {
            $returnData['message'] = '获取数据失败';
            return $returnData;
        }

        $socialiteData = $data['socialiteData'];
        $unionid = isset($socialiteData['unionid']) ? $socialiteData['unionid'] : null;
        $userData = $data['user'];
        $openid = $socialiteData['openid'];

        $socialites = Socialite::where(function ($query) use ($provider, $openid) {
            return $query->where(function ($q1) use ($provider) {
                if (! in_array($provider, ['qqios', 'qqandroid'])) {
                    return $q1->where('provider', $provider);
                }
            })
            ->where('openid', $openid);
            return $query->where('provider', $provider)
                ->where('openid', $openid);
        })
        ->orWhere(function ($query) use ($unionid) {
            // 微信登录方式校验
            return $query->whereIn('provider', ['wechat', 'weweb', 'wemp', 'weapp'])
                ->where('unionid', $unionid);
        })
        ->get();

        if ($socialites->isEmpty()) {
            // 当前登录方式未登录过
            DB::beginTransaction();
            # 新增用户
            $user = app('user')->store($userData);
            # 新增socialite
            Socialite::create(array_merge($socialiteData, ['user_id' => $user->id]));
            # 绑定微信提现渠道
            $user->withdrawChannels()
                ->updateOrCreate([
                    'type' => $provider
                ], [
                    'type' => $provider,
                    'account_number' => $socialiteData['openid'],
                    'account_name' => $userData['nickname']
                ]);

            DB::commit();

            $returnData['status'] = true;
            $returnData['data'] = $user;
            return $returnData;
        } else {
            if (! (isset($socialites[0]) && $socialites[0]->user)) {
                $returnData['message'] = '网络错误';
                return $returnData;
            }
            // 当前登录方式登录过
            if (in_array($provider, ['wechat', 'weweb', 'wemp', 'weapp'])) {
                // 微信登录过
                $isSocialite = false;
                foreach ($socialites as $socialite) {
                    if ($socialite->provider == $provider) {
                        # 当前微信登录方式登录过
                        $isSocialite = true;
                        break;
                    }
                }
                if (! $isSocialite) {
                    # 当前微信登录方式未登录过 保存登录记录
                    Socialite::create(array_merge($socialiteData, ['user_id' => $socialites[0]->user->id]));
                    # 绑定微信提现渠道
                    $socialites[0]->user->withdrawChannels()
                        ->updateOrCreate([
                            'type' => $provider
                        ], [
                            'type' => $provider,
                            'account_number' => $socialiteData['openid'],
                            'account_name' => $userData['nickname']
                        ]);
                }
            } elseif (in_array($provider, ['qqios', 'qqandroid'])) {
                // qq 登录过
                $isSocialite = false;
                foreach ($socialites as $socialite) {
                    if ($socialite->provider == $provider) {
                        $isSocialite = true;
                        break;
                    }
                }
                if (! $isSocialite) {
                    # 当前qq登录方式未登录过 保存登录记录
                    Socialite::create(array_merge($socialiteData, ['user_id' => $socialites[0]->user->id]));
                }
            }

            $returnData['status'] = true;
            $returnData['data'] = $socialites[0]->user;
            return $returnData;
        }
    }

    /**
     * get store data
     */
    private function getStoreData($provider, $socialite)
    {
        switch ($provider) {
            case 'wechat':
                $return = self::wechat($socialite);
                break;
            case 'weweb':
                $return = self::weweb($socialite);
                break;
            case 'wemp':
                $return = self::wemp($socialite);
                break;
            case 'weapp':
                $return = self::weapp($socialite);
                break;
            case 'qqios':
                $return = self::qqios($socialite);
                break;
            case 'qqandroid':
                $return = self::qqandroid($socialite);
                break;
            case 'weiboapp':
                $return = self::weiboapp($socialite);
                break;

            default:
                $return = false;
                break;
        }

        return $return;
    }

    /**
     * 获取微信开开放平台app数据
     */
    private function wechat($socialite)
    {
        $socialiteData = [
            'provider' => 'wechat',
            'openid' => $socialite->getId(),
            'oauth_token' => $socialite->token->access_token,
        ];
        if ($socialite->token->unionid) {
            $socialiteData['unionid'] = $socialite->token->unionid;
        }
        $user = [
            'nickname' => $socialite->getNickName(),
            'avatar' => $socialite->getAvatar(),
        ];
        return compact('socialiteData', 'user');
    }

    /**
     * 获取微信开放平台web数据
     */
    private function weweb($socialite)
    {
        $socialiteData = [
            'provider' => 'weweb',
            'openid' => $socialite->getId(),
            'oauth_token' => $socialite->token->access_token,
        ];
        if ($socialite->token->unionid) {
            $socialiteData['unionid'] = $socialite->token->unionid;
        }
        $user = [
            'nickname' => $socialite->getNickName(),
            'avatar' => $socialite->getAvatar(),
        ];
        return compact('socialiteData', 'user');
    }

    /**
     * 获取微信公众号数据
     */
    private function wemp($socialite)
    {
        $socialiteData = [
            'provider' => 'wemp',
            'openid' => $socialite->getId(),
            'oauth_token' => $socialite->token->access_token,
        ];
        if ($socialite->token->unionid) {
            $socialiteData['unionid'] = $socialite->token->unionid;
        }
        $user = [
            'nickname' => $socialite->getNickName(),
            'avatar' => $socialite->getAvatar(),
        ];
        return compact('socialiteData', 'user');
    }

    /**
     * 获取微信小程序数据
     */
    private function weapp($socialite)
    {
        $socialiteData = [
            'provider' => 'weapp',
            'openid' => $socialite->getId(),
            'oauth_token' => $socialite->token->access_token,
        ];
        // if ($socialite->token->unionid) {
        //     $socialiteData['unionid'] = $socialite->token->unionid;
        // }
        if ($socialite->fUnionId) {
            $socialiteData['unionid'] = $socialite->fUnionId;
        }
        $user = [
            'nickname' => $socialite->getNickName(),
            'avatar' => $socialite->getAvatar(),
        ];
        return compact('socialiteData', 'user');
    }

    /**
     * 获取qqios数据
     */
    private function qqios($socialite)
    {
        $socialiteData = [
            'provider' => 'qqios',
            'openid' => $socialite->getId(),
            'oauth_token' => $socialite->token->access_token,
        ];
        $user = [
            'nickname' => $socialite->nickname,
            'avatar' => $socialite->avatar,
        ];
        return compact('socialiteData', 'user');
    }

    /**
     * 获取qqios数据
     */
    private function qqandroid($socialite)
    {
        $socialiteData = [
            'provider' => 'qqandroid',
            'openid' => $socialite->getId(),
            'oauth_token' => $socialite->token->access_token,
        ];
        $user = [
            'nickname' => $socialite->nickname,
            'avatar' => $socialite->avatar,
        ];
        return compact('socialiteData', 'user');
    }

    /**
     * 获取微博数据
     */
    private function weiboapp($socialite)
    {
        $socialiteData = [
            'provider' => 'weiboapp',
            'openid' => $socialite->getId(),
            'oauth_token' => $socialite->token->access_token,
        ];
        $user = [
            'nickname' => $socialite['nickname'],
            'avatar' => $socialite['avatar'],
        ];
        return compact('socialiteData', 'user');
    }

    /**
     * 获取小程序支付openid
     */
    public function getWeappOpenid($userId)
    {
        $socialite = Socialite::where('user_id', $userId)
            ->where('provider', 'weapp')
            ->first();
        if ($socialite) {
            return $socialite->openid;
        }
        return false;
    }

    /**
     * 获取是否微信小程序登录过
     */
    public function isWeapp($userId)
    {
        $socialite = Socialite::where('user_id', $userId)
            ->where('provider', 'weapp')
            ->first();
        if ($socialite) {
            return true;
        }
        return false;
    }
}
