<?php

namespace App\Services;

use App\Models\ProductAttribute;
use App\Models\Attribute;

class ProductAttributeService
{
    /**
     * 新增商品属性
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        $hasExists = ProductAttribute::where('product_id', $arr['product_id'])
            ->where('attribute_id', $arr['attribute_id'])
            ->first();
        if (!is_null($hasExists)) {
            return ['status' =>false,'msg'=>'该属性已存在'];
        }
        $save = ProductAttribute::create($arr);
        if ($save) {
            return ['status' => true];
        }
        return ['status'=> false ,'msg' => '添加属性失败'];
    }

    public function index($id)
    {
        $attributeIds = ProductAttribute::where('product_id', $id)
            ->select('attribute_id')
            ->get();
        if ($attributeIds->isEmpty()) {
            return false;
        }
        $attributeIds = $attributeIds->toArray();
        return Attribute::whereIn('id', $attributeIds)
                    ->with('attributeValue')
                    ->get();
    }
}
