<?php

namespace App\Services;

use EcommerceManage\Models\Brand;
use App\Models\BrandSellProduct;
use App\Models\BrandCategory;
use DB;

class BrandService
{
    /**
     * 新增品牌
     * @param  [type] $arr        [description]
     * @param  [type] $categories [description]
     * @return [type]             [description]
     */
    public function store($arr, $categories)
    {
        $exist  = Brand::where('name', $arr['name'])
            ->first();

        if ($exist) {
             return ['status' => 0,'msg' => '新增失败,该品牌已存在'];
        }
        $create = Brand::create($arr);
        if (!$create) {
            return ['status' => 0,'msg' => '新增失败'];
        }

        if (!empty($categories)) {
            DB::beginTransaction();
            foreach ($categories as $key => $value) {
                $brandCate[$key]['category_id'] = $value;
                $brandCate[$key]['brand_id']    = $create->id;
            }
            $insert = BrandCategory::insert($brandCate);
            if ($insert) {
                DB::commit();
                return ['status' => 1,$create->id];
            }
            DB::rollback();
            return false;
        }
        return ['status' => 1];
    }

    /**
     * 编辑
     * @param  [type] $arr        [description]
     * @param  [type] $categories [description]
     * @return [type]             [description]
     */
    public function update($arr, $categories, $id)
    {
        $exist  = Brand::where('name', $arr['name'])
            ->first();

        if ($exist && $id != $exist->id) {
            return ['status' => 0,'msg' => '编辑失败,该品牌已存在'];
        }

        $update = Brand::where('id', $id)
            ->update($arr);

        if (!$update) {
            return ['status' => 0,'msg' => '编辑失败'];
        }

        $categoryIds = BrandCategory::where('brand_id', $id)
            ->pluck('category_id')
            ->toArray();

        if (empty($categories)) {
            $categories = [];
        }
        if (empty($categoryIds)) {
            $categoryIds = [];
        }

        $deleteCategoryId = array_diff($categoryIds, $categories);
        BrandCategory::whereIn('category_id', $deleteCategoryId)
            ->where('brand_id', $id)
            ->where('status', 1)
            ->update(['status' => 0]);
        $insertCategoryId = array_diff($categories, $categoryIds);

        $brandCate = [];
        foreach ($insertCategoryId as $value) {
            $brandCate[] = [
                'category_id' => $value,
                'brand_id'    => $id
            ];
        }

        //原先status 0置为1
        BrandCategory::whereIn('category_id', $categories)
            ->where('brand_id', $id)
            ->where('status', 0)
            ->update(['status' =>1]);
        if ($brandCate) {
            $insert = BrandCategory::insert($brandCate);
            if ($insert) {
                return ['status' => 1];
            }
            return false;
        }
        return ['status' => 1];
    }

    public function detail($id)
    {
         return Brand::where('id', $id)
                     ->with(['brandCategory' => function ($q1) {
                        return $q1->where('status', 1);
                     }])->first();
    }


    // 获取品牌分类表数据
    public function brandCategory($id)
    {
        $data =  Brand::where('id', $id)
            ->with('categorys', 'brandCategory')
            ->first();
        return $data;
    }

    /**
     * 移除商品
     * @param  [type] $brandId   [description]
     * @param  [type] $productId [description]
     * @return [type]            [description]
     */
    public function deleteBrand($brandId, $productId)
    {
        return app('brandProduct')->delete($brandId, $productId);
    }

    /**
     * 批量移除商品
     * @param  [type] $brandId [description]
     * @param  [type] $ids     [description]
     * @return [type]          [description]
     */
    public function deleteBrandProduct($brandId, $ids)
    {
        return app('brandProduct')->deleteAll($brandId, $ids);
    }

    /**
     * 品牌列表
     * @return [type] [description]
     */
    public function index()
    {
        return Brand::orderBy('created_at', 'desc')
            ->paginate(config('amii.apiPaginate'));
    }

    public function get()
    {
        return Brand::orderBy('created_at', 'asc')
            ->get();
    }

    // 品牌列表，不分页
    public function showAll()
    {
        return Brand::orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * 删除
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return Brand::destroy($id);
    }

    /**
     * 通过品牌分类id获取品牌id分类id
     */
    public function getResourceArrByBrandCateId($brandCateId)
    {
        $brandCate = BrandCategory::where('id', $brandCateId)
            ->first();
        if (! $brandCate) {
            return [
                'brand_id' => 0,
                'category_id' => 0
            ];
        }
        return [
            'brand_id' => $brandCate->brand_id,
            'category_id' => $brandCate->category_id
        ];
    }

    /**
    * 搜索商品
    *
     * @param  [type] $key [description]
     * @return [type]      [description]
    */
    public function search($key)
    {
        // 搜索商品详情
        $data = Product::where('status', 1)
            ->where(function ($q1) use ($key) {
                $q1->orWhere('name', 'like', '%'.$key.'%')
                   ->orWhere('snumber', 'like', '%'.$key.'%');
                return $q1;
            })
            ->select('id', 'name', 'snumber', 'main_image', 'product_price')
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.adminPaginate'));
        return $data;
    }
}
