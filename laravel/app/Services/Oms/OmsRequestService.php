<?php

namespace App\Services\Oms;

use GuzzleHttp\Client;

class OmsRequestService
{
    // OMS 系统服务器地址
    private $amiiOmsDomain;

    // oms 版本
    private $version = '1.0';

    // appkey
    private $appkey;

    // token
    private $token;

    // timestamp
    private $timestamp;

    // http client
    private $httpClient;

    // 请求链接
    private $requestUrl;

    public function __construct()
    {
        $this->amiiOmsDomain = config('amii.oms.domain');
        $this->appkey = config('amii.oms.appkey');
        $this->token = config('amii.oms.token');
        $this->timestamp = date('YmdHis', time());
        $this->httpClient = new Client();
    }

    /**
     * 发起请求
     */
    public function request($path = '', array $data = [])
    {
        $this->bulidUrl($path);

        // 配置
        $options = [];

        // 基础数据
        $formParams = self::baseParams();

        // 设置请求数据
        if (! empty($data)) {
            $formParams = array_merge_recursive($data, array_reverse($formParams));
        }
        $options['form_params'] = $formParams;

        $response = $this->httpClient->request(
            'POST',
            $this->requestUrl,
            $options
        );

        return $response;
    }

    /**
     * 请求基础信息处理
     */
    public function baseParams()
    {
        $formParams = [
            'appkey' => $this->appkey,
            'token' => $this->token,
            'timestamp' => $this->timestamp,
            'version' => $this->version
        ];

        // 签名
        $signStr = implode('+', $formParams);
        $sign = md5($signStr);

        $formParams['sign'] = $sign;

        return $formParams;
    }

    /**
     * 拼接请求链接
     */
    private function bulidUrl($path)
    {
        $this->requestUrl = $this->amiiOmsDomain . '/' . $path;
    }

    /**
     * log
     */
    public function omsLog($path, $data = '', $type = 'info')
    {
        // add oms log
        if ($type=='error') {
            \Log::useFiles(storage_path().'/logs/oms_'.date('Y-m-d').'_error.log','error');
            \Log::error('--------OMS--------');
            \Log::info(\Carbon\Carbon::now());
            if ($data) {
                if (is_string($data)) {
                    $data = array($data);
                }
                \Log::error('错误信息: ', $data);
            }
            \Log::error('失败(' . $path . ')');
            \Log::error('------------------------');
        }else{
            \Log::useFiles(storage_path().'/logs/oms_'.date('Y-m-d').'_info.log','info');
            \Log::info('--------OMS--------');
            \Log::info(\Carbon\Carbon::now());
            if ($data) {
                if (is_string($data)) {
                    \Log::info('信息: ' . $data);
                }
                if (is_array($data)) {
                    \Log::info('信息: ', $data);
                }
            }
            \Log::info('成功(' . $path . ')');
            \Log::info('------------------------');
        }
    }
}
