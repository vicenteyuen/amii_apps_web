<?php

namespace App\Services\Oms;

use App\Helpers\AesHelper;
use App\Libraries\Aes\BizDataCrypt;

class AesService
{
    // 公钥
    private $public;
    // key
    private $key;

    public function __construct()
    {
        $this->public = config('amii.aes.public');
        $this->key = config('amii.aes.key');
    }

    /**
     * 生成public key
     */
    public function newPublic()
    {
        return 'amii' . AesHelper::createKey(16);
    }

    /**
     * 生成key
     */
    public function newKey()
    {
        return base64_encode(AesHelper::createKey());
    }


    /**
     * 加密数据
     */
    public function aesEncode(array $data, &$iv, &$encryptedData)
    {
        $key = base64_decode($this->key);
        $iv = AesHelper::createIv(16);
        $amiimark = [
            'timestamp' => time(),
            'public' => $this->public
        ];
        $encryptData = array_merge($data, compact('amiimark'));

        $pc = new BizDataCrypt($this->public, $key);
        $errorCode = $pc->encryptData(json_encode($encryptData), $iv, $encryptedData);

        return $errorCode;
    }

    /**
     * 解密数据
     */
    public function aesDecode(string $encryptedData, $iv)
    {
        $pc = new BizDataCrypt($this->public, $this->key);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);

        if ($errCode != 0) {
            return array($errCode);
        }

        return array(0, json_decode($data, true));
    }
}
