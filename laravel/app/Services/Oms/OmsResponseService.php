<?php

namespace App\Services\Oms;

use Response;

class OmsResponseService
{
    // 请求错误
    const REQUEST_ERROR = 50000;

    // 解析数据错误
    const DATA_ERROR = 50001;

    /**
     * 获取请求返回数据
     */
    public function contents2Data($response)
    {
        $responseDataArr = json_decode($response->getBody()->getContents(), true);

        $vaildateData = $this->vaildation($responseDataArr);

        if ($vaildateData[0] != 0) {
            \Log::error('oms请求返回错误,错误码:' . $vaildateData[0]);
            return array(false, null);
        }
        return array(true, $vaildateData[1]);
    }

    /**
     * 校验请求返回数据
     */
    public function vaildation(array $data)
    {
        try {
            $status = $data['isSuccess'];
            if (! $status) {
                return array(self::REQUEST_ERROR, $data['errorCode']);
            }
            return array(0, $data['data']);
        } catch (\Exception $e) {
            return array(self::DATA_ERROR, null);
        }
    }

    // 返回数据处理
    private static function toString($data)
    {
        if (is_bool($data)) {
            return $data;
        } elseif (is_integer($data) || is_double($data)) {
            return $data.'';
        } elseif (is_string($data)) {
            return $data;
        } elseif (is_null($data)) {
            return;
        } elseif (is_object($data)) {
            if (is_callable([$data, 'toArray'])) {
                return self::toString($data->toArray());
            } elseif (is_callable([$data, 'getAttributes'])) {
                return self::toString($data->getAttributes());
            } elseif (is_callable([$data, '__toString'])) {
                return $data->__toString();
            } elseif (get_class($data) === 'stdClass' && $data === new \stdClass()) {
                return;
            } else {
                return self::toString((array) $data);
            }
        } elseif (is_array($data)) {
            $output = [];
            foreach ($data as $key => $value) {
                $output[$key] = self::toString($value);
            }

            return $output;
        } else {
            return (string) $data;
        }
    }

    public static function success($data = null)
    {
        $success = OmsResponse::success(static::toString($data));

        return Response::json($success->toArray(), 200);
    }

    public static function error($errorMessage = null, $errorCode = null, $data = null)
    {
        $error = OmsResponse::error($errorMessage, $errorCode, static::toString($data));

        return Response::json($error->toArray(), 202);
    }
}

class OmsResponse
{
    const ERROR = 'error';
    const SUCCESS = 'success';

    protected $data;
    protected $status;
    protected $errorCode;
    protected $errorMessage;

    public function __construct($status, array $data = null, $errorMessage = null, $errorCode = null)
    {
        $this->data = $data;
        $this->status = $status;
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    public static function success(array $data = null)
    {
        return new static(static::SUCCESS, $data);
    }

    public static function error($errorMessage, $errorCode = null, array $data = null)
    {
        return new static(static::ERROR, $data, $errorMessage, $errorCode);
    }

    public function toArray()
    {
        $arr = ['status' => $this->status];
        if ($this->status === self::SUCCESS) {
            $arr['data'] = $this->data;
        } elseif ($this->status === self::ERROR) {
            $arr['message'] = (string) $this->errorMessage;
            if (! empty($this->errorCode)) {
                $arr['code'] = $this->errorCode;
            }
            if (! empty($this->data)) {
                $arr['data'] = $this->data;
            }
        }

        return $arr;
    }
}
