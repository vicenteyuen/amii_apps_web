<?php

namespace App\Services\Oms;

use Illuminate\Http\Request;

use App\Models\Order;
use Carbon\Carbon;

class OmsOrderService
{
    /**
     * 订单发货
     */
    public function send(array $data)
    {
        $orderSn = isset($data['orderCode']) ? $data['orderCode'] : '';
        // 只存第一个包裹快递单号
        $shippingSn = isset($data['packageNo']) ? explode(',', $data['packageNo'])[0] : '';
        $com = isset($data['companyCode']) ? $data['companyCode'] : '';

        $arr = [
            'orderSn' => $orderSn,
            'shipping_sn' => $shippingSn,
            'com' => $com
        ];
        // 订单发货
        return app('order')->adminOrderSend($arr);
    }

    /**
     * 订单列表
     */
    public function index(array $data)
    {
        $pageSize = isset($data['pageSize']) ? $data['pageSize'] : '';
        $pageNo = isset($data['pageNo']) ? $data['pageNo'] : '';
        $startTime = isset($data['startTime']) ? $data['startTime'] : '';
        $endTime = isset($data['endTime']) ? $data['endTime'] : '';
        $startPayTime = isset($data['startPayTime']) ? $data['startPayTime'] : '';
        $endPayTime = isset($data['endPayTime']) ? $data['endPayTime'] : '';
        $status = isset($data['status']) ? $data['status'] : '';
        $code = isset($data['code']) ? $data['code'] : '';

        $orders = Order::where(function($query) {
            return $query->confirm()
                ->orWhere(function($q) {
                    return $q->where('provider', 'game')
                        ->whereIn('order_status', [1, 2, 3]);
                });
        });

        if ($code) {
            $orders = $orders->where('order_sn', $code);
        }

        if ($startTime) {
            $orders = $orders->where('order_start_time', '>=', $startTime);
        }

        if ($endTime) {
            $orders = $orders->where('order_end_time', '<=', $endTime);
        }

        if ($startPayTime) {
            $orders = $orders->where('pay_start_time', '>=', $startPayTime);
        }

        if ($endPayTime) {
            $orders = $orders->where('pay_end_time', '<=', $endPayTime);
        }

        if ($status || $status == 0) {
            switch ($status) {
                case '0':
                    $orders = $orders->where(function($query) {
                        return $query->sending()
                            // 待发货订单要返回游戏未完成订单
                            ->orWhere(function ($q) {
                                return $q->where('provider', 'game')
                                    ->where('order_status', 1)
                                    ->where('shipping_status', 0);
                            });
                    });
                    break;
                case '1':
                    $orders = $orders->receiving();
                    break;
                case '2':
                    $orders = $orders->appraising();
                    break;
                case '3':
                    $orders = $orders->where(function($query) {
                        return $query->cancel()
                            ->orWhere(function($q) {
                                return $q->where('provider', 'game')
                                    ->whereIn('order_status', [2, 3]);
                            });
                    });
                    break;

                default:
                    # code...
                    break;
            }
        }

        $orders = $orders->with('orderProducts.sellProduct.product.normalProduct.inventories')
            ->with(['orderProducts.sellProduct'])
            ->with(['payments' => function($q) {
                $q->where('status', 1);
            }])
            ->with('orderProducts.inventory');

        $orders = $orders->paginate($pageSize ? : config('amii.apiPaginate'))
            ->appends(['page' => $pageNo ? : 0]);

        // 返回数据处理
        $returnData = self::returnOrderData($orders->toArray());

        return $returnData;
    }

    /**
     * 订单列表返回数据处理
     */
    private function returnOrderData(array $orders)
    {
        $pageNo = $orders['current_page'];
        $pageSize = $orders['per_page'];
        $totalCount = $orders['total'];
        $hasNextPage = $orders['next_page_url'] ? true  : false;
        $totalPageCount = $orders['last_page'];
        $nextPage = $orders['next_page_url'] ? $orders['current_page'] + 1 : 0;
        $hasPrePage = $orders['prev_page_url'] ? true : false;
        $prePage = $orders['prev_page_url'] ? $orders['current_page'] - 1 : 0;
        $start = $orders['from'];

        $list = [];
        foreach ($orders['data'] as $order) {
            $isPoint = false;
            $total = 0;
            if ($order['provider'] == 'point') {
                $isPoint = true;
            }
            // 订单商品数据
            $orderProducts = [];
            foreach ($order['order_products'] as $orderProduct) {
                $tmpOrderProduct = [
                    'name' => $orderProduct['sell_product']['product']['name'],
                    'sku_code' => $orderProduct['inventory']['sku_code'],
                    'sku_id' => $orderProduct['inventory']['sku_code'],
                    'goods_code' => $orderProduct['sell_product']['product']['snumber'],
                    'goods_id' => $orderProduct['sell_product']['product']['id'],
                    'unit' => $orderProduct['sell_product']['product']['unit'] ? : '件',
                    'quantity' => $orderProduct['number'],
                    'price' => $orderProduct['inventory']['mark_price'],
                    'weight' => '',
                    'brand' => ''
                ];
                if ($isPoint) {
                    $tmpOrderProduct['price'] = self::calcPointSkuPrice($orderProduct['sell_product']['product']['normal_product'], $tmpOrderProduct['sku_code'], $tmpOrderProduct['price']);
                    $total += $tmpOrderProduct['price'] * $tmpOrderProduct['quantity'];
                }
                $orderProducts[] = $tmpOrderProduct;
            }
            $hasPay = empty($order['payments']) ? 0 : 1;
            $tmpOrder = [
                'code' => $order['order_sn'],
                'type' => self::orderType($order['provider']),
                'send_status' => self::sendStatus($order),
                'createTime' => Carbon::parse($order['created_at'])->timestamp,
                'payTime' => Carbon::parse($order['pay_start_time'])->timestamp,
                'payWay' => $hasPay ? self::payWay($order['payments'][0]['provider']) : 1,
                'total' => $order['order_fee'],
                'discount' => $order['discount'],
                'consigneeName' => $order['shipping_address']['name'] ? : '',
                'consigneeMobile' => $order['shipping_address']['phone'] ? : '',
                'postcode' => $order['shipping_address']['post_code'] ? : '',
                'province' => $order['shipping_address']['province'] ? : '',
                'city' => $order['shipping_address']['city'] ? : '',
                'county' => $order['shipping_address']['area'] ? : '',
                'town' => $order['shipping_address']['detail'] ? : '',
                'address' => $order['shipping_address']['address'] ? : '',
                'realname' => $order['shipping_address']['name'] ? : '',
                'receiptTitle' => $order['invoice'] ? : '',
                'source' => 'APP',
                'remark' => $order['remark'],
                'freight' => $order['shipping_fee'],
                'status' => self::orderStatus($order),
                'itemList' => $orderProducts
            ];
            if ($isPoint) {
                $tmpOrder['total'] = '0.00';
                $tmpOrder['discount'] = number_format($total, 2, '.', '');
            }
            $list[] = $tmpOrder;
        }

        return compact('pageNo', 'pageSize', 'totalCount', 'list', 'hasNextPage', 'totalPageCount', 'nextPage', 'hasPrePage', 'prePage', 'start');
    }

    /**
     * 订单支付方式处理
     */
    private function payWay($payType)
    {
        switch ($payType) {
            case 'weapp':
            case 'wemp':
            case 'weweb':
            case 'wechat':
                $payWay = 2;
                break;

            default:
                $payWay = 1;
                break;
        }
        return $payWay;
    }

    /**
     * 订单状态处理
     * @param array $order
     */
    private function orderStatus(array $order)
    {
        if ($order['provider'] == 'game' && $order['pay_status'] == 0) {
            if ($order['order_status'] == 1) {
                return 4;
            }
            return 0;
        }
        /**
         * "status": 订单状态（tinyint(3)）, 新订单-1,待付款-2,拣货中-3,待发货-4,待收货-5,已收货-6,已取消-0,退款完成-100
         */
        switch ($order['order_status_str']) {
            case 'paying':
                $status = 2;
                break;
            case 'sending':
                $status = 4;
                break;
            case 'receiving':
                $status = 5;
                break;
            case 'appraising':
                $status = 6;
                break;
            case 'close':
                $status = 0;
                break;

            default:
                $status = 1000;
                break;
        }
        return $status;
    }

    /**
     * 获取订单类型
     */
    private function orderType($provider = '')
    {
        /**
         * type订单类型 1普通订单 2积分订单 3游戏订单
         */
        switch ($provider) {
            case 'standard':
                $type = 1;
                break;
            case 'point':
                $type = 2;
                break;
            case 'game':
                $type = 3;
                break;

            default:
                $type = 0;
                break;
        }

        return $type;
    }

    /**
     * 计算积分商品价格
     */
    private function calcPointSkuPrice($sellProduct, $skuCode, $price)
    {
        $basePrice = number_format($price/200, 2, '.', '');
        if (! $sellProduct) {
            return $basePrice;
        }

        foreach ($sellProduct['inventories'] as $inventory) {
            if ($inventory['sku_code'] == $skuCode) {
                return number_format($inventory['mark_price'], 2, '.', '');
            }
        }
        return $basePrice;
    }

    /**
     * 获取订单可发货状态
     */
    private function sendStatus(array $order)
    {
        $sendStatus = true;
        if ($order['provider'] == 'game' && $order['pay_status'] == 0) {
            $sendStatus = false;
        }
        return $sendStatus;
    }
}
