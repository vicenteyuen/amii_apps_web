<?php

namespace App\Services\Oms;

use App\Models\SellProduct;
use Carbon\Carbon;

class OmsProductService
{
    /**
     * 商品列表
     */
    public function index(array $data)
    {
        $productId = isset($data['goods_id']) ? $data['goods_id'] : '';
        $productSn = isset($data['goods_code']) ? $data['goods_code'] : '';
        $productStatus = isset($data['goods_status']) ? $data['goods_status'] : '';
        $cstime = isset($data['cstime']) ? $data['cstime'] : '';
        $cetime = isset($data['cetime']) ? $data['cetime'] : '';
        $page = isset($data['pageNo']) ? $data['pageNo'] : 0;
        $pageSize = isset($data['page_size']) ? $data['page_size'] : '';

        $products = new SellProduct;

        if ($productId) {
            $products = $products->whereHas('omsProduct', function ($q) use ($productId) {
                return $q->where('id', $productId);
            });
        }

        if ($productSn) {
            $products = $products->whereHas('omsProduct', function ($q) use ($productSn) {
                return $q->where('snumber', $productSn);
            });
        }

        $products = $products->with('omsProduct')
            ->with('inventories')
            ->paginate($pageSize ? : config('amii.apiPaginate'))
            ->appends(['page' => $page]);

        // 返回数据处理
        $returnData = self::returnProductData($products->toArray());

        return $returnData;
    }

    /**
     * 商品数据处理
     */
    public function returnProductData(array $products)
    {
        $pageNo = $products['current_page'];
        $pageSize = $products['per_page'];
        $totalCount = $products['total'];
        $hasNextPage = $products['next_page_url'] ? true  : false;
        $totalPageCount = $products['last_page'];
        $nextPage = $products['next_page_url'] ? $products['current_page'] + 1 : 0;
        $hasPrePage = $products['prev_page_url'] ? true : false;
        $prePage = $products['prev_page_url'] ? $products['current_page'] - 1 : 0;
        $start = $products['from'];

        $list = [];
        foreach ($products['data'] as $product) {
            $tmp = [];
            foreach ($product['inventories'] as $inventory) {
                $tmp[] = [
                    'sku_id' => $inventory['id'],
                    'sku_code' => $inventory['sku_code'],
                    'sku_price' => $inventory['mark_price'],
                    'sku_stock' => $inventory['number']
                ];
            }
            $list[] = [
                'goods_id' => $product['id'],
                'goods_title' => $product['oms_product']['name'],
                'goods_code' => $product['oms_product']['snumber'],
                'goods_status' => $product['oms_product']['status'],
                'add_time' => Carbon::parse($product['oms_product']['created_at'])->timestamp,
                'update_time' => Carbon::parse($product['oms_product']['updated_at'])->timestamp,
                'good_url' => url('/wechat/mall/product/' . $product['product']['id']),
                'goods_img' => $product['oms_product']['main_image'],
                'skus' => $tmp
            ];
        }

        return compact('pageNo', 'pageSize', 'totalCount', 'list', 'hasNextPage', 'totalPageCount', 'nextPage', 'hasPrePage', 'prePage', 'start');
    }
}
