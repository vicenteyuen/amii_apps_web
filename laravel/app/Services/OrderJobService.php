<?php

namespace App\Services;

use DB;
use App\Helpers\AmiiHelper;
use App\Models\Order;
use App\Models\SellProduct;
use Ecommerce\Models\Inventory;

class OrderJobService
{
    /**
     * 订单未确认退回库存
     */
    public function unconfirm($orderSn)
    {
        DB::beginTransaction();

        $order = Order::where('order_sn', $orderSn)
            ->unconfirm()
            ->first();
        $logStr = '订单号: ' . $orderSn . '的';
        if (! $order) {
            \Log::info($logStr . '订单已确认或不存在');
            // 当前订单已确认过
            DB::commit();
            return true;
        }
        \Log::info($logStr . '订单失效退回库存');
        self::rebackInventories($order);

        // 订单设为已失效
        if (! $order->update(['order_status' => 3])) {
            DB::rollback();
        }

        DB::commit();
        return true;
    }

    /**
     * 订单未支付退回库存
     */
    public function cancel($orderSn)
    {
        DB::beginTransaction();

        $order = Order::where('order_sn', $orderSn)
            ->paying()
            ->first();
        $logStr = '订单号: ' . $orderSn . '的';
        if (! $order) {
            \Log::info($logStr . '订单已支付');
            // 当前订单已支付过
            DB::commit();
            return true;
        }
        \Log::info($logStr . '订单未支付退回库存');
        self::rebackInventories($order);

        // 退回订单使用优惠
        self::rebackDiscount($order);

        // 订单设为已关闭
        if (! $order->update(['order_status' => 2])) {
            DB::rollback();
            return false;
        }

        DB::commit();
        return true;
    }

    /**
     * 退回订单库存
     */
    private function rebackInventories($order)
    {
        $orderProducts = $order->orderProducts;

        // 库存id对应数量
        $inventoryNumber = $orderProducts->pluck('number', 'inventory_id')
            ->toArray();

        // 售卖商品id对应数量
        $sellProductsNumber = $orderProducts->pluck('number', 'sell_product_id')
            ->toArray();

        // 获取需要更新的库存条目以及当前数量
        $inventoriesQuery = Inventory::whereIn('id', array_keys($inventoryNumber));
        $inventories = (clone $inventoriesQuery)
            ->get();
        $inventoriesUpdateNumber = array_map(function ($key, $number) use ($inventories) {
            foreach ($inventories as $inventory) {
                if ($inventory->id == $key) {
                    return [
                        'key' => $key,
                        'value' => $inventory->number + intval($number)
                    ];
                }
            }
        }, array_keys($inventoryNumber), $inventoryNumber);

        // 获取需要更新的售卖商品条目以及当前数量
        $sellProductsQuery = SellProduct::whereIn('id', array_keys($sellProductsNumber));
        $sellProducts = (clone $sellProductsQuery)
            ->get();
        $sellProductsUpdateNumber = array_map(function ($key, $number) use ($sellProducts) {
            foreach ($sellProducts as $sellProduct) {
                if ($sellProduct->id == $key) {
                    return [
                        'key' => $key,
                        'value' => $sellProduct->number + intval($number)
                    ];
                }
            }
        }, array_keys($sellProductsNumber), $sellProductsNumber);

        AmiiHelper::updateColumnDiffValues($inventoriesQuery, 'number', 'id', $inventoriesUpdateNumber);
        AmiiHelper::updateColumnDiffValues($sellProductsQuery, 'number', 'id', $sellProductsUpdateNumber);

        // 退回首单
        app('amii.give.product')->delete($order->user_id);
    }

    /**
     * 退回订单使用优惠
     */
    public function rebackDiscount($order)
    {
        if (! $order) {
            return false;
        }

        if ((int)$order->point_discount) {
            self::rebackPoint($order);
        }

        if ($order->balance_discount) {
            self::rebackBalance($order);
        }

        if ($order->coupon_discount) {
            self::rebackCoupon($order);
        }
    }

    /**
     * 退回积分
     */
    private function rebackPoint($order)
    {
        app('user')->rebackPoint($order->user_id, $order->point_discount * 100);
    }

    /**
     * 退回余额
     */
    private function rebackBalance($order)
    {
        app('user')->rebackBalance($order->user_id, $order->balance_discount);
    }

    /**
     * 退回优惠券
     */
    private function rebackCoupon($order)
    {
        app('amii.manger.user.coupon')->rebackCoupon($order->user_id, $order->coupon_code);
    }
}
