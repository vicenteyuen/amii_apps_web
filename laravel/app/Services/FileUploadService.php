<?php

namespace App\Services;

use Closure;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\FilesystemAdapter;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadService
{
    protected $filesystemAdapter;

    public function upload($filePath, UploadedFile $file, $disk)
    {
        if ($file) {
            $this->filesystemAdapter = Storage::disk($disk);
            if (! $this->filesystemAdapter instanceof FilesystemAdapter) {
                throw new \Exception('File manager has no file system adapter no set!');
            }

            $extension = $file->getClientOriginalExtension();
            $hash = $this->generate($this->filesystemAdapter, $extension);
            $fileName = $hash . '.' . $extension;
            $path = '/' . $fileName;

            $this->filesystemAdapter->writeStream($path, fopen($filePath, 'r+'));

            return $fileName;
        }

    }

    public function generate(FilesystemAdapter $filesystemAdapter, $extension, $path = null)
    {
        $hash = sha1(time() + microtime());

        $exists = $filesystemAdapter->exists($path . '/' . $hash . '.' . $extension);

        if ($exists) {
            $this->generate($filesystemAdapter, $extension, $path);
        }

        return $hash;
    }
}