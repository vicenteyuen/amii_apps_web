<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\ExpressModels\Express;
use App\Models\Order;
use App\Models\SellProduct;
use App\Models\ExpressModels\ExpressLog;
use App\Libraries\Kuaidi100\Kuaidi100;

class ExpressService
{
    /**
     * 获取可用快递
     */
    public function orderExpress()
    {
        return Express::useful()
            ->orderBy('orderby', 'desc')
            ->get();
    }

    // 列表
    public function index()
    {
        return Express::get();
    }

    //新增快递公司
    public function add($arr)
    {
        $express = Express::where('com', $arr['com'])
            ->first();
        if ($express) {
            return false;
        }
        return Express::create($arr);
    }

    //update
    public function update($id, $arr)
    {
        return Express::where('id', $id)
            ->update($arr);
    }


    /**
     * 获取物流信息
     */
    public function getExpress($orderSn)
    {
        $order = Order::where('order_sn', $orderSn)
            ->with('packages')
            ->first();

        if (! $order || $order->packages->isEmpty()) {
            return [];
        }

        $package = $order->packages[0];
        $expressCompany = Express::where('com', $package->com)
            ->first();
        if (!$expressCompany) {
            return [];
        }

        // 物流信息记录
        $express = $package->express;

        if ($order->orderProducts->isEmpty()) {
            return [];
        }
        $sellProductId = $order->orderProducts[0]->sell_product_id;
        $sellProduct = SellProduct::find($sellProductId);
        if (! ($express && $express->is_poll)) {
            // 订单不支持订阅 只能查询重新保存
            $expressCom = self::expressCom();
            $expressData = self::poll($expressCom[$expressCompany->com], $package->shipping_sn);
            self::store($package->id, $expressCompany->com, $package->shipping_sn, $expressData);
            $expressLog =  ExpressLog::where('order_package_id', $package->id)
                ->first();

            $expressLog->image = $sellProduct->all_image[0];
            return $expressLog;
        }
        $express->image = $sellProduct->all_image[0];
        return $express;
    }

    /**
     * 物流信息存储
     */
    public function store($orderPackageId, $com, $nu, array $data, $isPoll = 0)
    {
        $express = ExpressLog::where('order_package_id', $orderPackageId)
            ->first();

        $info = [
            'com' => Express::where('com', $com)
                ->orWhere('id', $com)
                ->value('name'),
            'nu' => $nu,
        ];
        if (! $express) {
            $data = isset($data['data'])? $data['data'] : [];
            $expressInfo = array_push($data, [
                'time' => Carbon::now()->toDateTimeString(),
                'ftime' => Carbon::now()->toDateTimeString(),
                'context' => '订单已发货',
            ]);

            $info['data'] = $data;

            return ExpressLog::create([
                'order_package_id' => $orderPackageId,
                'info' => $info,
                'is_poll' => $isPoll
            ]);
        }

        $data = isset($data['data'])?$data['data'] : [];
        $expressInfo = array_push($data, [
                'time' =>  $express->created_at->toDateTimeString(),
                'ftime' =>  $express->created_at->toDateTimeString(),
                'context' => '订单已发货',
            ]);

        $info['data'] = $data;
        return $express->update([
            'info' => $info
        ]);
    }

    /**
     * 物流信息查询
     */
    private function poll($com, $nu)
    {
        return app('kuaidi100')->express($com, $nu);
    }

    public function expressCom()
    {
        return [
                "ZT" => "",
                "GDEMS" => "ems",
                "ZTO" => "zhongtong",
                "ZJS" => "zhaijisong",
                "YUNDA" => "yunda",
                "YTO" => "yuantong",
                "uc" => "none",
                "TT" => "tiantian",
                "suer" => "suer",
                "STO" => "shentong",
                "STKD" => "none",
                "SF" => "shunfeng",
                "RFD" => "rufengda",
                "QT" => "none",
                "QF" => "quanfengkuaidi",
                "POSTB" => "youzhengguonei",
                "KJ" => "kuaijiesudi",
                "JD" => "jd",
                "HTKY" => "baishiwuliu",
                "GTKD" => "guotongkuaidi",
                "EMS_ZX_ZX_US" => "none",
                "EMS" => "ems",
                "deppon" => "debangwuliu",
                "DDCOD" => "none",
                "chinapost" => "youzhengguonei",
                "BYP" => "none",
                "BESTEX" => "huitongkuaidi",
            ];
    }
}
