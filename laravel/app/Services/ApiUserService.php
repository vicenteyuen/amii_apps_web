<?php

namespace App\Services;

use App\Models\Token;

class ApiUserService
{
    public static function user($token)
    {
        if (! $token) {
            return false;
        }

        $tokenModel = Token::where('code', $token)
            ->first();

        if ($tokenModel && $tokenModel->isExpires) {
            return $tokenModel->user;
        }
        return false;
    }
}
