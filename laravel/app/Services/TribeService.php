<?php

namespace App\Services;

use App\Models\Tribe;
use App\Models\Gain;
use App\Models\User;
use App\Models\Balance;
use DB;

class TribeService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        return Tribe::create($arr);
    }


    /**
     * 部落首页
     * @return [type] [description]
     */
    public function show($user_id)
    {
        $data['weekRank']       = $this->rank('week');
        $data['monthRank']      = $this->rank('month');
        $data['cumulateRank']   = $this->rank('total');
        $user = Tribe::where('user_id', $user_id)
            ->with('user.level')
            ->first();
        $data['user'] = $user;
        // $this->currentRank('month');
        // $this->currentRank('week');
        // $this->cumulateRank();
        return $data;
    }

    /**
     * 收益明细
     * @return [type] [description]
     */
    public function detail($user_id)
    {
        return Gain::where('p_user_id', $user_id)
            ->with('user.level')
            ->whereHas('balance', function ($q1) {
                return $q1->where('status', 1);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.apiPaginate'));
    }

    /**
     * 排名调用方法
     * @param  array $arr 区间
     * @return [type]      [description]
     */
    public function rank($type)
    {
        \App\Models\User::$withoutAppends = true;
        switch ($type) {
            case 'week':
                // 本周排名
                return Tribe::orderBy('week_rank', 'asc')
                    ->where('week_rank', '>', 0)
                    ->with(['user' => function ($q1) {
                        $q1->select('id', 'nickname', 'avatar', 'phone');
                    }])
                    ->offset(0)
                    ->limit(20)
                    ->get();
            break;

            case 'month':
                // 本月排名
                return Tribe::orderBy('month_rank', 'asc')
                    ->where('month_rank', '>', 0)
                    ->with(['user' => function ($q1) {
                        $q1->select('id', 'nickname', 'avatar', 'phone');
                    }])
                    ->offset(0)
                    ->limit(20)
                    ->get();
            break;

            case 'total':
                // 累积排名
                return Tribe::orderBy('rank', 'asc')
                    ->where('rank', '>', 0)
                    ->with(['user' => function ($q1) {
                        $q1->select('id', 'nickname', 'avatar', 'phone');
                    }])
                    ->offset(0)
                    ->limit(20)
                    ->get();
            break;

            default:
                return null;
            break;
        }

    }

    /**
     * 上周时间范围
     * @return array [description]
     */
    public function lastWeekAmong()
    {
        $date          = date('Y-m-d');                                                             //当前日期
        $first         = 0;                                                                         //每周日为开始日期
        $days          = date('w', strtotime($date));                                               //获取当前周的第几天 周日是0
        $ThisweekStart = date('Y-m-d', strtotime("$date -".($days ? $days - $first : 6).' days'));  //获取本周开始日期
        $lastWeekStart = strtotime("$ThisweekStart - 7 days");                                      //上周开始日期
        $lastWeekEnd   = strtotime($ThisweekStart) - 1;                                             //上周结束日期
        return [$lastWeekStart,$lastWeekEnd];
    }

    /**
     * 本周时间范围
     * @return [type] [description]
     */
    public function weekAmong()
    {
        $date          = date('Y-m-d');                                                             //当前日期
        $first         = 0;                                                                         //每周日为开始日期
        $days          = date('w', strtotime($date));                                               //获取当前周的第几天 周日是0
        $ThisWeekStart = strtotime("$date -".($days ? $days - $first : 6).' days');                 //获取本周开始日期
        $ThisWeekEnd   = $ThisWeekStart + 7 * 24 * 3600 -1 ;
        return [$ThisWeekStart,$ThisWeekEnd];
    }


    /**
     * 上个月范围
     * @return array [description]
     */
    public function lastMonthAmong()
    {
        $ThisMonthStart =  date('Y-m-01', time());                            // 本月第一天
        $lastMonthStart =  mktime(0, 0, 0, date("m")-1, 1, date("Y"));        //上月开始日期
        $lastMonthEnd   =  strtotime($ThisMonthStart)- 1;                     //上月结束日期
        return [$lastMonthStart,$lastMonthEnd];
    }

    /**
     * 本月时间范围
     * @return [type] [description]
     */
    public function monthAmong()
    {
         $ThisMonthStart =  strtotime(date('Y-m-01', time()));                             // 本月第一天
         $lastMonthStart =  mktime(0, 0, 0, date("m")+1, 1, date("Y"))-1;       // 本月最后一天
         return [$ThisMonthStart,$lastMonthStart];
    }


    /**
     * 上周排名,上月排名
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function lastRank($type)
    {
        if (strtolower($type) == 'week') {
            //上周排名
            $limits = $this->lastWeekAmong();
            $option = 'last_week_rank';
        } else {
            // 上月排名
            $limits = $this->lastMonthAmong();
            $option = 'last_month_rank';
        }
        $gainRanks = self::getReceivedRankBy20($limits);
        DB::beginTransaction();
        foreach ($gainRanks as $k => $v) {
            dispatch(new \App\Jobs\TribeLastRank($option, $v));
        }
        DB::commit();
        return true;
    }


    /**
     * 本周排名,本月排名
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function currentRank($type)
    {
        if (strtolower($type) == 'week') {
            //本周排名
            $limits = $this->weekAmong();
            $rank   = 'week_rank';
            $income = 'week_income';
        } else {
            //本月排名
            $limits = $this->monthAmong();
            $rank   = 'month_rank';
            $income = 'month_income';
        }
        $gainRanks = self::getReceivedRankBy20($limits);
        // 重置周排名,月排名
        Tribe::where($rank, '!=', 0)
            ->orWhere($income, '!=', 0)
            ->update([$rank => 0,$income => 0]);
        DB::beginTransaction();
        foreach ($gainRanks as $k => $v) {
            dispatch(new \App\Jobs\TribeRank($rank, $income, $v));
        }
        DB::commit();
        return true;
    }

    /**
     * 积分总排名
     * @return [type] [description]
     */
    public function cumulateRank()
    {
        $rank = 'rank';
        $income = 'income';
        $gainRanks = self::getCumulateReceivedRankBy20();
        // 重置年排名
        Tribe::where($rank, '!=', 0)
            ->orWhere($income, '!=', 0)
            ->update([$rank => 0,$income => 0]);
        DB::beginTransaction();
        foreach ($gainRanks as $k => $v) {
            dispatch(new \App\Jobs\TribeRank($rank, $income, $v));
        }
        DB::commit();
        return true;

    }


    //只更新前20条的排名
    public function getRankBy20($limits)
    {
        return DB::select("
            select s.*,@rank := @rank + 1 as rank from (
                select user_id,p_user_id,sum(gains.income) as total_profile from gains join users
                on gains.p_user_id = users.id
                where unix_timestamp(gains.created_at) between $limits[0] and $limits[1]
                and users.phone <> ''
                group by p_user_id
            ) s , (select @rank := 0) init
                order by total_profile desc
                limit 20
        ");
    }

    //已到账只更新前20条的排名
    public function getReceivedRankBy20($limits)
    {
        return DB::select("
            select s.*,@rank := @rank + 1 as rank from (
                select user_id,sum(balance.value) as total_profile from balance join users
                on balance.user_id = users.id
                where unix_timestamp(balance.created_at) between $limits[0] and $limits[1]
                and balance.type = 1  and balance.status = 1
                group by user_id
            ) s , (select @rank := 0) init
                order by total_profile desc
                limit 20
        ");
    }

     //已到账只更新前20条的排名累积排名
    public function getCumulateReceivedRankBy20()
    {
        return DB::select("
            select s.*,@rank := @rank + 1 as rank from (
                select user_id,sum(balance.value) as total_profile from balance join users
                on balance.user_id = users.id
                where balance.type = 1  and balance.status = 1
                group by user_id
            ) s , (select @rank := 0) init
                order by total_profile desc
                limit 20
        ");
    }


    /**
     *  一级部落贡献值排名(调用sql)
     * @param  [type] $limits 限制条件
     * @param  [type] $id     父级id
     * @return [type]         [description]
     */
    public function getContributeRand($pid)
    {
        if (!$pid) {
            return false;
        }
        return DB::select("
                select s.*,@rank := @rank + 1 as contributeRank from (
                    select * from tribes
                    where pid = $pid
                    order by contribute_income desc
                ) s , (select @rank := 0) init     
        ");
    }

    /**
     *  二级部落贡献值排名(调用sql)
     * @param  [type] $limits 限制条件
     * @param  [type] $id     父级id
     * @return [type]         [description]
     */
    public function getSecondContributeRand(array $pid)
    {
        if (empty($pid)) {
            return false;
        }
        $pid = implode(',', $pid);
        return DB::select("
                select s.*,@rank := @rank + 1 as contributeRank from (
                    select * from tribes
                    where pid in ($pid)
                    order by contribute_income desc
                ) s , (select @rank := 0) init     
        ");
    }



    /**
     * 一级部落贡献值排名(队列调用)
     * @param  [type] $limits [description]
     * @param  [type] $id     [description]
     * @return [type]         [description]
     */
    public function contributeRankQueue($pid, $type, $level)
    {
        switch ($level) {
            // 一级部落调用sql
            case 'first':
                $contribeRank = $this->getContributeRand($pid);
                break;
            // 二级部落调用
            case 'second':
                $contribeRank = $this->getSecondContributeRand($pid);
                break;
            default:
                \Log::error('部落排名出错');
                return ;
        }
        if (empty($contribeRank)) {
            return false;
        }
        foreach ($contribeRank as $k => $v) {
            if (intval($v->contribute_income) == 0) {
                break;
            }
            if ($type == 'now') {
                // 当前排名
                Tribe::where('id', $v->id)
                    ->update(['user_current_rank' => $v->contributeRank]);
            } else {
                // 上次排名
                Tribe::where('id', $v->id)
                    ->update(['user_last_rank' => $v->contributeRank]);
            }
        }
    }

    /**
     * 一级部落排名
     * @param  int $pid [description]
     * @return [type]     [description]
     */
    public function contributeRank($pid)
    {
        dispatch(new \App\Jobs\ContributeIncomeRank($pid, 'now', 'first'));
    }

    /**
     * 二级部落排名
     * @param  array $pid [description]
     * @return [type]      [description]
     */
    public function SecondcontributeRank($pid)
    {
        dispatch(new \App\Jobs\ContributeIncomeRank($pid, 'now', 'second'));
    }

    public function updatelastUserRank()
    {
        DB::statement('update tribes set user_last_rank = user_current_rank where user_current_rank > 0');
    }


    /**
     * 队列本周本月排名
     * @return [type] [description]
     */
    function rankQueue($rank, $income, $gainObject)
    {
        if (!$gainObject) {
            return false;
        }
        $arr[$rank]    = $gainObject->rank;
        $arr[$income]  = $gainObject->total_profile;
        return Tribe::where('user_id', $gainObject->user_id)
            ->update($arr);
    }

    /**
     * 队列上周上月排名
     * @return [type] [description]
     */
    function lastRankQueue($option, $gainObject)
    {
        if (!$gainObject) {
            return false;
        }
        $arr[$option] = $gainObject->rank;
        return  Tribe::where('user_id', $gainObject->user_id)
            ->update($arr);
    }

    /**
     * 部落详情
     * @param  [type] $user_id [description]
     * @return [type]          [description]
     */
    public function index($provider, $id)
    {
        $tribeId = Tribe::where('user_id', $id)
            ->value('id');

        $tribe = Tribe::where('pid', $tribeId)
            ->get();

        if ($provider == 'first') {
            // 一级部落
            $this->contributeRank($tribeId); //排名调用队列
            $data =  Tribe::where('pid', $tribeId)
                ->with('user.level')
                ->orderByRaw('user_current_rank = 0 ,user_current_rank')
                ->paginate(config('amii.apiPaginate'));
        } else if ($provider == "secode") {
            // 二级部落
             $this->SecondcontributeRank($tribe->pluck('id')->toArray());
             $data = Tribe::whereIn('pid', $tribe->pluck('id'))
                ->with('user.level')
                ->select('*')
                ->orderByRaw('user_current_rank = 0 ,user_current_rank')
                ->paginate(config('amii.apiPaginate'));
        } else {
            return false;
        }

        $datas = $data->toArray();
        $tribeChilde = Tribe::whereIn('pid', $tribe->pluck('id'))->get();
        $datas['all_rank1'] = count($tribe); //一级部落总人数
        $datas['all_rank2'] = count($tribeChilde); //二级部落总人数

        if ($provider == 'first') {
            $childs = Tribe::whereIn('pid', $tribe->pluck('id'))
            ->get();
            foreach ($datas['data'] as $k1 => &$v1) {
                $v1['number'] = 0;
                foreach ($childs as $k2 => $v2) {
                    if ($v2['pid'] == $v1['id']) {
                        $v1['number']++;
                    }
                }
            }
        }
        return $datas;

    }

    // 返回部落上级
    public function parentTribe($userId)
    {
        $pid = Tribe::where('user_id', $userId)
            ->value('pid');
        if (!$pid) {
            return false;
        }
        return Tribe::where('id', $pid)
            ->with('user')
            ->first();
    }

    public function profit($id)
    {
        return Balance::where('user_id', $id)
            ->where('type', 1)
            ->where('status', 1)
            ->sum('value');
    }
}
