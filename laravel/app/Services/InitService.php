<?php

namespace App\Services;

class InitService
{
    /**
     * 获取积分商品顶部背景图
     * @return string url
     */
    public function getPointTopBackgroundImg()
    {
        $url = url('/') . '/static/init/point_top_background.png';

        return $url;
    }

    /**
     * 获取部落顶部背景图
     * @return string url
     */
    public function getTribeTopBackgroundImg()
    {
        $url = url('/') . '/static/init/tribe_top_background.png';

        return $url;
    }

    /**
     * 获取资产顶部背景图
     * @return string url
     */
    public function getAssetTopBackgroundImg()
    {
        $url = url('/') . '/static/init/asset_top_background.png';

        return $url;
    }
}
