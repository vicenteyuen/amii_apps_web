<?php

namespace App\Services;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Helpers\OrderJobHelper;

class PointOrderService
{
    /**
     * 提交兑换
     */
    public function store($userId, Request $request)
    {
        // 检查是否是禁用户
        $checkUser = app('user')->checkUser(auth()->id());
        if (!$checkUser) {
            return app('jsend')->error('经系统检测，您邀请好友注册获赠积分中存在操作异常情况。本平台根据用户注册协议现暂时冻结您的积分使用权');
        }
        $validator = Validator::make($request->all(), [
            'inventoryId' => 'required|integer',
            'number' => 'integer'
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }

        DB::beginTransaction();
        // 库存校验并处理
        $checkPointInventories = app('order')->inventoryCheck([
            [
                'id' => intval($request->inventoryId),
                'number' => intval($request->number ? : 1),
            ]
        ]);
        if (! $checkPointInventories) {
            DB::rollBack();
            return app('jsend')->error('库存不足,提交订单失败');
        }

        // 生成订单
        $createOrder = app('order')->createOrder($userId, $checkPointInventories, 'point');
        if (! $createOrder) {
            DB::rollBack();
            return app('jsend')->error('提交订单失败');
        }

        // 添加订单未确认退回库存任务
        OrderJobHelper::unconfirm($createOrder);

        DB::commit();
        return app('jsend')->success(['orderSn' => $createOrder]);
    }

    /**
     * 获取兑换单所需积分
     */
    public function orderNeedPoint($order)
    {
        $needPoint = 0;
        foreach ($order->orderProducts as $orderProduct) {
            $needPoint += $orderProduct->inventory->mark_price * $orderProduct->number;
        }
        return $needPoint;
    }
}
