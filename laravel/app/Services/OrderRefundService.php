<?php

namespace App\Services;

use App\Helpers\ImageHelper;
use App\Helpers\ReturnProfitHelper;
use App\Models\OrderProduct;
use DB;
use App\Models\OrderProductRefund;

class OrderRefundService
{
    /**
     * 新增售后记录
     * @param OrderProduct $product
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|boolean
     */
    public function store($product, array $data)
    {
        $refund = $product->refund;
        $refund_data = array_filter(array_only($data, [
            'provider',
            'express_fee',
            'pay',
            'applied_data',
        ]));
        if (!empty($refund_data)) {
            if ($refund) {
                if ($refund->status == 2) {
                    $refund_data['status'] = 0;
                    $refund_data['refund_status'] = 0;
                }
                $refund->update($refund_data);
            } else {
                $refund = $product->refund()->create($refund_data);
            }
        }
        if (!$refund) {
            return false;
        }

        $log = $refund->logs()->create(array_only($data, [
            'title',
            'content',
            'is_user_record',
        ]));
        if (!empty($data['images'])) {
            $log->images()->createMany($data['images']);
        }

        return $refund;
    }

    /**
     * 获取订单售后记录
     * @param $order_product_id
     * @return \Illuminate\Database\Eloquent\Builder|object
     */
    public function index($order_product_id)
    {
        $refund = OrderProductRefund::has('product')
            ->with('logs.images')
            ->where('order_product_id', $order_product_id)
            ->first();

        if ($refund
            && is_array($refund->applied_data)
            && !empty($refund->applied_data['images'])
            && is_array($refund->applied_data['images'])
        ) {
            $applied_data = $refund->applied_data;
            foreach ($applied_data['images'] as $k => $v) {
                $applied_data['images'][$k]['value'] = ImageHelper::getImageUrl($v['value'], 'refund');
            }
            $refund->applied_data = $applied_data;
        }

        return $refund;
    }

    /**
     * 获取用户申请数据拼接
     */
    private function applyContent($request, $provider)
    {
        $content = '';
        $type_str = [];
        switch ($provider) {
            case 'standard':
                $type_str['pay'] = '仅退款';
                $type_str['product'] = '退货退款';
                break;
            case 'point':
                $type_str['pay'] = '仅退积分';
                $type_str['product'] = '退货退积分';
                break;
        }
        $content .= '发起了' . $type_str[$request->provider] . '申请';

        if ($request->options_str) {
            $content .= '，' . $request->options_str;
        }

        if ($request->money) {
            switch ($provider) {
                case 'standard':
                    $content .= '，金额：¥' . $request->money;
                    break;
                case 'point':
                    $content .= '，积分：' . (float)$request->money;
                    break;
            }
        }
        if ($request->express_fee) {
            $content .= '，垫付运费：¥' . $request->express_fee;
        }
        if ($request->content) {
            $content .= '，退款说明：' . $request->content;
        }

        $images = [];
        if ($request->images) {
            $images = $request->images;
        }

        return array_filter(compact('content', 'images'));
    }

    /**
     * 用户申请售后
     * @param string $order_product_id 订单产品号
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    public function applyRefund($order_product_id, $request)
    {
        DB::beginTransaction();
        $product = OrderProduct::has('order')
            ->find($order_product_id);

        if ($product) {
            // 限制申请金额不能大于最高提现金额
            if ($request->money > $product->refundable_amount) {
                DB::rollBack();
                return false;
            }
            $refund = $product->refund;
            if ($refund && ($refund->status == 1 || !in_array($refund->refund_status, [0, 4]))) {
                DB::rollBack();
                return false;
            }
            $contentArr = self::applyContent($request, $product->order->provider);
            if (!$contentArr) {
                DB::rollBack();
                return false;
            }
            // 计算商品最高可退价格或积分
            $price = self::CalculatePrice($request->money);

            $storeRefund = self::store($product, [
                'title' => config('amii.refund.applyTitle'),
                'content' => $contentArr['content'],
                'provider' => $request->provider,
                'pay' => (float)$request->money,
                'express_fee' => (float)$request->express_fee,
                'is_user_record' => true,
                'images' => $request->images,
                'applied_data' => $request->only([
                    'provider',
                    'express_fee',
                    'options_str',
                    'money',
                    'content',
                    'images',
                ]),
            ]);
            if (!$storeRefund) {
                DB::rollBack();
                return false;
            }
            // 变更订单售后状态
//            $updateOrderRefund = app('order')->updateOrderRefund($product->order_id);
//            if (!$updateOrderRefund) {
//                DB::rollBack();
//                return false;
//            }

            DB::commit();
            return true;
        }
        DB::rollBack();
        return false;
    }

    /**
     * 填写快递信息
     * @param $order_product_id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Builder|boolean
     */
    public function express($order_product_id, $request)
    {
        $refund = $this->index($order_product_id);
        if (!$refund) {
            return false;
        }

        $refund->logs()->where('is_express', true)->update([
            'is_express' => false,
        ]);

        $refund->logs()->create([
            'is_user_record' => true,
            'title' => '买家已退货',
            'content' => '物流公司：' . $request->courier_company . '，物流单号：' . $request->courier_number,
        ]);

        return $refund;
    }

    //admin 同意售后
    public function adminAgreeRefund($order_product_id, $content = null)
    {
        DB::beginTransaction();
        $data = [];
        $product = OrderProduct::find($order_product_id);
        if (!$product) {
            return ['status' => 0, 'message' => '订单出错'];
        }

        $refund = $product->refund;
        if (!$refund) {
            return ['status' => 0, 'message' => '订单出错'];
        }

        if ($refund->applied_data['money'] > $refund->pay) {
            return ['status' => 0, 'message' => '退款(积分)超过最高可退金额(最高可退积分)'];
        }

        if ($refund->provider == 'pay') {
            $data = [
                [
                    'title' => '商家已同意申请',
                    'content' => '商家同意了本次售后服务申请',
                ],
            ];
        } elseif ($refund->provider == 'product') {
            if (empty($content)) {
                return ['status' => 0,'message' => '退货地址不能为空'];
            }
            $data = [
                [
                    'title' => '商家已确认收货地址',
                    'content' => $content,
                    'is_express' => true,
                ],
            ];
        }
        $refund->refund_status = 1;

        $logs = $refund->logs()->createMany($data);

        if ($refund->provider == 'pay') {
            // 退款
            $refund->refund_status = 2;
            //调起退款
            $refund->launchRefund();
            if (isset($refund->order->provider) && $refund->order->provider == 'standard') {
                ReturnProfitHelper::refundSuccessReturnProfit($refund->product->order_id, $refund->id);
            }
        } else {
            // 退款退货
            $refund->status = 0;
            //调起退款
        }
        $refundSave = $refund->save();
        if ($refundSave && $logs) {
            DB::commit();
            return ['status' => 1];
        }
        DB::rollBack();
        return ['status' => 0, '操作失败'];
    }

    /**
     * 商户拒绝申请
     **/
    public function refuse($id, $refuse_reason = null)
    {
        DB::beginTransaction();
        $refund = OrderProductRefund::where('id', $id)
            ->first();
        if (!$refund) {
            return false;
        }
        $refund->refund_status = 4;
        $refund->refuse_reason = $refuse_reason;
        $refund->status = 2;

        $data = [
            [
                'title' => '商家拒绝申请',
                'content' => '商家拒绝了本次售后服务申请' . "\n" . '拒绝原因：' . $refuse_reason,
            ],
        ];
        $save = $refund->save();
        // 变更订单售后状态
//        $updateOrderRefund = app('order')->updateOrderRefund($refund->product->order_id);
        //create log
        $logs = $refund->logs()->createMany($data);
//        if ($save && $updateOrderRefund && $logs) {
        DB::commit();
        // 重新计算佣金
        ReturnProfitHelper::refundFailReturnProfit($refund->product->order_id, $refund->id);
        return true;
//        }
//        DB::rollBack();
//        return true;

    }

    /**
     * 商户确认收货
     **/
    public function received($id)
    {
        DB::beginTransaction();
        $refund = OrderProductRefund::where('id', $id)
            ->first();
        if (!$refund) {
            return false;
        }
        $refund->refund_status = 2;
        $data = [
            [
                'title' => '商家确认',
                'content' => '商家确认了收货',
            ],
        ];
        $save = $refund->save();

        $refund->launchRefund();

        // 变更订单售后状态
//        $updateOrderRefund = app('order')->updateOrderRefund($refund->product->order_id);
        //create log
        $logs = $refund->logs()->createMany($data);
//        if ($save && $updateOrderRefund && $logs) {
        DB::commit();
         // 重新计算佣金
        ReturnProfitHelper::refundSuccessReturnProfit($refund->product->order_id, $refund->id);

        return true;
//        }
//        DB::rollBack();
//        return true;

    }

    /**
     * 获取订单商品是否售后
     */
    public function isRefund($orderProductId)
    {
        $refund = OrderProductRefund::where('order_product_id', $orderProductId)
            ->first();
        if (!$refund) {
            return null;
        }
        return (string)$refund->refund_status;
    }

    // 计算最高可退金额或积分
    public static function CalculatePrice()
    {
        return 1000;
    }
}
