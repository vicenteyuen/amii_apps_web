<?php

namespace App\Services;

use App\Models\Image;

class ImageService
{
    /**
     * 图片列表
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public function index($provider)
    {
        return Image::where('provider', $provider)
            ->paginate(config('amii.imagePaginate'));
    }

    public function adminIndex()
    {
        return Image::paginate(config('amii.imagePaginate'));
    }

    public function pageImage($provider, $page)
    {
        $num = 20;
        $skip = ($page-1) * $num;
        if ($page == 0) {
            return [];
        }
        
         return Image::where('provider', $provider)
            ->skip($skip)
            ->take($num)
            ->get();
    }

    // store image
    public function store($arr)
    {
        return Image::create($arr);
    }

    /**
     * 删除图片
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return Image::destroy($id);
    }

    public function deleteAll($ids)
    {
        return Image::whereIn('id', $ids)
            ->delete();
    }

    public function fileImage($fileId, $request = null)
    {
        $image = Image::where('image_file_id', $fileId)
            ->paginate(config('amii.imagePaginate'));
        return $request ? $image->appends($request->all()): $image;
    }
}
