<?php

namespace App\Services;

use App\Models\TodayTask;
use App\Models\PointReceived;
use App\Models\User;
use DB;

class TodayTaskService
{
    // 获取今日任务列表
    public function getIndex($userId)
    {
        $begintime = strtotime('today') + 8 * 3600;
        $endtime = $begintime + 86399;

        $user = User::where('id', $userId)
            ->first();
        $todayPoints = PointReceived::where('user_id', $userId)
            ->whereBetween(\DB::raw('unix_timestamp(created_at)'), [$begintime, $endtime])
            ->where('type', '!=', 'reback')
            ->where('type', '!=', 'consumer')
            ->sum('points') ?: "0";
        $todayTasks = TodayTask::orderBy('id')
            ->get();
        $data['nickname'] = $user->nickname;
        $data['avatar'] = $user->avatar;
        $data['got_points'] = $todayPoints;
        $data['task_list'] = $todayTasks;

        return $data;
    }
}
