<?php

namespace App\Services;

use App\Models\ProductService;

class ProductServiceService
{
    public static function index()
    {
        return ProductService::where('status', 1)
            ->get();
    }

    public static function adminIndex()
    {
        return ProductService::get();
    }

    public function update($id, $status)
    {
        return ProductService::where('id', $id)
            ->update(['status' => $status]);
    }
}
