<?php
namespace App\Services;

use App\Models\ProductCategories;
use App\Models\BrandProduct;
use Ecommerce\Models\Product;
use App\Models\BrandCategory;
use DB;

class ProductCategoriesService
{
    /**
     * 新增关联
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr, $arr1)
    {
        $exits = BrandProduct::where(['brand_id' => $arr['brand_id'],'product_id' => $arr['product_id']])
            ->first();

        if (!$exits) {
            return ['status' => 0,'msg'=>'该添加失败,品牌无此商品'];
        }
        $create = ProductCategories::firstOrCreate($arr);
        $createWithoutBrand = ProductCategories::firstOrCreate($arr1);
        if ($create && $createWithoutBrand) {
            return ['status'=>1,'msg'=>'添加成功'];
        } else {
            return ['status' => 0,'msg'=>'添加商品到分类失败'];
        }
     
    }

    //搜索商品
    public function searchCategoryProduct($key, $categoryId, $request = null, $pageNumber = null)
    {
        $pageNumber = $pageNumber ?:config('amii.adminPaginate');
        return Product::where(function ($q1) use ($key, $categoryId) {
                    $q1->orWhere('name', 'like', '%'.$key.'%')
                       ->orWhere('snumber', 'like', '%'.$key.'%');
                    return $q1;
        })->doesntHave('productCategory', 'and', function ($q3) use ($categoryId) {
            return $q3->where('brand_id', 0)
            ->where('category_id', $categoryId);
        })->select('id', 'name', 'snumber', 'main_image', 'product_price')
          ->orderBy('created_at', 'desc')
          ->paginate($pageNumber)
          ->appends($request->all());
    }

    //添加无品牌分类商品
    public function storeWithoutBrand($arr)
    {
        return ProductCategories::updateOrCreate($arr);
    }

    /**
     * 删除关联
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        return ProductCategories::where('id', $id)
            ->delete();
    }

    public function deleteAll($ids)
    {
        return ProductCategories::whereIn('id', $ids)
            ->delete();
    }

    /**
     * 查重
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function check($arr)
    {
        return ProductCategories::where($arr)
            ->first();
    }

    /**
     * 分类商品(所有数据)
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function index($id)
    {
         return Product::whereHas('productCategoryWithoutBrand', function ($q1) use ($id) {
            return $q1->where('category_id', $id);
         })->paginate(config('amii.apiPaginate'));
    }

    public function insertAll($brandId, $categoryId, $productIds)
    {
        $insert = [];
        foreach ($productIds as $key => $value) {
            $insert[] = [
                'brand_id'    => $brandId,
                'category_id' => $categoryId,
                'product_id'  => $value
            ];
            $insertWithoutBrand[] = [
                'brand_id'    => 0,
                'category_id' => $categoryId,
                'product_id'  => $value
            ];
        }
        if (empty($insert)) {
            return false;
        }

        $arr =  $insertWithoutBrand;
        $productCategories = ProductCategories::where(function ($q1) use ($insertWithoutBrand) {
            foreach ($insertWithoutBrand as $key => $value) {
                $q1->orWhere(['brand_id' => $value['brand_id'], 'category_id' => $value['category_id'], 'product_id' => $value['product_id']]);
            }
            return $q1;
        })->get();

        foreach ($productCategories as $k1 => $v1) {
            foreach ($insertWithoutBrand as $k2 => $v2) {
                if ($v1->brand_id == $v2['brand_id'] && $v1->category_id == $v2['category_id'] && $v1->product_id == $v2['product_id']) {
                    unset($arr[$k2]);
                }
            }
        }
        ProductCategories::insert($arr);
        ProductCategories::insert($insert);
        return true;
    }
}
