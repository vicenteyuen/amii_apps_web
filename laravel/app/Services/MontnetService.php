<?php

namespace App\Services;

use Log;
use Config;
use Cache;
use GuzzleHttp\Client;
use App\Helpers\SnumHelper;
use App\Helpers\AmiiHelper;

class MontnetService
{
    private $url;
    private $userId;
    private $pwd;
    private $client;

    public function __construct()
    {
        $cfg = config('amii.montent.sms');
        $this->url = $cfg['url'];
        $this->userId = $cfg['user_id'];
        $this->pwd = $cfg['pwd'];
        $this->client = new Client([
            // 'allow_redirects' => false,
            // 'http_errors' => false,
            'base_uri' => $this->url,
            'headers' => [
                'User-Agent' => 'Funshow (ganguo)',
            ],
            // 'timeout' => '60',
        ]);
    }

    // 请求返回
    private function handleResponse($response)
    {
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200) {
            return false;
        }

        $xmlStr = $response->getBody()->getContents();
        $arr = json_decode(json_encode(simplexml_load_string($xmlStr)), true);
        if (empty($arr)) {
            return false;
        }

        $responseData = $arr[0];
        $len = strlen($responseData);
        if ($len < 15 || $len > 25) {
            return false;
        }
        return true;
    }

    // 获取发送短信类容
    public function getRequestSmsContent($provider, array $data)
    {
        $str = '';
        switch ($provider) {
            case 'custom':
                if (isset($data['code'])) {
                    $code = $data['code'];
                } else {
                    return false;
                }
                $str .= '验证码是：' . $code . '，请在10分钟内完成验证。#首单7折，玩游戏送美衣，邀请好友享佣金，更多优惠等你来#';
                break;
            case 'sendPswd':
                if (isset($data['pwd'])) {
                    $pwd = $data['pwd'];
                } else {
                    return false;
                }
                $str .= '初始密码为：' . $pwd . '，如非本人操作，请忽略。请点击此链接： http://app.amii.com 下载APP';
                break;
            case 'blackList':
                if (isset($data['phone'])) {
                    $phone = $data['phone'];
                } else {
                    return false;
                }
                $str .= '经系统检测，您邀请好友注册获赠积分中存在操作异常情况。本平台根据用户注册协议现暂时冻结您的积分使用权与拼图送衣游戏的参与权，如有疑问，请在7个工作日内发邮件联系我们。联系邮箱：zhengfulin@amii.com感谢您的配合！';
                break;
            default:
                return false;
                break;
        }
        return $str;
    }

    // 发送短信
    public function requestSmsCode($phone, $provider = 'custom', $data = [])
    {
        $jsonArr = [
            'userId' => $this->userId,
            'password' => $this->pwd,
            'pszMobis' => $phone,
            'iMobiCount' => 1,
            'pszSubPort' => '*',
            'MsgId' => AmiiHelper::generateMontnetMsgId()
        ];

        $getContentDataArr = [];
        if ($provider == 'custom') {
            $code = SnumHelper::snum(4);
            $getContentDataArr['code'] = $code;

            // 验证码存到缓存中
            $cacheKey = $phone . '_sms_' . $code;
            Cache::put($cacheKey, bcrypt($code), 10);
        } elseif ($provider == 'sendPswd') {
            // 发送密码
            $pwd = $data['pswd'];
            $getContentDataArr['pwd'] = $pwd;
        } elseif ($provider == 'blackList') {
            // 发送
            $getContentDataArr['phone'] = $phone;
        }

        $content = $this->getRequestSmsContent($provider, $getContentDataArr);
        if ($content === false) {
            return false;
        }
        $jsonArr['pszMsg'] = $content;
        $bodyStr = http_build_query($jsonArr);
        $response = $this->client->post('MongateSendSubmit', [
            'body' => $bodyStr,
        ]);
        return $this->handleResponse($response);
    }

    // 验证短信验证码
    public function verifySmsCode($phone, $code, $provider = 'custom')
    {
        if ($provider == 'custom') {
            // 仅普通模版发送短信需要校验
            $cacheKey = $phone . '_sms_' . $code;
            if (! \Hash::check($code, Cache::get($cacheKey))) {
                return false;
            }

            // 清除校验码
            Cache::forget($cacheKey);
            return true;
        }
        return false;
    }
}
