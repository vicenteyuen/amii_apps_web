<?php

namespace App\Services;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Validator;
use App\Helpers\PaymentHelper;
use App\Helpers\OrderJobHelper;
use App\Helpers\OrderRefundJobHelper;
use App\Helpers\AmiiHelper;

use App\Models\Order;
use App\Models\Payment;
use App\Models\Cart;
use App\Models\UserGame;
use App\Models\OrderProduct;

class PayService
{
    public function store($userId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orderSn'       => 'required',
            'payType'       => 'required',
            'addrId'        => 'required|integer',
            'invoice'       => '',
            'remark'        => '',
            'couponCode'    => '',
            'point'         => 'numeric|max:2147483647',
            'balance'       => 'numeric|max:2147483647',
            'bestTime'      => '',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', null, $errors);
        }

        if (in_array($request->payType, ['weapp'])) {
            $validator = Validator::make($request->all(), [
                'openid' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return app('jsend')->error('参数错误', null, $errors);
            }
        }

        // 校验支付方式是否可选
        $provider = self::getPaymentProvider($request->payType);
        if (! $provider) {
            return app('jsend')->error('当前支付方式不可用');
        }

        DB::beginTransaction();

        // 获取当前支付订单
        $order = Order::where('order_sn', $request->orderSn)
            ->unconfirm()
            ->with(['payments' => function ($query) {
                return $query->where('status', 1)
                    ->where('refund', 0);
            }])
            ->first();

        if (! $order || ! $order->payments->isEmpty()) {
            return app('jsend')->error('当前订单不在可支付状态或已支付完成');
        }

        // 减库存
        $inventoryIds = $order->orderProducts
            ->pluck('number', 'inventory_id')
            ->toArray();
        $inventoryCheckData = array_map(function ($k, $v) {
            return [
                'id' => $k,
                'number' => $v
            ];
        }, array_keys($inventoryIds), $inventoryIds);

        $checkInventory = app('order')->inventoryCheck($inventoryCheckData, true);
        if (! $checkInventory) {
            // 设置当前订单无效
            $order->update([
                    'order_status' => 3
                ]);
            return app('jsend')->error('库存不足');
        }

        // 购物车购买删除购物车对应条目
        if ($order->cart_ids && ! Cart::whereIn('id', $order->cart_ids)->delete()) {
            DB::rollBack();
            return app('jsend')->error('系统错误');
        }

        // 保存订单数据
        $storeOrderData = self::storeOrderData($order, $request->all(), 'standard', $checkInventory);
        if (! $storeOrderData['status']) {
            DB::rollBack();
            return app('jsend')->error($storeOrderData['message']);
        }
        if (! $storeOrderData['data']) {
            DB::rollBack();
            return app('jsend')->error('存储订单数据失败,请重试');
        }

        DB::commit();

        // 添加订单未支付退回库存
        OrderJobHelper::cancel($order->order_sn);

        // 生成支付
        $returnPay = self::createOrderPay($provider, $order->order_sn, $order);

        return $returnPay;
    }

    /**
     * 校验支付方式
     */
    private function getPaymentProvider($provider)
    {
        switch ($provider) {
            case 'alipay':
                $provider = 'alipay';
                break;
            case 'alipayweb':
                $provider = 'alipayweb';
                break;
            case 'wechat':
                $provider = 'wechat';
                break;
            case 'wemp':
                $provider = 'wemp';
                break;
            case 'weapp':
                $provider = 'weapp';
                break;

            default:
                $provider = false;
                break;
        }
        return $provider;
    }

    /**
     * 获取支付返回数据
     */
    private function getPayReturn(array $data)
    {
        switch ($data['provider']) {
            case 'alipay':
                $returnPay = PaymentHelper::alipay($data['orderSn']);
                break;
            case 'alipayweb':
                $returnPay = PaymentHelper::alipayweb($data['orderSn']);
                break;
            case 'wechat':
                $returnPay = PaymentHelper::wechat($data['orderSn']);
                break;
            case 'wemp':
                $returnPay = PaymentHelper::wemp($data['orderSn'], $data['openid']);
                break;
            case 'weapp':
                $returnPay = PaymentHelper::weapp($data['orderSn'], $data['openid']);
                break;

            default:
                $returnPay = '';
                break;
        }
        return $returnPay;
    }

    /**
     * 未支付订单继续支付
     */
    public function pay($orderSn, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payType' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', null, $errors);
        }

        if (in_array($request->payType, ['weapp'])) {
            $validator = Validator::make($request->all(), [
                'openid' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return app('jsend')->error('参数错误', null, $errors);
            }
        }

        $returnPay = self::createOrderPay($request->payType, $orderSn);

        return $returnPay;
    }

    /**
     * 存储提交订单数据
     */
    public function storeOrderData($order, array $request, $orderProvider = 'standard', $checkInventory = [])
    {
        $returnData = [
            'status'    => false,
            'message'   => '',
        ];

        // 订单状态修改为已确认
        $order->order_status = 1;
        // 下单时间结束
        $order->order_end_time = Carbon::now();
        // 支付时间开始
        $order->pay_start_time = Carbon::now();

        // 添加7天后关闭售后
        // OrderRefundJobHelper::closeRefund($order->order_sn);

        // 最佳时间
        if (isset($request['bestTime'])) {
            $order->best_time = $request['bestTime'];
        }

        $userId = auth()->id();
        $user = auth()->user();

        // 订单地址
        $orderAddrArr = self::orderAddr($userId, $request['addrId']);
        if (! $orderAddrArr) {
            $returnData['message'] = '订单地址错误';
            return $returnData;
        }
        $order->shipping_region = $orderAddrArr['region'];
        $order->shipping_address = $orderAddrArr['address'];

        // 邮费
        $orderShipArr['data'] = 0;
        if ($orderProvider == 'point') {
            if ($order->product_amount > $user->points) {
                $returnData['message'] = '积分不足兑换';
                return $returnData;
            }

            $order->shipping_fee = 0;
            $order->discount = $order->product_amount;
            $order->point_discount = $order->product_amount;
            $order->order_fee = 0.00;
            $order->pay_status = 1;
            $order->pay_end_time = Carbon::now();
            //扣除积分
            app('pointReceived')->userAddPoint('consumer', 'consumer', $request['point'], $order->user_id);
        } elseif ($orderProvider == 'standard') {
            // 订单价格计算
            $calcPayFee = self::calcPayFee(
                $order->order_sn,
                $userId,
                $request['addrId'],
                isset($request['couponCode']) ? $request['couponCode'] : null,
                isset($request['point']) ? $request['point'] : 0,
                isset($request['balance']) ? $request['balance'] : 0,
                true
            );
            if ($calcPayFee[0] !== 0) {
                $returnData['message'] = '价格计算失败';
                return $returnData;
            }

            $calcFee = $calcPayFee[1];

            // 邮费
            $order->shipping_fee = $calcFee['shipfee'];

            // 订单发票抬头
            if (isset($request['invoice'])) {
                $order->invoice = $request['invoice'];
            }

            // 备注
            if (isset($request['remark'])) {
                $order->remark = $request['remark'];
            }

            // 使用优惠
            $discount = $calcFee['vip_discount']
                + $calcFee['coupon_discount']
                + $calcFee['point_discount']
                + $calcFee['balance_discount'];
            $order->discount = $discount;

            $order->order_fee = $calcFee['payfee'];
            // vip 优惠
            $order->vip_discount = $calcFee['vip_discount'];

            // 优惠券
            if (isset($request['couponCode'])) {
                $order->coupon_code = $request['couponCode'];
                $order->coupon_discount = $calcFee['coupon_discount'];
            }

            // 使用积分
            if (isset($request['point'])) {
                $order->point_discount = $calcFee['point_discount'];
            }

            // 使用余额
            if (isset($request['balance'])) {
                $order->balance_discount = $calcFee['balance_discount'];
            }

            if ($calcFee['payfee'] == 0) {
                $order->pay_status = 1;
                $order->pay_end_time = Carbon::now();
            }

            // 首单商品赠送
            app('amii.give.product')->firstPurchase($order->order_fee + $order->discount, $userId, $order->id);

            $orderAllFee = $calcFee['payfee'] - $calcFee['shipfee']
                + $discount;
            $orderDiscount = $calcFee['vip_discount'] + $calcFee['coupon_discount'];
            // 商品可退金额计算
            $orderProducts = OrderProduct::where('order_id', $order->id)
                ->where(function ($query) use ($checkInventory) {
                    foreach ($checkInventory as $checkInventoryWhere) {
                        $query->orWhere($checkInventoryWhere);
                    }
                })
                ->get()
                ->toArray();
            $orderProductIds = array_map(function ($orderProduct) {
                return $orderProduct['id'];
            }, $orderProducts);
            $updateOrderProductsData = array_map(function ($orderProduct) use ($orderDiscount, $orderAllFee, $checkInventory) {
                $value = 0.00;
                foreach ($checkInventory as $inventoryWhere) {
                    if (empty(array_diff($inventoryWhere, $orderProduct))) {
                        // 订单商品可退金额均摊vip优惠以及优惠券优惠
                        $value = (string)(floor($inventoryWhere['refundable_amount'] * (1 - $orderDiscount / $orderAllFee) * 100) / 100);
                        break;
                    }
                }
                return [
                    'key' => $orderProduct['id'],
                    'value' => $value
                ];
            }, $orderProducts);

            $queryOrderProduct = OrderProduct::whereIn('id', $orderProductIds);
            AmiiHelper::updateColumnDiffValues($queryOrderProduct, 'refundable_amount', 'id', $updateOrderProductsData);
        }

        if ($order->pay_status) {
            $order->incrementVolume();
        }

        $returnData = [
            'status' => true,
            'data' => $order->save()
        ];

        return $returnData;
    }

    /**
     * 创建支付单
     * @param string $provider 支付方式
     * @param string $orderSn 订单号
     * @param \Illuminate\Database\Eloquent|false $order
     */
    private function createOrderPay($provider, $orderSn, $order = false)
    {
        DB::beginTransaction();
        if (! $order) {
            // 待支付订单查询
            $order = Order::where('order_sn', $orderSn)
                ->paying()
                ->with(['payments' => function ($query) {
                    return $query->where('status', 1)
                        ->where('refund', 0);
                }])
                ->first();

            // 校验支付方式是否可选
            $provider = self::getPaymentProvider($provider);
            if (! $provider) {
                return app('jsend')->error('当前支付方式不可用');
            }
        }

        if (! $order) {
            // 订单不存在或已支付
            return app('jsend')->error('订单不存在或已支付');
        }

        if ($order->order_fee == 0) {
             //可申请售后状态
            $order->can_refund = 1;
            $order->save();
            DB::commit();
            if ($order->provider == 'standard') {
                 // 上级增加待返金额
                app('gain')->store(['user_id' => $order->user_id,'income' => ($order->order_fee + $order->balance_discount)], $order->id);
            }
            return app('jsend')->success(['isFee' => true, 'payData' => '']);
        }

        // 创建支付纪录
        $hasPayment = Payment::where('order_id', $order->id)
            ->where('provider', $provider)
            ->first();
        if (! $hasPayment) {
            Payment::create([
                'order_id'  => $order->id,
                'provider'  => $provider,
                'total_fee' => $order->order_fee,
            ]);
        }

        if ($provider == 'weapp') {
            $openid = \Request::get('openid');
        } elseif ($provider == 'wemp') {
            $openid = session('wemp_pay_open_id');
            if (! $openid) {
                return app('jsend')->error('openid必须');
            }
        } else {
            $openid = '';
        }

        // 生成对应支付
        $data = [
            'provider'  => $provider,
            'orderSn'   => $order->order_sn,
            'openid'    => $openid
        ];
        $returnPay = self::getPayReturn($data);

        if (! $returnPay) {
            DB::rollBack();
            return app('jsend')->error('创建支付单失败,请重试');
        }

        DB::commit();
        return app('jsend')->success(['isFee' => false, 'payData' => $returnPay]);
    }

    /**
     * 订单地址
     */
    private function orderAddr($userId, $addrId)
    {
        // 获取用户收货地址信息
        $addr = app('address')->addrOne($userId, $addrId);
        if (! $addr) {
            return false;
        }

        $addrStr = '';
        if (isset($addr->address['province'])) {
            $addrStr .= $addr->address['province']->name . ' ';
        }
        if (isset($addr->address['city'])) {
            $addrStr .=  $addr->address['city']->name . ' ';
        }
        if (isset($addr->address['area'])) {
            $addrStr .=  $addr->address['area']->name . ' ';
        }
        if ($addr->detail) {
            $addrStr .= $addr->detail;
        }
        return [
            'region' => $addr->region_id,
            'address' => [
                'post_code' => $addr->post_code,
                'name' => $addr->realname,
                'phone' => $addr->phone,
                'province' => isset($addr->address['province']->name) ? $addr->address['province']->name : '',
                'city' => isset($addr->address['city']->name) ? $addr->address['city']->name : '',
                'area' => isset($addr->address['area']->name) ? $addr->address['area']->name : '',
                'detail' => $addr->detail,
                'address' => $addrStr
            ]
        ];
    }

    /**
     * 订单邮费
     */
    public function orderShip($orderSn, $regionCode = null, $needRegionCode = false)
    {
        $returnData = [
            'status' => false
        ];

        $shipfee = \App\Helpers\ExpressPriceHelper::orderExpressPrice($orderSn, $regionCode);
        $shipFee = 0.00;
        if ($shipfee[0] !== 0) {
            if ($shipfee[0] !== 500100 || $needRegionCode) {
                $returnData['msg'] = $shipfee[1];
                return $returnData;
            }
        } else {
            $shipFee = $shipfee[1];
        }

        // 计算运费
        return [
            'status' => true,
            'data' => $shipFee
        ];
    }

    /**
     * 订单使用优惠券
     * @param integer $userId
     * @param string $couponCode 优惠券唯一码
     * @param decimal $orderFee
     * @return array
     */
    public function orderUseCoupon($userId, $couponCode, $orderFee, $useCoupon = false)
    {
        $returnData = [
            'status' => false,
        ];

        $userCoupon = app('amii.manger.user.coupon')->show($userId, $couponCode);

        if ($userCoupon) {
            $couponValue = 0;
            // 计算优惠券可用额度
            switch ($userCoupon->coupon->provider) {
                case 'cash':
                    $couponValue = $orderFee >= $userCoupon->coupon->value ? $userCoupon->coupon->value : $orderFee;
                    break;
                case 'fullcut':
                    $couponValue = $userCoupon->coupon->value;
                    if ($userCoupon->coupon->lowest > $orderFee) {
                        $couponValue = 0;
                        $returnData['message'] = '当前优惠券不能使用';
                        return $returnData;
                    }
                    break;
                case 'rebate':
                    $couponValue = $userCoupon->coupon->value * $userCoupon->coupon->discount/100;
                    if ($userCoupon->coupon->lowest != 0 && $userCoupon->coupon->lowest < $orderFee) {
                        $couponValue = 0;
                        $returnData['message'] = '当前优惠券不能使用';
                        return $returnData;
                    }
                    if ($couponValue > $userCoupon->coupon->value) {
                        $couponValue = $userCoupon->coupon->value;
                    }
                    break;

                default:
                    $couponValue = 0;
                    $returnData['message'] = '当前优惠券不能使用';
                    return $returnData;
                    break;
            }
            if ($useCoupon) {
                // 使用优惠券
                $use = app('amii.manger.user.coupon')->use($userId, $userCoupon->code);
                if (! $use) {
                    $returnData['message'] = '使用优惠券失败';
                    return $returnData;
                }
            }
            $returnData = [
                'status' => true,
                'data' => $couponValue
            ];
        } else {
            $returnData['message'] = '当前优惠券不能使用';
        }
        return $returnData;
    }

    /**
     * 订单使用积分
     * @param integer $userId
     * @param integer $point 使用积分量
     * @param \Illuminate\Database\Eloquent $order
     * @return decimal $pointAmount 优惠金额
     */
    private function orderUsePoint($userId, $point, $order)
    {
        $returnData = [
            'status' => false
        ];

        $userPoint = app('user')->userRemainPoint($userId);
        if ($point <= $userPoint) {
            $pointPrice = $point / 100;
            if ($pointPrice > $order->order_fee) {
                $pointPrice = $order->order_fee;
            }
            $returnData['status'] = true;
            $returnData['data'] = $pointPrice * 100;
            // 扣除积分
            $use = app('user')->usePoint(auth()->id(), $pointPrice * 100);
            if (! $use) {
                $returnData['message'] = '扣除积分失败';
                return $returnData;
            }
        } else {
            $returnData['message'] = '积分不足';
        }

        return $returnData;
    }

    /**
     * 订单使用余额
     * @param integer $userId
     * @param integer $balance 使用余额
     * @param \Illuminate\Database\Eloquent $order
     * @return decimal $balanceAmount 优惠金额
     */
    private function orderUseBalance($userId, $balance, $order)
    {
        $returnData = [
            'status' => false
        ];

        $userGain = app('user')->userRemainGain($userId);
        if ($balance <= $userGain) {
            if ($balance > $order->order_fee) {
                $balance = $order->order_fee;
            }
            $returnData['status'] = true;
            $returnData['data'] = $balance;
            // 扣除余额
            $use = app('user')->useGain(auth()->id(), $balance);
            if (! $use) {
                $returnData['message'] = '扣除余额失败';
                return $returnData;
            }
        } else {
            $returnData['message'] = '余额不足';
        }

        return $returnData;
    }

    /**
     * 订单各种金额计算
     * @param string $orderSn
     * @param integer $userId
     * @param integer|null $addrId
     * @param string|null $couponCode
     * @param integer $point
     * @param integer $balance
     * @param bool $useDiscount 是否使用当前优惠
     * @return array
     */
    public function calcPayFee($orderSn, $userId, $addrId = null, $couponCode = null, $point = 0, $balance = 0, $useDiscount = false)
    {
        $gameOrder = Order::where('order_sn', $orderSn)
            ->with('game')
            ->first();
        if (isset($gameOrder->game) && !empty($gameOrder->game)) {
            $order = app('order')->getOrder2Pay($userId, $orderSn, '', true);
        } else {
            $order = app('order')->getOrder2Pay($userId, $orderSn);
        }
        if (! $order) {
            return [70001, '订单状态异常'];
        }

        // 检查是否是禁用户
        $checkUser = app('user')->checkUser($order->user_id);
        if (!$checkUser) {
            $point = 0;
        }

        // 检查是否是禁用户
        $isFirstOrder = app('order')->isFirstOrder($userId, $order->id);
        if ($isFirstOrder) {
            $couponCode = null;
            $point = 0;
            $balance = 0;
        }

        // 订单商品总价金额
        $orderProductAmount = $order->product_amount;
        // vip优惠
        $vipDiscount = app('order')->vipDiscount($userId, $order);
        $vip_discount = number_format($vipDiscount, 2, '.', '');

        // 订单需支付金额
        $payfee = $orderProductAmount
            - $vip_discount;

        // 获取优惠券优惠金额
        if ($couponCode) {
            $couponDiscountCalc = app('pay')->orderUseCoupon($userId, $couponCode, $orderProductAmount, $useDiscount);
            if (! $couponDiscountCalc['status']) {
                return [70002, $couponDiscountCalc['message']];
            }
            $couponDiscount = $couponDiscountCalc['data'] * 100 > $payfee * 100 ? $payfee : $couponDiscountCalc['data'];
            $coupon_discount = number_format($couponDiscount, 2, '.', '');
        } else {
            $couponDiscount = 0;
            $coupon_discount = number_format($couponDiscount, 2, '.', '');
        }

        if ($addrId) {
            // 获取用户地址
            $addr = app('address')->addrOne($userId, $addrId);
            if (! $addr) {
                return [70003, '用户地址不存在'];
            }
            $regionCode = $addr->region_id;
        } else {
            // 获取默认地址
            $addr = app('address')->default($userId);
            if (! $addr) {
                $regionCode = null;
            } else {
                $regionCode = $addr->region_id;
            }
        }
        // 获取地址邮费
        $shipfeeCalc = app('pay')->orderShip($orderSn, $regionCode, $addrId ? true : false);
        if (! $shipfeeCalc['status']) {
            return [70004, $shipfeeCalc['msg']];
        }
        $shipfee = number_format($shipfeeCalc['data'], 2, '.', '');

        // 订单需支付金额
        $payfee = $payfee
            - $couponDiscount
            + $shipfee;

        // 可用积分
        $calcPointAmount = $payfee;
        $can_use_point = app('order')->getPoint4Order($calcPointAmount);
        $can_use_point_discount = number_format($can_use_point ? $can_use_point / 100 : 0, 2, '.', '');
        if ($point) {
            $usePoint = $point > $can_use_point ? $can_use_point : $point;
            $use_point = $payfee * 100 > $usePoint ? $usePoint : $payfee * 100;
            $pointDiscount = $use_point > 0 ? $use_point / 100 : 0;
            $point_discount = number_format($pointDiscount, 2, '.', '');
            if ($useDiscount) {
                // 扣除积分
                $usePoint = app('user')->usePoint($userId, $use_point ? : 0);
                if (! $usePoint) {
                    return [70005, '扣除积分失败'];
                }
            }
        } else {
            $use_point = 0;
            $pointDiscount = 0;
            $point_discount = number_format($pointDiscount, 2, '.', '');
        }

        // 订单需支付金额
        $payfee = $payfee
            - $pointDiscount;

        // 可用余额
        $can_use_balance = app('order')->getBalance4Order($order);
        if ($balance) {
            $useBalance = $balance * 100 > $can_use_balance * 100 ? $can_use_balance : $balance;
            $use_balance = $payfee * 100 > $useBalance * 100 ? $useBalance * 100 : $payfee * 100;
            $balance_discount = number_format($use_balance / 100, 2, '.', '');
            if ($useDiscount) {
                // 扣除使用余额
                $use = app('user')->useGain($userId, $balance_discount);
                if (! $use) {
                    return [70006, '扣除余额失败'];
                }
            }
        } else {
            $use_balance = 0;
            $balance_discount = number_format($use_balance, 2, '.', '');
        }

        // 订单需支付金额
        $payfee = $payfee
            - $use_balance / 100;
        $payfee = intval($payfee * 100) / 100;
        $payfee = number_format($payfee, 2, '.', '');

        $game = UserGame::where('order_id', $order->id)
                ->first();
        if ($game) {
            //游戏订单
            $payfee = '0.00';
        }

        return [
            0,
            compact(
                'vip_discount',
                'coupon_discount',
                'shipfee',
                'can_use_point',
                'can_use_point_discount',
                'use_point',
                'point_discount',
                'can_use_balance',
                'balance_discount',
                'payfee'
            )
        ];
    }

    /**
     * 订单是否展示首单商品
     * @param array $calcPayFee
     */
    public function isShowFirstOrderProduct(array $calcPayFee)
    {
        $give_line = app('amii.fisrt.product')->purchaseMoney();
        $calcFee = $calcPayFee['payfee']
            + $calcPayFee['point_discount']
            + $calcPayFee['balance_discount'];
        if ($calcFee * 100 >= $give_line * 100) {
            $is_show_first_order_product = 1;
        } else {
            $is_show_first_order_product = 0;
        }
        return $is_show_first_order_product;
    }
}
