<?php
namespace App\Services;

use App\Models\GiveProductHistory;

class GiveProductHistoryService
{

    //列表
    public function index()
    {
        return GiveProductHistory::with('sellProduct.product')
            ->paginate(config('amii.adminPaginate'));
    }
}
