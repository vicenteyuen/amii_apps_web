<?php
namespace App\Services;

use App\Models\UserComment;
use App\Models\CommentImage;
use App\Models\SellProduct;
use App\Models\Product;
use App\Models\Order;
use App\Models\CommentRank;
use DB;
use Validator;
use App\Helpers\AmiiHelper;
use App\Helpers\FigureCommentPraise;

class UserCommentService
{
    /**
     * 新增评论
     * @param  array $userId 用户id
     * @param  array $image [description]
     * @return bool         [description]
     */
    // public function store($arr, $image)
    public function store($userId, $request)
    {
        $returnData = [
            'status' => false,
            'message' => ''
        ];
        DB::beginTransaction();
        $order = Order::where('order_sn', $request->orderSn)
            ->where('user_id', $userId)
            ->appraising()
            ->with('orderProducts.sellProduct.product')
            ->with('orderProducts.inventory.sku.skuValue.attribute')
            ->first();
        if (! $order) {
            DB::rollback();
            $returnData['message'] = '订单错误';
            return $returnData;
        }
        $orderProducts = $order->orderProducts;

        $sellProductIds = array_unique(array_pluck($order->orderProducts, 'sell_product_id'));

        // 订单商品校验
        $orderInventoryIdsArr = $orderProducts->pluck('inventory_id')
            ->toArray();
        foreach ($request->comments as $comment) {
            if (! in_array($comment['inventoryId'], $orderInventoryIdsArr)) {
                DB::rollback();
                $returnData['message'] = '评价商品不存在';
                return $returnData;
            }
        }

        // 评价
        $contents = collect($request->comments)
            ->pluck('content', 'inventoryId')
            ->toArray();
        $contentStatus = collect($request->comments)
            ->pluck('status', 'inventoryId')
            ->toArray();

        $commentRank = collect($request->comments)
            ->pluck('comment_rank_id', 'inventoryId')
            ->toArray();

        $commentData = array_map(function ($orderProduct) use ($userId, $contents, $contentStatus, $commentRank) {
            return [
                'user_id'           => $userId,
                'inventory_id'      => $orderProduct['inventory_id'],
                'product_sell_id'   => $orderProduct['sell_product_id'],
                'content'           => $contents[$orderProduct['inventory_id']],
                'status'            => $contentStatus[$orderProduct['inventory_id']] ? 1 : 0,
                'comment_rank_id'   => $commentRank[$orderProduct['inventory_id']] ?:5,
            ];
        }, $orderProducts->toArray());
        $commentImagesData = [];
        foreach ($commentData as $comment) {
            $createComment = UserComment::create($comment);
            if (! $createComment) {
                DB::rollback();
                $returnData['message'] = '存储评价数据失败';
                return $returnData;
            }
            $commentId[] = $createComment->id;
        }
        foreach ($request->comments as $key => $comment) {
            if (isset($comment['images']) && ! empty($comment['images'])) {
                foreach ($comment['images'] as $image) {
                    $commentImagesData[] = [
                        'comment_id' => $commentId[$key],
                        'image' => $image,
                    ];
                }
                //增加积分
                if (\App\Helpers\AmiiHelper::utf8_strlen($comment['content'])>= 20) {
                    app('pointReceived')->userAddPoint('appraising', 'image');
                } else {
                    app('pointReceived')->userAddPoint('appraising', 'normal');
                }
            } else {
                app('pointReceived')->userAddPoint('appraising', 'normal');
            }
        }
        // 评价图片保存
        if ($commentImagesData) {
            CommentImage::insert($commentImagesData);
        }

        // 商品评价数量增加
        $productComment = [];
        foreach ($orderProducts as $orderProduct) {
            if (isset($productComment[$orderProduct->sell_product_id])) {
                $productComment[$orderProduct->sell_product_id] += 1;
            } else {
                $comNumber = $orderProduct->sellProduct->comment_number ;
                $productComment[$orderProduct->sell_product_id] = $comNumber + 1;
            }
        }
        $updateProductData = array_map(function ($sellProductId, $commentNumber) {
            return [
                'key' => $sellProductId,
                'value' => $commentNumber
            ];
        }, array_keys($productComment), $productComment);
        $updateQuery = SellProduct::whereIn('id', array_keys($productComment));
        AmiiHelper::updateColumnDiffValues($updateQuery, 'comment_number', 'id', $updateProductData);
        dispatch(new \App\Jobs\FigureCommentPraise($sellProductIds));

        // 订单状态更改为已评价
        $order->is_appraise = 1;
        if ($order->save()) {
            $returnData['status'] = true;
            DB::commit();
        } else {
            DB::rollback();
            $returnData['message'] = '修改订单已评价状态失败';
        }

        return $returnData;
    }

    /**
     * 评论列表
     * @return [type] [description]
     */
    public function index($id)
    {
        $userId = auth()->id()?:null;
        if ($userId) {
            return UserComment::where('product_sell_id', $id)
                ->where(function ($q1) use ($userId) {
                    return $q1->where('comment_status', 0)
                        ->orWhere('user_id', $userId);
                }) ->orderBy('comment_rank_id', 'desc')
                ->orderBy('created_at', 'desc')
                ->with('user.level.levelEnable.enable')
                ->with('image')
                ->with('inventory.sku.skuValue.attribute')
                ->paginate(config('amii.apiPaginate'));
        }

        return UserComment::where('product_sell_id', $id)
            ->where('comment_status', 0)
            ->orderBy('comment_rank_id', 'desc')
            ->orderBy('created_at', 'desc')
            ->with('user.level.levelEnable.enable')
            ->with('image')
            ->with('inventory.sku.skuValue.attribute')
            ->paginate(config('amii.apiPaginate'));

    }

    // 弹出评论
    public function popup($id)
    {
        return UserComment::where('product_sell_id', $id)
            ->where('comment_rank_id', 5)
            ->orderBy('created_at', 'desc')
            ->with('user.level.levelEnable.enable')
            ->with('image')
            ->with('inventory.sku.skuValue.attribute')
            ->paginate(config('amii.apiPaginate'));
    }

    /**
     * 删除
     * @param  [type] $id
     * @return integer
     */
    public function destroy($id)
    {
        $comment = UserComment::find($id);
        if (!$comment) {
            return false;
        }
        $inventory = DB::table('inventories')->where('id', $comment->inventory_id)
            ->first();

        if (!$inventory) {
            return false;
        }
        DB::beginTransaction();
        //删除评论 ，评论总数减1
        $delete = UserComment::destroy($id);
        $update = DB::table('sell_products')->where('id', $inventory->product_sell_id)
            ->decrement('comment_number', 1);

        if ($delete && $update) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    /**
     * 获取评论星级
     * @return array
     */
    public function getCommentRanks()
    {
        $ranks = CommentRank::orderBy('id')
            ->select('rank', 'desc')
            ->get();
        return $ranks;
    }

    /**
    * 商品评论状态停用
    * updateStatus usercomment
    */
    public function updateStatus($arr, $id)
    {

        $updateCommentStatus = UserComment::where('id', $id)
            ->update($arr);

        if ($updateCommentStatus) {
            return true;
        }
        return false;
    }

    /**
    * 商品评论内容
    * showCommentContent usercomment
    */
    public function showCommentContent($sell_product_id, $comment_id)
    {
        $data = UserComment::where('product_sell_id', $sell_product_id)
            ->where('id', $comment_id)
            ->first();
        if (!$data) {
            return false;
        }
        return $data;
    }

    /**
    * 商品评论图片
    * getCommentImg usercomment
    */
    public function getCommentImg($comment_id)
    {
        $data = CommentImage::where('comment_id', $comment_id)
            ->get();
        if (!$data) {
            return false;
        }
        return $data;
    }

    /**
    * 获取用户评论商品信息
    * @param  [type] $product_id      [description]
    * @param  [type] $provider        [description]
    * @param  [type] $comment_rank_id [description]
    * @return [type]                  [description]
    */
    public function getCommentInfo($product_id, $provider, $comment_rank_id = null, $search = null, $request = null,  $searchTime = null,$startTime = null, $endTime = null)
    {
        $data = UserComment::with('sellProduct.product', 'user')

            ->when($product_id, function ($q) use ($product_id, $provider) {
                return $q->whereHas('sellProduct', function ($query) use ($product_id, $provider) {
                    $query->where(['product_id' => $product_id, 'provider' => $provider]);
                });
            })
            // 选择星级
            ->when($comment_rank_id, function ($q) use ($comment_rank_id) {
                return $q->where('comment_rank_id', $comment_rank_id);
            })
            // 搜索
            ->when($search, function ($q) use ($search) {
                return $q->whereHas('sellProduct.product', function ($q1) use ($search) {

                    $q1->where('snumber', 'like', '%'.trim($search).'%');
                })->orWhereHas('user', function ($q2) use ($search) {

                    $q2->where('nickname', 'like', '%'.trim($search).'%');
                });
            })
            // 时间段搜索
            ->when($searchTime , function($q) use($startTime, $endTime) {
                return $q->whereBetween('created_at', [$startTime, $endTime]);
            });

            if(!$searchTime && $startTime){
                $data = clone($data)->when($startTime , function($q) use($startTime) {
                    return $q->where('created_at', '>=', $startTime);
                });
            }
            if (!$searchTime && $endTime) {
                $data = clone($data)->when($startTime , function($q) use($startTime) {
                    return $q->where('created_at', '<=', $endTime);
                });
            }
            $data = clone($data)->orderBy('created_at', 'desc')
            ->paginate(config('amii.adminPaginate'))
            ->appends($request->all());

        return  $data;
    }
}
