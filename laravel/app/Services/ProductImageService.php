<?php
namespace App\Services;

use App\Models\ProductImage;
use App\Models\SellProduct;

class ProductImageService
{
    /**
     *
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {
        return ProductImage::create($arr);

    }

    /**
     * 删除
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        return ProductImage::destroy($id);
    }

    /**
     * 获取商品图片(商品)
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function productShow($id)
    {
        return ProductImage::where('product_id', $id)
            ->with('product')
            ->get();
    }

    /**
     * 获取商品图片(库存)
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function index($where)
    {
        return ProductImage::where('product_id', $where['product_id'])
            ->get();

    }

    public function change($productImageId, $sellProductId)
    {
        $productImage = ProductImage::where('id', $productImageId)
            ->first();
        $sellProduct = SellProduct::where('id', $sellProductId)
            ->first();
        if (!$productImage || !$sellProduct) {
            return false;
        }
        $proImage = $productImage->getOriginal('image');
        $sellImage = $sellProduct->getOriginal('image');
        if (!$sellImage) {
            sellProduct::where('product_id', $sellProduct->product_id)
                ->update(['image' => $proImage]);
            return true;
        } else {
            $productImage->image = $sellImage;
            $productImage->save();
            sellProduct::where('product_id', $sellProduct->product_id)
                ->update(['image' => $proImage]);
            return true;
        }
    }
}
