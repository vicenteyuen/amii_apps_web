<?php
namespace App\Services;

use App\Models\User;
use App\Models\Share;
use App\Models\Subject;
use App\Models\Video;
use App\Models\SellProduct;
use Ecommerce\Models\Product;
use App\Models\ShareRelation;
use App\Models\UserGame;
use Cache;

class ShareRelationService
{

    // 获取选择分享数据
    public function getSelected($id, $provider)
    {
       switch ($provider) {
            case '1':
                $data = SellProduct::where('id', $id)
                    ->where('status', 1)
                    ->first();
                break;
            case '2':
                $data = Subject::where('id', $id)
                     ->where('status', 1)
                    ->first();
                break;
            case '3':
                $data = Video::where('id', $id)
                    ->where('status', 1)
                    ->first();
                break;
            case '4':
                // $provider = 'code';
                break;
            case '5':
                // $provider = 'tribe';
                break;
            case '6':
                // $provider = 'activity';
                break;
            case '7':
                $data = userGame::with('sellProduct.product')
                    ->where('id', $id)
                    ->first();
                break;
            default:
               return false;
                break;
        }

        return $data;
    }

    /**
    *  保存新增分享单品
    */
    public function storeShareRelation(array $arr)
    {
        $create =  ShareRelation::create($arr);
        return $create;
    }


}

