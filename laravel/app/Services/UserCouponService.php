<?php

namespace App\Services;

use DB;
use Carbon\Carbon;
use App\Models\UserCoupon;
use App\Models\CouponGrant;
use App\Models\Coupon;

class UserCouponService
{
    /**
     * 获取用户优惠券列表
     */
    public function index($userId, $type = 'all')
    {
        $coupons = UserCoupon::where('user_id', $userId)
            ->with('coupon');

        switch ($type) {
            case 'valid':
                // 可用优惠券
                $coupons = $coupons->whereHas('coupon', function($query) {
                        return $query->status()
                            ->timeUseful();
                    })
                    ->status();
                break;
            case 'expired':
                // 过期优惠券
                $coupons = $coupons->whereHas('coupon', function($query) {
                    return $query->where('end', '<=', Carbon::now());
                })
                ->status();
                break;
            case 'used':
                // 已用优惠券
                $coupons = $coupons->where('status', 0);
                break;

            default:
                $coupons = $coupons->status()
                    ->whereHas('coupon', function ($query) {
                        return $query->status();
                    });
                break;
        }

        return $coupons->get();
    }

    /**
     * 用户删除优惠券
     */
    public function delete($code)
    {
        return UserCoupon::where('code', $code)
            ->delete();
    }

    /**
     * 用户添加系统生成优惠券
     */
    public function store($userId, $cid)
    {
        DB::beginTransaction();
        // 用户是否领用优惠券
        $userCoupon = UserCoupon::where('user_id', $userId)
            ->whereHas('coupon', function ($query) use ($cid) {
                return $query->whereHas('couponGrant', function ($q) use ($cid) {
                    return $q->where('code', $cid);
                });
            })
            ->first();
        if ($userCoupon) {
            return app('jsend')->error('已领取此优惠券');
        }

        $couponGrant = CouponGrant::where('code', $cid)
            ->status()
            ->whereHas('coupon', function ($query) use ($cid) {
                return $query->status();
            })
            ->first();
        if ($couponGrant) {
            UserCoupon::create([
                'user_id' => $userId,
                'code' => $couponGrant->code,
                'status' => 1,
                'coupon_id' => $couponGrant->coupon_id,
            ]);

            // 优惠券更新为已领用
            $couponGrant->update([
               'status' => 1
            ]);

            $couponGrant->save();

            // 减优惠券数量
            Coupon::where('id', $couponGrant->coupon_id)
                ->decrement('remain');

            DB::commit();

            $coupon = Coupon::where('id', $couponGrant->coupon_id)
                ->first();

            $arr = ['user_id' => $userId,'coupon_desc' => $coupon->title];
            // 保存数据，并发送消息推送
            app('amii.notify.message')->sendNotifyMsg('notify', 'coupons', $arr);

            return app('jsend')->success(['coupons' => self::index($userId)]);
        }
        DB::rollBack();
        return app('jsend')->error('抱歉，优惠券无效或已被领取');
    }

    /**
     * 获取可用优惠券详情
     */
    public function show($userId, $code)
    {
        $userCoupon = UserCoupon::where('code', $code)
            ->where('user_id', $userId)
            ->status()
            ->whereHas('coupon', function ($query) use ($code) {
                $query->status()
                    ->timeUseful()
                    ->with(['couponGrant' => function ($q) use ($code) {
                        return $q->where('code', $code);
                    }]);
            })
            ->with('coupon')
            ->first();

        return $userCoupon;
    }

    /**
     * 获取订单可用优惠券
     */
    public function coupon4Order($userId, $order)
    {
        $coupons = UserCoupon::where('user_id', $userId)
            ->status()
            ->whereHas('coupon', function ($query) use ($order) {
                return $query->status()
                    ->timeUseful()
                    ->where(function ($q1) use ($order) {
                        return $q1->where(function ($q1) use ($order) {
                            // 现金券 无门槛
                            return $q1->cash();
                        })
                        ->orWhere(function ($q1) use ($order) {
                            // 满减券
                            return $q1->fullcut()
                                ->where('lowest', '<=', $order->product_amount);
                        })
                        ->orWhere(function ($q1) use ($order) {
                            // 折扣券 使用限制
                            return $q1->rebate()
                                ->where('lowest', 0)
                                ->orWhere('lowest', '<=', $order->product_amount);
                        });
                    });
            })
            ->with('coupon')
            ->get();
        return $coupons;
    }

    /**
     * 使用优惠券
     */
    public function use($userId, $code)
    {
        $userCoupon = UserCoupon::where('code', $code)
            ->where('user_id', $userId)
            ->status()
            ->whereHas('coupon', function ($query) use ($code) {
                $query->status()
                    ->timeUseful()
                    ->with(['couponGrant' => function ($q) use ($code) {
                        return $q->where('code', $code);
                    }]);
            })
            ->with('coupon')
            ->first();
        if (! $userCoupon) {
            return false;
        }
        return $userCoupon->update([
            'status' => 0,
        ]);
    }

    /**
     * 退回订单使用优惠券
     */
    public function rebackCoupon($userId, $couponCode)
    {
        return UserCoupon::where('code', $couponCode)
            ->where('user_id', $userId)
            ->update(['status' => 1]);
    }
}
