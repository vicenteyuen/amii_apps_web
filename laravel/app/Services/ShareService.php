<?php
namespace App\Services;

use App\Models\User;
use App\Models\Share;
use App\Models\Subject;
use App\Models\Video;
use App\Models\SellProduct;
use Ecommerce\Models\Product;
use App\Models\Token;
use App\Models\ShareType;
use App\Models\UserGame;
use App\Models\ShareRelation;
use Cache;

class ShareService
{

    /**
    *  admin 获取分享文案列表
    */
    public function adminShareIndex()
    {
        return Share::paginate(config('amii.adminPaginate'));
    }

    /**
    *  保存新增文案
    */
    public function store(array $arr, $shareSelectIds)
    {
        $create =  Share::create($arr);
        if (Cache::get('shares')) {
            Cache::forget('shares');
        }
        $field = $this->getShareRelationfield($create->provider_value);
        if ($shareSelectIds) {
            foreach ($shareSelectIds as $key => $shareSelectId) {
                ShareRelation::create(['share_id' => $create->id, $field => $shareSelectId ]);
            }
        }
        return $create;
    }

    /**
    * 更新文案内容
    */
    public function update($id, $arr, $shareSelectIds)
    {
        $update = Share::where('id', $id)->update($arr);
        $field = $this->getShareRelationfield($arr['provider']);

        $res = ShareRelation::where('share_id', $id)->get();

        if ($res && $update) {
            ShareRelation::where('share_id', $id)->delete();
            if ($shareSelectIds &&  $arr['is_shareAll'] != 1) {
                $newArr = array_map(function ($shareSelectId) use ($id, $field) {
                    return [
                        'share_id' => $id,
                         $field => $shareSelectId,
                    ];
                }, $shareSelectIds);

                foreach ($newArr as $key => $value) {
                    $res = ShareRelation::updateOrCreate($value);
                }
            }
            return true;
        } else {
            return false;
        }

    }

    /*
    *  获取文案详情
    */
    public function getShareDetail($id)
    {
        $data = Share::where('id', $id)
            ->first();
        if (!$data) {
            return false;
        }
        $provider = $data->provider_value;
        // 获取中间表字段
        $field = $this->getShareRelationfield($provider);

        if ($field) {
            // 获取中间表字段值集合
            $shareSelectIds = ShareRelation::where('share_id', $id)
                ->pluck($field);
            $shareSelectIds =  implode(',', $shareSelectIds->toArray());
            $data['shareSelectIds'] = $shareSelectIds;
        }
        return $data;
    }


    /**
     * 删除文案
     */
    public function delete($id)
    {
        $share = Share::destroy($id);
        if ($share) {
            $exits =  ShareRelation::where('share_id', $id)->first();
            if ($exits) {
                $shareRelation = ShareRelation::where('share_id', $id)->delete();
            }
            return true;
        }
        return false;
    }

    /**
    * 获取分享端口类型
    */
    public function getShareType()
    {
        $shareTypes = ShareType::orderBy('id')
            ->select('provider_id', 'desc')
            ->get();
        return $shareTypes;
    }

    /**
    * 获取用户uuid
    */
    public function getUserUuid($token)
    {
        if (!$token) {
            return null;
        }
        $user_id = Token::where('code', $token)->value('ref_id');
        $uuid  = User::where('id', $user_id)
                ->where('status', 1)
                ->value('uuid');
        if ($uuid) {
            $uuid ='?uuid='.$uuid;
        }
        return $uuid;
    }

    /**
    *   获取单品页端口的分享文案
    */
    public function getPoductShare($sell_product_id, $token)
    {
        $provider = 1;
        $collection = SellProduct::with('product')
                ->where('id', $sell_product_id)
                ->where('status', 1)
                ->first();
        if (! $collection) {
            return false;
        }

        // 有分享对象中间数据处理
        $data = $this->dealHasIDShareDates($token, $provider, $collection, $sell_product_id);
        return $data;
    }


    /**
    *   获取社区专题的分享文案
    */
    public function getSubjectShare($subject_id, $token)
    {
        $provider = 2;
        $collection =  Subject::where('id', $subject_id)
            ->where('status', 1)
            ->first();
        if (!$collection) {
            return false;
        }
        // 有分享对象中间数据处理
        $data = $this->dealHasIDShareDates($token, $provider, $collection, $subject_id);

        return $data;
    }

    /**
    *   获取社区视频的分享文案
    */
    public function getVideoShare($video_id, $token)
    {
        $provider = 3;
        $collection =  Video::where('id', $video_id)
            ->where('status', 1)
            ->first();
        if (!$collection) {
            return false;
        }
        // 有分享对象中间数据处理
        $data = $this->dealHasIDShareDates($token, $provider, $collection, $video_id);
        return $data;
    }

    /*
    *  获取二维码分享文案
    */
    public function getCodeShare($token)
    {
        $provider = 4;
        $collection['id'] = '';
        $collection['title'] = '有您有我有ta，精彩在A+';
        $collection['content'] = '免费送美衣、促单享高佣、积分排名拿大奖等精彩玩法尽在A+';
        $collection['image'] = asset(url('/assets/images/New-A+.png'));

         // 中间数据处理
        $data = $this->dealNotIDShareDates($token, $provider, $collection);
        return $data;
    }

    /*
    *  获取部落分享文案
    */
    public function getTribeShare($token)
    {
        $provider = 5;

        $collection['id'] = '';
        $collection['title'] = 'A+,时尚创业集市';
        $collection['content'] = '无门槛、零成本，品牌返佣高至30%,轻松实现您的时尚创业之路！';
        $collection['image'] = asset(url('/assets/images/New-A+.png'));

         // 全局分享的中间数据处理
        $data = $this->dealNotIDShareDates($token, $provider, $collection);
        return $data;
    }

    /*
    *  获取首单7折专区活动分享文案
    */
    public function getActivityShare($token)
    {
        $provider = 6;

        $collection['id'] = '';
        $collection['title'] = '首单7折+满送豪礼，精彩+++';
        $collection['content'] = 'A+首单享7折钜惠，满付还送旅行眼罩或U型枕或手表等豪礼！极简不简单,还有拼图送衣、促单返佣等。';
        $collection['image'] = asset(url('/assets/images/New-A+.png'));

        $source = 'advertisement';
        // 全局分享的中间数据处理
        $data = $this->dealNotIDShareDates($token, $provider, $collection, $source);
        return $data;
    }

    /*
    *  获取拼图游戏分享文案
    */
    public function getPuzzleShare($game_id, $token)
    {
        $provider = 7;
        $collection = SellProduct::with('userGame')
                ->wherehas('userGame', function ($q) use ($game_id) {
                    return $q->where('id', $game_id);
                })
                ->first();

        if (!$collection) {
            return false;
        }
        // 有分享对象中间数据处理
        $data = $this->dealHasIDShareDates($token, $provider, $collection, $game_id);
        return $data;
    }

    /*
    *  获取拼图游戏列表分享
    */
    public function getPuzzleListShare($token)
    {
        $provider = 8;

        $collection['id'] = '';
        $collection['title'] = 'WOW~万件美衣免费送!';
        $collection['content'] = '邀请好友帮忙点亮全部拼图合成美衣，美衣就属于您，邮费还是我们出哦~';
        $collection['image'] = asset(url('/assets/images/New-A+.png'));

        // 中间数据处理
        $data = $this->dealNotIDShareDates($token, $provider, $collection);
        return $data;
    }

    /*
    *  获取红包分享
    */
    public function getRedPacketShare($token)
    {
        $provider = 9;

        $collection['id'] = '';
        $collection['title'] = '《择天记》与《择衣计》傻傻分不清楚！';
        $collection['content'] = '你的好友力荐A+极简风美衣，还给你发了20元红包，赶紧领取吧！';
        $collection['image'] = asset(url('/assets/images/New-A+.png'));
        // $source = 'red_packet';
        // 中间数据处理
        $data = $this->dealNotIDShareDates($token, $provider, $collection);

        return $data;
    }

    /*
    *  获取红包分享
    */
    public function getHomeShare($token)
    {
        $provider = 10;

        $collection['id'] = '';
        $collection['title'] = 'AMII，开启极简时尚个人零售新时代！';
        $collection['content'] = '0投入、0风险、海量产品、高额佣金，玩游戏万件美衣免费送！';
        $collection['image'] = asset(url('/assets/images/New-A+.png'));
        // 中间数据处理
        $data = $this->dealNotIDShareDates($token, $provider, $collection);
        return $data;
    }

    /**
    *   判断后台是否应用分享到端口所有对象
    */
    protected function HasShareDefault($provider)
    {
        $default = Share::where('provider', $provider)
            ->where('is_shareAll', 1)
            ->where('is_default', 1)
            ->orderby('created_at', 'desc')
            ->first();
        return  $default;
    }

    /**
    *  有分享对象的provider【1，2，3，7】的方法中间数据处理
    *  @param $token 用户token
    *  @param $provider 分享端口
    *  @param $collection  对象的集合
    *  @param $object_id  被分享对象id
    */
    protected function dealHasIDShareDates($token, $provider, $collection, $object_id)
    {
        // 判断是否分享都所有对象
        $is_shareAll = $this->HasShareDefault($provider);
        if ($is_shareAll) {
            $data = $this->getNotIdResShareData($provider, $collection);
        } else {
            //处理是否使用默认规则或手动设置的判断后结果
            $data = $this->getResShareDates($provider, $object_id, $collection);
        }
        $id = $collection->id;
        $title =  $data['title'];
        $content =  $data['content'];
        $image =  $data['image'];

        $uuid = $this->getUserUuid($token);

        // 判断是否游戏商品分享
        if ($provider == 7) {
            if ($uuid) {
                $mark = '&';
            } else {
                $mark = '?';
            }
            $share_url = url('wechat/game/show/'. $object_id. $uuid. $mark. 'privider=3');
        } else {
            $share_url = $collection->share_url.$uuid;
        }

        $data = $this->Shareformat($provider, $id, $title, $content, $image, $share_url);
        return $data;
    }

    /**
    *   全局分享provider【4，5，6，8, 9】的方法中间数据处理
    *  @param $token 用户token
    *  @param $provider 分享端口
    *  @param $collection  对象的集合
    *  @param $source  分享来源
    */
    protected function dealNotIDShareDates($token, $provider, $collection, $source = null)
    {
        // 判断是否使用默认文案
        $data = $this->getNotIdResShareData($provider, $collection);
        $id  = $data['id'];
        $title =  $data['title'];
        $content =  $data['content'];
        $image =  $data['image'];
        $uuid = $this->getUserUuid($token);

        // share_url处理
        switch ($provider) {
            case '4':
            case '5':
            case '6':
            case '9':
                $share_url = url('wechat/my/share'. $uuid);
                break;
            case '8':
                // provider = 8 分享游戏首页
                $share_url = url('wechat/game/').$uuid;
                break;
            case '10':
                $share_url = url('/wechat/mall/wechat-share/').$uuid;
                break;
            default:
                return false;
                break;
        }

        $data = $this->Shareformat($provider, $id, $title, $content, $image, $share_url);
        return $data;
    }

    /**
    *   处理是否使用默认规则或手动设置的判断后结果
    *  @param $provider 分享端口
    *  @param $sharedObjectId  分享对象id
    *  @param $collection      对象的集合
    */
    protected function getResShareDates($provider, $sharedObjectId, $collection)
    {

        switch ($provider) {
            case '1':
            case '7':
                $collectionTitle = $collection->product->name;
                $collectionContent = $collection->product->name;
                break;
            case '2':
                $collectionTitle = $collection->title;
                $collectionContent = $collection->subtitle;
                break;
            case '3':
                $collectionTitle = $collection->title;
                $collectionContent = $collection->description;
                break;

            default:
                return false;
                break;
        }
        $collectionid = $collection->id;
        $collectionImage = $collection->image;

        // 获取中间表字段
        $field = $this->getShareRelationfield($provider);
        // 判断是否被添加为分享对象
        $selectedShare = ShareRelation::where($field, $sharedObjectId)->first();

        if ($selectedShare) {
            // 判断是否默认标题
            $share_id = $selectedShare->share_id;
            $selectedData = Share::where('id', $share_id)->first();
            if ($selectedData->is_defaultTitle) {
                $data['title'] = $collectionTitle;
            } else {
                $data['title'] = $selectedData->title;
            }
            // 判断是否默认内容
            if ($selectedData->is_defaultContent) {
                $data['content'] =  $collectionContent;
            } else {
                $data['content'] = $selectedData->content;
            }
             // 判断是否默认图片
            if ($selectedData->is_defaultImg) {
                $data['image'] = $collectionImage;
            } else {
                $data['image'] = $selectedData->image;
            }
        } else {
            // 若没有被添加为分享对象，使用系统默认的
            $data['title'] =  $collectionTitle;
            $data['content'] = $collectionContent;
            $data['image'] = $collectionImage ;
        }
        return $data;
    }

    /**
    *   处理不需传id的选择项判断是否设置了默认分享文案【4，5，6，8, 9】和 分享到端口所有对象判断
    *  @param $provider 分享端口
    *  @param $array    定义的标题、内容、图片的数组
    */
    protected function getNotIdResShareData($provider, $collection)
    {

        switch ($provider) {
            // 单品和游戏商品
            case '1':
            case '7':
                $collectionTitle = $collection->product->name;
                $collectionContent = $collection->product->name;
                $collectionImage = $collection->image;
                break;
            // 专题
            case '2':
                $collectionTitle = $collection->title;
                $collectionContent = $collection->subtitle;
                $collectionImage = $collection->image;
                break;
            // 视频
            case '3':
                $collectionTitle = $collection->title;
                $collectionContent = $collection->description;
                $collectionImage = $collection->image;
                break;
            case '4':
            case '5':
            case '6':
            case '8':
            case '9':
            case '10':
                $collectionTitle = $collection['title'];
                $collectionContent = $collection['content'];
                $collectionImage = $collection['image'];
                break;

            default:
                return false;
                break;
        }

        $default = $this->HasShareDefault($provider);
        if ($default) {
            if ($default->is_defaultTitle) {
                $data['title'] = $collectionTitle;
            } else {
                $data['title'] = $default->title;
            }
            // 判断是否默认内容
            if ($default->is_defaultContent) {
                $data['content'] = $collectionContent;
            } else {
                $data['content'] = $default->content;
            }
             // 判断是否默认图片
            if ($default->is_defaultImg) {
                $data['image'] =  $collectionImage;
            } else {
                $data['image'] = $default->image;
            }

            $data['id'] = $default->id;
        } else {
            $data['id'] = '';
            $data['title'] = $collectionTitle;
            $data['content'] = $collectionContent;
            $data['image'] = $collectionImage;
        }
        return $data;
    }


    /**
    *   返回数据
    * @param string $provider
    * @param string $id
    * @param string $title
    * @param string $image
    * @return array [desciption]
    */
    protected function Shareformat($provider, $id, $title, $content, $image, $share_url = null)
    {
        $data['id'] = $id;
        $data['title'] = $title;
        $data['content'] = $content;
        $data['image'] = $image;
        $data['provider'] = Share::getProviderAttribute($provider);
        $data['share_url'] = $share_url;
        $data['need_login'] = $provider == 5 ? 1 : 0;
        return $data;
    }

    /**
     * 搜索商品
     * @param  [type] $key [description]
     * @param  [type] $provider_id [description]
     * @return [type]      [description]
    */
    public function search($key, $provider_id)
    {
        // 获取关联字段
        $field = $this->getShareRelationfield($provider_id);
        // 获取关联表对象字段值的集合
        $shareSelectIds = ShareRelation::with('share')
                    ->whereHas('share', function ($q) use ($provider_id) {
                        return $q->where('provider', $provider_id);
                    })
                    ->pluck($field);
        // 判断搜索端口
        switch ($provider_id) {
            case '1':
                 // 搜索provider 普通库存的 商品详情
                $data  = SellProduct::with('product')
                ->whereHas('product', function ($q1) use ($key) {
                    $q1->orWhere('name', 'like', '%'.$key.'%')
                       ->orWhere('snumber', 'like', '%'.$key.'%');
                    return $q1;
                })
                ->where('status', 1)
                ->where('provider', 1);

                break;
            case '2':
                $data  =  Subject::where('status', 1)
                    ->where(function ($q) use ($key) {
                           $q->orWhere('title', 'like', '%'.$key.'%')
                           ->orWhere('subtitle', 'like', '%'.$key.'%');
                    });
                break;
            case '3':
                $data  =  Video::where('status', 1)
                    ->where(function ($q) use ($key) {
                           $q->orWhere('title', 'like', '%'.$key.'%')
                            ->orWhere('description', 'like', '%'.$key.'%');
                    });
                break;
            case '4':
                // $provider = 'code';
                break;
            case '5':
                // $provider = 'tribe';
                break;
            case '6':
                // $provider = 'activity';
                break;
            case '7':
                $data  = userGame::with('sellProduct.product')
                    ->whereHas('sellProduct', function ($q) use ($key) {
                        $q->whereHas('product', function ($q1) use ($key) {
                            $q1->orWhere('name', 'like', '%'.$key.'%')
                               ->orWhere('snumber', 'like', '%'.$key.'%');
                                return $q1;
                        })
                            ->where('status', 1)
                            ->where('provider', 3);
                        return $q;
                    });

                break;
            default:
                return false;
                break;
        }

        $arr = (clone $data)->whereNotIn('id', $shareSelectIds)
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.adminPaginate'));
        return $arr;
    }


    // 判断provider 对应 shareRelation 表中的字段
    protected function getShareRelationfield($provider_id)
    {
        switch ($provider_id) {
            case '1':
                $field = 'sell_product_id';
                break;
            case '2':
                $field = 'subject_id';
                break;
            case '3':
                $field = 'video_id';
                break;
            // case '4':
            //     $field = 'code_id';
            //     break;
            // case '5':
            //     $field = 'tribe_id';
            //     break;
            // case '6':
            //     $field = 'activity_id';
            //     break;
            case '7':
                $field = 'puzzle_game_id';
                break;
            // case '8':
            //     break;
            // case '9':
            //     break;
            default:
                return false;
                break;
        }
        return $field;
    }
}
