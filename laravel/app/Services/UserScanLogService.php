<?php
namespace App\Services;

use App\Models\UserScanLog;
use App\Models\SellProduct;

class UserScanLogService
{
    /**
     * 新增浏览
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {
        $sellProduct = SellProduct::find($arr['sell_product_id']);
        if (!$sellProduct) {
            return false;
        }
        $userScanLog = UserScanLog::where($arr)
            ->first();
        if ($userScanLog) {
            return $userScanLog->touch(); //update update timestamp
        }
        return UserScanLog::create($arr);

    }

    /**
     * 删除收藏
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        return UserScanLog::destroy($id);
        
    }

    /**
     * 删除多个
     * @param  [type] $ids [description]
     * @return [type]      [description]
     */
    public function deleteAll($ids)
    {
        return UserScanLog::destroy($ids);
    }

     /**
     * 获取收藏夹信息
     * @return collection
     */
    public function show($user_id)
    {
        return UserScanLog::where('user_id', $user_id)
            ->orderBy('updated_at', 'desc')
            ->with('sellProduct.product')
            ->whereHas('sellProduct', function ($q1) {
                return $q1->where('status', 1);
            })
            ->paginate(config('amii.apiPaginate'));
    }
}
