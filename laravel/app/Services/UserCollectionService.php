<?php
namespace App\Services;

use App\Models\UserCollection;
use App\Models\SellProduct;
use DB;

class UserCollectionService
{
    /**
     * 新增收藏
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr, $stores = 1)
    {
        $userCollection = UserCollection::where($arr)
            ->first();

        $sellProduct = SellProduct::where('id', $arr['sell_product_id'])
            ->value('status');
      
        if ($userCollection || $sellProduct == 0) {
            if ($stores == 'stores') {
                return true;
            }
            return false;
        }
        return UserCollection::create($arr);

    }

    /**
     * 批量收藏
     * @param  [type] $ids     [description]
     * @param  [type] $user_id [description]
     * @return [type]          [description]
     */
    public function stores($arr, $user_id)
    {
        $collection['user_id'] = $user_id;
        DB::beginTransaction();
        foreach ($arr as $key => $value) {
            $collection['sell_product_id'] = $value;
            $create = $this->store($collection, 'stores');
            if (!$create) {
                DB::rollback();
                return false;
            }
        }
        DB::commit();
        return true;
    }

    /**
     * 删除收藏
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        return UserCollection::destroy($id);
        
    }

    /**
     * 删除多个
     * @param  [type] $ids [description]
     * @return [type]      [description]
     */
    public function deleteAll($ids)
    {
        return UserCollection::destroy($ids);
    }

     /**
     * 获取普通商品收藏夹信息
     * @return collection
     */
    public function show($userId)
    {
        return UserCollection::where('user_id', $userId)
            ->where('provider', 1)
            ->orderBy('created_at', 'desc')
            ->with('sellProduct.product')
            ->whereHas('sellProduct', function ($q1) {
                return $q1->where('status', 1);
            })
            ->paginate(config('amii.apiPaginate'));
    }


    /**
     * 游戏商品
     * @return [type] [description]
     */
    public function gameIndex($userId)
    {
        return UserCollection::where('user_id', $userId)
            ->where('provider', 2)
            ->orderBy('created_at', 'desc')
            ->with('sellProduct.product')
            ->whereHas('sellProduct', function ($q1) {
                return $q1->where('status', 1);
            })
            ->paginate(config('amii.apiPaginate'));
    }
}
