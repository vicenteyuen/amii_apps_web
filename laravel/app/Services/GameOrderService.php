<?php

namespace App\Services;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Helpers\OrderJobHelper;
use App\Models\OrderProduct;
use App\Helpers\AmiiHelper;
use App\Models\SellProduct;
use Ecommerce\Models\Inventory;
use App\Models\UserGame;
use App\Models\Order;
use Carbon\Carbon;
use App\Models\Activity;

class GameOrderService
{
    /**
     * 参加游戏，创建游戏订单
     */
    public function store($userId, Request $request)
    {
        // 检查是否是禁用户
        $checkUser = app('user')->checkUser(auth()->id());
        if (!$checkUser) {
            return app('jsend')->error('经系统检测，您邀请好友注册获赠积分中存在操作异常情况。本平台根据用户注册协议现暂时冻结您拼图送衣游戏的参与权');
        }
        if (! config('amii.activity.game.status')) {
            return app('jsend')->error('亲，今天发起人数超过上限，明天再来吧~');
        }
        $validator = Validator::make($request->all(), [
            'inventoryId' => 'required|integer',
            'number' => 'integer|size:1',
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }
        $userId = auth()->id();
        if (!auth()->user()->phone) {
            return app('jsend')->error('请先绑定手机再参加游戏~');
        }
        // 一个用户一天最多发起三次
        $lessThree = self::checkTimesLessThree($userId);
        if ($lessThree <= 0) {
            return app('jsend')->error('每天最多可以发起3次,今天次数已用完,请明天来哈~');
        }

        //检查当前是否有正在进行的拼图
        $exitsPlaying = self::checkUserPlaying($userId);
        if (!$exitsPlaying) {
            return app('jsend')->error('亲还有未完成的游戏,请完成后再发起新游戏哦~');
        }
        DB::beginTransaction();

        // 当前库存是否为游戏商品
        $sellProductId = Inventory::where('id', $request->inventoryId)
            ->whereHas('sellproduct', function ($q1) {
                return $q1->where('status', 1)
                        ->where('provider', 3);
            })->value('product_sell_id');

        if (!$sellProductId) {
            DB::rollBack();
            return app('jsend')->error('游戏商品错误,提交订单失败');
        }

        // 库存校验并处理
        $checkPointInventories = app('order')->inventoryCheck([
            [
                'id' => intval($request->inventoryId),
                'number' => intval($request->number ? : 1),
            ]
        ]);

        $sellProduct = SellProduct::where('id', $sellProductId)
            ->first();

        if (! $checkPointInventories) {
            DB::rollBack();
            return app('jsend')->error('库存不足');
        }

        // 生成订单
        $createOrder = self::createOrder($userId, $checkPointInventories);
        if (! $createOrder) {
            DB::rollBack();
            return app('jsend')->error('提交订单失败');
        }
         // 加入游戏
        $gameProduct = app('amii.user.game')->store($sellProduct);
        if (!$gameProduct) {
            DB::rollback();
            return app('jsend')->error('提交订单失败');
        }
        $gameProduct->order_id = Order::where('order_sn', $createOrder)->value('id');
        $save = $gameProduct->save();

        if (!$save) {
            DB::rollback();
            return app('jsend')->error('提交订单失败');
        }
        // 减库存
        Inventory::where('id', $request->inventoryId)
            ->decrement('number', 1);
        SellProduct::where('id', $sellProductId)
            ->decrement('number', 1);

        // 添加订单一天后未成功退回库存任
        // OrderJobHelper::gameUnconfirm($createOrder);

        DB::commit();
        return app('jsend')->success(['orderSn' => $createOrder,'user_game_id' => $gameProduct->id]);
    }

    /**
     * 游戏商品提交订单
     */
    public function commitOrder($userId, $request)
    {
        if (! config('amii.activity.game.status')) {
            return app('jsend')->error('亲，今天美衣已送完，请明天及时合成获取美衣哦~');
        }
        $validator = Validator::make($request->all(), [
            'orderSn' => 'required|integer',
            'addrId' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return app('jsend')->error('参数错误');
        }
        //检验地址
        $orderAddrArr = self::orderAddr($userId, $request->addrId);
        if (! $orderAddrArr) {
            DB::rollBack();
            return app('jsend')->error('订单地址,提交订单失败');
        }

        $order = Order::where('order_sn', $request->orderSn)
            ->where('user_id', $userId)
            ->first();
        if (!$order) {
            return app('jsend')->error('订单出错');
        }

        //当前合成时间是否超过三天
        $orderStatus = UserGame::where('user_id', $userId)
            ->where('order_id', $order->id)
            ->value('order_status');
        if ($orderStatus == 2) {
            return app('jsend')->error('合成美衣失败,您未在120小时内合成');
        }

        // 检查当前拼图送衣是否超过三百件
        $over = self::checkFinishedGame();
        if (!$over) {
            $game = UserGame::where('order_id', $order->id)
                    ->first();
            // 过期时间加上24小时
            $addTimeCheck = (carbon::now()->timestamp - strtotime($game->add_expired_time))/60/60;
            if (is_null($game->add_expired_time) || $addTimeCheck > 24) {
                $game->expired_time = $game->expired_time + 24 * 60 * 60;
                $game->order_remain_time = $game->getOriginal('order_remain_time') + 24 * 60 * 60;
                $game->add_expired_time = Carbon::now();
                $game->save();
            }
            $sendNumber = Activity::where('provider', 'game')
                ->value('rule')?:300;
            return app('jsend')->error("今天已送出".$sendNumber."件美衣,请明天再合成美衣");
        }
        DB::beginTransaction();
        $order->shipping_region = $orderAddrArr['region'];
        $order->shipping_address = $orderAddrArr['address'];
        $order->order_status = 1;
        $order->pay_status = 1;
        $order->shipping_status = 0;
        $order->can_refund = 0;
        $order->order_start_time = Carbon::now();
        $order->order_end_time = Carbon::now();
        $order->pay_start_time = Carbon::now();
        $order->pay_end_time = Carbon::now();
        $order->incrementVolume();
        $game = UserGame::where('order_id', $order->id)
            ->first();
        $game->order_status = 1;

        $sellProduct = SellProduct::where('id', $game->sell_product_id)
            ->first();
        $sellProduct->game_number--; //正在玩的游戏人数减1
        $sellProduct->success_number++; //完成的游戏人数加1
        $sellProduct->save();
        $orderSave = $order->save();
        $gameSave = $game->save();
        if ($orderSave && $gameSave) {
            DB::commit();
            return app('jsend')->success();
        }
        DB::rollback();
        return app('jsend')->error('fail');
    }

    // 已完成的订单
    public function gameOrder()
    {
        $userId = auth()->id();
        $orderIds = UserGame::where('success', 1)
            ->where('user_id', $userId)
            ->pluck('order_id')
            ->toArray();

        // 待发货,待收货，待评价
        return Order::whereIn('id', $orderIds)
            ->where('order_status', 1)
            ->where('pay_status', 1)
            ->where(function ($q1) {
                return $q1->orWhere('shipping_status', 0)
                ->orWhere('shipping_status', 1)
                ->orWhere(function ($q2) {
                    return $q2->where('shipping_status', 2)
                        ->where('is_appraise', 0);
                });
            })
            ->with('orderProducts.inventory.sku.skuValue.attribute')
            ->with(['orderProducts' => function ($q1) {
                $q1->with(['sellProduct' => function ($q2) {
                    $q2->with(['product' => function ($q3) {
                        $q3->orWhere('status', 0);
                    }]);
                }]);
            }])
            ->orderBy('created_at', 'desc')
            ->paginate(config('api.apiPaginate'));
    }

    /**
     * 订单地址
     */
    private function orderAddr($userId, $addrId)
    {
        // 获取用户收货地址信息
        $addr = app('address')->addrOne($userId, $addrId);
        if (! $addr) {
            return false;
        }

        $addrStr = '';
        if (isset($addr->address['province'])) {
            $addrStr .= $addr->address['province']->name . ' ';
        }
        if (isset($addr->address['city'])) {
            $addrStr .=  $addr->address['city']->name . ' ';
        }
        if (isset($addr->address['area'])) {
            $addrStr .=  $addr->address['area']->name . ' ';
        }
        if ($addr->detail) {
            $addrStr .= $addr->detail;
        }
        return [
            'region' => $addr->region_id,
            'address' => [
                'post_code' => $addr->post_code,
                'name' => $addr->realname,
                'phone' => $addr->phone,
                'province' => isset($addr->address['province']->name) ? $addr->address['province']->name : '',
                'city' => isset($addr->address['city']->name) ? $addr->address['city']->name : '',
                'area' => isset($addr->address['area']->name) ? $addr->address['area']->name : '',
                'detail' => $addr->detail,
                'address' => $addrStr
            ]
        ];
    }

    /**
     * 检查次数是否小于三次
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function checkTimesLessThree($userId)
    {
        $hasExits = UserGame::where('user_id', $userId)
            ->where('created_at', '>=', Carbon::today())
            ->where('created_at', '<=', Carbon::today()->addDay())
            ->select('id')
            ->get();
        $number = 3; //一天可玩的次数
        $number = $number - count($hasExits);
        if ($number < 0) {
            $number = 0;
        }
        return $number;
    }

    /**
     * 检查用户是否有正在进行的拼图送衣
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function checkUserPlaying($userId)
    {
        $exitsPlaying = UserGame::where('user_id', $userId)
            ->where('expired', 0)
            ->where('success', 0)
            ->first();
        if ($exitsPlaying) {
            return false;
        }
        return true;
    }

    /**
     * 检查今天完成的次数是否超过300
     * @param  [type] $sell_product_id [description]
     * @return [type]                  [description]
     */
    public function checkFinishedGame()
    {
        $dt = Carbon::now();
        $start = $dt->startOfDay()->toDateTimeString();
        $number = UserGame::where('success', 1)
            ->where('order_status', 1)
            ->where('updated_at', '>=', $start)
            ->count();
        $sendNumber = Activity::where('provider', 'game')
            ->value('rule')?:300;
        if ($number < $sendNumber) {
            return true;
        }
        return false;
    }

    // 创建订单
    public function createOrder($userId, array $orderProducts, $orderProvider = 'game', $cartIds = [])
    {
        $productAmount = 0;
        foreach ($orderProducts as $orderProduct) {
            $productAmount += $orderProduct['number'] * $orderProduct['price'];
        }

        // create order
        $createData = [
            'user_id'           => $userId,
            'order_sn'          => AmiiHelper::generateOrderSn($userId),
            'product_amount'    => $productAmount,
            'provider'          => $orderProvider,
            'order_start_time'  => Carbon::now(),
            'order_status'      => 1,
        ];
        if (! empty($cartIds)) {
            $createData['cart_ids'] = $cartIds;
        }
        if ($orderProvider == 'point') {
            $createData['can_refund'] = 1;
        }
        $create = Order::create($createData);
        $orderId = $create->id;

        // store order products
        $orderProducts = array_map(function ($orderProduct) use ($orderId) {
            $mergeArr = [
                'product_provider' => 1,
                'order_id' => $orderId
            ];
            $tmpArr = $orderProduct;
            return array_merge($tmpArr, $mergeArr);
        }, $orderProducts);

        OrderProduct::insert($orderProducts);
        return $create->order_sn;
    }
}
