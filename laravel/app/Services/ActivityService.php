<?php

namespace App\Services;

use App\Helpers\ActivityHelper;
use App\Models\FirstPurchase;
use App\Models\Order;
use App\Models\Activity;

class ActivityService
{
    /**
     * 获取活动
     */
    public function getActivity($provider)
    {
        $activity = ActivityHelper::getActivity($provider);
        return $activity;
    }

    /**
     * 校验是否展示首单信息
     */
    public function isFirstOrder()
    {
        $userId = auth()->id();

        return app('amii.give.product')->checkFirstProduct(0, $userId, 0, false);
    }

    public function getActivityByProvider($provider)
    {
        return Activity::where('provider', $provider)
            ->first();
    }

    public function update($provider, $arr)
    {
        return Activity::where('provider', $provider)
            ->update($arr);
    }
}
