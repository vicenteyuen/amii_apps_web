<?php

namespace App\Services;

use App\Models\PointReceived;
use App\Models\PointLevel;
use App\Models\User;
use Carbon\Carbon;
use DB;
use App\Jobs\CheckUserShoppingPointJob;

class PointReceivedService
{
    protected $userId;

    public function __construct()
    {
        $this->userId = auth()->id();
    }

    //新增
    public function store($arr)
    {
        return PointReceived::create($arr);
    }


    //积分明细列表
    public function index()
    {
        return PointReceived::where('user_id', $this->userId)
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.apiPaginate'));
    }

     //积分明细列表
    public function userIndex($userId)
    {
        return PointReceived::where('user_id', $userId)
            ->orderBy('updated_at', 'desc')
            ->paginate(config('amii.adminPaginate'));
    }

    /**
     * 增加用户积分
     * @param  string  $type       类别
     * @param  string  $typeChild  子类别
     * @param  declima $purchase   购买商品总额
     * @return bool
     */
    public function userAddPoint($type, $typeChild, $purchase = null, $userId = null, $resource = null)
    {
        $type = strtolower($type);
        $typeChild = strtolower($typeChild);
        $this->userId = $this->userId ?:$userId; //注册
        $select  = ['user_id' => $this->userId,'type' => $type,'type_child' => $typeChild];
        switch ($type) {
            //注册
            case 'register':
                if ($typeChild == 'register') {
                    $points = 500;
                    return $this->userUpdatePoint($select, $points);
                } else if ($typeChild == 'bund') {
                    // 注册
                    $select['user_id'] = $userId;
                    $select['type_child'] = 'register';
                    $this->userId = $userId;
                    $points = 500;
                    $this->userUpdatePoint($select, $points);
                    // 绑定帐号
                    $select['type'] = 'update';
                    $select['type_child'] = $purchase;
                    $points = 10;
                    $this->userUpdatePoint($select, $points);
                }
                return false;

            //个人资料更新
            case 'update':
                if (in_array($typeChild, ['birthday','address','gender'], true)) {
                    $points = 5;
                } else if (in_array($typeChild, ['wechat','qq','weibo'], true)) {
                    $points = 10;
                } else {
                    return false;
                }
                return $this->userUpdatePoint($select, $points);
            //形象管理
            case 'looking':
                switch ($typeChild) {
                    case 'height':
                        $points = 5;
                        break;
                    case 'weight':
                        $points = 5;
                        break;
                    case 'bust':
                        $points = 5;
                        break;
                    case 'waist':
                        $points = 5;
                        break;
                    case 'hips':
                        $points = 5;
                        break;
                    case 'shoulder_width':
                        $points = 5;
                        break;
                    case 'thigh_width':
                        $points = 5;
                        break;
                    case 'leg_width':
                        $points = 5;
                        break;
                    case 'skin':
                        $points = 5;
                        break;
                    case 'hair':
                        $points = 5;
                        break;
                    case 'up_size':
                        $points = 5;
                        break;
                    case 'lower_size':
                        $points = 5;
                        break;
                    case 'front_photo':
                        $points = 25;
                        break;
                    case 'side_photo':
                        $points = 25;
                        break;
                    default:
                        return false;
                }
                return $this->userUpdatePoint($select, $points);
            //邀请
            case 'invite':
                // 检查是否是禁用户
                $checkUser = app('user')->checkUser($userId);
                if (!$checkUser) {
                    return false;
                }
                if ($typeChild == 'invite') {
                    $select['user_id'] = $userId;
                    $points = 200;
                    return $this->handPoint($select, $points);
                }
                return false;

            //购物
            case 'shopping':
                if ($typeChild == 'shopping') {
                    $points = intval($purchase);
                    $select['resource'] = $resource;
                    // 判断是否已送购物积分
                    $exist = PointReceived::where('type', $select['type'])
                        ->where('type_child', $select['type_child'])
                        ->where('user_id', $select['user_id'])
                        ->where('resource', $resource)
                        ->first();
                    if ($exist) {
                        return false;
                    }
                    return $this->handPoint($select, $points);
                }
                return false;

            //天数达标
            case 'received_day':
                if ($typeChild == 'received_day') {
                    $points = 5;
                    return $this->signPoint($select, $points);
                }
                return false;

            //评论
            case 'appraising':
                if ($typeChild == 'normal') {
                    $points = 5;
                } else if ($typeChild == 'image') {
                    $points = 10;
                } else {
                    return false;
                }
                return $this->handPoint($select, $points);

            //图文专区参与
            case 'image_appraising':
                if ($typeChild == 'mark') {
                    $points = 5;
                } else if ($typeChild == 'comment') {
                    $points = 5;
                } else {
                    return false;
                }
                return $this->sharePoint($select, $points);

            case 'share':
                if (in_array($typeChild, ['product','puzzle','subject','video','tribe','activity','puzzle_index','home'], true)) {
                    $points = 10;
                } else if (in_array($typeChild, ['person_comment'], true)) {
                    $points = 15;
                } else if (in_array($typeChild, ['subject_mark'], true)) {
                    $points = 5;
                } else {
                    return false;
                }
                return $this->sharePoint($select, $points);

            //活动 二期
            case 'activity':
                if ($typeChild == 'lottery') {
                    return $this->lotteryActivity();
                } else if (in_array($typeChild, ['show','collocation'], true)) {
                    return $this->markActivity();
                } else if ($typeChild == 'game') {
                    $points = 20;
                    return $this->handPoint($select, $points);
                } else {
                    return false;
                }

           //用户反馈
            case 'advice':
                if (in_array($typeChild, ['advice','game'])) {
                    $select['user_id'] = $userId;
                    $points = 200;
                    return $this->handPoint($select, $points);
                } else if (in_array($typeChild, ['useradvice','gameadvice'])) {
                    $points = 5;
                    return $this->advicePoint($select, $points);
                }
                return false;

            //消费花去积分
            case 'consumer':
                if ($typeChild == 'consumer') {
                    $points = $purchase;
                    $select['user_id'] = $userId;
                    return $this->decrementPoint($select, $points);
                }
                break;
            //退回积分
            case 'reback':
                if ($typeChild == 'points') {
                    $points = $purchase;
                    $select['user_id'] = $userId;
                    return $this->handPoint($select, $points, 'reback');
                }
                break;
            default:
                return false;
        }
    }


    // hand
    public function handPoint($select, $points, $elseOption = null)
    {
        // 最终获得积分
        $res_points = $points;
        DB::beginTransaction();
        $select['points'] = $points;
        $create         = PointReceived::create($select);
        $user           = User::where('id', $select['user_id']);
        $points         = $user->increment('points', $points);
        $updateLevel    = true;
        //退回积分不计入总积分
        if ($elseOption == 'reback') {
            $sum = true;
        } else {
            $sum = $user->increment('sum_points', $res_points);
        }
        $level = app('pointLevel')->points($user->first()->sum_points, $select['user_id']);
        if (!$create || !$level) {
            DB::rollback();
            return false;
        }
        // 更新用户等级
        if ($level != $user->first()->level) {
            $updateLevel = User::where('id', $select['user_id'])
                ->update(['level' => $level]);
        }

        // 检测是否发生并发
        $where = [
            'user_id'    => $create->user_id,
            'type'       => $create->type,
            'type_child' => $create->type_child,
            'created_at' => $create->created_at,
            'updated_at' => $create->updated_at,
        ];
        // 同一用户并发
        if (self::checkConcurrent($where)) {
            DB::rollback();
            return false;
        }
        if ($create &&$sum && $points && $updateLevel) {
            DB::commit();
            // 发送消息推送
            $this->notifyMsg($select, $res_points);
            return $res_points;
        }
        DB::rollback();
        return false;

    }

    /**
    * 生成系统通知消息，并短信推送消息
    * @param array     $select    [user_id, type, type_child]
    * @param number    $points    获取的积分
    */
    public function notifyMsg($select, $points)
    {
        $way = $this->getWay($select['type'], $select['type_child']);
        $arr = [
            'user_id' => $select['user_id'],
            'number'  => $points,
            'way'     => $way,
        ];
        // 保存数据，并发送消息推送
        app('amii.notify.message')->sendNotifyMsg('notify', 'points', $arr);
    }

    //更新个人数据  形象管理
    public function userUpdatePoint($select, $points)
    {
        $hasExits = PointReceived::where($select)
            ->first();
        if ($hasExits) {
            return false;
        }

        return $this->handPoint($select, $points);
    }

    //天数达标(签到)
    public function signPoint($select, $points)
    {
        $points = 5;
        $today = strtotime('today');
        $signReceived = PointReceived::where($select)
            ->orderBy('created_at', 'desc')
            ->take(1)
            ->get();

        if (isset($signReceived[0]['createdtime'])) {
            if ($signReceived[0]['createdtime'] - $today > 0) {
                 return false;   //今天已签到
            }
        }
        $createdArr = $signReceived->pluck('createdtime')->all();
        foreach ($createdArr as $key => $value) {
            $timeStart   = $today - 24 * ($key + 1) * 60 * 60;
            $timeEnd     = $timeStart + 24 * 60 * 60;
            if ($value >= $timeStart && $value <= $timeEnd) {
                 $points = $points +5;
                 // 最多10积分
                if ($points >= 10) {
                    $points = 10;
                }
            } else {
                break;
            }
        }
        return $this->handPoint($select, $points);
    }

    // 反馈积分
    public function advicePoint($select, $points)
    {
        $thisMonthStart =  strtotime(date('Y-m-01', time()));          // 本月第一天
        $thisMonthEnd =  mktime(0, 0, 0, date("m")+1, 1, date("Y"))-1;

        $sumPoints = PointReceived::where($select)
            ->whereBetween(\DB::raw('unix_timestamp(created_at)'), [$thisMonthStart,$thisMonthEnd])
            ->sum('points');
        if ($sumPoints < 50) {
            return $this->handPoint($select, $points);
        }
        return true;
    }

    // 连续签到天数
    public static function continuteSign($userId)
    {
        $day = 0;
        $today = strtotime('today');
        $signReceived = PointReceived::where('user_id', $userId)
            ->where('type', 'received_day')
            ->where('type_child', 'received_day')
            ->orderBy('created_at', 'desc')
            ->get();
        $createdArr = $signReceived->pluck('createdtime')->all();
        foreach ($createdArr as $key => $value) {
            $timeStart   = $today - 24 * $key * 60 * 60;
            $timeEnd     = $timeStart + 24 * 60 * 60;
            if ($value >= $timeStart && $value <= $timeEnd) {
                 $day++;
            } else {
                break;
            }
        }
        return $day;

    }

    //图文专区参与
    public function sharePoint($select, $points)
    {
        $received  = PointReceived::where('type', $select['type'])
            ->where('user_id', $select['user_id'])
            ->where('created_at', '>=', Carbon::today())
            ->where('created_at', '<=', Carbon::today()->addDay())
            ->get();
        if (count($received) <= 4) {
            return $this->handPoint($select, $points);
        }
        return false;
    }

    // 消费花去积分
    public function decrementPoint($select, $points)
    {
        $select['points'] = $points;
        $create = PointReceived::create($select);
        $user = User::find($select['user_id']);
        if ($user->points < $points) {
            return false;
        }
        $decrement = User::where('id', $select['user_id'])
            ->decrement('points', $points);
        if ($create  && $decrement) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    //活动投资 投资 二期
    public function lotteryActivity()
    {
        return false;
    }

    //活动投资 买家秀 每周搭配 二期
    public function markActivity()
    {
        return false;
    }

    // 是否发生并发
    protected static function checkConcurrent($where)
    {
        $count = PointReceived::where($where)
            ->get();
        if (count($count)>= 2) {
            return true;
        }
        return false;
    }

    // 获取订单积分
    public function receivedOrder($orderSn)
    {
        $order = PointReceived::where('user_id', $this->userId)
            ->where('status', 0)
            ->where('resource', $orderSn)
            ->first();
        if ($order) {
            $order->status = 1;
            $order->save();
            return (int)$order->points;
        }
        return false;
    }

    //判断获得积分途径
    public function getWay($type, $type_child)
    {
        switch ($type) {
            //注册
            case 'register':
                if ($type_child == 'register') {
                    return '注册';
                }
                return '注册';

            //个人资料更新
            case 'update':
                if (in_array($type_child, ['phone','birthday','address','gender'], true)) {
                    return  '个人信息';
                } else if ($type_child == 'wechat') {
                    return '绑定微信';
                } else if ($type_child == 'qq') {
                    return '绑定QQ';
                } else if ($type_child == 'weibo') {
                    return '绑定微博';
                } else {
                    return '更新个人资料';
                }

            //形象管理
            case 'looking':
                switch ($type_child) {
                    case 'height':
                        return '完善身高尺寸';
                    case 'weight':
                        return '完善体重';
                    case 'bust':
                        return '完善胸围尺寸';
                    case 'waist':
                        return '完善腰围尺寸';
                    case 'hips':
                        return '完善臀围尺寸';
                    case 'shoulder_width':
                        return '完善肩宽尺寸';
                    case 'thigh_width':
                        return '完善大腿围尺寸';
                    case 'skin':
                        return '完善肤色';
                    case 'leg_width':
                        return '完善小腿围';
                    case 'hair':
                        return '完善发色';
                    case 'up_size':
                        return '回答AMII上装问题';
                    case 'lower_size':
                        return '回答AMII下装问题';
                    case 'front_photo':
                        return '上传正面照';
                    case 'side_photo':
                        return '上传侧面照';
                        break;
                    default:
                        return '形象管理';
                }

            //邀请
            case 'invite':
                if ($type_child == 'invite') {
                    return '邀请';
                }
                return '邀请';

            //购物
            case 'shopping':
                if ($type_child == 'shopping') {
                    return '购物';
                }
                return '购物';

            //天数达标
            case 'received_day':
                if ($type_child == 'received_day') {
                    return '签到';
                }
                return '签到';

            //评论
            case 'appraising':
                if ($type_child == 'normal') {
                    return '普通评论';
                } else if ($type_child == 'image') {
                    return '图片评论';
                } else {
                    return '评论';
                }

            //图文专区参与
            case 'image_appraising':
                if ($type_child == 'mark') {
                    return '点赞';
                } else if ($type_child == 'comment') {
                    return '评论';
                } else {
                    return '参与图文专区活动';
                }

            //分享 二期
            case 'share':
                switch ($type_child) {
                    case 'product':
                        return '分享商品';
                        break;
                    case 'puzzle':
                        return '分享游戏';
                    case 'puzzle_index':
                        return '分享游戏首页';
                        break;
                    case 'person_comment':
                        return '';
                        break;
                    case 'subject':
                        return '分享专题';
                    case 'video':
                        return '分享视频';
                    case 'activity':
                        return '分享首单七折购';
                    case 'tribe':
                        return '分享分佣规则';
                    case 'subject_mark':
                        return '专题点赞';
                    case 'home':
                        return '分享首页';
                    break;
                    default:
                        return '';
                        break;
                }


            //活动 二期
            case 'activity':
                if ($type_child == 'lottery') {
                    return '活动';
                } else if (in_array($type_child, ['show','collocation'], true)) {
                    return '活动';
                } else if ($type_child == 'game') {
                    return $this->handPoint($select, $points);
                } else {
                    return '活动';
                }

           //客户建议
            case 'advice':
                if ($type_child == 'advice') {
                    return '用户反馈采纳';
                } else if ($type_child == 'game') {
                     return '游戏反馈采纳';
                } else if ($type_child == 'useradvice') {
                     return '用户反馈';
                }
                return '用户反馈';

            //消费花去积分
            case 'consumer':
                if ($type_child == 'consumer') {
                    return '消费';
                }
                return '消费';

            //退回积分
            case 'reback':
                if ($type_child == 'points') {
                    return 'reback';
                }
                return 'reback';
            default:
                return '';
        }
    }

    public function checkShoppingPoint($id = 0)
    {
        $checkUser = PointReceived::where('type', 'shopping')
            ->whereHas('order', function ($q1) {
                return $q1->where('provider', 'point');
            })
            ->where('id', '>', $id)
            ->first();

        if ($checkUser) {
            $user = User::find($checkUser->user_id);
            $user->points = $user->points - $checkUser->points;
            $user->sum_points = $user->sum_points - $checkUser->points;
            if ($user->points < 0) {
                $user->points = 0;
            }
            if ($user->sum_points < 0) {
                $user->sum_points = 0;
            }
            if ($user->points > $user->sum_points) {
                $user->points = $user_sum_points;
            }

            $checkUserId = $checkUser->id;
            $checkUser->delete();
            $user->save();
            // 加入队列校验下一个等级
            $job = new CheckUserShoppingPointJob($checkUserId);
            dispatch($job);
        }
        return;

    }
}
