<?php

namespace App\Services;

use App\Models\UserQuestion;

class UserQuestionService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        return UserQuestion::create($arr);
    }

    /**
     * 问题列表
     * @return [type] [description]
     */
    public function index()
    {
        return UserQuestion::get();
    }

    /**
     * 删除
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return UserQuestion::destroy($id);
    }
}
