<?php
namespace App\Services;

use App\Models\FirstPurchase;
use App\Models\GiveProduct;
use App\Models\GiveProductHistory;
use App\Models\SellProduct;
use App\Models\Activity;
use App\Models\Order;
use App\Models\OrderProduct;
use DB;

class GiveProductService
{
    /**
     * 购买商品调用
     * @param  [type] $pay    实付金额
     * @param  [type] $userId
     * @return [type]
     */

    // 首单购满足金额
    protected $purchaseMoney;

    public function __construct()
    {
        $this->purchaseMoney = app('amii.fisrt.product')->purchaseMoney();
    }

    public function firstPurchase($pay, $userId, $orderId)
    {
        $check = $this->checkFirstProduct($pay, $userId, $orderId, false);
        if ($check) {
            $firstPurchase = FirstPurchase::find(1);
            $activity = Activity::where('provider', 'first')
                    ->first();
            $arr = [
                    'sell_product_id'   => $firstPurchase->sell_product_id,
                    'user_id'           => $userId,
                    'activity_id'       => $activity->id,
                    'order_id'          => $orderId,
            ];
            if ((int)$pay >= $this->purchaseMoney) {
                $arr['status'] = 1;
                return  GiveProduct::create($arr);
            }
        }
        return false;
    }

    /**
     * 成功支付回调调用
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function paySuccess($userId, $orderId)
    {
        $firstPurchase = FirstPurchase::find(1);
        $giveProduct = GiveProduct::where('user_id', $userId)
            ->where('order_id', $orderId)
            ->where('status', 1)
            ->first();
        if (!$giveProduct) {
            return false;
        }

        DB::beginTransaction();
        $giveProduct->status = 2;
        $save = $giveProduct->save();
        // 总购买数加1
        $addTotalCount = $firstPurchase->increment('count', 1);
        // 该商品购赠品购买数加1
        $addCurrentCount = GiveProductHistory::where('sell_product_id', $firstPurchase->sell_product_id)
            ->increment('count', 1);
        if ($save && $addTotalCount && $addCurrentCount) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    /**
     * 检查用户是是否满足首单购
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function checkFirstProduct($pay = 0, $userId, $orderId = 0, $needProduct = true)
    {
        // 是否开启首单购
        $firstPurchase = FirstPurchase::where('id', 1)
            ->where('status', 1)
            ->first();

        if (!$firstPurchase) {
            return false;
        }

        $order = Order::withTrashed()
            ->where('user_id', $userId)
            ->where(function ($q1) {
                return $q1->where('order_status', 1)
                    ->orWhere(function ($q2) {
                        return $q2->where('order_status', 2)
                                ->where('shipping_status', '!=', 0);
                    });
            })
            ->doesntHave('game')
            ->whereIn('provider', ['standard','point']);
        if ($orderId) {
            $order = $order->where('id', '!=', $orderId);
        }
        $order = $order->first();
        if ($order) {
            return false;
        }

        $firstPurchase = FirstPurchase::find(1);
        if (! $firstPurchase) {
            return false;
        }

        if ($orderId && $pay < $firstPurchase->money) {
            return false;
        }

        if (! $needProduct) {
            // 不返回商品信息
            return true;
        }
        // 返回商品信息
        $firstOrderProduct = $firstPurchase->load('sellProduct.product');
        $activity = Activity::where('provider', 'first')
            ->first();
        $firstOrderProduct->activity = $activity;
        return $firstOrderProduct;
    }

    //购物首单信息
    public static function checkUserFirstProduct()
    {
        $successMessage = ['text' => '首单金额的30%将在用户确认收货后，返还到其AMII余额账户。','url' => url('/wechat/mall/advertisement')];
         // 是否开启首单购
        $firstPurchase = FirstPurchase::where('id', 1)
            ->where('status', 1)
            ->first();

        if (!$firstPurchase) {
            return null;
        }

        $userId = auth()->id();
        if (!$userId) {
            return $successMessage;
        }
        $order = Order::withTrashed()
            ->where('user_id', $userId)
            ->where('order_status', '<>', 0)
            ->first();
        if ($order) {
             return null;
        }
        return $successMessage;

    }

    //删除
    public function delete($userId)
    {
        return GiveProduct::where(['user_id' =>$userId])
            ->where('status', 1)
            ->delete();
    }

    //首单商品信息
    public function order($orderId)
    {
        return GiveProduct::where('order_id', $orderId)
            ->with('sellProduct.product')
            ->first();
    }
}
