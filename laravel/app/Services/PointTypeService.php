<?php

namespace App\Services;

use App\Models\PointType;

class PointTypeService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        return PointType::create($arr);
    }

    /**
     * 删除
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return PointType::destroy($id);
    }
}
