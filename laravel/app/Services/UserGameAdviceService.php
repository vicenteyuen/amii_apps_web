<?php

namespace App\Services;

use App\Models\UserGameAdvice;

class UserGameAdviceService
{
    public function store($arr)
    {
        // 送游戏反馈积分
        app('pointReceived')->userAddPoint('advice', 'gameadvice');
        return UserGameAdvice::create($arr);
    }

   /**
    * admin 获取用户游戏反馈信息
    *
    */
    public function getUserAdvices()
    {
        $data =UserGameAdvice::with('user')
            ->orderBy('status')
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.adminPaginate'));
        return $data;
    }

    /**
     *admin 获取游戏反馈详情
     * @param  [type] $advice_id [description]
     * @return [type]                  [description]
    */
    public function getUserAdvicesDetail($advice_id)
    {
        $data =UserGameAdvice::with('user')
            ->where('id', $advice_id)
            ->first();
        return $data;
    }

    /**
     * admin 更改游戏反馈采纳状态
     * @param  [type] $advice_id [description]
     * @return [type]            [description]
    */
    public function updateAdvicesStatus($advice_id, $arr)
    {
        $data = UserGameAdvice::where('id', $advice_id)
            ->first();
        if (!$data) {
            return false;
        }
        // 赠送积分
        if ($arr['status'] == 2) {
            app('pointReceived')->userAddPoint('advice', 'game', '', $data->user_id);
        }
        return UserGameAdvice::where('id', $advice_id)
            ->update($arr);
    }
}
