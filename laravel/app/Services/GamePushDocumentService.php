<?php

namespace App\Services;

use App\Models\GamePushDocument;

class GamePushDocumentService
{
    public function index()
    {
        return GamePushDocument::get();
    }

    public function update($id, $arr)
    {
        return GamePushDocument::where('id', $id)
            ->update($arr);
    }

    public function show($id)
    {
        return GamePushDocument::find($id);
    }
}
