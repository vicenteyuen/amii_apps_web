<?php

namespace App\Services;

use App\Models\SellProduct;

class IndividService
{
    // 获取个性化推荐
    public function index($provider, $sign = '')
    {
        switch ($provider) {
            case 'cart':
                $cartIds = explode(',', $sign);
                $data = self::cartIndivid($cartIds);
                break;

            default:
                $data = self::productIndivid($sign);
                break;
        }

        return $data;
    }

    /**
     * 获取购物车个性化推荐
     */
    public function cartIndivid(array $ids)
    {
        return SellProduct::orderBy('id', 'desc')
            ->where('provider', 1)
            ->where('status', 1)
            ->whereHas('product', function($query){
                $query->where('status', 1);
            })
            ->with('product')
            ->paginate(config('amii.apiPaginate'));
    }

    /**
     * 根据商品获取推荐
     */
    public function productIndivid($id)
    {
        return SellProduct::orderBy('id', 'desc')
            ->where('provider', 1)
            ->where('status', 1)
            ->whereHas('product', function($query){
                $query->where('status', 1);
            })
            ->with('product')
            ->paginate(config('amii.apiPaginate'));
    }
}
