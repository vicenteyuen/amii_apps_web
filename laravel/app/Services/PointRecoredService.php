<?php

namespace App\Services;

use App\Models\PointRecored;

class PointRecoredService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr)
    {
        return PointRecored::create($arr);
    }

    /**
     * 删除
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return PointRecored::destroy($id);
    }

    /**
     * 积分明细列表
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function index($id)
    {
        return PointRecored::where('user_id', $id)
            ->orderBy('created_at')
            ->paginate(config('amii.apiPaginate'));
    }
}
