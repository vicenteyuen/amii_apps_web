<?php

namespace App\Services;

use App\Models\Video;
use App\Models\VideoLike;

class VideoService
{
    public function homeIndex()
    {
        $new_videos = Video::orderBy('created_at', 'desc')
            ->orderBy('weight', 'desc')
            ->where('status', 1)
            ->take(4)
            ->get();
        $hot_videos = Video::orderBy('likes', 'desc')
            ->orderBy('weight', 'desc')
            ->where('status', 1)
            ->take(4)
            ->get();

        return compact('new_videos', 'hot_videos');
    }

    public function hotIndex()
    {
        $videos = Video::orderBy('likes', 'desc')
            ->orderBy('weight', 'desc')
            ->where('status', 1)
            ->paginate();

        return ['videos' => $videos];
    }

    public function newIndex()
    {
        $videos = Video::orderBy('created_at', 'desc')
            ->orderBy('weight', 'desc')
            ->where('status', 1)
            ->paginate();

        return compact('videos');
    }

    public function index()
    {
        return Video::where('status', 1)
            ->orderBy('weight', 'desc')
            ->paginate();
    }

    public function adminIndex()
    {
        return Video::orderBy('weight', 'desc')
            ->paginate();
    }

    public function find($id)
    {
        return Video::find($id);
    }

    public function store($arr)
    {
        return Video::create($arr);
    }

    public function update($id, array $arr)
    {
        return Video::where('id', $id)
            ->update($arr);
    }

    public function delete($id)
    {
        return Video::destroy($id);
    }

    public function addLike($id)
    {
        $exist = VideoLike::where('video_id', $id)
            ->where('user_id', auth()->id())
            ->first();

        if (!$exist) {
            VideoLike::create([
                'video_id' => $id,
                'user_id' => auth()->id()?:0,
            ]);
            Video::where('id', $id)
                ->increment('likes');
            if (auth()->id()) {
                app('pointReceived')->userAddPoint('image_appraising', 'mark');
            }
        }
    }

    public function removeLike($id)
    {
        $result = VideoLike::where('video_id', $id)
            ->where('user_id', auth()->id())
            ->delete();
        if ($result) {
            Video::where('id', $id)
                ->decrement('likes');
        }
    }

    public function relateLike($id)
    {
        $like = VideoLike::where('user_id', auth()->id())
            ->where('video_id', $id)
            ->first();

        return empty($like) ? 0 : 1;
    }
}
