<?php

namespace App\Services;

use App\Models\BrandProduct;
use Ecommerce\Models\Product;

class BrandProductService
{
     //品牌列表
    public function index($id, $request = null)
    {
        $productIds = BrandProduct::where('brand_id', $id)
            ->get()
            ->pluck('product_id');

        $data = Product::whereIn('id', $productIds)
            ->where(function ($q1) use ($request) {
                return $q1->orWhere('name', 'like', '%'.$request->key.'%')
                    ->orWhere('snumber', 'like', '%'.$request->key.'%');
            })
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.apiPaginate'));
        if ($request) {
            $data = $data->appends($request->all());
        }
        return $data;
    }

    public function delete($brandId, $productId)
    {
        return BrandProduct::where(['brand_id'=>$brandId,'product_id' => $productId])
            ->delete();
    }

    public function deleteAll($brandId, $ids)
    {
        return BrandProduct::where('brand_id', $brandId)
            ->whereIn('product_id', $ids)
            ->delete();
    }

    public function store($arr)
    {
        return BrandProduct::updateOrCreate($arr);
    }

    public function insertBrandProAll($brandId, $productIds)
    {
        $insert = [];
        foreach ($productIds as $key => $value) {
            $insert[] = [
                'brand_id'    => $brandId,
                'product_id'  => $value
            ];
        }
        if (empty($insert)) {
            return false;
        }
        return BrandProduct::insert($insert);
    }
}
