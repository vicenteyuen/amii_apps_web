<?php

namespace App\Services;

use App\Models\PointRankAward;
use App\Models\PointReceived;
use App\Models\PointRankAwardUser;
use Carbon\Carbon;

class PointRankAwardService
{
    /**
     * 获取积分轮播广告
     */
    public function cAnounce()
    {
        $items = PointRankAward::publishedDisplay()
            ->select('id', 'name')
            ->get();
        return $items;
    }

    /**
     * 获取获奖页面数据
     */
    public function show($id)
    {
        return PointRankAward::with('users.user')
            ->publishedDisplay()
            ->where('id', $id)
            ->first();
    }

    /**
     * newObject
     */
    public function newObject($id)
    {
        if ($id) {
            return PointRankAward::with('users.user')
                ->where('id', $id)
                ->first();
        }
        return new PointRankAward;
    }

    /**
     * 获取admin积分公告
     */
    public function adminIndex()
    {
        $items = PointRankAward::orderBy('id', 'desc')
            ->paginate(config('amii.adminPaginate'));
        return $items;
    }

    /**
     * admin 创建公告
     */
    public function create($arr)
    {
        $create = PointRankAward::create($arr);
        if ($create) {
            // 结算队列
            $data = [
                'id' => $create->id,
                'from' => $create->start_time,
                'to' => $create->end_time,
                'rank' => 0,
                'position' => 1,
                'number' => $create->number
            ];
            $this->settle($data);
            return true;
        }
        return false;
    }

    /**
     * 公示公告
     */
    public function publish($id)
    {
        return PointRankAward::where('id', $id)
            ->where('status', 1)
            ->update(['is_publish' => 1]);
    }

    /**
     * 下架公告
     */
    public function unPublish($id)
    {
        return PointRankAward::where('id', $id)
            ->where('status', 1)
            ->update(['is_publish' => 0]);
    }

    /**
     * 积分排行榜结算
     */
    public function settle(array $data)
    {
        $id = $data['id'];
        $from = $data['from'] ? : Carbon::now();
        $to = $data['to'] ? : Carbon::now();
        $rank = $data['rank'];
        $position = $data['position'];
        $number = $data['number'];
        $this->ranking($id, $from, $to, $rank, $position, $number);
    }

    /**
     * 积分排名
     */
    public function ranking($id, $from, $to, $rank, $position, $number)
    {
        if ($position > $number) {
            return;
        }

        $item = PointReceived::selectRaw("user_id, sum(points) as rank, max(created_at) as ctime")
            ->where('type', '!=', 'consumer')
            ->where('type', '!=', 'reback')
            ->where('created_at', '>=', $from)
            ->where('created_at', '<=', $to)
            ->whereHas('user', function ($q) {
                return $q->where('status', 1);
            })
            ->groupBy('user_id')
            ->orderBy('rank', 'desc')
            ->orderBy('ctime')
            ->offset($rank)
            ->first();
        if (! $item && $rank === 0) {
            // 没有排名数据
            return;
        }

        // 纪录排名
        $userId = $item->user_id;
        $points = $item->rank;
        PointRankAwardUser::create([
            'point_rank_award_id' => $id,
            'user_id' => $userId,
            'placed' => $position,
            'points' => $points
        ]);

        // 已处理完成
        if ($number == $position) {
            PointRankAward::where('id', $id)
                ->update(['status' => 1]);
            return;
        }

        $arr = [
            'id' => $id,
            'from' => $from,
            'to' => $to,
            'rank' => $rank + 1,
            'position' => $position + 1,
            'number' => $number
        ];
        dispatch(new \App\Jobs\PointRankAwadsJob($arr));
        return;
    }
}
