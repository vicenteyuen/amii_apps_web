<?php
namespace App\Services;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\SellAttribute;
use App\Models\SellAttributeValue;
use DB;

class AttributeService
{
    /**
     *新增属性
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {
        if (is_null($this->checkDuplicate(['name' => $arr['name']]))) {
            return Attribute::create($arr);
        }
        return false;

    }

    /**
     * 编辑属性
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function update($arr, $id)
    {
        $attribute = $this->checkDuplicate(['name' => $arr['name']]);
        if (is_null($attribute) || $id == $attribute->id) {
            return Attribute::where('id', $id)
                    ->update($arr);
        }
        return false;

    }

    /**
     * 查重
     * @param  [type] $arr [description]
     * @return bool      [description]
     */
    public function checkDuplicate($arr)
    {
        return Attribute::where($arr)
                ->select('id')
                ->first();

    }

    /**
     *
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        
        $deleteAttribute = Attribute::destroy($id);
        $value = AttributeValue::where('attribute_id', $id)
            ->first();
                    
        if (!$value) {
            return $deleteAttribute;
        }

        DB::beginTransaction();
        $deleteValue = AttributeValue::where('attribute_id', $id)
        ->delete();
        if ($deleteAttribute && $deleteValue) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
        
    }

    /**
     * 列表数据
     * @return [type] [description]
     */
    public function index()
    {
        return Attribute::with('attributeValue')
            ->paginate(20);
    }


     // *
     // * 商品属性
     // * @param  [type] $id [description]
     // * @return [type]     [description]
     
    public function productAttribute($id)
    {
        $sellId = SellAttribute::where('product_sell_attribute_id', $id)
            ->pluck('id');
    
        $attributeValue = SellAttributeValue::whereIn('sell_attribute_id', $sellId)
            ->pluck('attribute_value_id');

        $sellAttribute = SellAttribute::where('product_sell_attribute_id', $id)
            ->with(['attribute' => function ($q1) use ($attributeValue) {
                $q1->with(['attributeValue' => function ($q2) use ($attributeValue) {
                    $q2->where(function ($q3) use ($attributeValue) {
                        foreach ($attributeValue as $value) {
                            $q3->orWhere('id', $value);
                        }
                        return $q3;
                    });
                }]);
            }])->get();
        return $sellAttribute->pluck('attribute');
    }


    /**
     * 获取所有数据
     * @return [type] [description]
     */
    public function get()
    {
        return Attribute::get();
    }

    /**
     * 查找属性
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function find($id)
    {
        return Attribute::find($id);
    }
}
