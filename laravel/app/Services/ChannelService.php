<?php
namespace App\Services;

use App\Models\Channel;
use App\Helpers\QrcodeHelper;

class ChannelService
{

    // amdminIndex
    public function adminIndex()
    {
        $data = Channel::orderBy('id')
            ->paginate(config('amii.adminPaginate'));
        return $data;
    }

    // 获取详情
    public function getChannelDeitail($id)
    {
        $data = Channel::where('id', $id)->first();
        return $data;

    }
    // 渠道更新
    public function updateChannel($id, $arr)
    {
        $update = Channel::where('id', $id)
            ->update($arr);
        if ($update) {
            return true;
        }
        return false;
    }

    /**
     * 生成二维码 for apih5/VersionController
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function qrcode($url)
    {
        $qrcode = new QrcodeHelper;
        $qrcode = $qrcode->getChannelQr($url);
        return  $qrcode;
    }

    /**
    *  保存二维码到本地文件夹
    */
    public function saveChannelQrcode($qrcodeData, $dir_path, $fileName)
    {
        // 判断目录是否存在，不存在则生成
        if (!file_exists($dir_path) ) {
            mkdir("$dir_path",0777, true);
        }
        $fileurl = $dir_path.$fileName;
        // 保存到本地,如果不存在文件，则创建新的
        file_put_contents($fileurl, $qrcodeData, FILE_USE_INCLUDE_PATH);
    }

    /**
    *保存新添
    *@param   $data array
    *@return  type  [description]
    */
    public function store($data)
    {
        return  Channel::create($data);
    }

    // 更新下载量
    public function update($provider)
    {
        $num = Channel::where('code', $provider)->value('downloads');
        $num += 1;
        return  Channel::where('code', $provider)
            ->update([
                'downloads' => $num
            ]);
    }

    //生成唯一的16位随机字符串
    public function generateCode()
    {
        $arr = array_merge( range('a', 'z'), range('0', '9') );
        shuffle($arr);
        $str = implode('', $arr);
        $str = substr( md5( substr( $str, 0, 16) ), 0, 20 );
        return $str;
    }

    /**
    * 删除
    * @param  [type] $id [description]
    * @return [type]     [description]
    */
    public function delete($id)
    {
        // $name = Channel::where('id', $id)->value('name');
        // $url = url('/channelQrcode/'. $name .'.png');
        $res = Channel::where('id', $id)->delete();
        if ($res) {
            return true;
        }
        return false;
    }


}
