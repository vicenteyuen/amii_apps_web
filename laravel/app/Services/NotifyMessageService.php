<?php

namespace App\Services;

use App\Models\NotifyMessage;
use App\Models\UserNotifyMessage;
use App\Models\User;
use Carbon\Carbon;

class NotifyMessageService
{
    /**
     * 获取用户消息列表
     */
    public function index($userId, $provider = 'notify')
    {
        $fromTime = Carbon::now();
        if (auth()->user()) {
            $fromTime = auth()->user()
                ->created_at;
        }
        $messages = NotifyMessage::status()
            ->where('created_at', '>=', $fromTime)
            ->where(function ($query) use ($userId) {
                $query->where('user_id', $userId)
                    ->orWhere('user_id', 0);
            });
        switch ($provider) {
            case 'express':
                $messages = $messages->express();
                break;

            default:
                $messages = $messages->notify();
                break;
        }

        $messages = $messages->orderBy('id', 'desc')
            ->paginate(config('amii.apiPaginate'));

        if ($provider == 'express') {
            return $messages->setCollection($messages->makeVisible(['order_image', 'order_name', 'express_sn', 'order_sn']));
        }

        return $messages;
    }

    /**
    * 后台通知消息展示
    */
    public function adminIndex()
    {
        $notifyMessage = NotifyMessage::with('user')
            ->express()
            ->orderBy('created_at', 'desc')
            ->orderBy('status')
            ->orderBy('id')
            ->paginate(config('amii.adminPaginate'));

        return $notifyMessage;
    }

    /**
    * 搜索用户相关物流信息
    */
    public function search($search)
    {
        $notifyMessage = NotifyMessage::with('userNotifyMessage', 'user')
            ->express()
            ->when($search, function ($q) use ($search) {
                return $q->whereHas('user', function ($query) use ($search) {
                    return $query->where('realname', 'like', '%'.trim($search).'%')
                        ->orWhere('phone', 'like', '%'.trim($search).'%');
                });
            })
            ->orderBy('created_at', 'desc')
            ->orderBy('status')
            ->orderBy('id')
            ->paginate(config('amii.adminPaginate'));

        return $notifyMessage;
    }

    /**
    * 获取系统消息
    */
    public function notifyList()
    {
        $notifymessage = NotifyMessage::notify()
            ->where('user_id', 0)
            ->orderBy('created_at', 'desc')
            ->orderBy('status')
            ->paginate(config('amii.adminPaginate'));
        return $notifymessage;
    }

    /*
    *获取指定系统消息
    */
    public function showNotify($id)
    {
        $notifymessage = NotifyMessage::where('id', $id)
            ->notify()
            ->first();
        return $notifymessage;
    }

    /**
    *获取指定物流信息
    */
    public function showExpress($id)
    {
        $notifymessage = NotifyMessage::with('userNotifyMessage', 'user')
            ->express()
            ->where('id', $id)
            ->first();
        return $notifymessage;
    }

    /**
     * 获取通知消息主面板
     */
    public function home($userId)
    {
        $notifyMessage = NotifyMessage::where('user_id', $userId)
            ->status()
            ->orderBy('created_at', 'desc');
        $notify = (clone $notifyMessage)
            ->orWhere('user_id', 0)
            ->notify()
            ->first();
        $express = (clone $notifyMessage)
            ->express()
            ->first();

        return compact('notify', 'express');
    }

    /**
     * 新增系统消息
     */
    public function store($data)
    {
        return NotifyMessage::create($data);
    }

    /**
     * 更新用户消息为已读
     */
    public function read($userId, $provider)
    {
        if ($provider == 'notify') {
            $notifys = NotifyMessage::where('provider', $provider)
                ->where(function ($q) use ($userId) {
                    $q->where('user_id', 0)
                        ->orWhere('user_id', $userId);
                });

            $count = (clone $notifys)->where('status', 0)->count();
            if (!$count) {
                return false;
            }
            // 获取系统消息id
            $notifyIds = (clone $notifys)
                ->pluck('id')
                ->toArray();

            NotifyMessage::where('provider', $provider)
                ->where('user_id', $userId)
                ->update([
                    'status' => 1
                ]);
            $arr = array_map(function ($notifyId) use ($userId) {
                return [
                    'user_id' => $userId,
                    'notify_messages_id' => $notifyId
                ];
            }, $notifyIds);

            return UserNotifyMessage::insert($arr);
        }
        return NotifyMessage::where('provider', $provider)
            ->where('user_id', $userId)
            ->update(['status' => 1]);
    }

    /**
     * 删除消息
     */
    public function destroy($userId, $id)
    {
        return NotifyMessage::where('id', $id)
            ->where('user_id', $userId)
            ->delete();
    }

    /**
     * 批量删除消息
     */
    public function deletes($userId, array $ids)
    {
        return NotifyMessage::whereIn('id', $ids)
            ->where('user_id', $userId)
            ->delete();
    }

    /**
     * 获取通知消息总数
     */
    public function number($userId)
    {
        return NotifyMessage::where('status', 0)
            ->where(function ($q) use ($userId) {
                $q->where('user_id', $userId)
                    ->orWhere(function ($q1) use ($userId) {
                        $q1->where('user_id', 0)
                            ->whereDoesntHave('userNotifyMessage', function ($q2) use ($userId) {
                                $q2->where('user_id', $userId);
                            });
                    });
            })
            ->count();
    }

    /**
    * 各个推送点消息推送
    * @param  string  $provider  类别（notify\express）
    * @param  string  $type      推送点名称（积分，优惠券，快递发货、派送、签收）
    * @param  array   $data     【user_id（用户ID）,number（数量）, way(获取途径),shipping_sn（快递单号）,order_id(订单ID, product_name(商品名称))】
    * @return bool
    */
    public function sendNotifyMsg($provider, $type, $data)
    {
        $arr = [];
        if ($provider == 'notify') {
            switch ($type) {
                // 获得积分
                case 'points':
                    $title = '积分';
                    // 生成系统通知消息
                    $arr['title']   = '您获得了'.$data['number'].$title.'!';
                    // 售后退积分情况
                    if($data['way'] == 'reback' ){
                        $arr['content'] = '您通过取消订单或售后退回了'.$data['number'].$title.'~';
                    } else {
                        $arr['content'] = '您通过'.$data['way'].'获得了'.$data['number'].$title.'~';
                    }
                    break;
                // 优惠券
                case 'coupons':
                    $title = '优惠券';
                    $data['coupon_desc'] ?: '';
                    // 生成系统通知消息
                    $arr['title']   = '您获得了一张'.$title.'!';
                    $arr['content'] = '恭喜您！获得了一张'.$data['coupon_desc'].$title.'哦~';
                    break;
                default:
                    return false;
            }
        } else if ($provider == 'express') {
            switch ($type) {
                // 发货
                case 'deliver':
                    $title = '发货了';
                    break;
                // 派送中
                case 'sending':
                    $title = '在派送中';
                    break;
                // 签收
                case 'receive':
                    $title = '签收了';
                    break;
                default:
                    return false;
            }
            // 生成快递通知消息
            $arr['title']   = '您的购买的宝贝已经'.$title.'！';
            $arr['content'] = '订单已'.$title.'! 您购买的商品'.$data['product_name'].'已经'.$title.'~';
            $arr['type']    = 'order';
            $arr['resource']= $data['order_id'];
        } else {
            return false;
        }
        // 生成通知消息
        $arr['status']   = 0;
        $arr['user_id']  = $data['user_id'];
        $arr['provider'] = $provider;

        NotifyMessage::create($arr);

        //消息推送
        $Info['title']      = $arr['title'];
        $Info['content']    = $arr['content'];
        $Info['notifyData'] = ['action' => $provider];

        // 向用户发送消息推送
        app()->make('leancloud', ['push'])->push($data['user_id'], $Info);
    }

    //游戏推送
    public function sendGameNotifyMsg($userId, $content)
    {

        $arr = [
            'status'  => 0,
            'user_id' => $userId,
            'title'   => '拼图送衣',
            'content' => $content,
        ];
        NotifyMessage::create($arr);
        //消息推送
        $Info['title']      = $arr['title'];
        $Info['content']    = $arr['content'];
        // 向用户发送消息推送
        app()->make('leancloud', ['push'])->push($userId, $Info);
    }

}
