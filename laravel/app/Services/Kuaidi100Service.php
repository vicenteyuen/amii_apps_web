<?php

namespace App\Services;

use App\Libraries\Kuaidi100\Kuaidi100;
use App\Models\OrderPackage;
use App\Models\ExpressModels\ExpressLog;
use App\Models\Order;
use App\Helpers\RebackTribeMoneyHelper;
use App\Models\GiveProduct;
use App\Models\User;
use App\Helpers\FirstOrderReturnDiscount;
use App\Helpers\OrderReceivedHelper;

class Kuaidi100Service
{
    /**
     * 订阅
     */
    public function poll($number)
    {
        $kuaidi100 = new Kuaidi100();

        $poll = $kuaidi100->poll($number);

        if ($poll) {
            return true;
        }
        return false;
    }

    /**
     * 查询
     */
    public function express($com, $nu)
    {
        $kuaidi100 = new Kuaidi100();

        $express = $kuaidi100->express($com, $nu);

        return $express;
    }
    
    /**
     * 更新快递信息
     * @param  [type] $param [description]
     * @return [type]        [description]
     */
    public function handleExpressInfo($param)
    {
        $orderPackage = OrderPackage::where('shipping_sn', $param->lastResult->nu)->first();
        if (!isset($orderPackage->express) && empty($orderPackage->express)) {
            return false;
        }
        $expressLog = $orderPackage->express;

        $data = [];
        foreach ($param->lastResult->data as $key => $value) {
            $data[] = $value;
        }
        //接口无数据
        if (empty($data)) {
            return  false;
        }
        $expressInfo = $expressLog->info['data'];
        $data[] = $expressInfo[count($expressInfo)-1];
        $info = [
            'com'  => $expressLog->info['com'],
            'nu'   => $expressLog->info['nu'],
            'data' => $data
        ];
        $expressLog->info = $info;
        $expressLog->save();
        //已签收
        if ($param->lastResult->state == 3) {
            // 改成签到
            $orderId = OrderPackage::where('shipping_sn', $param->lastResult->nu)
                ->value('order_id');
            $order = Order::find($orderId);
            // if ($order->shipping_status == 2) {
            //     return true;
            // }
            $order->shipping_status = 2;
            $order->can_refund = 1;
            $order->save();
            // 确认收货时间,8天后关闭售后
            OrderReceivedHelper::received($order);
            // 首单退30%
            $delay =  8 * 24 * 60 * 60;
            $job = (new \App\Jobs\FirstPurchaseReturnMoneyJob(clone $order))->delay($delay);
            dispatch($job);
            //部落七天退回佣金
            RebackTribeMoneyHelper::expressRebackMoney($orderId);
        }
        return true;
    }
}
