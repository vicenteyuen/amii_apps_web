<?php
namespace App\Services;

use App\Models\SellProduct;
use Ecommerce\Models\Inventory;
use Ecommerce\Models\Product;
use App\Models\UserComment;
use App\Models\ProductImage;
use App\Helpers\ImageHelper;
use App\Models\OrderProduct;
use App\Models\SellAttribute;
use App\Models\SellAttributeValue;
use App\Models\InventoryAttributeValue;
use App\Helpers\AmiiHelper;
use App\Helpers\GamePuzzlesHelper;
use DB;
use Carbon\Carbon;

class SellProductService
{


    /**
     *新增库存
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {
        if ($this->checkDuplicate($arr)) {
            return SellProduct::create($arr);
        }
        return false;

    }

    public function detail($id)
    {
        return sellProduct::where('id', $id)
            ->with('product')
            ->first();
    }

      /**
     * 普通库存
     * @param  [type] $product_id [description]
     * @return [type]              1 新增 2 编辑
     */
    public function addSellType($product_id, $provider = 1, $type = 1)
    {
        // 积分 礼品 默认下架状态
        if ($provider == 2 || $provider == 4) {
            self::addPointProduct($product_id, $provider, $type);
        }

        $arr = [
        'product_id' => $product_id,
        'provider'   => $provider,
        ];

        if ($this->checkDuplicate($arr)) {
            $arr['number'] = 0 ;
            $arr['status'] = 0;
            DB::beginTransaction();
            $product = Product::find($product_id);
            $create = SellProduct::create($arr);
            if ($product) {
                if ($product->status == 0) {
                    $productStatus = Product::where('id', $product_id)
                    ->update(['status' => 1]);
                    if ($create && $productStatus) {
                        DB::commit();
                        return $create;
                    }
                    DB::rollback();
                    return false;
                }
            }
            if ($create) {
                DB::commit();
                return $create;
            }
            DB::rollback();
            return false;
        }
        if ($type == 2) {
            return SellProduct::where($arr)
            ->first();
        }
        return false;
    }

    // 积分库存
    public function addPointProduct($product_id, $provider = 2, $type = 1)
    {
        $arr = [
            'product_id' => $product_id,
            'provider'   => $provider,
        ];
        if ($provider == 2) {
             $sellProduct = SellProduct::where('product_id', $product_id)
                ->where('provider', 1)
                ->first();
            $arr['product_point'] = (int)($sellProduct->base_price * 100);
            $arr['market_price'] = $sellProduct->market_price;
        }
        if ($this->checkDuplicate($arr)) {
            $arr['number'] = 0 ;
            $arr['status'] = 0;
            DB::beginTransaction();
            $product = Product::find($product_id);
            $create = SellProduct::create($arr);
            if ($product) {
                if ($product->status == 0) {
                    $productStatus = Product::where('id', $product_id)
                    ->update(['status' => 1]);
                    if ($create && $productStatus) {
                        DB::commit();
                        self::addPointInventory($create);
                        return $create;
                    }
                    DB::rollback();
                    return false;
                }
            }
            if ($create) {
                DB::commit();
                self::addPointInventory($create);
                return $create;
            }
            DB::rollback();
            return false;
        }
        if ($type == 2) {
            return SellProduct::where($arr)
            ->first();
        }
        return false;

    }

    /**
     * 查重
     * @param  [type] $arr [description]
     * @return bool      [description]
     */
    public function checkDuplicate($arr)
    {
        return SellProduct::where($arr)
        ->get()
        ->isEmpty();

    }

    // 添加无属性库存
    public function update($arr, $inventoryNumber)
    {
        DB::beginTransaction();

        // 更新sellProduct
        $update = SellProduct::where('id', $arr['product_sell_id'])
        ->update(['number' => $arr['number'],'inventory_number' => $inventoryNumber]);
        //添加库存
        $where = ['product_sell_id' => $arr['product_sell_id'],'product_id' => $arr['product_id']];

        $editStatus =Inventory::where($where)
        ->first();

        if ($editStatus) {
            // 编辑状态
            $inventory = Inventory::where($where)
            ->update(['number' => $arr['number'],'inventory_number' => $inventoryNumber]);
        } else {
            //新增
            $inventory = Inventory::insert($arr);
        }
        if ($update && $inventory) {
            DB::commit();
            return true;
        }
            DB::rollback();
            return false;

    }

    /**
     * 上传商品主图
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function uploadImage($arr)
    {
        return SellProduct::where('id', $arr['id'])
        ->update(['image' => $arr['image']]);

    }
    /**
    *   获取商品主图
    */
    // public function getMainImage($product_id)
    // {
    //     $data = Product::where('id', $product_id)->value('main_image');
    //     return $data;
    // }

    /**
     * 刷新页面
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function number($arr)
    {
        return SellProduct::where($arr)
        ->first();
    }

    // 当前商品是否有库存
    public function hasInventory($id)
    {
        return Inventory::where('product_sell_id', $id)
            ->first();
    }

    /**
     * 保存商品卖点
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function saveSellPoint($id, $arr)
    {
        $data = Product::with('sellProduct')
            ->whereHas('sellProduct', function ($q) use ($id) {
                return $q->where('id', $id);
            })
            ->update($arr);
        return $data;
    }

    /**
     * 保存基本价格
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function save($id, $arr)
    {
        return SellProduct::where('id', $id)
        ->update($arr);

    }

    //商品上下架
    public function updataStatus($id, $arr)
    {
        $message = ['status' => 0,'msg' => ''];
        if ($arr['status'] == 1) {
            $exits = self::hasInventory($id);
            // 没有库存，上架失败
            if (!$exits) {
                $message['msg'] = '请先添加库存,才能上架';
                return $message;
            }
        }
        $update =  SellProduct::where('id', $id)
            ->update($arr);
        if ($update) {
            $message['status'] = 1;
            return $message;
        }
        return $message;
    }

    /**
     * 返回所有图片
     * @param  [type] $sellProductId [description]
     * @return [type]                [description]
     */
    public function images($sellProductId)
    {
        $sellProduct = SellProduct::where('id', $sellProductId)
        ->with('product')
        ->with('inventories')
        ->first();

        $images = [];
        if (!$sellProduct) {
            return [];
        }
        // if ($sellProduct['image']) {
        //     $images[]     = $sellProduct['image'];
        // }
        if ($sellProduct['product']['main_image']) {
            $images[]     = $sellProduct['product']['main_image']; //product 主图
        }
        $productImage = ProductImage::where('product_id', $sellProduct['product']['id'])
        ->get();
        foreach ($productImage as $key => $value) {
            $images[] = $value['image'];                       //product 列表图(tab3)
        }
        // $images[]     = $sellProduct['image'];                 //sellProduct 主图
        // foreach ($sellProduct['inventories'] as $key => $value) {
        //     if ($value['image']) {
        //          $images[] = $value['image'];                  // inventory 库存图
        //     }
        // }
        $images = array_filter($images);
        if (empty($images)) {
            $images[] = "http://7xw4yp.com1.z0.glb.clouddn.com/1459309578804.jpg";   //默认图
        }

        return array_slice($images, 0, 10);
    }

    public function image($productId)
    {
        $product = Product::where('id', $productId)
            ->first();
        $images = [];
       
        
        if ($product->main_image) {
            $images[]     = $product->main_image; //product 主图
        }

        $productImage = ProductImage::where('product_id', $productId)
        ->get();
        foreach ($productImage as $key => $value) {
            $images[] = $value['image'];                       //product 列表图(tab3)
        }
        $images = array_filter($images);
        if (empty($images)) {
            $images[] = "http://7xw4yp.com1.z0.glb.clouddn.com/1459309578804.jpg";   //默认图
        }
        return array_slice($images, 0, 10);
    }

    // 预览轮播图
    public function previewImage($productId)
    {
        $product = Product::where('id', $productId)
            ->with('sellProduct')
            ->first();
        if ($product->main_image) {
             $images[] = $product->main_image;
        }
        $productImage = ProductImage::where('product_id', $product->id)
            ->get();

        foreach ($productImage as $key => $value) {
            $images[] = $value['image'];
        }

        foreach ($product->sellProduct as $key => $value) {
            $images[] = $value['image'];
        }
        return $images;
    }


    /**
     * 删除
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        return SellProduct::destroy($id);
    }

    /**
     * 添加商品到游戏
     * @param [type] $id [description]
     */
    public function addProductToGame(array $arr, $suffix = '', $number = 0)
    {

        DB::beginTransaction();

        $checkArr = [
            'provider' => 3,
            'product_id' => $arr['product_id'],
        ];
        if (!$this->checkDuplicate($checkArr)) {
            return false;
        }
        //sell_products
        $copySellProduct = SellProduct::where('product_id', $arr['product_id'])
            ->where('provider', 1)
            ->first();

        $arr['provider'] = 3;
        $arr['status'] = 0;
        $arr['number'] = 0;
        $arr['inventory_number'] = 0;
        $arr['orderBy'] = $copySellProduct->orderBy;
        if (!empty($copySellProduct->image)) {
              $arr['image'] = basename($copySellProduct->image);
        }
        $sellProduct = SellProduct::create($arr);
        if (!$sellProduct) {
            DB::rollback();
            return ['status' => 0,'message' => '添加失败'];
        }

        //sell_attribute
        $copySellAttribute = SellAttribute::where('product_sell_attribute_id', $copySellProduct->id)
            ->get();

        $sellAttributeArr = [];
        foreach ($copySellAttribute as $value) {
            $sellAttributeArr[] = [
                'attribute_id' => $value->attribute_id,
                'product_sell_attribute_id' => $sellProduct->id
            ];
        }
        if ($sellAttributeArr) {
            $sellAttribute = SellAttribute::insert($sellAttributeArr);
            if (!$sellAttribute) {
                DB::rollback();
                return ['status' => 0,'message' => '添加失败'];
            }
        }
        //sell_attribute_values
        $sellAttributeIds = $copySellAttribute->pluck('id')->toArray();
        $newSellAttribute = SellAttribute::where('product_sell_attribute_id', $sellProduct->id)
            ->pluck('id', 'attribute_id')
            ->toArray();
        if ($sellAttributeIds) {
            $copySellAttributeValue = SellAttributeValue::whereIn('sell_attribute_id', $sellAttributeIds)
                ->get()
                ->toArray();

            $sellAttributeValueArr ;
            foreach ($copySellAttributeValue as $k2 => $v2) {
                $sellAttributeValueArr[] = [
                    'attribute_value_id' => $v2['attribute_value_id'],
                    'attribute_id'       => $v2['attribute_id'],
                    'sell_attribute_id'  => $newSellAttribute[$v2['attribute_id']],
                    'sell_product_attribute_value_image' => $v2['sell_product_attribute_value_image']?:'',
                ];
            }
            $sellAttributeValue = SellAttributeValue::insert($sellAttributeValueArr);
            if (!$sellAttributeValue) {
                DB::rollback();
                return ['status' => 0,'message' => '添加失败'];
            }
        }

        //inventories
        $copyInventory = Inventory::where('product_sell_id', $copySellProduct->id)
            ->get()
            ->toArray();
        if ($copyInventory) {
            $oldInventory = $copyInventory;
            $totalNumber = 0;
            if ($copyInventory) {
                $inventoryArr = [];
                foreach ($copyInventory as $k1 => $v1) {
                    $inventoryArr[] = [
                        'product_id'        => $v1['product_id'],
                        'sku_code'          => $v1['sku_code'].$suffix,
                        'value_key'         => $v1['value_key'],
                        'product_sell_id'   => $sellProduct->id,
                        'number'            => $number,
                        'warn_number'       => 0,
                        'sku_sell_count'    => 0,
                        'image'             => basename($v1['image'])?basename($v1['image']):'',
                        'status'            => $v1['status'],
                        'mark_price'        => $v1['mark_price'],
                        'shop_price'        => $v1['shop_price'],
                    ];
                    $totalNumber += $number;
                }
                if ($number) {
                    SellProduct::where('id', $sellProduct->id)
                        ->update(['number' => $totalNumber,'inventory_number' => $totalNumber]);
                }
                $inventory = Inventory::insert($inventoryArr);
                if (!$inventory) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }
            }

             //inventories_attribute_values
            $inventoryValueId = Inventory::where('product_sell_id', $sellProduct->id)
                ->pluck('id')
                ->toArray();
            $copyInventoryAttriubteValue = InventoryAttributeValue::whereIn('inventory_id', array_column($oldInventory, 'id'))
                ->get()
                ->toArray();

            if ($inventoryValueId) {
                $attributeValueIds = array_column($copyInventoryAttriubteValue, 'attribute_value_id');
                $times = count($attributeValueIds)/count($inventoryValueId);
                if (!is_int($times)) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }

                // 复制库存值
                foreach ($inventoryValueId as $key => $value) {
                    for ($i=1; $i <= $times; $i++) {
                        $inventoryValueIdArr[] = $value;
                    }
                }

                if (count($inventoryValueIdArr) != count($attributeValueIds)) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }

                $inventoryAttributeValueArr = array_map(function ($inventoryId, $attributeValudId) {
                        return [
                            'inventory_id' => $inventoryId,
                            'attribute_value_id' => $attributeValudId,
                        ];
                }, $inventoryValueIdArr, $attributeValueIds);
                $invetoryValue = InventoryAttributeValue::insert($inventoryAttributeValueArr);
                if (!$invetoryValue) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }
            }
        }
        DB::commit();
        $url = action('Admin\\SellController@edits', ['product_id' => $arr['product_id'],'provider' => $arr['provider']]);
        return ['status' => 1,'url' => $url];
    }

    /**
     * 商品游戏列表
     * @return [type] [description]
     */
    public function gameProduct($key)
    {
        $key = str_replace('%', '\%', $key);
        return SellProduct::where('provider', 3)
            ->where('status', 1)
            ->whereHas('product', function ($q2) use ($key) {
                return $q2->where('name', 'like', '%'.$key.'%')
                        ->orWhere('snumber', 'like', '%'.$key.'%');
            })
            ->with('product')
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.adminPaginate'));
    }


    /**
     * 后台商品游戏列表
     * @return [type] [description]
     */
    public function adminGameProduct($key)
    {
        $key = str_replace('%', '\%', $key);
        return SellProduct::where('provider', 3)
          ->whereHas('product', function ($q2) use ($key) {
              return $q2->where('name', 'like', '%'.$key.'%')
                      ->orWhere('snumber', 'like', '%'.$key.'%');
          })
          ->with('product')
          ->orderBy('created_at', 'desc')
          ->paginate(config('amii.adminPaginate'));
    }

    /**
     * 上下架商品 by id
     * @param  [type] $id     [description]
     * @param  [type] $status [description]
     * @return [type]         [description]
     */
    public function gameSellproductDownOrUp($arr)
    {
        return SellProduct::where('id', $arr['id'])
            ->update(['status'=>$arr['status']]);
    }


    //游戏商品批量上下架by id
    public function muliGameSellproductDownOrUp($ids, $status)
    {
        return SellProduct::whereIn('id', $ids)
            ->update(['status' => $status]);
    }

    //商品批量上下架by produt_id provider
    public function SellproductDownOrUp($productId, $provider, $status)
    {
        return SellProduct::where('product_id', $productId)
            ->where('provider', $provider)
            ->update(['status'=> $status]);
    }


    //商品批量上下架by product_id provider
    public function muliSellproductDownOrUp($ids, $provider, $status)
    {
        // dd($ids, $provider, $status);
        return SellProduct::whereIn('product_id', $ids)
            ->where('provider', $provider)
            ->update(['status' => $status]);
    }

    public function transferPrice($id, $value)
    {
        $sellProduct = SellProduct::find($id);
        if ($sellProduct && $sellProduct->provider == 2) {
            return (int)$value;
        }
        return $value;
    }

    // 增加销量
    public function addSales($orderId)
    {
        $orderProduct = OrderProduct::where('order_id', $orderId)
            ->with('sellProduct')
            ->get();
        foreach ($orderProduct as $k1 => $v1) {
            $v1->sellProduct->volume = $v1->sellProduct->volume + $v1->number;
            $v1->sellProduct->month_volume = $v1->sellProduct->month_volume+$v1->number;
            $v1->sellProduct->save();
        }
    }

    // 积分商品库存
    public function addPointInventory($sellProduct)
    {
        DB::beginTransaction();
        //sell_products
        $copySellProduct = SellProduct::where('product_id', $sellProduct->product_id)
            ->where('provider', 1)
            ->first();

        // update image

        if (!empty($copySellProduct->image)) {
            $sellProduct->image = basename($copySellProduct->image);
            $sellProduct->save();
        }

         //sell_attribute
        $copySellAttribute = SellAttribute::where('product_sell_attribute_id', $copySellProduct->id)
            ->get();

        $sellAttributeArr = [];
        foreach ($copySellAttribute as $value) {
            $sellAttributeArr[] = [
                'attribute_id' => $value->attribute_id,
                'product_sell_attribute_id' => $sellProduct->id
            ];
        }
        if ($sellAttributeArr) {
            $sellAttribute = SellAttribute::insert($sellAttributeArr);
            if (!$sellAttribute) {
                DB::rollback();
                return ['status' => 0,'message' => '添加失败'];
            }
        }
        //sell_attribute_values
        $sellAttributeIds = $copySellAttribute->pluck('id')->toArray();
        $newSellAttribute = SellAttribute::where('product_sell_attribute_id', $sellProduct->id)
            ->pluck('id', 'attribute_id')
            ->toArray();
        if ($sellAttributeIds) {
            $copySellAttributeValue = SellAttributeValue::whereIn('sell_attribute_id', $sellAttributeIds)
                ->get()
                ->toArray();

            $sellAttributeValueArr ;
            foreach ($copySellAttributeValue as $k2 => $v2) {
                $sellAttributeValueArr[] = [
                    'attribute_value_id' => $v2['attribute_value_id'],
                    'attribute_id'       => $v2['attribute_id'],
                    'sell_attribute_id'  => $newSellAttribute[$v2['attribute_id']],
                    'sell_product_attribute_value_image' => $v2['sell_product_attribute_value_image']?:'',
                ];
            }
            $sellAttributeValue = SellAttributeValue::insert($sellAttributeValueArr);
            if (!$sellAttributeValue) {
                DB::rollback();
                return ['status' => 0,'message' => '添加失败'];
            }
        }

        //inventories
        $copyInventory = Inventory::where('product_sell_id', $copySellProduct->id)
            ->get()
            ->toArray();
        if ($copyInventory) {
            $oldInventory = $copyInventory;
            $totalNumber = 0;
            if ($copyInventory) {
                $inventoryArr = [];
                foreach ($copyInventory as $k1 => $v1) {
                    // 积分加库存,积分值
                    if ($sellProduct->provider == 2) {
                        $number = $v1['number'];
                        $markPrice =  $v1['mark_price'] * 200;
                    } else {
                        $number = 0;
                        $markPrice = $v1['mark_price'];
                    }
                    $inventoryArr[] = [
                        'product_id'        => $v1['product_id'],
                        'sku_code'          => $v1['sku_code'], //重复
                        'value_key'         => $v1['value_key'],
                        'product_sell_id'   => $sellProduct->id,
                        'number'            => $number,
                        'warn_number'       => 0,
                        'sku_sell_count'    => 0,
                        'image'             => basename($v1['image'])?basename($v1['image']):'',
                        'status'            => $v1['status'],
                        'mark_price'        => $markPrice ,
                        'shop_price'        => $v1['shop_price'],
                    ];
                    $totalNumber += $number;
                }
                // 更新总库存
                if ($sellProduct->provider == 2) {
                    SellProduct::where('id', $sellProduct->id)
                        ->update(['number' => $totalNumber,'inventory_number' => $totalNumber]);
                }
                $inventory = Inventory::insert($inventoryArr);
                if (!$inventory) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }
            }
             //inventories_attribute_values
            $inventoryValueId = Inventory::where('product_sell_id', $sellProduct->id)
                ->pluck('id')
                ->toArray();
            $copyInventoryAttriubteValue = InventoryAttributeValue::whereIn('inventory_id', array_column($oldInventory, 'id'))
                ->get()
                ->toArray();
            if ($inventoryValueId) {
                $attributeValueIds = array_column($copyInventoryAttriubteValue, 'attribute_value_id');
                $times = count($attributeValueIds)/count($inventoryValueId);
                if (!is_int($times)) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }

                // 复制库存值
                foreach ($inventoryValueId as $key => $value) {
                    for ($i=1; $i <= $times; $i++) {
                        $inventoryValueIdArr[] = $value;
                    }
                }

                if (count($inventoryValueIdArr) != count($attributeValueIds)) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }

                $inventoryAttributeValueArr = array_map(function ($inventoryId, $attributeValudId) {
                        return [
                            'inventory_id' => $inventoryId,
                            'attribute_value_id' => $attributeValudId,
                        ];
                }, $inventoryValueIdArr, $attributeValueIds);
                $invetoryValue = InventoryAttributeValue::insert($inventoryAttributeValueArr);
                if (!$invetoryValue) {
                    DB::rollback();
                    return ['status' => 0,'message' => '添加失败'];
                }
            }
        }
        DB::commit();
        return true;

    }

    //库存添加游戏商品
    public function skuAddGameProduct($sku, $number)
    {
        $skuNumber = preg_match('/\d+/', $sku, $skuNumberArr);
        $suffix = substr($sku, strlen($skuNumberArr[0]));
        $inventory = Inventory::where('sku_code', $sku)
            ->first();

        // 有后缀
        if ($suffix) {
            if ($inventory) {
                //更新库存
                $update = self::updateNumber($inventory, $number, $sku);
                if (!$update) {
                    return ['status' => 0,'message' => '更新库存失败'];
                }
                return ['status' => 1,'message' => '游戏商品已存在'];
            }
            $inventory = Inventory::where('sku_code', $skuNumberArr[0])
                ->whereHas('NormalSellProduct', function ($q1) {
                    return $q1->where('provider', 3);
                })
                ->first();
            if ($inventory) {
                // 修正之前的游戏sku库存
                $inventories = Inventory::where('product_id', $inventory->product_id)
                    ->where('product_sell_id', $inventory->product_sell_id)
                    ->whereHas('NormalSellProduct', function ($q1) {
                        return $q1->where('provider', 3);
                    })
                ->get();
                foreach ($inventories as $key => $value) {
                    Inventory::where('product_id', $inventory->product_id)
                        ->where('product_sell_id', $inventory->product_sell_id)
                        ->where('sku_code', $value->sku_code)
                        ->update(['sku_code' => $value->sku_code.$suffix]);
                }
                //更新库存
                $update = self::updateNumber($inventory, $number, $sku);
                if (!$update) {
                    return ['status' => 0,'message' => '更新库存失败'];
                }
                return ['status' => 1,'message' => '游戏商品已存在'];
            }
            // 后缀添加游戏商品
            $inventory = Inventory::where('sku_code', $skuNumberArr[0])
                ->whereHas('NormalSellProduct', function ($q1) {
                    return $q1->where('provider', 1);
                })
                ->first();
            if (!$inventory) {
                 return ['status' => 0,'message' => 'sku错误'];
            }
        } else {
            return ['status' =>0,'message' => '格式出错'];
        }

        $puzzles = GamePuzzlesHelper::countPuzzles($inventory->NormalSellProduct->market_price);
        $arr = [
            'product_id' => $inventory->NormalSellProduct->product_id,
            'market_price' => $inventory->NormalSellProduct->market_price,
            'base_price'  => $inventory->NormalSellProduct->market_price,
            'puzzles'  => $puzzles,
        ];
        return self::addProductToGame($arr, $suffix, $number);
    }

    //更新库存
    public function updateNumber($inventory, $number, $skuCode)
    {

        $updateNumber = Inventory::where('product_id', $inventory->product_id)
            ->where('product_sell_id', $inventory->product_sell_id)
            ->where('sku_code', $skuCode)
            ->update(['number' => $number]);

        if (!$updateNumber) {
            return false;
        }

        SellProduct::where('id', $inventory->product_sell_id)
                ->update(['number' => DB::raw('(select sum(number) from inventories where product_sell_id = ' . $inventory->product_sell_id . ')')]);
        return true;
    }

    /**
     * 自动下架
     */
    public function autoDlist()
    {
        $time = Carbon::now()->toDateTimeString();
        // 记录异常处理
        SellProduct::where('number', 0)
            ->where('number_zero_time', null)
            ->update(['number_zero_time' => $time]);
        SellProduct::where('number', '!=', 0)
            ->where('number_zero_time', '!=', null)
            ->update(['number_zero_time' => null]);

        // 库存连续三天为0下架
        SellProduct::where('number', 0)
            ->where('status', 1)
            ->where('number_zero_time', '<=', Carbon::now()->subDays(3))
            ->update(['status' => 0]);

        // 今年之前库存不足7下架
        SellProduct::where('number', '<', 7)
            ->where('status', 1)
            ->whereHas('product', function ($query) {
                return $query->whereRaw('SUBSTRING(snumber, 2, 2) != ' . substr(Carbon::now()->year, 2));
            })
            ->update(['status' => 0]);
    }
}
