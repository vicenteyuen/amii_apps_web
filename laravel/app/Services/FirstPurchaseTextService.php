<?php
namespace App\Services;

use App\Models\FirstPurchaseText;
use App\Models\Activity;
use DB;

class FirstPurchaseTextService
{
    public function store($arr, $title, $orderDescription)
    {
        DB::beginTransaction();
        $first = FirstPurchaseText::create($arr);
        $activity = Activity::updateOrCreate(['provider' => 'first'], ['title' =>$title,'order_description'=>$orderDescription ]);
        if ($first && $activity) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    // admin
    public function index()
    {
        return FirstPurchaseText::orderBy('created_at', 'asc')
            ->paginate(config('amii.adminPaginate'));
    }

    // api
    public function apiIndex()
    {
        $data['text'] = FirstPurchaseText::orderBy('created_at', 'asc')
            ->get();
        $activity = Activity::where('provider', 'first')
            ->first();
        $isPop = app('amii.activity')->isFirstOrder();
        if ($isPop) {
            $activity->isPopup = 1;
        } else {
            $activity->isPopup = 0;
        }
        $data['title'] =  $activity->title;
        $data['activity'] = $activity;
        return $data;
    }

    public function update($arr, $title, $orderDescription)
    {
        DB::beginTransaction();
        $first = FirstPurchaseText::where('id', $arr['id'])
            ->update(['text' => $arr['text']]);
        $activity = Activity::updateOrCreate(['provider' => 'first'], ['title' =>$title,'order_description'=>$orderDescription]);
        if ($first && $activity) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;

    }
    public function show($id)
    {
        return FirstPurchaseText::find($id);
    }

    public function delete($id)
    {
        return FirstPurchaseText::destroy($id);
    }

    public function getActivity()
    {
        return Activity::where('provider', 'first')
            ->first();
    }
}
