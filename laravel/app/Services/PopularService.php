<?php

namespace App\Services;

use Ecommerce\Models\Product;
use App\Models\SellProduct;
use App\Models\BrandProduct;
use App\Models\Popular;
use App\Models\ProductCategories;
use App\Models\BrandCategory;
use Cache;

class PopularService
{
    protected $data;
    /**
     * banner index for api
     */
    public function index()
    {

        Cache::pull('populars');

        $populars = Cache::rememberForever('populars', function () {

            return Popular::where('status', 1)
                ->orderBy('weight', 'desc')
                ->get()
                ->toTree()
                ->toArray();
        });

        return $populars;
    }

    /**
     * popular index for admin
     */
    public function adminIndex($id = 0)
    {
        $query = Popular::where('parent_id', $id);
        $query = $query->orderBy('weight', 'desc')->paginate(config('amii.adminPaginate'));
        return $query;
    }

    /**
     * popular items for select
     */
    public function parents()
    {
        $populars = Popular::get()->toTree();
        $parents = self::tree($populars);
        return $parents;
    }

    private static function tree($populars, $lev = 1)
    {
        $arr = [];
        $arr[0] = '顶级';
        foreach ($populars as $popular) {
            $tmp = $popular->children;
            $levStr = '';
            for ($i=0; $i < $lev; $i++) {
                $levStr .= '&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $arr[$popular->id] = $levStr . $popular->title;
            if (! $tmp->isEmpty()) {
                $arr = $arr + self::tree($tmp, $lev + 1);
            }
        }
        return $arr;
    }

    /**
     * store popular
     */
    public function store(array $arr)
    {
        $create =  Popular::create($arr);
        if (Cache::get('populars')) {
            Cache::forget('populars');
        }
        return $create;
    }

    /*
    *  update  popular
    */
    public function update($id, $arr)
    {
        if ($id == $arr['parent_id']) {
            return false;
        }
        $check = $this->checkPopular($id, $arr['parent_id']);
        if (!$check) {
            return false;
        }
        $update =  Popular::where('id', $id)->update($arr);
        if ($update) {
            if (Cache::get('populars')) {
                Cache::forget('populars');
            }
            return true;
        }
        return false;
    }

    // 获取当前推广的下级
    public function getMyChild($id)
    {
        $populars = Popular::orderBy('weight', 'desc')->get();
        $arr = [];
        $data = [];
        $this->findIdChild($populars, $arr, $id);
        foreach ($arr as $k => $v) {
            $data[] = Popular::where('id', $v)->first();
        }
        if (!$data) {
            return null;
        }
        return $data;
    }

    /**
     * 是否可作为parent
     */
    public function checkPopular($id, $parent_id)
    {
        $populars = Popular::orderBy('weight', 'desc')->get();
        $arr = [];
        $this->findIdChild($populars, $arr, $id);
        if (in_array($id, $arr)) {
            return false;
        }
        return true;
    }

    public function findIdChild($populars, &$arr, $id)
    {
        foreach ($populars as $key => $value) {
            if ($id == $value['parent_id']) {
                array_push($arr, $value['id']);
                $this->findIdChild($populars, $arr, $value['id']);
            }
        }
    }


    /**
     * 获取商品列表筛选条件数据
     */
    public function filterData($provider, $resource, $filter = '')
    {
        $categoryId = 0;
        if ($filter) {
            $filterItem = explode('+', $filter);
            foreach ($filterItem as $item) {
                $filterArr = explode('_', $item);
                if ($filterArr[0] == 'category') {
                    $categoryId = isset($filterArr[1]) ? $filterArr[1] : 0;
                }
            }
        }

        switch ($provider) {
            case 'category':
                $returnData = self::category($resource, $categoryId);
                break;
            case 'brand':
                $returnData = self::brand($resource, $categoryId);
                break;
            case 'brandcate':
                $returnData = self::brandcate($resource, $categoryId);
                break;

            default:
                $returnData = self::default();
                break;
        }

        return $returnData;
    }

    /**
     * 获取默认筛选数据
     */
    private function default()
    {
        $filters = [];

        // 获取分类下分类
        $categorys = app('category')->getCategoryChildren(0);
    if (! $categorys->isEmpty()) {
        $filters[] = [
        'filter_key' => 'category',
        'filter_name' => '分类',
        'filter_data' => self::formatFilterData($categorys)
        ];
    }

        // 分类所属品牌
        $brands = app('category')->getCategoryBrands(0);
    if (! $brands->isEmpty()) {
        $filters[] = [
        'filter_key' => 'brand',
        'filter_name' => '品牌',
        'filter_data' => self::formatFilterData($brands)
        ];
    }

        // 获取服务
        $services = app('amii.product.service')->index();
    if (! $services->isEmpty()) {
        $filters[] = [
        'filter_key' => 'service',
        'filter_name' => '折扣和服务',
        'filter_select' => 'many',
        'filter_data' => $services
        ];
    }

        // 价格区间数据
        $priceFromTo = self::priceFromTo();
    if (! empty($priceFromTo)) {
        $filters[] = [
        'filter_key' => 'price',
        'filter_name' => '价格区间（元）',
        'filter_data' => $priceFromTo
        ];
    }

        $returnData = [
            'filter_image' => '',
            'filters' => $filters
        ];

        return $returnData;
}

    /**
     * 获取分类筛选数据
     */
private function category($resource, $categoryId)
{
    $filters = [];

    // 获取分类展示图
    $indexImage = app('category')->productIndexImage($resource);

    // 获取分类下分类
    $categorys = app('category')->getCategoryChildren($resource);
    if (! $categorys->isEmpty()) {
        $filters[] = [
            'filter_key' => 'category',
            'filter_name' => '分类',
            'filter_data' => self::formatFilterData($categorys)
        ];
    }

    // 分类所属品牌
    $brands = app('category')->getCategoryBrands($resource);
    if (! $brands->isEmpty()) {
        $filters[] = [
            'filter_key' => 'brand',
            'filter_name' => '品牌',
            'filter_data' => self::formatFilterData($brands)
        ];
    }

    // 获取服务
    $services = app('amii.product.service')->index();
    if (! $services->isEmpty()) {
        $filters[] = [
            'filter_key' => 'service',
            'filter_name' => '折扣和服务',
            'filter_select' => 'many',
            'filter_data' => $services
        ];
    }

    // 价格区间数据
    $priceFromTo = self::priceFromTo();
    if (! empty($priceFromTo)) {
        $filters[] = [
            'filter_key' => 'price',
            'filter_name' => '价格区间（元）',
            'filter_data' => $priceFromTo
        ];
    }

    $returnData = [
    'filter_image' => $indexImage,
    'filters' => $filters
    ];

    return $returnData;
}

    /**
     * 获取品牌筛选数据
     */
private function brand($resource, $categoryId)
{
    $filters = [];

    // 获取品牌下分类
    $categorys = app('amii.manger.brand.category')->getBrandCategorys($resource, $categoryId);
    // 获取品牌展示图
    $indexImage = app('amii.manger.brand.category')->productIndexImage($resource);

    if (! $categorys->isEmpty()) {
        $filters[] = [
            'filter_key' => 'category',
            'filter_name' => '分类',
            'filter_data' => $categorys
        ];
    }

    // 获取服务
    $services = app('amii.product.service')->index();
    if (! $services->isEmpty()) {
        $filters[] = [
            'filter_key' => 'service',
            'filter_name' => '折扣和服务',
            'filter_select' => 'many',
            'filter_data' => $services
        ];
    }

    // 价格区间数据
    $priceFromTo = self::priceFromTo();
    $filters[] = [
    'filter_key' => 'price',
    'filter_name' => '价格区间（元）',
    'filter_data' => $priceFromTo
    ];

    $returnData = [
    'filter_image' => $indexImage,
    'filters' => $filters
    ];

    return $returnData;
}

    /**
     * 品牌分类
     */
private function brandcate($resource, $categoryId)
{
    $filters = [];

    // 获取品牌id分类id
    $resourceArr = app('amii.manger.brand')->getResourceArrByBrandCateId($resource);

    // 获取品牌分类下分类
    $categorys = app('amii.manger.brand.category')->getBrandCategorys($resourceArr['brand_id'], $resourceArr['category_id']);

    // 获取品牌展示图
    $indexImage = app('category')->getBrandCateImg($resource);

    if (! $categorys->isEmpty()) {
        $filters[] = [
            'filter_key' => 'category',
            'filter_name' => '分类',
            'filter_data' => $categorys
        ];
    }

    // 获取服务
    $services = app('amii.product.service')->index();
    if (! $services->isEmpty()) {
        $filters[] = [
            'filter_key' => 'service',
            'filter_name' => '折扣和服务',
            'filter_select' => 'many',
            'filter_data' => $services
        ];
    }

    // 价格区间数据
    $priceFromTo = self::priceFromTo();
    if (! empty($priceFromTo)) {
        $filters[] = [
            'filter_key' => 'price',
            'filter_name' => '价格区间（元）',
            'filter_data' => $priceFromTo
        ];
    }

    $returnData = [
    'filter_image' => $indexImage,
    'filters' => $filters
    ];

    return $returnData;
}

    /**
     * 筛选返回数据处理
     */
private function formatFilterData($datas)
{
    $returnData = [];
    foreach ($datas as $data) {
        $returnData[] = [
            'id' => $data->id,
            'name' => $data->name
        ];
    }
    return $returnData;
}

    /**
     * 获取价格区间
     */
private function priceFromTo()
{
    return [
    [
    'from' => 0,
    'to' => 200,
    'name' => '0-200',
    ],
[
    'from' => 200,
    'to' => 400,
    'name' => '200-400',
],
      [
      'from' => 400,
      'to' => 600,
      'name' => '400-600',
      ],
    ];
}

    /**
    *  推广状态停用
    * updateStatus coupon
    */
public function updateStatus($data, $id)
{
    $updatePopular = Popular::where('id', $id)
    ->update($data);

    if ($updatePopular) {
        //相应的下级状态也改变
        $populars = Popular::get();
        $arr = [];
        $this->findIdChild($populars, $arr, $id);

        foreach ($arr as $k => $v) {
            Popular::where('id', $v)->update($data);
        }

        if (Cache::get('populars')) {
                Cache::forget('populars');
        }
        return true;
    }
    return false;
}

    /**
    *  获取推广详情
    *  show coupon
    */
public function show($id)
{
    $showPopular = Popular::where('id', $id)
    ->first();

    return $showPopular;
}

    /**
     * 删除
     * delete coupon
     */
public function delete($id)
{
    return Popular::destroy($id);
}

public function deleteAll($ids)
{
    return Popular::whereIn('id', $ids)
    ->delete();
}

    /**
    * 搜索商品
    *
     * @param  [type] $key [description]
     * @return [type]      [description]
    */
public function search($key)
{
    // 搜索商品详情
    $data = Product::where('status', 1)
    ->where(function ($q1) use ($key) {
        $q1->orWhere('name', 'like', '%'.$key.'%')
           ->orWhere('snumber', 'like', '%'.$key.'%');
        return $q1;
    })
    ->select('id', 'name', 'snumber', 'main_image', 'product_price')
    ->orderBy('created_at', 'desc')
    ->paginate(config('amii.adminPaginate'));
    return $data;

}


}
