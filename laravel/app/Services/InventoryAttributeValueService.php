<?php
namespace App\Services;

use App\Models\InventoryAttributeValue;
use DB;

class InventoryAttributeValueService
{
    /**
     * 新增关联
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {
        return DB::table('inventories_attribute_values')->insert($arr);

    }

    /**
     * 删除关联
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
        return InventoryAttributeValue::destroy($id);
        
    }
}
