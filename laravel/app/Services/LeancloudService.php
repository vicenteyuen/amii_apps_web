<?php

namespace App\Services;

use Log;
use Config;
use Cache;
use GuzzleHttp\Client;
use App\Contracts\SmsContract;
use App\Contracts\PushContract;
use App\Helpers\SnumHelper;

class LeancloudService implements SmsContract, PushContract
{
    private $url;
    private $appId;
    private $appKey;
    private $masterKey;
    private $prod;
    private $client;

    public function __construct(array $provider)
    {
        $provider = isset($provider[0]) ? $provider[0] : null;

        switch ($provider) {
            case 'sms':
                $cfg = config('amii.leancloud.sms');
                break;
            case 'push':
                $cfg = config('amii.leancloud.push');
                break;
            case 'count':
                $cfg = config('amii.leancloud.count');
                break;

            default:
                throw new \Exception('The provider of leancloud does not config');
                break;
        }
        $this->url = $cfg['url'];
        $this->appId = $cfg['appid'];
        $this->appKey = $cfg['appkey'];
        $this->masterKey = $cfg['appmaster'];
        $this->prod = $cfg['apns_production'] ? 'prod' : 'dev';
        $this->client = new Client([
            'allow_redirects' => false,
            'http_errors' => false,
            'base_uri' => $this->url,
            'headers' => [
                'User-Agent' => 'Funshow (ganguo)',
                'X-LC-Id' => $this->appId,
                'X-LC-Key' => $this->masterKey ? ($this->masterKey.',master') : $this->appKey,
            ],
            'timeout' => '60',
        ]);
    }

    private function handleResponse($response)
    {
        if (substr($response->getStatusCode(), 0, 1) != 2) {
            $message = (string) $response->getBody();
            error_log($message);
            Log::error($message);

            return false;
        }

        return true;
    }

    public function requestSmsCode($phone, $provider = 'custom', $data = [])
    {
        if (config('amii.leancloud.sms.debug')) {
            return true;
        }

        if (config('amii.sms.service') == 'MONTENT') {
            return app('montent')->requestSmsCode($phone, $provider, $data);
        }

        $jsonArr = [
            'mobilePhoneNumber' => $phone
        ];

        if ($provider == 'custom') {
            $code = SnumHelper::snum(4);
            $jsonArr['aCode'] = $code;
            $jsonArr['template'] = config('amii.leancloud.sms.template');

            // 验证码存到缓存中
            $cacheKey = $phone . '_sms_' . $code;
            Cache::put($cacheKey, bcrypt($code), 10);
        } elseif ($provider == 'sendPswd') {
            // 发送密码
            $jsonArr['template'] = config('amii.leancloud.sms.sendPswdTemplate');
            $psd = $data['pswd'];
            $jsonArr['pswd'] = $psd;
            $jsonArr['appLoadUrl'] = config('amii.app.download.url');
        }

        $response = $this->client->post('requestSmsCode', [
            'json' => $jsonArr,
        ]);

        return $this->handleResponse($response);
    }

    public function verifySmsCode($phone, $code, $provider = 'custom')
    {
        if (config('amii.leancloud.sms.debug')) {
            return true;
        }

        if (config('amii.sms.service') == 'MONTENT') {
            return app('montent')->verifySmsCode($phone, $code, $provider);
        }

        if ($provider == 'custom') {
            // 校验
            $cacheKey = $phone . '_sms_' . $code;
            if (! \Hash::check($code, Cache::get($cacheKey))) {
                return false;
            }

            // 清除校验码
            Cache::forget($cacheKey);
            return true;
        }

        $response = $this->client->post('verifySmsCode/'.$code.'?mobilePhoneNumber='.$phone, [
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        return $this->handleResponse($response);
    }

    /**
     * push data format
     */
    private function formatData(array $data)
    {
        $title      = $data['title'];
        $content    = isset($data['content']) ? $data['content'] : null;
        $notifyData = isset($data['notifyData']) ? $data['notifyData'] : null;

        $iosContent = [
            'title' => $title,
            'body' => $content
        ];

        $data = [
            'ios' => [
                'badge'  => 'Increment',
                'action' => 'com.amii.app',
                'alert'  => $iosContent,
                'sound'  => 'default',
                'content-available' => '',
                'notify-data' => $notifyData
            ],
            'android' => [
                'action' => 'com.amii.app',
                'alert'  => $content,
                'title'  => $title,
                'notify-data' => $notifyData,
                'silent' => false
            ]
        ];
        return $data;
    }

    // 指定用户推送
    public function push($userId, array $data)
    {
        // 用户id获取uuid
        $user = \App\Models\User::where('id', $userId)
            ->first();
        if (! $user) {
            return false;
        }
        $channel = $uuid = $user->uuid;
        return $this->pushChannel($channel, $data);

        $data = self::formatData($data);
        $response = $this->client->post('push', [
            'json' => [
                'where' => ['user_id' => ''.$userId],
                'prod' => $this->prod,
                'data' => $data,
                'expiration_interval' => 86400,
            ],
        ]);

        return $this->handleResponse($response);
    }

    // 全局推送
    public function globalPush(array $data)
    {
        $data = self::formatData($data);
        $response = $this->client->post('push', [
            'json' => [
                'prod' => $this->prod,
                'data' => $data,
                'expiration_interval' => 86400,
            ],
        ]);

        return $this->handleResponse($response);
    }
    public function pushChannel($channel, array $data)
    {
        $data['badge'] = 'Increment';
        $data['action'] = 'com.amii.app';
        $data = self::formatData($data);
        $response = $this->client->post('push', [
            'json' => [
                'channels' => [$channel],
                'prod' => $this->prod,
                'data' => $data,
                'expiration_interval' => 86400,
            ],
        ]);

        return $this->handleResponse($response);
    }
}
