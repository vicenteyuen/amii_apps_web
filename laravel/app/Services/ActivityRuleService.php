<?php

namespace App\Services;

use App\Models\ActivityRule;

class ActivityRuleService
{
    /**
     * 获取活动规则列表
     */
    public function adminIndex()
    {
        $items = ActivityRule::paginate(config('amii.apiPaginate'));
        return $items;
    }

    /**
     * 获取活动规则列表
     */
    public function index()
    {
        $items = ActivityRule::where('status', 1)
            ->paginate(config('amii.apiPaginate'));
        return $items;
    }

    /**
     * 添加活动规则
     * @param  array  $data 活动规则数据
     * @return bool
     */
    public function store(array $data)
    {
        return ActivityRule::create($data);
    }

    /**
     * 获取活动规则详情
     * @param  integer $id 活动规则id
     */
    public function show($id)
    {
        return ActivityRule::find($id);
    }

    /**
    * 获取制定的活动数据
    */
    public function find($id)
    {
        return ActivityRule::find($id);
    }

    /**
     * 删除活动规则
     * @param  integer $id 活动规则id
     * @return bool
     */
    public function delete($id)
    {
        return ActivityRule::destroy($id);
    }

    public function update(array $arr, $id)
    {
        return ActivityRule::where('id', $id)
            ->update($arr);
    }
}
