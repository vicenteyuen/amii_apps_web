<?php

namespace App\Services\AmiiTaobaoTop;

use App\Models\TaobaoOpen\TaobaoTopProductHandle;
use App\Jobs\TaobaoOpenJob;
use Ecommerce\Models\Product;

class SellPointService
{
    /**
     * 接受command任务
     */
    public function commandHandle()
    {
        self::handleSellPoint();
    }

    /**
     * 已发布商品查询
     * @param integer $id 已处理最大id
     */
    public function handleSellPoint()
    {
        $fromId = 0;
        if (func_num_args()) {
            $fromId = func_get_arg(0);
        }

        // 获取未处理的条目
        $itemsPaginate = TaobaoTopProductHandle::where('id', '>', $fromId)
            ->orderBy('id')
            ->paginate(config('amii.taobaoPaginate'));

        $idArr = $itemsPaginate->pluck('id')
            ->toArray();
        if (empty($idArr)) {
            return;
        }

        // 处理条目卖点
        foreach ($idArr as $id) {
            $data = compact('id');
            $job = new TaobaoOpenJob('handleItemSellPointJob', $data);
            dispatch($job);
        }

        $items = $itemsPaginate->toArray();
        $nextId = 0;
        if ($items['next_page_url']) {
            $nextId = end($idArr);
        }

        if ($nextId) {
            $data = compact('nextId');
            $job = new TaobaoOpenJob('handleSellPointJob', $data);
            dispatch($job);
        }
    }

    /**
     * 处理条目卖点
     */
    public function handleItemSellPoint($id)
    {
        $item = TaobaoTopProductHandle::where('id', $id)
            ->first();
        if (! $item) {
            return;
        }

        // 淘宝商品详情数据
        $itemValueArr = $item->value;
        // 提取卖点
        $sellPoint = app('taobao.open.product.handle.amii')->getSellPoint($itemValueArr);
        if (! $sellPoint) {
            return;
        }
        // snumber
        $snumber = $item->taobao_outer_id;

        Product::where('snumber', $snumber)
            ->update(['sell_point' => $sellPoint]);
    }
}
