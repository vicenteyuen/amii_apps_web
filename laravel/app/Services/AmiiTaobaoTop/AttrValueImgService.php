<?php

namespace App\Services\AmiiTaobaoTop;

use App\Models\TaobaoOpen\TaobaoTopProductHandle;
use App\Jobs\TaobaoOpenJob;
use App\Models\Attribute;
use App\Models\AttributeValue;
use Ecommerce\Models\Inventory;
use App\Models\SellAttribute;
use App\Models\SellAttributeValue;

class AttrValueImgService
{
    /**
     * 接受command任务
     */
    public function commandHandle()
    {
        self::handleAttrValueImg();
    }

    /**
     * 处理属性值图
     */
    public function handleAttrValueImg()
    {
        $fromId = 0;
        if (func_num_args()) {
            $fromId = func_get_args(0);
        }

        // 获取未处理条目
        $itemsPaginate = TaobaoTopProductHandle::where('id', '>', $fromId)
            ->orderBy('id')
            ->paginate(config('amii.taobaoPaginate'));

        $idArr = $itemsPaginate->pluck('id')
            ->toArray();
        if (empty($idArr)) {
            return;
        }

        // 处理属性值图
        foreach ($idArr as $id) {
            $data = compact('id');
            $job = new TaobaoOpenJob('handleItemAttrValueImgJob', $data);
            dispatch($job);
        }

        $items = $itemsPaginate->toArray();
        $nextId = 0;
        if ($items['next_page_url']) {
            $nextId = end($idArr);
        }

        if ($nextId) {
            $data = compact('nextId');
            $job = new TaobaoOpenJob('handleAttrValueImgJob', $data);
            dispatch($job);
        }
    }

    /**
     * 处理属性值图
     */
    public function handleItemAttrValueImg($id)
    {
        $item = TaobaoTopProductHandle::where('id', $id)
            ->first();
        if (! $item) {
            return;
        }

        // 淘宝商品详情数据
        $itemValueArr = $item->value;

        // sku获取
        $sku = app('taobao.open.product.handle.amii')->getSkus($itemValueArr);
        // sku数据处理
        if (! empty($sku)) {
            $handledSku = app('taobao.open.sku.amii')->handleSkus($sku);
        } else {
            $handledSku = [];
        }
        if (empty($handledSku)) {
            return;
        }

        // pvImgs
        $pvImgs = app('taobao.open.product.handle.amii')->getPvImgs($itemValueArr);

        foreach ($handledSku['items'] as $sku) {
            $attrItemsHandle = self::getAttrIdAttrValueId($sku['attr']);
            if ($attrItemsHandle[0] === 0) {
                $attrItems = $attrItemsHandle[1];
                foreach ($attrItems as $attr) {
                    if (isset($pvImgs[$attr['pvStr']]) && ! empty($pvImgs[$attr['pvStr']])) {
                        $attrId = $attr['attrKeyId'];
                        $attrValueId = $attr['attrValueId'];
                        $url = $pvImgs[$attr['pvStr']];
                        $skuCode = $sku['outer_id'];

                        $data = compact('skuCode', 'attrId', 'attrValueId', 'url');
                        $job = new TaobaoOpenJob('attrValueImgHandleJob', $data);
                        dispatch($job);
                    }
                }
            }
        }
    }

    /**
     * 对应属性属性值id
     */
    public function getAttrIdAttrValueId($attrArr)
    {
        $idArr = [];
        foreach ($attrArr as $attr) {
            $attrKey = $attr['attrKey'];
            $attrValue = $attr['attrValue'];
            // 属性
            $checkAttr = Attribute::where('name', $attrKey)
                ->first();
            if (! $checkAttr) {
                continue;
            }
            $attrKeyId = $checkAttr->id;
            $attrKeyId = 1;
            // 属性值
            $checkAttrValue = AttributeValue::where('value', $attrValue)
                ->where('attribute_id', $attrKeyId)
                ->first();
            if (! $checkAttrValue) {
                continue;
            }
            $attrValueId = $checkAttrValue->id;

            $pvStr = $attr['pvStr'];

            $idArr[] = compact('attrKeyId', 'attrValueId', 'pvStr');
        }
        if (empty($idArr)) {
            return array(90001);
        }
        return array(0, $idArr);
    }

    /**
     * 处理属性值图
     */
    public function attrValueImgHandle($skuCode, $attrId, $attrValueId, $url)
    {
        // 通过库存编号获取sell product id
        $inventorys = Inventory::where('sku_code', $skuCode)
            ->orWhere('sku_code', $skuCode . 'YS001')
            ->with('omsSellProduct')
            ->get();
        $sellProductIdsArr = [];
        foreach ($inventorys as $inventory) {
            if ($inventory->omsSellProduct) {
                $sellProductIdsArr[] = $inventory->omsSellProduct->id;
            }
        }
        if (empty($sellProductIdsArr)) {
            return;
        }

        // 通过sell product id 获取 sell attribute id
        $sellAttributeIdsArr = SellAttribute::where('attribute_id', $attrId)
            ->whereIn('product_sell_attribute_id', $sellProductIdsArr)
            ->pluck('id')
            ->toArray();
        if (empty($sellAttributeIdsArr)) {
            return;
        }
        // 更新库存图
        $name = app('amii.taobao.open.image')->imageStore($url, 'product');

        if ($name) {
            SellAttributeValue::where('attribute_id', $attrId)
                ->where('attribute_value_id', $attrValueId)
                ->whereIn('sell_attribute_id', $sellAttributeIdsArr)
                ->update(['sell_product_attribute_value_image' => $name]);
        }
    }
}
