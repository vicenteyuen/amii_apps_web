<?php

namespace App\Services\AmiiTaobaoTop;

use App\Models\TaobaoOpen\TaobaoTopCategory;
use App\Models\TaobaoOpen\TaobaoTopProductHandle;
use App\Jobs\TaobaoOpenJob;

class CategoryService
{
    /**
     * 查询分类 或 从淘宝获取
     */
    public function findCateOrGetFromTaobao($cid, $taobaoInventoryId)
    {
        $checkItem = TaobaoTopCategory::where('cid', $cid)
            ->first();
        if (! $checkItem) {
            self::getCatFromTaobao($cid, $taobaoInventoryId);
            return;
        }
        TaobaoTopProductHandle::where('id', $taobaoInventoryId)
            ->update([
                'check_cate' => 1
            ]);
        app('taobao.open.product.amii')->publish($taobaoInventoryId);
    }

    /**
     * 从淘宝获取类目信息并保存
     */
    public function getCatFromTaobao($cid, $taobaoInventoryId, $isParent = false)
    {
        $resCats = app('amii.taobao.open.category')->taobaoItemcatsGet($cid);
        if ($resCats) {
            $cats = isset($resCats['item_cats']) ? $resCats['item_cats'] : [];
            if (! empty($cats)) {
                $cat = isset($cats['item_cat']) ? $cats['item_cat'][0] : [];
                if (! empty($cat)) {
                    $cid = $cat['cid'];
                    $name = $cat['name'];
                    $parentCid = $cat['parent_cid'];
                    $checkItem = TaobaoTopCategory::where('cid', $cid)
                        ->first();
                    if (! $checkItem) {
                        TaobaoTopCategory::create([
                            'cid' => $cid,
                            'name' => $name,
                            'parent_cid' => $parentCid
                        ]);
                    }
                    if (! $cat['is_parent']) {
                        // 查询父类目
                        $id = $parentCid;
                        $data = compact('id');
                        $job = new TaobaoOpenJob('resCatJob', $data);
                        dispatch($job);
                        return;
                    } else {
                        TaobaoTopProductHandle::where('id', $taobaoInventoryId)
                            ->update([
                                'check_cate' => 1
                            ]);
                        app('taobao.open.product.amii')->publish($taobaoInventoryId);
                        return;
                    }
                }
            }
        }
        TaobaoTopProductHandle::where('id', $taobaoInventoryId)
            ->update([
                'check_cate' => 2
            ]);
        app('taobao.open.product.amii')->publish($taobaoInventoryId);
    }

    /**
     * 获取淘宝分类
     */
    public function index()
    {
        return TaobaoTopCategory::orderBy('id', 'desc')
            ->get();
    }

    /**
     * 淘宝分类映射系统分类
     */
    public function mapCate($taobaoCateId, $cateId)
    {
        return TaobaoTopCategory::where('id', $taobaoCateId)
            ->update([
                'category_id' => $cateId
            ]);
    }
}
