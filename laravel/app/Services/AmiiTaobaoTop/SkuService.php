<?php

namespace App\Services\AmiiTaobaoTop;

class SkuService
{
    /**
     * sku数据处理属性属性值
     */
    public function handleSkus(array $skus)
    {
        $items = [];
        $num = 0;
        foreach ($skus as $sku) {
            $tmpArr = [];
            $tmpArr['outer_id'] = isset($sku['outer_id']) ? $sku['outer_id'] : '';
            $tmpArr['price'] = $sku['price'];
            $tmpArr['quantity'] = $sku['quantity'];
            $tmpArr['attr'] = self::getAttr($sku);

            $num += $sku['quantity'];
            $items[] = $tmpArr;
        }

        return compact('num', 'items');
    }

    /**
     * 从淘宝商品sku中获取属性属性值信息
     */
    public function getAttr(array $sku)
    {
        $attrItems = [];
        $attrStr = $sku['properties_name'];
        $attrArr = explode(';', $attrStr);
        foreach ($attrArr as $attr) {
            $attrItemArr = explode(':', $attr);
            $pvStr = $attrItemArr[0] . ':' . $attrItemArr[1];
            $attrKey = $attrItemArr[2];
            $attrValue = $attrItemArr[3];
            $attrItems[] = compact('attrKey', 'attrValue', 'pvStr');
        }

        return $attrItems;
    }
}
