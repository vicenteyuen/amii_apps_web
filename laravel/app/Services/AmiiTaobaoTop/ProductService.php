<?php

namespace App\Services\AmiiTaobaoTop;

use App\Models\TaobaoOpen\TaobaoTopInventory;
use App\Models\TaobaoOpen\TaobaoTopProductHandle;
use App\Jobs\TaobaoOpenJob;
use Ecommerce\Models\Product;
use App\Models\SellProduct;
use App\Models\ProductImage;
use EcommerceManage\Models\Category;
use App\Models\ProductCategories;
use EcommerceManage\Models\Brand;
use App\Models\BrandProduct;
use App\Models\BrandCategory;
use Ecommerce\Models\Inventory;
use App\Models\InventoryAttributeValue;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\SellAttribute;
use App\Models\SellAttributeValue;
use App\Models\TaobaoOpen\TaobaoTopCategory;
use Carbon\Carbon;

class ProductService
{
    public function storeInventory($merchant, array $data)
    {
        // 获取outer_id
        $outerId = isset($data['outer_id']) ? $data['outer_id'] : '';

        // check if exist
        $checkItem = TaobaoTopInventory::where([
            'merchant' => $merchant,
            'taobao_outer_id' => $outerId,
        ])
        ->first();
        if ($checkItem && $outerId != '') {
            // 商品已存在 且外部id不为空
            return;
        }

        // 保存商品信息
        $create = TaobaoTopInventory::create([
            'merchant' => $merchant,
            'taobao_outer_id' => $outerId,
            'value' => $data,
        ]);

        // 进入商品处理队列
        $numIid = isset($data['num_iid']) ? $data['num_iid'] : '';
        $taobaoTopInventoryId = $create->id;
        $arr = compact('merchant', 'numIid', 'taobaoTopInventoryId');
        $job = new TaobaoOpenJob('amiiStoreProductJob', $arr);
        dispatch($job);
    }

    /**
     * 发布商品
     */
    public function publish($taobaoTopProductHandleId)
    {
        $item = TaobaoTopProductHandle::where('id', $taobaoTopProductHandleId)
            ->duringPublish()
            ->first();
        if (! $item) {
            return;
        }

        $merchant = $item->merchant;
        // 淘宝商品详情数据
        $itemValueArr = $item->value;

        // sku获取
        $sku = app('taobao.open.product.handle.amii')->getSkus($itemValueArr);
        // sku数据处理
        if (! empty($sku)) {
            $handledSku = app('taobao.open.sku.amii')->handleSkus($sku);
        } else {
            $handledSku = [];
        }
        if (empty($handledSku)) {
            return;
        }

        // snumber
        $snumber = $item->taobao_outer_id;

        // cid获取
        $cid = app('taobao.open.product.handle.amii')->getCid($itemValueArr);

        // 无线商品详情拼接
        $desc = self::handleDesc($item->handle_wap_vision);

        // price
        $price = app('taobao.open.product.handle.amii')->getPrice($itemValueArr);
        // pvImgs
        $pvImgs = app('taobao.open.product.handle.amii')->getPvImgs($itemValueArr);

        // 发布商品
        // title
        $title = app('taobao.open.product.handle.amii')->getTitle($itemValueArr);
        // isDelist
        $isDelist = app('taobao.open.product.handle.amii')->isDelist($itemValueArr);
        $productStatus = $isDelist ? 0 : 1;
        // 默认下架淘宝同步商品
        $productStatus = 0;

        // 商品图片
        $imgs = $item->handle_picture;
        $indexImg = empty($imgs) ? '' : $imgs[0];

        $checkProduct = Product::where('snumber', $snumber)
            ->first();
        if ($checkProduct) {
            return;
        }

        // 卖点提取
        $sellPoint = app('taobao.open.product.handle.amii')->getSellPoint($itemValueArr);

        $data = [
            'name' => $title,
            'sell_point' => $sellPoint,
            'status' => $productStatus,
            'snumber' => $snumber,
            'number' => 0,
            'main_image' => $indexImg,
            'detail' => $desc,
            'taobao_product_id' => $item->taobao_product_id
        ];
        $product = Product::firstOrCreate($data);

        // 普通商品
        $sellProduct = SellProduct::firstOrCreate([
            'product_id' => $product->id,
            'provider' => 1,
            'status' => $productStatus,
            'image' => $indexImg,
            'base_price' => $price,
            'market_price' => $price,
            'number' => isset($handledSku['num']) ? $handledSku['num'] : 0
        ]);

        // 商品图片
        foreach ($imgs as $key => $img) {
            if ($key == 0) {
                continue;
            }
            ProductImage::create([
                'product_id' => $product->id,
                'image' => $img
            ]);
        }

        // 商品和品牌绑定并返回系统品牌id
        $brandId = self::productBundBrand($product->id, $merchant);

        // 商品和分类绑定
        self::productBundCate($product->id, $cid, $brandId);

        // 库存发布
        self::publishSkus($product->id, $sellProduct->id, $handledSku['items'], $pvImgs);

        // 更新淘宝商品已发布状态
        $item->update([
            'is_publish' => 1
        ]);
    }

    /**
     * 更新商品基础信息
     */
    public function updateProduct($taobaoTopProductHandleId)
    {
        $item = TaobaoTopProductHandle::where('id', $taobaoTopProductHandleId)
            ->duringUpdate()
            ->first();
        if (! $item) {
            return;
        }
        // 商户
        $merchant = $item->merchant;
        // snumber
        $snumber = $item->taobao_outer_id;

        $checkProduct = Product::where('snumber', $snumber)
            ->first();
        if (! $checkProduct) {
            // 商品不存在
            return;
        }

        $productId = $checkProduct->id;

        // 淘宝商品详情数据
        $itemValueArr = $item->value;

        // 无线商品详情拼接
        $desc = self::handleDesc($item->handle_wap_vision);

        // price
        $price = app('taobao.open.product.handle.amii')->getPrice($itemValueArr);
        // pvImgs
        $pvImgs = app('taobao.open.product.handle.amii')->getPvImgs($itemValueArr);

        // 发布商品
        // title
        $title = app('taobao.open.product.handle.amii')->getTitle($itemValueArr);
        // isDelist
        $isDelist = app('taobao.open.product.handle.amii')->isDelist($itemValueArr);
        $productStatus = $isDelist ? 0 : 1;

        // 商品图片
        $imgs = $item->handle_picture;
        $indexImg = empty($imgs) ? '' : $imgs[0];

        $updateProductDatas = Product::where('snumber', $snumber)
            ->update([
                'name' => $title,
                'status' => $productStatus,
                'main_image' => $indexImg,
                'detail' => $desc,
                'taobao_product_id' => $item->taobao_product_id
            ]);

        $updateSellProductDatas = SellProduct::where('product_id', $productId)
            ->update([
                'product_id' => $productId,
                'provider' => 1,
                'status' => $productStatus,
                'image' => $indexImg,
                'base_price' => $price,
                'market_price' => $price,
            ]);
        // 商品图片
        ProductImage::where('product_id', $productId)
            ->delete();
        foreach ($imgs as $key => $img) {
            if ($key == 0) {
                continue;
            }
            ProductImage::create([
                'product_id' => $productId,
                'image' => $img
            ]);
        }

        // 更新淘宝商品已发布状态
        $item->update([
            'is_publish' => 1,
            'sync_update_end_time' => Carbon::now()
        ]);
    }

    /**
     * 无线商品html拼接
     */
    public function handleDesc($wapVision)
    {
        if (! is_array($wapVision)) {
            return '';
        }
        $str = '';
        foreach ($wapVision as $vision) {
            $str .= '<img src="/' . $vision . '" style="width: 100%; height: auto">';
        }
        return $str;
    }

    /**
     * 商品绑定分类
     */
    public function productBundCate($productId, $cid, $brandId)
    {
        // taobao分类查询
        $item = TaobaoTopCategory::where('cid', $cid)
            ->with('parent')
            ->first();
        if (! ($item && $item->category)) {
            return;
        }

        $checkCate = Category::where('id', $item->category->id)
            ->first();
        if (! $checkCate) {
            return;
        }

        // 分类和商品绑定
        ProductCategories::firstOrCreate([
            'product_id' => $productId,
            'category_id' => $checkCate->id,
            'brand_id' => $brandId
        ]);

        ProductCategories::firstOrCreate([
            'product_id' => $productId,
            'category_id' => $checkCate->id,
            'brand_id' => 0
        ]);

        // 分类和品牌绑定
        BrandCategory::firstOrCreate([
            'brand_id' => $brandId,
            'category_id' => $checkCate->id
        ]);
    }

    /**
     * 商品绑定品牌
     */
    public function productBundBrand($productId, $merchant)
    {
        switch ($merchant) {
            case 'amii':
                $deepMerchantId = '1024';
                $merchantNmae = 'AMII';
                break;
            case 'amiiR':
                $deepMerchantId = '1025';
                $merchantNmae = 'Amii Redefine';
                break;
            case 'amiiM':
                $deepMerchantId = '992';
                $merchantNmae = '摩尼菲格';
                break;
            case 'amiiC':
                $deepMerchantId = '981';
                $merchantNmae = 'AMII童装';
                break;

            default:
                $deepMerchantId = '';
                $merchantNmae = '';
                break;
        }
        $brand = Brand::where(function ($query) use ($merchant, $deepMerchantId) {
            $query->where('taobao_brand', $merchant)
                ->orWhere('merchant_id', $deepMerchantId);
        })
        ->first();
        if ($brand) {
            if (! $brand->taobao_brand) {
                $brand->update([
                    'taobao_brand' => $merchant
                ]);
            }
            $brandId = $brand->id;
        } else {
            // 新增品牌
            $newCreate = Brand::create([
                'taobao_brand' => $merchant,
                'name' => $merchantNmae
            ]);
            $brandId = $newCreate->id;
        }
        // 绑定
        BrandProduct::firstOrCreate([
            'brand_id' => $brandId,
            'product_id' => $productId
        ]);
        return $brandId;
    }

    /**
     * 发布库存
     */
    public function publishSkus($productId, $sellProductId, $skus, $pvImgs)
    {
        foreach ($skus as $sku) {
            // 属性属性值处理
            $attrItems = self::publishAttr($sku['attr']);
            // 库存发布
            $inventoryId = self::publishInventory($sku, $attrItems, $productId, $sellProductId, $pvImgs);
        }
    }

    /**
     * 系统属性属性值
     */
    public function publishAttr($attrArr)
    {
        $idArr = [];
        foreach ($attrArr as $attr) {
            $attrKey = $attr['attrKey'];
            $attrValue = $attr['attrValue'];
            // 属性
            $checkAttr = Attribute::where('name', $attrKey)
                ->first();
            if ($checkAttr) {
                $attrKeyId = $checkAttr->id;
            } else {
                $newCreateAttr = Attribute::create([
                    'name' => $attrKey
                ]);
                $attrKeyId = $newCreateAttr->id;
            }
            // 属性值
            $checkAttrValue = AttributeValue::where('value', $attrValue)
                ->where('attribute_id', $attrKeyId)
                ->first();
            if ($checkAttrValue) {
                $attrValueId = $checkAttrValue->id;
            } else {
                $newCreateAttrValue = AttributeValue::create([
                    'value' => $attrValue,
                    'attribute_id' => $attrKeyId
                ]);
                $attrValueId = $newCreateAttrValue->id;
            }

            $pvStr = $attr['pvStr'];

            $idArr[] = compact('attrKeyId', 'attrValueId', 'pvStr');
        }
        return $idArr;
    }

    /**
     * publish inventory
     */
    public function publishInventory($sku, $attrItems, $productId, $sellProductId, $pvImgs)
    {
        $attrValueIdStr = '';
        foreach ($attrItems as $attrItem) {
            $attrValueIdStr .= $attrItem['attrValueId'] . '_';
            // 库存属性
            $checkSellAttr = SellAttribute::where('product_sell_attribute_id', $sellProductId)
                ->where('attribute_id', $attrItem['attrKeyId'])
                ->first();
            if ($checkSellAttr) {
                $checkSellAttrId = $checkSellAttr->id;
            } else {
                $newCreateSellAttr = SellAttribute::create([
                    'attribute_id' => $attrItem['attrKeyId'],
                    'product_sell_attribute_id' => $sellProductId
                ]);
                $checkSellAttrId = $newCreateSellAttr->id;
            }
            // 库存属性值
            $sellAttrValue = SellAttributeValue::firstOrCreate([
                'sell_attribute_id' => $checkSellAttrId,
                'attribute_value_id' => $attrItem['attrValueId'],
                'attribute_id' => $attrItem['attrKeyId']
            ]);
            // 属性值图
            if (isset($pvImgs[$attrItem['pvStr']])) {
                $sellAttrValueId = $sellAttrValue->id;
                $url = $pvImgs[$attrItem['pvStr']];
                $data = compact('sellAttrValueId', 'url');
                $job = new TaobaoOpenJob('inventoryAttrValueImgJob', $data);
                dispatch($job);
            }
        }
        // attr value id sort
        $attrValueIdArr = explode('_', substr($attrValueIdStr, 0, -1));
        sort($attrValueIdArr);
        $attrValueIdStr = implode('_', $attrValueIdArr);
        $inventory = Inventory::firstOrCreate([
            'product_id' => $productId,
            'sku_code' => $sku['outer_id'],
            'value_key' => $attrValueIdStr,
            'mark_price' => $sku['price'],
            'number' => $sku['quantity'],
            'product_sell_id' => $sellProductId
        ]);
        // 库存图片校验
        foreach ($sku['attr'] as $skuAttr) {
            if (isset($pvImgs[$skuAttr['pvStr']])) {
                $pvImgUrl = $pvImgs[$skuAttr['pvStr']];
                if ($pvImgUrl) {
                    $url = $pvImgUrl;
                    $inventoryId = $inventory->id;
                    $data = compact('inventoryId', 'url');
                    $job = new TaobaoOpenJob('inventoryImgJob', $data);
                    dispatch($job);
                }
                break;
            }
        }
        foreach ($attrValueIdArr as $attrValueIdItemId) {
            InventoryAttributeValue::firstOrCreate([
                'inventory_id' => $inventory->id,
                'attribute_value_id' => $attrValueIdItemId
            ]);
        }
    }

    /**
     * 更新库存图片
     */
    public function inventoryImg($id, $url)
    {
        $name = app('amii.taobao.open.image')->imageStore($url, 'product');

        if ($name) {
            Inventory::where('id', $id)
                ->update([
                    'image' => $name
                ]);
        }
    }

    /**
     * 更新库存属性值图
     */
    public function inventoryAttrValueImg($sellAttrValueId, $url)
    {
        $name = app('amii.taobao.open.image')->imageStore($url, 'product');

        if ($name) {
            SellAttributeValue::where('id', $sellAttrValueId)
                ->update([
                    'sell_product_attribute_value_image' => $name
                ]);
        }
    }
}
