<?php

namespace App\Services\AmiiTaobaoTop;

use App\Jobs\TaobaoOpenJob;
use App\Models\TaobaoOpen\TaobaoTopProductHandle;
use Carbon\Carbon;
use Ecommerce\Models\Product;
use App\Models\Attribute;
use App\Models\AttributeValue;
use Ecommerce\Models\Inventory;

class ProductHandleService
{
    /**
     * 保存淘宝商品条目信息
     */
    public function storeTaobaoProductHandle($merchant, $taobaoTopInventoryId, array $data)
    {
        // 获取淘宝商品外部id
        $outerId = self::getOuterId($data);
        // 获取淘宝商品数字id
        $numIid = self::getNumIid($data);
        // 获取淘宝商品类目cid
        $cid = self::getCid($data);

        // 校验是否存在记录
        $checkItem = TaobaoTopProductHandle::where([
            'taobao_product_id' => $numIid,
            'merchant' => $merchant
        ])
        ->first();
        if ($checkItem) {
            return;
        }

        $create = TaobaoTopProductHandle::create([
            'taobao_top_inventory_id' => $taobaoTopInventoryId,
            'merchant' => $merchant,
            'taobao_outer_id' => $outerId,
            'taobao_product_id' => $numIid,
            'value' => $data
        ]);

        // 商品发布到系统前数据处理
        if ($create) {
            // 处理图片
            self::taobaoProductPic($create);
            // 处理详情
            self::taobaoProductWapVision($create);
            // sku数据
            $skus = self::getSkus($create->value);
            if (empty($skus)) {
                $create->update([
                    'check_skus' => 2
                ]);
            } else {
                $create->update([
                    'check_skus' => 1
                ]);
            }
            // 类目
            app('taobao.open.category.amii')->findCateOrGetFromTaobao($cid, $create->id);
        }
    }

    /**
     * 获取淘宝商品数据中的外部id
     */
    public function getOuterId(array $data)
    {
        if (isset($data['item']) && isset($data['item']['outer_id'])) {
            return $data['item']['outer_id'];
        }
        return '';
    }

    /**
     * 获取淘宝商品数据中的商品数字id
     */
    public function getNumIid(array $data)
    {
        if (isset($data['item']) && isset($data['item']['num_iid'])) {
            return $data['item']['num_iid'];
        }
        return '';
    }

    /**
     * 获取淘宝商品类目cid
     */
    public function getCid(array $data)
    {
        if (isset($data['item']) && isset($data['item']['cid'])) {
            return $data['item']['cid'];
        }
        return '';
    }

    /**
     * 获取淘宝商品条目信息中图片信息
     */
    public function getPicData(array $data)
    {
        if (isset($data['item']) && isset($data['item']['item_imgs']) && isset($data['item']['item_imgs']['item_img'])) {
            return $data['item']['item_imgs']['item_img'];
        }
        return [];
    }

    /**
     * 获取淘宝商品条目信息中无线详情页数据
     */
    public function getWapVisionData(array $data)
    {
        if (isset($data['item']) && isset($data['item']['wireless_desc'])) {
            return $data['item']['wireless_desc'];
        }
        return '';
    }

    /**
     * 获取淘宝商品名
     */
    public function getTitle(array $data)
    {
        if (isset($data['item']) && isset($data['item']['title'])) {
            return $data['item']['title'];
        }
        return '';
    }

    /**
     * 淘宝商品下架状态
     */
    public function isDelist(array $data)
    {
        if (isset($data['item']) && isset($data['item']['delist_time'])) {
            if (Carbon::now() >= Carbon::parse($data['item']['delist_time'])) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取淘宝商品条目中sku库存信息
     */
    public function getSkus(array $data)
    {
        if (isset($data['item']) && isset($data['item']['skus']) && isset($data['item']['skus']['sku'])) {
            return $data['item']['skus']['sku'];
        }
        return [];
    }

    /**
     * 获取淘宝商品基本价格
     */
    public function getPrice(array $data)
    {
        if (isset($data['item']) && isset($data['item']['price'])) {
            return $data['item']['price'];
        }
        return 0.00;
    }

    /**
     * 获取淘宝商品属性属性值图片
     */
    public function getPvImgs(array $data)
    {
        if (isset($data['item']) && isset($data['item']['prop_imgs']) && isset($data['item']['prop_imgs']['prop_img'])) {
            $pvImgs = $data['item']['prop_imgs']['prop_img'];
            $rPvImgs = [];
            foreach ($pvImgs as $pvImg) {
                $url = $pvImg['url'];
                if (isset($pvImg['properties'])) {
                    $rPvImgs[$pvImg['properties']] = $url;
                }
            }
            return $rPvImgs;
        }
        return [];
    }

    /**
     * 获取淘宝商品卖点
     */
    public function getSellPoint(array $data)
    {
        if (isset($data['item']) && isset($data['item']['sell_point'])) {
            return $data['item']['sell_point'];
        }
        return '';
    }

    /**
     * 处理淘宝商品图片
     */
    public function taobaoProductPic($data, $isUpdate = false)
    {
        // 定义参数
        $taobaoTopInventoryId = $data->id;

        // 待处理数据获取图片信息
        $item = $data->value;
        $itemImgs = self::getPicData($item);
        if (empty($itemImgs)) {
            $data->update([
                'handle_picture_status' => 3
            ]);
            return;
        }
        // 图片处理到系统图
        $imgNum = count($itemImgs);
        foreach ($itemImgs as $img) {
            $imgData = $img;
            $data = compact('taobaoTopInventoryId', 'imgNum', 'imgData', 'isUpdate');
            $job = new TaobaoOpenJob('handlePicJob', $data);
            dispatch($job);
        }
    }

    /**
     * 处理淘宝商品详情
     */
    public function taobaoProductWapVision($data, $isUpdate = false)
    {
        // 定义参数
        $taobaoTopInventoryId = $data->id;
        // 待处理数据获取详情信息
        $item = $data->value;
        $itemWapVision = self::getWapVisionData($item);
        if ($itemWapVision) {
            $itemWapVision = substr($itemWapVision, 9, -10);
            $itemWapVisionHandle = preg_replace("/(<img)([^']*?)(>)(http|https)([^']*?)(<\/img>)/i", "\$4\$5_amii_", $itemWapVision);
            $itemWapVisionArr = explode('_amii_', $itemWapVisionHandle);
            if (! end($itemWapVisionArr)) {
                array_pop($itemWapVisionArr);
            }
            $visionNum = count($itemWapVisionArr);
            foreach ($itemWapVisionArr as $key => $vision) {
                $position = $key;
                $url = $vision;
                $data = compact('taobaoTopInventoryId', 'url', 'position', 'visionNum', 'isUpdate');
                $job = new TaobaoOpenJob('handleWapVisionJob', $data);
                dispatch($job);
            }
        }
    }

    /**
     * 处理并保存商品图片
     */
    public function handlePic($id, $imgData, $imgNum, $isUpdate)
    {
        // 图片下载并保存
        $imgUrl = $imgData['url'];
        $position = $imgData['position'];
        $store = app('amii.taobao.open.image')->imageStore($imgUrl, 'product');
        $item = TaobaoTopProductHandle::find($id);
        if ($item) {
            $handlePic = $item->handle_picture;
            $itemImgs = self::getPicData($item->value);
            if ($handlePic) {
                $handlePic[$position] = $store;
                $unHandlePic = $handlePic;
            } else {
                $unHandlePic = [
                    $position => $store
                ];
            }
            ksort($unHandlePic);
            $handlePictureStatus = 1;
            if ($imgNum == count($itemImgs)) {
                $handlePictureStatus = 2;
            }
            $item->update([
                'handle_picture' => $unHandlePic,
                'handle_picture_status' => $handlePictureStatus
            ]);
            if ($handlePictureStatus == 2) {
                if ($isUpdate) {
                    // 更新
                    app('taobao.open.product.amii')->updateProduct($id);
                } else {
                    // 校验是否可以发布
                    app('taobao.open.product.amii')->publish($id);
                }
            }
        }
    }

    public function handleWapVision($id, $url, $position, $visionNum, $isUpdate)
    {
        // 无线详情
        $item = TaobaoTopProductHandle::find($id);
        if (! $item) {
            return;
        }
        $handleWapVision = $item->handle_wap_vision;
        $store = app('amii.taobao.open.image')->imageStore($url, 'product');
        if ($handleWapVision) {
            $handleWapVision[$position] = $store;
            $unHandleWapVision = $handleWapVision;
        } else {
            $unHandleWapVision = [
                $position => $store
            ];
        }
        ksort($unHandleWapVision);
        $handleWapVisionStatus = 1;
        if ($visionNum == count($unHandleWapVision)) {
            $handleWapVisionStatus = 2;
        }
        $item->update([
            'handle_wap_vision' => $unHandleWapVision,
            'handle_wap_vision_status' => $handleWapVisionStatus
        ]);
        if ($handleWapVisionStatus == 2) {
            if ($isUpdate) {
                // 更新商品信息
                app('taobao.open.product.amii')->updateProduct($id);
            } else {
                // 校验是否可以发布
                app('taobao.open.product.amii')->publish($id);
            }
        }
    }

    /**
     * 更新淘宝商品信息
     */
    public function updateTaobaoProductHandle($merchant, array $data)
    {
        // 获取淘宝商品外部id
        $outerId = self::getOuterId($data);
        // 校验当前商品是否已存在
        $checkProduct = Product::where('snumber', $outerId)
            ->first();
        if (! $checkProduct) {
            self::storeTaobaoProductHandle($merchant, 0, $data);
            return;
        }
        $productId = $checkProduct->id;
        $sellProduct = $checkProduct->sellProduct
            ->where('provider', 1)
            ->first();
        if (! $sellProduct) {
            return;
        }
        $sellProductId = $sellProduct->id;

        // 获取淘宝商品数字id
        $numIid = self::getNumIid($data);

        $create = TaobaoTopProductHandle::create([
            'merchant' => $merchant,
            'taobao_outer_id' => $outerId,
            'taobao_product_id' => $numIid,
            'value' => $data,
            'sync_update_start_time' => Carbon::now(),
        ]);

        if ($create) {
            // 处理图片
            self::taobaoProductPic($create, true);
            // 处理详情
            self::taobaoProductWapVision($create, true);
            // 更新库存图
            self::taobaoUpdateInventorysImg($create, $productId, $sellProductId);
        }
    }

    /**
     * 更新商品库存图
     */
    public function taobaoUpdateInventorysImg($data, $productId, $sellProductId)
    {
        $item = $data->value;
        // sku获取
        $skus = app('taobao.open.product.handle.amii')->getSkus($item);
        // sku数据处理
        if (! empty($skus)) {
            $handledSku = app('taobao.open.sku.amii')->handleSkus($skus);
        } else {
            $handledSku = [];
        }
        if (empty($handledSku)) {
            return;
        }
        if (empty($handledSku['items'])) {
            return;
        }

        // pvImgs
        $pvImgs = app('taobao.open.product.handle.amii')->getPvImgs($item);

        // 如果需要更新系统商品sku库存则更新 否则不更新
        $inventorys = Inventory::where('product_id', $productId)
            ->where('product_sell_id', $sellProductId)
            ->get();
        if ($inventorys->isEmpty()) {
            app('taobao.open.product.amii')->publishSkus($productId, $sellProductId, $handledSku['items'], $pvImgs);
        }

        // 有库存图的加入处理
        foreach ($handledSku['items'] as $sku) {
            foreach ($sku['attr'] as $attr) {
                if (isset($pvImgs[$attr['pvStr']]) && ! empty($pvImgs[$attr['pvStr']])) {
                    // 加入库存图片处理
                    $skuCode = $sku['outer_id'];
                    $url = $pvImgs[$attr['pvStr']];
                    $data = compact('skuCode', 'url');
                    $job = new TaobaoOpenJob('inventorysImgJob', $data);
                    dispatch($job);
                }
            }
        }
    }

    /**
     * 按库存编号更新库存图
     */
    public function inventorysImg($skuCode, $url)
    {
        $name = app('amii.taobao.open.image')->imageStore($url, 'product');

        if ($name) {
            Inventory::where('sku_code', $skuCode)
                ->update([
                    'image' => $name
                ]);
        }
    }
}
