<?php

namespace App\Services;

use App\Models\SellProduct;

class QrcodeService
{
    /**
     * 获取二维码扫描结果
     */
    public function show($value)
    {
        return self::formatQrcodeValue($value);
    }

    /**
     * 二维码扫描数据处理
     */
    private function formatQrcodeValue($value)
    {
        // 当前只支持商品详情
        $provider = 'product';

        switch ($provider) {
            case 'product':
                $formatValue = self::formatProductValue($value);
                break;

            default:
                $formatValue = self::format($value);
                break;
        }

        if (! $formatValue) {
            return [
                70001,
                '暂无当前商品'
            ];
        }

        // 返回扫描结果
        return [
            0,
            [
                'provider' => $provider,
                'value' => $formatValue
            ]
        ];
    }

    /*!
     * 返回二维码扫描结果处理过后的数据
     * @param  string $value 二维码扫描数据字符串
     * @return integer        商品id
     */
    private function formatProductValue($value)
    {
        // 商品查询
        $product = SellProduct::where('status', 1)
            ->where('provider', 1)
            ->whereHas('product', function ($query) use ($value) {
                return $query->where('snumber', $value)
                    ->where('status', 1);
            })
            ->with('product')
            ->first();
        if ($product) {
            return $product->product->id;
        }

        // 商品搜索并挑选最符合的一个
        $product = SellProduct::select('product_id', 'image', 'base_price', 'market_price', 'number', 'comment_number', 'volume', 'orderby', 'id', 'praise')
            ->where('status', 1)
            ->where('provider', 1)
            ->whereHas('product', function ($query) use ($value) {
                return $query->where('snumber', $value);
            })
            ->with('product')
            ->first();
        if ($product) {
            return $product->product->id;
        } else {
            // 现阶段不返回推荐商品
            return 0;
        }

        // 返回当前系统推荐商品
        $product = SellProduct::select('product_id', 'image', 'base_price', 'market_price', 'number', 'comment_number', 'volume', 'orderby', 'id', 'praise')
            ->where('status', 1)
            ->where('provider', 1)
            ->whereHas('product', function ($query) use ($value) {
                return $query->where('status', 1);
            })
            ->addSelect(\DB::raw('volume * 0.7 + comment_number * praise * 0.3 /100 as default_order_by'))
            ->orderBy('default_order_by', 'desc')
            ->with('product')
            ->first();
        if ($product) {
            return $product->product->id;
        }
        return 0;
    }
}
