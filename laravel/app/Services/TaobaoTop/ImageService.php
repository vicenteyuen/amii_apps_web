<?php

namespace App\Services\TaobaoTop;

use Gimage\Gimage;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Helpers\AesHelper;

class ImageService
{
    /**
     * 下载图片并转存系统图片
     */
    public function imageStore($url, $provider)
    {
        if (empty($url)) {
            return;
        }

        if (! config('amii.taobaoopen.sync_image')) {
            return config('amii.image');
        }

        $taobaoImgPath = config('amii.imageLocalPath') . '/taobao/';
        if (! is_dir($taobaoImgPath)) {
            mkdir($taobaoImgPath, 0755, true);
        }

        $urlArr = explode('/', $url);
        $imageName = end($urlArr);

        // 图片格式格式化
        if (in_array(substr($imageName, -4), ['.ss2', '.SS2'])) {
            $imageName = substr($imageName, 0, -4) . '.png';
        }

        $taobaoImageName = $taobaoImgPath . $imageName; // image path

        $client = new Client();
        try {
            $response = $client->get($url);
            $image = $response->getBody()->getContents();
        } catch (\Exception $e) {
            \Log::error('download taobao open image ' . $url . 'failed');
            return '';
        }
        if (file_exists($taobaoImageName)) {
            unlink($taobaoImageName);
        }
        $fp = fopen($taobaoImageName, 'x');
        fwrite($fp, $image);
        fclose($fp);

        $image = new UploadedFile($taobaoImageName, $imageName);

        $store = Gimage::uploadImage($image, $provider, 'images');
        if ($store instanceof \Exception) {
            return false;
        }
        if (file_exists($taobaoImageName)) {
            unlink($taobaoImageName);
        }

        return $store;
    }
}
