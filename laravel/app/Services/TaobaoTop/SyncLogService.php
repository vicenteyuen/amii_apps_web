<?php

namespace App\Services\TaobaoTop;

use App\Helpers\TaobaoTopHelper;
use App\Helpers\TaobaoTopJobHelper;
use Carbon\Carbon;
use App\Models\TaobaoOpen\TaobaoTopSyncLog;

class SyncLogService
{
    /**
     * 定时同步淘宝商品
     */
    public function daliySync()
    {
        self::start('amii');
        self::start('amiiM');
        self::start('amiiR');
        self::start('amiiC');
    }
    /**
     * 触发同步
     * @param string $merchant amii amiiR amiiM amiiC
     */
    public function start($merchant = 'amii')
    {
        $startModifiedTime = null;
        $endModifiedTime = null;
        // 同步时间
        self::setSyncTime($merchant, $nearlyTime);

        // 有更新记录
        if ($nearlyTime) {
            $startModifiedTime = $nearlyTime;
            // 加入队列任务
            // TaobaoTopJobHelper::taobaoItemsInventoryGet($merchant, $startModifiedTime, $endModifiedTime);
            TaobaoTopJobHelper::taobaoItemsOnsaleGet($merchant, $startModifiedTime, $endModifiedTime);
        } else {
            // 无更新记录
            // TaobaoTopJobHelper::taobaoItemsInventoryGet($merchant, $startModifiedTime, $endModifiedTime);
            TaobaoTopJobHelper::taobaoItemsOnsaleGet($merchant, $startModifiedTime, $endModifiedTime);
        }
        return;
    }

    /**
     * 搜索商品并同步
     */
    public function search($merchant = 'amii', $provider = 'onSale', $search = '')
    {
        if (! $search) {
            return [80003];
        }
        switch ($provider) {
            case 'onSale':
                return app('amii.taobao.open.product')->searchInventorys($merchant, $search);
                break;
            case 'inventory':
                return app('amii.taobao.open.product')->searchOnSaleInventorys($merchant, $search);
                break;

            default:
                return [80004];
                break;
        }
    }

    /**
     * 设置同步时间
     * @param [type] $merchant [description]
     */
    public function setSyncTime($merchant, &$nearlyTime)
    {
        $nearItem = TaobaoTopSyncLog::merchant($merchant)
            ->orderBy('end_sync_time', 'desc')
            ->first();
        if ($nearItem) {
            $nearlyTime = $nearItem->end_sync_time;
        }
    }

    /**
     * 保存查询纪录
     * @param string $merchant amii amiiR amiiM amiiC
     */
    public function store($merchant = 'amii', array $value, $requestStartSyncTime, $requestEndSyncTime, $pageNo = 1, $isOnsale = false)
    {
        if ($requestStartSyncTime == null && $requestEndSyncTime == null) {
            $requestStartSyncTime = $requestEndSyncTime = Carbon::now()->toDateTimeString();
        }
        // 同步时间
        self::getSyncTime($value, $startSyncTime, $endSyncTime);

        TaobaoTopSyncLog::create([
            'merchant' => $merchant,
            'value' => $value,
            'start_sync_time' => $startSyncTime,
            'end_sync_time' => $endSyncTime,
            'request_start_sync_time' => $requestStartSyncTime,
            'request_end_sync_time' => $requestEndSyncTime
        ]);

        if (! isset($value['items'])) {
            return;
        }

        // 数据中包含商品信息保存
        $items = isset($value['items']['item']) ? $value['items']['item'] : [];
        foreach ($items as $item) {
            app('taobao.open.product.amii')->storeInventory($merchant, $item);
        }

        /**
         * 数据下一页处理
         * 下一页修改结束时间为当前页最后一项时间
         */
        // if (config('app.debug')) {
        //     // 测试状态只取一页数据
        //     return;
        // }
        $nextPage = self::hasNextPage($value, $pageNo);
        if ($nextPage[0] === 0) {
            // 需要下一页操作
            $startModifiedTime = $nextPage['startModifiedTime'];
            $endModifiedTime = null;
            $pageNo = $nextPage['pageNo'];
            if ($isOnsale) {
                TaobaoTopJobHelper::taobaoItemsOnsaleGet($merchant, $startModifiedTime, $endModifiedTime, $pageNo);
            } else {
                TaobaoTopJobHelper::taobaoItemsInventoryGet($merchant, $startModifiedTime, $endModifiedTime, $pageNo);
            }
        }
    }

    /**
     * 获取当前请求返回数据起始时间
     */
    public function getSyncTime(array $value, &$startSyncTime, &$endSyncTime)
    {
        if (! isset($value['items'])) {
            return;
        }
        $items = isset($value['items']['item']) ? $value['items']['item'] : '';
        if ($items === '') {
            return;
        }
        $startSyncTime = $items[0]['modified'];
        $endSyncTime = end($items)['modified'];

        return true;
    }

    /**
     * 下一页处理
     */
    public function hasNextPage(array $value, $pageNo)
    {
        if (! isset($value['items'])) {
            return [80002];
        }
        $totalRes = isset($value['total_results']) ? $value['total_results'] : 0;
        if (! $totalRes) {
            return [80002];
        }

        $currentRes = isset($value['items']['item']) ? count($value['items']['item']) : 0;
        if (! $currentRes || $currentRes == 1) {
            return [80002];
        }

        $countRes = ($pageNo - 1) * config('amii.taobaoopen.page_size') + $currentRes;
        if ($countRes >= $totalRes) {
            return [80002];
        }

        if ($value['items']['item'][0]['modified'] === end($value['items']['item'])['modified']) {
            $pageNo++;
        } else {
            $pageNo = 1;
        }

        return [
            0 => $totalRes > $currentRes ? 0 : 80003,
            'startModifiedTime' => end($value['items']['item'])['modified'],
            'pageNo' => $pageNo,
        ];
    }
}
