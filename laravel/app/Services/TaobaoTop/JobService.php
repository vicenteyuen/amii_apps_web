<?php

namespace App\Services\TaobaoTop;

class JobService
{
    /**
     * 同步仓库商品 taobao.items.inventory.get
     */
    public function taobaoItemsInventoryGet(array $data)
    {
        $merchant = $data['merchant'];
        $startModifiedTime = $data['startModifiedTime'];
        $endModifiedTime = $data['endModifiedTime'];
        $pageNo = $data['pageNo'];

        app('amii.taobao.open.product')->inventorys($merchant, $startModifiedTime, $endModifiedTime, $pageNo);
    }

    /**
     * 同步在售商品 taobao items onsale get
     */
    public function taobaoItemsOnsaleGet(array $data)
    {
        $merchant = $data['merchant'];
        $startModifiedTime = $data['startModifiedTime'];
        $endModifiedTime = $data['endModifiedTime'];
        $pageNo = $data['pageNo'];

        app('amii.taobao.open.product')->onSaleInventorys($merchant, $startModifiedTime, $endModifiedTime, $pageNo);
    }

    /**
     * 获取并存储淘宝商品数据
     */
    public function amiiStoreProduct(array $data)
    {
        $merchant = $data['merchant'];
        $numIid = $data['numIid'];
        $taobaoTopInventoryId = $data['taobaoTopInventoryId'];

        app('amii.taobao.open.product')->taobaoItemSellerGet($merchant, $numIid, $taobaoTopInventoryId);
    }

    /**
     * 淘宝商品图片处理
     */
    public function handlePic(array $data)
    {
        $taobaoTopInventoryId = $data['taobaoTopInventoryId'];
        $imgNum = $data['imgNum'];
        $imgData = $data['imgData'];
        $isUpdate = $data['isUpdate'];

        app('taobao.open.product.handle.amii')->handlePic($taobaoTopInventoryId, $imgData, $imgNum, $isUpdate);
    }

    /**
     * 淘宝商品无线详情处理
     */
    public function handleWapVision(array $data)
    {
        $taobaoTopInventoryId = $data['taobaoTopInventoryId'];
        $url = $data['url'];
        $position = $data['position'];
        $visionNum = $data['visionNum'];
        $isUpdate = $data['isUpdate'];

        app('taobao.open.product.handle.amii')->handleWapVision($taobaoTopInventoryId, $url, $position, $visionNum, $isUpdate);
    }

    /**
     * 类目查询
     */
    public function resCat(array $data)
    {
        $id = $data['id'];
        $isParent = isset($data['isParent']) ? $data['isParent'] : false;

        app('taobao.open.category.amii')->getCatFromTaobao($id, $isParent);
    }

    /**
     * 库存图更新
     */
    public function inventoryImg(array $data)
    {
        $inventoryId = $data['inventoryId'];
        $url = $data['url'];

        app('taobao.open.product.amii')->inventoryImg($inventoryId, $url);
    }

    /**
     * 按库存编号更新库存图
     */
    public function inventorysImg(array $data)
    {
        $skuCode = $data['skuCode'];
        $url = $data['url'];
        app('taobao.open.product.handle.amii')->inventorysImg($skuCode, $url);
    }

    /**
     * 更新库存属性值图
     */
    public function inventoryAttrValueImg(array $data)
    {
        $sellAttrValueId = $data['sellAttrValueId'];
        $url = $data['url'];

        app('taobao.open.product.amii')->inventoryAttrValueImg($sellAttrValueId, $url);
    }

    /**
     * 商品卖点数据分批处理
     */
    public function handleSellPoint(array $data)
    {
        $nextId = $data['nextId'];
        app('taobao.open.sell.point.amii')->handleSellPoint($nextId);
    }

    /**
     * 处理条目卖点
     */
    public function handleItemSellPoint(array $data)
    {
        $id = $data['id'];
        app('taobao.open.sell.point.amii')->handleItemSellPoint($id);
    }

    /**
     * 商品属性值图分配处理
     */
    public function handleAttrValueImg(array $data)
    {
        $nextId = $data['nextId'];
        app('taobao.open.attrvalue.img.amii')->handleAttrValueImg($nextId);
    }

    /**
     * 处理属性值图
     */
    public function handleItemAttrValueImg(array $data)
    {
        $id = $data['id'];
        app('taobao.open.attrvalue.img.amii')->handleItemAttrValueImg($id);
    }

    /**
     * 通过库存修改属性值图
     */
    public function attrValueImgHandle(array $data)
    {
        $skuCode = $data['skuCode'];
        $attrId = $data['attrId'];
        $attrValueId = $data['attrValueId'];
        $url = $data['url'];
        app('taobao.open.attrvalue.img.amii')->attrValueImgHandle($skuCode, $attrId, $attrValueId, $url);
    }
}
