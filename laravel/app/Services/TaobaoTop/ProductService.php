<?php

namespace App\Services\TaobaoTop;

use App\Helpers\TaobaoTopHelper;
use Carbon\Carbon;
use App\Models\TaobaoOpen\TaobaoTopSyncLog;

class ProductService
{
    /**
     * 获取当前回话商户仓库商品
     * @param string $merchant amii amiiR amiiM amiiC
     */
    public function inventorys($merchant = 'amii', $startModified = null, $endModified = null, $pageNo = 1)
    {
        $sessionKey = TaobaoTopHelper::setSessionKey($merchant);

        $c = TaobaoTopHelper::newClass('TopClient');
        $req = TaobaoTopHelper::newClass('ItemsInventoryGetRequest');
        $req->setFields("approve_status,num_iid,title,nick,type,cid,pic_url,num,props,valid_thru,list_time,price,has_discount,has_invoice,has_warranty,has_showcase,modified,delist_time,postage_id,seller_cids,outer_id");
        $req = TaobaoTopHelper::setPageConf($req, $pageNo);
        $req->setOrderBy('modified:asc');
        if ($startModified) {
            $req->setStartModified($startModified);
        }
        if ($endModified) {
            $req->setEndModified($endModified);
        }

        // 请求并格式化数据
        $resp = $c->execute($req, $sessionKey);
        $formatRes = TaobaoTopHelper::formatRes($resp);

        // 存储数据
        if ($formatRes[0] === 0) {
            app('amii.taobao.open.sync.log')->store($merchant, $formatRes[1], $startModified, $endModified, $pageNo);
        }
    }

    /**
     * 搜索当前会话仓库商品
     */
    public function searchInventorys($merchant = 'amii', $search, $pageNo = 1)
    {
        if (! $search) {
            return [80003];
        }

        $sessionKey = TaobaoTopHelper::setSessionKey($merchant);

        $c = TaobaoTopHelper::newClass('TopClient');
        $req = TaobaoTopHelper::newClass('ItemsInventoryGetRequest');
        $req->setFields("approve_status,num_iid,title,nick,type,cid,pic_url,num,props,valid_thru,list_time,price,has_discount,has_invoice,has_warranty,has_showcase,modified,delist_time,postage_id,seller_cids,outer_id");
        $req = TaobaoTopHelper::setPageConf($req, $pageNo);
        $req->setOrderBy('modified:asc');
        $req->setQ($search);

        // 请求并格式化数据
        $resp = $c->execute($req, $sessionKey);
        $formatRes = TaobaoTopHelper::formatRes($resp);

        return $formatRes;
    }

    /**
     * 获取当前商户在售商品
     */
    public function onSaleInventorys($merchant = 'amii', $startModified = null, $endModified = null, $pageNo = 1)
    {
        $sessionKey = TaobaoTopHelper::setSessionKey($merchant);

        $c = TaobaoTopHelper::newClass('TopClient');
        $req = TaobaoTopHelper::newClass('ItemsOnsaleGetRequest');
        $req->setFields("approve_status,num_iid,title,nick,type,cid,pic_url,num,props,valid_thru,list_time,price,has_discount,has_invoice,has_warranty,has_showcase,modified,delist_time,postage_id,seller_cids,outer_id");
        $req = TaobaoTopHelper::setPageConf($req, $pageNo);
        $req->setOrderBy('modified:asc');
        if ($startModified) {
            $req->setStartModified($startModified);
        }
        if ($endModified) {
            $req->setEndModified($endModified);
        }

        // 请求并格式化数据
        $resp = $c->execute($req, $sessionKey);
        $formatRes = TaobaoTopHelper::formatRes($resp);

        // 存储数据
        if ($formatRes[0] === 0) {
            // 标记处理在售商品
            $isOnsale = true;
            app('amii.taobao.open.sync.log')->store($merchant, $formatRes[1], $startModified, $endModified, $pageNo, $isOnsale);
        }
    }

    /**
     * 搜索当前商户在售商品
     */
    public function searchOnSaleInventorys($merchant = 'amii', $search, $pageNo = 1)
    {
        if (! $search) {
            return [80003];
        }

        $sessionKey = TaobaoTopHelper::setSessionKey($merchant);

        $c = TaobaoTopHelper::newClass('TopClient');
        $req = TaobaoTopHelper::newClass('ItemsOnsaleGetRequest');
        $req->setFields("approve_status,num_iid,title,nick,type,cid,pic_url,num,props,valid_thru,list_time,price,has_discount,has_invoice,has_warranty,has_showcase,modified,delist_time,postage_id,seller_cids,outer_id");
        $req = TaobaoTopHelper::setPageConf($req, $pageNo);
        $req->setOrderBy('modified:asc');
        $req->setQ($search);

        // 请求并格式化数据
        $resp = $c->execute($req, $sessionKey);
        $formatRes = TaobaoTopHelper::formatRes($resp);

        return $formatRes;
    }

    /**
     * taobao.item.seller.get 根据淘宝商品数字id获取商品详细信息
     */
    public function taobaoItemSellerGet($merchant, $numIid, $taobaoTopInventoryId)
    {
        $numIid = (string) $numIid;
        $sessionKey = TaobaoTopHelper::setSessionKey($merchant);

        $c = TaobaoTopHelper::newClass('TopClient');
        $req = TaobaoTopHelper::newClass('ItemSellerGetRequest');
        $req->setFields("detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve_status,postage_id,product_id,auction_point,property_alias,item_img,prop_img,sku,outer_id,is_virtual,is_taobao,is_ex,is_timing,video,is_3D,score,volume,one_station,second_kill,auto_fill,cod_postage_id,is_prepay,ww_status,wap_detail_url,wap_desc,violation,sell_promise,period_sold_quantity,after_sale_id,barcode,created,custom_made_type_id,desc_module_info,features,food_security,inner_shop_auction_template_id,is_fenxiao,is_lightning_consignment,is_xinpin,is_area_sale,large_screen_image_url,cuntao_item_specific,desc_modules,with_hold_quantity,wireless_desc,video_id,template_id,sub_stock,sold_quantity,sell_point,props_name,promoted_service,paimai_info,outer_shop_auction_template_id,newprepay,mpic_video,locality_life,item_weight,item_size,cpv_memo,global_stock_country,global_stock_type");
        $req->setNumIid($numIid);

        // 请求并格式化数据
        $resp = $c->execute($req, $sessionKey);
        $formatRes = TaobaoTopHelper::formatRes($resp);

        // 存储数据
        if ($formatRes[0] === 0) {
            app('taobao.open.product.handle.amii')->storeTaobaoProductHandle($merchant, $taobaoTopInventoryId, $formatRes[1]);
        }
    }

    /**
     * taobao.item.seller.get 根据淘宝商品数字id获取商品详细信息
     */
    public function syncTaobaoItemSellerGet($merchant, $numIid)
    {
        $numIid = (string) $numIid;
        $sessionKey = TaobaoTopHelper::setSessionKey($merchant);

        $c = TaobaoTopHelper::newClass('TopClient');
        $req = TaobaoTopHelper::newClass('ItemSellerGetRequest');
        $req->setFields("detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve_status,postage_id,product_id,auction_point,property_alias,item_img,prop_img,sku,outer_id,is_virtual,is_taobao,is_ex,is_timing,video,is_3D,score,volume,one_station,second_kill,auto_fill,cod_postage_id,is_prepay,ww_status,wap_detail_url,wap_desc,violation,sell_promise,period_sold_quantity,after_sale_id,barcode,created,custom_made_type_id,desc_module_info,features,food_security,inner_shop_auction_template_id,is_fenxiao,is_lightning_consignment,is_xinpin,is_area_sale,large_screen_image_url,cuntao_item_specific,desc_modules,with_hold_quantity,wireless_desc,video_id,template_id,sub_stock,sold_quantity,sell_point,props_name,promoted_service,paimai_info,outer_shop_auction_template_id,newprepay,mpic_video,locality_life,item_weight,item_size,cpv_memo,global_stock_country,global_stock_type");
        $req->setNumIid($numIid);

        // 请求并格式化数据
        $resp = $c->execute($req, $sessionKey);
        $formatRes = TaobaoTopHelper::formatRes($resp);

        if ($formatRes[0] !== 0) {
            return [
                80008,
                $formatRes[1]
            ];
        }
        // 同步加入队列
        app('taobao.open.product.handle.amii')->updateTaobaoProductHandle($merchant, $formatRes[1]);
        return [0];
    }
}
