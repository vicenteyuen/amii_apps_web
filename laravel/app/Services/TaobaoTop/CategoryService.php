<?php

namespace App\Services\TaobaoTop;

use App\Helpers\TaobaoTopHelper;

class CategoryService
{
    /**
     * 获取类目信息 taobao.itemcats.get
     */
    public function taobaoItemcatsGet($id, $isParent = false)
    {
        $c = TaobaoTopHelper::newClass('TopClient');
        $req = TaobaoTopHelper::newClass('ItemcatsGetRequest');
        if ($isParent) {
            if (func_num_args() == 3) {
                $parentId = func_get_arg(3);
            } else {
                $parentId = $id;
            }
            $req->setParentCid((string) $parentId);
        } else {
            $req->setCids((string) $id);
        }
        $req->setFields("cid,parent_cid,name,is_parent,status,sort_order");
        $resp = $c->execute($req);

        $formatRes = TaobaoTopHelper::formatRes($resp);
        if ($formatRes[0] === 0) {
            return $formatRes[1];
        }
    }
}
