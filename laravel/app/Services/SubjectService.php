<?php

namespace App\Services;

use App\Models\Subject;
use App\Models\SubjectViews;
use App\Models\SubjectMark;

class SubjectService
{
    public function index()
    {
        $subjects = Subject::orderBy('weight', 'desc')
            ->orderBy('created_at', 'desc')
            ->where('status', 1)
            ->paginate(config('amii.apiPaginate'))
            ->appends(request()->all());
        foreach ($subjects as $subject) {
            $subject->addHidden('content');
        }

        return $subjects;
    }

    public function adminIndex()
    {
        $subjects = Subject::orderBy('weight', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.apiPaginate'))
            ->appends(request()->all());
        foreach ($subjects as $subject) {
            $subject->addHidden('content');
        }

        return $subjects;
    }

    public function find($id)
    {
        return Subject::with('products')
            ->find($id);
    }

    // store Subject
    public function store($arr)
    {
        return Subject::create($arr);
    }

    public function update(array $arr, $id)
    {
        return Subject::where('id', $id)
            ->update($arr);
    }

    public function delete($id)
    {
        return Subject::destroy($id);
    }

    /**
     * 专题浏览数+1
     *
     * @param $id
     * @param $user_id
     */
    public function addScan($id, $user_id = null)
    {
         Subject::where('id', $id)
                ->increment('scan');
        if ($user_id) {
            SubjectViews::create(['user_id' => $user_id,'subject_id' => $id]);
        }
    }

    public function h5addScan($id, $userId = null)
    {
        if ($userId) {
            $is_exist = SubjectViews::where('user_id', $userId)
                ->where('subject_id', $id)
                ->first();
            if (!$is_exist) {
                Subject::where('id', $id)
                    ->increment('scan');
                SubjectViews::create(['user_id' => $user_id,'subject_id' => $id]);
            }
        } else {
             Subject::where('id', $id)
                ->increment('scan');
        }
    
    }

    // 点赞或者取消
    public function mark($subjectId, $userId)
    {
        $subjects = SubjectMark::where('subject_id', $subjectId)
            ->where('user_id', $userId)
            ->first();
        // 取消点费
        if ($subjects) {
            $subjects->delete();
            return Subject::where('id', $subjectId)
                ->decrement('marks');
        }
        SubjectMark::create([
            'subject_id' => $subjectId,
            'user_id' => $userId
        ]);
        app('pointReceived')->userAddPoint('share', 'subject_mark');
        return Subject::where('id', $subjectId)
            ->increment('marks');
    }

    // 判断用户是否已点赞
    public function hadMark($subjectId)
    {
        if (!auth()->id()) {
            return false;
        }
        $exist = SubjectMark::where('subject_id', $subjectId)
            ->where('user_id', auth()->id())
            ->first();
        if ($exist) {
            return true;
        }
        return false;
    }
}
