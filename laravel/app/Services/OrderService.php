<?php

namespace App\Services;

use Illuminate\Http\Request;
use Validator;
use DB;
use Carbon\Carbon;

use App\Helpers\AmiiHelper;
use App\Helpers\OrderJobHelper;
use App\Helpers\RebackTribeMoneyHelper;
use App\Helpers\OrderReceivedHelper;
use App\Helpers\FirstOrderReturnDiscount;
use App\Models\SellProduct;
use Ecommerce\Models\Inventory;
use App\Models\Order;
use App\Models\Payment;
use App\Models\OrderProduct;
use App\Models\Cart;
use App\Models\OrderPackage;
use App\Models\Activity;
use App\Models\NotifyMessage;
use App\Models\GiveProduct;
use App\Models\User;

class OrderService
{
    /**
     * store order from buy now
     */
    public function buyNow($userId, Request $request)
    {
        $validator = self::validatorBuyNow($request);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', null, $errors);
        }

        DB::beginTransaction();
        // 库存处理
        $checkInventories = self::inventoryCheck([['id' => intval($request->inventory_id), 'number' =>intval($request->number)]]);
        if (! $checkInventories) {
            DB::rollBack();
            return app('jsend')->error('库存不足,提交订单失败');
        }

        // 生成订单
        $createOrder = self::createOrder($userId, $checkInventories);
        if (! $createOrder) {
            DB::rollBack();
            return app('jsend')->error('提交订单失败');
        }

        // 添加订单未确认退回库存任务
        OrderJobHelper::unconfirm($createOrder);

        DB::commit();
        return app('jsend')->success(['orderSn' => $createOrder]);
    }

    /**
     * store order from buy cart
     */
    public function buyCart($userId, Request $request)
    {
        $validator = self::validatorBuyCart($request);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return app('jsend')->error('参数错误', null, $errors);
        }

        // 获取购物车商品对应库存及数量
        $inventories = self::cartInventories($userId, $request->carts);
        if (! $inventories) {
            return app('jsend')->error('购物车商品有误,请刷新购物车再提交订单');
        }

        DB::beginTransaction();
        // 购物车库存
        $cartInventories = $inventories['inventories'];
        $cartInventories = array_map(function ($key, $value) {
            return [
            'id'        => $key,
            'number'    => $value
            ];
        }, array_keys($cartInventories), $cartInventories);
        // 购物车query
        $cartQuery = $inventories['cartQuery'];

        // 库存处理
        $checkInventories = self::inventoryCheck($cartInventories);
        if (! $checkInventories) {
            DB::rollBack();
            return app('jsend')->error('库存不足,提交订单失败');
        }

        // // 删除购物车对应条目
        // if (! $cartQuery->delete()) {
        //     DB::rollBack();
        //     return app('jsend')->error('提交订单失败');
        // }

        // 生成订单
        $createOrder = self::createOrder($userId, $checkInventories, 'standard', $request->carts);
        if (! $createOrder) {
            return app('jsend')->error('提交订单失败');
        }

        // 添加订单未确认退回库存任务
        OrderJobHelper::unconfirm($createOrder);

        DB::commit();
        return app('jsend')->success(['orderSn' => $createOrder]);
    }

    /**
     * Validator buy now data
     */
    private function validatorBuyNow(Request $request)
    {
        $rules = [
        'inventory_id'  => 'required|integer',
        'number'        => 'required|integer'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Validator buy cart data
     */
    private function validatorBuyCart(Request $request)
    {
        $rules = [
        'carts' => 'required|array'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * 获取购物商品库存
     * @param array $cartIds = [1, 2, 3]
     */
    private function cartInventories($userId, array $cartIds)
    {
        $carts = Cart::whereUserId($userId)
        ->whereIn('id', $cartIds);
        $cartInventories = (clone $carts)
        ->pluck('number', 'inventory_id')
        ->toArray();

        if (count($cartIds) != count($cartInventories)) {
            return false;
        }
        return [
        'inventories' => $cartInventories,
        'cartQuery' => (clone $carts)->lockForUpdate()
        ];
    }

    /**
     * 库存校验 减库存
     * @param array $inventories = [['id' => 1, 'number' => 2], ['id' => 3, 'number' => 5]]
     */
    public function inventoryCheck(array $inventories, $minusInventory = false)
    {
        $inventoryIds = collect($inventories)
        ->pluck('id')
        ->all();

        $checkInventories = Inventory::has('SellProduct')
        ->where(function ($query) use ($inventories) {
            foreach ($inventories as $inventory) {
                $query->orWhere(function ($q) use ($inventory) {
                    return $q->where('id', $inventory['id'])
                    ->where('number', '>=', $inventory['number'])
                    ->where('number', '>', 0);
                });
            }
            return $query;
        })
        ->lockForUpdate();

        $checkInventoryIds = (clone $checkInventories)
        ->lists('id')
        ->toArray();

        if (empty($checkInventoryIds)) {
            return false;
        }

        foreach ($inventoryIds as $inventoryId) {
            if (! in_array($inventoryId, $checkInventoryIds)) {
                return false;
            }
        }

        // 返回数据查询
        $returnData = (clone $checkInventories)
        ->select('id', 'product_id', 'product_sell_id', 'mark_price')
        ->get();

        if ($minusInventory) {
            // 减库存
            // 减少商品库存对应条目数量
            $currentInventories = (clone $checkInventories)
            ->pluck('number', 'id')
            ->toArray();
            // 销售商品对应数据
            $currentSellProductsId = (clone $checkInventories)
            ->pluck('product_sell_id', 'id')
            ->toArray();
            $minusInventories = array_map(function ($inventory) use ($currentInventories) {
                return [
                'key' => $inventory['id'],
                'value' => $currentInventories[$inventory['id']] - $inventory['number']
                ];
            }, $inventories);
            AmiiHelper::updateColumnDiffValues((clone $checkInventories), 'number', 'id', $minusInventories);

            // 减少商品库存总数
            // 当前购买商品数据[['id' => 'sell_product_id', 'number' => '购买数量']]
            $productsNumber = array_map(function ($inventory) use ($currentSellProductsId) {
                return [
                    'id' => $currentSellProductsId[$inventory['id']],
                    'number' => $inventory['number']
                ];
            }, $inventories);
            // 当前购买商品
            $sellProductQuery = SellProduct::whereIn('id', $currentSellProductsId);
            $currentSellProducts = (clone $sellProductQuery)
            ->pluck('number', 'id')
            ->toArray();
            // 当前购买商品数量计算
            $minusSellProducts = array_map(function ($productNumber) use ($currentSellProducts) {
                return [
                    'key' => $productNumber['id'],
                    'value' => $currentSellProducts[$productNumber['id']] - $productNumber['number']
                ];
            }, $productsNumber);
            // 更新商品总库存
            AmiiHelper::updateColumnDiffValues((clone $sellProductQuery), 'number', 'id', $minusSellProducts);
        }

        // 返回购买商品信息
        $returnData = array_map(function ($inventory) use ($inventories) {
            foreach ($inventories as $buyInventory) {
                if ($buyInventory['id'] == $inventory['id']) {
                    return [
                        'inventory_id'      => $inventory['id'],
                        'sell_product_id'   => $inventory['product_sell_id'],
                        'product_id'        => $inventory['product_id'],
                        'number'            => $buyInventory['number'],
                        'price'             => $inventory['mark_price'],
                        'refundable_amount' => number_format($inventory['mark_price'] * $buyInventory['number'], 2, '.', ''),
                    ];
                }
            }
        }, $returnData->toArray());

        return $returnData;
    }

    /**
     * create order
     */
    public function createOrder($userId, array $orderProducts, $orderProvider = 'standard', $cartIds = [])
    {
        $productAmount = 0;
        foreach ($orderProducts as $orderProduct) {
            $productAmount += $orderProduct['number'] * $orderProduct['price'];
        }

        // create order
        $createData = [
        'user_id'           => $userId,
        'order_sn'          => AmiiHelper::generateOrderSn($userId),
        'product_amount'    => $productAmount,
        'provider'          => $orderProvider,
        'order_start_time'  => Carbon::now(),
        ];
        if (! empty($cartIds)) {
            $createData['cart_ids'] = $cartIds;
        }
        if ($orderProvider == 'point') {
            $createData['can_refund'] = 1;
        }
        $create = Order::create($createData);
        $orderId = $create->id;

        // store order products
        $orderProducts = array_map(function ($orderProduct) use ($orderId) {
            $mergeArr = [
            'product_provider' => 1,
            'order_id' => $orderId
            ];
            $tmpArr = $orderProduct;
            return array_merge($tmpArr, $mergeArr);
        }, $orderProducts);

        OrderProduct::insert($orderProducts);
        return $create->order_sn;
    }

    /**
     * 获取订单可用优惠券
     */
    public function getCoupon4Order($order)
    {
        $coupons = app('amii.manger.user.coupon')->coupon4Order(auth()->id(), $order);
        return $coupons;
    }

    /**
     * 获取订单可用积分
     */
    public function getPoint4Order($orderProductAmount = 0)
    {
        $pointAmount = app('user')->userRemainPoint(auth()->id());

        // 订单可使用积分
        $canUsePoint = intval($orderProductAmount * 100 / 2);

        $point = $canUsePoint > $pointAmount ? $pointAmount : $canUsePoint;

        return $point;
    }

    /**
     * 获取订单可用余额
     */
    public function getBalance4Order($order)
    {
        $balance = app('user')->userRemainGain(auth()->id());
        return $balance;
    }

    /**
     * 获取vip用户订单可用优惠
     */
    public function vipDiscount($userId, $order)
    {
        $productAmount = $order->product_amount;
        $discount = app('enable')->discount($userId);
        if (! $discount) {
            return 0;
        }

        return intval($productAmount * (1 - $discount/100) * 100) / 100;
    }

    /**
     * order api index
     */
    public function index($userId, Request $request)
    {
        $orders = Order::whereUserId($userId)
        ->confirm();

        if ($request->status) {
            switch ($request->status) {
                case 'paying':
                    $orders->paying();
                    break;
                case 'sending':
                    $orders->sending();
                    break;
                case 'receiving':
                    $orders->receiving();
                    break;
                case 'appraising':
                    $orders->appraising();
                    break;
                case 'refunding':
                    $orders->refunding();
                    break;

                default:
                    # code...
                    break;
            }
        }

        if ($request->search) {
            $searchArr = explode(' ', $request->search);
            $orders->where(function ($query) use ($searchArr) {
                $query->where(function ($q) use ($searchArr) {
                    foreach ($searchArr as $searchValue) {
                        $searchValue = str_replace('%', '\%', $searchValue);
                        $q = $q->orWhere('order_sn', 'like', '%'.$searchValue.'%');
                    }
                    return $q;
                })
                ->doesntHave('game', 'and', function ($q1) {
                    return $q1->where('order_status', '!=', 1);
                })
                ->orWhereHas('orderProducts', function ($q1) use ($searchArr) {
                    return $q1->whereHas('sellProduct', function ($q2) use ($searchArr) {
                        return $q2->whereHas('product', function ($q3) use ($searchArr) {
                            foreach ($searchArr as $searchValue) {
                                $searchValue = str_replace('%', '\%', $searchValue);
                                $q3 = $q3->orWhere('name', 'like', '%'.$searchValue.'%');
                            }
                            return $q3;
                        });
                    });
                });
            });
        }

        $orders = $orders->with('orderProducts.inventory.sku.skuValue.attribute')
        ->with(['orderProducts' => function ($q1) {
            $q1->with(['sellProduct' => function ($q2) {
                $q2->with(['product' => function ($q3) {
                    $q3->orWhere('status', 0);
                }]);
            }]);
        }])
        ->doesntHave('game', 'and', function ($q4) {
            return $q4->where('order_status', '!=', 1);
        })
        ->with('giveProduct.sellProduct.product')
        ->with('giveProduct.activity')
        ->orderBy('created_at', 'desc')
        ->paginate(config('amii.apiPaginate'));

        return $orders;
    }

    /**
     * 获取订单商品
     */
    public function orderProducts($userId)
    {
        $orderProducts = OrderProduct::whereHas('order', function ($query) use ($userId) {
            return $query->where('user_id', $userId)
            ->paid();
        })
        ->groupBy('product_id')
        ->orderBy('id', 'desc')
        ->take(config('amii.order.nearlyNum'))
        ->with('sellProduct.product')
        ->get();

        return $orderProducts;
    }

    /**
     * 订单不同状态数量
     */
    public function statusCount($userId = 0)
    {
        $orders = Order::whereUserId($userId)
        ->confirm();

        $paying = (clone $orders)
        ->paying()
        ->doesntHave('game')
        ->count();
        $sending = (clone $orders)
        ->sending()
        ->count();
        $receiving = (clone $orders)
        ->receiving()
        ->count();
        $appraising = (clone $orders)
        ->appraising()
        ->count();
        $refunding = (clone $orders)
        ->refunds()
        ->count();

        return compact('paying', 'sending', 'receiving', 'appraising', 'refunding');
    }

    /**
     * 取消订单
     */
    public function cancel($userId, $orderSn)
    {
        // $updateData = [
        //     'pay_status'    => 0,
        //     'order_status'  => 2,
        //     'can_refund'    => 0
        // ];
        // $cancel = Order::where('user_id', $userId)
        //     ->where('order_sn', $orderSn)
        //     ->paying()
        //     ->update($updateData);
        // if ($cancel) {
        //     return true;
        // }
        // return false;
        return app('orderjob')->cancel($orderSn);
    }

    /**
     * 获取提交订单支付 订单商品信息
     */
    public function getOrder2Pay($userId, $orderSn, $orderProvider = null, $game = null)
    {
        $order = Order::where('order_sn', $orderSn)
        ->where('user_id', $userId);
        // 非游戏订单获取数据
        if (!$game) {
            $order = $order->unconfirm();
        }
        if ($orderProvider) {
            $order = $order->where('provider', $orderProvider);
        }

        $order = $order->with('orderProducts.sellProduct.product')
        ->with('orderProducts.inventory.sku.skuValue.attribute')
        ->first();
        if (! $order) {
            return false;
        }
        return $order;
    }

    /**
     * 获取订单详情
     */
    public function show($userId, $orderSn = '')
    {
        $order = Order::where('order_sn', $orderSn)
        ->where('user_id', $userId)
        ->confirm()
        ->with('orderProducts.inventory.sku.skuValue.attribute')
        ->with('giveProduct.sellProduct.product')
        ->with('giveProduct.activity')
        ->with(['orderProducts' => function ($q1) {
            $q1->with(['sellProduct' => function ($q2) {
                $q2->with(['product' => function ($q3) {
                    $q3->orWhere('status', 0);
                }]);
            }]);
        }])
        ->with(['payments' => function ($q4) {
            $q4->where('status', 1);
        }])->first();

        if (!$order) {
            return false;
        }

        $order->pay_type =
        $order->express = app('express')->getExpress($orderSn);
        return $order;
    }

    /**
     * 获取支付订单数据
     */
    public function payingData($orderSn = '')
    {
        $order = Order::where('order_sn', $orderSn)
        ->paying()
        ->with('orderProducts.sellProduct.product')
        ->with('orderProducts.inventory.sku.skuValue.attribute')
        ->first();
        return $order;
    }

    /**
     * 关闭售后
     */
    public function closeRefund($orderSn)
    {
        $order = Order::where('order_sn', $orderSn)
        ->canCloseRefund()
        ->first();
        if ($order) {
            $logStr = '关闭售后: 订单号: ' . $orderSn . ' ';
            if ($order->is_refund) {
                \Log::info($logStr . '申请过售后 不可关闭');
            } else {
                $order->update([
                'can_refund' => 0
                ]);
                $order->save();
                \Log::info($logStr . '关闭售后功能');
            }
        }
        return false;
    }

    /**
     * 微信app支付回调
     */
    public function wechatPaid($orderSn)
    {
        return self::paid($orderSn, 'wechat');
    }

    /**
     * 微信公众号支付
     */
    public function wempPaid($orderSn)
    {
        return self::paid($orderSn, 'wemp');
    }

    /**
     * 微信小程序支付
     */
    public function weappPaid($orderSn)
    {
        return self::paid($orderSn, 'weapp');
    }

    /**
     * 设置订单状态为支付完成
     */
    public function paid($orderSn, $provider = 'amii')
    {
        $order = Order::where('order_sn', $orderSn)
        ->paying()
        ->with(['payments' => function ($query) use ($provider) {
            $query->where('provider', $provider)
            ->where('status', 0);
        }])
        ->first();

        if (! $order) {
            return false;
        }

        // 支付状态更改为已支付
        $order->pay_status = 1;
        // 售后开启可申请
        $order->can_refund = 1;
        // 支付时间结束
        $order->pay_end_time = Carbon::now();

        $order->incrementVolume();

        $payment = $order->payments->isEmpty() ? [] : $order->payments[0];
        if (empty($payment)) {
            if (config('app.debug')) {
                $payment = Payment::create([
                'order_id'  => $order->id,
                'provider'  => $provider,
                'total_fee' => $order->order_fee,
                'paid_time' => Carbon::now(),
                ]);
            } else {
                return false;
            }
        }

        $payment->status = 1;
        $payment->paid_time = Carbon::now();
        $payment->save();

        // 上级增加待返金额
        if ($order->provider == 'standard') {
             app('gain')->store(['user_id' => $order->user_id,'income' => ($order->order_fee + $order->balance_discount)], $order->id);
        }


        // 赠送首单赠品
        app('amii.give.product')->paySuccess($order->user_id, $order->id);

        // 增加订单总数,消费总额
        $user = $order->user;
        $user->ordres++ ;
        $user->purchase_price = $user->purchase_price + $order->order_fee;
        $user->save();
        return $order->save();
    }

    /**
     * 商户发货
     */
    public function orderSend($orderSn)
    {
        $arr = [
        'orderSn' => $orderSn,
        'com' => 'amiitest',
        'shipping_sn' => mt_rand(10000, 100000)
        ];
        return self::adminOrderSend($arr);
    }

    /**
     * 商户开启售后
     */
    public function openRefund($orderSn, $can_refund)
    {
        if ($can_refund == 'true') {
            $can_refund = 1;
            $order = Order::where('order_sn', $orderSn)
            ->canOpenRefund()
            ->first();
        } else {
            $can_refund = 0;
            $order = Order::where('order_sn', $orderSn)
            ->first();
        }
        if ($order) {
            $order->can_refund = $can_refund ;
            return $order->save();
        }
            return false;
    }

    /**
     * 买家收货
     */
    public function orderReceive($userId, $orderSn)
    {
        $order = Order::with('packages')
        ->where('order_sn', $orderSn)
        ->where('user_id', $userId)
        ->receiving()
        ->first();
        if (! $order) {
            return false;
        }

        // 获取商品名称
        $product = Order::with('orderProducts.sellProduct.product')->where('order_sn', $orderSn)->first();
        $product_name = $product->orderProducts[0]->sellProduct->product['name'];

        // 发送快递签收消息推送
        $arr = [
        'user_id'     => $userId,
        'order_id'    => $order->id,
        'product_name'=> $product_name,
        ];
        app('amii.notify.message')->sendNotifyMsg('express', 'receive', $arr);

        $order = Order::where('order_sn', $orderSn)
        ->where('user_id', $userId)
        ->first();
        // 更改发货状态
        $order->shipping_status = 2;
        // 游戏订单不可开启售后
        if ($order->provider != 'game') {
            $order->can_refund = 1;
        }

        // 用户确认收货退回佣金
        RebackTribeMoneyHelper::expressRebackMoney($order->id);
        $order->save();

        // 确认收货时间,8天后关闭售后
        OrderReceivedHelper::received($order);

        // 首单退30%
        $delay =  8 * 24 * 60 * 60;
        $job = (new \App\Jobs\FirstPurchaseReturnMoneyJob(clone $order))->delay($delay);
        dispatch($job);
        return true;
    }

    /**
     * 删除订单
     */
    public function destroy($userId, $orderSn)
    {
        $order = Order::where('order_sn', $orderSn)
            ->where('user_id', $userId)
            ->canDelete()
            ->first();
        if (! $order) {
            return false;
        }

        return $order->delete();
    }

    /**
     * 获取新增评价数据
     */
    public function order4Comment($userId, $orderSn)
    {
        $order = Order::where('order_sn', $orderSn)
            ->appraising()
            ->where('user_id', $userId)
            ->whereIn('provider', ['standard','game'])
            ->whereHas('orderProducts', function ($q1) {
                return $q1->doesntHave('refund');
            })
            ->with('orderProducts.sellProduct')
            ->with('orderProducts.inventory')
            ->first();
        if (! $order) {
            return false;
        }

        $returnData = [];
        foreach ($order->orderProducts as $orderProduct) {
            $image = '';
            if (isset($orderProduct->sellProduct->allImage[0])) {
                $image = $orderProduct->sellProduct->allImage[0];
            }
            $returnData[] = [
                'name' => $orderProduct->sellProduct->product->name,
                'image' => $image,
                'inventory_id' => $orderProduct->inventory_id
            ];
        }

        return $returnData;
    }

    /**
     * 更新订单售后状态
     * @param intval $orderId
     */
    public function updateOrderRefund($orderId)
    {
        $order = Order::where('id', $orderId)
            ->first();
        if (! $order) {
            return false;
        }

        $orderProducts = $order->orderProducts;
        // 是否售后
        $isRefund = false;
        // 是否同意售后
        $isAgree = false;
        // 是否退款
        $isRefundPay = true;
        // 是否完成
        $isRefunded = false;
        // 是否拒绝
        $isRefuse = false;

        foreach ($orderProducts as $orderProduct) {
            if (! $orderProduct->refund) {
                $isRefund = true;
                continue;
            }
            if ($orderProduct->refund->status == 1) {
                $isRefunded = true;
                continue;
            }
            if ($orderProduct->refund->status == 2) {
                $isRefuse = true;
                continue;
            }
            switch ($orderProduct->refund->refund_status) {
                case '1':
                    $isAgree = true;
                    continue;
                    break;
                case '2':
                    $isRefundPay = true;
                    continue;
                    break;

                default:
                    continue;
                    break;
            }
        }

        if ($isRefundPay || $isAgree) {
            // 商家已同意
            $order->is_refund = 2;
            return $order->save();
        }

        if ($isRefund) {
            // 已申请
            $order->is_refund = 1;
            return $order->save();
        }

        if ($isRefunded) {
            // 已完成
            $order->is_refund = 3;
            return $order->save();
        }

        if ($isRefuse) {
            $order->is_refund = 4;
            return $order->save();
        }

        return true;
    }

    //admin 订单列表
    public function adminIndex(Request $request)
    {
        $search = trim($request->search);
        if ($request->status != 'cancel') {
            $orders = Order::confirm()
                ->withTrashed();
        }
        if ($request->status) {
            switch ($request->status) {
                case 'all':
                    break;
                case 'paying':
                    $orders->paying();
                    break;
                case 'sending':
                    $orders->sending();
                    break;
                case 'receiving':
                    $orders->receiving();
                    break;
                case 'appraising':
                    $orders->appraising();
                    break;
                case 'refunding':
                    Order::$payTime = true;
                    User::$withoutAppends = true;
                    $orders->refunding();
                    break;
                case 'cancel':
                    $orders = Order::where('order_status', 2);
                    break;
                case 'success':
                    $orders->success();
                    break;
                default:
                    # code...
                    break;
            }
        }
        $orders = $orders->with('user');

        if ($request->provider) {
            $orders = $orders->where('provider', $request->provider);
        }
        if (strlen($search) > 11 || strlen($search) == 0) {
            $orders = clone($orders)->where('order_sn', 'like', '%'.trim($search).'%');
        } else {
            $orders = clone($orders)->whereHas('user', function ($q) use ($search) {
                 return $q->orWhere('phone', trim($search));
            });
        }

        // 时间段搜索
        if ($request->start && $request->end) {
            $orders->whereBetween('created_at', [$request->start,$request->end]);
        } elseif ($request->start) {
            $orders->where('created_at', '>=', $request->start);
        } elseif ($request->end) {
            $orders->where('created_at', '<=', $request->end);
        }

        $orders = clone($orders)->orderBy('created_at', 'desc')
            ->paginate(config('admin.apiPaginate'))
            ->appends(request()->all());
         return $orders;
    }


    // 用户订单
    public function adminUserOrderIndex($userId)
    {
        return Order::whereUserId($userId)
            ->withTrashed()
            ->confirm()
            ->with('orderProducts.inventory.sku.skuValue.attribute')
            ->with(['orderProducts' => function ($q1) {
                $q1->with(['sellProduct' => function ($q2) {
                    $q2->with(['product' => function ($q3) {
                        $q3->orWhere('status', 0);
                    }]);
                }]);
            }])
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.apiPaginate'));
    }

    /**
     * 订单详情
     */
    public function adminShow($orderSn)
    {
        $order = Order::where('order_sn', $orderSn)
            ->withTrashed()
            ->confirm()
            ->with('orderProducts.inventory.sku.skuValue.attribute')
            ->with('orderProducts.refund.logs.images')
            ->with(['orderProducts' => function ($q1) {
                $q1->with(['sellProduct' => function ($q2) {
                    $q2->with(['product' => function ($q3) {
                        $q3->orWhere('status', 0);
                    }]);
                }]);
            }])
            ->with('payments')
            ->first();
        return $order;
    }

    /**
     * 后台发货
     **/
    public function adminOrderSend($data)
    {
        $order = Order::where('order_sn', $data['orderSn'])
            ->sending()
            ->first();

        if (! $order) {
            return false;
        }

        SellProduct::$allImageEmpty = true;
        $orderProduct = OrderProduct::where('order_id', $order->id)
            ->with('sellProduct.product')
            ->first();

        $product_name = '';
        if (isset($orderProduct->sellProduct->product)) {
            $product_name = $orderProduct->sellProduct->product->name;
        }

        DB::beginTransaction();
        $order->shipping_status = 1;        // 更改发货状态
        $order->can_refund      = 0;       // 关闭售后可申请状态
        $orderSave              = $order->save();

        $package['order_id']        = $order->id;     //包裹新增
        $package['shipping_status'] = 1;
        $package['com']             = $data['com'];
        $package['shipping_sn']     = $data['shipping_sn'];

        $packageCreate = OrderPackage::create($package);

        if ($orderSave && $packageCreate) {
            // 订阅快递
            $poll = app('kuaidi100')->poll($data['shipping_sn']);
            app('express')->store($packageCreate->id, $packageCreate->com, $packageCreate->shipping_sn, [], $poll ? 1 : 0);

            $arr = [
                'user_id'     => $order->user_id,
                'shipping_sn' => $data['shipping_sn'],
                'order_id'    => $order->id,
                'product_name'=> $product_name,
            ];
            // 添加队列退回佣金
            RebackTribeMoneyHelper::rebackMoney($order->id);

            // 保存数据，并发送消息推送
            app('amii.notify.message')->sendNotifyMsg('express', 'deliver', $arr);

            //存在首单10天自动签收首单7折
            $giveProduct = GiveProduct::where('order_id', $order->id)
                ->where('return_profit', 0)
                ->first();
            if ($giveProduct) {
                $delay = 10 * 24 * 60 * 60;
                $jobs = (new \App\Jobs\FirstPurchaseReturnProfit($order->id))->delay($delay);
                dispatch($jobs);
            }
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    /**
     * 获取订单可评价商品
     */
    public function orderAppraising($orderSn)
    {
        return Order::where('order_sn', $orderSn)
            ->appraising()
            ->whereHas('orderProducts', function ($q1) {
                return $q1->doesntHave('refund')
                    ->orWhereHas('refund', function ($q2) {
                        return  $q2->whereIn('status', [1,2]);
                    });
            })
            ->with('orderProducts.inventory.sku.skuValue.attribute')
            ->with('giveProduct.sellProduct.product')
            ->with('giveProduct.activity')
            ->with(['orderProducts' => function ($q1) {
                $q1->with(['sellProduct' => function ($q2) {
                    $q2->with(['product' => function ($q3) {
                        $q3->orWhere('status', 0);
                    }]);
                }]);
            }])->first();
    }

    //用户订单数
    public function userOrders($userId)
    {
        return Order::where('user_id', $userId)
            ->where('order_status', 1)
            ->where('pay_status', 1)
            ->where('shipping_status', 2)
            ->get()
            ->count();
    }

    // 用户总金额消费
    public function userConsumer($userId)
    {
        $order =  Order::where('user_id', $userId)
            ->where('provider', 'standard')
            ->where('order_status', 1)
            ->where('pay_status', 1)
            ->withTrashed()
            ->with(['orderProductRefunds' => function ($q1) {
                return $q1->where('status', 1)
                    ->where('refund_status', 3);
            }])
            ->get();
            $purchasePrice = 0.00;
        foreach ($order as $k1 => $v1) {
            $purchasePrice += $v1->order_fee + $v1->balance_discount;
            foreach ($v1->orderProductRefunds as $k2 => $v2) {
                $purchasePrice -= $v2->pay;
            }
        }
        if ($purchasePrice <= 0) {
            return '0.00';
        }
        return $purchasePrice;
    }

    /**
     * 用户第一张普通订单校验
     */
    public function isFirstOrder($userId, $orderId)
    {
        $order = Order::withTrashed()
            ->confirm()
            ->where('user_id', $userId)
            ->where('provider', 'standard')
            ->orderBy('id', 'desc')
            ->first();
        if ($order) {
            return false;
        }
        return true;
    }

    // 有售后纪录订单
    public function refundOrder($isAll = false, $request = null)
    {
        $orders = Order::whereHas('orderProductRefunds', function ($query) {
                //
        })
            ->with(['orderProducts' => function ($query) {
                return $query->whereHas('refund', function ($q) {
                        //
                })
                    ->with('inventory');
            }]);
        if ($isAll) {
            $orders = $orders->get();
        } else {
            $orders = $orders->orderBy('created_at', 'desc')
                ->paginate(config('amii.adminPaginate'));
        }

        return $orders;
    }
}
