<?php

namespace App\Services;

use App\Models\Setting;

class SettingService
{
    
    //find
    public function find($key)
    {
        return Setting::where('key', $key)
            ->first();
    }
    //update
    public function update($key, $value)
    {
        return Setting::where('key', $key)
            ->update(['value' => $value]);
    }
    
    public function check($key)
    {
        $value = Setting::where('key', $key)
            ->value('value');
        return $value ? true : false;
    }

    public function community()
    {
        return Setting::whereIn('key', ['subject','video','store'])
            ->get();
    }
}
