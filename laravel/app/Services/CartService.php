<?php
namespace App\Services;

use App\Models\Cart;
use App\Models\SellAttribute;
use App\Models\AttributeValue;
use App\Models\SellProduct;
use Ecommerce\Models\Inventory;
use App\Models\InventoryAttributeValue;
use DB;

class CartService
{

   /**
    * 新增
    * @param  [type] $arr [description]
    * @return [type]      [description]
    */
    public function store($arr)
    {
        if (!is_numeric($arr['number'])) {
            return false;
        }

        $inventory = Inventory::where('id', $arr['inventory_id'])
            ->with('SellProduct')
            ->first();

        // 查无该库存记录 库存不足
        if (!$inventory || !$inventory->SellProduct || $inventory->number < $arr['number']) {
            return false;
        }

        $where = [
            'inventory_id'    => $arr['inventory_id'],
            'sell_product_id' => $inventory->SellProduct->id,
            'user_id'         => $arr['user_id'],
        ];
        $arr['sell_product_id'] = $inventory->SellProduct->id;
        $hasExits = Cart::where($where)
            ->first();
        // 新增商品已存在
        if ($hasExits) {
            $update =  Cart::where($where)
                ->increment('number', $arr['number']);
            if ($update) {
                return Cart::where($where)
                    ->first();
            }
        }

        return Cart::create($arr);
    }

    /**
     * 批量新增到购物车
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function stores($arr, $user_id)
    {
        DB::beginTransaction();
        foreach ($arr as $key => &$value) {
            $value['user_id'] = $user_id;
            $create = $this->store($arr[$key]);
            if (!$create) {
                DB::rollback();
                return false;
            }
        }
        DB::commit();
        return true;
    }

    /**
     * 购物车列表
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function index($id)
    {
        $data['cart'] = Cart::where('user_id', $id)
            ->with('inventory.sku.skuValue.attribute')
            ->with('sellProduct.product')
            ->with('sellProduct.collection')
            ->get();
        $data['activityMessage'] = GiveProductService::checkUserFirstProduct();
        return $data;


    }

    /**
     * 单件商品的所有属性及属性值
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($sell_product_id)
    {
        $sellAttributeValue = [];
        $cart['inventories'] = app('inventory')->inventory($sell_product_id);               // 所有库存
        $cart['attributes']   = app('attribute')->productAttribute($sell_product_id);        // 商品属性
        $sellProduct = SellProduct::where('id', $sell_product_id)
            ->with('sellAttributeValue')
            ->first(); //属性值图
        if (isset($sellProduct->sellAttributeValue) && $sellProduct->sellAttributeValue) {
            $sellAttributeValue = $sellProduct->sellAttributeValue;
        }
        $cart['sell_attribute_value'] = $sellAttributeValue;
        if (!$cart['inventories']) {
            return false;
        }
        return $cart;

    }

   /**
    * 修改参数
    * @param  [type] $id  [description]
    * @param  [type] $arr [description]
    * @return [type]      [description]
    */
    public function update($id, $arr)
    {
        $inventory = Inventory::find($arr['inventory_id']);
        $cart = Cart::find($id);
        if (!$inventory || !$cart) {
            return false;
        }
        // 库存不足
        if ($inventory->number < $arr['number']) {
            return false;
        }
        $userId = auth()->id();
        $hasExits = Cart::where('inventory_id', $arr['inventory_id'])
            ->where('user_id', $userId)
            ->first();
        // 修改已存在的库存
        if ($arr['inventory_id'] != $cart->inventory_id && $hasExits) {
            DB::beginTransaction();
            $delete = Cart::destroy($id);
            $incr   = Cart::where('inventory_id', $arr['inventory_id'])
                ->where('user_id', $userId)
                ->increment('number', $arr['number']);
            if (!$delete || !$incr) {
                DB::rollback();
                return false;
            }
            DB::commit();
            return true;
        }
        if ($cart->sell_product_id != $inventory->product_sell_id) {
            return false;
        }
        return Cart::where('id', $id)
            ->update($arr);

    }

    /**
    * 删除 批量跟单删
    * @param  [type] $id [description]
    * @return [type]     [description]
    */
    public function delete($ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        return Cart::whereIn('id', $ids)
            ->delete();
    }

    /**
     * 获取用户购物车总数
     */
    public function number($userId)
    {
        return Cart::where('user_id', $userId)
            ->count();
    }
}
