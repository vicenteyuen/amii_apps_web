<?php
namespace App\Services;

use App\Models\ZhaoShang;

class ZhaoshangService
{
    /**
     * Zhaoshang index for admin
     */
    public function adminIndex($isStatus = false)
    {
        $query = new Zhaoshang;
        // 先以处理状态排序，再以创建时间排序
        $query = $query->orderBy('status')
            ->orderBy('created_at')
            ->paginate(config('amii.adminPaginate'));
        return $query;
    }

    /**
    *获取指定商家信息
    *@param   $user_id  int
    *@return  type [description]
    */
    public function show($user_id)
    {
        $query = Zhaoshang::where('id', $user_id)
            ->orderBy('created_at')
            ->first();

        return $query;
    }

    /**
    * 更新商家信息状态
    *@param   $arr       [description]
    *@param   $user_id   int
    *@return  type       [description]
    */
    public function update($arr, $user_id)
    {
        $exist = Zhaoshang::where('name', $arr['name'])
            ->first();

        if ($exist && $user_id != $exist->id ) {
            return ['status' => 0, 'msg' => '信息处理失败！该商家已存在'];
        }

        $update = Zhaoshang::where('id', $user_id)
            ->update($arr);

        if (!$update) {
            return ['status' => 0, 'msg' => '信息处理失败！'];
        }
        return ['status' => 1];
    }

}
