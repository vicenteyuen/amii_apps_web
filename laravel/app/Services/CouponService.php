<?php

namespace App\Services;

use App\Models\CouponGrant;
use App\Models\Coupon;
use App\Models\UserCoupon;
use App\Models\User;
use App\Helpers\AesHelper;
use DB;
use Carbon\Carbon;

class CouponService
{
    /**
     * coupon index for admin
     */
    public function adminIndex()
    {
        $query = Coupon::orderby('created_at', 'desc')
            ->paginate(config('amii.adminPaginate'));
        return $query;
    }

    /**
     * store coupon
     */
    public function store(array $arr)
    {
        $coupon = Coupon::create($arr);
        if (!$coupon) {
            return false;
        }
        dispatch(new \App\Jobs\CouponGrantCreate($coupon->id, $arr['number'], 0));
        return $coupon;
    }

    /**
    *  优惠券状态停用
    * updateStatus coupon
    */
    public function updateStatus($arr, $id)
    {
        $updateCoupon = Coupon::where('id', $id)
            ->update($arr);

        if ($updateCoupon) {
            return true;
        }
        return false;
    }

    /**
    *  获取优惠券券码
    * getCouponGrants coupon
    */
    public function getCouponGrants($id)
    {
        $data = CouponGrant::where('coupon_id', $id)
            ->paginate(50);
        return $data;
    }

    // 邀请领用优惠券
    public function invateCoupon($uuid, $userId)
    {
        DB::beginTransaction();
        $inviteUser = User::where('uuid', $uuid)
            ->first();
        if (!$inviteUser) {
            return false;
        }

        // add Coupon invite
        $inviteCouponArr = [
            'status'    => 1,
            'cid'       => AesHelper::createKey(10),
            'title'     => '满100减10',
            'number'    => 1,
            'remain'    => 0,
            'provider'  => 2,
            'value'     => 10,
            'lowest'    => 100,
            'start'     => Carbon::now(),
            'end'       => Carbon::now()->addDays(7),
        ];

        // add Coupon invited
        $invitedCouponArr1 = [
            'status'    => 1,
            'cid'       => AesHelper::createKey(10),
            'title'     => '满100减20',
            'number'    => 1,
            'remain'    => 0,
            'provider'  => 2,
            'value'     => 20,
            'lowest'    => 100,
            'start'     => Carbon::now(),
            'end'       => Carbon::now()->addMonth(),
        ];

        $inviteCoupon = Coupon::create($inviteCouponArr);
        $invitedCoupon1 = Coupon::create($invitedCouponArr1);
        if (!$inviteCoupon || !$invitedCoupon1) {
            DB::rollback();
            return false;
        }

        // add coupon_grants
        $inviteCouponGrantArr = [
            'coupon_id' => $inviteCoupon->id,
            'code'      => $inviteCoupon->cid,
            'status'    => 1,
        ];

        $invitedCouponGrantArr1 = [
            'coupon_id' => $invitedCoupon1->id,
            'code'      => $invitedCoupon1->cid,
            'status'    => 1,
        ];

        $inviteCouponGrant = CouponGrant::create($inviteCouponGrantArr);
        $invitedCouponGrant1 = CouponGrant::create($invitedCouponGrantArr1);
        if (!$inviteCouponGrant || !$invitedCouponGrant1) {
            DB::rollback();
            return false;
        }

        // add user_coupon
        $inviteUserCouponArr = [
            'coupon_id' => $inviteCoupon->id,
            'user_id'   => $inviteUser->id,
            'code'      => $inviteCoupon->cid,
            'status'    => 1,
        ];

        $invitedUserCouponArr1 = [
            'coupon_id' => $invitedCoupon1->id,
            'user_id'   => $userId,
            'code'      => $invitedCoupon1->cid,
            'status'    => 1,
        ];

        $inviteUserCoupon = UserCoupon::create($inviteUserCouponArr);
        $invitedUserCoupon1 = UserCoupon::create($invitedUserCouponArr1);
        if (!$inviteUserCoupon || !$invitedUserCoupon1) {
            DB::rollback();
            return false;
        }
        DB::commit();
        // 返回优惠券数据
        $data = [
            'inviteUserCouponTitle' =>   $inviteCouponArr['title'],
            'invitedUserCouponTitle' =>   $invitedCouponArr1['title']
        ];
        return $data;
    }
}
