<?php

namespace App\Services;

use App\Models\OrderProductRefundImage;

class OrderProductRefundImageService
{
    public function show($id)
    {
        return OrderProductRefundImage::where('id', $id)
            ->value('value');
    }
}
