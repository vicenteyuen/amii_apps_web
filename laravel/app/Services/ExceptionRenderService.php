<?php

namespace App\Services;

use \Illuminate\Http\Request;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionRenderService
{
    protected static $errorMsg = '请求错误';

    protected static $isAjax = false;

    public function __construct(Request $request)
    {
        if ($request->ajax() || $request->wantsJson()) {
            self::$isAjax = true;
        }
    }

    public static function render(Exception $e, $errorMsg = null)
    {
        if (self::$isAjax && ! config('app.debug')) {
            $error = false;
            if ($e instanceof ValidationException) {
                $error = true;
                $errorMsg = '验证失败';
            } else if ($e instanceof ModelNotFoundException) {
                $error = true;
                $errorMsg = '错误请求';
            } else if ($e instanceof AuthorizationException) {
                $error = true;
                $errorMsg = '登录错误';
            } else if ($e instanceof HttpException) {
                $error = true;
                $errorMsg = '网络错误';
            }
            if ($error) {
                $errorMsg = $errorMsg ? : self::$errorMsg;
                return app('jsend')->error($errorMsg);
            }
        }

        return false;
    }
}
