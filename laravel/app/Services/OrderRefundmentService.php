<?php

namespace App\Services;

use App\Models\OrderRefundment;

class OrderRefundmentService
{
    /**
     * 新增售后退款
     */
    public function store(array $data)
    {
        return OrderRefundment::create($data);
    }

    /**
     * 收到售后退货
     */
    public function receive($orderSn)
    {
        //
    }
}
