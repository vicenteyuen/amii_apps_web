<?php

namespace App\Services;

use App\Models\ExpressModels\ExpressTemplate;
use App\Models\ExpressModels\ExpressFee;
use App\Models\ExpressModels\ExpressCondition;
use Ecommerce\Models\Product;
use DB;

class ExpressTemplateService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr, $defaultFee = null, $province = null)
    {
        $arr['calc_type'] = 2; //满件 包邮金额
        if ($arr['free'] == 1) {
            return  ExpressTemplate::create($arr);
        }

        DB::beginTransaction();
        $template =  ExpressTemplate::create(array_only($arr, ['name','free','calc_type']));
        if (!$template) {
            return false;
        }
        $expressData = [
            'express_template_id' => $template->id,
            'default_fee' => $arr['default_fee'],
        ];

        // 包邮件条
        if (isset($arr['free_fee']) && !empty($arr['free_fee'])) {
            $expressData['free_fee'] = $arr['free_fee'];
        }

        // 包邮金额
        if (isset($arr['free_money']) && !empty($arr['free_money'])) {
            $expressData['free_money'] = $arr['free_money'];
        }

        $expressFee = ExpressFee::create($expressData);
        if (!$expressFee) {
            DB::rollback();
            return false;
        }

        $insert = [];
        if (!empty($defaultFee)) {
            foreach ($defaultFee as $key => $value) {
                $insert[] = [
                    'express_fee_id' => $expressFee->id,
                    'express_template_id' => $template->id,
                    'region_code'    => $province[$key],
                    'default_fee'    => $value,
                ];
            }
            if (!empty($insert)) {
                $insert = ExpressCondition::insert($insert);
                if (!$insert) {
                    DB::rollback();
                    return false;
                }
            }
        }
        DB::commit();
        return true;
    }

   /**
    * 编辑
    * @param  [type] $id         [description]
    * @param  [type] $arr        [description]
    * @param  [type] $defaultFee [description]
    * @param  [type] $province   [description]
    * @return [type]             [description]
    */
    public function save($id, $arr, $defaultFee = null, $province = null)
    {
        $arr['calc_type'] = 2; //满件 包邮金额
        if ($arr['free'] == 1) {
            ExpressTemplate::destroy($id);
            $create =  ExpressTemplate::create($arr);
            Product::where('express_template_id', $id)
                ->update(['express_template_id' => $create->id]);
            return $create;
        }

        DB::beginTransaction();
        $oldTemplate = ExpressTemplate::destroy($id); // 删除
        $template =  ExpressTemplate::create(array_only($arr, ['name','free','calc_type']));

        // 更新商品的运费模版
        $productUpdateExpress = Product::where('express_template_id', $id)
                ->update(['express_template_id' => $template->id]);

        if (!$oldTemplate || !$template) {
            return false;
        }
        $expressData = [
            'express_template_id' => $template->id,
            'default_fee' => $arr['default_fee'],
        ];

        //包邮条件
        if (isset($arr['free_fee']) && !empty($arr['free_fee'])) {
            $expressData['free_fee'] = $arr['free_fee'];
        }

        //包邮金额
        if (isset($arr['free_money']) && !empty($arr['free_money'])) {
            $expressData['free_money'] = $arr['free_money'];
        }

        $expressFeeId = ExpressFee::where('express_template_id', $id)
            ->value('id');
        //包邮条件表
        if ($expressFeeId) {
              $oldExpressFee = ExpressFee::destroy($expressFeeId); //删除
        }

        $expressFee = ExpressFee::create($expressData);
        if (!$expressFee) {
            DB::rollback();
            return false;
        }
    
        ExpressCondition::where('express_fee_id', $expressFeeId)
            ->delete(); //删除地区运费模版
        $insert = [];
        if (!empty($defaultFee)) {
            foreach ($defaultFee as $key => $value) {
                $insert[] = [
                    'express_fee_id' => $expressFee->id,
                    'express_template_id' => $template->id,
                    'region_code'    => $province[$key],
                    'default_fee'    => $value,
                ];
            }
            if (!empty($insert)) {
                $insert = ExpressCondition::insert($insert);
                if (!$insert) {
                    DB::rollback();
                    return false;
                }
            }
        }
        DB::commit();
        return true;
    }


    //列表
    public function index()
    {
        return ExpressTemplate::with('expressFee')
            ->get();
    }

    // 详情
    public function show($id)
    {
        return ExpressTemplate::where('id', $id)
            ->with('expressCondition.province')
            ->with('expressFee')
            ->first();
    }

    //删除
    public function delete($id)
    {
        $deleteTemplate = ExpressTemplate::where('id', $id)
            ->where('calc_type', 2)
            ->first();
        if ($deleteTemplate) {
            DB::beginTransaction();
            $deleteTemplate = $deleteTemplate->delete();
            Product::where('express_template_id', $id)
                ->update(['express_template_id' => 0]);
            if (isset($deleteTemplate->expressFee)) {
                $deleteFee = ExpressFee::destroy('express_template_id', $id);
                if ($deleteFee && $deleteTemplate) {
                    DB::commit();
                    return true;
                }
            } else {
                if ($deleteTemplate) {
                    DB::commit();
                    return true;
                }
            }
            
            DB::rollback();
            return false;
        }
        return false;
    }

    // 一键设置所有商品的运费模版
    public function setAllProductExpressTemplate($id)
    {
        return Product::where('express_template_id', '!=', $id)
            ->update(['express_template_id' => $id]);
    }


    //添加商品模版
    public function storeProduct($templateId, $productIds)
    {
        return Product::whereIn('id', $productIds)
            ->update(['express_template_id'=>$templateId]);
    }
    public function showTemplateProduct($id)
    {
        return Product::where('express_template_id', $id)
             ->paginate(config('amii.apiPaginate'));
    }
}
