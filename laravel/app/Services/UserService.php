<?php
namespace App\Services;

use App\Models\User;
use App\Models\PointLevel;
use App\Models\Enable;
use App\Models\LevelEnable;
use App\Models\Socialite;
use App\Models\Token;
use App\Helpers\AmiiHelper;
use App\Models\Tribe;
use App\Models\Balance;
use DB;
use App\Helpers\QrcodeHelper;
use App\Helpers\ImageHelper;
use App\Helpers\EnableHelper;

class UserService
{

    /**
     * 新增会员信息
     * @param  [type] $arr
     * @return cllection
     */
    public function store($arr)
    {
        if (isset($arr['phone']) && !isset($arr['nickname'])) {
            $arr['nickname'] = $arr['phone'];
        }
        $arr['gender'] = 2;
        return User::create(array_merge($arr, ['uuid' => AmiiHelper::newUuid()]));

    }

    /**
     * 删除会员
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {

        return User::destroy($id);

    }

    /**
     * find user by uuid
     */
    public function findByUuid($uuid = '')
    {
        return User::where('uuid', $uuid)
        ->first();
    }

     /**
     * 获取会员信息
     * @return collection
     */
    public function show($id)
    {
        return User::find($id);
    }

     /**
     * 更新会员信息
     * @param   $id  int
     * @param   $arr array
     * @return  bool
     */
    public function update(array $arr, $id)
    {
        $user = User::find($id);
        if ($user->birthday && isset($arr['birthday'])) {
            return ['status'=>0,'msg' =>'暂只支持客服修改，请联系客服修改','data' => ''];
        }
        $update = User::where('id', $id)
        ->update($arr);
        if ($update) {
             // 更新积分等级
            if (!empty($arr['birthday'])) {
                   app('pointReceived')->userAddPoint('update', 'birthday');
            }

            if (!empty($arr['gender'])) {
                   app('pointReceived')->userAddPoint('update', 'gender');
            }
            $updateUser =  User::where('id', $id)
            ->with('level.levelEnable.enable')
            ->first();
            return ['status' => 1,'data' => $updateUser,'msg' => ''];
        }
        return ['status'=>0,'msg' =>'修改个人信息失败','data' => ''];
    }

    /**
     * find user by where
     */
    public function findWhere(array $where, $item = true)
    {
        $query = User::where($where);
        if ($item) {
            return $query->first();
        }
        return $query->get();
    }

    /**
     * update password
     */
    public function updatePassword(User $user, $newpsd)
    {
        $user->password = bcrypt($newpsd);
        return $user->save();
    }

    /**
     * update phone
     */
    public function updatePhone(User $user, $newPhone)
    {
        $user->phone = $newPhone;
        return $user->save();
    }

    public function member($id, $request = null)
    {
        if (!is_null($request) && $request->purchase_price == true) {
            User::$calcPurchasePrice = true;
        }
        $user = User::where('id', $id)
        ->with('level.levelEnable.enable')
        ->first();
        if (!$user) {
            return false;
        }
        $user->enables = $this->userLevelEnable($id, $user->level);
        return $user;
    }

    /**
     * 生成二维码
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function qrcode($provider, $id)
    {
        $user = User::find($id);
        if (!$user || !$user->uuid) {
            return false;
        }
        // 我的二维码
        if (!$provider || is_numeric($provider)) {
            return QrcodeHelper::generateQr($provider, $user->uuid);
        }
        // 邀请用户注册的二维码
        if ($provider == 'info') {
            return QrcodeHelper::invateQrcode($user->uuid);
        }
        return null;

    }

    /**
     * 会员列表
     * @param  array $where [description]
     * @return [type]       [description]
     */
    public function index($where = [], $request = null)
    {
        if (!empty($request)) {
            $users = User::where(function ($query) use ($request) {
                return $query->where('phone', 'like', '%'.trim($request->search).'%')
                    ->orWhere('nickname', 'like', '%'.trim($request->search).'%');
            });
            // 时间段搜索
            if ($request->start && $request->end) {
                $users = clone($users)->whereBetween('created_at', [$request->start, $request->end]);
            } elseif ($request->start) {
                $users = clone($users)->where('created_at', '>=', $request->start);
            } elseif ($request->end) {
                $users = clone($users)->where('created_at', '<=', $request->end);
            }
            return clone($users)->paginate(20)
            ->appends($request->all());
        } else if (empty($where)) {
            return User::paginate(20);
        } else {
            return User::orderBy($where['sort'], $where['order'])
            ->paginate(20);
        }
    }
    /**
     * 获取会员，地址详情
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getDetail($id)
    {
        return User::where('id', $id)
        ->with('address')
        ->first();
    }

     /**
     * 修改该用户生日信息
     * @param  [type] $user_id [description]
     * @param  [type] $birthday [description]
     * @return [type]     [description]
     */
    public function updateBirthday($id, $birthday)
    {
        return User::where('id', $id)
        ->update(['birthday' => $birthday]);
    }

    /**
     * 获取用户部落信息
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getTribes($provider, $id)
    {
        $tribeId = Tribe::where('user_id', $id)
        ->value('id');

        $tribe = Tribe::where('pid', $tribeId)
        ->get();

        if ($provider == 'first') {
            // 一级部落
            $data =  Tribe::where('pid', $tribeId)
            ->with('user.level.levelEnable.enable')
            ->orderBy('created_at')
            ->paginate(config('ammii.adminPaginate'), ['*'], 'fpage');
        } else if ($provider == "secode") {
            // 二级部落
            $data = Tribe::whereIn('pid', $tribe->pluck('id'))
            ->with('user.level.levelEnable.enable')
            ->orderBy('created_at')
            ->paginate(config('ammii.adminPaginate'), ['*'], 'spage');
        } else {
            return false;
        }

            return $data;
    }

    /**
     * 获取用户可用积分
     */
    public function userRemainPoint($userId)
    {
        $user = User::where('id', $userId)
        ->first();
        if (! $user) {
            return 0;
        }
        return $user->points;
    }

    /**
     * 使用积分
     */
    public function usePoint($userId, $point)
    {
        // 添加积分消费记录
        return app('pointReceived')->userAddPoint('consumer', 'consumer', $point, $userId);
    }

    /**
     * 用户订单退回积分
     */
    public function rebackPoint($userId, $point)
    {
        // 退回积分增加明细
        app('pointReceived')->userAddPoint('reback', 'points', $point, $userId);
    }

    /**
     * 获取用户可用余额
     */
    public function userRemainGain($userId)
    {
        $user = User::where('id', $userId)
        ->first();
        if (! $user) {
            return 0;
        }
        return $user->cumulate_profit;
    }

    /**
     * 使用余额
     */
    public function useGain($userId, $gain)
    {
         //余额消费记录
        app('balance')->storeBalance([
        'value'     => -$gain,
        'type'      => 2,
        'status'    => 1,
        'user_id'   => $userId,
        ]);
        return User::where('id', $userId)
        ->where('cumulate_profit', '>=', $gain)
        ->decrement('cumulate_profit', $gain);
    }

    /**
     * 退回订单余额
     */
    public function rebackBalance($userId, $balance)
    {
        // 暂无退回余额退回记录
        User::where('id', $userId)
        ->increment('cumulate_profit', $balance);
    }

    /**
     * 绑定手机号
     */
    public function bundPhone($userId, $phone)
    {
        $returnData = [
        'status' => false
        ];

        DB::beginTransaction();

        $user = User::where('id', $userId)
        ->with('socialites')
        ->first();
        if (! $user->socialites->isEmpty()) {
            $provider = $user->socialites[0]->provider;
            $openid = $user->socialites[0]->openid;
            $unionid = $user->socialites[0]->unionid;
        } else {
            $provider = '';
            $openid = '';
            $unionid = '';
        }
            $userCheck = User::where('phone', $phone)
            ->first();

        if ($userCheck) {
            // 当前手机号已注册
            $userSocialites = $userCheck->load('socialites')
            ->get();
            if (! $userSocialites->isEmpty()) {
                foreach ($userSocialites as $socialite) {
                    if (in_array($socialite->provider, ['wechat', 'weweb', 'weapp']) && in_array($provider, ['wechat', 'weweb', 'weapp']) && $socialite->unionid != $unionid) {
                            // 微信登录过且unionid不相同
                            DB::rollback();
                            $returnData['message'] = '此手机号已绑定微信号';
                            return $returnData;
                    } elseif ($socialite->provider == $provider && $provider != '') {
                        // 此手机号绑定过相同的登录方式
                        DB::rollback();
                        $returnData['message'] = '此手机号已绑定过当前登录方式';
                        return $returnData;
                    }
                }
            }
                Socialite::where('user_id', $userId)
                ->where('provider', $provider)
                ->update([
                'user_id' => $userCheck->id
                ]);
                // 清楚此微信号绑定的用户
                User::where('id', $userId)
                ->delete();
                Token::where('ref_id', $userId)
                ->delete();
                $newUserId = $userCheck->id;
                // 更新token
                $token = app('token')->updateOrCreate($newUserId, 1);
        } else {
            // 当前手机号未注册
            $user->update([
            'phone' => $phone
            ]);
            $newUserId = $userId;
            $token = app('token')->updateOrCreate($userId, 1);
        }

            DB::commit();
            $childStr = '';
        if (in_array($provider, ['wechat', 'weweb', 'wemp', 'weapp'])) {
            // 微信
            $childStr = 'wechat';
        } elseif (in_array($provider, ['qqios', 'qqandroid'])) {
            // qq
            $childStr = 'qq';
        } elseif (in_array($provider, ['weiboapp'])) {
            // 微博
            $childStr = 'weibo';
        }
            app('pointReceived')->userAddPoint('register', 'bund', $childStr, $newUserId);
            $returnData['status'] = true;
            $returnData['data'] = $token;
            return $returnData;
    }

    /**
     * 校验手机号是否绑定当前的三方登录方式
     */
    private function phoneCheckBundOauth2($phone, $userId)
    {
        $user = User::where('id', $userId)
        ->with('socialites')
        ->first();
        if (! $user || $user->socialites->isEmpty()) {
            return false;
        }

        $provider = $user->socialites[0]->provider;

        $userCheck = User::where('phone', $phone)
        ->whereHas('socialites', function ($query) use ($provider) {
            return $query->where('provider', $provider);
        })
        ->first();
        if ($userCheck) {
            return true;
        }

        return false;
    }

    /**
     * 增加用户积分
     */
    public function addGain($phone, $number)
    {
        $user = User::where('phone', $phone)
        ->first();
        if (! ($user && $number)) {
            return false;
        }
        return $user->increment('cumulate_profit', $number);
    }

    // 通过邀请加积分
    public function addPointByInvente($uuid)
    {
        $user = User::where('uuid', $uuid)
        ->first();
        if ($user) {
            return app('pointReceived')->userAddPoint('invite', 'invite', null, $user->id);
        }
        return false;
    }

    //会员等级特权(lower)
    public function userLevelEnable($userId, $requiredLevel = null)
    {
        if ($requiredLevel <=1) {
            return null;
        }
        $userLevel =  User::where('id', $userId)
        ->value('level');

        $currentEnable = Enable::whereHas('levelEnable', function ($q1) use ($requiredLevel) {
            return $q1->where('level_id', $requiredLevel);
        })->get()
        ->toArray();
        foreach ($currentEnable as $key => $value) {
            $currentEnable[$key]['status'] = 0;
            EnableHelper::enable($currentEnable, $requiredLevel);
        }

        $userEnable = Enable::whereHas('levelEnable', function ($q1) use ($userLevel) {
            return $q1->where('level_id', $userLevel);
        })->get()
        ->toArray();

        if (empty($userEnable)) {
            return $currentEnable;
        }

        foreach ($currentEnable as $k1 => &$v1) {
            if ($userLevel >= $requiredLevel) {
                $v1['status'] = 1;
                $v1['image'] = str_replace('2.', '1.', $v1['image']);
            }
            $enable[] = $v1;
        }
        return $enable;
    }

    //通过手机找用户
    public function findUserByPhone($phone)
    {
        return User::where('phone', $phone)
        ->first();
    }

    // 获取用户id
    public function getUserId($uuid)
    {
        return User::where('uuid', $uuid)
        ->value('id');
    }

    // 生成密码
    public function createPassword($userId)
    {
        $user = self::show($userId);
        $password = time() - strtotime(date('Y-m-d', time()).' 00:00:00') + rand(1, 172800);//6位
        $password = substr($password, 0, 5).chr(rand(65, 90));
        $update = User::where('id', $userId)
        ->update(['password' => bcrypt($password)]);
        if ($update) {
            app()->make('leancloud', ['sms'])->requestSmsCode($user->phone, 'sendPswd', ['pswd' => $password]);
        }
    }


    // 更新用户信息
    public function updateUser($id, $arr)
    {
        $user = User::find($id);
        $user->status = $arr['status'];
        $user->save();
        if ($user->phone && $arr['status'] == 0) {
            $phone = $user->phone;
            app()->make('leancloud', ['sms'])->requestSmsCode($phone, 'blackList');
        }
        return true;
    }

    // 检查用户是否有被禁用
    public function checkUser($userId)
    {
        return User::where('id', $userId)
        ->value('status');
    }
    
    // 用户登陆添加游戏id
    public function addUserGameId($userId, $userGameId)
    {
        return User::where('id', $userId)
            ->update(['user_game_id' => $userGameId]);
    }
}
