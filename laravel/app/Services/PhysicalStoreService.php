<?php

namespace App\Services;

use App\Models\PhysicalStore;

class PhysicalStoreService
{
    public function index()
    {
        $stores = PhysicalStore::orderBy('weight', 'desc')
            ->orderBy('created_at', 'desc')
            ->where('status', 1)
            ->paginate(config('amii.apiPaginate'))
            ->appends(request()->all());

        foreach ($stores as $store) {
            $store->addHidden('description');
        }

        return $stores;
    }

    public function adminIndex()
    {
        $stores = PhysicalStore::orderBy('weight', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.apiPaginate'))
            ->appends(request()->all());

        foreach ($stores as $store) {
            $store->addHidden('description');
        }

        return $stores;
    }

    /**
     * 根据城市和经纬度获取门店列表
     *
     * @param $cityName   //城市名
     * @param $latitude   //纬度
     * @param $longitude  //经度
     * @param $is_paginate  //是否分页
     * @return mixed
     */
    public function indexOrderByDistance($cityName, $latitude = null, $longitude = null, $is_paginate = true, $is_all = true)
    {
        $distance_sql = "round(6371008 * acos(cos(radians(?)) * cos(radians(latitude)) * cos(radians(longitude) - radians(?)) + sin(radians(?)) * sin(radians(latitude)))) AS distance";

        $locationCityName = PhysicalStore::where('city_name', 'like', $cityName . '%')
            ->where('status', 1)
            ->value('city_name');

        if ($is_all) {
            $distance_sql = "round(6371008 * acos(cos(radians(?)) * cos(radians(latitude)) * cos(radians(longitude) - radians(?)) + sin(radians(?)) * sin(radians(latitude)))) AS distance, case city_name when '$locationCityName' then 1 when '北京市' then 2 when '上海市' then 3 when '广州市' then 4 else 5 end as main_city";
            $store = PhysicalStore::selectRaw('*,'.$distance_sql, [$latitude, $longitude, $latitude])
                ->where('status', 1)
                ->orderBy('main_city', 'asc')
                ->orderBy('distance', 'asc')
                ->orderBy('name_initial', 'asc');
        } else {
            $store = PhysicalStore::selectRaw('*,'.$distance_sql, [$latitude, $longitude, $latitude])
                ->where('status', 1)
                ->where('city_name', $locationCityName)
                ->orderBy('distance', 'asc');
        }
                
        $store = $is_paginate ? $store->paginate() : $store->get();
        return $store;
    }

    public function indexOrderCityName($cityName)
    {
        return PhysicalStore::where('city_name', 'like', $cityName . '%')
            ->where('status', 1)
            ->orderBy('name_initial')
            ->paginate();
    }

    /**
     * 根据门店首字母升序获取门店列表
     *
     * @return mixed
     */
    public function indexOrderByInitial()
    {
        return PhysicalStore::selectRaw("*, case city_name when '北京市' then 1 when '上海市' then 2 when '广州市' then 3 else 4 end as main_city")
            ->orderBy('main_city', 'asc')
            ->orderBy('name_initial', 'asc')
            ->paginate();
    }

    public function find($id)
    {
        return PhysicalStore::find($id);
    }

    public function store(array $arr)
    {
        return PhysicalStore::create($arr);
    }

    public function update(array $arr, $id)
    {
        return PhysicalStore::where('id', $id)
            ->update($arr);
    }

    public function delete($id)
    {
        return PhysicalStore::destroy($id);
    }
}
