<?php

namespace App\Services;

use App\Models\Tribe;
use App\Models\Gain;
use App\Models\User;
use App\Models\Balance;
use Carbon\Carbon;
use App\Models\Order;
use DB;

class GainService
{

    const FirstTribe = 0.1;
    const SecondTribe = 0.1;

    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function store($arr, $orderId)
    {
        // tribe表当前记录
        $currentUser = Tribe::where('user_id', $arr['user_id'])
            ->first();

        // 当前记录不存在
        if (!$currentUser) {
            return false;
        }
       
        // 上一级
        $upperUser = Tribe::where('id', $currentUser->pid)
            ->first();

        // 不存在上一级
        if (!$upperUser) {
            return false;
        }

        DB::beginTransaction();
         // 上上级
        $highestLevel = Tribe::where('id', $upperUser->pid)
            ->first();

        $income = $arr['income'] * self::SecondTribe;  // 计算上一级收益的值
        $income = floor(($income*100))/100;
        Order::where('id', $orderId)
            ->update(['first_tribe_return_money' =>  $income]);
        // 计算余额表的收益 Balance
        $createBlanace = [
            'user_id'       => $upperUser->user_id,
            'type'          => 1,
            'value'         => $income,
            'order_id'      => $orderId,
            'come_user_id'  => $arr['user_id'],
        ];
        // 添加余额表记录
        $balance = Balance::create($createBlanace);

        if (!$balance) {
            DB::rollback();
            return false;
        }

        $create = [
            'user_id'           => $arr['user_id'],
            'p_user_id'         => $upperUser->user_id,
            'income'            => $income,
            'total_orders'      => $arr['income'],
            'balance_id'        => $balance->id,
        ];
        // 添加记录
        $create1 = Gain::create($create);
        // 更新tribe 表记录(上一级)
        $addIncome = Tribe::where('id', $upperUser->id)
            ->increment('income', $income);
        // 贡献值
        $contributeIncome = Tribe::where('user_id', $arr['user_id'])
            ->increment('contribute_income', $income);
    
        // 增加待返金额
        $returnMoney = User::where('id', $upperUser->user_id)
            ->increment('return_money', $income);
        if (!$returnMoney) {
            DB::rollback();
            return false;
        }

        if ($highestLevel) {
            $income = $arr['income'] * self::FirstTribe;  //计算最高级收益的值
            $income = floor(($income*100))/100;
            // 计算余额表的收益 Balance
            Order::where('id', $orderId)
                ->update(['second_tribe_return_money' =>  $income]);
            $createUpperBlanace = [
                'user_id'       => $highestLevel->user_id,
                'type'          => 1,
                'value'         => $income,
                'order_id'      => $orderId,
                'come_user_id'  => $arr['user_id'],
            ];
            // 添加余额表记录
            $upperBalance = Balance::create($createUpperBlanace);
            if (!$upperBalance) {
                DB::rollback();
                return false;
            }
            $create = [
                'user_id'       => $arr['user_id'],
                'p_user_id'     => $highestLevel->user_id,
                'income'        => $income,
                'total_orders'  => $arr['income'],
                'balance_id'    => $upperBalance->id,
            ];

             // 添加记录
            $create2 = Gain::create($create);
            // 贡献值
            $addHighestIncome = Tribe::where('id', $highestLevel->id)
                ->increment('income', $income);

            // 增加待返金额
            $returnMoney = User::where('id', $highestLevel->user_id)
                ->increment('return_money', $income);
            if (!$returnMoney) {
                DB::rollback();
                return false;
            }

            if ($create1 && $balance && $upperBalance && $create2 && $addIncome && $addHighestIncome) {
                DB::commit();
                return true;
            }
            DB::rollback();
            return false;
        }
        // 只有上一级
        if ($create1 && $balance && $addIncome && $contributeIncome) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    // 绑定关系
    public function binding($uuid, $userId)
    {
        $pUserId = User::where('uuid', $uuid)
            ->value('id');
        if (!$pUserId) {
            return false;
        }
        $user = Tribe::where('user_id', $pUserId)
            ->first();
        if ($user) {
            return Tribe::create(['user_id'=>$userId,'pid' => $user->id]);
        } else {
            $pUser = Tribe::create(['user_id' => $pUserId,'pid' => 0]);
            if (!$pUser) {
                return false;
            }
            return Tribe::create(['user_id' =>$userId,'pid' => $pUser->id ]);
        }
    }

    // 部落首页
    public function userGain($userId)
    {
        $cumulateProfit = Balance::where('user_id', $userId)
            ->where('type', 1)
            ->where('status', 1)
            ->sum('value');
        $user = User::find($userId);
        $data['return_money'] = $user->return_money;
        $data['cumulate_profit'] = $cumulateProfit?:'0.00';
        $yesterdayIncome = Gain::where('p_user_id', $userId)
            ->where('created_at', '<=', Carbon::today())
            ->where('created_at', '>=', Carbon::today()->subDay())
            ->sum('income');
        $data['yesterdayIncome'] = number_format($yesterdayIncome, 2, '.', '');
        return $data;
    }

    // 近半个月的收益
    public function halfMonth($userId)
    {
        $today = strtotime('today');
        $halfMonthAgo =  $today - 15 * 86400;
        $incomes = Gain::selectRaw('sum(income) as income, date_format(created_at,"%Y-%m-%d") as date')
            ->where('p_user_id', $userId)
            ->where(DB::raw('unix_timestamp(created_at)'), '>=', $halfMonthAgo)
            ->groupBy(DB::raw('date_format(created_at,"%Y-%m-%d")'))
            ->pluck('income', 'date')
            ->toArray();
        $dateArr = array_keys($incomes);
        $recently = [];
        for ($i = $halfMonthAgo; $i < $today; $i = $i + 86400) {
            $current = date('Y-m-d', $i);
            if (in_array($current, $dateArr)) {
                $recently[] = ['day' => $current,'incomes' => $incomes[$current]];
            } else {
                $recently[] = ['day' => $current,'incomes' => '0.00'];
            }
        }
        return $recently;
    }
}
