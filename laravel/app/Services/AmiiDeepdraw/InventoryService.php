<?php

namespace App\Services\AmiiDeepdraw;

use App\Libraries\Deepdraw\Deepdraw;
use App\Jobs\AmiiDeepdraw;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\SellAttribute;
use App\Models\SellAttributeValue;
use Ecommerce\Models\Product;
use App\Models\SellProduct;
use Ecommerce\Models\Inventory;
use App\Models\InventoryAttributeValue;

class InventoryService
{
    /**
     * 商品库存数据处理并保存
     */
    public function storeInventorys($id, array $data)
    {
        $inventorys = $data;
        // 获取的数据固定了属性名 第一层为颜色 第二层为尺码
        $attributes = self::getAttribute($data);
        // 属性保存
        foreach ($attributes as $attribute) {
            $data = [
                'id' => $id,
                'endAttributeName' => end($attributes)['attributeName'],
                'attribute' => $attribute,
                'inventorys' => $inventorys
            ];
            $job = new AmiiDeepdraw('attribute', $data);
            dispatch($job);
        }
    }

    /**
     * 商品属性提取
     */
    private function getAttribute(array $data)
    {
        $arr = [];
        $tmp = [];
        foreach ($data as $key => $value) {
            $arr[] = $key;
            foreach ($value as $k => $v) {
                $tmp[] = $k;
            }
        }

        return [
            [
                'attributeName' => '颜色',
                'attributeValue' => array_unique($arr)
            ],
            [
                'attributeName' => '尺码',
                'attributeValue' => array_unique($tmp)
            ]
        ];
    }

    /**
     * 商品属性属性值处理
     */
    public function attribute(array $data)
    {
        $id = $data['id'];
        $attributeData = $data['attribute'];
        $endAttributeName = $data['endAttributeName'];
        $inventorys = $data['inventorys'];

        // 属性查询或创建并返回id
        $attribute = Attribute::where('name', $attributeData['attributeName'])
            ->first();
        if (! $attribute) {
            $create = Attribute::create([
                'name' => $attributeData['attributeName']
            ]);
            $attributeId = $create->id;
        } else {
            $attributeId = $attribute->id;
        }

        // 商品属性绑定
        $sellProduct = SellProduct::where('provider', 1)
            ->whereHas('product', function($q) use ($id) {
                $q->where('deepdraw_id', $id);
            })
            ->first();
        if (! $sellProduct) {
            return false;
        }
        $sellAttribute = SellAttribute::where('product_sell_attribute_id', $sellProduct->id)
            ->where('attribute_id', $attributeId)
            ->first();
        if (! $sellAttribute) {
            $createSellAttribute = SellAttribute::create([
                'attribute_id' => $attributeId,
                'product_sell_attribute_id' => $sellProduct->id
            ]);
            $sellAttributeId = $createSellAttribute->id;
        } else {
            $sellAttributeId = $sellAttribute->id;
        }

        foreach ($attributeData['attributeValue'] as $value) {
            $data = [
                'id' => $id,
                'sellAttributeId' => $sellAttributeId,
                'attributeId' => $attributeId,
                'attributeValue' => $value
            ];
            $job = new AmiiDeepdraw('attributeValue', $data);
            dispatch($job);
        }

        // 添加属性之后把商品库存处理放入队列，置于属性值之后处理
        if ($attributeData['attributeName'] == $endAttributeName) {
            foreach ($inventorys as $key => $inventory) {
                $data = [
                    'id' => $id,
                    'key' => $key,
                    'inventory' => $inventory
                ];
                $job = new AmiiDeepdraw('inventory', $data);
                dispatch($job);
            }
        }
    }

    /**
     * 属性值保存
     */
    public function attributeValue(array $data)
    {
        $id = $data['id'];
        $sellAttributeId = $data['sellAttributeId'];
        $attributeId = $data['attributeId'];
        $attributeValueData = $data['attributeValue'];

        // 属性值查询或保存并返回id
        $attributeValue = AttributeValue::where('value', $attributeValueData)
            ->first();
        if (! $attributeValue) {
            $create = AttributeValue::create([
                'attribute_id' => $attributeId,
                'value' => $attributeValueData
            ]);
            $attributeValueId = $create->id;
        } else {
            $attributeValueId = $attributeValue->id;
        }

        // 商品绑定属性值
        $sellProduct = SellProduct::where('provider', 1)
            ->whereHas('product', function($q) use ($id) {
                $q->where('deepdraw_id', $id);
            })
            ->first();
        if (! $sellProduct) {
            return false;
        }
        $sellAttributeValue = SellAttributeValue::where('sell_attribute_id', $sellAttributeId)
            ->where('attribute_value_id', $attributeValueId)
            ->where('attribute_id', $attributeId)
            ->first();
        if (! $sellAttributeValue) {
            SellAttributeValue::create([
                'sell_attribute_id' => $sellAttributeId,
                'attribute_value_id' => $attributeValueId,
                'attribute_id' => $attributeId
            ]);
        }
    }

    /**
     * 商品库存存储
     */
    public function inventory(array $data)
    {
        $id = $data['id'];
        $key = $data['key'];
        $inventory = $data['inventory'];

        // 获取商品id
        $sellProduct = SellProduct::where('provider', 1)
            ->whereHas('product', function($q) use ($id) {
                $q->where('deepdraw_id', $id);
            })
            ->first();
        if (! $sellProduct) {
            return false;
        }

        // 获取库存属性值id
        $id1 = self::getAttributeValueIdByAttributeValue($key);
        if ($id1 === false) {
            return false;
        }

        foreach ($inventory as $inventoryKey => $value) {
            // 保存商品编号
            if ($sellProduct->base_price === '0.00') {
                $sellProduct->update([
                    'base_price' => $value['价格'],
                    'market_price' => $value['价格']
                ]);
                $sellProduct->product->update([
                    'snumber' => $value['货号'],
                ]);
            }
            $idTmp = self::getAttributeValueIdByAttributeValue($inventoryKey);
            if ($idTmp === false) {
                return false;
            }

            $idArr = [
                $id1,
                $idTmp
            ];
            asort($idArr);
            $idStr = implode('_', $idArr);
            // 保存库存信息
            $inventoryData = [
                'product_id' => $sellProduct->product_id,
                'sku_code' => $value['商家编码'],
                'value_key' => $idStr,
                'mark_price' => $value['价格'],
                'number' => 0,
                'product_sell_id' => $sellProduct->id
            ];
            $data = [
                'id' => $id,
                'inventoryData' => $inventoryData
            ];
            $job = new AmiiDeepdraw('storeInventory', $data);
            dispatch($job);
        }
    }

    /**
     * 通过属性值获取属性值id
     */
    private function getAttributeValueIdByAttributeValue($value)
    {
        $attributeValue = AttributeValue::where('value', $value)
            ->first();
        if (! $attributeValue) {
            return false;
        }
        return $attributeValue->id;
    }

    /**
     * 存储商品sku库存
     */
    public function storeInventory(array $data)
    {
        $inventoryData = $data['inventoryData'];
        $create = Inventory::create($inventoryData);
        // 库存属性值保存
        $attributeValueIds = explode('_', $inventoryData['value_key']);
        foreach ($attributeValueIds as $attributeValueId) {
            InventoryAttributeValue::create([
                'inventory_id' => $create->id,
                'attribute_value_id' => $attributeValueId
            ]);
        }
    }
}
