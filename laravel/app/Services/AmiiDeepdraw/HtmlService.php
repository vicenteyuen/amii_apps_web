<?php

namespace App\Services\AmiiDeepdraw;

class HtmlService
{
    /**
     * image html
     */
    public function image($htmlStr = '', $url)
    {
        return $htmlStr . '<img src="/' . $url . '" style="width: 100%; height: auto">';
    }
}
