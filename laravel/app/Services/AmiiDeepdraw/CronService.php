<?php

namespace App\Services\AmiiDeepdraw;

use App\Jobs\AmiiDeepdraw;
use App\Models\AmiiDeepdraw\DeepdrawSaleCalender;

class CronService
{
    // step
    private $step;

    public function __construct(array $data = [])
    {
        if (! empty($data)) {
            $this->step = $data[0];
        } else {
            $this->step = 'all';
        }
    }
    /**
     * 定时任务调用入口
     * 停止深绘对接
     */
    public function handle()
    {
        switch ($this->step) {
            case 'merchant':
                \Log::info('--------- merchant --------');
                // 商户上货期数更新
                self::saleCalender();
                break;
            case 'calender':
                \Log::info('--------- calender ---------');
                // 商户未上货商品查询
                self::saleCalenderProduct();
                break;
            case 'product':
                \Log::info('--------- product ---------');
                // 补全未完整获取数据的商品信息
                self::productItem();
                break;
            case 'handle':
                \Log::info('--------- handle ---------');
                // 处理未处理的商品图片相关信息
                self::hadleProductItem();
                break;
            case 'publish':
                \Log::info('--------- publish ---------');
                // 发布待发布商品数据
                self::publish();
                break;
            case 'category':
                \Log::info('--------- category ---------');
                // 获取分类数据
                self::category();
                break;

            default:
                \Log::info('--------- all ---------');
                // 商户上货期数更新
                self::saleCalender();

                // 商户未上货商品查询
                self::saleCalenderProduct();

                // 补全未完整获取数据的商品信息
                self::productItem();

                // 处理未处理的商品图片相关信息
                self::hadleProductItem();

                // 发布待发布商品数据
                self::publish();

                // 获取分类数据
                self::category();
                break;
        }
    }

    /**
     * 商户上货期数查询
     */
    public function saleCalender()
    {
        $job = new AmiiDeepdraw('saleCalender');
        dispatch($job);
    }

    /**
     * 系统未上货期数商品查询
     */
    public function saleCalenderProduct()
    {
        $arrDay = app('amii.amiiDeepdraw.merchant')->undoSaleCalender();
        foreach ($arrDay as $key => $day) {

            \Log::info( 'GDebug: in saleCalenderProduct: day = ' , $day );


            // TODO: deepdraw: need to remove
            // if ($key > 4) {
            //     break;
            // }

            $arr = [
                'day' => $day['day'],
                'merchantId' => $day["merchant_id"],
            ];

            $job = new AmiiDeepdraw('saleCalenderProduct', $arr);
            dispatch($job);
        }
    }

    /**
     * 补全未完整的商品信息
     */
    public function productItem()
    {
        $productItems = app('amii.amiiDeepdraw.product.item')->unWhole();
        foreach ($productItems as $item) {
            if (! $item['basic_status']) {
                $data = [
                    'id' => $item['deepdraw_product_id']
                ];
                $job = new AmiiDeepdraw('productItemBasic', $data);
                dispatch($job);
            }

            // 商品图片
            if (! $item['picture_status']) {
                $data = [
                    'id' => $item['deepdraw_product_id']
                ];
                $job = new AmiiDeepdraw('productItemPicture', $data);
                dispatch($job);
            }

            // 商品详情
            if (! $item['vision_status']) {
                $data = [
                    'id' => $item['deepdraw_product_id']
                ];
                $job = new AmiiDeepdraw('productItemVision', $data);
                dispatch($job);
            }

            // 商品sku库存
            if (! $item['sku_status']) {
                $data = [
                    'id' => $item['deepdraw_product_id']
                ];
                $job = new AmiiDeepdraw('productItemSku', $data);
                dispatch($job);
            }
        }
    }

    /**
     * 处理图片相关的信息
     */
    public function hadleProductItem()
    {
        $unHandles = app('amii.amiiDeepdraw.product.item')->unHandle();
        foreach ($unHandles as $unHandle) {
            // 商品图片处理
            if ((! $unHandle['picture_handle_status']) && (! empty($unHandle['picture']))) {
                // 更改状态为已加入图片处理
                app('amii.amiiDeepdraw.product.item')->queuePictureHandle($unHandle['deepdraw_product_id']);
                $data = [
                    'id' => $unHandle['deepdraw_product_id'],
                    'images' => $unHandle['picture']
                ];
                $job = new AmiiDeepdraw('productImagesArr', $data);
                dispatch($job);
            }

            // 详情图片处理
            if ((! $unHandle['vision_handle_status']) && (! empty($unHandle['vision']))) {
                // 更改状态为已加入详情处理
                app('amii.amiiDeepdraw.product.item')->queueVisionHandle($unHandle['deepdraw_product_id']);
                $data = [
                    'id' => $unHandle['deepdraw_product_id'],
                    'images' => $unHandle['vision']
                ];
                $job = new AmiiDeepdraw('productDetailArr', $data);
                dispatch($job);
            }
        }
    }

    /**
     * 获取pull完的商品信息并发布商品
     */
    public function publish()
    {
        $productItemIdArr = app('amii.amiiDeepdraw.product.item')->ready();
        foreach ($productItemIdArr as $id) {
            $data = [
                'id' => $id
            ];
            $job = new AmiiDeepdraw('productItem', $data);
            dispatch($job);
        }
    }

    /**
     * 根据id获取所有子分类
     */
    public function category()
    {
        $id = 1;
        $categorys = app('amii.deepdraw.category')->category($id);
    }
}
