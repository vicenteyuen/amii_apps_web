<?php

namespace App\Services\AmiiDeepdraw;

use App\Models\AmiiDeepdraw\DeepdrawSaleCalender;
use App\Jobs\AmiiDeepdraw;

class MerchantService
{
    /**
     * 存储商户信息
     * @return boolean
     */
    public function store(array $data)
    {
        //
    }

    /**
     * 搜索并存储商户信息
     */
    public function searchStore(array $data)
    {
        //
    }

    /**
     * 商户上货期数存储
     */
    public function saleCalender(array $data)
    {
        $saleCalenderData = $data['saleCalenderData'];
        $merchantId = $data['merchantId'];

        $saleCalenders = DeepdrawSaleCalender::where(function ($query) use ($saleCalenderData, $merchantId) {
            foreach ($saleCalenderData as $saleCalender) {
                $query->orWhere(function ($q) use ($saleCalender, $merchantId) {
                    $q->where('sale_id', $saleCalender['id'])
                        ->where('merchant_id', $merchantId)
                        ->where('day', $saleCalender['day']);
                });
            }
        })
        ->get();
        $arrSaleCalenders = $saleCalenders->pluck('day', 'sale_id')
            ->toArray();

        $arrSaleCalender = [];
        foreach ($saleCalenderData as $saleCalender) {
            if (! in_array($saleCalender['id'], array_keys($arrSaleCalenders))) {
                $arrSaleCalender[] = $saleCalender;
            }
        }

        foreach ($arrSaleCalender as $saleCalender) {
            DeepdrawSaleCalender::create([
                'sale_id' => $saleCalender['id'],
                'merchant_id' => $merchantId,
                'day' => $saleCalender['day']
            ]);
        }
    }

    /**
     * 获取未处理上货日期
     */
    public function undoSaleCalender()
    {
        $arrDay = DeepdrawSaleCalender::undo()
            ->get();
        DeepdrawSaleCalender::undo()
            ->update([
                'status' => 1
            ]);

        return $arrDay->isEmpty() ? [] : $arrDay->toArray();
    }

    /**
     * 商户上货期数商品
     */
    public function calenderProducts(array $data)
    {
        $merchantId = $data['merchantId'];
        $data = $data['data'];
        foreach ($data as $product) {
            $arr = [
                'id' => $product['id'],
                'merchantId' => $merchantId
            ];
            $job = new AmiiDeepdraw('productItemStore', $arr);
            dispatch($job);
        }
    }
}
