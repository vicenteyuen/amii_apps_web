<?php

namespace App\Services\AmiiDeepdraw;

use App\Jobs\AmiiDeepdraw;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gimage\Gimage;
use App\Models\SellProduct;
use Ecommerce\Models\Product;
use App\Models\ProductImage;

class ImageService
{
    /**
     * 商品图片处理
     */
    public function productImagesArr($data)
    {
        $id = $data['id'];
        $images = $data['images'];

        foreach ($images as $provider => $providerImages) {
            $imagesKey = 0;
            if ($provider == 'TMALL') {
                // if (isset($providerImages['HOME'])) {
                //     foreach ($providerImages['HOME'] as $key => $image) {
                //         $data = [
                //             'id' => $id,
                //             'key' => $imagesKey++,
                //             'url' => $image,
                //         ];

                //         if ($key == count($providerImages['HOME']) - 1 && (! isset($images['ALL']['MODEL']))) {
                //             $data['end'] = true;
                //         } else {
                //             $data['end'] = false;
                //         }
                //         $job = new AmiiDeepdraw('productImages', $data);

                //         dispatch($job);
                //     }
                // }
            } elseif ($provider == 'ALL') {
                if (isset($providerImages['MODEL'])) {
                    foreach ($providerImages['MODEL'] as $key => $image) {
                        $data = [
                            'id' => $id,
                            'key' => $imagesKey,
                            'url' => $image,
                        ];
                        $imagesKey++;
                        $data['end'] = false;
                        if ($key == count($providerImages['MODEL']) - 1) {
                            $data['end'] = true;
                        }

                        $job = new AmiiDeepdraw('productImages', $data);
                        dispatch($job);
                    }
                }
            }
        }
    }

    /**
     * 商品图处理
     */
    public function productImages(array $data)
    {
        $id = $data['id'];
        $key = $data['key'];
        $url = $data['url'];
        $end = $data['end'];

        $imageName = self::imageStore($url, 'product');
        if (! $imageName) {
            return false;
        }
        $imageNameArr = [
            'key' => $key,
            'imageName' => $imageName
        ];

        app('amii.amiiDeepdraw.product.item')->pictureHandle($id, $imageNameArr, $end);
    }

    /**
     * 商品详情处理
     */
    public function productDetailArr($data)
    {
        $id = $data['id'];
        $images = $data['images'];
        $imageKey = 0;

        foreach ($images as $key => $detail) {
            if ($key>0) {
                break;
            }
            $screenShotSectionUrls = isset($detail['screenShotSectionUrls']) ? $detail['screenShotSectionUrls'] : [];
            if (! empty($screenShotSectionUrls)) {
                foreach ($screenShotSectionUrls as $k => $url) {
                    $data = [
                        'id' => $id,
                        'key' => $imageKey++,
                        'url' => $url,
                        'end' => false
                    ];
                    if ($k == count($screenShotSectionUrls) - 1) {
                        $data['end'] = true;
                    }
                    $job = new AmiiDeepdraw('productDetail', $data);
                    dispatch($job);
                }
            }
        }
    }

    /**
     * 商品详情图片处理
     */
    public function productDetail(array $data)
    {
        $id = $data['id'];
        $key = $data['key'];
        $url = $data['url'];
        $end = $data['end'];

        \Log::info('GDebug: productDetail: data: ', $data);

        $imageName = self::imageStore($url, 'product');
        if (! $imageName) {
            return false;
        }
        $imageNameArr = [
            'key' => $key,
            'imageName' => $imageName
        ];

        app('amii.amiiDeepdraw.product.item')->visionHandle($id, $imageNameArr, $end);
    }

    /**
     * 图片存储
     */
    public function imageStore($url, $provider)
    {

        if (empty($url)) {
            \Log::error('错误信息: ' . 'in imageStore: URL is empty .');
            return;
        }

        $deepdrawPath = config('amii.imageLocalPath') . '/deepdraw/';
        if (! is_dir($deepdrawPath)) {
            mkdir($deepdrawPath, 0755, true);
        }

        $urlArr = explode('/', $url);
        $imageName = end($urlArr);

        $deepdrawImageName = $deepdrawPath . $imageName; // image path


        $client = new Client();
        $response = $client->get($url);
        $image = $response->getBody()->getContents();
        if (file_exists($deepdrawImageName)) {
            unlink($deepdrawImageName);
        }
        $fp = fopen($deepdrawImageName, 'x');
        fwrite($fp, $image);
        fclose($fp);

        $image = new UploadedFile($deepdrawImageName, end($urlArr));

        $store = Gimage::uploadImage($image, $provider, 'images');
        if ($store instanceof \Exception) {
            return false;
        }
        if (file_exists($deepdrawImageName)) {
            unlink($deepdrawImageName);
        }

        return $store;
    }
}
