<?php

namespace App\Services\AmiiDeepdraw;

use App\Models\AmiiDeepdraw\DeepdrawCategory;
use EcommerceManage\Models\Category;
use App\Models\ProductCategories;

class CategoryService
{
    /**
     * 分类所有数据保存
     */
    public function storeCategorys(array $data)
    {

        $deepdrawCategory = DeepdrawCategory::where('deepdraw_category_id', $data['deepdrawCategoryId'])
            ->first();
        if (! $deepdrawCategory) {
            DeepdrawCategory::create([
                'deepdraw_category_id' => $data['deepdrawCategoryId'],
                'data' => $data['deepdrawCategorys']
            ]);
        } else {
            $deepdrawCategory->update([
                'data' => $data['deepdrawCategorys'],
            ]);
        }
    }

    /**
     * 商品绑定
     */
    public function product($productId, $merchantId, $categoryId)
    {
        \Log::info("in categoryService product: categoryId " . $categoryId);


        // 品牌处理
        $brandId = app('amii.amiiDeepdraw.brand')->brandNameId($merchantId);

        // 商品绑定品牌
        app('amii.amiiDeepdraw.brand')->brandProduct($brandId, $productId);

        // 获取分类家谱主线数据
        $categoryGene = self::geneTree($categoryId);
        if (empty($categoryGene)) {
            // 分类空不绑定分类
            return true;
        }

        // 分类处理
        $aCateId = self::categoryBrand($categoryGene, $categoryId, $brandId);
        // 商品绑定品牌分类
        self::brandCateProduct($brandId, $aCateId, $productId);
        return true;
    }

    /**
     * 商品绑定品牌分类
     */
    public function brandCateProduct($brandId, $cateId, $productId)
    {
        ProductCategories::firstOrCreate([
                'product_id' => $productId,
                'category_id' => $cateId,
                'brand_id' => $brandId
            ]);
    }

    /**
     * 根据id提取分类家谱树
     */
    public function geneTree($id)
    {
        // \Log::info("in categoryService geneTree: id: " . $id);

        // $id = 2;
        // $id = 9529;
        // // $id = 3;
        // $id = 8517;
        // 获取所有数据
        self::categorys($categorys);
        if (empty($categorys)) {
            return [];
        }

        $tree = self::tree($categorys, $id);
        if (empty($tree)) {
            return [];
        }
        return array_reverse($tree);
    }

    /**
     * 获取所有分类数据
     */
    private function categorys(&$categorys)
    {
        #TODO: 'deepdraw_category_id'
        #TODO: NEED review
        $deepdraw_category_id = 2;
        $data = DeepdrawCategory::where('deepdraw_category_id', $deepdraw_category_id)
            ->first();
        if (! $data) {
            $categorys = [];
        } else {
            $categorys = $data->data;
        }
    }

    /**
     * 通过id获取当前分类
     */
    private function category(array $data, $id)
    {
        $arr = [];
        foreach ($data as $categoryData) {
            if ($categoryData['id'] == $id) {
                if (isset($categoryData['children'])) {
                    unset($categoryData['children']);
                }
                $arr = $categoryData;
                break;
            } else {
                if (isset($categoryData['children'])) {
                    $arr = self::category($categoryData['children'], $id);
                    if (! empty($arr)) {
                        break;
                    }
                }
            }
        }
        return $arr;
    }

    /**
     * 通过id获取父分类
     */
    private function parent(array $data, $id)
    {
        $arr = [];
        foreach ($data as $category) {
            if (isset($category['children'])) {
                foreach ($category['children'] as $child) {
                    if ($child['id'] == $id) {
                        unset($category['children']);
                        $arr = $category;
                        break;
                    } else {
                        if (isset($child['children'])) {
                            $arr = self::parent($category['children'], $id);
                            break;
                        }
                    }
                }
            }
        }
        return $arr;
    }

    /**
     * 通过id获取家族树分类
     */
    private function tree(array $data, $id)
    {
        $arr = [];
        // 当前分类
        $category = self::category($data, $id);
        if (! empty($category)) {
            $arr[] = $category;
            $parent = self::parent($data, $id);
            if (! empty($parent)) {
                $arr = array_merge($arr, self::tree($data, $parent['id']));
            }
        }
        return $arr;
    }

    /**
     * 分类绑定品牌id
     */
    public function categoryBrand(array $categorys, $categoryId, $brandId)
    {
        $aCateId = 0;
        foreach ($categorys as $category) {
            if (isset($cateId)) {
                $parentId = $cateId;
            } else {
                $parentId = 0;
                $aCateId = self::deepCate($category, $parentId);
            }
            // 品牌分类对应
            $cateId = self::deepCate($category, $parentId);
            app('amii.amiiDeepdraw.brand')->brandCategory($brandId, $cateId);
        }
        return $aCateId;
    }

    /**
     * 分类对应
     */
    public function deepCate(array $data, $parentId)
    {
        $category = Category::firstOrCreate([
                'name' => $data['name'],
                'parent_id' => $parentId
            ]);

        return $category->id;
    }

}
