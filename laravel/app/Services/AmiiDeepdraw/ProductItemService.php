<?php

namespace App\Services\AmiiDeepdraw;

use App\Models\AmiiDeepdraw\DeepdrawProductItem;
use Ecommerce\Models\Product;
use App\Models\SellProduct;


class ProductItemService
{
    /**
     * 存储获取商品数据
     */
    public function storeProduct($id, $merchantId)
    {
        $item = DeepdrawProductItem::where('deepdraw_product_id', $id)
            ->where('merchant_id', $merchantId)
            ->first();
        if (! $item) {
            DeepdrawProductItem::create([
                'deepdraw_product_id' => $id,
                'merchant_id' => $merchantId
            ]);
        }
    }

    /**
     * 获取商品信息未完整的条目
     */
    public function unWhole()
    {
        $items = DeepdrawProductItem::unWhole()
            ->get()
            ->toArray();
        return $items;
    }

    /**
     * 获取图片未处理的条目
     */
    public function unHandle()
    {
        $items = DeepdrawProductItem::unHandle()
            ->get()
            ->toArray();
        return $items;
    }

    /**
     * 更新商品图片加入队列处理
     */
    public function queuePictureHandle($id)
    {
        DeepdrawProductItem::where('deepdraw_product_id', $id)
            ->update([
                'picture_handle_queue_status' => 1
            ]);
    }

    /**
     * 更新商品详情加入队列处理
     */
    public function queueVisionHandle($id)
    {
        DeepdrawProductItem::where('deepdraw_product_id', $id)
            ->update([
                'vision_handle_queue_status' => 1
            ]);
    }

    /**
     * 获取待添加商品信息
     */
    public function ready()
    {
        $productItemIdArr = DeepdrawProductItem::publishing()
            ->status()
            ->pluck('id')
            ->toArray();
        return $productItemIdArr;
    }

    /**
     * 更改商品发布状态为已完成发布状态
     */
    public function publish(array $data)
    {
        // 这里的id是deepdraw product item 表id
        $id = $data['id'];

        // dd("in publish: " . $id);

        $item = DeepdrawProductItem::where('id', $id)
            ->first();
        if (! $item || $item->is_publish) {
            return false;
        }

        // 更改已发布状态
        $item->update([
            'is_publish' => 1
        ]);

        $deepdrawProductId = $item->deepdraw_product_id;

        // 发布商品基本信息
        $data = [
            'name' => $item->basic['title'],
            'deepdraw_id' => $deepdrawProductId,
            'merchant_id' => $item->merchant_id,
            'status' => 1,
            'snumber' => $item->basic['code'],
        ];


        $product = Product::create($data);

        SellProduct::create([
            'product_id' => $product->id,
            'provider' => 1,
            'status' => 1
        ]);

        // 商品绑定分类
        $categoryId = $item->basic['trade']['id'];
        app('amii.amiiDeepdraw.category')->product($product->id, $item->merchant_id, $categoryId);

        // 发布商品图
        $productImages = $item->picture_handle;
        $productIndexImage = array_shift($productImages);
        // 商品列表展示图
        app('amii.amiiDeepdraw.product')->productImageStore($deepdrawProductId, $productIndexImage);
        // 商品主图
        $productImage = array_shift($productImages);
        app('amii.amiiDeepdraw.product')->sellProductImage($deepdrawProductId, $productImage);
        // 商品图
        foreach ($productImages as $productImage) {
            app('amii.amiiDeepdraw.product')->productImagesStore($deepdrawProductId, $productImage);
        }

        if (empty($item->vision_handle)) {
            \Log::error( 'GDebug: in publish: vision_handle is null.' );
            return;
        }

        // 发布商品详情
        $htmlStr = '';
        foreach ($item->vision_handle as $detail) {
            $htmlStr = app('amii.amiiDeepdraw.html')->image($htmlStr, $detail);
        }
        $product->update([
            'detail' => $htmlStr
        ]);

        if (empty($item->sku)) {
            return;
        }



        // 发布商品sku库存
        app('amii.amiiDeepdraw.inventory')->storeInventorys($deepdrawProductId, $item->sku);
    }

    /**
     * 商品基本信息填充
     */
    public function base($deepdrawProductId, $str)
    {
        if (is_array($str)) {
            $str = json_encode($str);
        }
        DeepdrawProductItem::where('deepdraw_product_id', $deepdrawProductId)
            ->update([
                'basic' => $str,
                'basic_status' => 1
            ]);
    }

    /**
     * 商品图片信息填充
     */
    public function picture($deepdrawProductId, $str)
    {
        if (is_array($str)) {
            $str = json_encode($str);
        }
        DeepdrawProductItem::where('deepdraw_product_id', $deepdrawProductId)
            ->update([
                'picture' => $str,
                'picture_status' => 1
            ]);
    }

    /**
     * 商品详情信息填充
     */
    public function vision($deepdrawProductId, $str)
    {
        if (is_array($str)) {
            $str = json_encode($str);
        }
        DeepdrawProductItem::where('deepdraw_product_id', $deepdrawProductId)
            ->update([
                'vision' => $str,
                'vision_status' => 1
            ]);
    }

    /**
     * 商品sku数据填充
     */
    public function sku($deepdrawProductId, $str)
    {
        if (is_array($str)) {
            $str = json_encode($str);
        }
        DeepdrawProductItem::where('deepdraw_product_id', $deepdrawProductId)
            ->update([
                'sku' => $str,
                'sku_status' => 1
            ]);
    }

    /**
     * 商品基本信息更新
     */
    public function productItemBasic($id)
    {
        // 获取商品基本信息并保存
        app('amii.deepdraw.product')->show($id);
    }

    /**
     * 商品图片信息更新
     */
    public function productItemPicture($id)
    {
        // 获取并保存图片信息
        app('amii.deepdraw.product')->productImage($id);
    }

    /**
     * 商品详情信息更新
     */
    public function productItemVision($id)
    {
        // 获取并保存图片信息
        app('amii.deepdraw.product')->productDetail($id);
    }

    /**
     * 商品sku库存信息更新
     */
    public function productItemSku($id)
    {
        // 获取并保存sku库存信息
        app('amii.deepdraw.inventory')->inventorys($id);
    }

    /**
     * 商品图片数据处理
     */
    public function pictureHandle($id, $imageNameArr, $end = false)
    {
        $item = DeepdrawProductItem::where('deepdraw_product_id', $id)
            ->first();
        if (! $item) {
            return false;
        }
        $handlePicture = $item->picture_handle;
        $handlePicture[$imageNameArr['key']] = $imageNameArr['imageName'];

        $item->update([
            'picture_handle' => $handlePicture
        ]);

        if ($end) {
            $item->update([
                'picture_handle_status' => 1
            ]);
        }
    }

    /**
     * 商品详情数据处理
     */
    public function visionHandle($id, $imageNameArr, $end = false)
    {
        $item = DeepdrawProductItem::where('deepdraw_product_id', $id)
            ->first();
        if (! $item) {
            return false;
        }
        $handleVisionImage = $item->vision_handle;
        $handleVisionImage[$imageNameArr['key']] = $imageNameArr['imageName'];

        $item->update([
            'vision_handle' => $handleVisionImage
        ]);

        if ($end) {
            ksort($handleVisionImage);
            $item->update([
                'vision_handle' => $handleVisionImage
            ]);
            $item->update([
                'vision_handle_status' => 1,
            ]);
        }
    }
}
