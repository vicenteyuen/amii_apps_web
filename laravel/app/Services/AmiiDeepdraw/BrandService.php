<?php

namespace App\Services\AmiiDeepdraw;

use EcommerceManage\Models\Brand;
use App\Models\BrandProduct;
use App\Models\BrandCategory;

class BrandService
{
    /**
     * 通过品牌名获取品牌id
     */
    public function brandNameId($merchantId)
    {
        $brand = Brand::firstOrCreate(['merchant_id' => $merchantId]);
        return $brand->id;
    }

    /**
     * 商品绑定品牌
     */
    public function brandProduct($brandId, $productId)
    {
        BrandProduct::firstOrCreate([
                'brand_id' => $brandId,
                'product_id' => $productId
            ]);
    }

    /**
     * 品牌对应分类
     */
    public function brandCategory($brandId, $categoryId)
    {
        BrandCategory::firstOrCreate([
                'brand_id' => $brandId,
                'category_id' => $categoryId
            ]);
    }
}
