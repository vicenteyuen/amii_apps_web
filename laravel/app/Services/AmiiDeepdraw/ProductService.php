<?php

namespace App\Services\AmiiDeepdraw;

use App\Jobs\AmiiDeepdraw;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gimage\Gimage;
use App\Models\SellProduct;
use Ecommerce\Models\Product;
use App\Models\ProductImage;

class ProductService
{
    /**
     * 存储商品信息
     */
    public function store($id, array $data)
    {
        self::product($id, $data);
    }

    /**
     * 商品基本信息存储
     * @param integer $id
     * @param array $data
     */
    public function product($id, array $data)
    {
        $title = $data['title'];

        $product = Product::create([
            'name' => $title,
            'deepdraw_id' => $id
        ]);

        SellProduct::create([
            'product_id' => $product->id,
            'provider' => 1,
            'status' => 0
        ]);
    }

    /**
     * 存储商品图片
     * @param integer $id
     * @param array $data
     */
    public function productImage($id, array $data)
    {
        foreach ($data as $provider => $images) {
            if ($provider == 'ALL') {
                // if (isset($images['MODEL'])) {
                //     foreach ($images['MODEL'] as $key => $image) {
                //         $data = [
                //             'id' => $id,
                //             'provider' => 'productImagesStore',
                //             'url' => $image,
                //             'imageProvider' => 'product',
                //         ];

                //         $job = new AmiiDeepdraw('imageStore', $data);
                //         dispatch($job);
                //     }
                // }
            } elseif ($provider == 'TMALL') {
                if (isset($images['HOME'])) {
                    foreach ($images['HOME'] as $key => $image) {
                        $data = [
                            'id' => $id,
                            'url' => $image,
                            'imageProvider' => 'product',
                        ];

                        if ($key == 0) {
                            $data['provider'] = 'sellProductImage';
                        } elseif ($key == 1) {
                            $data['provider'] = 'productImageStore';
                        } else {
                            $data['provider'] = 'productImagesStore';
                        }

                        $job = new AmiiDeepdraw('imageStore', $data);
                        dispatch($job);
                    }
                }
            }
        }
    }

    /**
     * 保存商品展示图
     */
    public function sellProductImage($id, $imageName)
    {
        SellProduct::where('provider', 1)
            ->whereHas('product', function ($query) use ($id) {
                $query->where('deepdraw_id', $id);
            })
            ->update([
                'image' => $imageName
            ]);
    }

    /**
     * 保存商品主图
     */
    public function productImageStore($id, $imageName)
    {
        Product::where('deepdraw_id', $id)
            ->update([
                'main_image' => $imageName
            ]);
    }

    /**
     * 保存商品图
     */
    public function productImagesStore($id, $imageName)
    {
        $product = Product::where('deepdraw_id', $id)
            ->first();
        if (! $product) {
            return false;
        }
        ProductImage::create([
            'product_id' => $product->id,
            'image' => $imageName
        ]);
    }

    /**
     * 存储商品详情
     * @param integer $id
     * @param array $data
     */
    public function productDetail($id, array $data)
    {
        foreach ($data as $key => $detail) {

            // \Log::info("in productDetail : " . $key);

            if ($key > 0) break;

            $screenShotSectionUrls = isset($detail['screenShotSectionUrls']) ? $detail['screenShotSectionUrls'] : [];
            if (! empty($screenShotSectionUrls)) {
                foreach ($screenShotSectionUrls as $url) {
                    if ($url) {
                        $data = [
                            'id' => $id,
                            'provider' => 'detailImage',
                            'url' => $url,
                            'imageProvider' => 'product'
                        ];

                        $job = new AmiiDeepdraw('imageStore', $data);
                        dispatch($job);
                    }
                }
            }
        }
    }

    /**
     * 商品详情数据拼接
     */
    public function productDetailStore($id, $imageName)
    {
        $product = Product::where('deepdraw_id', $id)
            ->first();
        if ($product) {
            $htmlStr = $product->detail;
        } else {
            $htmlStr = '';
        }

        $htmlStr = app('amii.amiiDeepdraw.html')->image($htmlStr, $imageName);

        Product::where('deepdraw_id', $id)
            ->update([
                'detail' => $htmlStr,
            ]);
    }

    /**
     * 图片链接获取图片并保存
     */
    public function imageStore($id, $provider, $url, $imageProvider)
    {
        $deepdrawPath = public_path() . '/uploads/deepdraw/';

        $urlArr = explode('/', $url);
        $imageName = end($urlArr);

        $deepdrawImageName = $deepdrawPath . $imageName; // image path


        $client = new Client();
        $response = $client->get($url);
        $image = $response->getBody()->getContents();
        if (file_exists($deepdrawImageName)) {
            unlink($deepdrawImageName);
        }
        $fp = fopen($deepdrawImageName, 'x');
        fwrite($fp, $image);
        fclose($fp);

        $image = new UploadedFile($deepdrawImageName, end($urlArr));

        $store = Gimage::uploadImage($image, $imageProvider, 'images');
        if ($store instanceof \Exception) {
            return false;
        }
        if (file_exists($deepdrawImageName)) {
            unlink($deepdrawImageName);
        }

        $data = [
            'id' => $id,
            'name' => $imageName,
            'provider' => $provider
        ];

        $job = new AmiiDeepdraw('productImageStore', $data);
        dispatch($job);
    }
}
