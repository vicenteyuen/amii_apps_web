<?php

namespace App\Services\AmiiDeepdraw;

class JobService
{
    // 商户id
    private $merchantid;

    public function __construct()
    {
        $this->merchantid = config('amii.deepdraw.merchant.id');
    }

    /**
     * 接收队列
     */
    public function handle($jobProvider, array $data)
    {
        switch ($jobProvider) {
            case 'inventorys':
                self::inventorys($data);
                break;
            case 'attribute':
                self::attribute($data);
                break;
            case 'attributeValue':
                self::attributeValue($data);
                break;
            case 'storeInventory':
                self::storeInventory($data);
                break;



// __START__

            case 'saleCalender':
                self::saleCalender();
                return true;
                break;
            case 'saleCalenderProduct':
                self::saleCalenderProduct($data);
                break;
            case 'productItem':
                self::productItem($data);
                break;
            case 'productItemStore':
                self::productItemStore($data);
                break;
            case 'productItemBasic':
                self::productItemBasic($data);
                break;
            case 'productItemPicture':
                self::productItemPicture($data);
                break;
            case 'productItemVision':
                self::productItemVision($data);
                break;
            case 'productItemSku':
                self::productItemSku($data);
                break;
            case 'productImagesArr':
                self::productImagesArr($data);
                break;
            case 'productImages':
                self::productImages($data);
                break;
            case 'productDetailArr':
                self::productDetailArr($data);
                break;
            case 'productDetail':
                self::productDetail($data);
                break;
            case 'inventory':
                self::inventory($data);
                break;

            default:
                return false;
                break;
        }
    }

    /**
     * 根据商品id获取商品库存
     */
    public function inventorys(array $data)
    {
        if (! empty($data)) {
            app('amii.deepdraw.inventory')->inventorys($data['id']);
        }
    }

    /**
     * 属性校验并绑定商品
     */
    public function attribute(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.inventory')->attribute($data);
        }
    }

    /**
     * 属性值保存
     */
    public function attributeValue(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.inventory')->attributeValue($data);
        }
    }

    /**
     * 库存处理
     */
    public function inventory(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.inventory')->inventory($data);
        }
    }

    /**
     * 商品库存保存
     */
    public function storeInventory(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.inventory')->storeInventory($data);
        }
    }






// __END__

    /**
     * 商户上货期数查询
     */
    public function saleCalender()
    {
        // 测试数据，取得商家1-20商户的数据
        // for ($i=1; $i < 20 ; $i++) {
        //     app('amii.deepdraw.merchant')->saleCalender($i);
        // }

        // 1024,1025
        $merchantIdArr = explode(',', $this->merchantid);
        foreach ($merchantIdArr as $merchantid) {
            \Log::info( 'GDebug: in saleCalender: merchantid = ' . $merchantid);
            app('amii.deepdraw.merchant')->saleCalender($merchantid);
        }
    }

    /**
     * 商户期数商品查询
     */
    public function saleCalenderProduct(array $data)
    {
        if (! empty($data)) {
            app('amii.deepdraw.merchant')->calenderProducts($data['merchantId'], $data['day']);
        }
    }

    /**
     * 存储deepdraw product id
     */
    public function productItemStore(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.product.item')->storeProduct($data['id'], $data['merchantId']);
        }
    }

    /**
     * 获取deepdraw商品基本信息
     */
    public function productItemBasic(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.product.item')->productItemBasic($data['id']);
        }
    }

    /**
     * 获取deepdraw商品图片信息
     */
    public function productItemPicture(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.product.item')->productItemPicture($data['id']);
        }
    }

    /**
     * 获取deepdraw商品详情信息
     */
    public function productItemVision(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.product.item')->productItemVision($data['id']);
        }
    }

    /**
     * 获取deepdraw商品sku库存信息
     */
    public function productItemSku(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.product.item')->productItemSku($data['id']);
        }
    }

    /**
     * 商品图片数据处理
     */
    public function productImagesArr(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.image')->productImagesArr($data);
        }
    }

    /**
     * 保存商品图片
     */
    public function productImages(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.image')->productImages($data);
        }
    }

    /**
     * 商品详情处理
     */
    public function productDetailArr(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.image')->productDetailArr($data);
        }
    }

    /**
     * 商品详情处理
     */
    public function productDetail(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.image')->productDetail($data);
        }
    }

    /**
     * 发布商品
     */
    public function productItem(array $data)
    {
        if (! empty($data)) {
            app('amii.amiiDeepdraw.product.item')->publish($data);
        }
    }
}
