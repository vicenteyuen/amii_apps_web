<?php

namespace App\Services;

use EcommerceManage\Models\Brand;
use App\Models\BrandCategory;
use EcommerceManage\Models\Category;

class BrandCategoryService
{
    /**
     * 获取品牌下分类
     */
    public function getBrandCategorys($brand, $categoryId)
    {
        // 获取品牌所有分类id
        $categoryIds = Category::whereHas('brand', function ($q) use ($brand) {
            return $q->where('brand_categorys.brand_id', $brand);
        })
        ->whereHas('brandProduct', function ($q1) {
            return $q1->where('status', 1);
        })
        ->pluck('id')
        ->toArray();
        // 获取品牌所有分类
        $categorys = Category::whereHas('brand', function ($q) use ($brand) {
            return $q->where('brand_categorys.brand_id', $brand);
        })
         ->whereHas('brandProduct', function ($q1) {
            return $q1->where('status', 1);
         })
        ->where(function ($q) use ($categoryId, $categoryIds) {
            if ($categoryId) {
                return $q->where('parent_id', $categoryId);
            }
            return $q->whereIn('id', $categoryIds);
        })
        ->get();
        return $categorys;
        
    }

    //删除分类下的商品
    public function delete($brandId, $productId)
    {
        return BrandCategory::where(['brand_id'=>$brandId,'product_id' => $productId])
            ->delete();
    }

    //获取品牌展示图
    public function productIndexImage($brandId = 0)
    {
        $brand = Brand::where('id', $brandId)
            ->first();
        if (! $brand) {
            return '';
        }

        return $brand->logo;
    }
}
