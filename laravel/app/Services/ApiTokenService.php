<?php

namespace App\Services;

class ApiTokenService
{
    public static function generateToken()
    {
        return md5(openssl_random_pseudo_bytes(16) . microtime());
    }
}
