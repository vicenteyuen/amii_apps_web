<?php

namespace App\Services;

use App\Models\Banner;
use Cache;

class BannerService
{
    /**
     * banner index for api
     */
    public function index($take = 8)
    {
        $banners = Cache::rememberForever('banners', function () {
            return Banner::where('status', 1)->get()
                ->toArray();
        });
        return $banners;
    }

    /**
     * banner index for admin
     */
    public function adminIndex($isStatus = false)
    {
        $query = new Banner;
        $query = $query->orderBy('weight', 'desc')->paginate(config('amii.adminPaginate'));
        return $query;
    }

    /**
     * store banner
     */
    public function store(array $arr)
    {
        Banner::create($arr);

        if (Cache::get('banners')) {
            Cache::forget('banners');
        }
        Cache::rememberForever('banners', function () {
            return Banner::get()
                ->toArray();
        });
        return true;
    }

    /**
    *  更新首页轮播
    *  updateStatus banner
    */
    public function updateStatus($arr, $id)
    {

        $updateBanner = Banner::where('id', $id)
            ->update($arr);

        if ($updateBanner) {
            if (Cache::get('banners')) {
                Cache::forget('banners');
            }
            return true;
        }
        return false;
    }

    /**
    *  获取轮播详情
    *  show banner
    */
    public function showBanner($id)
    {
        $showBanner = Banner::where('id', $id)
            ->first();

        return $showBanner;
    }

    /**
     * 删除
     * delete banner
     */
    public function delete($id)
    {
        return Banner::destroy($id);
    }
}
