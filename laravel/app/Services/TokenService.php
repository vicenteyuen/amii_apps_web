<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Token;

class TokenService
{
    /**
     * update or create token
     * @param $refId 用户id
     * @param $type 登录类型 1客户端登陆
     */
    public function updateOrCreate($refId, $type)
    {
        $newToken = self::newToken();

        $token = Token::whereRefId($refId)
            ->whereType($type)
            ->first();
        if ($token) {
            if (config('app.debug')) {
                if ($refId == 1) {
                    return $token->code;
                }
            }
            $token->code = $newToken;
            $token->save();
        } else {
            Token::create([
                'ref_id'    => $refId,
                'type'      => $type,
                'code'      => $newToken,
            ]);
        }

        return $newToken;
    }

    /**
     * 创建新token删除过期token
     */
    public function createAndExpires($refId)
    {
        $newToken = self::newToken();

        // 创建新token
        Token::create([
                'ref_id' => $refId,
                'code' => $newToken
            ]);
        return $newToken;
    }

    /**
     * 重置用户所有token
     */
    public function resetAllToken($refId, $type = 1)
    {
        Token::whereRefId($refId)->delete();
        $newToken = Token::create([
            'ref_id'    => $refId,
            'type'      => $type,
            'code'      => self::newToken()
        ]);
        return $newToken->code;
    }

    /**
     * create new token
     */
    protected function newToken()
    {
        return ApiTokenService::generateToken();
    }
}
