<?php
namespace App\Services;

use App\Models\UserSuit;
use App\Models\SuitSize;
use App\Models\UserQuestion;
use App\Models\UserAnswer;
use Ecommerce\Models\Inventory;
use App\Helpers\SizeHelper;
use DB;

class UserSuitService
{
    /**
     * 新增个人身材数据
     * @param  [type] $arr
     * @return cllection
     */
    public function store(array $arr)
    {
        //第一条设为默认地址
        $collection = UserSuit::where('user_id', $arr['user_id'])
            ->where('status', '1')
            ->first();

        if (is_null($collection)) {
            $arr['status'] = 1;
            return UserSuit::create($arr);
        }
        
        // 没有设为默认地址并且不是第一条
        if (!$arr['status']) {
            return UserSuit::create($arr);
        }

        // 新增地址并设为默认地址
        DB::beginTransaction();

        $updateStatus = UserSuit::where('user_id', $arr['user_id'])
            ->update(['status' => 0]);

        $insertModel = UserSuit::create($arr);

        if (!$updateStatus || !$insertModel) {
            DB::rollback();
            return false;
        }

        DB::commit();
        return $insertModel;
    }

    /**
     * 设置默认身材数据
     * @param  [type] $user_id [description]
     * @param  [type] $id      [description]
     * @return [type]          [description]
     */
    public function status($user_id, $id)
    {
         DB::beginTransaction();

        $updateStatus = UserSuit::where('user_id', $user_id)
            ->update(['status' => 0]);

        $dafaultStatus = UserSuit::where('id', $id)
            ->where('user_id', $user_id)
            ->update(['status' => 1]);

        if (!$updateStatus || !$dafaultStatus) {
            DB::rollback();
            return false;
        }

        DB::commit();
        return true;

    }

    /**
     * 编辑身材数据
     * @param  [type] $arr [description]
     * @param  [type] $id  [description]
     * @return [type]      [description]
     */
    public function update($arr, $id, $answer)
    {
         DB::beginTransaction();
         // 回答问题
        if (!empty($answer)) {
              UserAnswer::where('user_id', $arr['user_id'])
                  ->where('user_suit_id', $id)
                  ->delete();
              $answer = app('answer')->insert($answer);
        } else {
            $answer = true;
        }

        // 推荐尺寸
        if (!empty($arr['height'])) {
            $sizeId = SizeHelper::recommendSize($arr['height']);
            if ($sizeId) {
                  $arr['size_id'] = $sizeId;
            }
        }

        if ($arr['status'] == 1) {
            $updateStatus = UserSuit::where('user_id', $arr['user_id'])
                ->update(['status' => 0]);

            $dafaultStatus = UserSuit::where('id', $id)
                ->where('user_id', $arr['user_id'])
                ->update($arr);

            if (!$updateStatus || !$dafaultStatus || !$answer) {
                DB::rollback();
                return false;
            }

            DB::commit();
            return true;
        }

        $update = UserSuit::where('id', $id)
            ->update($arr);
        if (!$update || !$answer) {
            DB::rollback();
            return false;
        }
        DB::commit();
        return true;

    }

    /**
     * 获取身材数据
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function detail($id)
    {
        return UserSuit::where('id', $id)
            ->with('question.question')
            ->first();
    }

    /**
     * 删除身材数据
     * @param  [type] $id
     * @return integer
     */
    public function delete($id)
    {
       
         $suit = UserSuit::find($id);

        // 删除地址是默认地址
        if ($suit && $suit->status == 1) {
            $update = UserSuit::where('user_id', $suit->user_id)
                ->where('id', '<>', $id)
                ->orderBy('created_at', 'desc')
                ->first();

            // 删除默认地址并且该用户没有其他地址
            if (!$update) {
                return UserSuit::destroy($id);
            }
            DB::beginTransaction();
            $delete = UserSuit::destroy($id);
            $update->status = 1;
            $save = $update->save();
            if ($save && $save) {
                DB::commit();
                return true;
            }

            DB::rollback();
            return false;
        }
        return UserSuit::destroy($id);
        
    }

     /**
     * 获身材数据信息
     * @return collection
     */
    public function show($user_id)
    {
        return UserSuit::where('user_id', $user_id)
            ->orderBy('status', 'desc')
            ->orderBy('created_at', 'desc')
            ->with('question.question')
            ->get();
    }

    //用户推荐尺寸
    public function defaults($user_id, $id)
    {
        $data = ['recommend' => null,'inventory' => null];
        if (empty($user_id)) {
            return $data;
        }
        $size  = UserSuit::where('user_id', $user_id)
            ->where('status', '1')
            ->has('size')
            ->first();

        if (!$size) {
            return $data;
        }

        // 是否有尺寸属性
        $attribute = Inventory::where('product_sell_id', $id)
             ->with(['sku' => function ($q1) use ($size) {
                $q1->with(['skuValue' => function ($q2) use ($size) {
                    $q2->where(DB::raw("upper(value)"), $size->size->name)
                    ->with(['attribute' => function ($q3) {
                        $q3->where('id', 2);
                    }]);
                }]);
             }])
            ->first();
       
        // 没有推荐尺寸
        if (!$attribute) {
            return $data;
        }

        $inventory = $attribute->toArray();
        foreach ($inventory['sku'] as $key => $value) {
            if (empty($value['sku_value']['attribute'])) {
                unset($inventory['sku'][$key]);
            }
        }
        $inventory['sku'] = array_values($inventory['sku']);
        if (empty($inventory['sku'])) {
            return $data;
        }
        $data['recommend'] = $size->size->name;
        $data['inventory'] = $inventory;
        return $data;
    }

    // 上下装 肤色 发色 问题 数据
    public function size()
    {
        $userSize = SuitSize::get();
        $size['skin']       = [];
        $size['hair']       = [];
        foreach ($userSize as $key => $value) {
            if ($value['type'] == 3) {
                $size['skin'][] = $value;
            } else if ($value['type'] ==4) {
                $size['hair'][] = $value;
            }
        }
        $size['question'] = UserQuestion::with('option')
        ->get();
        return $size;
    }

    public function addlookingPoint($arr)
    {
        // 增加身材数据积分
        $pointAdd = array_filter($arr);
        foreach ($pointAdd as $key => $value) {
            app('pointReceived')->userAddPoint('looking', $key);
        }
    }
}
