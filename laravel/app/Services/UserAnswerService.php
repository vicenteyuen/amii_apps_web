<?php

namespace App\Services;

use App\Models\UserAnswer;

class UserAnswerService
{
    /**
     * 新增
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function insert($arr)
    {
        return UserAnswer::insert($arr);
    }

    /**
     * 删除
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        return UserAnswer::destroy($id);
    }
}
