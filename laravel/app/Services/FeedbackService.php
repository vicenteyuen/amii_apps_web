<?php

namespace App\Services;

use App\Models\Feedback;

class FeedbackService
{
    public function store($data)
    {
        app('pointReceived')->userAddPoint('advice', 'useradvice');//反馈加积分
        return Feedback::create($data);
    }

    // admin
    public function adminIndex()
    {
        return Feedback::with('user')
            ->orderBy('created_at', 'desc')
            ->paginate(config('amii.adminPaginate'));
    }

    /**
    *获取指定反馈内容
    *@param   $id  int
    *@return  type [description]
    */
    public function show($id)
    {
        $query = Feedback::with('user')
            ->where('id', $id)
            ->orderBy('created_at')
            ->first();

        return $query;
    }

    /**
    * 更新用户反馈状态
    *@param   $arr       [description]
    *@param   $nickname  [description]
    *@param   $id        int
    *@return  type       [description]
    */
    public function update($arr, $nickname, $id)
    {
        $exist = Feedback::with('user')
            ->when($nickname, function ($q) use ($nickname) {
                return $q->whereHas('user', function ($query) use ($nickname) {
                    $query->where('nickname', $nickname);
                });
            })
            ->when($id, function ($query) use ($id) {
                return $query->where('id', $id);
            })
            ->first();

        if ($exist && $id != $exist->id) {
            return ['status' => 0, 'msg' => '信息处理失败！'];
        }

        $update = Feedback::where('id', $id)
            ->update($arr);

        if (!$update) {
            return ['status' => 0, 'msg' => '信息处理失败！'];
        }
        // 赠送积分
        if ($arr['status'] == 2) {
            app('pointReceived')->userAddPoint('advice', 'advice', '', $exist->user_id);
        }
        return ['status' => 1];
    }
}
