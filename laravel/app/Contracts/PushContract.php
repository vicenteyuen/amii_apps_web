<?php

namespace App\Contracts;

interface PushContract
{
    public function push($userId, array $data);
    public function pushChannel($channel, array $data);
}
