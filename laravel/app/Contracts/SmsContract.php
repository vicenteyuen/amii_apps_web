<?php

namespace App\Contracts;

interface SmsContract
{
    public function requestSmsCode($phone);
    public function verifySmsCode($phone, $code);
}
