<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\OrderRebackInventoryHelper;
use App\Models\UserGame;
use App\Models\Order;

class OrderRebackInventory extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    // 任务类型
    private $provider;

    // 订单号
    private $orderSn;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($provider, $orderSn)
    {
        $this->provider = $provider;
        $this->orderSn = $orderSn;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orderId = Order::where('order_sn', $this->orderSn)
            ->value('id');
        $success = UserGame::where('order_id', $orderId)
            ->value('success');
        if ($success) {
            return false;
        }
        OrderRebackInventoryHelper::handle($this->provider, $this->orderSn);
    }
}
