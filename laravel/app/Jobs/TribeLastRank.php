<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\TribeService;

class TribeLastRank extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $rank;
    protected $gainObject;
    public function __construct($rank, $gainObject)
    {
        $this->rank = $rank;
        $this->gainObject = $gainObject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tribe = new TribeService;
        $tribe->lastRankQueue($this->rank, $this->gainObject);
    }
}
