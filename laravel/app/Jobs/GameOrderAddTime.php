<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\OrderRebackInventoryHelper;
use App\Models\UserGame;
use App\Models\Order;
use App\Models\SellProduct;

class GameOrderAddTime extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $userGameId;
    public function __construct($userGameId)
    {
        $this->userGameId = $userGameId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userGame = UserGame::find($this->userGameId);
        if ($userGame->order_status == 1) {
            return false;
        } else if ($userGame->order_status == 0) {
            $userGame->order_status = 2;
            $userGame->save();
            $order = Order::find($userGame->order_id);
            //退回库存
            OrderRebackInventoryHelper::handle('unconfirm', $order->order_sn);
            //推送
            $sellProduct = SellProduct::where('id', $userGame->sell_product_id)
                ->first();
            \App\Helpers\GamePushDocumentHelper::pushGameDocument($order->user_id, 6, $sellProduct->product->name, $sellProduct->product->snumber);
            $sellProduct->game_number--;
            $sellProduct->save();
        }
        return false;
    }
}
