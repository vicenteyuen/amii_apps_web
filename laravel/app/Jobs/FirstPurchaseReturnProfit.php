<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Order;
use App\Models\GiveProduct;
use App\Models\User;

class FirstPurchaseReturnProfit extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $orderId;
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::find($this->orderId);
        $order->shipping_status = 2;//改为签收状态

        //首单退30%到余额
        $giveProduct = GiveProduct::where('order_id', $order->id)
            ->where('return_profit', 0)
            ->first();
         $orderService = in_array($order->is_refund, [0,4]) || in_array($order->is_refund_product, [0,4]);
        $user = User::find($order->user_id);
        if ($giveProduct && $orderService) {
            $user->cumulate_profit = $user->cumulate_profit + $order->order_fee * 0.3;
            $giveProduct->return_profit = 1;
            $giveProduct->save();
            $user->save();
        }
        return $order->save();
    }
}
