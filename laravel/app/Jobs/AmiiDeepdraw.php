<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AmiiDeepdraw extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * 任务类型
     */
    private $jobProvider;

    /**
     * 数据
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($jobProvider, $data = [])
    {
        $this->jobProvider = $jobProvider;
        $this->data = is_array($data) ? $data : [];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        app('amii.amiiDeepdraw.job')->handle($this->jobProvider, $this->data);
    }
}
