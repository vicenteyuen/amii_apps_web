<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\TribeService;

class ContributeIncomeRank extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    protected $pid;
    protected $type;
    protected $level;
    public function __construct($pid, $type, $level)
    {
        $this->pid = $pid;
        $this->type = $type;
        $this->level = $level;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tribe = new TribeService;
        $tribe->contributeRankQueue($this->pid, $this->type, $this->level);
    }
}
