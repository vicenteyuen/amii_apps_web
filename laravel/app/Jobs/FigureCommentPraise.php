<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Helpers\FigureComment;

class FigureCommentPraise extends Job implements ShouldQueue
{
    use InteractsWithQueue;

    use  SerializesModels;

    protected $sellProductIds;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sellProductIds)
    {
        $this->sellProductIds = $sellProductIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        FigureComment::figurePraise($this->sellProductIds);
        
    }
}
