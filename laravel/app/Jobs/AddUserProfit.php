<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Order;
use App\Helpers\FirstOrderReturnDiscount;
use App\Helpers\OrderReceivedHelper;

class AddUserProfit extends Job implements ShouldQueue
{
    use InteractsWithQueue;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    protected $userId;
    protected $orderId;
    protected $income;
    protected $type;
    public function __construct($userId, $income, $orderId, $type = null)
    {
        $this->userId = $userId;
        $this->income = $income;
        $this->orderId = $orderId;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $order =  Order::find($this->orderId);
        if (!$order) {
            return false;
        }
        // 改成签到
        $order->shipping_status = 2;
        // 开启售后
        $order->can_refund = 1;
        $order->save();
        
        //首单返金额
        $delay =  8 * 24 * 60 * 60;
        $job = (new \App\Jobs\FirstPurchaseReturnMoneyJob(clone $order))->delay($delay);
        dispatch($job);


        // 确认收货时间,8天后关闭售后
        OrderReceivedHelper::received($order);
        if (is_null($this->userId)) {
            return false;
        }
        // 佣金部落
        $delay = config('amii.tribe.calcTribeMoneyTime');
        $job = (new \App\Jobs\ReturnProfit($this->userId, $this->income, $this->orderId, $this->type))->delay($delay);
        dispatch($job);
       
    }
}
