<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\PointsRank;

class LastPointRank extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $rank;
    protected $pointRankObject;
    public function __construct($rank, $pointRankObject)
    {
        $this->rank = $rank;
        $this->pointRankObject = $pointRankObject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $arr = [
            'user_id' => $this->pointRankObject->user_id,
            $this->rank => $this->pointRankObject->rank,
        ];
        $exits = PointsRank::where('user_id', $this->pointRankObject->user_id)
            ->first();
        if ($exits) {
            return PointsRank::where('user_id', $this->pointRankObject->user_id)
                ->update($arr);
        }
        return PointsRank::create($arr);
    }
}
