<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\UserGame;

class pushGameDocument extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $userGameId;
    protected $productName;
    protected $productSnumber;
    protected $provider;
    public function __construct($provider, $userGameId, $productName, $productSnumber)
    {
        $this->provider = $provider;
        $this->userGameId = $userGameId;
        $this->productName = $productName;
        $this->productSnumber = $productSnumber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->provider) {
            case '2':
                $userGame = UserGame::find($this->userGameId);
                if ($userGame->success == 1) {
                    return true;
                }
                $time = number_format($userGame->remain_time/60/60, 2, '.', '');
                \App\Helpers\GamePushDocumentHelper::pushGameDocument($userGame->user_id, 2, $this->productName, $this->productSnumber, '', $userGame->count - $userGame->finished_number, $time);
                break;

            case '3':
                $userGame = UserGame::find($this->userGameId);
                if ($userGame->order_status == 1) {
                    return true;
                }
                $time = number_format($userGame->order_remain_time/60/60, 2, '.', '');
                \App\Helpers\GamePushDocumentHelper::pushGameDocument($userGame->user_id, 3, $this->productName, $this->productSnumber, '', '', $time);
                break;

            default:
                # code...
                break;
        }
    }
}
