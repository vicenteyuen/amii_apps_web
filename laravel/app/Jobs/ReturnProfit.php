<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\ReturnProfitHelper;

class ReturnProfit extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

     /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $userId;
    protected $orderId;
    protected $income;
    protected $type;

    public function __construct($userId, $income, $orderId, $type = null)
    {
        $this->userId = $userId;
        $this->income = $income;
        $this->orderId = $orderId;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ReturnProfitHelper::returnProfitToUser($this->userId, $this->income, $this->orderId, $this->type);
    }
}
