<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\PointsReward;
use DB;

class LastMonthRewardJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $month;
    protected $year;
    protected $pointsObject;
    public function __construct($month, $year, $pointsObject)
    {
        $this->month = $month;
        $this->year = $year;
        $this->pointsObject = $pointsObject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->pointsObject)) {
            return false;
        }

        $rank = 1;
        $lastValue = count($this->pointsObject);
        $strSql = "insert into points_reward (user_id,month_income,year,month,month_rank) values ";
        foreach ($this->pointsObject as $key => $value) {
            if ($rank == $lastValue) {
                $strSql .= "({$value->user_id},{$value->sum_points},{$this->year},{$this->month},$rank)";
            } else {
                $strSql .= "({$value->user_id},{$value->sum_points},{$this->year},{$this->month},$rank),";
            }
            $rank++;
        }
        \Log::error(['str' => $strSql]);
        PointsReward::where('year', $this->year)
            ->where('month', $this->month)
            ->delete();
        DB::statement($strSql);
    }
}
