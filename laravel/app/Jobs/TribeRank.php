<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\TribeService;

class TribeRank extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $rank;
    protected $income;
    protected $GainObject;
    public function __construct($rank, $income, $GainObject)
    {
        $this->rank = $rank;
        $this->income = $income;
        $this->GainObject = $GainObject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tribe = new TribeService;
        $tribe->rankQueue($this->rank, $this->income, $this->GainObject);
    }
}
