<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\UserGame;
use App\Models\SellProduct;
use App\Models\Order;
use Carbon\Carbon;

class UserGameExpire extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $game = UserGame::find($this->id);
        if (!$game) {
            return false;
        }
        if ($game->success == 0) {
            if (! $game->gaming_end_time) {
                if ($game->created_at >= Carbon::parse('2017-06-20 12:00:00') && $game->created_at < Carbon::parse('2017-06-21 12:00:00')) {
                    $game->gaming_end_time = Carbon::parse($game->created_at)->addHours(60)->toDateTimeString();
                } else {
                    $game->gaming_end_time = Carbon::parse($game->created_at)->addHours(24)->toDateTimeString();
                }
                $game->save();
            }
            if ($game->gaming_end_time && Carbon::now() < Carbon::parse($game->gaming_end_time)) {
                $delay = Carbon::parse($game->gaming_end_time)->diffInSeconds(Carbon::now());
                dispatch((new UserGameExpire($game->id))->delay($delay));
                return;
            }

            $game->expired = 1;
            //未完成推送
            $sellProduct = SellProduct::where('id', $game->sell_product_id)
                ->first();
            \App\Helpers\GamePushDocumentHelper::pushGameDocument($game->user_id, 5, $sellProduct->product->name, $sellProduct->product->snumber);
            $sellProduct->game_number--; //正在玩的游戏人数减1
            $sellProduct->save();
            $order = Order::find($game->order_id);
            $order->order_status = 3;
            $order->save();
            return $game->save();
        }
    }
}
