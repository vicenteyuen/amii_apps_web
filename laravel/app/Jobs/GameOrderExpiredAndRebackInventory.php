<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\OrderRebackInventoryHelper;
use App\Models\UserGame;
use App\Models\Order;
use App\Models\SellProduct;

class GameOrderExpiredAndRebackInventory extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $userGameId;
    public function __construct($userGameId)
    {
        $this->userGameId = $userGameId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userGame = UserGame::find($this->userGameId);
        if ($userGame->order_status == 1) {
            return false;
        } else if ($userGame->order_status == 0) {
            $delay = 60 * 60 * 24;
            dispatch((new GameOrderAddTime($this->userGameId))->delay($delay));
        }
        return false;
    }
}
