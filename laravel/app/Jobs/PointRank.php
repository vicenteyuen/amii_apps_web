<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\PointsRank;
use DB;

class PointRank extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $rank;
    protected $income;
    protected $pointRankObject;
    public function __construct($rank, $income, $pointRankObject)
    {
        $this->rank = $rank;
        $this->income = $income;
        $this->pointRankObject = $pointRankObject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->income == 'month_income') {
            $income = 'year_income';
        } else {
            $income = 'month_income';
        }
        switch ($this->rank) {
            case 'month_rank':
                $rank1 = 'year_rank';
                $rank2 = 'last_year_rank';
                $rank3 = 'last_month_rank';
                break;

            case 'year_rank':
                $rank1 = 'month_rank';
                $rank2 = 'last_year_rank';
                $rank3 = 'last_month_rank';
                break;

            case 'last_year_rank':
                $rank1 = 'month_rank';
                $rank2 = 'year_rank';
                $rank3 = 'last_month_rank';
                break;

            case 'last_month_rank':
                $rank1 = 'month_rank';
                $rank2 = 'year_rank';
                $rank3 = 'last_year_rank';
                break;
            
            default:
                return ;
        }

        $lastValue = end($this->pointRankObject);
        $updateStr = '';
        if (empty($this->pointRankObject)) {
            return false;
        }
        foreach ($this->pointRankObject as $key => $value) {
            if ($lastValue == $value) {
                $updateStr .= "({$value->user_id},{$value->total_points},{$value->rank},0,0,0,0)";
            } else {
                $updateStr .= "({$value->user_id},{$value->total_points},{$value->rank},0,0,0,0),";
            }
        }
        $strSql = "insert into points_rank(user_id,{$this->income},{$this->rank},$income,$rank1,$rank2,$rank3)  values ".$updateStr."on duplicate key update {$this->income}=values({$this->income}),{$this->rank}=values({$this->rank})";

        PointsRank::where($this->rank, '!=', 0)
            ->orWhere($this->income, '!=', 0)
            ->update([$this->rank => 0,$this->income => 0]);
        DB::statement($strSql);
    }
}
