<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderRefund extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $orderSn;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orderSn)
    {
        $this->orderSn = $orderSn;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // 7天关闭售后
        app('order')->closeRefund($this->orderSn);
    }
}
