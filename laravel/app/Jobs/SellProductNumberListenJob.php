<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\SellProduct;

class SellProductNumberListenJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    // job provider
    protected $provider;

    // job data
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($provider, $data)
    {
        $this->provider = $provider;
        $this->data = is_array($data) ? $data : array($data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->provider) {
            case 'created':
                self::created();
                break;
            case 'updated':
                self::updated();
                break;

            default:
                # code...
                break;
        }
    }

    /**
     * created
     */
    private function created()
    {
        $data = $this->data;
        $sellProductId = $data['sellProductId'];
        $time = $data['time'];

        SellProduct::where('id', $sellProductId)
            ->where('number', 0)
            ->update(['number_zero_time' => $time]);
    }

    /**
     * updated
     */
    private function updated()
    {
        $data = $this->data;
        $sellProductId = $data['sellProductId'];
        $time = $data['time'];
        $type = $data['type'];

        switch ($type) {
            case 'set':
                SellProduct::where('id', $sellProductId)
                    ->where('number', 0)
                    ->update(['number_zero_time' => $time]);
                break;
            case 'del':
                SellProduct::where('id', $sellProductId)
                    ->where('number', '!=', 0)
                    ->update(['number_zero_time' => null]);
                break;

            default:
                # code...
                break;
        }
    }
}
