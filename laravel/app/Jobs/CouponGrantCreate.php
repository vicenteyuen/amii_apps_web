<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\CouponGrant;
use App\Helpers\AesHelper;

class CouponGrantCreate extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    protected $couponId;
    protected $num;
    protected $status;
    public function __construct($couponId, $num, $status)
    {
        $this->couponId = $couponId;
        $this->num = $num;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        for ($i=1; $i <= $this->num; $i++) {
            CouponGrant::create([
                'coupon_id' => $this->couponId,
                'status'    => $this->status,
                'code'      => AesHelper::createKey(24)
            ]);
        }
    }
}
