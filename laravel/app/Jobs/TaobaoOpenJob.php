<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Helpers\TaobaoTopJobHelper;

class TaobaoOpenJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $func;
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($func, $data)
    {
        $this->func = $func;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        TaobaoTopJobHelper::handle($this->func, $this->data);
    }
}
