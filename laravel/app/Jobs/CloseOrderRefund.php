<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Order;

class CloseOrderRefund extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $orderId;
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::where('id', $this->orderId)
            ->first();
        // 购物送积分
        if (!$order) {
            return false;
        }
         //新增积分
        if ($order->provider == 'standard') {
            app('pointReceived')->userAddPoint('shopping', 'shopping', $order->order_fee, $order->user_id, $order->order_sn);
        }
        //关闭售后
        $order->can_refund = 0;
        $order->save();
    }
}
