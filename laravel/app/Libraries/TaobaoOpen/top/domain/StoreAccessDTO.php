<?php

/**
 * 入参
 * @author auto create
 */
class StoreAccessDTO
{
	
	/** 
	 * 仓库地址
	 **/
	public $address_name;
	
	/** 
	 * 定位仓库区县
	 **/
	public $area_name;
	
	/** 
	 * 定位城市
	 **/
	public $cicty_name;
	
	/** 
	 * 货主id
	 **/
	public $owner_user_id;
	
	/** 
	 * 定位省份
	 **/
	public $prov_name;
	
	/** 
	 * 仓库编码
	 **/
	public $store_code;
	
	/** 
	 * 列表
	 **/
	public $store_cover_list;
	
	/** 
	 * 仓库名
	 **/
	public $store_name;
	
	/** 
	 * 仓库镇
	 **/
	public $town_name;	
}
?>