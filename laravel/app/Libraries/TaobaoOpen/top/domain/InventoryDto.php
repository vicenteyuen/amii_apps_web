<?php

/**
 * 库存模型DTO
 * @author auto create
 */
class InventoryDto
{
	
	/** 
	 * 库存列表
	 **/
	public $item_inventorylist;
	
	/** 
	 * 货主id
	 **/
	public $owner_user_id;
	
	/** 
	 * 当前页面数
	 **/
	public $page_no;
	
	/** 
	 * 库存商品量
	 **/
	public $total_count;	
}
?>