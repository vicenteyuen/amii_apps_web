<?php

/**
 * 发货人信息
 * @author auto create
 */
class SenderInfo
{
	
	/** 
	 * 区域
	 **/
	public $area;
	
	/** 
	 * 城市
	 **/
	public $city;
	
	/** 
	 * 公司名称
	 **/
	public $company;
	
	/** 
	 * 国家二字码
	 **/
	public $contry_code;
	
	/** 
	 * 详细地址
	 **/
	public $detail_address;
	
	/** 
	 * 电子邮箱
	 **/
	public $email;
	
	/** 
	 * 证件号
	 **/
	public $id;
	
	/** 
	 * 移动电话
	 **/
	public $mobile;
	
	/** 
	 * 姓名
	 **/
	public $name;
	
	/** 
	 * 省份
	 **/
	public $province;
	
	/** 
	 * 固定电话
	 **/
	public $tel;
	
	/** 
	 * 村镇
	 **/
	public $town;
	
	/** 
	 * 邮编
	 **/
	public $zip_code;	
}
?>