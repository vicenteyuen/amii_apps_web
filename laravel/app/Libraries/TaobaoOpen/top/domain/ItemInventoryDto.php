<?php

/**
 * 库存列表
 * @author auto create
 */
class ItemInventoryDto
{
	
	/** 
	 * 生产批号
	 **/
	public $batch_code;
	
	/** 
	 * 渠道编码，type=3时字段有返回值。（TB 淘系，OTHERS 其他）
	 **/
	public $channel_code;
	
	/** 
	 * 菜鸟系统录入商家仓库CODE
	 **/
	public $cn_store_code;
	
	/** 
	 * 商品过期日期YYYY-MM-DD
	 **/
	public $expire_date;
	
	/** 
	 * 扩展属性
	 **/
	public $extend_props;
	
	/** 
	 * 1 销售库存 101 残次102 机损 103箱损201冻结 301调拨在途 401 采购在途
	 **/
	public $inventory_type;
	
	/** 
	 * ERP系统商品编码
	 **/
	public $item_code;
	
	/** 
	 * 淘系后端货品Id
	 **/
	public $item_id;
	
	/** 
	 * 商品名称
	 **/
	public $item_name;
	
	/** 
	 * 冻结库存数量
	 **/
	public $lock_quantity;
	
	/** 
	 * 生产批号
	 **/
	public $produce_code;
	
	/** 
	 * 商品生产日期 YYYY-MM-DD
	 **/
	public $product_date;
	
	/** 
	 * 前端店铺商品ID，多商品以逗号分隔
	 **/
	public $product_id;
	
	/** 
	 * 未冻结库存数量
	 **/
	public $quantity;
	
	/** 
	 * 仓库编码
	 **/
	public $store_code;	
}
?>