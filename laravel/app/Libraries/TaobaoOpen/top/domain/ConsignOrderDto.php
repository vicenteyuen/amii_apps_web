<?php

/**
 * 发货单模型DTO
 * @author auto create
 */
class ConsignOrderDto
{
	
	/** 
	 * 订单应收金额，消费者还需要付的金额，单位分
	 **/
	public $ar_amount;
	
	/** 
	 * 菜鸟仓库CODE
	 **/
	public $cn_store_code;
	
	/** 
	 * 配送要求
	 **/
	public $deliver_requirements;
	
	/** 
	 * 订单优惠金额，整单优惠金额，单位分
	 **/
	public $discount_amount;
	
	/** 
	 * 拓展属性
	 **/
	public $extend_fields;
	
	/** 
	 * 订单已付金额，消费者已经支付的金额，单位分
	 **/
	public $got_amount;
	
	/** 
	 * 发票信息列表
	 **/
	public $invoice_info_list;
	
	/** 
	 * 订单总金额,=总商品金额-订单优惠金额+快递费用，单位分
	 **/
	public $order_amount;
	
	/** 
	 * ERP订单号
	 **/
	public $order_code;
	
	/** 
	 * 订单创建时间,ERP创建订单时间戳
	 **/
	public $order_create_time;
	
	/** 
	 * 订单审核时间,ERP创建支付时间戳
	 **/
	public $order_examination_time;
	
	/** 
	 * 订单标识 (1: cod –货到付款，4:invoiceinfo-需要发票)
	 **/
	public $order_flag;
	
	/** 
	 * 订单商品信息列表
	 **/
	public $order_item_list;
	
	/** 
	 * 订单支付时间戳
	 **/
	public $order_pay_time;
	
	/** 
	 * 下单时间，订单在交易平台创建时间戳
	 **/
	public $order_shop_create_time;
	
	/** 
	 * 订单来源（213 天猫，201 淘宝，214 京东，202 1688 阿里中文站 ，203 苏宁在线，204 亚马逊中国，205 当当，208 1号店，207 唯品会，209 国美在线，210 拍拍，206 易贝ebay，211 聚美优品，212 乐蜂网，215 邮乐，216 凡客，217 优购，218 银泰，219 易讯，221 聚尚网，222 蘑菇街，223 POS门店，301 其他）
	 **/
	public $order_source;
	
	/** 
	 * 单据类型 201 一般交易出库单 202 B2B交易出库单 502 换货出库单 503 补发出库单
	 **/
	public $order_type;
	
	/** 
	 * 货主ID
	 **/
	public $owner_user_id;
	
	/** 
	 * 快递费用，单位分
	 **/
	public $postfee;
	
	/** 
	 * 前物流订单号，订单类型为502 换货出库单 503 补发出库单时，需求传入此内容
	 **/
	public $prev_order_code;
	
	/** 
	 * 收件人信息
	 **/
	public $receiver_info;
	
	/** 
	 * 备注
	 **/
	public $remark;
	
	/** 
	 * 发货人信息
	 **/
	public $sender_info;
	
	/** 
	 * COD服务费，单位分
	 **/
	public $service_fee;
	
	/** 
	 * 仓库编码
	 **/
	public $store_code;
	
	/** 
	 * 快递公司编码
	 **/
	public $tms_service_code;
	
	/** 
	 * 快递公司名称
	 **/
	public $tms_service_name;	
}
?>