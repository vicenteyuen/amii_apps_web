<?php

/**
 * 列表
 * @author auto create
 */
class StoreAccessCoverDTO
{
	
	/** 
	 * 覆盖城市
	 **/
	public $city_name;
	
	/** 
	 * 是否优先覆盖
	 **/
	public $is_priority;
	
	/** 
	 * 覆盖省份
	 **/
	public $prov_name;
	
	/** 
	 * 覆盖类型, 0按省份覆盖，1按城市覆盖
	 **/
	public $type;	
}
?>