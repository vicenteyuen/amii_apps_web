<?php

/**
 * result
 * @author auto create
 */
class BaseAccessResult
{
	
	/** 
	 * 错误码
	 **/
	public $code;
	
	/** 
	 * 成功标记
	 **/
	public $flag;
	
	/** 
	 * 返回信息
	 **/
	public $message;	
}
?>