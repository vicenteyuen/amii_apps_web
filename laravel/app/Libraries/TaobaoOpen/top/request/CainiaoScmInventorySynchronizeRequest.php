<?php
/**
 * TOP API: cainiao.scm.inventory.synchronize request
 * 
 * @author auto create
 * @since 1.0, 2017.02.13
 */
class CainiaoScmInventorySynchronizeRequest
{
	/** 
	 * 库存模型DTO
	 **/
	private $param0;
	
	private $apiParas = array();
	
	public function setParam0($param0)
	{
		$this->param0 = $param0;
		$this->apiParas["param0"] = $param0;
	}

	public function getParam0()
	{
		return $this->param0;
	}

	public function getApiMethodName()
	{
		return "cainiao.scm.inventory.synchronize";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
