<?php
/**
 * TOP API: cainiao.scm.consign.order.synchronize request
 * 
 * @author auto create
 * @since 1.0, 2017.02.15
 */
class CainiaoScmConsignOrderSynchronizeRequest
{
	/** 
	 * 发货单模型DTO
	 **/
	private $param0;
	
	private $apiParas = array();
	
	public function setParam0($param0)
	{
		$this->param0 = $param0;
		$this->apiParas["param0"] = $param0;
	}

	public function getParam0()
	{
		return $this->param0;
	}

	public function getApiMethodName()
	{
		return "cainiao.scm.consign.order.synchronize";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
