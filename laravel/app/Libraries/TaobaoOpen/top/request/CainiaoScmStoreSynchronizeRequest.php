<?php
/**
 * TOP API: cainiao.scm.store.synchronize request
 * 
 * @author auto create
 * @since 1.0, 2017.02.14
 */
class CainiaoScmStoreSynchronizeRequest
{
	/** 
	 * 入参
	 **/
	private $storeAccessDto;
	
	private $apiParas = array();
	
	public function setStoreAccessDto($storeAccessDto)
	{
		$this->storeAccessDto = $storeAccessDto;
		$this->apiParas["store_access_dto"] = $storeAccessDto;
	}

	public function getStoreAccessDto()
	{
		return $this->storeAccessDto;
	}

	public function getApiMethodName()
	{
		return "cainiao.scm.store.synchronize";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
