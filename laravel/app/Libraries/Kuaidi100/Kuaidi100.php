<?php

namespace App\Libraries\Kuaidi100;

use GuzzleHttp\Client;

class Kuaidi100
{
    // 网络请求状态
    const NoResultStatus = 0;
    const SuccessStatus = 1;
    const ErrorStatus = 2;
    const CustomerSigned = 3;

    // client
    private $client;

    // debug
    private $debug;

    // is free
    private $isFree;

    public function __construct()
    {
        $this->client = new Client();
        $this->debug = config('amii.kuaidi100.debug');
        $this->isFree = config('amii.kuaidi100.is_free');
    }

    /**
     * 查询快递
     * @param $com string 快递公司代码
     * @param $nu string 快递公司对应单号
     * @return array
     */
    public function express($com, $nu)
    {
        if ($this->debug) {
            $data = [
                'message' => 'ok',
                'nu' => '882180286425539531',
                'ischeck' => '1',
                'condition' => 'F00',
                'com' => 'yuantong',
                'status' => '200',
                'state' => '3',
                'data' => [
                    0 => [
                        'time' => '2016-07-01 12:32:03',
                        'ftime' => '2016-07-01 12:32:03',
                        'context' => '客户 签收人: 前台 已签收  感谢使用圆通速递，期待再次为您服务',
                    ],
                    1 => [
                        'time' => '2016-07-01 06:43:52',
                        'ftime' => '2016-07-01 06:43:52',
                        'context' => '广东省广州市天河区猎德公司(点击查询电话)阮** 派件中 派件员电话13710341324',
                    ],
                    2 => [
                        'time' => '2016-07-01 06:13:51',
                        'ftime' => '2016-07-01 06:13:51',
                        'context' => '广东省广州市天河区猎德公司 已收入',
                    ],
                    3 => [
                        'time' => '2016-07-01 00:48:07',
                        'ftime' => '2016-07-01 00:48:07',
                        'context' => '广州转运中心 已发出,下一站 广东省广州市天河区猎德',
                    ],
                    4 => [
                        'time' => '2016-07-01 00:03:53',
                        'ftime' => '2016-07-01 00:03:53',
                        'context' => '广州转运中心 已收入',
                    ],
                    5 => [
                        'time' => '2016-06-30 21:44:38',
                        'ftime' => '2016-06-30 21:44:38',
                        'context' => '广东省广州市白云区石井公司 已发出,下一站 广州转运中心',
                    ],
                    6 => [
                        'time' => '2016-06-30 21:39:55',
                        'ftime' => '2016-06-30 21:39:55',
                        'context' => '广东省广州市白云区石井公司(点击查询电话) 已揽收',
                    ],
                    7 => [
                        'time' => '2016-06-30 21:39:55',
                        'ftime' => '2016-06-30 21:39:55',
                        'context' => '广东省广州市白云区石井公司 已打包',
                    ],
                    8 => [
                        'time' => '2016-06-30 18:57:03',
                        'ftime' => '2016-06-30 18:57:03',
                        'context' => '广东省广州市白云区石井公司 取件人: 肖雄锦 已收件',
                    ],
                ],
            ];

            return $data;
        }
        if ($this->isFree) {
            $data = self::free($com, $nu);
        } else {
            $data = self::enterprise($com, $nu);
        }
        return $data;
    }

    /**
     * 免费版
     * @param $com
     * @param $nu
     * @return array
     */
    public function free($com, $nu)
    {
        $response = $this->client
            ->get('http://api.kuaidi100.com/api', [
                'query' => [
                    'id' => config('amii.kuaidi100.free.key'),
                    'com' => $com,
                    'nu' => $nu,
                    // 'valicode' => '',
                    'show' => '0',
                    'muti' => '1',
                    'order' => 'desc'
                ]
            ]);

        $responseData = json_decode($response->getBody()->getContents(), true);
        if ($responseData['status'] != static::SuccessStatus) {
            \Log::error('快递100: ' . $responseData['message'], (array)$responseData);
            return [];
        }
        return $responseData;
    }

    /**
     * 企业版
     * @param $com
     * @param $nu
     * @return array
     */
    public function enterprise($com, $nu)
    {
        $param = json_encode([
            'com' => $com,
            'num' => $nu,
        ]);
        $sign = strtoupper(md5($param . config('amii.kuaidi100.enterprise.key') . config('amii.kuaidi100.enterprise.customer')));

        $response = $this->client
            ->post('http://poll.kuaidi100.com/poll/query.do', [
                'form_params' => [
                    'customer' => config('amii.kuaidi100.enterprise.customer'),
                    'param' => $param,
                    'sign' => $sign,
                ],
            ]);

        $responseData = json_decode($response->getBody()->getContents(), true);
        if (! empty($responseData->returnCode)) {
            \Log::error('快递100: '. $responseData['message'], (array)$responseData);
            return [];
        }
        return $responseData;
    }

    /**
     * 订阅快递
     */
    public function poll($number)
    {
        $param = [
            'number' => $number,
            'key' => config('amii.kuaidi100.poll.key'),
            'parameters' => [
                'callbackurl' => action('Kuaidi100\PollController@callback'),
                'autoCom' => 1
            ]
        ];

        $response = $this->client
            ->post('http://poll.kuaidi100.com/poll', [
                'form_params' => [
                    'schema' => 'json',
                    'param' => json_encode($param)
                ],
            ]);

        $responseData = json_decode($response->getBody()->getContents(), true);
        if ($responseData['result']) {
            return true;
        }
        \Log::error('快递100: 快递单号(' . $number . '),' . ' 订阅失败(' . $responseData['message'] . ')');
        return false;
    }
}
