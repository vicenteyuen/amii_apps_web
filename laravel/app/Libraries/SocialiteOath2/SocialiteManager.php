<?php

/*
 * socialite plus
 */

namespace App\Libraries\SocialiteOath2;

use Overtrue\Socialite\SocialiteManager as BaseSocialiteManager;

/**
 * Class SocialiteManager.
 */
class SocialiteManager extends BaseSocialiteManager
{
    /**
     * socialite plus config
     */
    protected $driversPlus;

    /**
     * SocialiteManager constructor.
     *
     * @param array                                          $config
     * @param \Symfony\Component\HttpFoundation\Request|null $request
     */
    public function __construct(array $config = null, Request $request = null)
    {
        $config = $config ? : config('oath2');
        $this->driversPlus = config('oath2socialiteplus');
        parent::__construct($config, $request);
    }

    /**
     * Create a new driver instance.
     *
     * @param string $driver
     *
     * @throws \InvalidArgumentException
     *
     * @return mixed
     */
    protected function createDriver($driver)
    {
        if (isset($this->initialDrivers[$driver])) {
            $provider = $this->initialDrivers[$driver];
            $provider = 'Overtrue\\Socialite\\Providers\\' . $provider.'Provider';

            return $this->buildProvider($provider, $this->formatConfig($this->config->get($driver)));
        } else if (isset($this->driversPlus[$driver])) {
            // socialite plus provider
            $provider = $this->driversPlus[$driver];
            $provider = __NAMESPACE__.'\\Providers\\'.$provider.'Provider';

            return $this->buildProvider($provider, $this->formatConfig($this->config->get($driver)));
        }

        if (isset($this->customCreators[$driver])) {
            return $this->callCustomCreator($driver);
        }

        throw new InvalidArgumentException("Driver [$driver] not supported.");
    }
}
