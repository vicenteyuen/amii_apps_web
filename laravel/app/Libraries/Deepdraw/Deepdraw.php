<?php

namespace App\Libraries\Deepdraw;

use App\Libraries\Deepdraw\Client\Client;

/**
* Deepdraw
*/
class Deepdraw
{
    // testing server url
    // const SERVER_URL = 'http://open.soomey.net';

    // production server url
    const SERVER_URL = 'http://open.deepdraw.cn';

    // app key
    private $appKey;

    // app secret
    private $appSecret;

    // client
    private $client;

    public function __construct()
    {
        $this->appKey = config('amii.deepdraw.app_key');
        $this->appSecret = config('amii.deepdraw.app_secret');
        $this->client = new Client(Deepdraw::SERVER_URL, $this->appKey, $this->appSecret);
    }

    // request
    public function request($uri, $querys = null, $headers = null)
    {
        return $this->client->get($uri, $querys, $headers);
    }

    // log
    public function log($provider, $id, $info = '', $type = 'error')
    {
        // add deepdraw log
        \Log::useFiles(storage_path().'/logs/deepdraw_'.date('Y-m-d').'_info.log','info');
        \Log::useFiles(storage_path().'/logs/deepdraw_'.date('Y-m-d').'_error.log','error');


        if ($type=='error') {
            \Log::error('--------Deepdraw--------');
            if ($info) {
                 \Log::error('错误信息: ' . $info);
            }
            \Log::error('获取' . $provider . ': 失败(' . $id . ')');
            \Log::error('------------------------');

        }else{
            \Log::info('--------Deepdraw--------');
            if ($info) {
                 \Log::info('信息: ' . $info);
            }
            \Log::info('获取' . $provider . ': 成功(' . $id . ')');
            \Log::info('------------------------');

        }



    }
}
