<?php

namespace App\Libraries\Aes;

class BizDataCrypt
{
    private $public;
    private $key;

    public function __construct($public, $key)
    {
        $this->public = $public;
        $this->key = $key;
    }

    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData($encryptedData, $iv, &$data)
    {
        if (strlen($this->key) != 24) {
            return ErrorCode::$IllegalAesKey;
        }
        $aesKey = base64_decode($this->key);

        if (strlen($iv) != 24) {
            return ErrorCode::$IllegalIv;
        }
        $aesIV = base64_decode($iv);

        $aesCipher = base64_decode($encryptedData);

        $pc = new Prpcrypt($aesKey);
        $result = $pc->decrypt($aesCipher,$aesIV);

        if ($result[0] != 0) {
            return $result[0];
        }

        $dataObj = json_decode( $result[1] );
        if( $dataObj  == NULL ) {
            return ErrorCode::$IllegalBuffer;
        }
        if( $dataObj->amiimark->public != $this->public ) {
            return ErrorCode::$IllegalBuffer;
        }
        $data = $result[1];
        return ErrorCode::$OK;
    }

    /**
     * 加密数据
     */
    public function encryptData($data, $iv, &$encryptedData)
    {
        if (strlen($this->key) != 16) {
            return ErrorCode::$IllegalAesKey;
        }

        if (strlen($iv) != 16) {
            return ErrorCode::$IllegalIv;
        }

        $pc = new Prpcrypt($this->key);
        $result = $pc->encrypt($data, $iv);

        if ($result[0] != 0) {
            return $result[0];
        }

        $encryptedData = base64_encode($result[1]);
        return ErrorCode::$OK;
    }
}
