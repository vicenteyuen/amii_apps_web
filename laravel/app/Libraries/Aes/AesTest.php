<?php

namespace App\Libraries\Aes;

use App\Libraries\Aes\Prpcrypt;

class AesTest
{
    public static function test()
    {
        $public = '12322312';
        $key = 'tiihtNczf5v6AKRyjwEUhQ==';
        $encryptedData = "3GyC8rqcee3Q5ibMhkO8GUw/0S3LiMdfTy7cSx/vQcJKv/7is0L/PZEIhHAMo8fQ417n5ZjvXEha51jDEHKfCmXUicUKKM4OMO3ihzzinxk=";

        $iv = 'r7BXXKkLb8qrSNn05n0qiA==';

        $pc = new BizDataCrypt($public, $key);
        $errCode = $pc->decryptData($encryptedData, $iv, $data );

        if ($errCode == 0) {
            dd(json_decode($data, true));
            print($data . "\n");
        } else {
            dd($errCode);
            print($errCode . "\n");
        }
    }

    public static function encrypt()
    {
        $arr = [
            'key1' => 'data1',
            'key2' => 'data2',
            // amii水印
            'amiimark' => [
                "timestamp" => 1477314187,
                "public" => "12322312"
            ]
        ];
        $public = '12322312';
        $key = 'tiihtNczf5v6AKRyjwEUhQ==';
        $iv = 'r7BXXKkLb8qrSNn05n0qiA==';

        $pc = new BizDataCrypt($public, $key);
        $errCode = $pc->encryptData(json_encode($arr), $iv, $data);

        dd($errCode);
    }
}
