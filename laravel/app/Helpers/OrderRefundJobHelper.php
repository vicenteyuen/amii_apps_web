<?php

namespace App\Helpers;

use App\Jobs\OrderRefund;

class OrderRefundJobHelper
{
    /**
     * 7天关闭售后功能
     */
    public static function closeRefund($orderSn)
    {
        $delay = 7 * 24 * 60 * 60;

        $job = (new OrderRefund($orderSn))->delay($delay);
        dispatch($job);
    }
}
