<?php

namespace App\Helpers;

class HtmlHelper
{
    public static function statusAdmin($status)
    {
        if ($status) {
            return '<i class="fa fa-check text-green"></i>';
        }
        return '<i class="fa fa-close text-red"></i>';
    }

    /**
     * 轮播类型
     */
    public static function bannerProviderStr($provider)
    {
        switch ($provider) {
            case 'index':
                $str = '商品列表';
                break;
            case 'product':
                $str = '商品详情';
                break;
            case 'html':
                $str = '网页链接';
                break;

            default:
                $str = '未定义';
                break;
        }

        return $str;
    }
}
