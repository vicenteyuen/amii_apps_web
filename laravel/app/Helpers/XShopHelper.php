<?php

namespace App\Helpers;

class XShopHelper
{
    /**
     * This class should not be instantiated.
     */
    private function __construct()
    {
    }

    public static function orderProcessHtml($order)
    {
        $html = '';

        if ($order['pay_status'] == 0) {
            $html .= '<span class="text-danger">[未付款]</span>';
        } elseif ($order['pay_status'] == 1) {
            $html .= '<span class="text-warning">[付款中]</span>';
        } elseif ($order['pay_status'] == 2) {
            $html .= '<span class="text-success">[已付款]</span>';
        } else {
            $html .= '订单出现系统错误，请联系管理员';
        }

        $html .= ' ';

        if ($order['order_status'] == 0) {
            $html .= '<span class="text-primary">[用户未确认收货]</span>';
        } elseif ($order['order_status'] == 1) {
            $html .= '<span class="text-success">[用户已确认收货]</span>';
        } elseif ($order['order_status'] == 2) {
            $html .= '<span class="text-info">[已取消]</span>';
        } elseif ($order['order_status'] == 3) {
            $html .= '<span class="text-warning">[无效]</span>';
        } elseif ($order['order_status'] == 4) {
            $html .= '<span class="text-danger">[退货]</span>';
        } else {
            $html .= '<span class="text-danger">[订单出现系统错误，请联系管理员]</span>';
        }

        $html .= ' ';

        if ($order['shipping_status'] == 0) {
            $html .= '<span class="text-info">[商家未发货]</span>';
        } elseif ($order['shipping_status'] == 1) {
            $html .= '<span class="text-success">[商家已发货]</span>';
        } elseif ($order['shipping_status'] == 2) {
            $html .= '<span class="text-success">[商家确认用户已收货]</span>';
        } else {
            $html .= '<span class="text-danger">[订单出现系统错误，请联系管理员]</span>';
        }

        return $html;
    }

    // check relation
    public static function checkObjectExist($objects, $object)
    {
        foreach ($objects as $value) {
            if ($object->id == $value->id) {
                return true;
            }
        }
        // Otherwise
        return false;
    }

    public static function checkCategoryExist($objects, $category)
    {
        foreach ($objects as $key => $value) {
            if ($value->category_id == $category->id) {
                return true;
            }
        }
        // Otherwise
        return false;
    }
}
