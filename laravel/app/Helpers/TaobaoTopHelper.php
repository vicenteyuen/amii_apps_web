<?php

namespace App\Helpers;

require(app_path() . '/Libraries/TaobaoOpen/TopSdk.php');

class TaobaoTopHelper
{
    /**
     * 创建
     */
    public static function newClass($class = '')
    {
        $newClass = new $class;
        if ($class === 'TopClient') {
            // client基础配置
            $newClass->appkey = config('amii.taobaoopen.app_key');
            $newClass->secretKey = config('amii.taobaoopen.app_secret');
            $newClass->format = 'json';
        }
        return $newClass;
    }

    /**
     * 设置会话密钥
     */
    public static function setSessionKey($merchant = 'amii')
    {
        switch ($merchant) {
            case 'amii':
                $sessionKey = config('amii.taobaoopen.session_key.amii');
                break;
            case 'amiiR':
                $sessionKey = config('amii.taobaoopen.session_key.amii_r');
                break;
            case 'amiiM':
                $sessionKey = config('amii.taobaoopen.session_key.amii_m');
                break;
            case 'amiiC':
                $sessionKey = config('amii.taobaoopen.session_key.amii_c');
                break;

            default:
                $sessionKey = '';
                break;
        }

        return $sessionKey;
    }

    /**
     * 分页配置
     */
    public static function setPageConf($req, $pageNo = 1)
    {
        $req->setPageSize(config('amii.taobaoopen.page_size'));
        if ($pageNo > 1 && $pageNo <= 100) {
            $req->setPageNo($pageNo);
        }
        return $req;
    }

    /**
     * 格式化请求返回数据
     */
    public static function formatRes($res)
    {
        if (isset($res->code) && $res->code) {
            return array(80001, $res->msg);
        }
        return array(0, json_decode(json_encode($res), true));
    }
}
