<?php

namespace App\Helpers;

class UtilesHelper
{
    /*
     * check is in wechat
     */
    public static function isWeChat()
    {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            return true;
        }

        return false;
    }
}
