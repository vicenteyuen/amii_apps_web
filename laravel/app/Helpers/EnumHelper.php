<?php

namespace App\Helpers;

class EnumHelper
{
    public static function getGender($gender)
    {
        $genderString = '';
        switch ($gender) {
            case 1:
                $genderString = '男';
                break;
            case 0:
                $genderString = '女';
                break;
            default:
                $genderString = '其他';
                break;
        }

        return $genderString;
    }

    public static function getShippingStatus()
    {
        $status = ['未发货' => 0, '已发货' => 1];

        return $status;
    }

    /*
     *  获取订单状态string
     *  $status     order stauts string
     */
    public static function getOrderStatus($status)
    {
        $statusString = '';
        switch ($status) {
            case 'paying':
                $statusString = '未付款';
                break;
            case 'cancel':
                $statusString = '交易关闭';
                break;
            case 'sending':
                $statusString = '待发货';
                break;
            case 'receiving':
                $statusString = '待收货';
                break;
            case 'appraising':
                $statusString = '待评价';
                break;
            case 'success':
                $statusString = '交易完成';
                break;

            default:
                # code...
                break;
        }
        return $statusString;
    }

    /*
     *  获取订单列表操作button
     *  $status     order stauts string
     *  $payType    order pay type
     */
    public static function getOrderControl($status, $orderSn, $payType)
    {
        $controlString = '<div class="order-control">';
        switch ($status) {
            case 'paying':
                $controlString .= self::orderBtn($orderSn, 'cancel', '取消');
                $controlString .= self::orderBtnPay($orderSn, $payType);
                // $controlString .= self::orderBtn($orderSn, 'express', '查看物流');
                // $controlString .= self::orderBtn($orderSn, 'confirm', '确认收货');
                // $controlString .= self::orderBtn($orderSn, 'apprise', '评价');
                // $controlString .= self::orderBtn($orderSn, 'delete', '删除');
                break;
            case 'cancel':
                $controlString .= self::orderBtn($orderSn, 'delete', '删除');
                break;
            case 'sending':
                goto Undefine;
                break;
            case 'receiving':
                $controlString .= self::orderBtn($orderSn, 'express', '查看物流');
                $controlString .= self::orderBtn($orderSn, 'confirm', '确认收货');
                break;
            case 'appraising':
                $controlString .= self::orderBtn($orderSn, 'apprise', '评价');
                break;
            case 'success':
                $controlString .= self::orderBtn($orderSn, 'delete', '删除');
                break;

            default:
                goto Undefine;
                break;
        }
        return $controlString . '</div>';

        Undefine:
            $controlString = '';
            return $controlString;
    }

    /*
     *  订单操作支付拼接
     */
    public static function orderBtnPay($orderSn, $payType)
    {
        $btn = '';
        $dataControl = '';
        if (\Utils::isWeixin() xor $payType == 'alipay') {
            $btn = '支付';
            $dataControl = 'can-pay';
        } else {
            $btn = '当前打开方式不支持支付';
        }

        return sprintf(
            '<div class="order-control-pay" data-control="%s" data-order-sn="%s">%s</div>',
            $dataControl,
            $orderSn,
            $btn
        );
    }

    /*
     *  订单操作拼接
     */
    public static function orderBtn($orderSn, $control, $btn)
    {
        return sprintf(
            '<div class="order-control-%s" data-order-sn="%s">%s</div>',
            $control,
            $orderSn,
            $btn
        );
    }
}
