<?php

namespace App\Helpers;

use App\Models\Order;
use Ecommerce\Models\Product;
use App\Models\OrderProducts;
use App\Models\UserAddress;
use App\Models\ExpressModels\ExpressCondition;

use DB;

class ExpressPriceHelper
{
    // 计算订单邮费
    public static function orderExpressPrice($orderSn = null, $shippingRegion = null)
    {
        if (!$orderSn) {
            return array(0, 10.00);
        }
        $order = Order::where('order_sn', $orderSn)
            ->with('orderProducts')
            ->first();
        if (! $order) {
            return array(50001, '订单不存在');
        }
        if (! $shippingRegion) {
            // 获取默认地址region code
            $defaultAddr = app('address')->default($order->user_id);
            if (! $defaultAddr) {
                return array(500100, '请添加地址');
            }
            $shippingRegion = $defaultAddr->region_id;
        }
        $price = 0;
        foreach ($order->orderProducts as $key => $value) {
            $currentPrice = self::productExpressPrice($value->product_id, $shippingRegion, $value->number, $order->product_amount);
            if ($currentPrice > $price) {
                $price = $currentPrice;
            }
        }
        return array(0, number_format($price, 2, '.', ''));
    }

    // 单个商品邮费
    public static function productExpressPrice($procutId, $shippingRegion, $num = null, $orderFee = null)
    {
        $productExpress = Product::where('id', $procutId)
            ->with('expressTemplate.expressFee.expressCondition');
            
        $product = (clone $productExpress)->first();
        // 包邮
        if (!isset($product->expressTemplate)) {
            return 0;
        }
        if ($product->expressTemplate->free == 1) {
            return 0;
        }
        $expressCondition = $product->expressTemplate->expressFee->expressCondition
            ->pluck('region_code')
            ->toArray();
        // 未设置地区默认邮费
        if (!$expressCondition) {
            if ($num >= $product->expressTemplate->expressFee->free_fee && $product->expressTemplate->expressFee->free_fee != 0) {
                return 0;  //满件包邮
            }
            if ($orderFee >= $product->expressTemplate->expressFee->free_money && $product->expressTemplate->expressFee->free_money != 0) {
                return 0; //满金额包邮
            }
            return $product->expressTemplate->expressFee->default_fee; //默认运费
        } else {
        // 设置地区默认邮费
            $province = substr($shippingRegion, 0, 2).'0000';
            if (in_array($province, $expressCondition)) {
                //当前地区有设置运费模版
                return ExpressCondition::where('region_code', $province)
                    ->where('express_fee_id', $product->expressTemplate->expressFee->id)
                    ->value('default_fee');
            }
            return $product->expressTemplate->expressFee->default_fee;
        }
    }
}
