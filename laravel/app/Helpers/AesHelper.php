<?php

namespace App\Helpers;

class AesHelper
{
    private static $chars = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g',
        'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't',
        'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G',
        'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T',
        'U', 'V', 'W', 'X', 'Y', 'Z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!',
        '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_',
        '[', ']', '{', '}', '<', '>', '~', '`', '+', '='
    ];

    // 获取加密向量
    public static function createIv($length = 16)
    {
        // 在 $chars 中随机取 $length 个数组元素键名
        $chars = self::$chars;
        $keys = array_rand($chars, $length);

        $iv = '';
        for($i = 0; $i < $length; $i++) {
            // 将 $length 个数组元素连接成字符串
            $iv .= $chars[$keys[$i]];
        }
        // dd($iv, base64_encode($iv), base64_decode($iv));
        return $iv;
        // return base64_encode($iv);
    }

    // 生成key
    public static function createKey($length = 16)
    {
        $chars = self::$chars;
        $keys = array_rand($chars, $length);

        $key = '';
        for ($i=0; $i < $length; $i++) {
            $key .= $chars[$keys[$i]];
        }

        return $key;
    }
}
