<?php

namespace App\Helpers;

use App\Models\User;
use App\Models\Order;
use App\Models\FirstOrder;
use DB;

class FirstOrderReturnDiscount
{
    //首单七折返现
    public static function returnDiscount($order)
    {

        $firstOrderStatus = Order::where('user_id', $order->user_id)
            ->where('first_order_return_status', 1)
            ->first();
        if ($firstOrderStatus) {
            return false;
        }
        DB::beginTransaction();
          // 首单商品
        $firstOrder = Order::orderBy('created_at', 'asc')
            ->withTrashed()
            ->where('order_status', 1)
            ->where('pay_status', 1)
            ->where('provider', 'standard')
            ->where('shipping_status', 2)
            ->where('user_id', $order->user_id)
            ->lockForUpdate()
            ->first();
        $user = User::find($order->user_id);
        if (!$firstOrder || !$user) {
            DB::rollback();
            return false;
        }
        //首单退30%到余额
        if ($firstOrder->id == $order->id && $firstOrder->first_order_return_status == 0) {
            // 是否有售后成功的
            $orderRefund = Order::where('id', $order->id)
            ->whereHas('orderProducts', function ($q1) {
                return $q1->whereHas('refund', function ($q2) {
                    return $q2->where('refund_status', '!=', 4);
                });
            })
            ->with('orderProducts.refund')
            ->first();
            if ($orderRefund) {
                // 如果所有商品都售后，邮费也不产生首单返现
                $hasUnRefund = false;
                // 有售后
                // 售后退回的金额
                $refundPrice = 0;
                foreach ($orderRefund->orderProducts as $k2 => $v2) {
                    if ($v2->refund) {
                        $refundPrice += $v2->refund->pay;
                    } else {
                        $hasUnRefund = true;
                    }
                }

                $clacFee = $order->order_fee - $refundPrice;
                if (! $hasUnRefund) {
                    $clacFee -= $order->shipping_fee;
                }

                if ($clacFee < 0) {
                    return false;
                }
                $user->cumulate_profit = $user->cumulate_profit + $clacFee * 0.3;
                $firstOrdervalue = $clacFee * 0.3;
            } else {
                  // 无售后或售后正在进行中直接退回首单
                $user->cumulate_profit = $user->cumulate_profit + $order->order_fee * 0.3;
                $firstOrdervalue = $order->order_fee * 0.3;
            }

            $firstOrder->first_order_return_status = 1;//首单改成已改退
            $firstUpdate = $firstOrder->save();
            $userUpdate = $user->save();
            if ($firstUpdate && $userUpdate) {
                FirstOrder::create([
                    'user_id'  => $order->user_id,
                    'order_id' => $order->id,
                    'value'    => $firstOrdervalue,
                ]);
                DB::commit();
                return true;
            }
            DB::rollback();
            return false;
        }
        DB::rollback();
        return false;

    }

    // 首单判断
    public static function firstOrderCheck($order)
    {
        $firstOrderStatus = Order::where('user_id', $order->user_id)
            ->where('first_order_return_status', 1)
            ->first();
        if ($firstOrderStatus) {
            return false;
        }
        return true;
    }
}
