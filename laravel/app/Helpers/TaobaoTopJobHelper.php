<?php

namespace App\Helpers;

use App\Jobs\TaobaoOpenJob;

class TaobaoTopJobHelper
{
    /**
     * 接受队列处理
     * @param  string $func 处理数据的函数
     * @param  array $data 待处理数据
     */
    public static function handle($func, $data)
    {
        call_user_func(['App\Helpers\TaobaoTopJobHelper', $func], $data);
    }

    /**
     * taobao.items.inventory.get 商户仓库商品
     * @param  string  $merchant          商户代号
     * @param  string  $startModifiedTime 请求筛选修改终止时间
     * @param  string  $endModifiedTime   请求筛选修改开始时间
     * @param  integer $pageNo            [description]
     * @return [type]                     [description]
     */
    public static function taobaoItemsInventoryGet($merchant, $startModifiedTime, $endModifiedTime, $pageNo = 1)
    {
        // 设置请求筛选开始时间
        \Log::info('TaobaoItemsInventoryGet merchant: ' . $merchant);
        if ($startModifiedTime) {
            \Log::info('TaobaoItemsInventoryGet startModifiedTime: ' . $startModifiedTime);
        }
        if ($endModifiedTime) {
            \Log::info('TaobaoItemsInventoryGet endModifiedTime: ' . $endModifiedTime);
        }
        \Log::info('TaobaoItemsInventoryGet pageNo: ' . $pageNo);

        $data = compact('merchant', 'startModifiedTime', 'endModifiedTime', 'pageNo');
        $job = new TaobaoOpenJob('taobaoItemsInventoryGetJob', $data);
        dispatch($job);
    }

    /**
     * taobao.items.onsale.get 商户在售商品
     * @param  string  $merchant          商户代号
     * @param  string  $startModifiedTime 请求筛选修改终止时间
     * @param  string  $endModifiedTime   请求筛选修改开始时间
     * @param  integer $pageNo            页数
     */
    public static function taobaoItemsOnsaleGet($merchant, $startModifiedTime, $endModifiedTime, $pageNo = 1)
    {
        // 设置请求筛选开始时间
        \Log::info('taobaoItemsOnsaleGet merchant: ' . $merchant);
        if ($startModifiedTime) {
            \Log::info('taobaoItemsOnsaleGet startModifiedTime: ' . $startModifiedTime);
        }
        if ($endModifiedTime) {
            \Log::info('taobaoItemsOnsaleGet endModifiedTime: ' . $endModifiedTime);
        }
        \Log::info('taobaoItemsOnsaleGet pageNo: ' . $pageNo);

        $data = compact('merchant', 'startModifiedTime', 'endModifiedTime', 'pageNo');
        $job = new TaobaoOpenJob('taobaoItemsOnsaleGetJob', $data);
        dispatch($job);
    }

    /**
     * 处理队列任务
     */
    public static function __callStatic($name, $arguments)
    {
        if (substr($name, -3) === 'Job') {
            $func = substr($name, 0, -3);
            app('amii.taobao.open.job')->$func($arguments[0]);
        }
    }
}
