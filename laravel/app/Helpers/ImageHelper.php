<?php

namespace App\Helpers;

class ImageHelper
{
    public static function getImageUrl($value, $provider = 'product', $type = 'medium')
    {
        if (preg_match('/^http[s]?:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/', $value)) {
            return $value;
        }
        if (!$value) {
            return self::defatultImg($provider);
        }
        if (self::isFilename($value)) {
            if (func_num_args() == 2) {  ### default medium
                return config('amii.imageHost') . '/uploads/images/' . $provider . '/medium/' . $value;
            }
            if ($type=='source') {
                return config('amii.imageHost') . '/uploads/images/' . $provider . '/' . $value;
            }

            return config('amii.imageHost') . '/uploads/images/' . $provider . '/' . $type . '/' . $value;
        }

        if (self::isUrl($value)) {
            return $value;
        }

        return config('amii.testImageHost') . $value;
    }

    public static function getVideoUrl($value)
    {
        if (self::isFilename($value)) {
            return config('amii.videoHost') . '/uploads/videos/'.  $value;
        }

        if (self::isUrl($value)) {
            return $value;
        }

        return false;
    }

    public static function getPackageUrl($value)
    {
        if (self::isFilename($value)) {
            return config('amii.packageHost') . '/uploads/packages/'.  $value;
        }

        if (self::isUrl($value)) {
            return $value;
        }

        return false;
    }

    private static function defatultImg($provider = 'product')
    {
        switch ($provider) {
            case 'product':
                return config('amii.imageBanner');
                break;
            case 'banner':
                return config('amii.imageProduct');
                break;

            default:
                return config('amii.image');
                break;
        }
    }

    /**
     * check Filename
     */
    public static function isFilename($filename)
    {
        return preg_match('/^[a-zA-Z0-9]+\.[a-zA-Z0-9]{0,4}$/', $filename);
    }

    /**
     *  check uri
     */
    public static function isUrl($filename)
    {
        return preg_match('/^(http).*$/', $filename);
    }
}
