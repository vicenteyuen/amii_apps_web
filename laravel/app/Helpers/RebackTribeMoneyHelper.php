<?php
namespace App\Helpers;

use App\Models\Tribe;
use App\Models\Gain;
use App\Models\User;
use App\Models\Order;

class RebackTribeMoneyHelper
{
    const FirstTribe = 0.1;
    const SecondTribe = 0.1;

    //发货调用
    public static function rebackMoney($orderId)
    {
        $delay =  config('amii.tribe.autoReceivedTime');
        $order = Order::find($orderId);
        if (!$order) {
            return false;
        }
        $userId = $order->user_id;
        //签收10天签收
        $job = (new \App\Jobs\AddUserProfit(null, null, $orderId))->delay($delay);
        dispatch($job);

        // tribe表当前记录
        $currentUser = Tribe::where('user_id', $userId)
            ->first();

        // 当前记录不存在
        if (!$currentUser) {
            return false;
        }

        // 上一级
        $upperUser = Tribe::where('id', $currentUser->pid)
            ->first();

        // 不存在上一级
        if (!$upperUser) {
            return false;
        }
        $income = ($order->order_fee + $order->balance_discount) * self::SecondTribe;
        $income = floor(($income*100))/100;
        //签收10天后返回佣金
        $job = (new \App\Jobs\AddUserProfit($upperUser->user_id, $income, $orderId, 'first'))->delay($delay);
        dispatch($job);
         // 上上级
        $highestLevel = Tribe::where('id', $upperUser->pid)
            ->first();
        if ($highestLevel) {
            $income = ($order->order_fee + $order->balance_discount) * self::FirstTribe;
            $income = floor(($income*100))/100;
            $jobs = (new \App\Jobs\AddUserProfit($highestLevel->user_id, $income, $orderId, 'second'))->delay($delay);
            dispatch($jobs);
        }
    }

     // 快递签收调用
    public static function expressRebackMoney($orderId)
    {
        $order = Order::find($orderId);
        if (!$order) {
            return false;
        }
        $userId = $order->user_id;

        // tribe表当前记录
        $currentUser = Tribe::where('user_id', $userId)
            ->first();

        // 当前记录不存
        if (!$currentUser) {
            return false;
        }

        // 上一级
        $upperUser = Tribe::where('id', $currentUser->pid)
            ->first();

        // 不存在上一级
        if (!$upperUser) {
            return false;
        }
        $income = ($order->order_fee + $order->balance_discount) * self::SecondTribe;
        $income = floor(($income*100))/100;
        $delay = config('amii.tribe.calcTribeMoneyTime');
        $job = (new \App\Jobs\ReturnProfit($upperUser->user_id, $income, $orderId, 'first'))->delay($delay);
        dispatch($job);
        $highestLevel = Tribe::where('id', $upperUser->pid)
            ->first();
        if ($highestLevel) {
            $income = ($order->order_fee + $order->balance_discount) * self::FirstTribe;
            $income = floor(($income*100))/100;
            $jobs = (new \App\Jobs\ReturnProfit($highestLevel->user_id, $income, $orderId, 'second'))->delay($delay);
            dispatch($jobs);
        }
    }
}
