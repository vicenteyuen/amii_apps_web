<?php

namespace App\Helpers;

use App\Models\Size;

class ActivityHelper
{
    /**
     * 获取活动
     */
    public static function getActivity($provider)
    {
        switch ($provider) {
            case 'home':
                $activity = self::getHomeActivity();
                break;
            case 'cart':
                $activity = self::getCartActivity();
                break;
            case 'order':
                $activity = self::getOrderActivity();
                break;

            default:
                $activity = [];
                break;
        }

        return $activity;
    }

    /**
     * 获取首页活动
     */
    private static function getHomeActivity()
    {
        $activity = [];
        $firstOrder = self::getFirstOrder('home');
        if ($firstOrder[0] === 0) {
            $activity[] = $firstOrder[1];
        }
        return $activity;
    }

    /**
     * 获取购物车活动
     */
    private static function getCartActivity()
    {
        $activity = [];
        $firstOrder = self::getFirstOrder('cart');
        if ($firstOrder[0] === 0) {
            $activity[] = $firstOrder[1];
        }
        return $activity;
    }

    /**
     * 获取订单活动
     */
    private static function getOrderActivity()
    {
        $activity = [];
        $firstOrder = self::getFirstOrder('order');
        if ($firstOrder[0] === 0) {
            $activity[] = $firstOrder[1];
        }
        return $activity;
    }

    /**
     * 获取首单活动
     */
    private static function getFirstOrder($provider)
    {
        $isFirstOrder = app('amii.activity')->isFirstOrder();
        if (! $isFirstOrder) {
            return [60000];
        }

        $contentStr = '';
        $notice = '';
        $url = url('/wechat/mall/advertisement');
        switch ($provider) {
            case 'home':
                $contentStr = '首单7折，确认收货后返还，余额可提现';
                break;
            case 'cart':
                $contentStr = '首单7折';
                break;

            default:
                $contentStr = '首单7折';
                $notice = '首单金额的30%将在确认收货后，返回到您的余额账户，可用于购物，也可提现(首单7折不能与其他优惠共享)。';
                break;
        }

        return [
            0,
            [
                'activity_name' => 'first_order',
                'content' => $contentStr,
                'url' => $url,
                'notice' => $notice
            ]
        ];
    }
}
