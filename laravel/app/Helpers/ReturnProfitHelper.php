<?php

namespace App\Helpers;

use App\Models\Order;
use App\Models\User;
use App\Models\Balance;
use App\Models\OrderProductRefund;
use App\Models\OrderProduct;
use App\Models\Tribe;
use App\Models\Gain;
use Carbon\Carbon;
use DB;

class ReturnProfitHelper
{
    const FirstTribe = 0.1;
    const SecondTribe = 0.1;
    public static function returnProfitToUser($userId, $income, $orderId, $type)
    {

        $order =  Order::find($orderId);
        $user = User::find($userId);
        if (!$order || !$user) {
            return false;
        }
        if ($type == 'first') {
            $income = $order->first_tribe_return_money;
        } else {
            $income = $order->second_tribe_return_money;
        }
        $balance = Balance::where('order_id', $orderId)
                ->where('user_id', $userId)
                ->first();

        if (!$balance) {
            return false;
        }
        // 是否有在售后中的
        $orderRefund = Order::where('id', $order->id)
            ->whereHas('orderProducts', function ($q1) {
                return $q1->whereHas('refund', function ($q2) {
                    return $q2->whereIn('refund_status', [0,1,2]);
                });
            })
            ->with('orderProducts.refund')
            ->get();

        if (($balance->status === 0) && $orderRefund->isEmpty()) {
            // 增加用户收益,减少待返金额
            $user->cumulate_profit = $user->cumulate_profit + $income;
            $user->return_money = $user->return_money - $income;
            $user->save();
            // 关闭售后
            $order->can_refund = 0;
            $order->save();
            //佣金状态改成已返
            Balance::where('order_id', $orderId)
                ->where('user_id', $userId)
                ->update(['status' => 1]);
            return true;
        }
        return false;
    }

    public static function userTribeLevel($user, $order)
    {
        $currentUser = Tribe::where('user_id', $order->user_id)
            ->first();
        // 当前记录不存在
        if (!$currentUser) {
            return false;
        }
        // 上一级
        $upperUser = Tribe::where('id', $currentUser->pid)
            ->first();
        // 不存在上一级
        if (!$upperUser) {
            return false;
        }
        // 上一级
        if ($user->id == $upperUser->user_id) {
            return $second = self::SecondTribe;
        }
        $highestLevel = Tribe::where('id', $upperUser->pid)
            ->first();
        // 最上一级不存在
        if (!$highestLevel) {
            return false;
        }
        if ($user->id == $highestLevel->user_id) {
            return $first = self::FirstTribe;
        }
        return false;
    }

    // 售后成功重新计算佣金
    public static function cacluSuccessProfit($orderId, $refundId, $returnMoney = null)
    {
        $order = order::where('id', $orderId)
            ->first();
        if (!$order) {
            return false;
        }

        $currentUser = Tribe::where('user_id', $order->user_id)
            ->first();
        // 当前记录不存在
        if (!$currentUser) {
            return false;
        }
        // 上一级
        $upperUser = Tribe::where('id', $currentUser->pid)
            ->first();
        // 不存在上一级
        if (!$upperUser) {
            return false;
        }

        $tribeUpperUser = User::where('id', $upperUser->user_id)
            ->first();
        $refund = OrderProductRefund::where('id', $refundId)
            ->first();
       
        $balance = Balance::where('order_id', $orderId)
            ->where('user_id', $upperUser->user_id)
            ->where('status', 0)
            ->first();

        if (!$balance) {
            return false;
        }

        //更新佣金记录表gain
        $gain = Gain::where('balance_id', $balance->id)
            ->first();

        if (!$gain) {
            return false;
        }

        $orderRefund = Order::where('id', $orderId)
            ->whereHas('orderProducts', function ($q1) {
                return $q1->whereHas('refund', function ($q2) {
                    return $q2->where('refund_status', 3);
                });
            })
            ->with('orderProducts.refund')
            ->first();
            
        $refundPrice = 0;
        if ($orderRefund) {
            foreach ($orderRefund->orderProducts as $k2 => $v2) {
                if ($v2->refund && $v2->refund->refund_status == 3) {
                    $refundPrice += $v2->refund->pay;
                }
            }
        }
        // 售后佣金重新计算
        DB::beginTransaction();
        $oldFirstTribeProfit = floor($order->first_tribe_return_money * 100)/100;//之前的佣金
        $firstTribeProfit = floor(($order->order_fee + $order->balance_discount + $order->point_discount - $refundPrice) * self::SecondTribe * 100)/100;
        $order->first_tribe_return_money = $firstTribeProfit; //order 一级部落值
        $balance->value = $firstTribeProfit; //收益表值
        $gain->income = $firstTribeProfit; //记录表值
        $currentUser->contribute_income = $currentUser->contribute_income - $oldFirstTribeProfit + $firstTribeProfit; //更新tribe贡献值
        $tribeUpperUser->return_money = $tribeUpperUser->return_money - $oldFirstTribeProfit + $firstTribeProfit; //用户待返金额修改

        // 返回待返金额
        if ($returnMoney) {
            $tribeUpperUser->cumulate_profit = $tribeUpperUser->cumulate_profit + $firstTribeProfit;
            $tribeUpperUser->return_money = $tribeUpperUser->return_money - $firstTribeProfit;
            $balance->status = 1;
        }
        $order->save();
        $balance->save();
        $gain->save();
        $currentUser->save();
        $tribeUpperUser->save();

        // 上上级
        $highestLevel = Tribe::where('id', $upperUser->pid)
            ->first();
        if ($highestLevel) {
            $balanceUpper = Balance::where('order_id', $orderId)
                ->where('user_id', $highestLevel->user_id)
                ->where('status', 0)
                ->first();
            if (!$balanceUpper) {
                DB::rollback();
                return false;
            }
            $tribeHighestUser = User::where('id', $highestLevel->user_id)
                ->first();

            //更新佣金记录表gain
            $gainUpper = Gain::where('balance_id', $balanceUpper->id)
                ->first();

            if (!$gainUpper) {
                DB::rollback();
                return false;
            }

            $oldSecondTribeProfit = floor($order->second_tribe_return_money * 100)/100;//之前的佣金
            $secondTribeProfit = floor(($order->order_fee + $order->balance_discount + $order->point_discount - $refundPrice) * self::FirstTribe * 100)/100;
            $order->second_tribe_return_money = $secondTribeProfit; //order 二级部落值
            $balanceUpper->value = $secondTribeProfit; //收益表值
            $gainUpper->income = $secondTribeProfit; //记录表值
            $tribeHighestUser->return_money = $tribeHighestUser->return_money - $oldSecondTribeProfit + $secondTribeProfit; // 用户待返金额修改

         // 返回待返金额
            if ($returnMoney) {
                $tribeHighestUser->cumulate_profit = $tribeHighestUser->cumulate_profit + $secondTribeProfit;
                $tribeHighestUser->return_money = $tribeHighestUser->return_money - $secondTribeProfit;
                $balanceUpper->status = 1;
            }
            $order->save();
            $balanceUpper->save();
            $gainUpper->save();
            $tribeHighestUser->save();
        
            DB::commit();
            return true;
        }
        DB::commit();
        return true;
    }

    // 售后失败退回佣金
    public static function cacluFailProfit($orderId, $refundId)
    {
        $order = order::where('id', $orderId)
            ->first();
        if (!$order) {
            return false;
        }

        $currentUser = Tribe::where('user_id', $order->user_id)
            ->first();
        // 当前记录不存在
        if (!$currentUser) {
            return false;
        }
        // 上一级
        $upperUser = Tribe::where('id', $currentUser->pid)
            ->first();
        // 不存在上一级
        if (!$upperUser) {
            return false;
        }
      
        // 售后佣金重新计算
        DB::beginTransaction();

        $balance = Balance::where('order_id', $orderId)
            ->where('user_id', $upperUser->user_id)
            ->where('status', 0)
            ->first();

        if (!$balance) {
            return false;
        }

        $tribeUpperUser = User::where('id', $upperUser->user_id)
            ->first();

        // 返回待返金额
        $tribeUpperUser->cumulate_profit = $tribeUpperUser->cumulate_profit + $order->first_tribe_return_money;
        $tribeUpperUser->return_money = $tribeUpperUser->return_money - $order->first_tribe_return_money;
        $balance->status = 1;

        $balance->save();
        $tribeUpperUser->save();

        // 上上级
        $highestLevel = Tribe::where('id', $upperUser->pid)
            ->first();
        if ($highestLevel) {
            $balanceUpper = Balance::where('order_id', $orderId)
                ->where('user_id', $highestLevel->user_id)
                ->where('status', 0)
                ->first();

            if (!$balanceUpper) {
                return false;
            }
            
            $tribeHighestUser = User::where('id', $highestLevel->user_id)
                ->first();
             // 返回待返金额
            $tribeHighestUser->cumulate_profit = $tribeHighestUser->cumulate_profit + $order->second_tribe_return_money;
            $tribeHighestUser->return_money = $tribeHighestUser->return_money - $order->second_tribe_return_money;
            $balanceUpper->status = 1;
            $balanceUpper->save();
            $tribeHighestUser->save();
            
            DB::commit();
            return true;
        }
        DB::commit();
        return true;
    }

    // 售后成功重新计算佣金
    public static function refundSuccessReturnProfit($orderId, $refundId)
    {
        $order = order::where('id', $orderId)
            ->first();
        if (!$order) {
            return false;
        }
        $dt = Carbon::parse($order->receipt_time);
        $time = $dt->timestamp + 8 * 24 * 60 * 60;
        if (time() > $time) {
            // 更新售后的记录
            // 是否有售后成功的
            $orderRefund = Order::where('id', $orderId)
                ->whereHas('orderProducts', function ($q1) {
                    return $q1->whereHas('refund', function ($q2) {
                        return $q2->whereIn('refund_status', [0,1,2]);
                    });
                })
                ->with('orderProducts.refund')
                ->get();
            // 超过8天正在进行的售后不退回佣金
            if (!$orderRefund->isEmpty()) {
                return false;
            }
             self::cacluSuccessProfit($orderId, $refundId, true);
        } else {
            // 重新计算待返金额
            self::cacluSuccessProfit($orderId, $refundId);
        }
    }

    //拒绝售后超过8天待返金额
    public static function refundFailReturnProfit($orderId, $refundId)
    {
        $order = order::where('id', $orderId)
            ->first();
        if (!$order) {
            return false;
        }
        $dt = Carbon::parse($order->receipt_time);
        $time = $dt->timestamp + 8 * 24 * 60 * 60;
        if (time() > $time) {
            // 更新售后的记录
            // 是否有售后成功的
            $orderRefund = Order::where('id', $orderId)
                ->whereHas('orderProducts', function ($q1) {
                    return $q1->whereHas('refund', function ($q2) {
                        return $q2->whereIn('refund_status', [0,1,2]);
                    });
                })
                ->with('orderProducts.refund')
                ->get();
            // 超过8天正在进行的售后不退回佣金
            if (!$orderRefund->isEmpty()) {
                return false;
            }
            self::cacluFailProfit($orderId, $refundId);
        }
    }
}
