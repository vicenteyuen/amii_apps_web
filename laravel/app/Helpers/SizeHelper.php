<?php

namespace app\Helpers;

use App\Models\Size;

class SizeHelper
{
    // 匹配身材数据
    public static function recommendSize($height)
    {
        $size = Size::get();
        foreach ($size as $key => $value) {
            if ($value['least_height'] <= $height && $value['most_height'] > $height) {
                 return $value->id;
            }
        }
        return false;
    }
}
