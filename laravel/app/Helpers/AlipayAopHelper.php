<?php

namespace App\Helpers;

use App\Libraries\AlipayAop\AopClient;
use App\Libraries\AlipayAop\request\AlipayFundTransOrderQueryRequest;
use App\Libraries\AlipayAop\request\AlipayFundTransToaccountTransferRequest;
use App\Libraries\AlipayAop\request\AlipayOpenPublicTemplateMessageIndustryModifyRequest;
use App\Libraries\AlipayAop\request\AlipayTradeAppPayRequest;
use App\Libraries\AlipayAop\request\AlipayTradeFastpayRefundQueryRequest;
use App\Libraries\AlipayAop\request\AlipayTradeQueryRequest;
use App\Libraries\AlipayAop\request\AlipayTradeRefundRequest;
use App\Libraries\AlipayAop\request\AlipayTradeWapPayRequest;
use App\Libraries\AlipayAop\request\AlipayUserInfoAuthRequest;

if (!defined("AOP_SDK_WORK_DIR")) {
    define("AOP_SDK_WORK_DIR", storage_path('framework/cache/alipay_aop'));
}

class AlipayAopHelper
{
    static public $instance;
    static public $type;

    /**
     * @param null $type
     * @return self
     */
    static public function getInstance($type = null)
    {
        if (!static::$instance || static::$type != $type) {
            $aop = new AopClient();

            switch ($type) {
                case 'alipay':
                    $aop->appId = config('amii.payment.alipay.appid');
                    $aop->rsaPrivateKey = static::getRasData(resource_path('payment/cer/alipay_open_amii/rsa_private_key.pem'));
                    $aop->alipayrsaPublicKey = static::getRasData(resource_path('payment/cer/alipay_open_amii/alipay_public_key.pem'));
                    break;
                case 'withdraw':
                    $aop->appId = env('ALIPAY_OPEN_WITHDRAW');
                    $aop->rsaPrivateKey = static::getRasData(resource_path('payment/cer/alipay_open_amii_withdraw/rsa_private_key.pem'));
                    $aop->alipayrsaPublicKey = static::getRasData(resource_path('payment/cer/alipay_open_amii_withdraw/alipay_public_key.pem'));
                    break;
                case 'alipayweb':
                default:
                    $aop->appId = config('amii.payment.alipayweb.appid');
                    $aop->rsaPrivateKey = static::getRasData(resource_path('payment/cer/alipay_open_amii_web/rsa_private_key.pem'));
                    $aop->alipayrsaPublicKey = static::getRasData(resource_path('payment/cer/alipay_open_amii_web/alipay_public_key.pem'));
                    break;
            }
            $aop->debugInfo = true;
            static::$instance = new static($aop);
        }
        return static::$instance;
    }

    static public function getRasData($path)
    {
        $data = preg_replace("/---.+---/", '', file_get_contents($path));
        $data = str_replace("\n", '', $data);
        return $data;
    }

    private $aop;

    private function __construct(AopClient $aop)
    {
        $this->aop = $aop;
    }

    /**
     * @param \App\Libraries\AlipayAop\AlipayRequestInterface|object $request
     * @param array $bizContent
     * @return \SimpleXMLElement[]
     * @throws \Exception
     */
    private function executeBizContent($request, array $bizContent)
    {
        $request->setBizContent(json_encode(array_filter($bizContent)));
        $result = $this->aop->execute($request);

        if (!$result) throw new \Exception('支付宝sdk执行错误');

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $response_result = $result->$responseNode;
        if (!empty($response_result->code) && $response_result->code == 10000) {
            return $response_result;
        }
        \Log::error(self::class . ':' . get_class($request) . '请求失败', (array)$response_result);
        return $response_result;
    }

    /**
     * 手机网站支付跳转链接:http://doc.open.alipay.com/doc2/detail.htm?treeId=203&articleId=105463&docType=1
     * @param string $subject 订单标题最多256个字
     * @param string $out_trade_no 商户网站唯一订单号
     * @param float $total_amount 订单金额
     * @param string|null $body 订单具体描述最多128个字
     * @param string|null $timeout_express 允许的最晚付款时间,1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）
     * @return \App\Libraries\AlipayAop\|string
     */
    public function tradeWapPay(string $subject, string $out_trade_no, float $total_amount, string $body = null, string $timeout_express = null)
    {
        $request = new AlipayTradeWapPayRequest();

        $bizContent = [
            'subject' => $subject,
            'out_trade_no' => $out_trade_no,
            'total_amount' => $total_amount,
            'product_code' => 'QUICK_WAP_PAY',
            'body' => $body,
            'timeout_express' => $timeout_express,
        ];
//        $request->setReturnUrl(url('wechat/mall/rtt'));
//        $request->setNotifyUrl(url('wechat/mall/rtt'));

        $request->setBizContent(json_encode(array_filter($bizContent)));
        $result = $this->aop->pageExecute($request, 'get');

        return $result;
    }

    /**
     * App支付服务端:https://doc.open.alipay.com/docs/doc.htm?spm=a219a.7629140.0.0.8RYGhj&treeId=193&articleId=106370&docType=1
     * @param string $subject
     * @param string $out_trade_no
     * @param float $total_amount
     * @param string|null $body
     * @param string|null $timeout_express
     * @return string
     */
    public function tradeAppPay(string $subject, string $out_trade_no, float $total_amount, string $body = null, string $timeout_express = null)
    {
        $request = new AlipayTradeAppPayRequest();

        $bizContent = [
            'subject' => $subject,
            'out_trade_no' => $out_trade_no,
            'total_amount' => $total_amount,
            'product_code' => 'QUICK_MSECURITY_PAY',
            'body' => $body,
            'timeout_express' => $timeout_express,
        ];

//        $request->setNotifyUrl(url('wechat/mall/rtt'));

        $request->setBizContent(json_encode(array_filter($bizContent)));
        $result = $this->aop->sdkExecute($request);

        return $result;
    }

    /**
     * 异步回调通知处理
     * @param array $post
     * @return bool
     */
    public function notify(array $post)
    {
        return $this->aop->rsaCheckV1($post, NULL, "RSA");
    }

    /**
     * 交易查询:https://doc.open.alipay.com/doc2/apiDetail.htm?apiId=757&docType=4
     * @param string $out_trade_no 商户订单号
     * @param string|null $trade_no 支付宝交易号
     * @return null|\SimpleXMLElement[]
     */
    public function tradeQuery(string $out_trade_no, string $trade_no = null)
    {
        $request = new AlipayTradeQueryRequest();

        $bizContent = [
            'out_trade_no' => $out_trade_no,
            'trade_no' => $trade_no,
        ];

        return $this->executeBizContent($request, $bizContent);
    }

    /**
     * 交易退款接口:https://doc.open.alipay.com/doc2/apiDetail.htm?apiId=759&docType=4
     * @param string $out_trade_no 商户订单号
     * @param float $refund_amount 退款的金额，该金额订单金额
     * @param string $out_request_no 退款单号,同一笔交易多次退款需要保证唯一
     * @param string|null $refund_reason 退款的原因说明
     * @param string|null $trade_no 支付宝交易号
     * @return null|\SimpleXMLElement[]
     */
    public function tradeRefund(string $out_trade_no, float $refund_amount, string $out_request_no, string $refund_reason = null, string $trade_no = null)
    {
        $request = new AlipayTradeRefundRequest();

        $bizContent = [
            'out_trade_no' => $out_trade_no,
            'trade_no' => $trade_no,
            'refund_amount' => $refund_amount,
            'out_request_no' => $out_request_no,
            'refund_reason' => $refund_reason,
        ];

        return $this->executeBizContent($request, $bizContent);
    }

    /**
     * 退款查询
     * @param string $out_trade_no 商户订单号
     * @param string $out_request_no 商户退款单号
     * @param string|null $trade_no 支付宝交易号
     * @return null|\SimpleXMLElement[]
     */
    public function tradeFastpayRefundQuery(string $out_trade_no, string $out_request_no, string $trade_no = null)
    {
        $request = new AlipayTradeFastpayRefundQueryRequest();

        $bizContent = [
            'out_trade_no' => $out_trade_no,
            'trade_no' => $trade_no,
            'out_request_no' => $out_request_no,
        ];

        return $this->executeBizContent($request, $bizContent);
    }

    /**
     * 单笔转账到支付宝账户接口:https://doc.open.alipay.com/docs/api.htm?apiId=1321&docType=4
     * @param string $out_biz_no 发起转账的转账单据号
     * @param string $payee_account 收款登陆账号
     * @param float $amount 金额
     * @param string $payee_real_name 收款人真实姓名
     * @param string $remark 备注
     * @param string $payer_show_name 付款信息
     * @return bool|mixed
     */
    public function fundTransToaccountTransfer(string $out_biz_no, string $payee_account, float $amount, string $payee_real_name = null, string $remark = null, string $payer_show_name = null)
    {
        $request = new AlipayFundTransToaccountTransferRequest();

        $bizContent = [
            'out_biz_no' => $out_biz_no,
            'payee_type' => 'ALIPAY_LOGONID',
            'payee_account' => $payee_account,
            'amount' => $amount,
            'payer_show_name' => $payer_show_name,
            'payee_real_name' => $payee_real_name,
            'remark' => $remark,
        ];

        return $this->executeBizContent($request, $bizContent);
    }

    /**
     * 查询转账订单接口:https://doc.open.alipay.com/docs/api.htm?docType=4&apiId=1322
     * @param string $out_biz_no 发起转账的转账单据号
     * @param string|null $order_id 支付宝转账单据号
     * @return null|\SimpleXMLElement[]
     */
    public function fundTransOrderQuery(string $out_biz_no, string $order_id = null)
    {
        $request = new AlipayFundTransOrderQueryRequest();

        $bizContent = [
            'out_biz_no' => $out_biz_no,
            'order_id' => $order_id,
        ];

        return $this->executeBizContent($request, $bizContent);
    }

    public function userInfoAuth()
    {
        $request = new AlipayUserInfoAuthRequest();

        $bizContent = [
            'scopes' => [
                'auth_base',
            ],
            'state' => 'init',
            'is_mobile' => 'true',
        ];

        return $this->executeBizContent($request, $bizContent);
    }

    public function openPublicTemplateMessageIndustryModify()
    {
        $request = new AlipayOpenPublicTemplateMessageIndustryModifyRequest();

        $bizContent = [
            'primary_industry_name' => 'IT科技/IT软件与服务',
            'primary_industry_code' => '10001/20102',
            'secondary_industry_code' => '10001/20102',
            'secondary_industry_name' => 'IT科技/IT软件与服务',
        ];

        return $this->executeBizContent($request, $bizContent);
    }
}
