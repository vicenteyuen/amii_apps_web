<?php

namespace App\Helpers;

class EnableHelper
{
    // 用户等级
    public static function enable(&$currentEnable, $level)
    {

        switch ($level) {
            case '2':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            break;
                        case '极速退款':
                            break;
                        case '生日积分':
                            $value['describe'] ='1000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '9折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '1次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '1次/年';
                            break;
                        case '积分加倍':
                            break;
                        
                        default:
                            return ;
                    }
                }

                break;
            case '3':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            break;
                        case '极速退款':
                            break;
                        case '生日积分':
                            $value['describe'] ='2000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '9折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '2次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '2次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.1倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;
            case '4':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9.9折';
                            break;
                        case '极速退款':
                            $value['describe'] = '100元额度';
                            break;
                        case '生日积分':
                            $value['describe'] ='2000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '9折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '3次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '3次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.1倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;
            case '5':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9.8折';
                            break;
                        case '极速退款':
                            $value['describe'] = '300元额度';
                            break;
                        case '生日积分':
                            $value['describe'] = '3000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '8.5折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '4次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '4次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.2倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;
            case '6':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9.8折';
                            break;
                        case '极速退款':
                            $value['describe'] = '500元额度';
                            break;
                        case '生日积分':
                            $value['describe'] ='3000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '9折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '5次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '5次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.2倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;
            case '7':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9.7折';
                            break;
                        case '极速退款':
                            $value['describe'] = '1000元额度';
                            break;
                        case '生日积分':
                            $value['describe'] ='3000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '9折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '6次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '6次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.3倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;
            case '8':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9.6折';
                            break;
                        case '极速退款':
                            $value['describe'] = '2000元额度';
                            break;
                        case '生日积分':
                            $value['describe'] ='5000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '8.5折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '8次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '8次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.4倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;
            case '9':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9.5折';
                            break;
                        case '极速退款':
                            $value['describe'] = '5000元额度';
                            break;
                        case '生日积分':
                            $value['describe'] ='5000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '8.5折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '10次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '10次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.5倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;

            case '10':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9.3折';
                            break;
                        case '极速退款':
                            $value['describe'] = '10000元额度';
                            break;
                        case '生日积分':
                            $value['describe'] ='10000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '8折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '20次/年';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '20次/年';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.6倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;

            case '11':
                foreach ($currentEnable as $key => &$value) {
                    switch (trim($value['name'])) {
                        case '会员专享价':
                            $value['describe'] = '9折';
                            break;
                        case '极速退款':
                            $value['describe'] = '20000元额度';
                            break;
                        case '生日积分':
                            $value['describe'] ='10000积分';
                            break;
                        case '生日折扣卡':
                            $value['describe'] = '8折';
                            break;
                        case '尊享包邮卡':
                            $value['describe'] = '年度包邮';
                            break;
                        case '退货保障卡':
                            $value['describe'] = '无理由退货';
                            break;
                        case '积分加倍':
                            $value['describe'] = '1.8倍';
                            break;
                        
                        default:
                            return ;
                    }
                }
                break;
                
            default:
                break;
        }
    }
}
