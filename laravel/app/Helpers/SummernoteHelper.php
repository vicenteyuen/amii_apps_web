<?php
/**
 * Created by PhpStorm.
 * User: tom
 * Date: 23/2/2017
 * Time: 10:30 AM
 */

namespace App\Helpers;

class SummernoteHelper
{
    /**
     * 格式化图片链接，取出图片名和后缀  eg: xxx.jpg
     *
     * @param $string      //待格式化字符串
     * @param $provider    //图片所属文件夹
     * @return mixed
     */
    public static function getImgName($string, $provider)
    {
        $str = preg_replace('/(<img)(.*?)(src=\".*?)((http|https:\/\/.*?)?)(uploads\/images\/' . $provider . '\/)(.*?)\.(jpg|gif|bmp|bnp|png|jpeg)(\")(.*?)>/i', "\${1} \${3}\${7}.\${8}\${9}\${10}>", $string);

        if ($str == null) {
            return $string;
        }
        return $str;
    }

    /**
     * 根据图片名补全图片链接
     *
     * @param $string     //待格式化字符串
     * @param $provider   //图片所属文件夹
     * @return mixed
     */
    public static function getImgUrl($string, $provider)
    {
        $pregStr = preg_replace('/(src=\")(.*?)\.(jpg|gif|bmp|bnp|png|jpeg)(\")(.*?)>/i', "src='".config('amii.imageHost')."/uploads/images/". $provider ."/\${2}.\${3}' style='width:100%'>", $string);

        #$resultStr = preg_replace("/(src=')(http|https)(.*?)(http|https)(.*?)\.(jpg|gif|bmp|bnp|png)(')(.*?)>/i", "src='"."\${4}\${5}.\${6}' style='width:100%'>", $pregStr);

        #$resultStr = preg_replace("/(src=')(.+?)(http|https)(.*?)\.(jpg|gif|bmp|bnp|png)(.*?)(')/i", "src='"."\$3\$4.$5' style='width:100%'>", $pregStr);

        $resultStr = preg_replace("/(src=')([^']*)(http|https)([^']*)\.(jpg|gif|bmp|bnp|png)(')/i", "src='\$3\$4.\$5'", $pregStr);

        return $resultStr;
    }
}
