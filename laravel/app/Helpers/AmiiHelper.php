<?php

namespace App\Helpers;

use DB;

class AmiiHelper
{
    /**
     * 生成uuid
     */
    public static function newUuid()
    {
        NEWUUID:
        $newUuid = strtoupper(substr(sha1(bin2hex(openssl_random_pseudo_bytes(16)), false), rand(4, 20), 64));
        // find user by uuid
        $user = app('user')->findByUuid($newUuid);
        if ($user) {
            goto NEWUUID;
        }
        return $newUuid;
    }
    /**
     * 十八位订单号的生成
     * @param    [type]                   $user_id     用户id
     * @param    int                  $code_length 订单号长度，默认18位
     * @return   string                    数字格式订单号
     */
    public static function generateOrderSn($user_id, $code_length = 18)
    {
        $time = time();
        $date = date('Y-m-d', $time);
        $y = strrev(date('y', $time));//2位
        $day = date('md', $time);//4位
        $seconds = strrev($time - strtotime($date.' 00:00:00') + 86400);//5位或6位
        $pad_length = $code_length - strlen($seconds.$user_id) - 6;
        if ($pad_length > 0) {
            $seconds = str_pad($seconds, strlen($seconds) + $pad_length, 235, STR_PAD_LEFT);
        }

        return $y.$user_id.$day.$seconds;
    }

    /**
     * 生产montnet流水号
     */
    public static function generateMontnetMsgId($code_length = 18)
    {
        $time = time();
        $date = date('Y-m-d', $time);
        $y = strrev(date('y', $time));//2位
        $day = date('md', $time);//4位
        $seconds = strrev($time - strtotime($date.' 00:00:00') + 86400);//5位或6位
        $pad_length = $code_length - strlen($seconds) - 6;
        if ($pad_length > 0) {
            $seconds = str_pad($seconds, strlen($seconds) + $pad_length, 235, STR_PAD_LEFT);
        }

        return intval($y.$day.$seconds);
    }

    /**
     * 十位商品编号生成
     */
    public static function generateProductSn()
    {
        $time = time();
        $date = date('Y-m-d', $time);
        $seconds = $time - strtotime($date.' 00:00:00') + 172800;//6位
        $str = date('ymd', $time).(intval($seconds / 100) + $seconds % 100000);

        return $str;
    }

    /**
     * 批量更新多条目 同一列 不同值
     */
    public static function updateColumnDiffValues($query, $columnUpdate, $columnCheck, array $dataArr)
    {
        // $columnUpdate    will be update
        // $columnCheck     check model column
        // $dataArr         [['key' => 'keyValue', 'value' => 'valueValue'], ['key' => 'keyValue', 'value' => 'valueValue']]
        $sql = 'case ' . $columnCheck;
        foreach ($dataArr as $data) {
            $sql .= ' when ' . $data['key'] . ' then ' . $data['value'];
        }
        $sql .= ' end';

        return $query->update([
            $columnUpdate => DB::raw($sql),
        ]);
    }

    /**
        utf8 编码汉英个数
     **/
    public static function utf8_strlen($string = null)
    {
        // 将字符串分解为单元
        preg_match_all("/./us", $string, $match);
        // 返回单元个数
        return count($match[0]);
    }
}
