<?php

namespace App\Helpers;

class SnumHelper
{
    private static $nums = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    ];

    // 获取加密向量
    public static function snum($length = 16)
    {
        // 在 $nums 中随机取 $length 个数组元素键名
        $nums = self::$nums;
        $keys = array_rand($nums, $length);

        $snum = '';
        for($i = 0; $i < $length; $i++) {
            // 将 $length 个数组元素连接成字符串
            $snum .= $nums[$keys[$i]];
        }
        return $snum;
    }
}
