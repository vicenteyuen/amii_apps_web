<?php

namespace App\Helpers;

class PaginateHelper
{
    private static $paginateKeys = [
        "total",
        "per_page",
        "current_page",
        "last_page",
        "next_page_url",
        "prev_page_url",
        "from",
        "to",
    ];

    public static function format(array $data)
    {
        $returnData = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $switch = true;
                foreach (static::$paginateKeys as $paginateKey) {
                    if (! array_key_exists($paginateKey, $value)) {
                        $switch = false;
                        break;
                    }
                }
                if ($switch) {
                    $paginateData = $value['data'];
                    unset($value['data']);
                    $returnData['pagination'] = $value;
                    $returnData['paginate_data'][$key] = $paginateData;
                } else {
                    if (is_array($value)) {
                        $returnData[$key] = self::format($value);
                    } else {
                        $returnData[$key] = $value;
                    }
                }
            } else {
                $returnData[$key] = $value;
            }
        }
        return $returnData;
    }
}
