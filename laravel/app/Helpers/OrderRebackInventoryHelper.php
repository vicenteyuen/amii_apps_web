<?php

namespace App\Helpers;

class OrderRebackInventoryHelper
{
    /**
     * 接受处理操作
     */
    public static function handle($provider, $orderSn)
    {
        switch ($provider) {
            case 'unconfirm':
                app('orderjob')->unconfirm($orderSn);
                break;
            case 'cancel':
                app('orderjob')->cancel($orderSn);
                break;

            default:
                # code...
                break;
        }
    }
}
