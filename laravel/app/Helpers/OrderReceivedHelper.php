<?php

namespace App\Helpers;

use Carbon\Carbon;

class OrderReceivedHelper
{
    // 签收
    public static function received($order)
    {
        if (!$order->receipt_time) {
            $order->receipt_time = Carbon::now(); //签收时间
            $order->save();
        }
        // 8天关闭售后
        $delay = config('amii.tribe.closeOrderTime');
        $job = (new \App\Jobs\CloseOrderRefund($order->id))->delay($delay);
        dispatch($job);
    }
}
