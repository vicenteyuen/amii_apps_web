<?php

namespace App\Helpers;

use QrCode;
use URL;
use App\Models\User;

class QrcodeHelper
{
    /*
     * qrcode different color and background
     */
    public static function generateQr($provider = null, $uuid)
    {
        if (!$provider) {
            $provider = User::where('uuid', $uuid)
                ->value('image_provider');
        } else {
            User::where('uuid', $uuid)
                ->update(['image_provider' => $provider]);
        }
        switch ($provider) {
            case 1:
                //black
                $color = [0, 0, 0];
                $backUrl = '../public/assets/images/Qrcode/qrcode1.png';
                $backUrlImg = imagecreatefrompng($backUrl);
                $x = 80;
                $y = 230;
                $size = 490;
                $marginX = 0;
                $marginY = 0;
                break;
            case 2:
                //red
                $color = [255, 59, 48];
                $backUrl = '../public/assets/images/Qrcode/qrcode2.png';
                $backUrlImg = imagecreatefrompng($backUrl);
                $x = 340;
                $y = 578;
                $size = 391;
                $marginX = 15;
                $marginY = 15;
                break;
            case 3:
                //lighter red
                $color = [255, 75, 149];
                $backUrl = '../public/assets/images/Qrcode/qrcode3.png';
                $backUrlImg = imagecreatefrompng($backUrl);
                $x = 129;
                $y = 267;
                $size = 424;
                $marginX = 15;
                $marginY = 15;
                break;
            case 4:
                // blue
                $color = [0, 183, 238];
                $backUrl = '../public/assets/images/Qrcode/qrcode4.png';
                $backUrlImg = imagecreatefrompng($backUrl);
                $x = 280;
                $y = 474;
                $size = 391;
                $marginX = 15;
                $marginY = 15;
                break;
            case 5:
                // white
                $color = [249, 134, 31];
                $backUrl = '../public/assets/images/Qrcode/qrcode5.png';
                $backUrlImg = imagecreatefrompng($backUrl);
                $x = 256;
                $y = 435;
                $size = 391;
                $marginX = 15;
                $marginY = 15;
                break;
            case 6:
                // pink
                $color = [254, 103, 179];
                $backUrl = '../public/assets/images/Qrcode/qrcode6.png';
                $backUrlImg = imagecreatefrompng($backUrl);
                $x = 228;
                $y = 480;
                $size = 391;
                $marginX = 15;
                $marginY = 15;
                break;

            default:
                return false;
        }

        $url = url('/wechat') . '/my/share?uuid=' . $uuid;
        $qrcodeUrl = QrCode::encoding('UTF-8')
            ->format('png')
            ->errorCorrection('Q')
            ->size($size)
            ->color($color[0], $color[1], $color[2])
            ->margin(0)
            ->merge('/public/assets/images/A+icon.png');


        if ($provider == 5) {
             $qrcodeUrl =  $qrcodeUrl->backgroundColor(250, 189, 3);
        }
        $qrcodeUrl = $qrcodeUrl->generate($url, public_path('assets/images/Qrcode/qrcode.png'));
        $qrcodeImg = imagecreatefrompng('../public/assets/images/Qrcode/qrcode.png');
        $qrcodeInfo = getimagesize('../public/assets/images/Qrcode/qrcode.png');
        $alpha = 100;
        $color = imagecolorallocate($backUrlImg, 0, 0, 0);
        imagecopymerge($backUrlImg, $qrcodeImg, $x, $y, $marginX, $marginY, $size - $marginX * 2, $size - $marginY * 2, $alpha);
        ob_start(); // Let's start output buffering.
        imagepng($backUrlImg); //This will normally output the image, but because of ob_start(), it won't.
        $contents = ob_get_contents(); //Instead, output above is saved to $contents
        ob_end_clean(); //End the output buffer.
        imagedestroy($backUrlImg);
        imagedestroy($qrcodeImg);
        return base64_encode($contents);
    }

    /**
    *  generateQr of version channel download
    */
    public function getChannelQr($url)
    {
       //black
        $color = [0, 0, 0];
        $backUrl = '../public/assets/images/Qrcode/squareQrcode.png';
        $backUrlImg = imagecreatefrompng($backUrl);
        $x = 20;
        $y = 20;
        $size = 520;
        $marginX = 0;
        $marginY = 0;

        $qrcodeUrl = QrCode::encoding('UTF-8')
            ->format('png')
            ->errorCorrection('Q')
            ->size($size)
            ->color($color[0], $color[1], $color[2])
            ->margin(0)
            ->merge('/public/assets/images/A+icon.png', .3);

        $qrcodeUrl = $qrcodeUrl->generate($url, public_path('assets/images/Qrcode/qrcode.png'));
        $qrcodeImg = imagecreatefrompng('../public/assets/images/Qrcode/qrcode.png');
        $qrcodeInfo = getimagesize('../public/assets/images/Qrcode/qrcode.png');
        $alpha = 100;
        $color = imagecolorallocate($backUrlImg, 0, 0, 0);
        imagecopymerge($backUrlImg, $qrcodeImg, $x, $y, $marginX, $marginY, $size - $marginX * 2, $size - $marginY * 2, $alpha);
        ob_start(); // Let's start output buffering.
        imagepng($backUrlImg); //This will normally output the image, but because of ob_start(), it won't.
        $contents = ob_get_contents(); //Instead, output above is saved to $contents
        ob_end_clean(); //End the output buffer.
        imagedestroy($backUrlImg);
        imagedestroy($qrcodeImg);
        return base64_encode($contents);
    }

    // 邀请用注册二维码
    public static function invateQrcode($uuid)
    {
        $color = [0, 0, 0];
        $backUrl = '../public/assets/images/Qrcode/squareQrcode.png';
        $backUrlImg = imagecreatefrompng($backUrl);
        $x = 35;
        $y = 35;
        $size = 500;
        $marginX = 0;
        $marginY = 0;
        $url = url('/wechat/my/share') . '?uuid=' . $uuid;
        $qrcodeUrl = QrCode::encoding('UTF-8')
            ->format('png')
            ->errorCorrection('Q')
            ->size($size)
            ->color($color[0], $color[1], $color[2])
            ->margin(0)
            ->merge('/public/assets/images/A+icon.png');
        $qrcodeUrl = $qrcodeUrl->generate($url, public_path('assets/images/Qrcode/qrcode.png'));
        $qrcodeImg = imagecreatefrompng('../public/assets/images/Qrcode/qrcode.png');
        $qrcodeInfo = getimagesize('../public/assets/images/Qrcode/qrcode.png');
        $alpha = 100;
        $color = imagecolorallocate($backUrlImg, 0, 0, 0);
        imagecopymerge($backUrlImg, $qrcodeImg, $x, $y, $marginX, $marginY, $size - $marginX * 2, $size - $marginY * 2, $alpha);
        ob_start(); // Let's start output buffering.
        imagepng($backUrlImg); //This will normally output the image, but because of ob_start(), it won't.
        $contents = ob_get_contents(); //Instead, output above is saved to $contents
        ob_end_clean(); //End the output buffer.
        imagedestroy($backUrlImg);
        imagedestroy($qrcodeImg);
        return base64_encode($contents);
    }
}
