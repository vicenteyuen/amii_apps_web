<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use App\Helpers\EasyWechatHelper;
use App\Libraries\Alipay\AliPaymentSdk;
use App\Libraries\Alipay\AlipayWap;
use App\Libraries\Alipay\Alipay;

class PaymentHelper
{
    /**
     * 获取支付金额
     */
    private static function getPayAmount($amount = 1, $provider = 'alipay')
    {
        if (config('app.debug')) {
            $amount = 0.01;
        }
        if ($provider != 'alipay') {
            $amount = intval($amount * 100);
        }
        return $amount;
    }

    /**
     * 支付宝app支付
     */
    public static function alipay($orderSn = '')
    {
        $payingData = app('order')->payingData($orderSn);
        if (! $payingData) {
            return false;
        }
        $orderFee = self::getPayAmount($payingData->order_fee);
        $subject = '';
        if ($payingData->orderProducts->isEmpty()) {
            return false;
        }
        $subject = $payingData->orderProducts[0]->sellProduct->product->name;

        $alipay = new AliPaymentSdk();
        $alipay->setService('mobile.securitypay.pay');//设置接口名称
        $alipay->setSignType('RSA');//要用RSA签名
        $alipay->setOutTradeNo($orderSn);
        $alipay->setTotalFee($orderFee);//传给支付宝的金额单位是元
        $alipay->setSubject($subject);
        $alipay->setNotifyUrl(config('amii.payment.alipay.notifyurl'));
        $payStr = $alipay->getPayPara();
        return $payStr;
    }

    /**
     * 支付宝网页支付
     */
    public static function alipayweb($orderSn = '')
    {
        $payingData = app('order')->payingData($orderSn);
        if (! $payingData) {
            return false;
        }
        $orderFee = self::getPayAmount($payingData->order_fee);
        $subject = '';
        if ($payingData->orderProducts->isEmpty()) {
            return false;
        }
        $subject = $payingData->orderProducts[0]->sellProduct->product->name;

        $alipaySubmit = new AlipayWap();
        //支付宝中的支付类型,必填，不能修改
        $info['payment_type'] = '1';
        // //服务器异步通知页面路径,需http://格式的完整路径，不能加?id=123这类自定义参数
        // $info['notify_url'] = 'http://127.0.0.1:8000/payment/alipay-notify';
        //页面跳转同步通知页面路径,需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
        $info['return_url'] = url('/wechat/my/orders/show/' . $orderSn);
        //商户订单号,商户网站订单系统中唯一订单号，必填
        $info['out_trade_no'] = $orderSn;
        // 商品信息
        $info['subject'] = $subject;
        // 支付价格
        $info['total_fee'] = $orderFee;
        //商品展示地址,需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html
        //$info['show_url'] = "http://商户网关地址/create_direct_pay_by_user-PHP-UTF-8/return_url.php";
        //防钓鱼时间戳,若要使用请调用类文件submit中的query_timestamp函数
        //$anti_phishing_key = $alipaySubmit->query_timestamp();
        //客户端的IP地址,非局域网的外网IP地址，如：221.0.0.1
        //$exter_invoke_ip = "";
        $info['_input_charset'] = config('amii.payment.alipayweb.input_charset');
        $info['partner'] = trim(config('amii.payment.alipayweb.partnerid'));
        $info['seller_email'] = trim(config('amii.payment.alipayweb.seller_email'));
//        $info['service'] = 'create_direct_pay_by_user';
        //建立请求
        $requestUrl = $alipaySubmit->buildRequestUrl($info);

        return $requestUrl;
    }

    /**
     * 微信app支付
     */
    public static function wechat($orderSn = '')
    {
        $payingData = app('order')->payingData($orderSn);
        if (! $payingData) {
            return false;
        }
        $orderFee = self::getPayAmount($payingData->order_fee, 'wechat');
        $subject = '';
        if ($payingData->orderProducts->isEmpty()) {
            return false;
        }
        $subject = $payingData->orderProducts[0]->sellProduct->product->name;

        $easywechat = EasyWechatHelper::easywechatWechat();

        $payment = $easywechat->payment;

        $attributes = [
            'trade_type'       => 'APP', // JSAPI，NATIVE，APP...
            'body'             => $subject,
            'detail'           => $subject,
            'out_trade_no'     => $orderSn,
            'total_fee'        => $orderFee,
            'notify_url'       => config('amii.payment.wechat.notifyurl'), // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            // ...
        ];

        $order = EasyWechatHelper::order($attributes);

        $result = $payment->prepare($order);
        if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS'){
            $prepayId = $result->prepay_id;
            $payStr = $payment->configForAppPayment($prepayId);
            return json_encode($payStr);
        }
        return false;
    }

    /**
     * 微信公众号支付
     */
    public static function wemp($orderSn = '', $openid = '')
    {
        $payingData = app('order')->payingData($orderSn);
        if (! $payingData) {
            return false;
        }
        $orderFee = self::getPayAmount($payingData->order_fee, 'weweb');
        $subject = '';
        if ($payingData->orderProducts->isEmpty()) {
            return false;
        }
        $subject = $payingData->orderProducts[0]->sellProduct->product->name;

        $easywechat = EasyWechatHelper::easywechatWemp();
        $payment = $easywechat->payment;

        $attributes = [
            'openid'           => $openid,
            'trade_type'       => 'JSAPI', // JSAPI，NATIVE，APP...
            'body'             => $subject,
            'detail'           => $subject,
            'out_trade_no'     => $orderSn,
            'total_fee'        => $orderFee,
            'notify_url'       => config('amii.payment.wemp.notifyurl'), // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            // ...
        ];

        $order = EasyWechatHelper::order($attributes);

        $result = $payment->prepare($order);

        if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS'){
            $prepayId = $result->prepay_id;
            $payStr = $payment->configForJSSDKPayment($prepayId);
            return $payStr;
        }
        \Log::error(self::class.':生成支付单失败', $result->toArray());
        return false;
    }

    /**
     * 微信小程序支付
     */
    public static function weapp($orderSn = '', $openid = '')
    {
        $payingData = app('order')->payingData($orderSn);
        if (! $payingData) {
            return false;
        }
        $orderFee = self::getPayAmount($payingData->order_fee, 'weapp');
        $subject = '';
        if ($payingData->orderProducts->isEmpty()) {
            return false;
        }
        $subject = $payingData->orderProducts[0]->sellProduct->product->name;

        $easywechat = EasyWechatHelper::easywechatWeapp();
        $payment = $easywechat->payment;

        $attributes = [
            'openid'           => $openid,
            'trade_type'       => 'JSAPI', // JSAPI，NATIVE，APP...
            'body'             => $subject,
            'detail'           => $subject,
            'out_trade_no'     => $orderSn,
            'total_fee'        => $orderFee,
            'notify_url'       => config('amii.payment.weapp.notifyurl'), // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            // ...
        ];

        $order = EasyWechatHelper::order($attributes);

        $result = $payment->prepare($order);

        if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS'){
            $prepayId = $result->prepay_id;
            $payStr = $payment->configForJSSDKPayment($prepayId);
            // 小程序返回json
            return $payStr;
        }
        return false;
    }

    /**
     * 支付接受异步通知
     */
    public static function notify($provider, $request)
    {
        \Log::info(self::class.'支付成功回调通知:'.$provider, request()->all());
        switch ($provider) {
            case 'alipay':
                $returnData = self::notifyAlipay();
                break;
            case 'alipayweb':
                $returnData = self::notifyAlipayweb();
                break;
            case 'weopenpay':
                $returnData = self::notifyWechat();
                break;
            case 'weapp':
                $returnData = self::notifyWeapp();
                break;
            case 'wemppay':
                $returnData = self::notifyWemppay();
                break;

            default:
                $returnData = '';
                break;
        }

        return $returnData;
    }

    /**
     * 支付宝付款异步通知
     */
    private static function notifyAlipay()
    {
        $alipay = new Alipay([]);
        $result = $alipay->verifyCallback();
        if ($result) {
            if ('TRADE_SUCCESS' == \Request::get('trade_status')) {
                $paid = app('order')->paid(\Request::get('out_trade_no'), 'alipay');
                if ($paid) {
                    return 'success';
                }
            }
        }

        return 'fail';
    }

    /**
     * 支付宝网页支付异步通知
     */
    public static function notifyAlipayweb()
    {
        $alipayweb = new Alipay([]);
        $result = $alipayweb->verifyCallback();
        if ($result) {
            if ('TRADE_SUCCESS' == \Request::get('trade_status')) {
                $paid = app('order')->paid(\Request::get('out_trade_no'), 'alipayweb');
                if ($paid) {
                    return 'success';
                }
            }
        }

        return 'fail';
    }

    /**
     * 微信app支付异步通知
     */
    private static function notifyWechat()
    {
        $easywechat = EasyWechatHelper::easywechatWechat();
        $return = EasyWechatHelper::notify($easywechat->payment, 'wechat');
        return $return;
    }

    /**
     * 微信小程序支付异步通知
     */
    private static function notifyWeapp()
    {
        $easywechat = EasyWechatHelper::easywechatWeapp();
        $return = EasyWechatHelper::notify($easywechat->payment, 'weapp');
        return $return;
    }

    /**
     * 微信公众号支付异步通知
     */
    private static function notifyWemppay()
    {
        $easywechat = EasyWechatHelper::easywechatWemp();
        $return = EasyWechatHelper::notify($easywechat->payment, 'wemp');
        return $return;
    }
}
