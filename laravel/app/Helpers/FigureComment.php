<?php
namespace App\Helpers;

use App\Models\SellProduct;
use App\Models\UserComment;
use DB;

class FigureComment
{
    // 计算好评率
    public static function figurePraise($sellProductIds)
    {
        $comment = UserComment::whereIn('product_sell_id', $sellProductIds)
            ->where('comment_rank_id', '>=', 3)
            ->groupBy('product_sell_id')
            ->select('product_sell_id', DB::raw('count(product_sell_id) as total'))
            ->get();

        $sellProducts = SellProduct::whereIn('id', $sellProductIds)
            ->select('id', 'volume')
            ->get();

        $praise = (clone $comment);

        if ($praise->isEmpty() || $sellProducts->isEmpty()) {
            return false;
        }
        foreach ($sellProducts as $k1 => $v1) {
            foreach ($praise as $k2 => $v2) {
                if ($v2['product_sell_id'] == $v1['id']) {
                    $datas['id'][] = $v1['id'];
                    if ($v1['volume'] <= 0) {
                        $datas['praise'][] = 95;
                        break;
                    }
                    $like = intval(($v2['total']/$v1['volume']) * 100);
                    if ($like <= 95) {
                         $datas['praise'][] = 95;
                         break;
                    }
                    if ($like >= 100) {
                        $datas['praise'][] = 100;
                        break;
                    }
                    $datas['praise'][] = $like ;
                    break;
                }
            }
        }
        $sql = 'case id';
        foreach ($datas['id'] as $key => $id) {
            $sql .= ' when ' . $id . ' then ' . $datas['praise'][$key];
        }
        $sql .= ' end';
        return SellProduct::whereIn('id', $sellProductIds)
            ->update(['praise' => DB::raw($sql)]);

    }
}
