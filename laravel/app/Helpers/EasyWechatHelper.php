<?php

namespace App\Helpers;

use EasyWeChat\Foundation\Application as EasyWeChatApplication;
use EasyWeChat\Payment\Order as EasyWeChatOrder;

class EasyWechatHelper
{
    /**
     * easywechat 实例化
     */
    private static function easywechatInit(array $options)
    {
        return new EasyWeChatApplication($options);
    }

    /**
     * 获取微信公众号配置
     */
    public static function wempConfig()
    {
        $easywechat = self::easywechatWemp();
        $easywechat['config']->set('app_id', config('oath2.wemp.client_id'));
        $easywechat['config']->set('secret', config('oath2.wemp.client_secret'));

        $presim = [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone',
            'startRecord',
            'stopRecord',
            'onVoiceRecordEnd',
            'playVoice',
            'pauseVoice',
            'stopVoice',
            'onVoicePlayEnd',
            'uploadVoice',
            'downloadVoice',
            'chooseImage',
            'previewImage',
            'uploadImage',
            'downloadImage',
            'translateVoice',
            'getNetworkType',
            'openLocation',
            'getLocation',
            'hideOptionMenu',
            'showOptionMenu',
            'hideMenuItems',
            'showMenuItems',
            'hideAllNonBaseMenuItem',
            'showAllNonBaseMenuItem',
            'closeWindow',
            'scanQRCode',
            'chooseWXPay',
            'openProductSpecificView',
            'addCard',
            'chooseCard',
            'openCard',
        ];

        return $easywechat->js
            ->setUrl(request('url', request()->fullUrl()))
            ->config($presim, false, false, false);
    }

    /**
     * app支付实例化
     */
    public static function easywechatWechat()
    {
        $options = [
            'debug' => config('amii.easywechat.debug'),
            'app_id' => config('amii.payment.wechat.appid'),
            // 'aes_key' => null, // 可选
            // payment
            'payment' => [
                'merchant_id' => config('amii.payment.wechat.merchantid'),
                'key' => config('amii.payment.wechat.key'),
                'cert_path' => resource_path('payment/cer/weixin/wechat/apiclient_cert.pem'), // XXX: 绝对路径！！！！
                'key_path' => resource_path('payment/cer/weixin/wechat/apiclient_key.pem'),      // XXX: 绝对路径！！！！
                'notify_url' => config('amii.payment.wechat.notifyurl'),       // 你也可以在下单时单独设置来想覆盖它
            ],
        ];

        $easywechat = self::easywechatInit($options);

        return $easywechat;
    }

    /**
     * 小程序支付实例化
     */
    public static function easywechatWeapp()
    {
        // 小程序和公众号共用一个微信商户
        $options = [
            'debug' => config('amii.easywechat.debug'),
            'app_id' => config('amii.payment.weapp.appid'),
            // 'aes_key' => null, // 可选
            // payment
            'payment' => [
                'merchant_id' => config('amii.payment.weapp.merchantid'),
                'key' => config('amii.payment.weapp.key'),
                'cert_path' => resource_path('payment/cer/weixin/wemp/apiclient_cert.pem'), // XXX: 绝对路径！！！！
                'key_path' => resource_path('payment/cer/weixin/wemp/apiclient_key.pem'),      // XXX: 绝对路径！！！！
                'notify_url' => config('amii.payment.weapp.notifyurl'),       // 你也可以在下单时单独设置来想覆盖它
            ],
        ];

        $easywechat = self::easywechatInit($options);

        return $easywechat;
    }

    /**
     * 公众号支付实例化
     */
    public static function easywechatWemp()
    {
        $options = [
            'debug' => config('amii.easywechat.debug'),
            'app_id' => config('amii.payment.wemp.appid'),
            // 'aes_key' => null, // 可选
            // payment
            'payment' => [
                'merchant_id' => config('amii.payment.wemp.merchantid'),
                'key' => config('amii.payment.wemp.key'),
                'cert_path' => resource_path('payment/cer/weixin/wemp/apiclient_cert.pem'), // XXX: 绝对路径！！！！
                'key_path' => resource_path('payment/cer/weixin/wemp/apiclient_key.pem'),      // XXX: 绝对路径！！！！
                'notify_url' => config('amii.payment.wemp.notifyurl'),       // 你也可以在下单时单独设置来想覆盖它
            ],
        ];

        $easywechat = self::easywechatInit($options);

        return $easywechat;
    }

    /**
     * 创建订单
     */
    public static function order(array $attributes)
    {
        return new EasyWeChatOrder($attributes);
    }

    /**
     * 异步通知
     */
    public static function notify($payment, $provider = 'amii')
    {
        switch ($provider) {
            case 'wechat':
                $response = $payment->handleNotify(function ($notify, $successful) {
                    if ($successful) {
                        // 更改订单状态已支付
                        $paid = app('order')->wechatPaid($notify['out_trade_no']);
                        if ($paid) {
                            return true;
                        }
                    }
                    return false;
                });
                break;
            case 'wemp':
                $response = $payment->handleNotify(function ($notify, $successful) {
                    if ($successful) {
                        // 更改订单状态已支付
                        $paid = app('order')->wempPaid($notify['out_trade_no']);
                        if ($paid) {
                            return true;
                        }
                    }
                    return false;
                });
                break;
            case 'weapp':
                $response = $payment->handleNotify(function ($notify, $successful) {
                    if ($successful) {
                        // 更改订单状态已支付
                        $paid = app('order')->weappPaid($notify['out_trade_no']);
                        if ($paid) {
                            return true;
                        }
                    }
                    return false;
                });
                break;

            default:
                $response = '';
                break;
        }

        return $response;
    }

    /**
     * @param EasyWeChatApplication $application
     * @param string $orderNo 支付订单号
     * @param string $refundNo 售后订单号
     * @param float $totalFee 支付金额
     * @param null|float $refundFee 售后金额
     * @return \EasyWeChat\Support\Collection
     */
    public static function refund(EasyWeChatApplication $application, $orderNo, $refundNo, $totalFee, $refundFee = null)
    {
        $payment = $application->payment;

        $result = $payment->query($refundNo);
        if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS') return $result;

        $totalFee = (int)($totalFee * 100);
        if ($refundFee) $refundFee = (int)($refundFee * 100);
        if ($refundFee > $totalFee) $refundFee = $totalFee;

        $result = $payment->refund($orderNo, $refundNo, $totalFee, $refundFee);
        if ($result->result_code == 'FAIL' && $result->err_code == 'NOTENOUGH') {
            $result = $payment->refund($orderNo, $refundNo, $totalFee, $refundFee, null, 'out_trade_no', 'REFUND_SOURCE_RECHARGE_FUNDS');
        }
        return $result;
    }

    /**
     * 微信企业付款
     * 查询 merchantPay->query($partnerTradeNo)
     * @param EasyWeChatApplication $application
     * @param $partnerTradeNo
     * @param $open_id
     * @param $amount
     * @param $desc
     * @param null $re_user_name
     * @return null
     */
    public static function merchantPaySend(EasyWeChatApplication $application, $partnerTradeNo, $open_id, $amount, $desc, $re_user_name = null)
    {
        $merchantPay = $application->merchant_pay;
        $check_name = 'NO_CHECK';
        if ($re_user_name) {
            $check_name = 'OPTION_CHECK';
        }
        $merchantPayData = [
            'partner_trade_no' => $partnerTradeNo, //随机字符串作为订单号，跟红包和支付一个概念。
            'openid' => $open_id, //收款人的openid
            'check_name' => $check_name,  //文档中有三种校验实名的方法 NO_CHECK OPTION_CHECK FORCE_CHECK
            're_user_name' => $re_user_name,     //OPTION_CHECK FORCE_CHECK 校验实名的时候必须提交
            'amount' => $amount * 100,  //单位为分
            'desc' => $desc,
            'spbill_create_ip' => '127.0.0.1',  //发起交易的IP地址
        ];
        $res = $merchantPay->send($merchantPayData);
        if ($res->return_code == 'SUCCESS' && $res->result_code == 'SUCCESS') {
            return $res;
        }

        \Log::error(self::class . '企业付款失败：[' . $partnerTradeNo . ':' . $open_id . ']', (array)$res);
        return  $res;
    }
}
