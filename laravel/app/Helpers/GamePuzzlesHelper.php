<?php

namespace App\Helpers;

class GamePuzzlesHelper
{
    //计算最优行列数
    public static function countRowCol($puzzles)
    {
        if ($puzzles <= 1) {
            return null;
        }
        if ($puzzles == 6) {
            return  ['row'=> 3,'col' =>2];
        }

        $rowCol = [];
        $initAbs = 'init' ;
        $half = $puzzles / 2;
        for ($i=1; $i <= $half+1; $i++) {
            for ($j=2; $j <= $half+1; $j++) {
                if ($i * $j == $puzzles) {
                    $abs = abs($i - $j);
                    $data = ['row' => $i,'col' => $j];
                    if ($initAbs === 'init') {
                        $initAbs = $abs;
                        $rowCol = $data;
                    } else {
                        if ($abs < $initAbs) {
                            $rowCol = $data;
                            $initAbs = $abs;
                        }
                    }
                    break;
                }
            }
        }
        if (empty($rowCol)) {
            return null;
        }
        return $rowCol;
    }

    public static function countNumber($puzzles)
    {
        if ($puzzles <= 1) {
            return null;
        }
        $number = 0;
        for ($i=1; $i < $puzzles; $i++) {
            if ($i * $i >= $puzzles) {
                $number = $i;
                break;
            }
        }
        if ($number) {
             return ['row' => $number,'col' => $number];
        }

    }

    // 计算块数
    public static function countPuzzles($price)
    {
        if ($price >= 0 && $price <= 50) {
            return 4;
        } elseif ($price >= 51 && $price <= 125) {
            return 6;
        } elseif ($price >= 126 && $price <= 200) {
            return 9;
        } elseif ($price >= 201 && $price <= 275) {
            return 12;
        } elseif ($price >= 276 && $price <= 375) {
            return 16;
        } elseif ($price >= 376 && $price <= 500) {
            return 20;
        } elseif ($price >= 501 && $price <= 650) {
            return 25;
        }
        return 0;
    }
}
