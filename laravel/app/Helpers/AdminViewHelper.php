<?php

namespace App\Helpers;

class AdminViewHelper
{
    /**
     * This class should not be instantiated.
     */
    private function __construct()
    {
    }

    public static function enableHtml($enabled)
    {
        if ($enabled) {
            return '<i class="fa fa-check text-green"></i>';
        } else {
            return '<i class="fa fa-close text-red"></i>';
        }
    }

    public static function checkboxHtml($name, $value, $checked, $text = '')
    {
        $checkedHtml = '';
        if ($checked) {
            $checkedHtml = ' checked="checked" ';
        }

        return '<div class="checkbox">
                    <label class="checkbox-inline checkbox-styled checkbox-primary">
                        <input type="checkbox" name = "'.$name.'" value="'.$value.'"
                            '.$checkedHtml.'>
                        <span>'.$text.'</span>
                    </label>
                </div>';
    }

    public static function radioHtml($name, $values, $current)
    {
        #values: ['value'=>'label']
        $html = '';
        foreach ($values as $k => $v) {
            $checkedHtml = '';
            if ($k == $current) {
                $checkedHtml = ' checked ';
            }
            if ($k == 0) {
                 $status = " radio-danger ";
            } else {
                 $status = " radio-success ";
            }

            $html .= '<div class="radio radio-styled '.$status. '">
            <label>
              <input name="'.$name.'" id="'.$name.'" value="'.$k.'" type="radio" '.$checkedHtml.'>
              <span>'.$v.'</span>
            </label>
            </div>
            ';
        }

        return $html;
    }

    public static function categoryTreeHtml($categories, $brand = null)
    {
        foreach ($categories as $category) {
            echo '<ol class="dd-list">'.PHP_EOL;
            echo '  <li class="dd-item">'.PHP_EOL;
            echo '    <div class="btn btn-default-light dd-button" data-id="'.$category->id.'" ><label class="checkbox-inline checkbox-styled"><input type="checkbox" name="crategories[]" class="checkbox" value="'.$category->id.'"></label><label class="checkbox-inline checkbox-styled">'.$category->id .' '.$category['name'].'</label></div>'.PHP_EOL;
            if (!empty($category->childs)) {
                 echo \App\Helpers\AdminViewHelper::categoryTreeHtml($category->childs);
            }
            echo '  </li>'.PHP_EOL;
            echo '</ol>'.PHP_EOL;
        }
    }

    public static function categorySelectHtml($categories, $brand, $object_categories)
    {

        foreach ($categories as $category) {
                // 没有子类
            if (count($category->childs) < 1) {
                if (\App\Helpers\XShopHelper::checkCategoryExist($object_categories, $category)) {
                    echo '<option value="'.$category->id.'" selected="selected">'.$brand.$category->name.'</option>'.PHP_EOL;
                } else {
                    echo '<option value="'.$category->id.'">'.$brand.$category->name.'</option>'.PHP_EOL;
                }
            } else {
                // 有子类
                if (\App\Helpers\XShopHelper::checkCategoryExist($object_categories, $category)) {
                    echo '<option value="'.$category->id.'" selected="selected">'.$brand.$category->name.'</option>'.PHP_EOL;
                } else {
                    echo '<option value="'.$category->id.'">'.$brand.$category->name.'</option>'.PHP_EOL;
                }
                if ($brand =='') {
                     echo \App\Helpers\AdminViewHelper::categorySelectHtml($category->childs, "&nbsp;&nbsp", $object_categories);
                } else {
                     $brand = "&nbsp;&nbsp;".$brand;
                     echo \App\Helpers\AdminViewHelper::categorySelectHtml($category->childs, $brand, $object_categories);
                }
                echo '</option>'.PHP_EOL;
            }
        }
    }



    public static function attributesSelectHtml($attributes)
    {
        foreach ($attributes as $attribute) {
            echo '<option value="'.$attribute->id.'">'.$attribute->name.'</option>'.PHP_EOL;
        }
    }

    public static function objectSelectHtml($objects, $select_id)
    {
        foreach ($objects as $object) {
            if ($object->id == $select_id) {
                echo '<option value="'.$object->id.'"  selected="selected">'.$object->name.'</option>'.PHP_EOL;
            } else {
                echo '<option value="'.$object->id.'" >'.$object->name.'</option>'.PHP_EOL;
            }
        }
    }

    public static function arraySelectHtml($array, $selectValue)
    {
        foreach ($array as $key => $value) {
            if ($value == $selectValue) {
                echo '<option value="'.$value.'"  selected="selected">'.$key.'</option>'.PHP_EOL;
            } else {
                echo '<option value="'.$value.'" >'.$key.'</option>'.PHP_EOL;
            }
        }
    }
}
