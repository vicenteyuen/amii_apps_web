<?php

namespace App\Helpers;

class OrderHelper
{
    /**
     * 订单状态
     */
    public static function orderStatusStr(array $attributes)
    {
        $str = 'undifine';
        if ($attributes['order_status'] == 0) {
            $str = 'unconfirm';
        } elseif ($attributes['order_status'] == 1) {
            // 正常状态订单
            if ($attributes['pay_status'] == 0) {
                $str = 'paying';
            } elseif ($attributes['pay_status'] == 1) {
                // 已支付订单
                $str = self::unrefundOrderStatus($attributes);
            }
        } elseif ($attributes['order_status'] == 2) {
            $str = 'close';
        } else {
            $str = 'invalid';
        }

        return $str;
    }

    /**
     * 订单展示状态
     */
    public static function orderStatus(array $attributes)
    {
        ## 'order_status',          '订单状态 0未确认 1已确认 2已取消 3无效'
        ## 'shipping_status',       '配送状态 0未发货 1已发货 2已收货 3已拒收'
        ## 'pay_status',            '支付状态 0未付款 1已付款'
        ## 'is_appraise',           '是否评价 0未评价 1已评价'
        ## 'can_refund'             '是否可申请售后 0不可申请 1可申请'
        ## 'is_refund',             '退款状态 0未申请 1已申请 2已同意申请 3已退款 4已拒绝'
        ## 'is_refund_product',     '退货状态 0未申请 1已申请 2已同意申请 3已退货 4已拒绝'

        // 定义订单状态
        $str = 'undifine';

        if ($attributes['order_status'] == 0) {
            $str = 'unconfirm';
        } elseif ($attributes['order_status'] == 1) {
            if ($attributes['pay_status'] == 0) {
                // 待支付
                $str = 'paying';
            } elseif ($attributes['pay_status'] == 1) {
                // 支付完成订单状态
                $str = self::paidOrderStatus($attributes);
            }
        } elseif ($attributes['order_status'] == 2) {
            // 交易关闭
            $str = 'close';
        } elseif ($attributes['order_status'] == 3) {
            // 订单无效
            $str = 'invalid';
        }

        return $str;
    }

    /**
     * 支付完成订单状态
     */
    private static function paidOrderStatus(array $attributes)
    {
        $str = 'paying';
        if ($attributes['is_refund'] == 0) {
            // 未申请售后订单状态
            $str = self::unrefundOrderStatus($attributes);
        } elseif ($attributes['is_refund'] == 1 || $attributes['is_refund'] == 2) {
            // 申请售后中
            $str = 'refunding';
        } elseif ($attributes['is_refund'] == 3) {
            // 售后已完成订单状态
            $str = self::refundOrderStatus($attributes);
        } elseif ($attributes['is_refund'] == 4) {
            // 售后已拒绝订单状态
            $str = self::refundFailOrderStatus($attributes);
        }
        return $str;
    }

    /**
     * 未申请售后订单状态
     */
    private static function unrefundOrderStatus(array $attributes)
    {
        $str = 'unrefund';
        if ($attributes['shipping_status'] == 0) {
            // 待发货
            $str = 'sending';
        } elseif ($attributes['shipping_status'] == 1) {
            // 待收货
            $str = 'receiving';
        } elseif ($attributes['shipping_status'] == 2) {
            if ($attributes['is_appraise'] == 0) {
                // 待评价
                $str = 'appraising';
            } elseif ($attributes['is_appraise'] == 1) {
                // 交易成功
                $str = 'success';
            }
        }
        return $str;
    }

    /**
     * 申请售后订单状态
     */
    private static function refundOrderStatus(array $attributes)
    {
        $str = 'refunding';
        if ($attributes['is_refund_product'] == 0) {
            if ($attributes['is_appraise'] == 1) {
                // 仅退款成功 交易完成 订单关闭
                $str = 'closerefundedpay';
            } else {
                // 仅退款成功
                $str = 'refundpaysuccess';
            }
        } elseif ($attributes['is_refund_product'] == 2) {
            // 退货退款成功 交易失败
            $str = 'failrefunded';
        }
        return $str;
    }

    /**
     * 被拒绝订单状态
     */
    private static function refundFailOrderStatus(array $attributes)
    {
        $str = 'refundfail';
        if ($attributes['is_appraise'] == 1) {
            // 退款拒绝 交易成功
            $str = 'closerefundfail';
        } else {
            // 退款拒绝
            $str = 'refundfail';
        }
        return $str;
    }

    /**
     * 订单状态转为中文
     */
    public static function statusStr($status)
    {
        switch ($status) {
            case 'all':
                $str = '全部';
                break;
            case 'paying':
                $str = '待支付' ;
                break;

            case 'sending':
                 $str = '待发货' ;
                break;

            case 'receiving':
                 $str = '待收货' ;
                break;

            case 'appraising':
                 $str = '待评价' ;
                break;
            case 'cancel':
                $str = '交易关闭';
                break;

            case 'success':
                 $str = '交易完成' ;
                break;

            case 'refunding':
                 $str = '售后中' ;
                break;
            default:
                 $str = '已取消' ;
                break;
        }
        return $str;
    }
}
