<?php

namespace App\Helpers;

use App\Models\GamePushDocument;

class GamePushDocumentHelper
{
    public static function pushGameDocument($userId, $provider, $productName = '', $productSnumber = '', $userName = '', $remainNumber = '', $remainTime = '')
    {
        $push = GamePushDocument::where('provider', $provider)
            ->first();
        if (!$push) {
            return false;
        }
        $str = str_replace('{name}', $userName, $push->desc);
        $str = str_replace('{productName}', $productName, $str);
        $str = str_replace('{productSnumber}', $productSnumber, $str);
        $str = str_replace('{remainTime}', $remainTime, $str);
        $str = str_replace('{number}', $remainNumber, $str);
        // 保存数据，并发送消息推送
        app('amii.notify.message')->sendGameNotifyMsg($userId, $str);
    }
}
