<?php

namespace App\Helpers;

use App\Jobs\OrderRebackInventory;

class OrderJobHelper
{
    /**
     * 未确认订单退回库存
     */
    public static function unconfirm($orderSn)
    {
        $delay = config('amii.order.jobminute.unconfirm') * 60;

        $job = (new OrderRebackInventory('unconfirm', $orderSn))->delay($delay);
        dispatch($job);
    }

    /**
     * 未支付订单退回库存
     */
    public static function cancel($orderSn)
    {
        $delay = config('amii.order.jobminute.cancel') * 60;

        $job = (new OrderRebackInventory('cancel', $orderSn))->delay($delay);
        dispatch($job);
    }

    /**
     * 拼图游戏未完成一天后退回库存
     */
    public static function gameUnconfirm($orderSn)
    {
        $delay = 24 * 60 * 60 + 1;
        $job = (new OrderRebackInventory('unconfirm', $orderSn))->delay($delay);
        dispatch($job);
    }
}
