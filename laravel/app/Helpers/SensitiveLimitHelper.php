<?php

namespace App\Helpers;

class SensitiveLimitHelper
{
    const LIMITWORK = [
     " ",
     "AMY",
     "AMII",
     "REDEFINE",
     "REX",
     "MNFG",
     "KIDS",
     "Manifique",
     "童装",
     "摩尼菲格",
     "艾米",
     "西伍",
     "服饰",
     "极简",
     "石松",
     "陈庆平",
     "西伍服饰",
     "极简服饰",
     "构成服饰",
     "贝壳网络科技",
     "迈尔仕",
     "素云服饰",
     "斯坦德",
     "加百列",
     "KIVU",
    ];

    //昵称检查
    public static function limit($name)
    {
        foreach (self::LIMITWORK as $value) {
            if (preg_match("/(.*)$value(.*)/i", $name)) {
                return false;
            }
        }
        if (!preg_match("/^[0-9A-Z_a-z\x{4e00}-\x{9fa5}]*$/u", $name)) {
            return false;
        }
        return true;
    }
}
