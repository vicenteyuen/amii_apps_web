<?php

namespace Gimage;

use Illuminate\Support\Facades\Config;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Gimage
{



    public static function getName()
    {

        dd(Config::get('gimage'));

        return 'Gimage';

    }

    public static function uploadImage(UploadedFile $uploadedFile, $context, $storage = 'local')
    {
        $config = Config::get('gimage');
        $file = File::get($uploadedFile);
        $extension = $uploadedFile->getClientOriginalExtension();

        $extension = strtolower($extension);

        // error_log('uploadImage extension:'.$extension);

        return app('gimage.processor')->upload(
            $context,
            $file,
            $extension,
            Storage::disk($storage)
        );

    }
}
