<?php

namespace Gimage;

use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Config;
use Gimage\FileManager;

class Processor
{
    protected $config;



    protected $fileManager;


    public function __construct(
        array $config,
        FileManager $fileManager
    ) {
        $this->config = $config;
        $this->fileManager = $fileManager;
    }


    public function upload($context, $file, $extension, FilesystemAdapter $filesystemAdapter)
    {
        // Upload original image
        $fileName = $this->fileManager->setFileSystemAdapter($filesystemAdapter)
            ->uploadFile($file, $extension, $context);
        error_log('uploade image path: '.$fileName);

        try {
            $this->processSizes($file, $fileName, $context, $extension, $filesystemAdapter);
        } catch (\Exception $e) {
            throw $e;
        }

        return $fileName;
    }


    public function rebuild($fileName, $context, FilesystemAdapter $filesystemAdapter)
    {
        // Open original file
        $originalImage = $filesystemAdapter->get('/' . $context . '/' . $fileName);
        $originalImage = $this->openImageHandler->loadImage($originalImage);
        $extension = $this->findExtension($fileName);



        // Rebuild sizes
        $this->deleteSizes($fileName, $context, $filesystemAdapter);
        $this->fileManager->setFileSystemAdapter($filesystemAdapter);
        $this->processSizes($originalImage, $fileName, $context, $extension, $filesystemAdapter);

        return true;
    }




    /**
     * Find file extension
     *
     * @param string $fileName
     *
     * @return string
     */
    protected function findExtension($fileName)
    {
        preg_match('/.[a-Z]{3,4}$/', $fileName, $matches);

        return array_shift($matches);
    }



    public function processSizes($file, $fileName, $context, $extension, FilesystemAdapter $filesystemAdapter)
    {
        if (in_array($extension, $this->config['filetype'])) {
            foreach ($this->config[$context] as $sizeName => $values) {
                //error_log('in processSizes fileName:'.$fileName);

                // open an image file
                $image = \InterImage::make($file);

                if ($values['operation'] == 'resize') {
                    $image->resize($values['width'], $values['height'], function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } elseif ($values['operation'] == 'fit') {
                    $image->fit($values['width'], $values['height'], function ($constraint) {
                        $constraint->upsize();
                    });
                } elseif ($values['operation'] == 'resizeCanvas') {
                    $image->resize($values['width'], $values['height'], function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $image->resizeCanvas($values['width'], $values['height'], 'center', false, 'ffffff');
                } else {
                    error_log('-------- processSizes no operation. --------');
                }


                $this->fileManager->setFileSystemAdapter($filesystemAdapter)
                    ->uploadFile($image->stream($extension), $extension, $context, $sizeName, $fileName);
            }

            return $this;
        } else {
            error_log('gimage filetype is not support. filetype: '. $extension);
            return ;
        }
    }
}
