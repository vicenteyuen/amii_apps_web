<?php

return [
    // operation: ['resize','fit','resizeCanvas']
    // resize 缩放 ，不裁剪
    // resizeCanvas 缩放 ，不裁剪，不够的填充
    // fit: 缩放 ， 裁剪


    'filetype' => ['jpg','png','jpeg','gif'],

    'default' => [
        'small' => [
            'width' => 100,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 600,
            'height' => 600,
            'operation' => 'resize',
        ]
    ]
];
