<?php

namespace Gimage;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider as BaseProvider;
use Gimage\FileManager;



class ServiceProvider extends BaseProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/config/config.php' => config_path('gimage.php'),
            ],
            'gimage'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/config.php',
            'gimage'
        );

        $this->app->bind(
            'gimage.processor',
            function () {
                return new Processor(
                    Config::get('gimage'),
                    new FileManager()
                );
            }
        );

    }
}
