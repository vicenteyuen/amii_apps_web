# Xshop laravel 5.3 EcommerceManage package

## 配置项
```
在根目录下修改composer.json添加
"EcommerceManage\\": "app/Xshop/packages/EcommerceManage/src",
到psr-4

在laravel/app/config/app.php添加
EcommerceManage\EcommerceManageServiceProvider::class
到providers

```

### 包含功能
* [分类功能](#category)
> * [新增分类](/#categoryStore)
> * [更新分类](/#categoryUpdate)
> * [删除分类](/#categoryDelete)
> * [分类列表](/#categorytIndex)

* [品牌功能](#brand)
> * [添加品牌](/#brandStore)
> * [更新品牌](/#brandUpdate)
> * [删除品牌](/#brandDelete)
> * [品牌列表](/#brandIndex)

* [标签功能](#tag)
> * [添加标签](/#tagStore)
> * [更新标签](/#tagUpdate)
> * [删除标签](/#tagDelete)
> * [标签列表](/#tagIndex)