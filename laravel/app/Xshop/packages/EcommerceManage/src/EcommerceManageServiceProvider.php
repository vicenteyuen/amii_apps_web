<?php
namespace EcommerceManage;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class EcommerceManageServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->singleton('category', function ($app) {
            return new Support\CategorySupport;
        });
    }
}
