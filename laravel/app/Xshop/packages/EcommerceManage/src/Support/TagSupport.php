<?php

namespace EcommerceManage\Support;

use Illuminate\Database\Eloquent\SoftDeletes;
use EcommerceManage\Models\Tag;

/**
* Product Manager
*/
class TagSupport
{
    use SoftDeletes;

     /**
     * 添加标签
     * @param array $arr 添加标签
     */
    public function store($arr)
    {
        return Tag::create($arr);

    }

    /**
     * 删除标签
     * @param int $id 标签id
     * @param bool $isTrashed 是否强制删除
     */
    public function delete($id, $isTrashed = false)
    {
        $tag = Tag::find($id);
        if (! $tag) {
            return false;
        }

        if ($isTrashed) {
            return $tag->forceDelete();
        }

        return $tag->delete();
    }

       /**
     * 更新标签
     * @param int $id 更新标签的id
     * @param array $arr 标签修改数据
     */
    public function update($id, $arr)
    {
        return Tag::where("id", $id)
            ->update($arr);
    }

    /**
     * 库存列表
     * @param string $queryFunc sql query func
     * @param string $queryColumn
     * @param string $queryData
     */
    public function index()
    {
        return Tag::paginate(20);
    }
}
