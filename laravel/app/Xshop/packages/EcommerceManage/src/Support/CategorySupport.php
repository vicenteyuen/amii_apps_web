<?php

namespace EcommerceManage\Support;

use Illuminate\Database\Eloquent\SoftDeletes;
use EcommerceManage\Models\Category;
use EcommerceManage\Models\Brand;
use Ecommerce\Models\Product;
use App\Models\BrandCategory;
use App\Models\ProductCategories;
use DB;

/**
* Product Manager
*/
class CategorySupport
{
    use SoftDeletes;

    /**
     * 查找子分类
     * @param  [type] &$arr [description]
     * @param  [type] $id   [description]
     * @return [type]       [description]
     */
    protected function findChild($arr, $parent_id)
    {
        $childs = [];
        foreach ($arr as $key => $value) {
            if ($value['parent_id'] == $parent_id) {
                $childs[] = $value;
            }
        }
        return $childs;
    }
    /**
     * 分类树
     * @param  [type] $rows    [description]
     * @param  [type] $parent_id [description]
     * @return [type]          [description]
     */
    protected function buildTree($rows, $parent_id)
    {
        $childs=$this->findChild($rows, $parent_id);
        if (empty($childs)) {
               return [];
        }
        foreach ($childs as $key => $value) {
            $rescurTree=$this->buildTree($rows, $value['id']);
            if ([] !== $rescurTree) {
                $childs[$key]['childs']=$rescurTree;
            }
        }
        return $childs;
    }

    /**
     * 通过id查找子类id
     * @param  [type] $Id [description]
     * @return [type]     [description]
     */
    protected function findIdChild($categories, &$arr, $id)
    {

        foreach ($categories as $key => $value) {
            if ($id == $value['parent_id']) {
                array_push($arr, $value['id']);
                $id = $value['id'];
                $this->findIdChild($categories, $arr, $id);
            }
        }
    }


     /**
      * 获取无限分类列表
      * @param  [type] $id [description]
      * @return [type]            [description]
      */
    public function index()
    {
        return Category::orderBy('weight', 'desc')
            ->get();
    }

     /**
     * 新增分类
     * @param array $arr 分类数据
     */
    public function store($arr)
    {
        $data = Category::insertGetId($arr);
        return $data;
    }

    /**
     * 更新分类
     * @param int $id 更新分类的id
     * @param array $arr 分类修改数据
     */
    public function update($id, $arr)
    {

        return Category::where("id", $id)
            ->update($arr);
    }



    /**
     * 删除分类
     * @param int $id 分类id
     */
    public function delete($id)
    {
        $category = Category::find($id);
        if (! $category) {
            return false;
        }
        $arr = [$category->id];
        $categories = $this->index();
        $this->findIdChild($categories, $arr, $id);
        return Category::whereIn('id', $arr)
            ->delete();
    }

    /**
     * 通过id查找分类
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function find($id)
    {
        return Category::find($id);
    }

    /**
     * 批量删除分类
     * @param  Array  $categories [description]
     * @return [type]             [description]
     */
    public function deleteCategories($arr)
    {
        $category = Category::whereIn('id', $arr)->get();
        if (! $category) {
            return false;
        }
        $categories = $this->index();
        foreach ($category as $key => $value) {
             $array[$key] = $value->id;
             $this->findIdChild($categories, $array, $value->id);
             $res = Category::whereIn('id', $array)
                    ->delete();
        }
        return $res;
    }

    /**
     * 添加查重
     * @param  [type] $arr [description]
     * @return  bool      [description]
     */
    public function checkAdd($arr)
    {
        $category = Category::where('parent_id', $arr['parent_id'])
            ->where('name', $arr['name'])
            ->first();

        if (is_null($category)) {
            return false;
        }
        return true;
    }
     /**
     * 获取分类树
     * @return [type] [description]
     */
    public function getCategory()
    {
        $datas = $this->index();
        return $this->buildTree($datas, 0);
    }


    /**
     * 品牌分类列表
     * @return [type] [description]
     */
    public function getBrandCatrgory()
    {
        $datas = Category::where('status', 1)
            ->get();
        return $this->buildTree($datas, 0);
    }

    /**
     * 获取分类下分类
     */
    public function getCategoryChildren($category)
    {
        $data = Category::where('parent_id', $category)
            ->get();
        return $data;
    }

    /**
     * 获取分类所属品牌
     */
    public function getCategoryBrands($categoryId)
    {
        return Brand::whereHas('categorys', function ($query) use ($categoryId) {
            return $query->where('categories.id', $categoryId);
        })->get();
    }

    /**
     * 分类展示图
     * @return [type] [description]
     */
    public function productIndexImage($cateId)
    {
        $cate = Category::where('id', $cateId)
            ->first();
        if (! $cate) {
            return '';
        }

        return $cate->img;
    }

    /**
    * 获取分类数据
    */
    public function showCategoryData($id)
    {
        return Category::where('id', $id)
            ->with(['brandProduct' => function ($q1) {
                return $q1->where('status', 1);
            }])
            ->with('brandProduct.brand')
            ->first();

    }

    // 获取分类品牌商品数据
    public function showDetail($categoryId, $brandId, $request = null)
    {
        $productIds = ProductCategories::where('category_id', $categoryId)
                        ->where('brand_id', $brandId)
                        ->pluck('product_id', 'id')
                        ->all();
        $products = Product::whereIn('id', $productIds)
            ->with(['productCategories' => function ($q1) use ($categoryId, $brandId) {
                $q1->where(['brand_id' => $brandId ,'category_id' => $categoryId]);
            }])
            ->paginate(config('amii.apiPaginate'))
            ->appends($request->all());
         return $products;

    }

    // 添加品牌分类图片
    public function updateBrandCateImg($brandCateId, $img)
    {
        $data = BrandCategory::where('id', $brandCateId)->first();

        if (!$data) {
            return false;
        }
        $res =  (clone $data)->update([
                'image' =>  $img,
                ]);
        if (!$res) {
            return false;
        }
        return true;
    }

    // 获取制定品牌分类头图
    public function getBrandCateImg($brandCateId)
    {
        $data = BrandCategory::where('id', $brandCateId)
            ->first();
        if (! $data) {
            return '';
        }
        return $data->image;
    }
}
