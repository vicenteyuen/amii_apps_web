<?php

namespace EcommerceManage\Models;

use App\Helpers\ImageHelper;
use XsBaseDatabase\BaseModel as Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'merchant_id', 'taobao_brand', 'logo', 'recommend', 'status', 'weight','hot'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at_millisecond',
        'created_at',
        'updated_at_millisecond',
        'updated_at',
    ];


    // 修改器
    public function getLogoAttribute()
    {
         return ImageHelper::getImageUrl($this->attributes['logo'], 'brand');
    }

    /**
     * 品牌已有分类
     */
    public function categorys()
    {
        return $this->belongsToMany('EcommerceManage\Models\Category', 'brand_categorys', 'brand_id', 'category_id');
    }

    //中间表category_id 用于编辑
    public function brandCategory()
    {
        return $this->hasMany('App\Models\BrandCategory', 'brand_id', 'id');
    }

    public function brandProduct()
    {
        return $this->hasMany('App\Models\BrandProduct', 'brand_id', 'id');
    }
}
