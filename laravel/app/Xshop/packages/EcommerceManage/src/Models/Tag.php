<?php

namespace EcommerceManage\Models;

use XsBaseDatabase\BaseModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    protected $table = 'tags';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'logo',
        'mark',
        'status',
        'weight',
    ];
}
