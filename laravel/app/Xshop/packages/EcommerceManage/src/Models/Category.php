<?php

namespace EcommerceManage\Models;

// use App\Helpers\UtilsHelper;
// use App\Libraries\Nestedset\NodeTrait;
use XsBaseDatabase\BaseModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\ImageHelper;

class Category extends Model
{
    // use SoftDeletes, NodeTrait;
    use SoftDeletes;

    protected $table = 'categories';
    protected $dates = [
        'deleted_at',
    ];
    protected $guarded = [
        'id',
    ];
    protected $fillable = [
        'name',
        'cate_desc',
        'img',
        'status',
        'weight',
        'parent_id',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '_lft',
        '_rgt',
        'pivot',
        'deleted_at',
        'deleted_at_millisecond',
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];

    // get img link
    public function getImgAttribute()
    {
        if (!$this->attributes['img']) {
            return null;
        }
        return ImageHelper::getImageUrl($this->attributes['img'], 'category', 'small');
    }

    // 关联品牌分类的品牌
    public function brandProduct()
    {
        return $this->hasMany('App\Models\BrandCategory', 'category_id', 'id');
    }

    // brand
    public function brand()
    {
        return $this->belongsToMany('EcommerceManage\Models\Brand', 'brand_categorys', 'category_id', 'brand_id');
    }
    public function brandCategory()
    {
        return $this->hasOne('EcommerceManage\Models\Brand', 'category_id', 'id');
    }
}
