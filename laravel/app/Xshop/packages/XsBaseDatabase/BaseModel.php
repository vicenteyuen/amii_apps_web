<?php

namespace XsBaseDatabase;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    // public static $is_api = false;
    // protected $dateFormat = 'Y-m-d H:i:s';

    // public function setAttribute($key, $value)
    // {
    //     $parent = parent::setAttribute($key, $value);

    //     if ($value && (in_array($key, $this->getDates()) || $this->isDateCastable($key))) {
    //         //因为难以区分传过来的时间究竟是带毫秒还是不带毫秒,于是干脆定为默认的两个字段是从系统获取,自定义的字段从传入的字符串获取
    //         if (in_array($key, ['last_log_time', 'created_at', 'updated_at'])) {
    //             $microtime = microtime(true);
    //             $milliseconds = round(($microtime - floor($microtime)) * 1000);
    //         } else {
    //             $milliseconds = Carbon::parse($value)->micro / 1000;
    //         }
    //         $this->setAttribute($key.'_millisecond', $milliseconds);
    //     }

    //     return $parent;
    // }

    // public function attributesToArray()
    // {
    //     $attributes = $this->getArrayableAttributes();
    //     $new = [];
    //     foreach ($this->getDates() as $key) {
    //         if (! isset($attributes[$key])) {
    //             continue;
    //         }
    //         $attributes[$key] = $this->serializeDate(
    //             $this->asDateTime($attributes[$key])
    //         );
    //         $Iso8601 = Carbon::parse(Carbon::createFromFormat($this->getDateFormat(), $attributes[$key])->toIso8601String());
    //         $milliseconds = $this->getAttribute($key.'_millisecond');
    //         if (self::$is_api) {
    //             $new[$key] = $Iso8601->setTimezone('UTC')->format("Y-m-d\TH:i:s").'.'.sprintf('%03d', $milliseconds).'Z';
    //         } else {
    //             $new[$key] = $Iso8601->setTimezone('UTC')->format('Y-m-d');
    //         }
    //     }
    //     $old = parent::attributesToArray();

    //     return array_merge($old, $new);
    // }

    // public function fromDateTime($value)
    // {
    //     $value = Carbon::parse($value)->setTimezone('Asia/Shanghai');

    //     $format = 'Y-m-d H:i:s';

    //     $value = $this->asDateTime($value);

    //     return $value->format($format);
    // }

    // protected function asDateTime($value)
    // {
    //     // If this value is already a Carbon instance, we shall just return it as is.
    //     // This prevents us having to reinstantiate a Carbon instance when we know
    //     // it already is one, which wouldn't be fulfilled by the DateTime check.
    //     if ($value instanceof Carbon) {
    //         return $value;
    //     }

    //     // If the value is already a DateTime instance, we will just skip the rest of
    //     // these checks since they will be a waste of time, and hinder performance
    //     // when checking the field. We will just return the DateTime right away.
    //     if ($value instanceof DateTime) {
    //         return Carbon::instance($value);
    //     }

    //     // If this value is an integer, we will assume it is a UNIX timestamp's value
    //     // and format a Carbon object from this timestamp. This allows flexibility
    //     // when defining your date fields as they might be UNIX timestamps here.
    //     if (is_numeric($value)) {
    //         return Carbon::createFromTimestamp($value);
    //     }

    //     // If the value is in simply year, month, day format, we will instantiate the
    //     // Carbon instances from that format. Again, this provides for simple date
    //     // fields on the database, while still supporting Carbonized conversion.
    //     if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $value)) {
    //         return Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
    //     }

    //     // Finally, we will just assume this date is in the format used by default on
    //     // the database connection and use that format to create the Carbon object
    //     // that is returned back out to the developers after we convert it here.
    //     try {
    //         return Carbon::createFromFormat($this->getDateFormat(), $value);
    //     } catch (\InvalidArgumentException $e) {
    //         return Carbon::parse($value);
    //     }
    // }
}
