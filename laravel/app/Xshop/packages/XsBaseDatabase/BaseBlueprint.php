<?php

namespace XsBaseDatabase;

use Illuminate\Database\Schema\Blueprint;

class BaseBlueprint extends Blueprint
{
    public function timestamp($column)
    {
        // $this->addColumn('smallInteger', $column.'_millisecond')->nullable();

        return $this->addColumn('timestamp', $column)->nullable();
    }
}
