<?php

namespace XsBaseDatabase;

use Prettus\Repository\Eloquent\BaseRepository as Base;

class BaseRepository extends Base
{
    public function model()
    {
        
    }

    /**
     * 通过id查找
     * @param  int   $id
     * @return mixed collection or null
     */
    public function findById($id)
    {
        return $this->findWhere(['id' => $id])
            ->first();
    }
}
