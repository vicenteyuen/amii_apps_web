# Xshop laravel 5.3 Ecommerce package

## 配置项
```
在根目录下修改composer.json添加
"Ecommerce\\": "app/Xshop/packages/Ecommerce/src",
到psr-4

在laravel/app/config/app.php中

添加
Ecommerce\EcommerceServiceProvider::class,
到providers

```

### 包含功能
* [商品部分](#product)
> * [新增商品](/#productStore)
> * [编辑商品](/#productUpdate)
> * [删除商品](/#productDelete)
> * [获取商品详情(包含一张展示图)](/#productShow)
> * [商品列表](/#productIndex)

* [商品库存](#inventory)
> * [添加库存](/#inventoryStore)
> * [更新库存](/#inventoryUpdate)
> * [删除库存](/#inventoryDelete)
> * [库存列表](/#inventoryIndex)

* [图片功能](#image)
> * [添加图片](/#imageStore)
> * [删除图片(支持批量)](/#imageDelete)
> * [图片列表](/#imageIndex)

* [订单部分](#order)
> * [生成订单](/#orderStore)
> * [订单详情](/#orderShow)
> * [订单列表](/#orderIndex)
> * [删除订单](/#orderDelete)

* [支付部分](#pay)
> * [提交支付](/#payStore)
> * [获取支付状态](/#payStatus)

---
### product
#### productStore
#### productUpdate
#### productDelate
#### productShow
#### productIndex
---
### inventory
#### inventoryStore
#### inventoryUpdate
#### inventoryDelate
---
### order
#### orderStore
#### orderShow
#### orderIndex
#### orderDelate
---
### pay
#### payStore
#### payStatus
---
