<?php
namespace Ecommerce\Repositories;

use XsBaseDatabase\BaseRepository;
use Ecommerce\Models\ProductImag;

class ProductImagRepository extends BaseRepository
{
    public function model()
    {
        return ProductImag::class;
    }
}
