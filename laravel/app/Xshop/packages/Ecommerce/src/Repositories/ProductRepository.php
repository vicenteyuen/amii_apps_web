<?php
namespace Ecommerce\Repositories;

use XsBaseDatabase\BaseRepository;
use Ecommerce\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }
}
