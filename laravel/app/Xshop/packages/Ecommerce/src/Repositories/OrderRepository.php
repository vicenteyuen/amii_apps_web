<?php
namespace Ecommerce\Repositories;

use XsBaseDatabase\BaseRepository;
use Ecommerce\Models\Order;

class OrderRepository extends BaseRepository
{
    public function model()
    {
        return Order::class;
    }
}
