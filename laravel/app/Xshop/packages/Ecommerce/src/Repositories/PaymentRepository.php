<?php
namespace Ecommerce\Repositories;

use XsBaseDatabase\BaseRepository;
use Ecommerce\Models\Payment;

class PaymentRepository extends BaseRepository
{
    public function model()
    {
        return Payment::class;
    }
}
