<?php
namespace Ecommerce\Repositories;

use XsBaseDatabase\BaseRepository;
use Ecommerce\Models\Inventory;

class InventoryRepository extends BaseRepository
{
    public function model()
    {
        return Inventory::class;
    }
}
