<?php

namespace Ecommerce\Supports;

use Ecommerce\Models\Inventory;
use App\Models\SellProduct;
use App\Models\AttributeValue;
use App\Models\InventoryAttributeValue;
use DB;

/**
* Product Manager
*/
class InventorySupport
{
    /**
     * 获取库存填充数据
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function show($arr)
    {
        return Inventory::where($arr)
            ->first();
    }

     /**
     * 添加库存(有属性)
     * @param array $arr 添加库存
     */
    public function store($arr)
    {
        // 添加时多次编辑
        if ($this->check($arr)) {
            DB::beginTransaction();
            $where['value_key'] = $arr['value_key'];
            $where['product_sell_id'] = $arr['product_sell_id'];

             //单条数据的库存
            $oldSingleNumber = Inventory::where($where)
                ->first();

            //编辑之前的总库存
            $oldAllNumber = SellProduct::where('id', $where['product_sell_id'])
                ->first();

             //更新的总库存(剩余库存)
            $number = (int)$oldAllNumber->number - (int)$oldSingleNumber->number + (int)$arr['number'];
            //总库存(不变)
            $inventoryNumber = (int)$oldAllNumber->inventory_number - (int)$oldSingleNumber->number + (int)$arr['number'];
            $updateNumber = SellProduct::where('id', $where['product_sell_id'])
                ->update(['number' => $number,'inventory_number' => $inventoryNumber]);

            // 更新单条库存记录
            $update = Inventory::where($where)
                ->update($arr);

            if ($updateNumber && $updateNumber) {
                DB::commit();
                return true;
            }
            DB::rollback();
            return false;
        }

        DB::beginTransaction();
        //新增库存
        $inventory = Inventory::create($arr);

        //更新总库存
        $update = SellProduct::where('id', $arr['product_sell_id'])
            ->update([
                    'number' => DB::raw('number+'.$arr['number']),
                    'inventory_number' => DB::raw('inventory_number+'.$arr['number'])
            ]);

        // 新增库存属性表
        $inventoryAttribute = explode('_', $arr['value_key']);
        foreach ($inventoryAttribute as $k1 => $v1) {
            $inventoryAttr[$k1]['inventory_id'] = $inventory->id;
            $inventoryAttr[$k1]['attribute_value_id'] = $v1;
        }
        $inventoryAttr = DB::table('inventories_attribute_values')
            ->insert($inventoryAttr);

        if ($inventory && $update && $inventoryAttr) {
            DB::commit();
            return ['status' => 1];
        }
        DB::rollback();
        return ['status' => 0,'msg' =>'新增库存失败'];
    }

    /**
     * 编辑库存
     * @param  [type] $arr   [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function update($arr, $where)
    {

        //编辑之前的总库存
        $oldAllNumber = SellProduct::where('id', $where['product_sell_id'])
            ->first();

        $hasExits = Inventory::where($where)
            ->first();

        // 原先没有该属性记录
        if (!$hasExits) {
            DB::beginTransaction();
            $arr['value_key']       =    $where['value_key'];
            $arr['product_sell_id'] = $where['product_sell_id'];
            $create = Inventory::create($arr);
            $update = DB::table('sell_products')->where('id', $where['product_sell_id'])
                ->update([
                    'number' => DB::raw('number+'.$arr['number']),
                    'inventory_number' => DB::raw('inventory_number+'.$arr['number'])
                ]);
            if ($create && $update) {
                DB::commit();
                return true;
            }
            return false;
        }

        DB::beginTransaction();

        //单条数据的库存
        $oldSingleNumber = Inventory::where($where)
            ->first();

        //更新的总库存
        $number = $oldAllNumber->number - $oldSingleNumber->number + $arr['number'];
        $inventoryNumber = $oldAllNumber->inventory_number - $oldSingleNumber->number + $arr['number'];
        $updateNumber = SellProduct::where('id', $where['product_sell_id'])
            ->update(['number' => $number,'inventory_number' => $inventoryNumber]);
        // 更新单条库存记录
        $update = Inventory::where($where)
            ->update($arr);

        if ($update && $updateNumber) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    /**
     * 删除库存
     * @param int $id 库存id
     * @param bool $isTrashed 是否强制删除
     */
    public function delete($id)
    {
        $inventory = Inventory::find($id);
        if (! $inventory) {
            return false;
        }

        return $inventory->delete();
    }

    /**
     * 查重
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    public function check($arr)
    {
        return Inventory::where('value_key', $arr['value_key'])
            ->where('product_sell_id', $arr['product_sell_id'])
            ->first();
    }


    /**
     * 库存
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function inventory($id)
    {
        return  Inventory::where('product_sell_id', $id)
            ->with('sku.skuValue.attribute')
            ->get();

    }

    /**
     * 库存列表
     * @param string $queryFunc sql query func
     * @param string $queryColumn
     * @param string $queryData
     */
    public function index()
    {
        return Inventory::paginate(config('amii.adminPaginate'));
    }

    /**
     * oms update
     */
    public function omsUpdate($skuCode, $number)
    {
        // 游戏sku
        $gameSkuSign = 'YS001';
        $endSkuStr = substr($skuCode, -5);
        if ($endSkuStr === $gameSkuSign) {
            // 游戏商品扣除已使用库存
            $useNum = app('amii.user.game')->countPlayingBySkuCode($skuCode);
            $number -= $useNum;
            if ($number < 0) {
                $number = 0;
            }

            $findOrCreate = app('sellproduct')->skuAddGameProduct($skuCode, $number);
            if ($findOrCreate['status']) {
                return array(0);
            }
            return array(400101);
        }

        // 普通商品或积分商品
        $inventoryQuery = Inventory::where('sku_code', $skuCode)
            ->whereHas('omsSellProduct', function($query){
                $query->where('provider', 1)
                    ->orWhere('provider', 2);
            });
        $inventorys = $inventoryQuery->get();
        if ($inventorys->isEmpty()) {
            return array(400100);
        }

        // 更新商品库存差值
        $updateNumber = $inventorys[0]->number - $number;

        $updateInventory = $inventoryQuery->update([
            'number' => $number
        ]);
        if (! $updateInventory) {
            return array(400111);
        }

        foreach ($inventorys as $inventory) {
            SellProduct::where('id', $inventory->product_sell_id)
                ->update(['number' => DB::raw('(select sum(number) from inventories where product_sell_id = ' . $inventory->product_sell_id . ')')]);
        }

        return array(0);
    }
}
