<?php

namespace Ecommerce\Supports;

use Ecommerce\Models\Product;
use App\Models\SellProduct;
use App\Models\ProductImage;
use App\Models\UserCollection;
use Illuminate\Http\Request;
use App\Models\UserComment;
use App\Models\UserScanLog;
use App\Models\BrandProduct;
use App\Models\ProductCategories;
use App\Models\UserGame;
use App\Models\GameRelationUser;
use Ecommerce\Models\Inventory;

use DB;
use Auth;

/**
* Product Manager
*/
class ProductSupport
{
    /**
     * 新增商品
     * @param  [type] $arr        [description]
     * @return [type]             [description]
     */
    public function store($arr)
    {
        return Product::create($arr);
    }

    /**
     * 编辑商品
     * @param int $id 更改商品的id
     * @param array $arr 商品修改数据
     */
    public function update($arr, $id)
    {
        $update = Product::where('id', $id)
            ->update($arr);
        if (!$update) {
            return false;
        }
        return $id;
    }

    /**
     * 更新商品运费模版
     * @param  [type] $ids        [description]
     * @param  [type] $templateId [description]
     * @return [type]             [description]
     */
    public function updateTemplate($ids, $templateId)
    {
        return Product::whereIn('id', $ids)
            ->update(['express_template_id' => $templateId]);
    }
    /**
     * 更新商品
     * @param  [type] $arr [description]
     * @param  [type] $id  [description]
     * @return [type]      [description]
     */
    public function updateDetail($arr, $id)
    {
        return Product::where('id', $id)
            ->update($arr);
    }

    /**
     * 上下架商品
     * @param  [type] $arr [description]
     * @param  [type] $id  [description]
     * @return [type]      [description]
     */
    public function updateStatus($arr, $id)
    {
        DB::beginTransaction();
        $updateProduct = Product::where('id', $id)
            ->update($arr);
        $updateSellProduct = SellProduct::whereIn('product_id', [$id])
            ->update($arr);
        if ($updateProduct && $updateSellProduct) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function detail($id)
    {
        return Product::where('id', $id)
            ->with('sellProduct')
            ->first();
    }

    /**
     * 获取商品详情页数据
     */
    public function productInfo($id)
    {
        $product = Product::where('id', $id)
            ->first();
        if ($product) {
            return $product->detail;
        }
        return '';
    }


    /**
     * 搜索商品
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function search($key, $id = null, $categoryId = null, $request = null, $pageNumber = null)
    {
        if (empty($id)) {
            return Product::where('status', 1)
                ->where(function ($q1) use ($key) {
                    $q1->orWhere('name', 'like', '%'.$key.'%')
                       ->orWhere('snumber', 'like', '%'.$key.'%')
                       ->orWhere('id', 'like', '%'.$key.'%');
                    return $q1;
                })->whereHas('sellProduct', function ($q2) {
                    return $q2->where('provider', 1)
                            ->where('status', 1);
                })->doesntHave('sellProduct', 'and', function ($q3) {
                    return $q3->where('provider', 3);
                })
                ->select('id', 'name', 'snumber', 'main_image', 'product_price')
                ->orderBy('created_at', 'desc')
                ->paginate(config('amii.adminPaginate'));
        }

        $pageNumber = $pageNumber ?:config('amii.adminPaginate');

        return BrandProduct::where('brand_products.brand_id', $id)
            ->whereHas('product', function ($q1) use ($id, $categoryId) {
                return $q1->where('status', 1)
                          ->whereDoesntHave('productCategoryOne', function ($q4) use ($id, $categoryId) {
                                return $q4->where('brand_id', $id)
                                    ->where('category_id', $categoryId);
                          });
            })
            ->join('products', 'brand_products.product_id', '=', 'products.id')
            ->where(function ($q1) use ($key) {
                $q1->orWhere('products.name', 'like', '%'.$key.'%')
                   ->orWhere('products.snumber', 'like', '%'.$key.'%');
                return $q1;
            })
            ->select('products.id', 'products.name', 'products.snumber', 'products.main_image', 'products.product_price')
            ->orderBy('products.created_at', 'desc')
            ->paginate($pageNumber)
            ->appends($request->all());
    }

    // 品牌商品搜索
    public function searchBrandProduct($key, $request = null)
    {
        $productIds = BrandProduct::get()
            ->pluck('product_id')
            ->toArray();
        // 查找该品牌下 ，分类下的商品
        return Product::where('status', 1)
                ->whereNotIn('id', $productIds)
                ->where(function ($q1) use ($key) {
                    $q1->orWhere('name', 'like', '%'.$key.'%')
                       ->orWhere('snumber', 'like', '%'.$key.'%');
                    return $q1;
                })
                ->select('id', 'name', 'snumber', 'main_image', 'product_price')
                ->orderBy('created_at', 'desc')
                ->paginate(config('amii.adminPaginate'))
                ->appends($request->all());
    }


    //搜索商品
    public function productSearch($key, $request = null)
    {
        return Product::where(function ($q1) use ($key) {
                $q1->orWhere('name', 'like', '%'.$key.'%')
                       ->orWhere('snumber', 'like', '%'.$key.'%');
                return $q1;
        })
                ->with('sellProduct')
                ->orderBy('created_at', 'desc')
                ->paginate(config('admin.adminPaginate'))
                ->appends($request->all());
    }

     //首单购搜索商品
    public function firstPurchaseSearch($key)
    {
        return Product::where('status', 1)
                ->where(function ($q1) use ($key) {
                    $q1->orWhere('name', 'like', '%'.$key.'%')
                       ->orWhere('snumber', 'like', '%'.$key.'%');
                    return $q1;
                })->whereHas('sellProduct', function ($q2) {
                    return $q2->where('provider', 4)
                        ->where('status', 1);
                })
                ->with('sellProduct')
                ->select('id', 'name', 'snumber', 'main_image', 'product_price')
                ->orderBy('created_at', 'desc')
                ->get();
    }

    public function gameProductSearch($key)
    {
        return Product::where('status', 1)
                ->where(function ($q1) use ($key) {
                    $q1->orWhere('name', 'like', '%'.$key.'%')
                       ->orWhere('snumber', 'like', '%'.$key.'%');
                    return $q1;
                })->whereHas('sellProductOne', function ($q2) {
                    return $q2->where('provider', 1)
                            ->where('status', 1);
                })->with('sellProductOne')
                ->with(['sellProductGame' =>function ($q3) {
                    return $q3->where('provider', 3);
                }])
                ->orderBy('created_at', 'desc')
                ->paginate(config('admin.adminPaginate'));
    }

    /**
     * admin商品列表
     * @return [type] [description]
     */
    public function adminIndex($key, $request, $page = null)
    {
        SellProduct::$allImageEmpty = true;
        $products = Product::where(function ($q1) use ($key) {
            return $q1->orWhere('products.name', 'like', '%'.$key.'%')
                    ->orWhere('products.snumber', 'like', '%'.$key.'%');
        })
            ->with('expressTemplate')
            ->with('sellProduct');

        if ($request->provider) {
            foreach ($request->provider as $value) {
                $products->whereHas('sellProductOne', function ($q) use ($value, $request) {
                    if (isset($request->status) && $request->status != 2) {
                        $q->where('status', $request->status);
                    }
                    return $q->where('provider', $value);
                });
            }
        }

        if ($request->sort) {
            $providerSort = $request->provider[0];
            $products->join('sell_products', 'sell_products.product_id', '=', 'products.id')
            ->where('provider', $providerSort)
            ->select('products.*', 'sell_products.number as numbers')
            ->orderBy('numbers', $request->sort);
        }
        if ($request->templateId) {
            $products->where('express_template_id', $request->templateId);
        }
        if ($request->brandId) {
            $products->whereHas('productBrand', function ($q4) use ($request) {
                return $q4->where('brand_id', $request->brandId);
            });
        }

        // 主图筛选
        if ($request->mainImage) {
            $products->whereHas('sellProductOne', function ($q) use ($request) {
                $q->where('provider', 1);
                // 有主图
                if ($request->mainImage == 1) {
                    return $q->whereNotNull('image')
                        ->orWhere('image', '!=', '');
                }
                // 无主图
                return $q->whereNull('image')
                    ->orWhere('image', '');
            });
        }

        // 时间段搜索
        if ($request->start && $request->end) {
            $products->whereBetween('products.updated_at', [$request->start,$request->end]);
        } elseif ($request->start) {
            $products->where('products.updated_at', '>=', $request->start);
        } elseif ($request->end) {
            $products->where('products.updated_at', '<=', $request->end);
        }

        $products->orderBy('products.id', 'desc');
        $page = $page ?:50;

        return $products->paginate($page)
        ->appends($request->all());
    }

   /**
    * 积分商品列表
    * @param  [type] $provider [description]
    * @return [type]           [description]
    */
    public function pointProduct($request, $provider = 2, $sort = [])
    {
        SellProduct::$allImageEmpty = true;
        $product = SellProduct::with('product')
            ->orderBy('created_at', 'desc')
            ->where('status', '1')
            ->where('provider', $provider)
            ->orderBy('product_point', 'asc');

        if (empty($sort)) {
            return  $product->paginate(config('amii.apiPaginate'))
                        ->appends($request->all());
        }
        $sort = explode('_', $sort);
        // sort($sort, SORT_NUMERIC);
        if ($sort[0] == '60000') {
             $product->where('product_point', '>=', $sort[0]);
        } else {
            $product->whereBetween('product_point', $sort);
        }
        return $product->paginate(config('amii.apiPaginate'))
            ->appends($request->all());

    }

    /**
     * product index
     */
    public function index(Request $request, $provider = 1)
    {
        SellProduct::$allImageEmpty = true;
        $sortArr = explode('_', $request->sort);
        $products = SellProduct::with('product')
            ->select('sell_products.product_id', 'sell_products.image', 'sell_products.base_price', 'sell_products.market_price', 'sell_products.number', 'sell_products.comment_number', 'sell_products.volume', 'sell_products.orderby', 'sell_products.id', 'sell_products.praise', 'sell_products.provider', 'sell_products.puzzles')
            ->where('sell_products.provider', $provider)
            ->where('sell_products.status', 1);

        if ($request->search) {
            $request->search = str_replace('%', '\%', $request->search);
            $searchArr = explode(' ', $request->search);

            // 保存搜索词
            app('hotSearch')->store($searchArr);

            // 商品名搜索
            $products = $products->whereHas('product', function ($query) use ($searchArr) {
                return $query->where(function ($q) use ($searchArr) {
                    foreach ($searchArr as $search) {
                        $q->orWhere('name', 'like', '%' . $search . '%')
                            ->orWhere('snumber', 'like', '%' . $search . '%');
                    }
                });
            });
        }

        if ($request->provider && $request->resource) {
            $resource = $request->resource;
            // 分类筛选
            if ($request->provider == 'category') {
                $products = $products->whereHas('product.productCategories', function ($query) use ($resource) {
                    return $query->where('category_id', $resource)
                        ->where('brand_id', 0);
                });
            }

            // 品牌筛选
            if ($request->provider == 'brand') {
                $products = $products->whereHas('product.brand', function ($query) use ($resource) {
                    return $query->where('brands.id', $resource);
                });
            }

            // 品牌分类筛选
            if ($request->provider == 'brandcate') {
                $products = $products->whereHas('product', function ($query) use ($resource) {
                    return $query->whereHas('productCategory', function ($q) use ($resource) {
                        return $q->whereHas('productCategory', function ($q1) use ($resource) {
                            return $q1->where('id', $resource);
                        })
                        ->whereHas('productBrand', function ($q1) use ($resource) {
                            return $q1->where('id', $resource);
                        });
                    });
                });
            }
        }

        // 筛选
        if ($request->filter) {
            $filterItem = explode('+', $request->filter);
            foreach ($filterItem as $item) {
                $filterArr = explode('_', $item);

                if (isset($filterArr[1])) {
                    $filterValue = $filterArr[1];
                } else {
                    $filterValue = '';
                }

                // 分类筛选
                if ($filterArr[0] == 'category') {
                    $products = $products->whereHas('product.productCategories', function ($query) use ($filterValue) {
                        return $query->where('category_id', $filterValue);
                    });
                }

                // 品牌筛选
                if ($filterArr[0] == 'brand') {
                    $products = $products->whereHas('product.brand', function ($query) use ($filterValue) {
                        return $query->where('brands.id', $filterValue);
                    });
                }

                // 服务筛选
                if ($filterArr[0] == 'service') {
                    $servicesArr = explode(',', $filterArr[1]);
                    // 添加服务筛选
                }
            }
        }

        // 价格筛选
        if ($request->price) {
            $priceItem = explode('+', $request->price);
            foreach ($priceItem as $item) {
                $priceArr = explode('_', $item);
                if (isset($priceArr[1])) {
                    $price = $priceArr[1];

                    if ($priceArr[0] == 'from') {
                        $products = $products->where('base_price', '>=', $price);
                    } elseif ($priceArr[0] == 'to') {
                        $products = $products->where('base_price', '<=', $price);
                    }
                }
            }
        }

        if ($sortArr[0] == 'sell') {
            if (isset($sortArr[1]) && $sortArr[1] == 'asc') {
                $products->orderBy('volume', 'asc');
            } else {
                $products->orderBy('volume', 'desc');
            }
        } elseif ($sortArr[0] == 'new') {
            $products->orderBy('created_at', 'desc');
        } elseif ($sortArr[0] == 'price') {
            if (isset($sortArr[1]) && $sortArr[1] == 'asc') {
                $products->orderBy('base_price', 'asc');
            } else {
                $products->orderBy('base_price', 'desc');
            }
        } else {
            $products->join('products', 'sell_products.product_id', '=', 'products.id')
                ->addSelect(DB::raw('case SUBSTRING(products.snumber, 4, 1) when 6 then 5 when 2 then 4 when 7 then 3 when 1 then 2 else 1 end as default_column_order_by'))
                ->orderBy('default_column_order_by', 'desc')
                ->orderBy('sell_products.id', 'desc');
        }
        return $products->paginate(config('amii.apiPaginate'))
            ->appends($request->all());
    }

    /**
     * api 商品详情
     * @return [type] [description]
     */
    public function productDetail($product_id, $provider = 1)
    {
        inventory::$provider = $provider;
        $sellProduct =  SellProduct::where(['product_id' => $product_id, 'provider' => $provider])
            ->where('status', 1)
            ->with('product.expressTemplate.expressFee')
            ->with('inventories.sku.skuValue.attribute')
            ->with('sellAttributeValue')
            ->first();
        if (!$sellProduct) {
            return false;
        }

        $express_str = '包邮';
         // 游戏商品积分商品包邮
        if ($provider == 2 || $provider == 3) {
             $express_str = '包邮';
        } else {
            if (isset($sellProduct->product->expressTemplate)) {
                if ($sellProduct->product->expressTemplate->free == 0) {
                    if (isset($sellProduct->product->expressTemplate->expressFee)) {
                        $express_str = $sellProduct->product->expressTemplate->expressFee->default_fee;
                    }
                } else {
                    $express_str = '包邮';
                }
            }
        }


        $sellProduct->express_str =  $express_str;

        // 添加浏览记录
        $userId = auth()->id() ?: 0;
        if ($userId && $provider == 1) {
            app('scan')->store(['sell_product_id' =>$sellProduct->id,'user_id' => $userId]);
        }

        $sellProduct->collection_id = UserCollection::where('sell_product_id', $sellProduct->id)
            ->where('user_id', $userId)
            ->value('id');

        $sellProduct->attributes = app('attribute')->productAttribute($sellProduct->id);

        $sellProduct->comment = UserComment::where('product_sell_id', $sellProduct->id)
            ->orderBy('created_at', 'desc')
            ->with('user.level')
            ->with('image')
            ->with('inventory.sku.skuValue.attribute')
            ->first();

        return $sellProduct;
    }

    //游戏详情
    public function productGameDetail($id, $provider = 3)
    {
        $detail =  UserGame::where('id', $id)
            ->with('user')
            ->with('sellProduct.product')
            ->with('sellProduct.inventories.sku.skuValue.attribute')
            ->first();

        if (!$detail) {
            return false;
        }
        $detail->time_to_in = app('amii.game.order')->checkTimesLessThree(auth()->id());
        $detail->sellProduct->attributes =  app('attribute')->productAttribute($detail->sellProduct->id);

        $exits = GameRelationUser::where('user_id', auth()->id())
            ->whereHas('game', function ($q1) {
                return $q1->where('order_status', 1)
                      ->orWhere(function ($q2) {
                        return $q2->where('expired', 0)
                            ->where('success', 0);
                      });
            })
            ->first();

        if ($exits) {
            $detail->canHelp = 0;
        } else {
            $detail->canHelp = 1;
        }

        return $detail;
    }

    /**
     * 积分值
     * @return [type] [description]
     */
    public function pointScore()
    {
        return [
            ['from' => 0,'to' => 5000],
            ['from' => 5000,'to' => 10000],
            ['from' => 10000,'to' => 20000],
            ['from' => 20000,'to' => 40000],
            ['from' => 40000,'to' => 60000],
            ['from' => 60000,'to' => ''],
        ];
    }

    /**
     * 删除分类商品
     * @param int $id 分类id
     */
    public function delCateProduct($id)
    {
        $del1 = BrandProduct::where('product_id', $id)->delete();
        $del2 = product::where('id', $id)->delete();
        if ($del1 && $del2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * product main_iamge
     * @param  [type] $product [description]
     * @return [type]          [description]
     */
    public function getMainImage($productId)
    {
        return Product::where('id', $productId)
            ->value('main_image');
    }

    public function change($productId, $sellProductId)
    {
        $product = Product::where('id', $productId)
            ->first();
        $sellProduct = SellProduct::where('id', $sellProductId)
            ->first();
        $proImage = $product->getOriginal('main_image');
        $sellImage = $sellProduct->getOriginal('image');
        if (!$sellImage) {
            sellProduct::where('product_id', $sellProduct->product_id)
                ->update(['image' => $proImage]);
            return true;
        } else {
            $product->main_image = $sellImage;
            sellProduct::where('product_id', $sellProduct->product_id)
                ->update(['image' => $proImage]);
            $product->save();
            return true;
        }
    }

    public function deleteMainImage($id)
    {
        $product = Product::find($id);
        $product->main_image = '';
        $product->save();
        return true;
    }
}
