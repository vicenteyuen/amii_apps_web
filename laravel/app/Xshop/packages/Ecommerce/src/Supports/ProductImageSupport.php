<?php

namespace Ecommerce\Supportse;

use Ecommerce\Repositories\ProductImageRepository;
use Ecommerce\Models\ProductImage;

/**
* Product Manager
*/
class ProductImageSupport
{
    
    protected $repository;

    public function __construt(ProductImageRepository $repository)
    {
        $this->repository = $repository;

    }
     /**
     * 添加图片
     * @param array $arr 图片数据
     */
    public function store($arr)
    {
        return ProductImage::create($arr);

    }
    public function productShow($id)
    {
        return ProductImage::where('product_id', $id)
            ->where('sell_product_id', 0)
            ->get();
    }

    /**
     * 删除图片(批量删除)
     * @param mixed $id 图片id
     */
    public function delete($id)
    {
        if (is_numeric($id)) {
            $id = [$id];
        } else if (!is_array($id)) {
            return false;
        }
        return ProductImage::whereIn('id', $id)->delete();
    }


    /**
     * 图片列表
     * @param string $queryFunc sql query func
     * @param string $queryColumn
     * @param string $queryData
     */
    public function index($queryFunc, $queryColumn, $queryData)
    {
        // $ProductImages = ProductImage::where('status', 1);
        // return $ProductImages->paginate(20);
        return 1;
    }
}
