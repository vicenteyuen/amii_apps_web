<?php

namespace Ecommerce\Models;

use XsBaseDatabase\BaseModel as Model;
use App\Helpers\UtilsHelper;
use App\Model\SellProduct;
use App\Helpers\ImageHelper;

class Inventory extends Model
{
    protected $table = 'inventories';

    protected $fillable = [
        'product_sell_id',
        'value_key',
        'product_id',
        'mark_price',
        'shop_price',
        'warn_number',
        'status',
        'sku_code',
        'sku_count',
        'sku_sell_count',
        'number',
        'image',
    ];
    protected $hidden = [
        'created_at',
        'created_at_millisecond',
        'updated_at',
        'updated_at_millisecond',
    ];
    protected $appends = [
        'show_price',
    ];

    public static $provider = 1;

    public function getShowPriceAttribute()
    {
        // if (isset($this->attributes['mark_price'])) {
        //     return app('sellproduct')->transferPrice($this->attributes['product_sell_id'], $this->attributes['mark_price']);
        // }
        if(isset($this->attributes['mark_price'])){
            if (static::$provider == 2) {
                return (int)$this->attributes['mark_price'];
            }
            return $this->attributes['mark_price'];
        }
        return 0;
    }

    public function getMarkPriceAttribute($value)
    {
        if (static::$provider == 2) {
             return (int)$value;
        }
        return $value;
    }

    public function getImageAttribute()
    {
        if (!$this->attributes['image']) {
            return null;
        }
        return ImageHelper::getImageUrl($this->attributes['image'], 'product');
    }

    public function SellProduct()
    {
        return $this->hasOne('App\Models\SellProduct', 'id', 'product_sell_id')
            ->where('status', 1);
    }

    /**
     * oms sell product
     */
    public function omsSellProduct()
    {
        return $this->belongsTo('App\Models\SellProduct', 'product_sell_id', 'id');
    }

    public function NormalSellProduct()
    {
        return $this->hasOne('App\Models\SellProduct', 'id', 'product_sell_id');
    }

    /**
     * 库存属性值
     */
    public function sku()
    {
        return $this->hasMany('App\Models\InventoryAttributeValue', 'inventory_id', 'id');
    }
}
