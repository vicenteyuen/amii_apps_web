<?php

namespace Ecommerce\Models;

use XsBaseDatabase\BaseModel as Model;
use App\Helpers\UtilsHelper;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    // use SoftDeletes;

    public $timestamps = false;

    protected $table = 'product_images';
    protected $hidden = [
        'id',
        'product_id',
        'inventory_id',
    ];

    protected $appends = [
         'image',
     ];

    protected $fillable = ['id', 'product_id', 'inventory_id', 'name', 'image'];

    // 修改器
    public function getImagesAttribute($value)
    {
        $value = $this->image;
        $arr = [];
        if ($value) {
            $arr = [
                'small' => UtilsHelper::getImageUrl('product_image', 'small', $value),
                'medium' => UtilsHelper::getImageUrl('product_image', 'medium', $value),
                'large' => UtilsHelper::getImageUrl('product_image', 'large', $value),

            ];
        } else {
            $arr = [
                'small'  => \Config::get('xshop.default_image'),           // __TODO__ 设置占位图
                'medium' => \Config::get('xshop.default_image'),           // __TODO__ 设置占位图
                'large'  => \Config::get('xshop.default_image'),           // __TODO__ 设置占位图
            ];
        }

        return $arr;
    }

    // 属性修改器
    // public function getNameAttribute($value)
    // {
    //     return [
    //         'thumb_img' => UtilsHelper::getImageUrl('product', 'small', $value),
    //         'img'       => UtilsHelper::getImageUrl('product', 'medium', $value)
    //     ];
    // }
}
