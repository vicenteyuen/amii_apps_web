<?php

namespace Ecommerce\Models;

use App\Helpers\SummernoteHelper;
use XsBaseDatabase\BaseModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\ImageHelper;
use App\Models\SellProduct;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $dates = [
        'deleted_at',
    ];

    protected $hidden = [
        'status',
        'user',
        'deleted_at',
        'deleted_at_millisecond',
        'created_at',
        'created_at_millisecond',
    ];

    protected $fillable = [
        'name',
        'sell_point',
        'snumber',
        'number',
        'status',
        'product_price',
        'unit',
        'brief',
        'give_point',
        'detail',
        'main_image',
        'free_status',
        'time_status',
        'price_status',
        'product_price',
        'deepdraw_id',
        'merchant_id',
        'express_template_id',
        'updated_at',
        'updated_at_millisecond',
    ];
    protected $appends = [
        'detail_url',
    ];

    public function getProductPriceAttribute($value)
    {
        // return SellProduct::where('product_id', $this->attributes['id'])
        //     ->where('provider', 1)
        //     ->value('base_price');
        return '0.00';
    }

    // 商品详情做链接处理
    public function getDetailUrlAttribute()
    {
        return url('/apih5/product') . '/' . $this->attributes['id'];
    }

    // 获取主图
    public function getMainImageAttribute()
    {
        if (!$this->attributes['main_image']) {
            return null;
        }
        return ImageHelper::getImageUrl($this->attributes['main_image'], 'product');
    }
    // 获取商品详情
    public function getDetailAttribute()
    {
        return SummernoteHelper::getImgUrl($this->attributes['detail'], 'product');

    }
    public function sellProduct()
    {
        return $this->hasMany('App\Models\SellProduct', 'product_id', 'id');
    }
    public function normalProduct()
    {
        return $this->hasOne('App\Models\SellProduct', 'product_id', 'id')
            ->where('provider', 1);
    }
    public function images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'id');
    }

    public function productCategories()
    {
        return $this->hasMany('App\Models\ProductCategories', 'product_id', 'id');
    }

    public function productCategory()
    {
        return $this->hasMany('App\Models\ProductCategories', 'product_id', 'id');
    }

    public function productCategoryWithoutBrand()
    {
        return $this->hasOne('App\Models\ProductCategories', 'product_id', 'id')->where('brand_id', 0);
    }

    public function productCategoryOne()
    {
        return $this->hasMany('App\Models\ProductCategories', 'product_id', 'id');
    }


    public function expressTemplate()
    {
        return $this->belongsTo('App\Models\ExpressModels\ExpressTemplate', 'express_template_id', 'id');
    }

    public function productBrand()
    {
        return $this->hasMany('App\Models\BrandProduct', 'product_id', 'id');
    }

    public function productBrandOne()
    {
        return $this->hasMany('App\Models\BrandProduct', 'product_id', 'id');
    }

    // brand
    public function brand()
    {
        return $this->belongsToMany('EcommerceManage\Models\Brand', 'brand_products', 'product_id', 'brand_id');
    }

    // categorys
    public function categorys()
    {
        return $this->belongsToMany('EcommerceManage\Models\Category', 'product_categories', 'product_id', 'category_id');
    }

    // 一对一
    public function sellProductOne()
    {
        return $this->hasOne('App\Models\SellProduct', 'product_id', 'id');
    }

    public function sellProductGame()
    {
        return $this->hasOne('App\Models\SellProduct', 'product_id', 'id');
    }
}
