<?php
namespace Ecommerce;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class EcommerceServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        //
    }

    public function register()
    {
        // product
        $this->app->singleton('product', function ($app) {
            return new \Ecommerce\Supports\ProductSupport;
        });

        // inventory
        $this->app->singleton('inventory', function ($app) {
            return new \Ecommerce\Supports\InventorySupport;
        });
    }
}
