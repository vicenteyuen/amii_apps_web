<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use DB;
use Event;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // validator extend phone not include 170
        Validator::extend('phone', function ($attribute, $value, $parameters) {

            if (preg_match('/^170/', $value)) {
                return false;
            }
            if (preg_match('/^171/', $value)) {
                return false;
            }
            if (preg_match('/^152/', $value)) {
                return false;
            }
            if (preg_match('/^156/', $value)) {
                return false;
            }
            if (preg_match('/^145/', $value)) {
                return false;
            }
            if (preg_match('/^147/', $value)) {
                return false;
            }

            if (preg_match('/^1(3[0-9]|4[0-9]|5[012356789]|8[0-9]|7[1-9])\d{8}$/', $value)) {
                return true;
            }

            return false;
        }, '该手机号段禁止注册');

        // validator extend phone include 170
        Validator::extend('phone170', function ($attribute, $value, $parameters) {
            if (preg_match('/^1(3[0-9]|4[0-9]|5[012356789]|8[0-9]|7[1-9])\d{8}$/', $value)) {
                return true;
            }

            return false;
        }, '该手机号段禁止注册');

        // 正式环境强制使用https
        if (! config('app.debug')) {
            \URL::forceSchema('https');
        }


        // dump sql
        if (env('APP_ENV') === 'local') {
            DB::connection()->enableQueryLog();
            Event::listen('kernel.handled', function ($request, $response) {
                if ($request->has('sqldebug')) {
                    $queries = DB::getQueryLog();
                    dd($queries);
                }
            });
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // init
        $this->app->singleton('amii.init', function ($app) {
            return new \App\Services\InitService;
        });

        // version
        $this->app->singleton('version', function ($app) {
            return new \App\Services\VersionService;
        });

        // channel
        $this->app->singleton('channel', function ($app) {
            return new \App\Services\ChannelService;
        });
        // montent
        $this->app->singleton('montent', function ($app) {
            return new \App\Services\MontnetService;
        });
        // leancloud
        $this->app->singleton('leancloud', function ($app, $provider) {
            return new \App\Services\LeancloudService($provider);
        });
        // user
        $this->app->singleton('user', function ($app) {
            return new \App\Services\UserService;
        });
        // token
        $this->app->singleton('token', function ($app) {
            return app(\App\Services\TokenService::class);
        });
        // user collection
        $this->app->singleton('collection', function ($app) {
            return new \App\Services\UserCollectionService;
        });
        // user scan
        $this->app->singleton('scan', function ($app) {
            return new \App\Services\UserScanLogService;
        });
        // user suit
        $this->app->singleton('suit', function ($app) {
            return new \App\Services\UserSuitService;
        });
        // user address
        $this->app->singleton('address', function ($app) {
            return new \App\Services\UserAddressService;
        });
        // sell_product
        $this->app->singleton('sellproduct', function ($app) {
            return new \App\Services\SellProductService;
        });
         // attribute
        $this->app->singleton('attribute', function ($app) {
            return new \App\Services\AttributeService;
        });
        // attribute_value
        $this->app->singleton('attributeValue', function ($app) {
            return new \App\Services\AttributeValueService;
        });
        // inventory_attribute_value
        $this->app->singleton('inventoryAttribute', function ($app) {
            return new \App\Services\InventoryAttributeValueService;
        });
        // image
        $this->app->singleton('imagestore', function ($app) {
            return new \App\Services\ImageService;
        });
         // productAttribute
        $this->app->singleton('productAttribute', function ($app) {
            return new \App\Services\ProductAttributeService;
        });
        // productImage
        $this->app->singleton('productImage', function ($app) {
            return new \App\Services\ProductImageService;
        });
         // sell_attribute
        $this->app->singleton('sellAttribute', function ($app) {
            return new \App\Services\SellAttributeService;
        });
         // sell_attribute_value
        $this->app->singleton('sellAttributeValue', function ($app) {
            return new \App\Services\SellAttributeValueService;
        });
        // banner
        $this->app->singleton('banner', function ($app) {
            return new \App\Services\BannerService;
        });
        // popular
        $this->app->singleton('popular', function ($app) {
            return new \App\Services\PopularService;
        });
        // coupon
        $this->app->singleton('coupon', function ($app) {
            return new \App\Services\CouponService;
        });
        // productCategory
        $this->app->singleton('productCategory', function ($app) {
            return new \App\Services\ProductCategoriesService;
        });
        // order
        $this->app->singleton('order', function ($app) {
            return new \App\Services\OrderService;
        });
        // pay
        $this->app->singleton('pay', function ($app) {
            return new \App\Services\PayService;
        });

        // subject
        $this->app->singleton('subject', function ($app) {
            return new \App\Services\SubjectService;
        });

        // subjectProduct
        $this->app->singleton('subjectProduct', function ($app) {
            return new \App\Services\SubjectProductService;
        });

        // video
        $this->app->singleton('video', function ($app) {
            return new \App\Services\VideoService;
        });

        // physicalStore
        $this->app->singleton('physicalStore', function ($app) {
            return new \App\Services\PhysicalStoreService;
        });

        // order job
        $this->app->singleton('orderjob', function ($app) {
            return new \App\Services\OrderJobService;
        });

        // socialite
        $this->app->singleton('socialite', function ($app) {
            return new \App\Services\SocialiteService;
        });

        // file upload
        $this->app->singleton('fileUpload', function ($app) {
            return new \App\Services\FileUploadService;
        });

        // express
        $this->app->singleton('express', function ($app) {
            return new \App\Services\ExpressService;
        });

        // brand
        $this->app->singleton('brand', function ($app) {
            return new \App\Services\BrandService;
        });

         // productbrand
        $this->app->singleton('brandProduct', function ($app) {
            return new \App\Services\BrandProductService;
        });

        //zhaoshang
        $this->app->singleton('Zhaoshang', function ($app) {
            return new \App\Services\ZhaoshangService;
        });

        // sharerelation
        $this->app->singleton('shareRelation', function ($app) {
            return new \App\Services\ShareRelationService;
        });

        //template
        $this->app->singleton('template', function ($app) {
            return new \App\Services\ExpressTemplateService;
        });
        //setting
        $this->app->singleton('setting', function ($app) {
            return new \App\Services\SettingService;
        });

        //log image
        $this->app->singleton('logImage', function ($app) {
            return new \App\Services\OrderProductRefundImageService;
        });

        //image file
        $this->app->singleton('imageFile', function ($app) {
            return new \App\Services\ImageFileService;
        });

        // point rank award
        $this->app->singleton('point.rank.award', function ($app) {
            return new \App\Services\PointRankAwardService;
        });

        //game push document
        $this->app->singleton('game.push.document', function ($app) {
            return new \App\Services\GamePushDocumentService;
        });
    }
}
