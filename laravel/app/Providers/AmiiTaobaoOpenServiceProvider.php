<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AmiiTaobaoOpenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // taobao open image store
        $this->app->singleton('amii.taobao.open.image', function ($app) {
            return new \App\Services\TaobaoTop\ImageService;
        });
        // taobao open job
        $this->app->singleton('amii.taobao.open.job', function ($app) {
            return new \App\Services\TaobaoTop\JobService;
        });
        // taobao open sync log
        $this->app->singleton('amii.taobao.open.sync.log', function ($app) {
            return new \App\Services\TaobaoTop\SyncLogService;
        });
        // taobao open product
        $this->app->singleton('amii.taobao.open.product', function ($app) {
            return new \App\Services\TaobaoTop\ProductService;
        });
        // taobao open category
        $this->app->singleton('amii.taobao.open.category', function ($app) {
            return new \App\Services\TaobaoTop\CategoryService;
        });

        // taobao open product amii
        $this->app->singleton('taobao.open.product.amii', function ($app) {
            return new \App\Services\AmiiTaobaoTop\ProductService;
        });
        // taobao open product handle amii
        $this->app->singleton('taobao.open.product.handle.amii', function ($app) {
            return new \App\Services\AmiiTaobaoTop\ProductHandleService;
        });
        // taobao open category amii
        $this->app->singleton('taobao.open.category.amii', function ($app) {
            return new \App\Services\AmiiTaobaoTop\CategoryService;
        });
        // taobao open sku amii
        $this->app->singleton('taobao.open.sku.amii', function ($app) {
            return new \App\Services\AmiiTaobaoTop\SkuService;
        });
        // taobao open attrvalue img amii
        $this->app->singleton('taobao.open.attrvalue.img.amii', function ($app) {
            return new \App\Services\AmiiTaobaoTop\AttrValueImgService;
        });
        // taobao open sell point amii
        $this->app->singleton('taobao.open.sell.point.amii', function ($app) {
            return new \App\Services\AmiiTaobaoTop\SellPointService;
        });
    }
}
