<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * 服务提供者加是否延迟加载.
     *
     * @var bool
     */
    // protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('jsend', function ($app) {
            return new \App\Services\ResponseService;
        });

        // user_comment
        $this->app->singleton('userComment', function ($app) {
            return new \App\Services\UserCommentService;
        });

        // cart
        $this->app->singleton('cart', function ($app) {
            return new \App\Services\CartService;
        });

        // user_enable
        $this->app->singleton('userEnable', function ($app) {
            return new \App\Services\UserEnableService;
        });
        // point_type
        $this->app->singleton('pointType', function ($app) {
            return new \App\Services\PointTypeService;
        });
        // point_recored
        $this->app->singleton('pointRecored', function ($app) {
            return new \App\Services\PointRecoredService;
        });
        // enable
        $this->app->singleton('enable', function ($app) {
            return new \App\Services\EnableService;
        });
        // point_level
        $this->app->singleton('pointLevel', function ($app) {
            return new \App\Services\PointLevelService;
        });
        // tribe
        $this->app->singleton('tribe', function ($app) {
            return new \App\Services\TribeService;
        });
        // gain
        $this->app->singleton('gain', function ($app) {
            return new \App\Services\GainService;
        });

         // hot search
        $this->app->singleton('hotSearch', function ($app) {
            return new \App\Services\HotSearchService;
        });

        // user search
        $this->app->singleton('userSearch', function ($app) {
            return new \App\Services\UserSearchService;
        });
        // point_received
        $this->app->singleton('pointReceived', function ($app) {
            return new \App\Services\PointReceivedService;
        });

         // balance
        $this->app->singleton('balance', function ($app) {
            return new \App\Services\BalanceService;
        });

        // user_question
        $this->app->singleton('question', function ($app) {
            return new \App\Services\UserQuestionService;
        });

        // user_answer
        $this->app->singleton('answer', function ($app) {
            return new \App\Services\UserAnswerService;
        });

        // user_answer
        $this->app->singleton('commentRank', function ($app) {
            return new \App\Services\CommentRankService;
        });

        // user_answer
        $this->app->singleton('gameAdvice', function ($app) {
            return new \App\Services\UserGameAdviceService;
        });

        // today_task
        $this->app->singleton('todayTask', function ($app) {
            return new \App\Services\TodayTaskService;
        });

        // share CopyWriting
        $this->app->singleton('share', function ($app) {
            return new \App\Services\ShareService;
        });
         // ponintRank
        $this->app->singleton('pointRank', function ($app) {
            return new \App\Services\PointsRankService;
        });
    }
}
