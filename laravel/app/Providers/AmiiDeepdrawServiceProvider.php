<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AmiiDeepdrawServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // 定时任务
        $this->app->singleton('amii.amiiDeepdraw.cron', function ($app, $step) {
            return new \App\Services\AmiiDeepdraw\CronService($step);
        });
        // 队列
        $this->app->singleton('amii.amiiDeepdraw.job', function ($app) {
            return new \App\Services\AmiiDeepdraw\JobService;
        });
        // deepdraw product item
        $this->app->singleton('amii.amiiDeepdraw.product.item', function ($app) {
            return new \App\Services\AmiiDeepdraw\ProductItemService;
        });
        // html 处理
        $this->app->singleton('amii.amiiDeepdraw.html', function ($app) {
            return new \App\Services\AmiiDeepdraw\HtmlService;
        });
        // image 处理
        $this->app->singleton('amii.amiiDeepdraw.image', function ($app) {
            return new \App\Services\AmiiDeepdraw\ImageService;
        });
        // merchant
        $this->app->singleton('amii.deepdraw.merchant', function ($app) {
            return new \App\Services\Deepdraw\MerchantService;
        });
        // product
        $this->app->singleton('amii.deepdraw.product', function ($app) {
            return new \App\Services\Deepdraw\ProductService;
        });
        // inventory
        $this->app->singleton('amii.deepdraw.inventory', function ($app) {
            return new \App\Services\Deepdraw\InventoryService;
        });
        // category
        $this->app->singleton('amii.deepdraw.category', function ($app) {
            return new \App\Services\Deepdraw\CategoryService;
        });
        // amii category
        $this->app->singleton('amii.amiiDeepdraw.category', function ($app) {
            return new \App\Services\AmiiDeepdraw\CategoryService;
        });
        // amii deepdraw product
        $this->app->singleton('amii.amiiDeepdraw.product', function ($app) {
            return new \App\Services\AmiiDeepdraw\ProductService;
        });
        // amii deepdraw merchant
        $this->app->singleton('amii.amiiDeepdraw.merchant', function ($app) {
            return new \App\Services\AmiiDeepdraw\MerchantService;
        });
        // amii deepdraw inventory
        $this->app->singleton('amii.amiiDeepdraw.inventory', function ($app) {
            return new \App\Services\AmiiDeepdraw\InventoryService;
        });
        // amii deepdraw brand
        $this->app->singleton('amii.amiiDeepdraw.brand', function ($app) {
            return new \App\Services\AmiiDeepdraw\BrandService;
        });
    }
}
