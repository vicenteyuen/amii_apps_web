<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AmiiFirstPurchaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // first purchase
        $this->app->singleton('amii.fisrt.product', function ($app) {
            return new \App\Services\FirstPurchaseService;
        });

        // give product
        $this->app->singleton('amii.give.product', function ($app) {
            return new \App\Services\GiveProductService;
        });

        // first purchase text
        $this->app->singleton('amii.purchase.text', function ($app) {
            return new \App\Services\FirstPurchaseTextService;
        });

        //give product history
        $this->app->singleton('amii.give.history.product', function ($app) {
            return new \App\Services\GiveProductHistoryService;
        });
    }
}
