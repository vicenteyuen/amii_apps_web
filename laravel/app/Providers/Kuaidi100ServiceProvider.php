<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class Kuaidi100ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // kuaidi100
        $this->app->singleton('kuaidi100', function ($app) {
            return new \App\Services\Kuaidi100Service;
        });
    }
}
