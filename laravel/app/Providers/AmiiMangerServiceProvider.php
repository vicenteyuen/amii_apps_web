<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AmiiMangerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // brand
        $this->app->singleton('amii.manger.brand', function ($app) {
            return new \App\Services\BrandService;
        });
        // brand category
        $this->app->singleton('amii.manger.brand.category', function ($app) {
            return new \App\Services\BrandCategoryService;
        });
        //category
        $this->app->singleton('amii.manger.category', function ($app) {
            return new \App\Services\CategoryService;
        });

        // amii product service
        $this->app->singleton('amii.product.service', function ($app) {
            return new \App\Services\ProductServiceService;
        });

        // user coupon
        $this->app->singleton('amii.manger.user.coupon', function ($app) {
            return new \App\Services\UserCouponService;
        });

        // order refund
        $this->app->singleton('amii.order.refund', function ($app) {
            return new \App\Services\OrderRefundService;
        });
        $this->app->singleton('amii.order.refund.log', function ($app) {
            return new \App\Services\OrderRefundLogService;
        });
        $this->app->singleton('amii.order.refund.image', function ($app) {
            return new \App\Services\OrderRefundImageService;
        });
        $this->app->singleton('amii.order.refundment', function ($app) {
            return new \App\Services\OrderRefundmentService;
        });
        $this->app->singleton('amii.order.refund.product', function ($app) {
            return new \App\Services\OrderRefundProductService;
        });

        // notify message
        $this->app->singleton('amii.notify.message', function ($app) {
            return new \App\Services\NotifyMessageService;
        });

        // point order
        $this->app->singleton('amii.point.order', function ($app) {
            return new \App\Services\PointOrderService;
        });

        // feedback
        $this->app->singleton('amii.feedback', function ($app) {
            return new \App\Services\FeedbackService;
        });

        // individ
        $this->app->singleton('amii.individ', function ($app) {
            return new \App\Services\IndividService;
        });

        // qrcode
        $this->app->singleton('amii.qrcode', function ($app) {
            return new \App\Services\QrcodeService;
        });

        // activity
        $this->app->singleton('amii.activity', function ($app) {
            return new \App\Services\ActivityService;
        });

        // activity rule
        $this->app->singleton('amii.activity.rule', function ($app) {
            return new \App\Services\ActivityRuleService;
        });
    }
}
