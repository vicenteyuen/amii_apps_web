<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AmiiOmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // oms
        $this->app->singleton('amii.oms.aes', function ($app) {
            return new \App\Services\Oms\AesService;
        });
        // oms request
        $this->app->singleton('amii.oms.request', function ($app) {
            return new \App\Services\Oms\OmsRequestService;
        });
        // oms response
        $this->app->singleton('amii.oms.response', function ($app) {
            return new \App\Services\Oms\OmsResponseService;
        });
        // oms order
        $this->app->singleton('amii.oms.order', function ($app) {
            return new \App\Services\Oms\OmsOrderService;
        });
        // oms product
        $this->app->singleton('amii.oms.product', function ($app) {
            return new \App\Services\Oms\OmsProductService;
        });
    }
}
