<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AmiiGameServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services
     * @return void
     */
    public function register()
    {
        // game_relation_users
        $this->app->singleton('amii.game.relation.user', function ($app) {
            return new \App\Services\GameRelationUserService;
        });

        // user_game
        $this->app->singleton('amii.user.game', function ($app) {
            return new \App\Services\UserGameService;
        });

        // game order
        $this->app->singleton('amii.game.order', function ($app) {
            return new \App\Services\GameOrderService;
        });
    }
}
