<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Console\Commands\AmiiDeepdrawSync;
use App\Console\Commands\AmiiTaobaoAttrValueImg;
use App\Console\Commands\AmiiTaobaoSellPoint;
use App\Console\Commands\AmiiCheckUserLevel;
use App\Console\Commands\AmiiLastMonthPointRank;
use App\Console\Commands\AmiiCheckUserShoppingPoint;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,

        // 立即开启同步deepdraw数据
        // 停止使用深绘数据
        // AmiiDeepdrawSync::class,

        // 处理卖点数据
        AmiiTaobaoAttrValueImg::class,
        // 处理属性值图数据
        AmiiTaobaoSellPoint::class,

        // 会员积分矫正
        AmiiCheckUserLevel::class,


        //上月积分排名
        AmiiLastMonthPointRank::class,

        //用户购物积分矫正
        AmiiCheckUserShoppingPoint::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (config('amii.cron')) {
            // $schedule->command('inspire')
            //          ->hourly();
            //
            $schedule->call(function () {
                app('tribe')->currentRank('week');
                app('tribe')->currentRank('month');
                app('tribe')->cumulateRank();
                app('tribe')->updatelastUserRank();
            })->daily();

            $schedule->call(function () {
                app('tribe')->lastRank('week');
            })->weekly();

            $schedule->call(function () {
                app('tribe')->lastRank('month');
                app('pointRank')->lastRank('month');
                app('pointRank')->lastMonthRewardJob();
            })->monthly();

            $schedule->call(function () {
                app('pointRank')->lastRank('year');
            })->yearly();

            $schedule->call(function () {
                app('pointRank')->currentRank('month');
                app('pointRank')->currentRank('year');
            })->hourly();
            // deepdraw
            // 停止使用深绘数据
            // $schedule->call(function () {
            //     if (config('amii.deepdraw.sync')) {
            //         app('amii.amiiDeepdraw.cron')->handle();
            //         // 分类
            //         app('amii.amiiDeepdraw.cron')->handleCategory();
            //     } else {
            //         \Log::error('please set DEEPDRAW_SYNC value true of env');
            //     }
            // });

            if (! config('app.debug')) {
                // 一天一次同步淘宝商品
                $schedule->call(function () {
                    app('amii.taobao.open.sync.log')->daliySync();
                })->dailyAt('2:00');
            }

            // 长期无效库存商品下架
            $schedule->call(function () {
                app('sellproduct')->autoDlist();
            })->dailyAt('1:00');
        }
    }
}
