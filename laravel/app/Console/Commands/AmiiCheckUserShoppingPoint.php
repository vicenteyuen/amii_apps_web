<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AmiiCheckUserShoppingPoint extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amiiuspoint';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'amii check user shopping point';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         app('pointReceived')->checkShoppingPoint();
    }
}
