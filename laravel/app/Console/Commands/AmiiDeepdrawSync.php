<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AmiiDeepdrawSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
        amiideep
        {--s|step=all : step of sync[all, merchant, calender, product, handle, publish, category]}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'amii sync deepdraw products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // check deepdraw path exist?
        $deepdrawPath = public_path() . '/uploads/deepdraw/';
        if (! is_dir($deepdrawPath)) {
            $this->info($deepdrawPath . ' path is not exist, create it. ');
            mkdir($deepdrawPath, 0755, true);
        }

        // start cron
        if (config('amii.deepdraw.sync')) {
            $this->info('------- start deepdraw sync -------');

            // get step
            $step = self::formatStep();

            app()->make('amii.amiiDeepdraw.cron', [$step])->handle();

            $this->info('------- syncing, wait for moment -------');
        } else {
            $this->info('please set DEEPDRAW_SYNC value true of env');
        }
    }

    /**
     * 校验step参数
     */
    private function formatStep()
    {
        switch ($this->option('step')) {
            case 'merchant':
                $step = 'merchant';
                break;
            case 'calender':
                $step = 'calender';
                break;
            case 'product':
                $step = 'product';
                break;
            case 'handle':
                $step = 'handle';
                break;
            case 'publish':
                $step = 'publish';
                break;
            case 'category':
                $step = 'category';
                break;

            default:
                $step = 'all';
                break;
        }

        return $step;
    }
}
