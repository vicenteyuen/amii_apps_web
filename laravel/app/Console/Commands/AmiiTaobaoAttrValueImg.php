<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AmiiTaobaoAttrValueImg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amiitb:attrvalueimg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'handle synced taobao product attrvalue img';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app('taobao.open.attrvalue.img.amii')->commandHandle();
    }
}
