<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AmiiLastMonthPointRank extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amiilmupointrank';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'amii user last month user point rank';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app('pointRank')->lastMonthRewardJob();
    }
}
