let mix = require('laravel-mix').mix;
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
let networkInterfaces = require('os').networkInterfaces(), IPv4;
if (networkInterfaces.en0) {
    for (i of networkInterfaces.en0) {
        if (i.family == 'IPv4') IPv4 = i.address;
    }
} else {
    IPv4 = '127.0.0.1';
}

let $assets_wechat_path = 'resources/assets/wechat/';
mix.setPublicPath('public')
    .js($assets_wechat_path + 'app.js', 'assets/wechat/js')
    .sass($assets_wechat_path + 'assets/scss/app.scss', 'assets/wechat/css')
    .sourceMaps(false)
    .extract([
        'vue',
        'vuex',
        'vue-router',
        'axios',
        'promise.prototype.finally',
        'array.prototype.find',
        'array.prototype.findindex',
    ])
    .webpackConfig({
        output: {
            publicPath: process.argv.includes('--hot')?'http://'+IPv4+':8080/':'/',
        },
        resolve: {
            alias: {
                '/api': path.resolve(__dirname, $assets_wechat_path+'api'),
                '/assets': path.resolve(__dirname, $assets_wechat_path+'assets'),
                '/components': path.resolve(__dirname, $assets_wechat_path+'components'),
                '/config': path.resolve(__dirname, $assets_wechat_path+'config'),
                '/modules': path.resolve(__dirname, $assets_wechat_path+'modules'),
                '/directives': path.resolve(__dirname, $assets_wechat_path+'directives'),

                'axios': 'axios/dist/axios.min.js',
            },
        },
        devServer: {
            host: '0.0.0.0',
        },
    })
    .options({
        imgLoaderOptions: {
            enabled: process.env.NODE_ENV === 'production',
            pngquant: {
                quality: 70,
                floyd: 0.5,
                speed: 10
            },
        },
    });

if(process.env.NODE_ENV === 'production'){
    mix.version();
}