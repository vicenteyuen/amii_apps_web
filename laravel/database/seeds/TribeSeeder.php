<?php

use Illuminate\Database\Seeder;
use App\Models\Tribe;
use App\Models\Gain;

class TribeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        Tribe::create([
            'user_id'  => 1,
            'pid' => 0,
            'income' => 100,
        ]);

        Tribe::create([
            'user_id'  => 2,
            'pid' => 1,
            'income' => 200,
        ]);

        Tribe::create([
            'user_id'  => 3,
            'pid' => 1,
            'income' => 300,
        ]);
        Tribe::create([
            'user_id'  => 4,
            'pid' => 2,
            'income' => 300,
        ]);
        Tribe::create([
            'user_id'  => 5,
            'pid' => 3,
            'income' => 400,
        ]);
        Tribe::create([
            'user_id'  => 6,
            'pid' => 0
        ]);
        Tribe::create([
            'user_id'  => 7,
            'pid' => 6
        ]);
        Tribe::create([
            'user_id'  => 8,
            'pid' => 2
        ]);
        Tribe::create([
            'user_id'  => 9,
            'pid' => 1
        ]);
        Tribe::create([
            'user_id'  => 10,
            'pid' => 3
        ]);

        Gain::create([
            'user_id' => 2,
            'p_user_id' => 1,
            'income'   => 20,
            'total_orders' => 10,
        ]);
        Gain::create([
            'user_id' => 2,
            'p_user_id' => 1,
            'income'   => 20,
            'total_orders' => 10,

        ]);
        Gain::create([
            'user_id' => 3,
            'p_user_id' => 1,
            'income'   => 20,
            'total_orders' => 10,
        ]);
        Gain::create([
            'user_id' => 4,
            'p_user_id' => 2,
            'income'   => 10,
            'total_orders' => 10,
        ]);
        Gain::create([
            'user_id' => 5,
            'p_user_id' => 3,
            'income'   => 200,
            'total_orders' => 10,
        ]);
        Gain::create([
            'user_id' => 7,
            'p_user_id' => 6,
            'income'   => 800,
            'total_orders' => 400,
        ]);
        Gain::create([
            'user_id' => 9,
            'p_user_id' => 1,
            'income'   => 10,
            'total_orders' => 10,
        ]);
        Gain::create([
            'user_id' => 8,
            'p_user_id' => 2,
            'income'   => 1000,
            'total_orders' => 200,
        ]);
      
    }
}
