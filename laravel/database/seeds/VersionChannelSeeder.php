<?php

use Illuminate\Database\Seeder;
use App\Models\VersionChannel;

class VersionChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VersionChannel::truncate();
        VersionChannel::create(['channel' => 'mobileApp','desc' => '默认']);
        VersionChannel::create(['channel' => 'stage','desc' => '测试环境']);
        VersionChannel::create(['channel' => 'production','desc' => '多服务器环境']);
        VersionChannel::create(['channel' => 'wandoujia','desc' => '豌豆荚']);
        VersionChannel::create(['channel' => 'jifeng','desc' => '机锋市场']);
        VersionChannel::create(['channel' => 'yingyonghui','desc' => '应用汇']);
        VersionChannel::create(['channel' => 'qihu360','desc' => '360手机助手']);
        VersionChannel::create(['channel' => 'sogou','desc' => '搜狗手机助手']);
        VersionChannel::create(['channel' => 'xiaomi','desc' => '小米应用中心']);
        VersionChannel::create(['channel' => 'jiuyi','desc' => '91手机商城']);
        VersionChannel::create(['channel' => 'baidu','desc' => '百度手机助手']);
        VersionChannel::create(['channel' => 'huawei','desc' => '华为智云汇']);
        VersionChannel::create(['channel' => 'oppo','desc' => 'OPPO NearMe']);
        VersionChannel::create(['channel' => 'yingyongbao','desc' => '腾讯应用宝']);
        VersionChannel::create(['channel' => 'flyme','desc' => '魅族商店']);
        VersionChannel::create(['channel' => 'wostore','desc' => '沃商店']);
        VersionChannel::create(['channel' => 'coolapk','desc' => '酷安']);
        VersionChannel::create(['channel' => 'lenovo','desc' => '联想']);
        VersionChannel::create(['channel' => 'mumayi','desc' => '木蚂蚁']);
        VersionChannel::create(['channel' => 'anzhi','desc' => '安智市场']);
        VersionChannel::create(['channel' => 'hiapk','desc' => '安卓市场']);
        VersionChannel::create(['channel' => 'uc','desc' => '阿里应用分发平台']);
        VersionChannel::create(['channel' => 'vivo','desc' => 'VIVO渠道']);
    }
}
