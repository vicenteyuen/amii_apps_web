<?php

use Illuminate\Database\Seeder;
use App\Models\NotifyMessage;

class NotifyMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 100; $i++) {
            $provider = mt_rand(0, 1) ? 'notify' : 'express';
            NotifyMessage::create([
                'status'    => mt_rand(0, 2),
                'user_id'   => 1,
                'title'     => $provider . '消息标题',
                'provider'  => $provider,
                'content'   => $provider . '消息内容',
                'type'      => mt_rand(0, 2),
                'resource'  => mt_rand(1, 50)
            ]);
        }
    }
}
