<?php

use Illuminate\Database\Seeder;
use App\Models\CommentRank;

class CommentRankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommentRank::create(['rank' => 1,'desc' => '一星']);
        CommentRank::create(['rank' => 2,'desc' => '二星']);
        CommentRank::create(['rank' => 3,'desc' => '三星']);
        CommentRank::create(['rank' => 4,'desc' => '四星']);
        CommentRank::create(['rank' => 5,'desc' => '五星']);
        
    }
}
