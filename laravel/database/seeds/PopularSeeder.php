<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;
use App\Models\Popular;

class PopularSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 5; $i++) {
            Banner::create([
                'status'    => 1,
                'title'     => '轮播图' . $i,
                'provider'  => mt_rand(1, 2),
                'resource'  => mt_rand(1, 10),
                'mobile_image' => "/static/bannerMobile/".$i.".jpg",
                'app_image' => "/static/bannerApp/".$i.".jpg",
                'weapp_image' => "/static/bannerWeapp/".$i.".jpg",
            ]);
        }

        // amii
        Popular::create([
            'status'    => 1,
            'parent_id' => 0,
            'title'     => 'AMII',
            'descript'  => '极简 不动声色',
            'is_click'  => 0,
            'image' => "/static/popular/1.jpg",
            'weapp_image' => '/static/popularWeapp/1.jpg'
        ]);
        // amii
        Popular::create([
            'status'    => 1,
            'parent_id' => 0,
            'title'     => 'AMII REDFINE',
            'descript'  => '东 方 极 简',
            'is_click'  => 0,
            'image' => "/static/popular/2.jpg",
            'weapp_image' => '/static/popularWeapp/2.jpg'
        ]);
        // amii
        Popular::create([
            'status'    => 1,
            'parent_id' => 0,
            'title'     => '摩尼菲格',
            'descript'  => '诗意/自由/随性',
            'is_click'  => 0,
            'image' => "/static/popular/3.jpg",
            'weapp_image' => '/static/popularWeapp/3.jpg'
        ]);
        // amii
        Popular::create([
            'status'    => 1,
            'parent_id' => 0,
            'title'     => 'AMII童装',
            'descript'  => '极简 初见的美',
            'is_click'  => 0,
            'image' => "/static/popular/4.jpg",
            'weapp_image' => '/static/popularWeapp/4.jpg'
        ]);

        # id --> 1
        $parentId = 1;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => 'T恤/背心',
            'is_click'  => 0,
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '裙装',
            'is_click'  => 0,
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '裤装',
            'is_click'  => 0,
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '针织衫',
            'is_click'  => 0,
        ]);
        # id --> 2
        $parentId = 2;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '毛衣',
            'is_click'  => 0,
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '裤装',
            'is_click'  => 0,
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '裙装',
            'is_click'  => 0,
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '衬衫',
            'is_click'  => 0,
        ]);
        # id --> 3
        $parentId = 3;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '连衣裙',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '毛针织衫',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '外套',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '上衣',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id --> 4
        $parentId = 4;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '亲子装',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '秋冬特价',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '当季热卖',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '春装新品',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id --> 5
        $parentId = 5;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => 'T恤',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '卫衣',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id -> 6
        $parentId = 6;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '连衣裙',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id -> 7
        Popular::create([
            'status'    => 1,
            'parent_id' => 7,
            'title'     => '牛仔裤',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => 7,
            'title'     => '长裤',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => 7,
            'title'     => '打底裤',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id -> 8
        Popular::create([
            'status'    => 1,
            'parent_id' => 8,
            'title'     => '短裤',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => 8,
            'title'     => '中长款',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id -> 9
        $parentId = 9;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '短款毛衣',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '中长款毛衣',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '套头毛衣',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '高领毛衣',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id -> 10
        Popular::create([
            'status'    => 1,
            'parent_id' => 10,
            'title'     => '阔腿裤',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => 10,
            'title'     => '打底裤',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id -> 11
        $parentId = 11;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '长袖连衣裙',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '棉麻连衣裙',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '背心连衣裙',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        # id -> 12
        $parentId = 12;
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '长款衬衫',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '长袖衬衫',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '短袖衬衫',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);
        Popular::create([
            'status'    => 1,
            'parent_id' => $parentId,
            'title'     => '纯色衬衫',
            'is_click'  => 1,
            'provider'  => mt_rand(1, 3),
            'resource'  => mt_rand(1, 2)
        ]);

        Popular::fixTree();
    }
}
