<?php

use Illuminate\Database\Seeder;
use App\Models\ShareType;

class ShareTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShareType::truncate();
        ShareType::create(['provider_id' => 1,'desc' => '单品页']);
        ShareType::create(['provider_id' => 2,'desc' => '社区专题']);
        ShareType::create(['provider_id' => 3,'desc' => '社区视频']);
        ShareType::create(['provider_id' => 4,'desc' => '用户二维码']);
        ShareType::create(['provider_id' => 5,'desc' => '用户部落']);
        ShareType::create(['provider_id' => 6,'desc' => '首单7折活动']);
        ShareType::create(['provider_id' => 7,'desc' => '拼图送衣游戏']);
        ShareType::create(['provider_id' => 8,'desc' => '拼图送衣游戏首页']);
        ShareType::create(['provider_id' => 9,'desc' => '红包分享']);
        ShareType::create(['provider_id' => 10,'desc' => '首页分享']);
    }
}
