<?php

use Illuminate\Database\Seeder;
use App\Models\AddressInfo;
use App\Models\Province;
use App\Models\City;
use App\Models\Area;

class NewAddressInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::truncate();
        City::truncate();
        Area::truncate();
        $province = json_decode(file_get_contents(base_path().'/resources/views/admin/address/province.json'), true);
        $city = json_decode(file_get_contents(base_path().'/resources/views/admin/address/city.json'), true);
        $area = json_decode(file_get_contents(base_path().'/resources/views/admin/address/area.json'), true);
        // province
        foreach ($province as $k1 => $v1) {
            Province::create([
              'province_id'    => $v1['id'],
              'name'           => $v1['fullname'],
              'lat'            => $v1['location']['lat'],
              'lng'            => $v1['location']['lng'],
            ]);
        }
        // city
        foreach ($city as $k2 => $v2) {
            $pro = substr($v2['id'], 0, 2).'0000';
            City::create([
              'city_id'     => $v2['id'],
              'name'        => $v2['fullname'],
              'province_id' => $pro,
              'lat'         => $v2['location']['lat'],
              'lng'         => $v2['location']['lng'],
            ]);
        }
        // area
        foreach ($area as $k3 => $v3) {
            $city = substr($v3['id'], 0, 4).'00';
            Area::create([
              'area_id'     => $v3['id'],
              'name'        => $v3['fullname'],
              'city_id'     => $city,
              'lat'         => $v3['location']['lat'],
              'lng'         => $v3['location']['lng'],
            ]);
        }
    }
}
