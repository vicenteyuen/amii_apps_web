<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'key' => 'subject',
            'value'=> '1',
        ]);

        Setting::create([
            'key' => 'video',
            'value'=> '1',
        ]);

        Setting::create([
            'key' => 'store',
            'value'=> '1',
        ]);
    }
}
