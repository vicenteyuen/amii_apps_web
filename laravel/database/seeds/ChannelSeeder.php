<?php

use Illuminate\Database\Seeder;
use App\Models\Channel;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Channel::create(['name' => '默认', 'code' => 'app.amii.com']);
    }
}
