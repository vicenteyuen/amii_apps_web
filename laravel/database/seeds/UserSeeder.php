<?php

use Illuminate\Database\Seeder;
use App\Helpers\AmiiHelper;
use App\Models\User;
use App\Models\Token;
use Faker\Factory;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $passwordString = '123456';
        $faker = Factory::create('zh_CN');

        User::create([
            'nickname'  => 'demo',
            'uuid'      => AmiiHelper::newUuid(),
            'password'  => bcrypt($passwordString),
            'phone'     => '15115115151',
            'sum_points'=> '10000',
            'points'    =>  '5000',
            'level'     =>  rand(1, 9),
        ]);

        for ($i = 0; $i < 10; $i++) {
            $sumPoints = rand(0, 10000);
            User::create([
                'nickname'  => $faker->userName,
                'uuid'      => AmiiHelper::newUuid(),
                'password'  => bcrypt($passwordString),
                'phone'     => $faker->phoneNumber,
                'birthday'  => '1995-04-20',
                'realname'  => 'user'.$i,
                'sum_points'=> $sumPoints,
                'points'    => (int)rand(0, 10000)%$sumPoints,
                'level'     => rand(1, 9),
            ]);
        }

        Token::create([
            'type'      => 1,
            'ref_id'    => 1,
            'code'      => 1,
        ]);
    }
}
