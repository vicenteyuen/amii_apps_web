<?php

use Illuminate\Database\Seeder;
use App\Models\FirstPurchase;
use App\Models\GiveProductHistory;

class FirstPurchaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FirstPurchase::create([
            'money' => 300,
            'sell_product_id' => 1,
            'status' => 1,
        ]);

        GiveProductHistory::create([
            'sell_product_id' => 1,
        ]);
    }
}
