<?php

use Illuminate\Database\Seeder;
use App\Models\ExpressModels\Express;
use App\Models\ExpressModels\ExpressFee;

class ExpressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 添加快递公司
        $arr = [];

        $arr[] = ['id'=>1,'name'=>'上门提货','com'=>'ZT','status'=>0,];
        $arr[] = ['id'=>2,'name'=>'EMS广东省','com'=>'GDEMS','status'=>0,];
        $arr[] = ['id'=>3,'name'=>'中通快递','com'=>'ZTO','status'=>0,];
        $arr[] = ['id'=>4,'name'=>'宅急送','com'=>'ZJS','status'=>0,];
        $arr[] = ['id'=>5,'name'=>'韵达快运','com'=>'YUNDA','status'=>0,];
        $arr[] = ['id'=>6,'name'=>'圆通速递','com'=>'YTO','status'=>0,];
        $arr[] = ['id'=>7,'name'=>'uc物流','com'=>'uc','status'=>0,];
        $arr[] = ['id'=>8,'name'=>'天天快递','com'=>'TT','status'=>0,];
        $arr[] = ['id'=>9,'name'=>'速尔快递','com'=>'suer','status'=>0,];
        $arr[] = ['id'=>10,'name'=>'申通快递','com'=>'STO','status'=>0,];
        $arr[] = ['id'=>11,'name'=>'申通快递2','com'=>'STKD','status'=>0,];
        $arr[] = ['id'=>12,'name'=>'顺丰快递','com'=>'SF','status'=>0,];
        $arr[] = ['id'=>13,'name'=>'如风达','com'=>'RFD','status'=>0,];
        $arr[] = ['id'=>14,'name'=>'其它','com'=>'QT','status'=>0,];
        $arr[] = ['id'=>15,'name'=>'全峰','com'=>'QF','status'=>0,];
        $arr[] = ['id'=>16,'name'=>'邮政小包','com'=>'POSTB','status'=>0,];
        $arr[] = ['id'=>17,'name'=>'快捷快递','com'=>'KJ','status'=>0,];
        $arr[] = ['id'=>18,'name'=>'京东物流','com'=>'JD','status'=>1,];
        $arr[] = ['id'=>19,'name'=>'百世快递','com'=>'HTKY','status'=>0,];
        $arr[] = ['id'=>20,'name'=>'国通快递','com'=>'GTKD','status'=>0,];
        $arr[] = ['id'=>21,'name'=>'美国E邮宝','com'=>'EMS_ZX_ZX_US','status'=>0,];
        $arr[] = ['id'=>22,'name'=>'EMS','com'=>'EMS','status'=>0,];
        $arr[] = ['id'=>23,'name'=>'德邦','com'=>'deppon','status'=>0,];
        $arr[] = ['id'=>24,'name'=>'当当货到付款','com'=>'DDCOD','status'=>0,];
        $arr[] = ['id'=>25,'name'=>'邮政','com'=>'chinapost','status'=>0,];
        $arr[] = ['id'=>26,'name'=>'百世优派','com'=>'BYP','status'=>0,];
        $arr[] = ['id'=>27,'name'=>'百世汇通','com'=>'BESTEX','status'=>1,];

        Express::insert($arr);
    }
}
