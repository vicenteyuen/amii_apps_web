<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if (config('app.debug')) {
            // debug seed
            // $this->call(UserSeeder::class);
            $this->call(PopularSeeder::class);
            // $this->call(ProductSeeder::class);
            $this->call(CouponSeeder::class);
            $this->call(TribeSeeder::class);
            $this->call(CommunitySeeder::class);
            $this->call(NotifyMessageSeeder::class);
            $this->call(SearchSeeder::class);
            $this->call(PointReceivedSeeder::class);
            $this->call(FirstPurchaseSeeder::class);
        }
        // seed
        $this->call(ProductServiceSeeder::class);
        $this->call(ExpressSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(AddressInfoSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(CommentRankSeeder::class);
        $this->call(SuitSizeSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(SizeSeeder::class);
        $this->call(EnableSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(ProductServiceServiceSeeder::class);
        $this->call(NewAddressInfoSeeder::class);
        $this->call(VersionChannelSeeder::class);
        $this->call(ShareTypeSeeder::class);
        $this->call(LevelEnableSeeder::class);
        $this->call(GamePushDocumentSeeder::class);
        $this->call(ChannelSeeder::class);
        $this->call(TodayTaskSeeder::class);
    }
}
