<?php

use Illuminate\Database\Seeder;
use App\Models\TodayTask;

class TodayTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TodayTask::truncate();
        // 购物送积分
        TodayTask::create([
            'type'  => 'shopping',
            'title' => '购物送积分',
            'content' => '在A+商城购物,即获得积分',
            'image' => '/static/todayTask/shopping.png',
            'jump_url' => '/wechat/mall',
            'task_points' => [],
        ]);

        // 邀请好友
        TodayTask::create([
            'type'  => 'invite',
            'title' => '邀请好友',
            'content' => '邀请朋友得500积分!还可获得10%佣金',
            'image' => '/static/todayTask/invite.png',
            'jump_url' => '/wechat/my/share?uuid=',
            'task_points' => [200],
        ]);

        // 转发分享
        TodayTask::create([
            'type'  => 'share',
            'title' => '转发分享',
            'content' => '分享活动、专题、视频、商品评论、专属二维码等',
            'image' => '/static/todayTask/share.png',
            'jump_url' => '/wechat/community/games',
            'task_points' => [15,25],
        ]);

        // 身体管理
        TodayTask::create([
            'type'  => 'figure',
            'title' => '身体管理',
            'content' => '填写完整身材信息,为您推荐尺码和搭配喔',
            'image' => '/static/todayTask/figure.png',
            'jump_url' => '/wechat/settings/figure',
            'task_points' => [105],
        ]);

        // 购物评论
        TodayTask::create([
            'type'  => 'shop_comment',
            'title' => '购物评论',
            'content' => '确认收货后填写商品评论',
            'image' => '/static/todayTask/shop_comment.png',
            'jump_url' => '/wechat/my/orders/appraising',
            'task_points' => [5,10],
        ]);

        // 社区点赞&评论
        TodayTask::create([
            'type'  => 'community_subject',
            'title' => '社区点赞&评论',
            'content' => '社区专题/视频点赞、评论',
            'image' => '/static/todayTask/community.png',
            'jump_url' => '/wechat/community/topics',
            'task_points' => [5],
        ]);


    }
}
