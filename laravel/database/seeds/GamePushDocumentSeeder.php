<?php

use Illuminate\Database\Seeder;

use App\Models\GamePushDocument;

class GamePushDocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GamePushDocument::truncate();
        // 进行中文案
        GamePushDocument::create([
            'desc'      => '{name}已为你点亮1块拼图,还差{number}块，要继续加油哦~',
            'status'    => 1,
            'provider'  => 1,
        ]);

        // 进行中，未完成
        GamePushDocument::create([
            'desc'      => '亲,你发起的{productName}({productSnumber})拼图游戏剩余{remainTime}小时,剩余{number}块,抓紧时间了哟~',
            'status'    => 1,
            'provider'  => 2,
            'hours'     => 2,
        ]);

        // 未合成
        GamePushDocument::create([
            'desc'      => '亲,你发起的{productName}({productSnumber})拼图游戏剩余{remainTime}小时,抓紧时间合成获取美衣哟~',
            'status'    => 1,
            'provider'  => 3,
            'hours'     => 5,
        ]);

        // 完成需合成
        GamePushDocument::create([
            'desc'      => '亲,{productName}({productSnumber})的拼图已经全部点亮,请及时合成美衣！超过96小时未合成,将会自动失效哦~',
            'status'    => 1,
            'provider'  => 4,
        ]);

         // 超时 未完成
        GamePushDocument::create([
            'desc'      => '非常遗憾，您发起的{productName}({productSnumber})的拼图游戏因超时未完成，已自动失效~',
            'status'    => 1,
            'provider'  => 5,
        ]);

         // 超时 未合成
        GamePushDocument::create([
            'desc'      => '非常遗憾，您发起的{productName}({productSnumber})的拼图游戏因超时未合成，已自动失效~',
            'status'    => 1,
            'provider'  => 6,
        ]);
    }
}
