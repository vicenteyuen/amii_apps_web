<?php

use Illuminate\Database\Seeder;
use App\Models\UserEnable;
use App\Models\Enable;
use App\Models\PointLevel;
use App\Models\PointRecored;
use App\Models\LevelEnable;

class EnableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 等级 特权
        //
        PointLevel::create([
          'name'   => 'V0',
          'points' =>  0
         ]);
        PointLevel::create([
          'name'   => 'V1',
          'points' =>  1000
         ]);

        PointLevel::create([
          'name'   => 'V2',
          'points' =>  5000
         ]);

        PointLevel::create([
          'name'   => 'V3',
          'points' =>  10000
         ]);

        PointLevel::create([
          'name'   => 'V4',
          'points' =>  20000
         ]);

        PointLevel::create([
          'name'   => 'V5',
          'points' =>  50000
         ]);

        PointLevel::create([
          'name'   => 'V6',
          'points' =>  150000
         ]);

        PointLevel::create([
          'name'   => 'V7',
          'points' =>  400000
         ]);

        PointLevel::create([
          'name'   => 'V8',
          'points' =>  800000
         ]);


        for ($i = 0; $i<40; $i++) {
            if ($i % 2 == 0) {
                $provider = 'order';
                $position = '+';
            } else {
                $provider = 'else';
                $position = "-";
            }
            if ($provider == 'else') {
                $type = rand(1, 2);
            } else {
                $type = 0;
            }
            $points = $position.rand(1, 3000);
            PointRecored::create([
                'provider' => $provider,
                'type'     => $type,
                'user_id'  =>  1,
                'points'   => $points,
            ]);
        }

        Enable::create([
            "name" => '会员专享价',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/vip.png'),
        ]);

        Enable::create([
            "name" => '极速退款',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/refund.png'),
        ]);

        Enable::create([
            "name" => '生日积分特权',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/birthday.png'),
        ]);

        // Enable::create([
        //     "name" => '生日折扣特权',
        // ]);
        // Enable::create([
        //     "name" => '包邮特权',

        // ]);
        // Enable::create([
        //     "name" => '退货特权',
        // ]);
       
        // Enable::create([
        //     "name" => '活动专享',
        // ]);

        // v1
        LevelEnable::create([
            'level_id'  => 2,
            'enable_id' => 3,
            'get'       => '1000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 2,
        //     'enable_id' => 4,
        //     'get'       => '90',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 2,
        //     'enable_id' => 5,
        //     'get'       => '1',
        //     'describe'  => '包邮次数(年度)',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 2,
        //     'enable_id' => 6,
        //     'get'       => '1',
        //     'describe'  => '退货次数(年度)',
        // ]);

        //v2
        LevelEnable::create([
            'level_id'  => 3,
            'enable_id' => 3,
            'get'       => '2000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 3,
        //     'enable_id' => 4,
        //     'get'       => '90',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 3,
        //     'enable_id' => 5,
        //     'get'       => '2',
        //     'describe'  => '包邮次数(年度)',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 3,
        //     'enable_id' => 6,
        //     'get'       => '2',
        //     'describe'  => '退货次数(年度)',
        // ]);

        // LevelEnable::create([
        //     'level_id'  => 3,
        //     'enable_id' => 7,
        //     'get'       => '1.1',
        //     'describe'  => '活动积分加倍',
        // ]);

        //v3
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 1,
            'get'       => '99',
            'describe'  => '会员折扣',
        ]);
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 2,
            'get'       => '100',
            'describe'  => '退款额度',
        ]);
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 3,
            'get'       => '2000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 4,
        //     'enable_id' => 4,
        //     'get'       => '90',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 4,
        //     'enable_id' => 5,
        //     'get'       => '3',
        //     'describe'  => '包邮次数(年度)',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 4,
        //     'enable_id' => 6,
        //     'get'       => '3',
        //     'describe'  => '退货次数(年度)',
        // ]);

        // LevelEnable::create([
        //     'level_id'  => 4,
        //     'enable_id' => 7,
        //     'get'       => '1.1',
        //     'describe'  => '活动积分加倍',
        // ]);

        //v4
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 1,
            'get'       => '98',
            'describe'  => '会员折扣',
        ]);
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 2,
            'get'       => '300',
            'describe'  => '退款额度',
        ]);
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 3,
            'get'       => '3000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 5,
        //     'enable_id' => 4,
        //     'get'       => '85',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 5,
        //     'enable_id' => 5,
        //     'get'       => '4',
        //     'describe'  => '包邮次数(年度)',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 5,
        //     'enable_id' => 6,
        //     'get'       => '4',
        //     'describe'  => '退货次数(年度)',
        // ]);

        // LevelEnable::create([
        //     'level_id'  => 5,
        //     'enable_id' => 7,
        //     'get'       => '1.2',
        //     'describe'  => '活动积分加倍',
        // ]);


        //v5
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 1,
            'get'       => '98',
            'describe'  => '会员折扣',
        ]);
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 2,
            'get'       => '500',
            'describe'  => '退款额度',
        ]);
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 3,
            'get'       => '3000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 6,
        //     'enable_id' => 4,
        //     'get'       => '85',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 6,
        //     'enable_id' => 5,
        //     'get'       => '5',
        //     'describe'  => '包邮次数(年度)',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 6,
        //     'enable_id' => 6,
        //     'get'       => '5',
        //     'describe'  => '退货次数(年度)',
        // ]);

        // LevelEnable::create([
        //     'level_id'  => 6,
        //     'enable_id' => 7,
        //     'get'       => '1.2',
        //     'describe'  => '活动积分加倍',
        // ]);

         //v6
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 1,
            'get'       => '97',
            'describe'  => '会员折扣',
        ]);
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 2,
            'get'       => '1000',
            'describe'  => '退款额度',
        ]);
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 3,
            'get'       => '3000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 7,
        //     'enable_id' => 4,
        //     'get'       => '85',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 7,
        //     'enable_id' => 5,
        //     'get'       => '8',
        //     'describe'  => '包邮次数(年度)',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 7,
        //     'enable_id' => 6,
        //     'get'       => '8',
        //     'describe'  => '退货次数(年度)',
        // ]);

        // LevelEnable::create([
        //     'level_id'  => 7,
        //     'enable_id' => 7,
        //     'get'       => '1.3',
        //     'describe'  => '活动积分加倍',
        // ]);


        //v7
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 1,
            'get'       => '96',
            'describe'  => '会员折扣',
        ]);
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 2,
            'get'       => '2000',
            'describe'  => '退款额度',
        ]);
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 3,
            'get'       => '5000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 8,
        //     'enable_id' => 4,
        //     'get'       => '80',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 8,
        //     'enable_id' => 5,
        //     'get'       => '10',
        //     'describe'  => '包邮次数(年度)',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 8,
        //     'enable_id' => 7,
        //     'get'       => '10',
        //     'describe'  => '退货次数(年度)',
        // ]);

        // LevelEnable::create([
        //     'level_id'  => 8,
        //     'enable_id' => 7,
        //     'get'       => '1.4',
        //     'describe'  => '活动积分加倍',
        // ]);

        //v8
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 1,
            'get'       => '95',
            'describe'  => '会员折扣',
        ]);
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 2,
            'get'       => '5000',
            'describe'  => '退款额度',
        ]);
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 3,
            'get'       => '5000',
            'describe'  => '生日获取积分',
        ]);
        // LevelEnable::create([
        //     'level_id'  => 9,
        //     'enable_id' => 4,
        //     'get'       => '80',
        //     'describe'  => '生日折扣',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 9,
        //     'enable_id' => 5,
        //     'get'       => '包邮次数(年度)',
        //     'describe'  => '',
        // ]);
        // LevelEnable::create([
        //     'level_id'  => 9,
        //     'enable_id' => 6,
        //     'get'       => 'no_reason',
        //     'describe'  => '退货次数(年度)',
        // ]);

        // LevelEnable::create([
        //     'level_id'  => 9,
        //     'enable_id' => 7,
        //     'get'       => '1.4',
        //     'describe'  => '活动积分加倍',

        // ]);


     
    }
}
