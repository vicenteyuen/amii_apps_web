<?php

use Illuminate\Database\Seeder;
use App\Models\Coupon;
use App\Models\CouponGrant;
use App\Models\UserCoupon;
use Carbon\Carbon;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Coupon::create([
            'status'    => 1,
            'orderby'   => 1,
            'cid'       => 'coupon1',
            'title'     => '20元现金券',
            'number'    => 100,
            'remain'    => 100,
            'provider'  => 1,
            'value'     => 20,
            'lowest'    => 0,
            'start'     => Carbon::now()->subDay(10),
            'end'       => Carbon::now()->addDay(60),
        ]);
        Coupon::create([
            'status'    => 1,
            'orderby'   => 1,
            'cid'       => 'coupon2',
            'title'     => '20元现金券',
            'number'    => 100,
            'remain'    => 100,
            'provider'  => 1,
            'value'     => 20,
            'lowest'    => 0,
            'start'     => Carbon::now()->subDay(10),
            'end'       => Carbon::now()->addDay(30),
        ]);
        Coupon::create([
            'status'    => 1,
            'orderby'   => 1,
            'cid'       => 'coupon3',
            'title'     => '20元现金券',
            'number'    => 100,
            'remain'    => 100,
            'provider'  => 1,
            'value'     => 20,
            'lowest'    => 0,
            'start'     => Carbon::now()->subDay(10),
            'end'       => Carbon::now()->addDay(50),
        ]);
        Coupon::create([
            'status'    => 1,
            'orderby'   => 1,
            'cid'       => 'coupon4',
            'title'     => '满200减50',
            'number'    => 100,
            'remain'    => 100,
            'provider'  => 2,
            'value'     => 50,
            'lowest'    => 200,
            'start'     => Carbon::now()->subDay(10),
            'end'       => Carbon::now()->addDay(50),
        ]);
        Coupon::create([
            'status'    => 1,
            'orderby'   => 1,
            'cid'       => 'coupon5',
            'title'     => '满200减50',
            'number'    => 100,
            'remain'    => 100,
            'provider'  => 2,
            'value'     => 50,
            'lowest'    => 200,
            'start'     => Carbon::now()->subDay(10),
            'end'       => Carbon::now()->addDay(40),
        ]);
        Coupon::create([
            'status'    => 1,
            'orderby'   => 1,
            'cid'       => 'coupon6',
            'title'     => '满200减50',
            'number'    => 100,
            'remain'    => 100,
            'provider'  => 2,
            'value'     => 50,
            'lowest'    => 200,
            'start'     => Carbon::now()->subDay(30),
            'end'       => Carbon::now()->addDay(60),
        ]);
        // Coupon::create([
        //     'status'    => 1,
        //     'orderby'   => 1,
        //     'cid'       => 'coupon7',
        //     'title'     => '8折券满300可用最高抵扣100元',
        //     'number'    => 100,
        //     'remain'    => 100,
        //     'provider'  => 3,
        //     'value'     => 100,
        //     'discount'  => 80,
        //     'lowest'    => 300,
        //     'start'     => Carbon::now()->subDay(60),
        //     'end'       => Carbon::now()->subDay(30),
        // ]);
        // Coupon::create([
        //     'status'    => 1,
        //     'orderby'   => 1,
        //     'cid'       => 'coupon8',
        //     'title'     => '8折券满300可用最高抵扣100元',
        //     'number'    => 100,
        //     'remain'    => 100,
        //     'provider'  => 3,
        //     'value'     => 100,
        //     'discount'  => 80,
        //     'lowest'    => 300,
        //     'start'     => Carbon::now()->subDay(3),
        //     'end'       => Carbon::now()->addDay(30),
        // ]);
        // Coupon::create([
        //     'status'    => 1,
        //     'orderby'   => 1,
        //     'cid'       => 'coupon9',
        //     'title'     => '8折券满300可用最高抵扣100元',
        //     'number'    => 100,
        //     'remain'    => 100,
        //     'provider'  => 3,
        //     'value'     => 100,
        //     'discount'  => 80,
        //     'lowest'    => 300,
        //     'start'     => Carbon::now()->addDay(30),
        //     'end'       => Carbon::now()->addDay(60),
        // ]);

        // 发行优惠券
        // 现金券
        for ($i=1; $i <= 3; $i++) {
            for ($j=1; $j < 100; $j++) {
                if ($j == 1) {
                    CouponGrant::create([
                        'coupon_id' => $i,
                        'code'      => 'coupon' . $i . '-' . $j,
                        'status'    => 1
                    ]);
                    UserCoupon::create([
                        'user_id'   => 1,
                        'code'      => 'coupon' . $i . '-' . $j,
                        'coupon_id' => $i,
                        'status'    => 1
                    ]);
                } else {
                    CouponGrant::create([
                        'coupon_id' => $i,
                        'code'      => 'coupon' . $i . '-' . $j,
                        'status'    => 0
                    ]);
                }
            }
        }
        // 满减券
        for ($i=4; $i <= 6; $i++) {
            for ($j=1; $j < 100; $j++) {
                if ($j == 1) {
                    CouponGrant::create([
                        'coupon_id' => $i,
                        'code'      => 'coupon' . $i . '-' . $j,
                        'status'    => 1
                    ]);
                    UserCoupon::create([
                        'user_id'   => 1,
                        'code'      => 'coupon' . $i . '-' . $j,
                        'coupon_id' => $i,
                        'status'    => 1
                    ]);
                } else {
                    CouponGrant::create([
                        'coupon_id' => $i,
                        'code'      => 'coupon' . $i . '-' . $j,
                        'status'    => 0
                    ]);
                }
            }
        }
        // 折扣券
        // for ($i=7; $i <= 9; $i++) {
        //     for ($j=1; $j < 100; $j++) {
        //         if ($j == 1) {
        //             CouponGrant::create([
        //                 'coupon_id' => $i,
        //                 'code'      => 'coupon' . $i . '-' . $j,
        //                 'status'    => 1
        //             ]);
        //             UserCoupon::create([
        //                 'user_id'   => 1,
        //                 'code'      => 'coupon' . $i . '-' . $j,
        //                 'coupon_id' => $i,
        //                 'status'    => 1
        //             ]);
        //         } else {
        //             CouponGrant::create([
        //                 'coupon_id' => $i,
        //                 'code'      => 'coupon' . $i . '-' . $j,
        //                 'status'    => 0
        //             ]);
        //         }
        //     }
        // }
    }
}
