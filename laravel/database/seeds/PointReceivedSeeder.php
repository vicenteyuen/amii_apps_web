<?php

use Illuminate\Database\Seeder;
use App\Models\PointReceived;
use App\Models\Balance;

class PointReceivedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PointReceived::create([
            'user_id' => 1,
            'type' => 'register',
            'type_child' => 'register',
            'points' => 1000
        ]);

        PointReceived::create([
            'user_id' => 1,
            'type' => 'update',
            'type_child' => '10',
            'points' => 10
        ]);
        PointReceived::create([
            'user_id' => 1,
            'type' => 'update',
            'type_child' => 'birthday',
            'points' => 10
        ]);

         PointReceived::create([
            'user_id' => 1,
            'type' => 'update',
            'type_child' => 'wechat',
            'points' => 20
         ]);
         PointReceived::create([
            'user_id' => 1,
            'type' => 'update',
            'type_child' => 'qq',
            'points' => 20
         ]);

         PointReceived::create([
            'user_id' => 1,
            'type' => 'looking',
            'type_child' => 'figure',
            'points' => 10
         ]);


         PointReceived::create([
            'user_id' => 1,
            'type' => 'invite',
            'type_child' => 'invite',
            'points' => 500
         ]);

         PointReceived::create([
            'user_id' => 1,
            'type' => 'image_appraising',
            'type_child' => 'mark',
            'points' => 10
         ]);
         PointReceived::create([
            'user_id' => 1,
            'type' => 'image_appraising',
            'type_child' => 'comment',
            'points' => 10
         ]);

         PointReceived::create([
            'user_id' => 1,
            'type' => 'looking',
            'type_child' => 'figure',
            'points' => 10
         ]);

         PointReceived::create([
            'user_id' => 1,
            'type' => 'received_day',
            'type_child' => 'received_day',
            'points' => 10
         ]);
        
         for ($i=0; $i< 10 ; $i++) { 
            $type = rand(1,2);
            if($type ==  1){
                $value = rand(1,1000);
            }else{
                $value = -(rand(1,1000));
            }
             Balance::create([
                'user_id' => 1,
                'type'    => $type,
                'value'   => $value,
            ]);
         }

    }
}
