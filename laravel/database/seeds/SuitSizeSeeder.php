<?php

use Illuminate\Database\Seeder;
use App\Models\SuitSize;

class SuitSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 上装
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'XXXS',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'XXS',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'XS',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'S',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'M',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'L',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'XL',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'XXL',
            'question_id' => 1,
        ]);
        SuitSize::create([
            'type'  => 1,
            'kinds' => 'XXXL',
            'question_id' => 1,
        ]);

        // 下装
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'XXXS',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'XXS',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'XS',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'S',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'M',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'L',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'XL',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'XXL',
            'question_id' => 2,
        ]);
        SuitSize::create([
            'type'  => 2,
            'kinds' => 'XXXL',
            'question_id' => 2,
        ]);

        // 肤色
        SuitSize::create([
            'type'  => 3,
            'kinds' => '偏白',
        ]);
        SuitSize::create([
            'type'  => 3,
            'kinds' => '正常',
        ]);
        SuitSize::create([
            'type'  => 3,
            'kinds' => '偏暗',
        ]);
         SuitSize::create([
            'type'  => 3,
            'kinds' => '偏黄',
         ]);

         // 发色
         SuitSize::create([
            'type'  => 4,
            'kinds' => '黑色',
         ]);
         SuitSize::create([
            'type'  => 4,
            'kinds' => '金发色',
         ]);
         SuitSize::create([
            'type'  => 4,
            'kinds' => '褐发色',
         ]);
         SuitSize::create([
            'type'  => 4,
            'kinds' => '赤褐发色',
         ]);
         SuitSize::create([
            'type'  => 4,
            'kinds' => '栗发色',
         ]);
         SuitSize::create([
            'type'  => 4,
            'kinds' => '红发色',
         ]);
         SuitSize::create([
            'type'  => 4,
            'kinds' => '灰白色',
         ]);
         SuitSize::create([
            'type'  => 4,
            'kinds' => '白发色',
         ]);
    }
}
