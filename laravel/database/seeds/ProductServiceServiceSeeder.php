<?php

use Illuminate\Database\Seeder;
use App\Models\ProductService;

class ProductServiceServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductService::truncate();
        ProductService::create([
            'name' => '七天退货',
            'status' => 1,
        ]);
    }
}
