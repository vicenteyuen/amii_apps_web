<?php

use Illuminate\Database\Seeder;
use App\Models\UserQuestion;
use App\Models\FirstPurchaseText;
use App\Models\Activity;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserQuestion::truncate();
        FirstPurchaseText::truncate();
        Activity::truncate();
        UserQuestion::create([
            'question' => 'Q1.你在AMII购买的上装尺码多少?',
        ]);
        UserQuestion::create([
            'question' => 'Q2.你在AMII购买的下装尺码多少?',
        ]);

        FirstPurchaseText::create(['text' => '新用户在商城首次下单,首单实付金额的30%将在系统显示签收包裹后,以账户余额的方式返还给客户的AMII账号中。用户帐号中的余额可累积,可用于购买AMII产品,也可以用户于余额提现。','provider' => 'first']);

        FirstPurchaseText::create(['text' => '首单实付金额满200元,送价值199元的戒指项链套装,数量有限,先到先得。','provider' => 'first']);
        Activity::create(['provider' => 'first','title' => '首单购+见面礼','order_description' => '首单购赠品']);
        Activity::create(['provider' => 'game','title' => '拼图游戏','order_description' => '游戏','rule' => 300]);
    }
}
