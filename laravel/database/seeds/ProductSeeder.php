<?php

use Illuminate\Database\Seeder;
use App\Models\PointLevel;
use App\Helpers\AmiiHelper;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\SellProduct;
use App\Models\SellAttribute;
use App\Models\SellAttributeValue;
use Ecommerce\Models\Inventory;
use EcommerceManage\Models\Category;
use Ecommerce\Models\Product;
use App\Models\InventoryAttributeValue;
use App\Models\UserComment;
use App\Models\BrandProduct;

class ProductSeeder extends Seeder
{
   /**
         * Run the database seeds
         * @return void
         */
    public function run()
    {
        // categories
        Category::create([
            'name' => '上装',
            'cate_desc' => '上装',
            'parent_id' => '0',
        ]);

        Category::create([
            'name' => '下装',
            'cate_desc' => '下装',
            'parent_id' => '0',
        ]);

        Category::create([
            'name' => '衬衣',
            'cate_desc' => '上装',
            'parent_id' => '1',
        ]);

         Category::create([
            'name' => '内衣',
            'cate_desc' => '上装',
            'parent_id' => '3',
         ]);

         // Attribute
         Attribute::create([
            'name' => '颜色'
         ]);
         Attribute::create([
            'name' => '尺寸'
         ]);

         AttributeValue::create([
            'attribute_id' => '1',
            'value'        => '白色'
         ]);

         AttributeValue::create([
            'attribute_id' => '1',
            'value'        => '黑色'
         ]);

         AttributeValue::create([
            'attribute_id' => '2',
            'value'        => 'XL'
         ]);

         AttributeValue::create([
            'attribute_id' => '2',
            'value'        => 'XXL'
         ]);
         $k = 1;
         $comment = 1;

         for ($i=1; $i <= 60; $i++) {
            $provider = 1;
            if ($i> 30) {
                $provider = 2;
            }

                $rand = rand(1, 10);
                Product::create([
                    'name'  => '商品'.$i,
                    'snumber' => AmiiHelper::generateProductSn().(rand(0, 10)*$i),
                    'product_price' => rand(1,100).rand(1, 9),
                    'main_image' => "/static/product/".$rand.".jpg",
                    'detail' => '<p><img src="http://ojeglhle9.bkt.clouddn.com/ganguo/ganguotshirt1.png"></p><p><img src="http://ojeglhle9.bkt.clouddn.com/ganguo/ganguotshirt2.png"></p><p><img src="http://ojeglhle9.bkt.clouddn.com/ganguo/ganguotshirt3.png"></p><p><img src="http://ojeglhle9.bkt.clouddn.com/ganguo/ganguotshirt4.png"></p><p><img src="http://ojeglhle9.bkt.clouddn.com/ganguo/ganguotshirt5.png"></p>'
                ]);
                BrandProduct::create([
                    'product_id' => $i,
                    'brand_id' => rand(1, 10)
                ]);

                SellProduct::create([
                    'product_id'      => $i,
                    'number'          => 0,
                    'status'          => 1,
                    'provider'        => $provider,
                    'product_point'   => 1000 * rand(0, 10),
                    'base_price'      => rand(1, 100).rand(1, 9),
                    'market_price'    => rand(1, 100).rand(1, 9),
                    'number'          => 200,
                    'image'           => "/static/product/".$rand.".jpg",
                ]);

             // 商品属性
                SellAttribute::create([
                    'attribute_id' => 1,
                    'product_sell_attribute_id'     => $i
                ]);
                SellAttribute::create([
                    'attribute_id' => 2,
                    'product_sell_attribute_id'     => $i
                ]);

             // 商品属性值
                SellAttributeValue::create([
                    'sell_attribute_id' => $i*2-1,
                    'attribute_id' => 1,
                    'attribute_value_id' => 1,
                ]);

                SellAttributeValue::create([
                    'sell_attribute_id' => $i*2-1,
                    'attribute_id' => 1,
                    'attribute_value_id' => 2,
                ]);

                SellAttributeValue::create([
                    'sell_attribute_id' => $i*2,
                    'attribute_id' => 2,
                    'attribute_value_id' => 3,
                ]);


                SellAttributeValue::create([
                    'sell_attribute_id' => $i*2,
                    'attribute_id' => 2,
                    'attribute_value_id' => 4,
                ]);

             // 商品库存
                Inventory::create([
                    'product_id' => $i,
                    'product_sell_id' => $i,
                    'value_key'  => '1_3',
                    'number'     => 200,
                    'warn_number' => 10,
                    'mark_price'  => rand(1,100).rand(1, 9),
                    'shop_price'  => rand(1,100).rand(1, 9),
                ]);

                Inventory::create([
                    'product_id' => $i,
                    'product_sell_id' => $i,
                    'value_key'  => '1_4',
                    'number'     => 200,
                    'warn_number' => 10,
                    'mark_price'  => rand(1,100).rand(1, 9),
                    'shop_price'  => rand(1,100).rand(1, 9),
                ]);
                Inventory::create([
                    'product_id' => $i,
                    'product_sell_id' => $i,
                    'value_key'  => '2_3',
                    'number'     => 200,
                    'warn_number' => 10,
                    'mark_price'  => rand(1,100).rand(1, 9),
                    'shop_price'  => rand(1,100).rand(1, 9),
                ]);

                Inventory::create([
                    'product_id' => $i,
                    'product_sell_id' => $i,
                    'value_key'  => '2_4',
                    'number'     => 200,
                    'warn_number' => 10,
                    'mark_price'  => 40,
                    'shop_price'  => 59,
                ]);

                for ($com=0; $com < 4; $com++) {
                    UserComment::create([
                        'inventory_id' => $comment++,
                        'user_id'      => 1,
                        'product_sell_id' => $i,
                        'content'      => 'comment'.rand(1, 100),
                        'comment_rank_id' => rand(1,5),
                     ]);
                }

                InventoryAttributeValue::create([
                    'inventory_id' => $k,
                    'attribute_value_id' => 1,
                ]);
                InventoryAttributeValue::create([
                    'inventory_id' => $k++,
                    'attribute_value_id' => 3,
                ]);
                InventoryAttributeValue::create([
                    'inventory_id' => $k,
                    'attribute_value_id' => 1,
                ]);
                InventoryAttributeValue::create([
                    'inventory_id' => $k++,
                    'attribute_value_id' => 4,
                ]);
                InventoryAttributeValue::create([
                    'inventory_id' => $k,
                    'attribute_value_id' => 2,
                ]);
                InventoryAttributeValue::create([
                    'inventory_id' => $k++,
                    'attribute_value_id' => 3,
                ]);
                InventoryAttributeValue::create([
                    'inventory_id' => $k,
                    'attribute_value_id' => 2,
                ]);
                InventoryAttributeValue::create([
                    'inventory_id' => $k++,
                    'attribute_value_id' => 4,
                ]);
            }


    }
}
