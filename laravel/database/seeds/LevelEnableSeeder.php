<?php

use Illuminate\Database\Seeder;
use App\Models\UserEnable;
use App\Models\Enable;
use App\Models\PointLevel;
use App\Models\PointRecored;
use App\Models\LevelEnable;

class LevelEnableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         PointLevel::truncate();
         Enable::truncate();
         LevelEnable::truncate();

        // 等级 特权
        //
        PointLevel::create([
          'name'   => 'V0',
          'points' =>  0
         ]);
        PointLevel::create([
          'name'   => 'V1',
          'points' =>  1000
         ]);

        PointLevel::create([
          'name'   => 'V2',
          'points' =>  5000
         ]);

        PointLevel::create([
          'name'   => 'V3',
          'points' =>  10000
         ]);

        PointLevel::create([
          'name'   => 'V4',
          'points' =>  20000
         ]);

        PointLevel::create([
          'name'   => 'V5',
          'points' =>  50000
         ]);

        PointLevel::create([
          'name'   => 'V6',
          'points' =>  150000
         ]);

        PointLevel::create([
          'name'   => 'V7',
          'points' =>  400000
         ]);

        PointLevel::create([
          'name'   => 'V8',
          'points' =>  800000
         ]);

        PointLevel::create([
          'name'   => 'V9',
          'points' =>  1500000
         ]);

        PointLevel::create([
          'name'   => 'V10',
          'points' =>  3000000
         ]);

        Enable::create([
            "name" => '会员专享价',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/icon_memberprice2.png'),
            "describe" => '享受会员折扣99折',
        ]);

        Enable::create([
            "name" => '极速退款',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/icon_refund2.png'),
            "describe" => '享受额度100元的极速退款',
        ]);

        Enable::create([
            "name" => '生日积分',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/icon_birthday2.png'),
            "describe" => '生日当天可获得系统奖励2000积分',
        ]);

        Enable::create([
            "name" => '生日折扣卡',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/icon_discount2.png'),
            "describe" => '生日日期可领取专区生日折扣卡,有效期为一个月',
        ]);
        Enable::create([
            "name" => '尊享包邮卡',
             "image" => env('IMAGE_HOST').('/assets/images/Enable/icon_postage2.png'),
            "describe" => '尊享包邮,可取尊享包邮卡',

        ]);
        Enable::create([
            "name" => '退货保障卡',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/icon_return2.png'),
            "describe" => '任性退货,可领取退货保障卡',
        ]);
       
        Enable::create([
            "name" => '积分加倍',
            "image" => env('IMAGE_HOST').('/assets/images/Enable/icon_doublepoint2.png'),
            "describe" => '进行限时活时,会员可获得购物积分的额外奖励',
        ]);

        // v1
        LevelEnable::create([
            'level_id'  => 2,
            'enable_id' => 3,
            'get'       => '1000',
            'describe'  => '1000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 2,
            'enable_id' => 4,
            'get'       => '90',
            'describe'  => '9折',
        ]);
        LevelEnable::create([
            'level_id'  => 2,
            'enable_id' => 5,
            'get'       => '1',
            'describe'  => '1次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 2,
            'enable_id' => 6,
            'get'       => '1',
            'describe'  => '1次/年',
        ]);

        //v2
        LevelEnable::create([
            'level_id'  => 3,
            'enable_id' => 3,
            'get'       => '2000',
            'describe'  => '2000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 3,
            'enable_id' => 4,
            'get'       => '95',
            'describe'  => '9.5折',
        ]);
        LevelEnable::create([
            'level_id'  => 3,
            'enable_id' => 5,
            'get'       => '2',
            'describe'  => '2次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 3,
            'enable_id' => 6,
            'get'       => '2',
            'describe'  => '2次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 3,
            'enable_id' => 7,
            'get'       => '1.1',
            'describe'  => '1.1倍',
        ]);

        //v3
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 1,
            'get'       => '99',
            'describe'  => '9.9折',
        ]);
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 2,
            'get'       => '100',
            'describe'  => '100元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 3,
            'get'       => '2000',
            'describe'  => '2000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 4,
            'get'       => '95',
            'describe'  => '9.5折',
        ]);
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 5,
            'get'       => '3',
            'describe'  => '3次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 6,
            'get'       => '3',
            'describe'  => '3次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 4,
            'enable_id' => 7,
            'get'       => '1.1',
            'describe'  => '1.1倍',
        ]);

        //v4
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 1,
            'get'       => '98',
            'describe'  => '9.8折',
        ]);
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 2,
            'get'       => '300',
            'describe'  => '300元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 3,
            'get'       => '3000',
            'describe'  => '3000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 4,
            'get'       => '90',
            'describe'  => '9折',
        ]);
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 5,
            'get'       => '4',
            'describe'  => '4次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 6,
            'get'       => '4',
            'describe'  => '4次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 5,
            'enable_id' => 7,
            'get'       => '1.2',
            'describe'  => '1.2倍',
        ]);


        //v5
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 1,
            'get'       => '98',
            'describe'  => '9.8折',
        ]);
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 2,
            'get'       => '500',
            'describe'  => '500元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 3,
            'get'       => '3000',
            'describe'  => '3000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 4,
            'get'       => '90',
            'describe'  => '9折',
        ]);
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 5,
            'get'       => '5',
            'describe'  => '5次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 6,
            'get'       => '5',
            'describe'  => '5次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 6,
            'enable_id' => 7,
            'get'       => '1.2',
            'describe'  => '1.2倍',
        ]);

         //v6
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 1,
            'get'       => '97',
            'describe'  => '9.7折',
        ]);
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 2,
            'get'       => '1000',
            'describe'  => '1000元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 3,
            'get'       => '3000',
            'describe'  => '3000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 4,
            'get'       => '90',
            'describe'  => '9折',
        ]);
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 5,
            'get'       => '6',
            'describe'  => '6次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 6,
            'get'       => '6',
            'describe'  => '6次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 7,
            'enable_id' => 7,
            'get'       => '1.3',
            'describe'  => '1.3倍',
        ]);


        //v7
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 1,
            'get'       => '96',
            'describe'  => '9.6折',
        ]);
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 2,
            'get'       => '2000',
            'describe'  => '2000元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 3,
            'get'       => '5000',
            'describe'  => '5000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 4,
            'get'       => '85',
            'describe'  => '8.5折',
        ]);
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 5,
            'get'       => '8',
            'describe'  => '8次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 6,
            'get'       => '8',
            'describe'  => '8次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 8,
            'enable_id' => 7,
            'get'       => '1.4',
            'describe'  => '1.4倍',
        ]);

        //v8
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 1,
            'get'       => '95',
            'describe'  => '9.5折',
        ]);
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 2,
            'get'       => '5000',
            'describe'  => '5000元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 3,
            'get'       => '5000',
            'describe'  => '5000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 4,
            'get'       => '85',
            'describe'  => '8.5折',
        ]);
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 5,
            'get'       => '10',
            'describe'  => '10次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 6,
            'get'       => '10',
            'describe'  => '10次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 9,
            'enable_id' => 7,
            'get'       => '1.5',
            'describe'  => '1.5倍',
        ]);

        //v9
        LevelEnable::create([
            'level_id'  => 10,
            'enable_id' => 1,
            'get'       => '93',
            'describe'  => '9.3折',
        ]);
        LevelEnable::create([
            'level_id'  => 10,
            'enable_id' => 2,
            'get'       => '10000',
            'describe'  => '10000元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 10,
            'enable_id' => 3,
            'get'       => '10000',
            'describe'  => '10000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 10,
            'enable_id' => 4,
            'get'       => '80',
            'describe'  => '8折',
        ]);
        LevelEnable::create([
            'level_id'  => 10,
            'enable_id' => 5,
            'get'       => '20',
            'describe'  => '20次/年',
        ]);
        LevelEnable::create([
            'level_id'  => 10,
            'enable_id' => 6,
            'get'       => '20',
            'describe'  => '20次/年',
        ]);

        LevelEnable::create([
            'level_id'  => 10,
            'enable_id' => 7,
            'get'       => '1.6',
            'describe'  => '1.6倍',
        ]);

         //v10
        LevelEnable::create([
            'level_id'  => 11,
            'enable_id' => 1,
            'get'       => '90',
            'describe'  => '9折',
        ]);
        LevelEnable::create([
            'level_id'  => 11,
            'enable_id' => 2,
            'get'       => '20000',
            'describe'  => '20000元额度',
        ]);
        LevelEnable::create([
            'level_id'  => 11,
            'enable_id' => 3,
            'get'       => '10000',
            'describe'  => '10000积分',
        ]);
        LevelEnable::create([
            'level_id'  => 11,
            'enable_id' => 4,
            'get'       => '80',
            'describe'  => '8折',
        ]);
        LevelEnable::create([
            'level_id'  => 11,
            'enable_id' => 5,
            'get'       => 'no_reason',
            'describe'  => '年度包邮',
        ]);
        LevelEnable::create([
            'level_id'  => 11,
            'enable_id' => 6,
            'get'       => 'no_reason',
            'describe'  => '无理由退货',
        ]);

        LevelEnable::create([
            'level_id'  => 11,
            'enable_id' => 7,
            'get'       => '1.8',
            'describe'  => '1.8倍',
        ]);
    }
}
