<?php

use Illuminate\Database\Seeder;
use App\Models\Size;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // 上装
        Size::create([
            'name' => 'XXXS',
            'least_height' => 147,
            'most_height' => 150,
        ]);
        Size::create([
            'name' => 'XXS',
            'least_height' => 150,
            'most_height' => 153,
        ]);
        Size::create([
            'name' => 'XS',
            'least_height' => 153,
            'most_height' => 157,
        ]);
        Size::create([
            'name' => 'S',
            'least_height' => 157,
            'most_height' => 162,
        ]);
        Size::create([
            'name' => 'M',
            'least_height' => 162,
            'most_height' => 167,
        ]);
        Size::create([
            'name' => 'L',
            'least_height' => 167,
            'most_height' => 172,
        ]);
        Size::create([
            'name' => 'XL',
            'least_height' => 172,
            'most_height' => 177,
        ]);
        Size::create([
            'name' => 'XXL',
            'least_height' => 177,
            'most_height' => 180,
        ]);
        Size::create([
            'name' => 'XXXL',
            'least_height' => 180,
            'most_height' => 200,
        ]);
    }
}
