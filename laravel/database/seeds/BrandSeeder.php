<?php

use Illuminate\Database\Seeder;

use EcommerceManage\Models\Brand;
use App\Models\BrandCategory;
use App\Models\BrandSellProduct;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [];

        $arr[] = ['merchant_id'=>1024,'name'=>'AMII','hot'=>100,'logo'=>"/static/brand/amii_logo.jpg"];
        $arr[] = ['merchant_id'=>1025,'name'=>'Amii Redefine','hot'=>90,'logo'=>"/static/brand/amii_redfine.jpg"];
        $arr[] = ['merchant_id'=>992,'name'=>'摩尼菲格','hot'=>80,'logo'=>"/static/brand/amii_mnfg.jpg"];
        $arr[] = ['merchant_id'=>981,'name'=>'AMII童装','hot'=>70,'logo'=>"/static/brand/amii_logo.jpg"];


        for ($i=1; $i <= 4; $i++) {
            // 品牌
            Brand::create($arr[$i-1]);

            // 品牌分类
            BrandCategory::create([
                'brand_id' => $i,
                'category_id' => 1,
            ]);

            BrandCategory::create([
                'brand_id' => $i,
                'category_id' => 2,
            ]);

            // 品牌商品
            for ($j=1; $j <= 30; $j++) {
                if (mt_rand(0, 1)) {
                    BrandSellProduct::create([
                        'brand_id' => $i,
                        'sell_product_id' => $j
                    ]);
                }
            }
        }
    }
}
