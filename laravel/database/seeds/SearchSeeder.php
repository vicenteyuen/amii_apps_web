<?php

use Illuminate\Database\Seeder;
use App\Models\HotSearch;

class SearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            '毛衣',
            'T恤',
            '夹克',
            '大衣',
            '女生外套',
            '男生外套',
            'amii纪念杉',
            '风衣',
            '西服',
        ];

        for ($i=0; $i <8; $i++) {
            HotSearch::create([
                'name' => $name[$i],
                'number' => rand(1, 50),
            ]);
        }
    }
}
