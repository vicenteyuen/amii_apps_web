<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateNotifyMessageTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 通知消息
        $schema->create('notify_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(0)->comment('状态 0未读 1已读 2无效');
            $table->integer('user_id')->default(0)->comment('用户id');
            $table->string('provider')->default('notify')->comment('通知消息类型');
            $table->string('title')->default('')->comment('消息标题');
            $table->string('content')->default('')->comment('消息内容');
            $table->string('type')->default('')->comment('跳转类型');
            $table->string('resource')->default('')->comment('跳转资源标识');

            $table->softDeletes();
            $table->timestamps();
        });

        // 用户通知消息已读表
        $schema->create('user_notify_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0)->comment('用户id');
            $table->integer('notify_messages_id')->default(0)->comment('通知消息表id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notify_messages');
        Schema::dropIfExists('user_notify_messages');
    }
}
