<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateCommunityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // subjects 专题表
        $schema->create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('标题');
            $table->string('subtitle')->nullable()->comment('子标题');
            $table->text('content')->comment('文章内容');
            $table->integer('scan')->unsigned()->default(0)->comment('浏览数');
            $table->string('image')->comment('专题主图');
            $table->timestamps();
        });

        // subject_products 专题商品关联表
        $schema->create('subject_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->comment('product表id');
            $table->integer('subject_id')->unsigned()->comment('专题表id');
            $table->timestamps();
        });

        //subject_views 专题查看表
        $schema->create('subject_views', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_id')->unsighed()->comment('专题id');
            $table->integer('user_id')->unsigned()->comment('用户id');
            $table->timestamps();
        });

        //video 视频表
        $schema->create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('标题');
            $table->string('image')->comment('封面');
            $table->string('description')->comment('视频描述');
            $table->string('video')->comment('视频链接');
            $table->integer('likes')->unsigned()->comment('点赞数');
            $table->timestamps();
        });

        //video_likes 视频点赞表
        $schema->create('video_likes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('video_id');
            $table->integer('user_id');
            $table->timestamps();
        });

        //physical_stores 实体店表
        $schema->create('physical_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('名称');
            $table->string('name_initial')->comment('名称首字母');
            $table->string('phone')->nullable()->comment('联系电话');
            $table->string('city_name')->comment('城市名');
            $table->string('address')->comment('地址');
            $table->string('detail_address')->nullable()->comment('详细地址');
            $table->string('image')->comment('配图');
            $table->float('latitude', 10, 6);
            $table->float('longitude', 10, 6);
            $table->text('introduction')->nullable()->comment('商店描述');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subjects');
        Schema::drop('subject_products');
        Schema::drop('subject_views');
        Schema::drop('videos');
        Schema::drop('video_likes');
        Schema::drop('physical_stores');
    }
}
