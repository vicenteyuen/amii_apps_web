<?php

use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreatePopularTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 首页轮播图表
        $schema->create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(0)->comment('可用状态');
            $table->string('title', 64)->default('')->comment('标题');
            $table->string('app_image')->default('')->comment('移动应用图片');
            $table->string('mobile_image')->default('')->comment('移动网页图片');
            $table->string('weapp_image')->default('')->comment('微信小程序图片');
            $table->integer('orderby')->default(0)->comment('排序');
            $table->tinyInteger('provider')->default(0)->comment('点击跳转类型');
            $table->string('resource')->default('')->comment('跳转数据');

            $table->timestamps();
        });

        // 推广表
        $schema->create('populars', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(0)->comment('可用状态');
            $table->integer('orderby')->default(0)->comment('排序');
            $table->integer('parent_id')->default(0)->comment('父级id');
            $table->integer('_lft')->nullable()->unsigned();   //left node
            $table->integer('_rgt')->nullable()->unsigned();   //right node
            $table->string('title', 64)->default('')->comment('标题');
            $table->string('descript')->default('')->comment('描述');
            $table->string('image')->default('')->comment('图片');
            $table->string('weapp_image')->default('')->comment('小程序图片');
            $table->tinyInteger('is_click')->default(1)->comment('是否可点击跳转');
            $table->tinyInteger('provider')->default(0)->comment('点击跳转类型');
            $table->string('resource')->default('')->comment('跳转数据');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banners');
        Schema::drop('populars');
    }
}
