<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateCouponTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 优惠券表
        $schema->create('coupons', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->tinyInteger('status')->default(0)->comment('优惠券状态');
            $table->integer('orderby')->default(0)->comment('展示排序');
            $table->string('cid', 64)->index()->comment('优惠券唯一标识');
            $table->string('title', 128)->default('优惠券')->comment('优惠券标题');
            $table->string('image', 1024)->default('')->comment('优惠券图');
            $table->integer('number')->default(0)->comment('优惠券数量');
            $table->integer('remain')->default(0)->comment('优惠券剩余可领数量');
            $table->tinyInteger('provider')->default(0)->comment('优惠券类型');
            $table->decimal('value', 8, 2)->default(0)->comment('优惠券可用总金额');
            $table->integer('discount')->default(0)->comment('优惠券折扣');
            $table->decimal('lowest', 8, 2)->default(0)->comment('优惠计算额度 0为无门槛券');
            $table->timestamp('start')->nullable()->comment('开始时间 为空则无开始时间');
            $table->timestamp('end')->nullable()->comment('结束时间 为空则无结束时间');

            $table->softDeletes();
            $table->timestamps();
        });

        // 优惠券发放表
        $schema->create('coupon_grants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id')->default(0)->comment('优惠券id');
            $table->string('code', 64)->index()->default('')->comment('发放优惠券唯一标识');
            $table->tinyInteger('status')->default(0)->comment('优惠券领用状态 0未领用 1已领用');

            $table->softDeletes();
            $table->timestamps();
        });

        // 优惠券领取表
        $schema->create('user_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id')->default(0)->comment('优惠券id');
            $table->integer('user_id')->default(0)->comment('领取优惠券用户id');
            $table->string('code', 64)->index()->default('')->comment('发放优惠券唯一标识');
            $table->tinyInteger('status')->default(1)->comment('优惠券可用状态 0不可使用 1可使用');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coupons');
        Schema::drop('coupon_grants');
        Schema::drop('user_coupons');
    }
}
