<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateAttributeShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 分类表
        $schema->create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 30);
            $table->string('cate_desc')->comment('分类描述，此处预留，到时看是否需要');
            $table->integer('weight')->comment('权重，数值越大越靠前显示，否则按照id越小越靠前显示');
            $table->string('img', 1024)->nullable()->comment('分类的图片');
            $table->tinyInteger('status')->default(1)->comment('分类启用状态');
            $table->integer('parent_id')->unsigned()->comment('上级分类id');
            $table->integer('_lft')->nullable()->unsigned()->comment('left node');
            $table->integer('_rgt')->nullable()->unsigned()->comment('right node');
            $table->softDeletes();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        // 商品分类关联表
        $schema->create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->index();
            $table->integer('category_id')->index();
            $table->integer('brand_id');

            $table->timestamps();
        });

        // 品牌表
        $schema->create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('品牌名');
            $table->string('logo')->comment('存储品牌logo的url');
            $table->tinyInteger('hot')->default(0)->comment('品牌热度');
            $table->tinyInteger('recommend')->default(0)->comment('品牌推荐字段');
            $table->tinyInteger('weight')->default(0)->comment('品牌排序');

            $table->timestamps();
        });

        // 品牌分类
        $schema->create('brand_categorys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->comment('品牌id');
            $table->integer('category_id')->comment('分类id');

            $table->timestamps();
        });

        // 品牌商品
        $schema->create('brand_sell_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->comment('品牌id');
            $table->integer('sell_product_id')->comment('销售商品id');

            $table->timestamps();
        });

        // 商品tag表
        $schema->create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('item name');
            $table->string('logo')->comment('item logo url');
            $table->string('mark')->comment('item 标识');
            $table->integer('weight')->comment('权重，数值越大越靠前显示，否则按照id越小越靠前显示');
            $table->tinyInteger('status')->default(0)->comment('tags状态');
            $table->softDeletes();
            $table->timestamps();
            $table->unique('mark');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('product_categories');
        Schema::dropIfExists('brands');
        Schema::dropIfExists('brand_categorys');
        Schema::dropIfExists('brand_sell_products');
        Schema::dropIfExists('tags');
    }
}
