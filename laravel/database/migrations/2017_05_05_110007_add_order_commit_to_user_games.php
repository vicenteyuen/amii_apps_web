<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderCommitToUserGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_games', function (Blueprint $table) {
            $table->integer('order_remain_time')->default(0)->comment('合成订单剩余时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_games', function (Blueprint $table) {
            $table->dropColumn('order_remain_time');
        });
    }
}
