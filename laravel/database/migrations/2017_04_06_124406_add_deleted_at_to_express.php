<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedAtToExpress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('express_templates', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('express_conditions', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
            $table->integer('express_template_id')->comment('快递公司运费模版默认运费id');

        });
        Schema::table('express_fees', function (Blueprint $table) {
             $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('express_templates', function (Blueprint $table) {
             $table->dropColumn('deleted_at');
        });
        Schema::table('express_conditions', function (Blueprint $table) {
             $table->dropColumn('deleted_at');
             $table->dropColumn('express_template_id');
        });
        Schema::table('express_fees', function (Blueprint $table) {
             $table->dropColumn('deleted_at');
        });
    }
}
