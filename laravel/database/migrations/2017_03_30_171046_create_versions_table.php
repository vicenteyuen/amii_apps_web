<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        //系统版本记录表
        Schema::create('versions', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('channel')->comment('版本渠道');
            $table->string('version_code')->comment('版本码');
            $table->string('version_type')->comment('版本类型: android, ios');
            $table->string('version_number')->comment('版本序号');
            $table->string('version_url')->comment('版本url');
            $table->boolean('is_newest')->default(1)->comment('是否最新版本 0 否 1 是');
            $table->string('description')->comment('版本描述');
            $table->timestamps();

            $table->index('is_newest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('versions');
    }
}
