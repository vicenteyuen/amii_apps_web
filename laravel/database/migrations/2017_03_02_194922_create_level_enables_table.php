<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateLevelEnablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });


        $schema->create('level_enables', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyinteger('level_id');
            $table->tinyinteger('enable_id');
            $table->string('get', 10)->comment('不同特权具有的权限');
            $table->string('describe')->comment('特权描述');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('level_enables');
    }
}
