<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateZhaoshangTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 意见反馈
        $schema->create('zhaoshangs', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(0)->comment('招商信息状态 0未处理 1已处理');
            $table->string('name');
            $table->string('phone');
            $table->string('email', 64);
            $table->timestamps();

            $table->index('name');
            $table->index('phone');
            $table->index('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhaoshangs');
    }
}
