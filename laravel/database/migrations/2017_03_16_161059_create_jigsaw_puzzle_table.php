<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateJigsawPuzzleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 用户发起的拼图
        $schema->create('user_games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->comment('用户id');
            $table->integer('sell_product_id')->comment('发起拼图的商品');
            $table->tinyInteger('count')->comment('拼图总数');
            $table->tinyInteger('finished_number')->comment('已完成的数量');
            $table->tinyInteger('expired')->default(0)->comment('1 过期 0 未过期');
            $table->tinyInteger('success')->default(0)->comment('0 未成功 1成功 ');
            $table->tinyInteger('order_status')->comment('0 未提交订单 1提交订单');
            $table->integer('order_id')->default(0)->comment('拼图成功下单id');
            $table->integer('row')->comment('行数');
            $table->integer('col')->comment('列数');
            $table->timestamps();
            $table->index('user_id');
        });


        // 激活拼图的用户
        $schema->create('game_relation_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_games_id')->comment('用户发起拼图的id');
            $table->integer('user_id')->comment('激活用户id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_games');
        Schema::drop('game_relation_users');
    }
}
