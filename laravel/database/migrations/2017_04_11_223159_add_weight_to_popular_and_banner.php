<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWeightToPopularAndBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->integer('weight')->defult(0);
        });
        Schema::table('populars', function (Blueprint $table) {
            $table->integer('weight')->defult(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('weight');
        });
        Schema::table('populars', function (Blueprint $table) {
            $table->dropColumn('weight');
        });

    }
}
