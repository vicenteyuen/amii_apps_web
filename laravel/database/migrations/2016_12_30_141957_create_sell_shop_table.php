<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateSellShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 属性销售中间表--属性表 的中间表
        $schema->create('sell_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->comment('属性表id');
            $table->integer('product_sell_attribute_id')->unsigned()->comment('属性销售中间表的id');
            $table->timestamps();
        });

        // 属性销售中间表--属性表 的中间表
        $schema->create('sell_attribute_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_attribute_id')->unsigned()->comment('sell_attributes表id');
            $table->integer('attribute_value_id')->unsigned()->comment('属性值表的id');
            $table->integer('attribute_id')->unsigned()->comment('属性表的id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sell_attributes');
        Schema::drop('sell_attribute_values');
    }
}
