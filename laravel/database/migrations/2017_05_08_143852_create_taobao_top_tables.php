<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateTaobaoTopTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 更新记录表
        $schema->create('taobao_top_sync_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant')->default('')->comment('商户品牌');
            $table->timestamp('start_sync_time')->nullable()->comment('同步开始时间');
            $table->timestamp('end_sync_time')->nullable()->comment('同步结束时间');
            $table->timestamp('request_start_sync_time')->nullable()->comment('请求同步修改开始时间');
            $table->timestamp('request_end_sync_time')->nullable()->comment('请求同步修改结束时间');
            $table->text('value')->default('')->comment('查询结果');

            $table->timestamps();
        });

        // 店铺库存商品
        $schema->create('taobao_top_inventorys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant')->default('')->comment('商户品牌');
            $table->integer('taobao_outer_id')->default(0)->comment('淘宝商品外部id');
            $table->text('value')->default('')->comment('在售商品库存条目信息');
            $table->tinyInteger('is_handle')->default(0)->comment('商品是否处理 0未处理 1已处理');

            $table->timestamps();
        });

        // 商品数据处理
        $schema->create('taobao_top_product_handles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('taobao_top_inventory_id')->comment('店铺商品表id');
            $table->string('merchant')->default('')->comment('商户品牌');
            $table->integer('taobao_outer_id')->default(0)->comment('淘宝商品外部id');
            $table->string('taobao_product_id')->default('')->comment('淘宝商品数字id');

            $table->tinyInteger('is_publish')->default(0)->comment('是否发布 0未发布 1已发布');

            $table->mediumText('value')->default('')->comment('淘宝单个商品详细信息');

            $table->tinyInteger('handle_picture_status')->default(0)->comment('图片信息处理状态0 未处理 1已进入处理程序 2已处理完成 3图片信息空不需要处理');
            $table->text('handle_picture')->default('')->comment('处理后的图片数据');

            $table->tinyInteger('handle_wap_vision_status')->default(0)->comment('无线商品详情数据处理状态0未处理 1已进入处理程序 2已处理完成 3无线详情数据不需要处理');
            $table->text('handle_wap_vision')->default('')->comment('处理后的无线商品详情数据');

            $table->tinyInteger('check_skus')->default(0)->comment('检验sku信息 0未检测 1可以发布 2没有sku信息');

            $table->tinyInteger('check_cate')->default(0)->comment('查询类目是否完整 0未检测 1完整 2未查询出结果');

            $table->timestamps();
        });

        // 淘宝分类数据
        $schema->create('taobao_top_categorys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cid')->default(0)->comment('分类id');
            $table->integer('parent_cid')->default(0)->comment('分类父id');
            $table->string('name')->default('')->comment('分类名');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taobao_top_sync_logs');
        Schema::dropIfExists('taobao_top_inventorys');
        Schema::dropIfExists('taobao_top_product_handles');
        Schema::dropIfExists('taobao_top_categorys');
    }
}
