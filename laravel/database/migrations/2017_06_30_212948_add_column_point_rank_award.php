<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPointRankAward extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_rank_awards', function (Blueprint $table) {
            $table->tinyInteger('number')->default(0)->comment('人数');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_rank_awards', function (Blueprint $table) {
            $table->dropColumn('number');
        });
    }
}
