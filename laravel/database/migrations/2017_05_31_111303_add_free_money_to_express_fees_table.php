<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreeMoneyToExpressFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('express_fees', function (Blueprint $table) {
            $table->decimal('free_money')->default(null)->nullable()->comment('满多少钱包邮');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('express_fees', function (Blueprint $table) {
            $table->dropColumn('free_money');
        });
    }
}
