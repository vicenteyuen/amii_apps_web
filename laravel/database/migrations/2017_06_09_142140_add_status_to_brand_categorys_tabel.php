<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToBrandCategorysTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brand_categorys', function (Blueprint $table) {
            $table->integer('status')->default(1)->comment('1 可用 0不可用');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brand_categorys', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
