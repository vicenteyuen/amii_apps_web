<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateShopTable extends Migration
{
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        // 商品表
        $schema->create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->comment('商品名称');
            $table->string('snumber')->default('')->comment('商品编号');
            $table->integer('number')->unsigned()->default(0)->comment('库存');
            $table->decimal('product_price')->comment('售价');
            $table->char('unit', 10)->comment('商品计量单位');
            $table->string('brief', 255)->comment('商品描述');
            $table->text('detail')->comment('商品详情');
            $table->integer('give_point')->unsigned()->comment('购买时赠送的积分数量');
            $table->integer('click')->comment('商品浏览量');
            $table->integer('volume')->comment('商品总销量');
            $table->integer('sku')->default(0)->comment('独立库存');
            $table->string('main_image')->comment('商品主图');
            $table->tinyInteger('free_status')->default()->comment('0 需要运费 1 不需要运费');

            // $table->integer('fee_templates_id');     //运费模板
            $table->tinyInteger('status')->default(1)->comment('0下架1上架2设置定时自动上架');
            $table->timestamp('time_status')->comment('定时上架时间');
            $table->tinyInteger('price_status')->default(0)->comment('设置是否显示市场价跟会员价 0 不显示 1显示 默认只显示基本价格 0');

            // deepdraw
            $table->integer('deepdraw_id')->default(0)->comment('deepdraw商品id');

            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'INNODB';
        });
        DB::statement('ALTER TABLE '.'products ADD FULLTEXT '.'product_search(name)');

        // 属性表
        $schema->create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('属性名称');
            $table->tinyInteger('has_img')->default(0)->comment('是有图片');
            $table->tinyInteger('is_multi')->default(0)->comment('是否支持多选');
            $table->string('image')->comment('图片名称');
            $table->integer('weight')->default(0)->comment('显示权重');
            $table->tinyInteger('status')->default(1)->comment('分类启用状态');
            $table->tinyInteger('type')->default(0)->comment('属性类型');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'INNODB';
        });
        DB::statement('ALTER TABLE '.'attributes ADD FULLTEXT '.'attribute_search(name)');

        // 属性值表
        $schema->create('attribute_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->comment('所属的属性名称');
            $table->string('value')->comment('属性值');
            $table->string('image')->nullable()->comment('图片');
            $table->tinyInteger('status')->default(1)->comment('分类启用状态');
            $table->integer('weight')->default(0)->comment('显示权重');
            $table->index('attribute_id');
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'INNODB';
        });
        DB::statement('ALTER TABLE '.'attribute_values ADD FULLTEXT '.'attribute_values_search(value)');

        // 商品属性值合集库存表
        $schema->create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->comment('商品id');
            $table->string('sku_code')->comment('商品sku编号');
            $table->string('value_key')->comment('商品sku 属性值id字符串');
            $table->integer('sku_sell_count')->unsigned()->comment('销量');
            $table->decimal('mark_price')->comment('市场价格');
            $table->decimal('shop_price')->comment('会员价格');
            $table->integer('warn_number')->default('0')->comment('库存警告阀值');
            $table->integer('number')->default('0')->comment('库存总量');
            $table->integer('product_sell_id')->comment('中间表id');
            $table->string('image')->comment('库存图');
            $table->integer('status')->default('1')->comment('1有货0脱销');
            $table->index('product_sell_id');
            $table->timestamps();
        });

        // 商品图片表
        $schema->create('product_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('image')->comment('商品主图');
            $table->timestamps();
        });

        // 订单表
        $schema->create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('provider')->default('standard')->comment('订单类型 standard标准 point积分');
            $table->string('order_sn')->comment('订单号');
            $table->integer('user_id');
            $table->integer('shipping_region')->comment('收货地址代码');
            $table->text('shipping_address')->comment('收货地址冗余');
            $table->tinyInteger('order_status')->default(0)->comment('订单状态 0未确认 1已确认 2已取消 3无效');
            $table->tinyInteger('pay_status')->default(0)->comment('支付状态 0未付款 1已付款');
            $table->tinyInteger('shipping_status')->default(0)->comment('配送状态 0未发货 1已发货 2已收货 3已拒收');
            $table->tinyInteger('is_appraise')->default(0)->comment('是否评价 0未评价 1已评价');

            ## 售后
            $table->tinyInteger('can_refund')->default(0)->comment('是否可申请售后 0不可申请 1可申请');
            $table->timestamp('refund_time')->nullable()->comment('申请售后时间');
            $table->tinyInteger('is_refund')->default(0)->comment('退款状态 0未申请 1已申请 2已同意申请 3已退款 4已拒绝');
            $table->tinyInteger('is_refund_product')->default(0)->comment('退货状态 0未申请 1已申请 2已同意申请 3已退货 4已拒绝');

            $table->string('best_time')->default('任意时间')->comment('最佳送货时间,此为预留字段');
            $table->string('remark')->comment('订单备注');
            $table->string('invoice')->comment('发票抬头');
            $table->decimal('product_amount') ->comment('商品总价');
            $table->decimal('shipping_fee')->comment('货运费用');
            $table->decimal('discount')->comment('订单优惠金额');
            $table->decimal('vip_discount')->comment('vip优惠金额');
            $table->string('coupon_code')->default('')->comment('使用优惠券标识');
            $table->decimal('coupon_discount')->default(0)->comment('优惠券优惠金额');
            $table->decimal('balance_discount')->default(0)->comment('使用余额抵用金额');
            $table->decimal('point_discount')->default(0)->comment('使用积分抵用金额');
            $table->decimal('order_fee')->comment('订单需支付总价');

            ## 时间
            $table->timestamp('order_start_time')->nullable()->comment('下单开始时间');
            $table->timestamp('order_end_time')->nullable()->comment('下单结束时间');
            $table->timestamp('pay_start_time')->nullable()->comment('支付开始时间');
            $table->timestamp('pay_end_time')->nullable()->comment('支付结束时间');
            $table->timestamp('receipt_time')->nullable()->comment('收货时间');

            $table->softDeletes();
            $table->timestamps();

            $table->index('order_sn');
        });

        // 订单商品表
        $schema->create('order_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->comment('订单id');
            $table->integer('sell_product_id')->comment('销售商品id');
            $table->integer('product_id')->comment('商品id');
            $table->tinyInteger('product_provider')->default(0)->comment('商品售卖方式');
            $table->integer('inventory_id')->comment('库存id');
            $table->integer('number')->default(1)->comment('购买商品数量');
            $table->decimal('price')->comment('单价');
            $table->decimal('refundable_amount')->comment('可退款金额');
        });

        // 支付表
        $schema->create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id')->comment('系统订单id');
            $table->tinyInteger('status')->default(0)->comment('订单状态 0未支付完成 1支付完成 2支付失败');
            $table->string('provider', 16)->default('alipay')->comment('支付类型alipay支付宝 wechat微信支付 weapp小程序支付 weweb微信网页支付');
            $table->string('trade_no')->index()->default('')->comment('支付商交易订单号');
            $table->string('total_fee')->default('')->comment('付款的金额');
            $table->string('subject')->default('')->comment('订单名称');
            $table->timestamp('paid_time')->nullable()->comment('支付完成时间');
            $table->tinyInteger('refund')->default(0)->comment('支付退款纪录0未退款 1已退款');
            $table->timestamp('refund_time')->nullable()->comment('退款时间');
            $table->timestamps();
        });

        // 订单配送表
        $schema->create('order_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->comment('订单id');
            $table->tinyInteger('shipping_status')->default(1)->comment('配送状态 1配送中 2已收 3拒收');
            $table->string('com')->comment('快递公司代号');
            $table->string('shipping_sn')->comment('发货单号');
            $table->string('products', 1024)->comment('配送商品冗余');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
        Schema::drop('attributes');
        Schema::drop('attribute_values');
        Schema::drop('product_images');
        Schema::drop('inventories');
        Schema::drop('orders');
        Schema::drop('order_products');
        Schema::drop('payments');
        Schema::drop('order_packages');
    }
}
