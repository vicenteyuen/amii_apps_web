<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateProductInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // sell_priducts销售商品表
        $schema->create('sell_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->comment('商品id');
            $table->integer('product_point')->unsigned()->comment('商品id');
            $table->tinyInteger('provider')->unsigned()->default(1)->comment('商品销售方式 1普通库存 2积分库存 3游戏');
            $table->string('image')->nullable()->comment('销售商品展示图');
            $table->decimal('base_price')->comment('商品基本价格');
            $table->decimal('market_price')->comment('商品市场价格');
            $table->integer('number')->default(0)->unsigned()->comment('商品销售数量');
            $table->integer('status')->default(1)->unsigned()->comment('商品状态 1 上架 0 下架');
            $table->integer('comment_number')->default(0)->unsigned()->comment('销售商品评价数');
            $table->integer('volume')->default(0)->unsigned()->comment('销量');
            $table->integer('month_volume')->default(0)->unsigned()->comment('月销量');
            $table->integer('orderby')->default(0)->unsigned()->comment('排序');
            $table->tinyInteger('puzzles')->comment('拼图总数');
            $table->integer('game_number')->comment('玩游戏总人数');
            $table->integer('success_number')->comment('拼图完成的总人数');
            $table->tinyInteger('praise')->comment('好评');
            $table->timestamps();
            $table->index('product_id');
            $table->index('provider');
        });

        // 商品表 属性表 关联表
        $schema->create('product_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->index();
            $table->integer('attribute_id')->index();
            $table->timestamps();
        });

        // 库存属性值关联表
        $schema->create('inventories_attribute_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_id')->index();
            $table->integer('attribute_value_id')->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sell_products');
        Schema::drop('inventories_attribute_values');
        Schema::drop('product_attributes');
    }
}
