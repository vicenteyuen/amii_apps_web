<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->tinyInteger('status')->default(1);
        });
        Schema::table('subjects', function (Blueprint $table) {
            $table->tinyInteger('status')->default(1);
        });
        Schema::table('physical_stores', function (Blueprint $table) {
            $table->tinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('subjects', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('physical_stores', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
