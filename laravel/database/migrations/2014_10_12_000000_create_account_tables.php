<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateAccountTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 用户表
        $schema->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 64)->default('')->comment('用户唯一标识');
            $table->string('nickname', 128)->default('新用户')->comment('用户昵称');
            $table->string('realname', 20)->nullable()->comment('真实姓名');
            $table->string('phone', 16)->default('')->comment('用户手机号');
            $table->string('password', 64)->comment('用户密码');
            $table->string('payment_password', 64)->nullable()->comment('提现密码');
            $table->string('avatar', 1024)->default('')->comment('用户头像');
            $table->tinyInteger('gender')->default(0)->comment('性别0未设置1女2男');
            $table->tinyInteger('status')->default(1)->comment('用户状态0不可用1可用');
            $table->timestamp('last_log_time')->comment('最后一次登录时间');
            $table->string('last_log_ip', 30)->comment('最后一次登录IP');
            $table->integer('sum_points')->unsigned()->default(0)->comment('总积分');
            $table->integer('points')->unsigned()->default(0)->comment('剩余积分');
            $table->integer('level')->default(1)->comment('等级');
            $table->decimal('purchase_price')->unsigned()->defalut(0)->comment('消费总额');
            $table->decimal('cumulate_profit')->defalut(0)->comment('总收益');
            $table->integer('ordres')->unsigned()->default(0)->comment('订单总数');
            $table->rememberToken();
            $table->string('birthday')->comment('出生日期');
            $table->timestamps();
        });

        // auth token
        $schema->create('tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->index()->comment('登录类型');
            $table->string('ref_id')->index()->comment('登录用户id');
            $table->string('code', 64)->unique()->comment('token');
            $table->integer('expires')->default(86400 * 7)->unsigned()->comment('有效时常');

            $table->timestamps();
        });

        // 三方登录表
        $schema->create('socialites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0)->comment('用户id');
            $table->string('provider', 16)->default('')->comment('三方登录类型');
            $table->string('openid')->default('')->comment('用户登录服务器id');
            $table->string('oauth_token')->default('')->comment('三方服务器准入token');
            $table->string('oauth_token_secret')->default('')->comment('三方服务器准入token密钥');
            $table->string('unionid')->default('')->comment('微信绑定公众帐号用户id');

            $table->timestamps();
        });

        // 管理员表
        $schema->create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 255)->nullable()->unique();
            $table->string('password', 64)->nullable();
            $table->string('nickname', 64)->default('')->comment('管理员昵称');
            $table->string('phone', 16)->default('')->comment('手机号');
            $table->date('last_log_time')->nullable()->comment('最后一次登陆时间');
            $table->string('last_log_ip', 30)->default('')->comment('最后一次登陆ip');
            $table->tinyInteger('status')->default(1)->comment('状态0不可用1可用');
            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('tokens');
        Schema::drop('socialites');
        Schema::drop('admins');
    }
}
