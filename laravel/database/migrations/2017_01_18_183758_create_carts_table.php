<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // carts 购物车
        $schema->create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->integer('inventory_id')->unsigned()->comment('inventory表id');
            $table->integer('sell_product_id')->unsigned()->comment('product_sell_id表id');
            $table->integer('number')->unsigned()->comment('购物车数量');
            $table->timestamps();
        });

        // point_recoreds 积分记录
        $schema->create('point_recoreds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('provider', 10)->comment('order 订单积分 else 其他积分');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->tinyinteger('type')->unsigned()->comment('1 消费 2 签到...');
            $table->decimal('points')->comment('积分值 包括正负');
            $table->timestamps();
        });

        // point_levels 等级表
        $schema->create('point_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('等级名称');
            $table->integer('points')->comment('等级最小分数');
            $table->timestamps();
        });

        // enables 特权表
        $schema->create('enables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('等级名称');
            $table->string('image')->comment('特权描述');
            $table->timestamps();
        });

        // user_enables 用户特权中间表
        $schema->create('user_enables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->integer('enable_id')->unsigned()->comment('特权表id');
            $table->timestamps();
        });

        // point_types 积分类别表
        $schema->create('point_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('类别名称');
            $table->string('description')->comment('积分描述');
            $table->timestamps();
        });

         // tribe 部落表
        $schema->create('tribes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->integer('pid')->unsigned()->comment('父级id');
            $table->decimal('income')->comment('总收益');
            $table->decimal('week_income')->comment('本周收益');
            $table->decimal('month_income')->comment('本月收益');
            $table->decimal('contribute_income')->comment('贡献值');
            $table->integer('week_rank')->unsigned()->comment('本周排名');
            $table->integer('month_rank')->unsigned()->comment('本月排名');
            $table->integer('last_week_rank')->unsigned()->comment('上周排名');
            $table->integer('last_month_rank')->unsigned()->comment('上月排名');
            $table->integer('rank')->unsigned()->comment('总排名');
            $table->timestamps();
            $table->index('income');
            $table->index('week_income');
            $table->index('month_income');
            $table->index('user_id');
        });

         // gains 收益表
        $schema->create('gains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->integer('p_user_id')->unsigned()->comment('父级user表id');
            $table->decimal('income')->comment('收益');
            $table->decimal('total_orders')->comment('订单总额');
            $table->timestamps();
            $table->index('p_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carts');
        Schema::drop('point_recoreds');
        Schema::drop('point_levels');
        Schema::drop('enables');
        Schema::drop('user_enables');
        Schema::drop('point_types');
        Schema::drop('tribes');
        Schema::drop('gains');
    }
}
