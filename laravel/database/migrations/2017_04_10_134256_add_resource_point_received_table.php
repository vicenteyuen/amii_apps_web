<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResourcePointReceivedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_received', function (Blueprint $table) {
            $table->string('resource')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0 未读 1已读');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_received', function (Blueprint $table) {
            $table->dropColumn('resource');
            $table->dropColumn('status');
        });
    }
}
