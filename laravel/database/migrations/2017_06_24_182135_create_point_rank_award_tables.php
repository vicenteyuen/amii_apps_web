<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointRankAwardTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 积分排名期数表
        Schema::create('point_rank_awards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('')->comment('期数名称');
            $table->tinyInteger('status')->default(0)->comment('期数状态 0未结算完成 1已结算完成');
            $table->tinyInteger('is_publish')->default(0)->comment('发布状态 0未发布 1已发布并显示 2已发布不显示');
            $table->tinyInteger('provider')->default(0)->comment('期数类型 月排名1 年排名2');
            $table->timestamp('start_time')->nullable()->comment('期数开始时间');
            $table->timestamp('end_time')->nullable()->comment('期数截止时间');

            $table->timestamps();
        });

        // 期数获奖人员名次表
        Schema::create('point_rank_award_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('point_rank_award_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('placed')->default(0)->comment('名次');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_rank_awards');
        Schema::dropIfExists('point_rank_award_users');
    }
}
