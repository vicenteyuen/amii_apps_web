<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeIdToShareRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('share_relations', function (Blueprint $table) {
            $table->integer('code_id')->after('video_id')->comment('用户二维码');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('share_relations', function (Blueprint $table) {
            $table->dropColumn('code_id');
        });
    }
}
