<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMerchantIdToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deepdraw_product_items', function (Blueprint $table) {
            $table->integer('merchant_id')->nullable()->after('deepdraw_product_id');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->integer('merchant_id')->nullable()->after('deepdraw_id');
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->integer('merchant_id')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deepdraw_product_items', function (Blueprint $table) {
            $table->dropColumn('merchant_id');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('merchant_id');
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->dropColumn('merchant_id');
        });
    }
}
