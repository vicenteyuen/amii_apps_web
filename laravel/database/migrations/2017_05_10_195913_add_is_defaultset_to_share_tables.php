<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDefaultsetToShareTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shares', function (Blueprint $table) {
            $table->tinyInteger('is_defaultTitle')->default(0)->comment('是否默认标题');
            $table->tinyInteger('is_defaultContent')->default(0)->comment('是否默认内容');
            $table->tinyInteger('is_defaultImg')->default(0)->comment('是否默认图片');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('shares', function (Blueprint $table) {
             $table->dropColumn('is_defaultTitle');
             $table->dropColumn('is_defaultContent');
             $table->dropColumn('is_defaultImg');
        });
    }
}
