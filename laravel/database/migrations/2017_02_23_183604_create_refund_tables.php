<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateRefundTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 售后记录表
        $schema->create('order_refunds', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(0)->comment('申请售后状态 0申请中 1已完成 2已拒绝');
            $table->tinyInteger('refund_status')->default(0)->comment('售后状态 0申请中 1商家已同意 2商家已退款 3到账 4已拒绝');
            $table->string('provider')->default('')->comment('售后类型 pay仅退款 product退货退款');
            $table->integer('order_product_id')->comment('订单产品id');
            $table->decimal('pay')->default(0)->comment('售后退款金额');
            $table->timestamp('pay_time')->nullable()->comment('退款完成时间');
            $table->text('applied_data')->nullable()->comment('申请数据');
            $table->text('refuse_reason')->cmment('商家拒绝原因');
            $table->string('out_refund_no')->nullable()->comment('退款订单编号');
            $table->timestamps();
        });

        // 售后内容记录表
        $schema->create('order_refund_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_refund_id')->comment('售后记录表id');
            $table->string('title')->default('')->comment('售后记录标题');
            $table->text('content')->default('')->comment('售后记录内容');
            $table->tinyInteger('is_express')->default(0)->comment('是否开启填写退货物流信息');
            $table->boolean('is_user_record')->default(0)->comment('是否用户记录');

            $table->timestamps();
        });

        // 售后记录图片表
        $schema->create('order_refund_log_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_refund_log_id')->comment('售后记录id');
            $table->string('value')->default('')->comment('售后记录图片');

            $table->timestamps();
        });

        // 售后退款表
        $schema->create('order_refundments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_refund_id')->comment('售后记录表id');
            $table->tinyInteger('status')->default(0)->comment('退款状态 0未完成 1完成 2失败');
            $table->string('provider', 16)->default('alipay')->comment('退款类型');
            $table->string('trade_no')->index()->default('')->comment('支付商交易订单号');
            $table->string('total_fee')->default('')->comment('付款的金额');
            $table->string('subject')->default('')->comment('付款单名称');
            $table->timestamp('paid_time')->nullable()->comment('付款完成时间');

            $table->timestamps();
        });

        // 售后买家退货表
        $schema->create('order_refund_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->comment('售后记录表id');
            $table->string('express_name')->comment('快递公司');
            $table->string('shipping_sn')->comment('发货单号');
            $table->string('is_receive')->default(0)->comment('卖家是否收到货');
            $table->timestamp('receive_time')->nullable()->comment('卖家收货时间');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_refunds');
        Schema::dropIfExists('order_refund_logs');
        Schema::dropIfExists('order_refund_log_images');
        Schema::dropIfExists('order_refundments');
        Schema::dropIfExists('order_refund_products');
    }
}
