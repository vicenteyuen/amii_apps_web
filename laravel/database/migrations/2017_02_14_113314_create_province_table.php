<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateProvinceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

         // province
        $schema->create('provinces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('province_id')->comment('id');
            $table->string('name', 40)->comment('名称');
            $table->string('lat')->comment('纬度');
            $table->string('lng')->comment('经度');
            $table->timestamps();
        });

         // city
        $schema->create('citys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->comment('id');
            $table->integer('province_id')->comment('id');
            $table->string('name', 40)->comment('名称');
            $table->string('lat')->comment('纬度');
            $table->string('lng')->comment('经度');
            $table->timestamps();
        });

         // area
        $schema->create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_id')->comment('id');
            $table->integer('city_id')->comment('id');
            $table->string('name', 40)->comment('全名');
            $table->string('lat')->comment('纬度');
            $table->string('lng')->comment('经度');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('provinces');
        Schema::drop('citys');
        Schema::drop('areas');
    }
}
