<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        Schema::create('share_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('share_id')->comment('share表id');
            $table->Integer('sell_product_id')->comment('分享端口商品id');
            $table->Integer('subject_id')->comment('专题id');
            $table->Integer('video_id')->comment('视频id');
            $table->Integer('tribe_id')->comment('部落id');
            $table->Integer('activity_id')->comment('专区活动id');
            $table->Integer('puzzle_game_id')->comment('拼图游戏id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('share_relations');
    }
}
