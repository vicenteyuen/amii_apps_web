<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateUserInfosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // user_address 用户地址表
        $schema->create('user_address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('region_id', 10)->comment('地址数据最底层id');
            $table->string('post_code')->comment('邮编');
            $table->string('detail', 100)->default('')->comment('详细地址');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->tinyInteger('status')->unsigned()->comment('默认地址 1是 0否');
            $table->string('phone', '11')->comment('电话号码');
            $table->string('realname', '100')->comment('真实姓名');
            $table->index('user_id');
            $table->softDeletes();
            $table->timestamps();
        });

        // user_collections 产品收藏表
        $schema->create('user_collections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->integer('sell_product_id')->unsigned()->comment('商品id');
            $table->index('user_id');
            $table->softDeletes();
            $table->timestamps();
        });

        // user_scan_logs 浏览过的商品表
        $schema->create('user_scan_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->integer('sell_product_id')->unsigned()->comment('商品id');
            $table->index('user_id');
            $table->softDeletes();
            $table->timestamps();
        });


        // user_suits 用户身材数据表
        $schema->create('user_suits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->float('height')->nullable()->comment('身高 单位cm');
            $table->float('weight')->nullable()->comment('体重 单位kg');
            $table->float('shoulder_width')->nullable()->comment('肩宽');
            $table->float('thigh_width')->nullable()->comment('大腿围');
            $table->string('bust')->nullable()->comment('胸围 单位cm');
            $table->string('waist')->nullable()->comment('腰围 单位cm');
            $table->string('hips')->nullable()->comment('臀围 单位cm');
            $table->string('status')->nullable()->comment('默认地址 1 是 0否');
            $table->string('name')->nullable()->comment('姓名');
            $table->tinyInteger('skin')->nullable()->comment('1 黄色 2 白色 3 黑色');
            $table->tinyInteger('hair')->nullable()->comment('1 黑色 2 白色 3 金黄色');
            $table->string('front_photo')->nullable()->comment('正面照片');
            $table->string('side_photo')->nullable()->comment('侧面照片');
            $table->string('size_id')->comment('推荐尺寸id');
            $table->index('user_id');
            $table->timestamps();
        });

        // user_scan_logs 浏览过的商品表
        $schema->create('suit_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->comment('1 上装尺码 2 下装尺码 3 肤色 4 发色');
            $table->integer('question_id');
            $table->string('kinds', 10)->comment('类别');
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_address');
        Schema::drop('user_collections');
        Schema::drop('user_scan_logs');
        Schema::drop('user_suits');
        Schema::drop('suit_sizes');
    }
}
