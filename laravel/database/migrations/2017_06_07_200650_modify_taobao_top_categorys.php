<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTaobaoTopCategorys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taobao_top_categorys', function (Blueprint $table) {
            $table->integer('category_id')->default(0)->after('name')->comment('分类id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taobao_top_categorys', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
}
