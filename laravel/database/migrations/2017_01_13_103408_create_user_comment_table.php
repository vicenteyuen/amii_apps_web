<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateUserCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // user_comments 用户评价表
        $schema->create('user_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
            $table->integer('inventory_id')->unsigned()->comment('inventory_id');
            $table->integer('status')->unsigned()->comment('是否匿名 1 是 0否');
            $table->integer('product_sell_id')->unsigned()->comment('product_sell_id表id');
            $table->text('content')->comment('评价商品内容');
            $table->tinyInteger('comment_rank_id')->comment('星级');
            $table->tinyInteger('comment_status')->default(0)->comment('0 开启访评论 1 关闭该评论');
            $table->softDeletes();
            $table->timestamps();
        });

        // user_comments 评价图片表
        $schema->create('comment_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comment_id')->unsigned()->comment('user_comments表id');
            $table->string('image')->comment('图片名称');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_comments');
        Schema::drop('comment_images');
    }
}
