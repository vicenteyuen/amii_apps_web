<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserLastWeekRankToTribeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tribes', function (Blueprint $table) {
            $table->integer('user_last_rank')->default(0);
            $table->integer('user_current_rank')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tribes', function (Blueprint $table) {
            $table->dropColumn('user_last_rank');
            $table->dropColumn('user_current_rank');
        });
    }
}
