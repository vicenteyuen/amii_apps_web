<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateFirstPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        $schema->create('first_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->comment('1 开启 0 关闭');
            $table->integer('sell_product_id')->comment('赠送的商品id(普通商品)');
            $table->integer('count')->comment('赠品已送总人数');
            $table->decimal('money')->comment('首单购金额');
            $table->timestamps();
        });

        $schema->create('give_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_product_id')->comment('赠送商品的id');
            $table->integer('user_id')->comment('user id');
            $table->integer('order_id')->comment('订单id');
            $table->integer('activity_id')->comment('活动id');
            $table->integer('status')->comment('添加记录, 1 实付款超过200,0 实付款不超过200');
            $table->timestamps();
        });

        $schema->create('give_product_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_product_id')->comment('赠送商品的id');
            $table->integer('count')->comment('当前商品已送人数');
            $table->timestamps();
        });

        $schema->create('first_purchase_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text')->comment('首单购文本');
            $table->string('provider', 40)->default('first')->comment('首单购标识');
            $table->timestamps();
        });

        $schema->create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->comment('活动标题');
            $table->text('order_description')->comment('订单描述');
            $table->string('provider', 40)->default('first')->comment('活动标识');
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('first_purchases');
        Schema::dropIfExists('give_products');
        Schema::dropIfExists('give_product_histories');
        Schema::dropIfExists('first_purchase_texts');
        Schema::dropIfExists('activities');
    }
}
