<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTodayTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('today_tasks', function (Blueprint $table) {
            $table->string('type')->comment('任务类型')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('today_tasks', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
