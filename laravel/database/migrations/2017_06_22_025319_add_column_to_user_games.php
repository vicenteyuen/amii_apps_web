<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUserGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_games', function (Blueprint $table) {
            $table->timestamp('gaming_end_time')->nullable()->comment('游戏进行中结束时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_games', function (Blueprint $table) {
            $table->dropColumn('gaming_end_time');
        });
    }
}
