<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameShareDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        Schema::create('game_push_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('provider')->comment('1 好友激活,2 进行中,3未合成,4拼图完成未合成,5未完成拼图 ,6完成拼图未合成超时 ');
            $table->string('desc')->comment('描述');
            $table->integer('hours')->default(0)->comment('小时');
            $table->tinyInteger('status')->default(1)->comment('默认文本');
            $table->string('redundant')->default('')->comment('冗余字段');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_push_documents');
    }
}
