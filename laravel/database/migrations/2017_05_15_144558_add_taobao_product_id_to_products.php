<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaobaoProductIdToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 商品表添加天猫商品标记
        Schema::table('products', function (Blueprint $table) {
            $table->integer('taobao_product_id')->nullable()->after('merchant_id');
        });
        // 品牌表添加天猫商户标记
        Schema::table('brands', function (Blueprint $table) {
            $table->string('taobao_brand')->nullable()->after('merchant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('taobao_product_id');
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->dropColumn('taobao_brand');
        });
    }
}
