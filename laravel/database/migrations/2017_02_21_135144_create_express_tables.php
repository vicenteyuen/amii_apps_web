<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateExpressTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 运费模板
        $schema->create('express_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('')->comment('运费模版名称');
            $table->tinyInteger('free')->default(0)->comment('是否包邮 0表示自定义包邮，1为全部包邮');
            $table->tinyInteger('assign')->default(0)->comment('是否开启指定条件包邮');
            $table->tinyInteger('calc_type')->comment('1 默认 2 按件 3按重量');
            $table->timestamps();
        });

        // express 快递公司
        $schema->create('express', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128)->comment('快递公司名称');
            $table->string('com', 128)->comment('快递公司标识');
            $table->integer('orderby')->comment('快递排序');
            $table->tinyInteger('status')->default(1)->comment('状态 1可用 0停用');

            $table->timestamps();
        });

        // 物流信息
        $schema->create('express_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_package_id')->comment('订单包裹id');
            $table->text('info')->comment('物流信息');
            $table->tinyInteger('is_poll')->comment('是否可订阅');

            $table->timestamps();
        });

        // 快递公司运费
        $schema->create('express_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('express_template_id');
            $table->decimal('default_fee')->comment('运费默认价格');
            $table->integer('free_fee')->comment('包邮门槛');
        });

        // 运费模板免运费地区
        $schema->create('express_conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('express_fee_id')->comment('快递公司运费模版默认运费id');
            $table->integer('region_code')->comment('地区标识');
            $table->decimal('default_fee')->comment('价格');
            $table->decimal('free_fee')->comment('包邮门槛');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('express_templates');
        Schema::dropIfExists('express');
        Schema::dropIfExists('express_logs');
        Schema::dropIfExists('express_fees');
        Schema::dropIfExists('express_conditions');
    }
}
