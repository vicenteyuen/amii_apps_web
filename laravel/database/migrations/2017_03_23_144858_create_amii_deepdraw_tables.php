<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreateAmiiDeepdrawTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        // 商户期数表
        $schema->create('deepdraw_sale_calenders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_id')->comment('上货期数id');
            $table->string('merchant_id')->comment('商户id');
            $table->string('day')->comment('上货期数日期');
            $table->tinyInteger('status')->default(0)->comment('上货期数是否上货到系统 0未上货 1已上货');

            $table->timestamps();
        });


        // DeepDraw ProductItems
        $schema->create('deepdraw_product_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('deepdraw_product_id')->comment('deepdraw product id');

            $table->tinyInteger('is_publish')->default(0)->comment('是否发布 0未发布 1已发布');

            $table->text('basic')->default('')->comment('商品基本信息');
            $table->tinyInteger('basic_status')->default(0)->comment('商品基本信息-状态');

            $table->text('picture')->default('')->comment('商品图片资源');
            $table->tinyInteger('picture_status')->default(0)->comment('商品图片资源-状态');

            $table->text('picture_handle')->default('')->comment('商品图片处理后的资源');
            $table->tinyInteger('picture_handle_status')->default(0)->comment('商品图片是否处理 0未处理 1已处理');
            $table->tinyInteger('picture_handle_queue_status')->default(0)->comment('商品图片是否进入队列处理 0否 1是');

            $table->text('vision')->comment('详情页信息');
            $table->tinyInteger('vision_status')->default(0)->comment('详情页信息-状态');

            $table->text('vision_handle')->default('')->comment('商品详情处理后的资源');
            $table->tinyInteger('vision_handle_status')->default(0)->comment('商品详情是否处理 0未处理 1已处理');
            $table->tinyInteger('vision_handle_queue_status')->default(0)->comment('商品详情是否进入队列处理 0否 1是');

            $table->text('sku')->comment('sku数据');
            $table->tinyInteger('sku_status')->default(0)->comment('sku数据-状态');

            $table->timestamps();
        });

        // deepdraw category
        $schema->create('deepdraw_categorys', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('deepdraw_category_id')->default(0)->comment('分类id');
            $table->text('data')->default('')->comment('子树数据');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deepdraw_sale_calenders');
        Schema::dropIfExists('deepdraw_product_items');
        Schema::dropIfExists('deepdraw_categorys');
    }
}
