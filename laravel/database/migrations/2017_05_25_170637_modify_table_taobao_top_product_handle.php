<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTableTaobaoTopProductHandle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taobao_top_product_handles', function (Blueprint $table) {
            $table->integer('is_sync_update')->nullable()->after('is_publish');
            $table->timestamp('sync_update_start_time')->nullable()->after('is_sync_update');
            $table->timestamp('sync_update_end_time')->nullable()->after('is_sync_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taobao_top_product_handles', function (Blueprint $table) {
            $table->dropColumn('is_sync_update');
            $table->dropColumn('sync_update_start_time');
            $table->dropColumn('sync_update_end_time');
        });
    }
}
