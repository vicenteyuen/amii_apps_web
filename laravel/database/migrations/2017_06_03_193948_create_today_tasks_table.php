<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodayTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema =  DB::connection()->getSchemaBuilder();
        $schema->BlueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        Schema::create('today_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->comment('任务类型');
            $table->string('title')->comment('任务标题');
            $table->string('content')->comment('任务内容');
            $table->string('image')->comment('任务封面图');
            $table->string('task_points')->nullable()->comment('任务积分');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('today_tasks');
    }
}
