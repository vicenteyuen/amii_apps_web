<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProviderToUserCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_collections', function (Blueprint $table) {
            $table->tinyInteger('provider')->default(1)->comment('1普通商品 2游戏商品');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_collections', function (Blueprint $table) {
            $table->dropColumn('provider');
        });
    }
}
