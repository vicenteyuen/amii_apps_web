<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellProductAttributeValueImageToSellAttributeValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sell_attribute_values', function (Blueprint $table) {
            $table->string('sell_product_attribute_value_image')->comment('图片名称');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sell_attribute_values', function (Blueprint $table) {
            $table->dropColumn('sell_product_attribute_value_image');
        });
    }
}
