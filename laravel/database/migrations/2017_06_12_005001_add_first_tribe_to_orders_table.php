<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirstTribeToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('first_tribe_return_money')->default(null)->nullable()->comment('一级部落收益');
            $table->decimal('second_tribe_return_money')->default(null)->nullable()->comment('一级部落收益');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('first_tribe_return_money');
            $table->dropColumn('second_tribe_return_money');
        });
    }
}
