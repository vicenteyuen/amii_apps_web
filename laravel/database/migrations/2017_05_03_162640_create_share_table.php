<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });
        // 文案分享
        Schema::create('shares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 64)->comment('分享标题,若为默认标题值为2');
            $table->string('image')->comment('分享文案的图片');
            $table->tinyInteger('provider')->default(0)->comment('分享的端口类型');
            $table->tinyInteger('is_default')->default(1)->comment('是否默认使用');

            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('shares');
    }
}
