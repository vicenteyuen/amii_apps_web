<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJumpUrlToTodayTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('today_tasks', function (Blueprint $table) {
            $table->string('jump_url')->comment('跳转url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('today_tasks', function (Blueprint $table) {
            $table->dropColumn('jump_url');
        });
    }
}
