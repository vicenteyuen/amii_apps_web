<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreatePointsRankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

         // tribe 部落表
        $schema->create('points_rank', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('user表id');
          
            $table->integer('year_income')->comment('本年收益');
            $table->integer('month_income')->comment('本月收益');
    
            $table->integer('year_rank')->unsigned()->comment('本年排名');
            $table->integer('month_rank')->unsigned()->comment('本月排名');
            $table->integer('last_year_rank')->unsigned()->comment('上年排名');
            $table->integer('last_month_rank')->unsigned()->comment('上月排名');
      
            $table->timestamps();
            $table->index('year_income');
            $table->index('month_income');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('points_rank');
    }
}
