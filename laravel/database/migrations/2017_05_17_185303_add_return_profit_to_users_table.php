<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReturnProfitToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('give_products', function (Blueprint $table) {
            $table->tinyInteger('return_profit')->default(0)->comment('0 未返回首单金额 1 返回');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('give_products', function (Blueprint $table) {
            $table->dropColumn('return_profit');
        });
    }
}
