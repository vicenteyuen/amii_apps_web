<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyWithdrawRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdraw_records', function (Blueprint $table) {
            $table->decimal('amount')->comment('提现金额')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdraw_records', function (Blueprint $table) {
            $table->integer('amount')->comment('提现金额')->change();
        });
    }
}
