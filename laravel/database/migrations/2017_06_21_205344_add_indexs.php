<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // activities index
        Schema::table('activities', function (Blueprint $table) {
            $table->index('provider');
        });
        // admins index
        Schema::table('admins', function (Blueprint $table) {
            $table->index('phone');
        });
        // areas index
        Schema::table('areas', function (Blueprint $table) {
            $table->index('area_id');
            $table->index('city_id');
            $table->index('name');
            $table->index('lat');
            $table->index('lng');
        });
        // balance index
        Schema::table('balance', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('type');
            $table->index('value');
            $table->index('come_user_id');
            $table->index('order_id');
            $table->index('status');
        });
        // brand_categorys index
        Schema::table('brand_categorys', function (Blueprint $table) {
            $table->index('brand_id');
            $table->index('category_id');
        });
        // brand_products index
        Schema::table('brand_products', function (Blueprint $table) {
            $table->index('brand_id');
            $table->index('product_id');
        });
        // brand_sell_products index
        Schema::table('brand_sell_products', function (Blueprint $table) {
            $table->index('brand_id');
            $table->index('sell_product_id');
        });
        // brands index
        Schema::table('brands', function (Blueprint $table) {
            $table->index('name');
            $table->index('merchant_id');
        });
        // carts index
        Schema::table('carts', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('inventory_id');
            $table->index('sell_product_id');
        });
        // categories index
        Schema::table('categories', function (Blueprint $table) {
            $table->index('name');
            $table->index('parent_id');
        });
        // channels index
        Schema::table('channels', function (Blueprint $table) {
            $table->index('code');
        });
        // citys index
        Schema::table('citys', function (Blueprint $table) {
            $table->index('city_id');
            $table->index('province_id');
            $table->index('name');
        });
        // comment_images index
        Schema::table('comment_images', function (Blueprint $table) {
            $table->index('comment_id');
        });
        // comment_ranks index
        Schema::table('comment_ranks', function (Blueprint $table) {
            $table->index('rank');
        });
        // coupon_grants index
        Schema::table('coupon_grants', function (Blueprint $table) {
            $table->index('coupon_id');
            $table->index('status');
        });
        // coupons index
        Schema::table('coupons', function (Blueprint $table) {
            $table->index('remain');
            $table->index('provider');
            $table->index('start');
            $table->index('end');
        });
        // express index
        Schema::table('express', function (Blueprint $table) {
            $table->index('com');
            $table->index('name');
            $table->index('status');
            $table->index('orderby');
        });
        // express_conditions index
        Schema::table('express_conditions', function (Blueprint $table) {
            $table->index('express_fee_id');
            $table->index('region_code');
        });
        // express_fees index
        Schema::table('express_fees', function (Blueprint $table) {
            $table->index('express_template_id');
        });
        // express_logs index
        Schema::table('express_logs', function (Blueprint $table) {
            $table->index('order_package_id');
            $table->index('is_poll');
        });
        // feedbacks index
        Schema::table('feedbacks', function (Blueprint $table) {
            $table->index('status');
            $table->index('user_id');
        });
        // first_purchase_texts index
        Schema::table('first_purchase_texts', function (Blueprint $table) {
            $table->index('provider');
        });
        // first_purchases index
        Schema::table('first_purchases', function (Blueprint $table) {
            $table->index('status');
            $table->index('sell_product_id');
            $table->index('count');
            $table->index('money');
        });
        // gains index
        Schema::table('gains', function (Blueprint $table) {
            $table->index('user_id');
        });
        // game_push_documents index
        Schema::table('game_push_documents', function (Blueprint $table) {
            $table->index('provider');
            $table->index('status');
        });
        // game_relation_users index
        Schema::table('game_relation_users', function (Blueprint $table) {
            $table->index('user_games_id');
            $table->index('user_id');
        });
        // give_product_histories index
        Schema::table('give_product_histories', function (Blueprint $table) {
            $table->index('sell_product_id');
        });
        // give_products index
        Schema::table('give_products', function (Blueprint $table) {
            $table->index('sell_product_id');
            $table->index('user_id');
            $table->index('order_id');
            $table->index('activity_id');
            $table->index('return_profit');
        });
        // hot_search index
        Schema::table('hot_search', function (Blueprint $table) {
            $table->index('name');
            $table->index('number');
        });
        // images index
        Schema::table('images', function (Blueprint $table) {
            $table->index('provider');
            $table->index('image_file_id');
        });
        // inventories index
        Schema::table('inventories', function (Blueprint $table) {
            $table->index('product_id');
            $table->index('sku_code');
            $table->index('number');
        });
        // level_enables index
        Schema::table('level_enables', function (Blueprint $table) {
            $table->index('level_id');
            $table->index('enable_id');
        });
        // notify_messages index
        Schema::table('notify_messages', function (Blueprint $table) {
            $table->index('status');
            $table->index('user_id');
            $table->index('provider');
            $table->index('deleted_at');
        });
        // order_packages index
        Schema::table('order_packages', function (Blueprint $table) {
            $table->index('order_id');
            $table->index('com');
            $table->index('shipping_sn');
        });
        // order_products index
        Schema::table('order_products', function (Blueprint $table) {
            $table->index('order_id');
            $table->index('sell_product_id');
            $table->index('product_id');
            $table->index('product_provider');
            $table->index('inventory_id');
        });
        // order_refund_log_images index
        Schema::table('order_refund_log_images', function (Blueprint $table) {
            $table->index('order_refund_log_id');
        });
        // order_refund_logs index
        Schema::table('order_refund_logs', function (Blueprint $table) {
            $table->index('order_refund_id');
        });
        // order_refund_products index
        Schema::table('order_refund_products', function (Blueprint $table) {
            $table->index('order_id');
            $table->index('shipping_sn');
        });
        // order_refundments index
        Schema::table('order_refundments', function (Blueprint $table) {
            $table->index('order_refund_id');
            $table->index('status');
            $table->index('provider');
        });
        // order_refunds index
        Schema::table('order_refunds', function (Blueprint $table) {
            $table->index('status');
            $table->index('refund_status');
            $table->index('provider');
            $table->index('order_product_id');
        });
        // orders index
        Schema::table('orders', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('order_status');
            $table->index('pay_status');
            $table->index('shipping_status');
            $table->index('cart_ids');
        });
        // payments index
        Schema::table('payments', function (Blueprint $table) {
            $table->index('order_id');
            $table->index('status');
            $table->index('provider');
            $table->index('refund');
        });
        // physical_stores index
        Schema::table('physical_stores', function (Blueprint $table) {
            $table->index('name');
            $table->index('name_initial');
        });
        // point_received index
        Schema::table('point_received', function (Blueprint $table) {
            $table->index('status');
        });
        // point_recoreds index
        Schema::table('point_recoreds', function (Blueprint $table) {
            $table->index('provider');
            $table->index('user_id');
            $table->index('type');
        });
        // populars index
        Schema::table('populars', function (Blueprint $table) {
            $table->index('parent_id');
        });
        // product_categories index
        Schema::table('product_categories', function (Blueprint $table) {
            $table->index('brand_id');
        });
        // product_images index
        Schema::table('product_images', function (Blueprint $table) {
            $table->index('product_id');
        });
        // products index
        Schema::table('products', function (Blueprint $table) {
            $table->index('sell_point');
            $table->index('snumber');
            $table->index('number');
            $table->index('status');
        });
        // provinces index
        Schema::table('provinces', function (Blueprint $table) {
            $table->index('province_id');
            $table->index('name');
            $table->index('lat');
            $table->index('lng');
        });
        // sell_attribute_values index
        Schema::table('sell_attribute_values', function (Blueprint $table) {
            $table->index('sell_attribute_id');
            $table->index('attribute_value_id');
            $table->index('attribute_id');
        });
        // sell_attributes index
        Schema::table('sell_attributes', function (Blueprint $table) {
            $table->index('attribute_id');
            $table->index('product_sell_attribute_id');
        });
        // sell_products index
        Schema::table('sell_products', function (Blueprint $table) {
            $table->index('product_point');
            $table->index('base_price');
            $table->index('market_price');
            $table->index('number');
            $table->index('number_zero_time');
            $table->index('puzzles');
            $table->index('game_number');
        });
        // share_relations index
        Schema::table('share_relations', function (Blueprint $table) {
            $table->index('share_id');
            $table->index('sell_product_id');
            $table->index('subject_id');
            $table->index('video_id');
            $table->index('code_id');
            $table->index('tribe_id');
            $table->index('activity_id');
            $table->index('puzzle_game_id');
        });
        // share_types index
        Schema::table('share_types', function (Blueprint $table) {
            $table->index('provider_id');
        });
        // shares index
        Schema::table('shares', function (Blueprint $table) {
            $table->index('provider');
            $table->index('is_default');
            $table->index('is_defaultTitle');
            $table->index('is_defaultContent');
            $table->index('is_defaultImg');
            $table->index('is_shareAll');
        });
        // sizes index
        Schema::table('sizes', function (Blueprint $table) {
            $table->index('least_height');
            $table->index('most_height');
        });
        // socialites index
        Schema::table('socialites', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('openid');
            $table->index('unionid');
        });
        // subject_products index
        Schema::table('subject_products', function (Blueprint $table) {
            $table->index('product_id');
            $table->index('subject_id');
        });
        // subject_views index
        Schema::table('subject_views', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('subject_id');
        });
        // subjects index
        Schema::table('subjects', function (Blueprint $table) {
            $table->index('scan');
            $table->index('status');
        });
        // taobao_top_categorys index
        Schema::table('taobao_top_categorys', function (Blueprint $table) {
            $table->index('cid');
            $table->index('parent_cid');
            $table->index('name');
            $table->index('category_id');
        });
        // taobao_top_inventorys index
        Schema::table('taobao_top_inventorys', function (Blueprint $table) {
            $table->index('taobao_outer_id');
            $table->index('is_handle');
        });
        // taobao_top_product_handles index
        Schema::table('taobao_top_product_handles', function (Blueprint $table) {
            $table->index('taobao_top_inventory_id');
            $table->index('taobao_outer_id');
            $table->index('is_publish');
            $table->index('check_skus');
            $table->index('check_cate');
        });
        // taobao_top_sync_logs index
        Schema::table('taobao_top_sync_logs', function (Blueprint $table) {
            $table->index('start_sync_time');
            $table->index('end_sync_time');
            $table->index('request_start_sync_time');
            $table->index('request_end_sync_time');
        });
        // today_tasks index
        Schema::table('today_tasks', function (Blueprint $table) {
            $table->index('type');
        });
        // user_answer index
        Schema::table('user_answer', function (Blueprint $table) {
            $table->index('question_id');
            $table->index('user_id');
            $table->index('user_suit_id');
            $table->index('option_id');
        });
        // user_collections index
        Schema::table('user_collections', function (Blueprint $table) {
            $table->index('sell_product_id');
        });
        // user_comments index
        Schema::table('user_comments', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('inventory_id');
            $table->index('status');
            $table->index('product_sell_id');
            $table->index('comment_status');
        });
        // user_coupons index
        Schema::table('user_coupons', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('coupon_id');
            $table->index('status');
        });
        // user_enables index
        Schema::table('user_enables', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('enable_id');
        });
        // user_game_advices index
        Schema::table('user_game_advices', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('provider');
            $table->index('status');
        });
        // user_games index
        Schema::table('user_games', function (Blueprint $table) {
            $table->index('sell_product_id');
            $table->index('success');
            $table->index('order_id');
            $table->index('order_status');
            $table->index('expired');
        });
        // user_notify_messages index
        Schema::table('user_notify_messages', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('notify_messages_id');
        });
        // user_scan_logs index
        Schema::table('user_scan_logs', function (Blueprint $table) {
            $table->index('sell_product_id');
        });
        // users index
        Schema::table('users', function (Blueprint $table) {
            $table->index('phone');
            $table->index('uuid');
        });
        // video_likes index
        Schema::table('video_likes', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('video_id');
        });
        // withdraw_channels index
        Schema::table('withdraw_channels', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('type');
        });
        // withdraw_records index
        Schema::table('withdraw_records', function (Blueprint $table) {
            $table->index('state');
            $table->index('channel_id');
            $table->index('trade_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropIndex('provider');
        });
        Schema::table('admins', function (Blueprint $table) {
            $table->dropIndex('phone');
        });
        Schema::table('areas', function (Blueprint $table) {
            $table->dropIndex('area_id');
            $table->dropIndex('city_id');
            $table->dropIndex('name');
            $table->dropIndex('lat');
            $table->dropIndex('lng');
        });
        Schema::table('balance', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('type');
            $table->dropIndex('value');
            $table->dropIndex('come_user_id');
            $table->dropIndex('order_id');
            $table->dropIndex('status');
        });
        Schema::table('brand_categorys', function (Blueprint $table) {
            $table->dropIndex('brand_id');
            $table->dropIndex('category_id');
        });
        Schema::table('brand_products', function (Blueprint $table) {
            $table->dropIndex('brand_id');
            $table->dropIndex('product_id');
        });
        Schema::table('brand_sell_products', function (Blueprint $table) {
            $table->dropIndex('brand_id');
            $table->dropIndex('sell_product_id');
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->dropIndex('name');
            $table->dropIndex('merchant_id');
        });
        Schema::table('carts', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('inventory_id');
            $table->dropIndex('sell_product_id');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->dropIndex('name');
            $table->dropIndex('parent_id');
        });
        Schema::table('channels', function (Blueprint $table) {
            $table->dropIndex('code');
        });
        Schema::table('citys', function (Blueprint $table) {
            $table->dropIndex('city_id');
            $table->dropIndex('province_id');
            $table->dropIndex('name');
        });
        Schema::table('comment_images', function (Blueprint $table) {
            $table->dropIndex('comment_id');
        });
        Schema::table('comment_ranks', function (Blueprint $table) {
            $table->dropIndex('rank');
        });
        Schema::table('coupon_grants', function (Blueprint $table) {
            $table->dropIndex('coupon_id');
            $table->dropIndex('status');
        });
        Schema::table('coupons', function (Blueprint $table) {
            $table->dropIndex('remain');
            $table->dropIndex('provider');
            $table->dropIndex('start');
            $table->dropIndex('end');
        });
        Schema::table('express', function (Blueprint $table) {
            $table->dropIndex('com');
            $table->dropIndex('name');
            $table->dropIndex('status');
            $table->dropIndex('orderby');
        });
        Schema::table('express_conditions', function (Blueprint $table) {
            $table->dropIndex('express_fee_id');
            $table->dropIndex('region_code');
        });
        Schema::table('express_fees', function (Blueprint $table) {
            $table->dropIndex('express_template_id');
        });
        Schema::table('express_logs', function (Blueprint $table) {
            $table->dropIndex('order_package_id');
            $table->dropIndex('is_poll');
        });
        Schema::table('feedbacks', function (Blueprint $table) {
            $table->dropIndex('status');
            $table->dropIndex('user_id');
        });
        Schema::table('first_purchase_texts', function (Blueprint $table) {
            $table->dropIndex('provider');
        });
        Schema::table('first_purchases', function (Blueprint $table) {
            $table->dropIndex('status');
            $table->dropIndex('sell_product_id');
            $table->dropIndex('count');
            $table->dropIndex('money');
        });
        Schema::table('gains', function (Blueprint $table) {
            $table->dropIndex('user_id');
        });
        Schema::table('game_push_documents', function (Blueprint $table) {
            $table->dropIndex('provider');
            $table->dropIndex('status');
        });
        Schema::table('game_relation_users', function (Blueprint $table) {
            $table->dropIndex('user_games_id');
            $table->dropIndex('user_id');
        });
        Schema::table('give_product_histories', function (Blueprint $table) {
            $table->dropIndex('sell_product_id');
        });
        Schema::table('give_products', function (Blueprint $table) {
            $table->dropIndex('sell_product_id');
            $table->dropIndex('user_id');
            $table->dropIndex('order_id');
            $table->dropIndex('activity_id');
            $table->dropIndex('return_profit');
        });
        Schema::table('hot_search', function (Blueprint $table) {
            $table->dropIndex('name');
            $table->dropIndex('number');
        });
        Schema::table('images', function (Blueprint $table) {
            $table->dropIndex('provider');
            $table->dropIndex('image_file_id');
        });
        Schema::table('inventories', function (Blueprint $table) {
            $table->dropIndex('product_id');
            $table->dropIndex('sku_code');
            $table->dropIndex('number');
        });
        Schema::table('level_enables', function (Blueprint $table) {
            $table->dropIndex('level_id');
            $table->dropIndex('enable_id');
        });
        Schema::table('notify_messages', function (Blueprint $table) {
            $table->dropIndex('status');
            $table->dropIndex('user_id');
            $table->dropIndex('provider');
            $table->dropIndex('deleted_at');
        });
        Schema::table('order_packages', function (Blueprint $table) {
            $table->dropIndex('order_id');
            $table->dropIndex('com');
            $table->dropIndex('shipping_sn');
        });
        Schema::table('order_products', function (Blueprint $table) {
            $table->dropIndex('order_id');
            $table->dropIndex('sell_product_id');
            $table->dropIndex('product_id');
            $table->dropIndex('product_provider');
            $table->dropIndex('inventory_id');
        });
        Schema::table('order_refund_log_images', function (Blueprint $table) {
            $table->dropIndex('order_refund_log_id');
        });
        Schema::table('order_refund_logs', function (Blueprint $table) {
            $table->dropIndex('order_refund_id');
        });
        Schema::table('order_refund_products', function (Blueprint $table) {
            $table->dropIndex('order_id');
            $table->dropIndex('shipping_sn');
        });
        Schema::table('order_refundments', function (Blueprint $table) {
            $table->dropIndex('order_refund_id');
            $table->dropIndex('status');
            $table->dropIndex('provider');
        });
        Schema::table('order_refunds', function (Blueprint $table) {
            $table->dropIndex('status');
            $table->dropIndex('refund_status');
            $table->dropIndex('provider');
            $table->dropIndex('order_product_id');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('order_status');
            $table->dropIndex('pay_status');
            $table->dropIndex('shipping_status');
            $table->dropIndex('cart_ids');
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->dropIndex('order_id');
            $table->dropIndex('status');
            $table->dropIndex('provider');
            $table->dropIndex('refund');
        });
        Schema::table('physical_stores', function (Blueprint $table) {
            $table->dropIndex('name');
            $table->dropIndex('name_initial');
        });
        Schema::table('point_received', function (Blueprint $table) {
            $table->dropIndex('status');
        });
        Schema::table('point_recoreds', function (Blueprint $table) {
            $table->dropIndex('provider');
            $table->dropIndex('user_id');
            $table->dropIndex('type');
        });
        Schema::table('populars', function (Blueprint $table) {
            $table->dropIndex('parent_id');
        });
        Schema::table('product_categories', function (Blueprint $table) {
            $table->dropIndex('brand_id');
        });
        Schema::table('product_images', function (Blueprint $table) {
            $table->dropIndex('product_id');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropIndex('sell_point');
            $table->dropIndex('snumber');
            $table->dropIndex('number');
            $table->dropIndex('status');
        });
        Schema::table('provinces', function (Blueprint $table) {
            $table->dropIndex('province_id');
            $table->dropIndex('name');
            $table->dropIndex('lat');
            $table->dropIndex('lng');
        });
        Schema::table('sell_attribute_values', function (Blueprint $table) {
            $table->dropIndex('sell_attribute_id');
            $table->dropIndex('attribute_value_id');
            $table->dropIndex('attribute_id');
        });
        Schema::table('sell_attributes', function (Blueprint $table) {
            $table->dropIndex('attribute_id');
            $table->dropIndex('product_sell_attribute_id');
        });
        Schema::table('sell_products', function (Blueprint $table) {
            $table->dropIndex('product_point');
            $table->dropIndex('base_price');
            $table->dropIndex('market_price');
            $table->dropIndex('number');
            $table->dropIndex('number_zero_time');
            $table->dropIndex('puzzles');
            $table->dropIndex('game_number');
        });
        Schema::table('share_relations', function (Blueprint $table) {
            $table->dropIndex('share_id');
            $table->dropIndex('sell_product_id');
            $table->dropIndex('subject_id');
            $table->dropIndex('video_id');
            $table->dropIndex('code_id');
            $table->dropIndex('tribe_id');
            $table->dropIndex('activity_id');
            $table->dropIndex('puzzle_game_id');
        });
        Schema::table('share_types', function (Blueprint $table) {
            $table->dropIndex('provider_id');
        });
        Schema::table('shares', function (Blueprint $table) {
            $table->dropIndex('provider');
            $table->dropIndex('is_default');
            $table->dropIndex('is_defaultTitle');
            $table->dropIndex('is_defaultContent');
            $table->dropIndex('is_defaultImg');
            $table->dropIndex('is_shareAll');
        });
        Schema::table('sizes', function (Blueprint $table) {
            $table->dropIndex('least_height');
            $table->dropIndex('most_height');
        });
        Schema::table('socialites', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('openid');
            $table->dropIndex('unionid');
        });
        Schema::table('subject_products', function (Blueprint $table) {
            $table->dropIndex('product_id');
            $table->dropIndex('subject_id');
        });
        Schema::table('subject_views', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('subject_id');
        });
        Schema::table('subjects', function (Blueprint $table) {
            $table->dropIndex('scan');
            $table->dropIndex('status');
        });
        Schema::table('taobao_top_categorys', function (Blueprint $table) {
            $table->dropIndex('cid');
            $table->dropIndex('parent_cid');
            $table->dropIndex('name');
            $table->dropIndex('category_id');
        });
        Schema::table('taobao_top_inventorys', function (Blueprint $table) {
            $table->dropIndex('taobao_outer_id');
            $table->dropIndex('is_handle');
        });
        Schema::table('taobao_top_product_handles', function (Blueprint $table) {
            $table->dropIndex('taobao_top_inventory_id');
            $table->dropIndex('taobao_outer_id');
            $table->dropIndex('is_publish');
            $table->dropIndex('check_skus');
            $table->dropIndex('check_cate');
        });
        Schema::table('taobao_top_sync_logs', function (Blueprint $table) {
            $table->dropIndex('start_sync_time');
            $table->dropIndex('end_sync_time');
            $table->dropIndex('request_start_sync_time');
            $table->dropIndex('request_end_sync_time');
        });
        Schema::table('today_tasks', function (Blueprint $table) {
            $table->dropIndex('type');
        });
        Schema::table('user_answer', function (Blueprint $table) {
            $table->dropIndex('question_id');
            $table->dropIndex('user_id');
            $table->dropIndex('user_suit_id');
            $table->dropIndex('option_id');
        });
        Schema::table('user_collections', function (Blueprint $table) {
            $table->dropIndex('sell_product_id');
        });
        Schema::table('user_comments', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('inventory_id');
            $table->dropIndex('status');
            $table->dropIndex('product_sell_id');
            $table->dropIndex('comment_status');
        });
        Schema::table('user_coupons', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('coupon_id');
            $table->dropIndex('status');
        });
        Schema::table('user_enables', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('enable_id');
        });
        Schema::table('user_game_advices', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('provider');
            $table->dropIndex('status');
        });
        Schema::table('user_games', function (Blueprint $table) {
            $table->dropIndex('sell_product_id');
            $table->dropIndex('success');
            $table->dropIndex('order_id');
            $table->dropIndex('order_status');
            $table->dropIndex('expired');
        });
        Schema::table('user_notify_messages', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('notify_messages_id');
        });
        Schema::table('user_scan_logs', function (Blueprint $table) {
            $table->dropIndex('sell_product_id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('phone');
            $table->dropIndex('uuid');
        });
        Schema::table('video_likes', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('video_id');
        });
        Schema::table('withdraw_channels', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('type');
        });
        Schema::table('withdraw_records', function (Blueprint $table) {
            $table->dropIndex('state');
            $table->dropIndex('channel_id');
            $table->dropIndex('trade_no');
        });
    }
}
