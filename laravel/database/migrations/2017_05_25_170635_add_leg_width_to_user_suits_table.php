<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegWidthToUserSuitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_suits', function (Blueprint $table) {
            $table->float('leg_width')->nullable()->comment('小腿围');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_suits', function (Blueprint $table) {
            $table->dropColumn('leg_width');
        });
    }
}
