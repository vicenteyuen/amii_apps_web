<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsAddLinkToActivityRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_rules', function(Blueprint $table){
            $table->integer('is_addLink')->default(0)->comment('是否添加活动链接');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_rules', function(Blueprint $table){
            $table->dropColumn('is_addLink');
        });
    }
}
