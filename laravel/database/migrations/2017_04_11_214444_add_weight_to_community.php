<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWeightToCommunity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->integer('weight')->defult(0);
        });
        Schema::table('videos', function (Blueprint $table) {
            $table->integer('weight')->defult(0);
        });
        Schema::table('physical_stores', function (Blueprint $table) {
            $table->integer('weight')->defult(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->dropColumn('weight');
        });
        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn('weight');
        });
        Schema::table('physical_stores', function (Blueprint $table) {
            $table->dropColumn('weight');
        });
    }
}
