<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointsToPointRankAward extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_rank_award_users', function (Blueprint $table) {
            $table->integer('points')->default(0)->comment('人数');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_rank_award_users', function (Blueprint $table) {
            $table->dropColumn('points');
        });
    }
}
