<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToBrandCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('brand_categorys', function(Blueprint $table){
            $table->string('image')->comment('品牌分类头图');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brand_categorys', function(Blueprint $table){
            $table->dropColumn('image');
        });
    }
}
