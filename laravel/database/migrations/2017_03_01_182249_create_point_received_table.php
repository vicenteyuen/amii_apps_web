<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use XsBaseDatabase\BaseBlueprint as Blueprint;

class CreatePointReceivedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        $schema->create('point_received', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('points');
            $table->string('type', 40)->comment('register, update, looking,invite,shopping,received_day,appraising,image_appraising,share,activity,advice');
            $table->string('type_child', 40)->comment('
                register(register),
                update(phone,birthday,address,wechat,qq,weibo),
                looking(figure),
                invite(invite),
                shopping(shoppint),
                received_day(reveived_day),
                appraising(normal,image),
                image_appraising(mark,comment),
                share(product_url,person_comment,activity_share,game_url),
                activity(lottery,game,show,collocation)
                advice(advice)');

             $table->timestamps();
             $table->index(['user_id','type','type_child']);
             $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('point_received');
    }
}
