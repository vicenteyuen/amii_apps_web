<?php

return [
    // operation: ['resize','fit','resizeCanvas']
    // resize 缩放 ，不裁剪
    // resizeCanvas 缩放 ，不裁剪，不够的填充
    // fit: 缩放 ， 裁剪

    'filetype' => ['jpg','png','jpeg','gif'],


    'default' => [
        'small' => [
            'width' => 100,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 600,
            'height' => 600,
            'operation' => 'resize',
        ],
    ],

    // product
    'product' => [
        'small' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 750,
            'height' => 750,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1200,
            'height' => 1200,
            'operation' => 'resize',
        ],
    ],

    // banner
    'bannerApp' => [
        'small' => [
            'width' => 230,
            'height' => 310,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 690,
            'height' => 930,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1380,
            'height' => 1860,
            'operation' => 'resize',
        ],
    ],

    'bannerMobile' => [
        'small' => [
            'width' => 230,
            'height' => 310,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 690,
            'height' => 930,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1380,
            'height' => 1860,
            'operation' => 'resize',
        ],
    ],
    'bannerWeapp' => [
        'small' => [
            'width' => 235,
            'height' => 186,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 705,
            'height' => 560,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1410,
            'height' => 1120,
            'operation' => 'resize',
        ],
    ],

    'popular' => [
        'small' => [
            'width' => 230,
            'height' => 310,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 690,
            'height' => 930,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1380,
            'height' => 1860,
            'operation' => 'resize',
        ],
    ],

    'popularWeapp' => [
        'small' => [
            'width' => 230,
            'height' => 310,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 690,
            'height' => 930,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1380,
            'height' => 1860,
            'operation' => 'resize',
        ],
    ],

    // category
    'category' => [
        'small' => [
            'width' => 375,
            'height' => 235,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 750,
            'height' => 470,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1500,
            'height' => 940,
            'operation' => 'resize',
        ],
    ],
    // brand
    'brand' => [
        'small' => [
            'width' => 375,
            'height' => 235,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 750,
            'height' => 470,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1500,
            'height' => 940,
            'operation' => 'resize',
        ],
    ],
    // brandCareImg
    'brandCateHeard' => [
        'small' => [
            'width' => 125,
            'height' => 78,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 750,
            'height' => 470,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1500,
            'height' => 940,
            'operation' => 'resize',
        ],
    ],
    // coupon
    'coupon' => [
        'small' => [
            'width' => 100,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 600,
            'height' => 600,
            'operation' => 'resize',
        ],
    ],

    //subject
    'subject' => [
        'small' => [
            'width' => 250,
            'height' => 120,

            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 750,
            'height' => 358,

            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1500,
            'height' => 716,

            'operation' => 'resize',
        ],
    ],

     'subjectCont' => [
        'small' => [
            'width' => 100,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 600,
            'height' => 600,
            'operation' => 'resize',
        ],
    ],


    //physicalStore
    'physicalStore' => [
        'small' => [
            'width' => 234,
            'height' => 110,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 700,
            'height' => 330,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1400,
            'height' => 660,
            'operation' => 'resize',
        ],
    ],

    //video
    'video' => [
        'small' => [
            'width' => 120,
            'height' => 80,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 360,
            'height' => 240,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 980,
            'height' => 730,
            'operation' => 'resize',
        ],
    ],

    // share
    'share' => [
        'small' => [
            'width' => 100,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 600,
            'height' => 600,
            'operation' => 'resize',
        ],
    ],

    'avatar' => [
        'small' => [
            'width' => 100,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 600,
            'height' => 600,
            'operation' => 'resize',
        ],
    ],

    'refund' => [
        'small' => [
            'width' => 100,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 300,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 600,
            'height' => 600,
            'operation' => 'resize',
        ],
    ],

    'activityRule' => [
        'small' => [
            'width' => 230,
            'height' => 100,
            'operation' => 'resize',
        ],
        'medium' => [
            'width' => 690,
            'height' => 300,
            'operation' => 'resize',
        ],
        'large' => [
            'width' => 1400,
            'height' => 600,
            'operation' => 'resize',
        ],
    ],
];
