<?php

/**
 * socialite plus driver config
 */
return [
    /**
     * weweb 开放平台网页登录
     */
    'weweb' => 'Weweb',

    /**
     * wemp 公众号登录
     */
    'wemp' => 'Wemp',

    /**
     * weapp 微信小程序
     */
    'weapp' => 'Weapp',

    /**
     * weibo app
     */
    'weiboapp' => 'WeiboApp',

    /**
     * qq ios
     */
    'qqios' => 'Qqios',

    /**
     * qq android
     */
    'qqandroid' => 'Qqandroid',
];
