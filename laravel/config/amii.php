<?php

return [
    'produce' => env('APP_PRODUCE', true),

    // hmac keys
    'hmackey' => env('HMAC_KEYS', ''),

    // amii app
    'app' => [
        'download' => [
            'url' => env('APP_DOWNLOAD_NET_URL', '')
        ]
    ],

    // activity
    'activity' => [
        'game' => [
            'status' => env('AMII_ACTIVITY_GAME_STATUS', false),
        ],
    ],
    // cron switch
    'cron' => env('AMII_CRON_SWITCH', false),

    // taobao open
    'taobaoopen' => [
        'app_key' => env('TAOBAO_OPEN_APP_KEY', ''),
        'app_secret' => env('TAOBAO_OPEN_APP_SECRET', ''),
        'sync_image' => env('TAOBAO_OPEN_SYNC_IMAGE', true),
        // 每页返回条目设置
        // 不知道taobao什么bug设置无效
        'page_size' => 40,
        'session_key' => [
            'amii' => env('TAOBAO_OPEN_AMII_SESSION_KEY', ''),
            'amii_r' => env('TAOBAO_OPEN_AMIIR_SESSION_KEY', ''),
            'amii_m' => env('TAOBAO_OPEN_AMIIM_SESSION_KEY', ''),
            'amii_c' => env('TAOBAO_OPEN_AMIIC_SESSION_KEY', ''),
        ],
    ],

    // aes
    'aes' => [
        'public' => env('AES_PUBLIC', ''),
        'key' => env('AES_KEY', '')
    ],

    // oms
    'oms' => [
        # 服务器地址
        'domain' => env('OMS_DOMAIN', ''),
        # appkey
        'appkey' => env('OMS_APPKEY', ''),
        # token
        'token' => env('OMS_TOKEN', ''),
        # log
        'log' => env('OMS_LOG', false),
        # log data
        'log_request_data' => env('OMS_LOG_REQUEST_DATA', false),
    ],

    // deepdraw
    'deepdraw' => [
        // 是否同步
        'sync' => env('DEEPDRAW_SYNC', false),
        'app_key' => env('DEEPDRAW_APP_KEY', ''),
        'app_secret' => env('DEEPDRAW_APP_SECRET', ''),
        // 商户
        'merchant' => [
            'id' => env('DEEPDRAW_MERCHANT_ID', 1024),
        ],

    ],

    // admin paginate
    'adminPaginate' => 20,

    //api paginate
    'apiPaginate' => 20,

    // taobao sync paginate
    'taobaoPaginate' => 20,

    // admin image paginate
    'imagePaginate' => 24,

    // hot search paginate
    'hotSearchPaginate' => 8,

    // test image host
    'testImageHost' => env('TEST_IMAGE_HOST', 'http://amii.dev.ganguo.hk'),
    // image host
    'imageHost' => env('IMAGE_HOST', '/'),
    // local image path
    'imageLocalPath' => env('IMAGE_LOCAL_PATH', public_path() . '/uploads/images'),

    // local package path
    'packageLocalPath' => env('PACKAGE_LOCAL_PATH', public_path() . '/uploads/packages'),

    // image upload provider
    'imageUploadProvider' => [
        'product',
        'ApbannerApp',
        'bannerMobile',
        'bannerWeapp',
        'popular',
        'popularWeapp',
        'category',
        'brand',
        'coupon',
        'subject',
        'subjectCont',
        'physicalStore',
        'video',
        'avatar',
        'refund'
    ],

    // image default
    'image' => env('AMII_DEFAULT_IMAGE', 'http://7xw4yp.com1.z0.glb.clouddn.com/1459309578804.jpg'),

    // image default banner
    'imageBanner' => env('AMII_DEFAULT_IMAGE_BANNER', 'http://7xw4yp.com1.z0.glb.clouddn.com/1459309578804.jpg'),

    // image default product
    'imageProduct' => env('AMII_DEFAULT_IMAGE_PRODUCT', 'http://7xw4yp.com1.z0.glb.clouddn.com/1459309578804.jpg'),

    'videoHost' => env('VIDEO_HOST', '/'),

    'packageHost' => env('PACKAGE_HOST', '/'),

    // kuaidi100
    'kuaidi100' => [
        'debug' => env('KUAIDI100_DEBUG', true),

        // 是否使用免费接口
        'is_free' => env('KUAIDI100_ISFEE', true),

        // poll 订阅
        'poll' => [
            'key' => env('KUAIDI100_POLL_KEY', ''),
        ],

        // fee 免费查询
        'fee' => [
            'key' => env('KUAIDI100_FEE_KEY', ''),
        ],

        // enterprise 企业查询
        'enterprise' => [
            'key' => env('KUAIDI100_ENTERPRISE_KEY', ''),
            'customer' => env('KUAIDI100_ENTERPRISE_CUSTOMER', ''),
        ],
    ],

    // sms service
    'sms' => [
        'service' => env('AMII_SMS_SERVICE', 'LEANCLOUD'),
    ],

    // montent
    'montent' => [
        'sms' => [
            'url' => env('MONTENT_SMS_URL', ''),
            'user_id' => env('MONTENT_SMS_USER_ID', ''),
            'pwd' => env('MONTENT_SMS_PWD', ''),
        ],
    ],

    // leancloud
    'leancloud' => [
        'sms' => [
            'debug' => env('LEANCLOUD_SMS_DEBUG', true),
            'url' => env('LEANCLOUD_SMS_URL', ''),
            'appid' => env('LEANCLOUD_SMS_APP_ID', ''),
            'appkey' => env('LEANCLOUD_SMS_APP_KEY', ''),
            'appmaster' => env('LEANCLOUD_SMS_MASTER_KEY', ''),
            'apns_production' => env('LEANCLOUD_SMS_APNS_PRODUCTION', ''),
            'template' => env('LEANCLOUD_SMS_TEMPLATE', 'Amii商城'),
            'sendPswdTemplate' => env('LEANCLOUD_SMS_SEND_PSWD_TEMPLATE', ''),
        ],
        'push' => [
            'debug' => env('LEANCLOUD_PUSH_DEBUG', true),
            'url' => env('LEANCLOUD_PUSH_URL', ''),
            'appid' => env('LEANCLOUD_PUSH_APP_ID', ''),
            'appkey' => env('LEANCLOUD_PUSH_APP_KEY', ''),
            'appmaster' => env('LEANCLOUD_PUSH_MASTER_KEY', ''),
            'apns_production' => env('LEANCLOUD_PUSH_APNS_PRODUCTION', ''),
        ],
        'count' => [
            'debug' => env('LEANCLOUD_COUNT_DEBUG', true),
            'url' => env('LEANCLOUD_COUNT_URL', ''),
            'appid' => env('LEANCLOUD_COUNT_APP_ID', ''),
            'appkey' => env('LEANCLOUD_COUNT_APP_KEY', ''),
            'appmaster' => env('LEANCLOUD_COUNT_MASTER_KEY', ''),
            'apns_production' => env('LEANCLOUD_COUNT_APNS_PRODUCTION', ''),
        ],
    ],

    // easywechat
    'easywechat' => [
        'debug' => env('EASYWECHAT_DEBUG', true),
    ],

    // 微信回调调试
    'weweb' => [
        'bund' => env('WEWEB_BUND', false),
        'token' => env('WEWEB_TOKEN', 'm8LDj'),
    ],

    // pay
    'payment' => [
        'alipay' => [
            'appid' => env('ALIPAY_APP_APPID', ''),
            'key' => env('ALIPAY_APP_AES', ''),
            'notifyurl' => env('ALIPAY_APP_NOTIFY_URL', ''),
            'partnerid' => env('ALIPAY_PARTNER_ID', ''),
            'sellerid' => env('ALIPAY_SELLER_ID', ''),
            'sellere_mail' => env('ALIPAY_SELLER_EMAIL', ''),
            'sign_type' => 'RSA',
            'cacert' => resource_path('payment/cer/alipay/ali_rsa_public_key.pem'),
            'private_key_path' => resource_path('payment/cer/alipay/rsa_private_key.pem'),
            'public_key_path' => resource_path('payment/cer/alipay/rsa_public_key.pem'),
            'input_charset'=> strtolower('utf-8'), //字符编码格式 目前支持 gbk 或 utf-8
        ],
        'alipayweb' => [
            'appid' => env('ALIPAY_WEB_APPID', ''),
            'key' => env('ALIPAY_WEB_AES', ''),
            'notifyurl' => env('ALIPAY_WEB_NOTIFY_URL', ''),
            'partnerid' => env('ALIPAY_PARTNER_ID', ''),
            'sellerid' => env('ALIPAY_SELLER_ID', ''),
            'sellere_mail' => env('ALIPAY_SELLER_EMAIL', ''),
            'sign_type' => 'RSA',
            'cacert' => resource_path('payment/cer/alipay/rsa_public_key.pem'),
            'private_key_path' => resource_path('payment/cer/alipay/rsa_private_key.pem'),
            'public_key_path' => resource_path('payment/cer/alipay/rsa_public_key.pem'),
            'input_charset'=> strtolower('utf-8'), //字符编码格式 目前支持 gbk 或 utf-8
        ],
        'wechat' => [
            'appid' => env('WECHAT_OPEN_PAY_APPID', ''),
            'merchantid' => env('WECHAT_OPEN_PAY_MERCHANT_ID', ''),
            'key' => env('WECHAT_OPEN_PAY_KEY', ''),
            'notifyurl' => env('WECHAT_OPEN_PAY_NOTIFY_URL', '')
        ],
        'weapp' => [
            'appid' => env('WECHAT_APP_PAY_APPID', ''),
            'merchantid' => env('WECHAT_APP_PAY_MERCHANT_ID', ''),
            'key' => env('WECHAT_APP_PAY_KEY', ''),
            'notifyurl' => env('WECHAT_APP_PAY_NOTIFY_URL', '')
        ],
        'wemp' => [
            'appid' => env('WECHAT_MP_PAY_APPID', ''),
            'merchantid' => env('WECHAT_MP_PAY_MERCHANT_ID', ''),
            'key' => env('WECHAT_MP_PAY_KEY', ''),
            'notifyurl' => env('WECHAT_MP_PAY_NOTIFY_URL', '')
        ],
    ],

    // order
    'order' => [
        'jobminute' => [
            'unconfirm' => env('ORDER_JOB_UNCONFIRM', 35),
            'cancel' => env('ORDER_JOB_CANCEL', 30),
        ],
        // 最近购买展示数量
        'nearlyNum' => 8,
    ],

    // order refund
    'refund' => [
        'applyTitle' => env('ORDER_REFUND_APPLY_TITLE', '买家发起申请'),
    ],

    // 佣金返回时间设置
    'tribe' => [
        'autoReceivedTime' => env('AMII_AUTO_RECEIVED_TIME', 10 * 24 * 60 * 60),
        'calcTribeMoneyTime' => env('AMII_CALC_TRIBE_MONEY_TIME', 8 * 24 * 60 * 60),
        'closeOrderTime'   => env('AMII_CLOSE_ORDER_TIME', 8 * 24 * 60 * 60),
    ],
];
