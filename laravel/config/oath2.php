<?php

return [
    /*
     * wechat 开放平台app登录
     */
    'wechat' => [
        'client_id'     => env('WECHAT_OPEN_APP_APPID', 'your-app-id-wechat'),
        'client_secret' => env('WECHAT_OPEN_APP_SECRET', 'your-app-secret-wechat'),
        'redirect'      => env('WECHAT_OPEN_APP_REDIRECT_URL', 'your-app-redirect-wechat'),
    ],

    /**
     * weweb 开放平台网页登录
     */
    'weweb' => [
        'client_id'     => env('WECHAT_OPEN_WEB_APPID', 'your-app-id-weweb'),
        'client_secret' => env('WECHAT_OPEN_WEB_SECRET', 'your-app-secret-weweb'),
        'redirect'      => env('WECHAT_OPEN_WEB_REDIRECT_URL', 'your-app-redirect-weweb'),
    ],

    /**
     * wemp 公众号登录
     */
    'wemp' => [
        'client_id'     => env('WECHAT_MP_APPID', 'your-app-id-weweb'),
        'client_secret' => env('WECHAT_MP_SECRET', 'your-app-secret-weweb'),
        'redirect'      => env('WECHAT_MP_REDIRECT_URL', 'your-app-redirect-weweb'),
    ],

    /**
     * weapp 小程序
     */
    'weapp' => [
        'client_id'     => env('WECHAT_APP_APPID', 'your-app-id-weapp'),
        'client_secret' => env('WECHAT_APP_SECRET', 'your-app-secret-weapp'),
        'redirect'      => env('WECHAT_APP_REDIRECT_URL', 'your-app-redirect-weweb'),
    ],

    /**
     * qq
     */
    'qq' => [
        'client_id'     => env('QQ_APPID', 'your-app-id-qq'),
        'client_secret' => env('QQ_SECRET', 'your-app-secret-qq'),
        'redirect'      => env('QQ_REDIRECT_URL', ''),
    ],

    /*
     * oath2 qq ios
     */
    'qqios' => [
        'client_id'     => env('QQ_IOS_APPID', 'your-app-id-qq'),
        'client_secret' => env('QQ_IOS_SECRET', 'your-app-secret-qq'),
        'redirect'      => env('QQ_IOS_REDIRECT_URL', 'amii.dev.ganguo.hk/notify/a'),
    ],

    /*
     * oath2 qq android
     */
    'qqandroid' => [
        'client_id'     => env('QQ_ANDROID_APPID', 'your-app-id-qq'),
        'client_secret' => env('QQ_ANDROID_SECRET', 'your-app-secret-qq'),
        'redirect'      => env('QQ_ANDROID_REDIRECT_URL', ''),
    ],

    /*
     * oath2 weibo
     */
    'weiboapp' => [
        'client_id'     => env('WEIBO_APPID', 'your-app-id-weibo'),
        'client_secret' => env('WEIBO_SECRET', 'your-app-secret-weibo'),
        'redirect'      => env('WEIBO_REDIRECT_URL', ''),
    ],
];
