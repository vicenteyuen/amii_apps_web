@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">售后纪录订单</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-head">
                <header>
                    <a href="{{ action('Admin\ReportController@exportRefundOrder') }}" class="btn ink-reaction btn-primary btn-md download">下载</a>
                </header>
            </div>
            <div class="card-body">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th>订单号</th>
                            <th>售后商品货号</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->order_sn }}</td>
                            <td>
                            @foreach ($item->orderProducts as $orderProduct)
                                {{ $orderProduct->inventory->sku_code }}<br>
                            @endforeach
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {!! $items->render() !!}
            </div>
        </div>
    </div>
</section>
@stop
