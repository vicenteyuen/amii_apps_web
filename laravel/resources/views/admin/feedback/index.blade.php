@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active"> 用户反馈列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                 <form class="form floating-label" autocomplete="off">
                    <div class="form-group">

                    </div>
                </form>
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th >id</th>
                            <th >用户昵称</th>
                            <th >反馈内容</th>
                            <th >信息反馈状态</th>
                            <th >操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($feedbacks  as $feedback)
                        <tr >
                            <td>{{ $feedback->id }}</td>
                            <td>{{ $feedback->user->nickname }}</td>
                            <td>{{ $feedback->content }}</td>
                             <td>
                           
                            @if($feedback->status == 0)
                                 @php
                                     $btnType = 'text-danger';
                                     $btnName = '未处理';
                                 @endphp
                            @elseif($feedback->status == 1)
                                 @php
                                     $btnType = 'text-primary';
                                     $btnName = '已处理,未采纳';
                                 @endphp
                            @elseif($feedback->status == 2)
                                @php
                                     $btnType = 'text-success';
                                     $btnName = '已采纳';
                                 @endphp
                            @endif

                             <span class=" {{$btnType}} " type="text" >{{$btnName}}</span>
                             </td>
                            <td>
                                <a href="{{ Action('Admin\FeedBackController@showDetail', ['id' => $feedback->id]) }}" class="btn ink-reaction btn-success btn-xs" type="button">查看</a>
                                </td>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($feedbacks->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$feedbacks->toArray()['to'] - $feedbacks->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$feedbacks->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                    {!! $feedbacks->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop
