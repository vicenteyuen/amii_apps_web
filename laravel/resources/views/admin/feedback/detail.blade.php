@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2-bootstrap.css">
@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">反馈详情</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="formFeedBack" method="POST">
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                       <input type="hidden" name="id"  value="{{$feedbacksInfo->id or ''}}">
                        <div class="tab-content clearfix">

                           <div class="row">

                                @include('admin.widget.input', [
                                    'colsm'    => '12',
                                    'collg'    => '12',
                                    'name'     => 'nickname',
                                    'id'       => 'nickname',
                                    'title'    => '用户昵称' ,
                                    'type'     => 'text',
                                    'readonly' => 'readonly',
                                    'value'    => isset($feedbacksInfo->user->nickname )? $feedbacksInfo->user->nickname : ''
                                ])

                                @include('admin.widget.textarea', [
                                    'colsm'    => '48',
                                    'collg'    => '12',
                                    'name'     => 'content',
                                    'id'       => 'content',
                                    'title'    => '反馈内容' ,
                                    'type'     => 'text',
                                    'readonly' => 'readonly',
                                    'value'    => isset($feedbacksInfo->content )? $feedbacksInfo->content : ''
                                ])

                            </div>
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next">
                                @if ($feedbacksInfo->status == 0)
                                    <button type="submit" class="btn ink-reaction btn-primary btn-md">处理 </button>
                                    <input type="hidden" name="status" value="1"> 
                                @elseif($feedbacksInfo->status == 1)
                                     <button type="submit" class="btn ink-reaction btn-primary btn-md">采纳</button>
                                     <input type="hidden" name="status" value="2">
                                @else
                                    <a href="{!! action('Admin\\FeedBackController@index') !!}" class="btn ink-reaction btn-primary btn-md" type="button" >返回</a>
                                @endif
                               </li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop

@section('script')
<script type="text/javascript">

    var url  = "{!! action('Admin\\FeedBackController@edits') !!}";
    var msg  = '操作成功';

    // wizard on tab next 提交表单
     $('#formFeedBack').ajaxForm({
        url: url,
        type: 'POST',
        dataType: 'json',
        async:false,
        success: function (returnData) {

            if (returnData.status == 'success') {

                layer.msg(msg, {icon: 1});
                setTimeout(function(){
                    // 返回上一页
                    window.location.href = "{{action('Admin\\FeedBackController@index')}}";
                },1000)
            }else{
                layer.msg(returnData.msg,{icon: 0});
            }
        }
    });

</script>
@stop
