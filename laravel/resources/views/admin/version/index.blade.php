@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active"> 系统版本列表</li>
        </ol>
    </div>
    <div class="section-body">

        <div class="card">
            <div class="card-head">
                <header></header>

                <div class="tools">
                    <div class="btn-group">
                        <form method="GET" action="" accept-charset="UTF-8" id="search-form">
                            <div class="input-group input-group-lg pull-left" style="width: 280px;">
                                <div class="input-group-content" >
                                    <input type="text" placeholder="请输入版本名称\版本号\渠道" id="search" name="search" class="form-control" value="" style="width:280px">
                                    <div class="form-control-line"></div>
                                </div>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-floating-action btn-default-bright"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="btn-group text-right">
                    <a href="{!! action('Admin\VersionController@create') !!}" class="btn ink-reaction btn-default btn-sm pull-right">
                        添加
                    </a>
                </div>
            </div><!--end .card-head -->
            <div class="card-body">

                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th >id</th>
                            <th >版本名</th>
                            <th >是否最新版</th>
                            <th >版本类型</th>
                            <th >版本渠道</th>
                            <th >版本号</th>
                            <th >创建时间</th>
                            <th >操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($versions as $version)

                        <tr>
                            <td>{{ $version->id }}</td>
                            <td>{{ $version->name }}</td>
                            @if ($version->is_newest == 1)
                                <td> 是 </td>
                            @else
                                <td> 否 </td>
                            @endif
                            <td>{{ $version->version_type }}</td>

                            <td>
                                @foreach ($channels as $channel)
                                    @if ($channel->channel  == $version->channel)
                                     {{ $channel->desc }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ $version->version_number }}</td>
                            <td>{{ $version->created_at }}</td>
                            <td>

                                <a href="{{ action('Admin\VersionController@showDetail', ['type' => 'edits', 'id' => $version->id] )}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">编辑</a>

                                <a href="{{ action('Admin\VersionController@showDetail', ['type' => 'show', 'id' => $version->id] )}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">查看</a>

                                <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-flat btn-xs btn-danger js-delete" data-id="{{$version->id}}" data-toggle="modal" data-target="#confirmModal">删除</a>

                            </td>

                        </tr>

                        @endforeach
                    </tbody>

                </table>
                @if($versions->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$versions->toArray()['to'] - $versions->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$versions->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                      {!! $versions->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var _token = $('#_token').val();

     $('.js-delete').click(function(){
        var _this = $(this);
        layer.confirm('您确定删除？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\VersionController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

</script>
@stop
