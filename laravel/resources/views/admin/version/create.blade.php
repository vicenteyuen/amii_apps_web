@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2-bootstrap.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">

@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">
                @if (isset($type) && $type == 'edits' )
                    编辑系统版本
                @elseif (isset($type) && $type == 'show')
                    系统版本详情
                @else
                    添加新系统版本
                @endif
            </li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="formVersion"  method="POST">
                    <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" class="js_token" id="id" value="{{ isset($versions->id) ? $versions->id : '' }}">

                        <div class="tab-content clearfix">

                           <div class="row">

                                @include('admin.widget.input', [
                                    'colsm'    => '12',
                                    'collg'    => '6',
                                    'name'     => 'name',
                                    'id'       => 'name',
                                    'title'    => '版本名' ,
                                    'type'     => 'text',
                                    'value'    => isset($versions->name) ? $versions->name : '',
                                    'readonly' => isset($type) && $type == 'show' ? 'readonly' : ''
                                ])

                                @include('admin.widget.select', [
                                    'colsm' => '12',
                                    'collg' => '6',
                                    'attribute' => 'attributeValue',
                                    'id' => 'version_type',
                                    'name' => 'version_type',
                                    'title' => '版本类型',
                                    'attribute' => isset($type) && $type == 'show' ? 'disabled' : '',
                                    'selected' => isset($versions->version_type) ? $versions->version_type : '',
                                    'values' => [
                                        'android' => 'android',
                                        'ios' => 'ios',
                                    ]
                                ])


                                @include('admin.widget.select', [
                                    'colsm' => '12',
                                    'collg' => '6',
                                    'selected' => isset($versions->channel) ? $versions->channel : '',
                                    'attribute' => isset($type) && $type == 'show' ? 'disabled' : '',
                                    'name'     => 'channel',
                                    'id'       => 'channel',
                                    'title'    => '版本渠道' ,
                                    'values' => [
                                        '默认'       => 'mobileApp',
                                        '多服务器环境'   => 'production',
                                        '测试环境  '  => 'stage',
                                        '豌豆荚'      => 'wandoujia',
                                        '机锋市场'    =>  'jifeng',
                                        '应用汇'      => 'yingyonghui',
                                        '360手机助手' => 'qihu360',
                                        '搜狗手机助手' => 'sogou',
                                        '小米应用中心' => 'xiaomi',
                                        '91手机商城'  => 'jiuyi',
                                        '百度手机助手' => 'baidu',
                                        '华为智云汇'   => 'huawei',
                                        'OPPO NearMe' =>  'oppo',
                                        '腾讯应用宝'   =>  'yingyongbao',
                                        '魅族商店'     => 'flyme',
                                        '沃商店'      =>  'wostore',
                                        '酷安'        =>  'coolapk',
                                        '联想'        =>  'lenovo',
                                        '木蚂蚁'      =>  'mumayi',
                                        '安智市场'    =>  'anzhi',
                                        '安卓市场'    =>  'hiapk',
                                        '阿里应用分发平台' =>  'uc',
                                        'VIVO渠道'       => 'vivo',
                                    ]
                                ])

                                @include('admin.widget.input', [
                                    'colsm'    => '12',
                                    'collg'    => '6',
                                    'name'     => 'version_code',
                                    'id'       => 'version_code',
                                    'title'    => 'VersionCode',
                                    'type'     => 'text',
                                    'value'    => isset($versions->version_code) ? $versions->version_code : '',
                                    'readonly' => isset($type) && $type == 'show' ? 'readonly' : ''
                                ])

                                @include('admin.widget.input', [
                                    'colsm'    => '12',
                                    'collg'    => '12',
                                    'name'     => 'version_number',
                                    'id'       => 'version_number',
                                    'title'    => '版本号' ,
                                    'type'     => 'text',
                                    'value'    => isset($versions->version_number) ? $versions->version_number : '',
                                    'readonly' => isset($type) && $type == 'show' ? 'readonly' : ''
                                ])

                                @include('admin.widget.radio', [
                                    'name' => 'is_uploadApk',
                                    'id' => 'is_uploadApk',
                                    'title' => '是否上传apk安装包',
                                    'value' => old('is_uploadApk') ? old('is_uploadApk') : (isset($versions->is_uploadApk)? $versions->is_uploadApk : 0 ),
                                    'hidden' => isset($type) && $type == 'show' ? 'readonly' : ''
                                ])
                                <div class="showUrl">
                                @include('admin.widget.input', [
                                    'colsm'    => '12',
                                    'collg'    => '12',
                                    'name'     => 'version_url',
                                    'id'       => 'version_url',
                                    'title'    => '版本URL' ,
                                    'type'     => 'text',
                                    'value'    => isset($versions->version_url) ? $versions->version_url : '',
                                    'readonly' => isset($type) && $type == 'show' ? 'readonly' : ''
                                ])
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12" id="upload-container">
                                <div class="form-group">
                                <label>
                                    @if (isset($type) && $type == 'edits')
                                        更换安装包
                                    @else
                                        上传安装包
                                    @endif
                                </label><br>
                                <div id="fileupload-container">
                                    <a id="pickfiles" href="javascript:;" class="btn btn-sm btn-primary add-operation">选择文件</a>
                                    <a id="uploadfiles" href="javascript:;" class="btn btn-sm btn-primary add-operation">上传文件</a>
                                </div>
                                <a id="deletefiles" href="javascript:;" class="btn btn-sm btn-danger delete-operation hide">删除文件</a>
                                <br>
                                <span id="filelist">你的浏览器或浏览器版本出现错误，无法上传</span>
                                <div id="upload-proccess"></div>
                                <br>
                                </div>
                            </div>

                                @include('admin.widget.textarea', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'description',
                                    'id'    => 'description',
                                    'title' => '系统版本简要描述',
                                    'value' => isset($versions->description) ? $versions->description : '',
                                    'readonly' => isset($type) && $type == 'show' ? 'readonly' : ''
                                ])

                                @include('admin.widget.radio', [
                                    'colsm' => '12',
                                    'collg' => '6',
                                    'name'  => 'is_newest',
                                    'id'    => 'is_newest',
                                    'title' => '是否最新版本',
                                    'value' => isset($versions->is_newest) ? $versions->is_newest : 1,
                                    'readonly' => isset($type) && $type  == 'show' ? 'readonly' : ''
                                ])

                            </div>
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next">
                                <button type="submit" class="btn ink-reaction btn-primary btn-md">
                                @if (isset($type) && $type  == 'show')
                                   <a style="text-decoration: none;" href=" {{ action('Admin\\VersionController@index') }}">返回</a>
                                @elseif (isset($type) && $type == 'edits')
                                    编辑
                                @else
                                    提交
                                @endif
                                </button></li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/plupload/i18n/zh_CN.js"></script>
@stop

@section('script')
<script type="text/javascript">

    $('#upload-container').hide();

    // 显示详情处理
    @if(isset($type) && $type  == 'show')
        $('input[name=is_uploadApk]').attr('disabled', true);
        $('input[name=is_newest]').attr('disabled', true);
    @endif

    // 编辑时，处理
    @if(isset($type) && $type  == 'edits')
        @if(isset($versions->is_uploadApk) && $versions->is_uploadApk == 1)
            $('#upload-container').show();
            $('#version_url').prop('readonly', 'true');
        @endif
    @endif

    // 判断是否上传apk安装包
    $(document).on('click', 'input[name=is_uploadApk]',function(){
        var value = $(this).val();
        if(value == 1){
            @if(isset($type) && $type  == 'edits')
                $('.showUrl').show();
                @if(isset($versions->is_uploadApk) && $versions->is_uploadApk == 1)
                    $('#upload-container').show();
                    $('#version_url').prop('readonly', 'true');
                @else
                    $('#version_url').removeAttr('readonly');
                @endif
            @else
                $('.showUrl').hide();
                $('#version_url').val('');
            @endif
            $('#upload-container').show();
        } else {
            @if(isset($type) && $type  == 'edits')
                $('#version_url').removeAttr('readonly');
            @endif
            $('.showUrl').show();
            $('#upload-container').hide();
        }
    })

    @if (isset($versions))
        var url  = "{!! action('Admin\\VersionController@edits') !!}";
        var message  = '编辑成功';
    @else
        var url  = "{!! action('Admin\\VersionController@store') !!}";
        var message  = '添加成功';
    @endif
    // wizard on tab next 提交表单
     $('#formVersion').ajaxForm({
        url: url,
        type: 'POST',
        dataType: 'json',
        async: false,
        success: function (returnData) {
            if (returnData.status == 'success') {

                layer.msg(message, {icon: 1});
                setTimeout(function(){
                    // 返回上一页
                    window.location.href = "{!! action('Admin\\VersionController@index') !!}";
                },1000)
            }else{
                var msg =  JSON.parse(returnData.message);
                $.each(msg, function(i) {
                    var info = msg[i];
                    for (var i = 0; i < info.length; i++) {
                        layer.msg(info[i],{icon: 0});
                    }
                });

            }
        }
    });

    $(document).ready(function() {
            var uploader = new plupload.Uploader({
                runtimes : 'gears,html5,flash,silverlight,browserplus',
                browse_button : 'pickfiles',
                container: 'fileupload-container',
                max_file_size: "100mb",
                multi_selection: false,
                chunk_size: '1mb',
                unique_names: true,
                url: "{{ action('Admin\VersionController@upload') }}",
                flash_swf_url : '/assets/lib/plupload/Moxie.swf',
                silverlight_xap_url : '/assets/lib/plupload/Moxie.xap',
                filters : [
                    {
                        title : "Application files",
                        extensions : "ipa,apk"
                    }
                ],

                init: {
                    PostInit: function() {
                        $('#filelist').html('');

                        $('#uploadfiles').click(function() {
                            uploader.start();
                            return false;
                        });
                    },

                    FilesAdded: function(up, files) {
                        $.each(up.files, function (i, file) {
                            if (up.files.length <= 1) {
                                return;
                            }
                            up.removeFile(file);
                        });
                        plupload.each(files, function(file) {
                            $('#filelist').html('<div>' + file.name + ' (' + plupload.formatSize(file.size) + ')</div>');
                        });
                    },

                    UploadProgress: function(up, file) {
                        $('#upload-proccess').html('<div style="height:20px;"><div style="border-radius:2px;background-color:#5cb85c;height: 100%;width:' + file.percent +'%"></div></div>');
                    },

                    Error: function(up, err) {
                        console.log('错误码: ' + err.code + ', 错误信息: ' + err.message);
                    },

                    FileUploaded: function (uploader, files, result) {
                        var response = JSON.parse(result.response);
                        if (response.status == 'error') {
                            layer.msg(response.message);
                        } else{
                            $('#fileupload-container').hide();
                            $('.delete-operation').removeClass('hide');
                            $('#version_url').val(response.data.file_name);
                            layer.msg('上传成功');
                        }
                    }
                }
            });

            uploader.init();
        });

        $('.delete-operation').click(function () {
            $(this).addClass('hide');
            $('#fileupload-container').show();
            $('#filelist').html('');
            $('#upload-proccess').html('');
            $('#version_url').val('');
        })

</script>
@stop
