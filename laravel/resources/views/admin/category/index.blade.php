@extends('admin.widget.body')
@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2-bootstrap.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">分类列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-head">
                <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                    <li class="active"><a href="#index">分类列表</a></li>
                    <li><a href="#create">新增分类</a></li>
                </ul>
            </div>
            <div class="card-body tab-content">
                <!-- 分类列表 -->
                <div class="tab-pane active" id="index">
                    <div class="row">
                        <div class="col-md-3" >
                            <!-- Box -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">分类列表
                                        <a id="deleteCategoryAnchor" data-toggle="modal" data-target="#confirmModal" title="{{ trans('general.button.delete') }}" class='btn btn-danger btn-sm pull-right' style="display: none;">删除</a>
                                    </h3>
                                    <div class="box-tools pull-right">
                                    </div>
                                </div>
                                <div class="box-body">

                                    <div class="dd nestable-list " style="max-height:calc(114vh - 140px); overflow-y: scroll;">
                                        {!! \App\Helpers\AdminViewHelper::categoryTreeHtml($categories) !!}
                                    </div>

                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>

                        <div class='col-md-9'>
                                <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">品牌分类列表</h3>
                                    <div class="box-tools pull-right">
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <table class="table table-hover table-condensed table-striped no-margin">
                                                <thead>
                                                    <tr>
                                                        <th>序号#</th>
                                                        <th>分类名称</th>
                                                        <th>品牌分类ID</th>
                                                        <th>品牌名称</th>
                                                        <th>品牌分类头图</th>
                                                        <th>操作</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="brand_tbody">
                                                </tbody>
                                            </table>
                                            <div class="text-center">

                                            </div>
                                        </div>
                                    </div>

                                </div><!-- /.box-body -->
                            </div>
                                <!-- 模态框（Modal） -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="max-height:calc(100vh - 140px); overflow-y: scroll;">
                                        <div class="modal-header">
                                            <button type="button" id='modalClose' class="close" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                添加图片
                                            </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card-body">

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  ">
                                                <div class="input-group">
                                                    <div class="input-group-content">
                                                        <div class="form-group note-group-select-from-files"><label>从本地上传</label><input class="note-image-input form-control" type="file" name="files" id="newcategoryImage" multiple="multiple"></div>
                                                        <input type="hidden" id='brandCateId' value="">
                                                    </div>
                                                </div>
                                                    <div class="form-group pull-right">
                                                        <a id="submitBrandCateImg"  title="" class='btn btn-success '>提交</a>
                                                    </div>
                                                </div>

                                           </div>
                                            <!-- ./card-body -->

                                        </div>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal -->
                            </div>

                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">分类详情</h3>
                                    <div class="box-tools pull-right">
                                    </div>
                                </div>
                                <div class="box-body">
                                    <input type="hidden" name="_token" class="js_token" value="{{csrf_token()}}">

                                    <div class="form-group floating-label">
                                        <label for="id">分类栏目ID</label>
                                        <input type="text" id ="id" name="id" class="form-control" disabled="disabled" value="{{ (!empty($categories)) ? $categories[0]->id :'-1'}}">
                                    </div>

                                    <div class="form-group floating-label">
                                        <label for="name">分类栏目名</label>
                                        <input type="text" disabled="disabled" class="form-control" id="name" name="name" value="{{ (!empty($categories)) ? $categories[0]->name : ''}}">
                                    </div>

                                    <div class="form-group floating-label">
                                        <label for="weight">权重</label>
                                        <input type="text" class="form-control" id="weight"  name="weight" value="{{ (!empty($categories)) ? $categories[0]->weight : 0}}">
                                    </div>

                                    <div class="form-group floating-label">
                                        <label for="cate_desc">分类描述</label>
                                        <input type="text" class="form-control" id="cate_desc"  name="cate_desc" value="{{ (!empty($categories)) ? $categories[0]->cate_desc : ''}}">
                                    </div>

                                    <div class="form-group floating-label">
                                        <label for="parent_id">上级栏目</label>
                                        <select id="parent_id" name="parent_id" class="form-control" disabled="disabled">
                                            <option value="0" selected="selected">顶级栏目</option>

                                             {!! \App\Helpers\AdminViewHelper::categorySelectHtml($categories,'',$categories) !!}

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">是否启用</label>
                                        {!! \App\Helpers\AdminViewHelper::radioHtml('status', ['0'=>'关闭','1'=>'启用'], (!empty($categories)) ? $categories[0]->status : 1)  !!}
                                    </div>

                                    <div class="form-group">
                                        <label>分类图片</label>
                                        <div>
                                            <img id="showImage" src="@if(!empty($categories)) {{$categories[0]->img}} @endif" id="img"  class="img mgt-10" style="max-width=125px; max-height:78px;">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>上传分类图片(尺寸: 750x470)</label>
                                        <input type="file" class="form-control" name="img" id="categoryImage">
                                    </div>

                                    <div id="attributes-container"></div>
                                    <div class="form-group">
                                        &nbsp;
                                    </div>
                                    <div class="form-group">
                                        <a id="saveAnchor"    data-toggle="modal" data-target="#confirmModal" title="{{ trans('general.button.save') }}" class='btn btn-success'>保存</a>
                                        <a id="deleteAnchor"    data-toggle="modal" data-target="#confirmModal" title="{{ trans('general.button.delete') }}" class='btn btn-danger'>删除</a>
                                         <a id="productAnchor"    data-toggle="modal" data-target="#confirmModal" title="{{ trans('general.button.save') }}" class='btn btn-success'>分类商品</a>
                                    </div>
                                </div><!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 新增分类 -->
                <div class="tab-pane" id="create">
                    <div class="row">
                        <div class="box-body">
                        <input type="hidden" name="_token" class="js_token" value="{{csrf_token()}}">
                        <div class="form-group floating-label">
                            <label for="name">分类栏目名</label>
                            <input type="text" class="form-control" id="newname" name="name" value="">
                        </div>

                        <div class="form-group floating-label">
                            <label for="cate_desc">分类栏目描述</label>
                            <input type="text" class="form-control" id="newcate_desc"  name="cate_desc" value="">

                        </div>

                        <div class="form-group floating-label">
                            <label for="newweight">权重</label>
                            <input type="text" class="form-control" id="newweight"  name="newweight" value="0">
                        </div>

                        <div class="form-group floating-label">
                            <label for="newparent_id">上级栏目</label>
                            <select id="newparent_id" name="parent_id" class="form-control">
                                <option value="0" selected="selected">顶级栏目</option>
                                 {!! \App\Helpers\AdminViewHelper::categorySelectHtml($categories,'',$categories) !!}
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sta">是否启用</label>
                            {!! \App\Helpers\AdminViewHelper::radioHtml('sta', ['0'=>'关闭','1'=>'启用'], 1)!!}
                        </div>

                        <div class="form-group">
                            <label>上传分类图片(尺寸: 750x470)</label>
                            <input type="file" class="form-control" name="img" id="newcategoryImage">
                        </div>

                        <div id="attributes-container"></div>
                        <div class="form-group">
                            &nbsp;
                        </div>
                        <div class="form-group">
                            <a id="addAnchor"    data-toggle="modal" data-target="#confirmModal" title="{{ trans('general.button.add') }}" class='btn btn-success'>添加</a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop
@section('script')

<script type="text/javascript">

    window.onload=function() {
        $('.dd-button')[0].click();
    }

    // 设置默认品牌分类头图
    Helper.setDefaultImg($('#updateBrandCateImg~img'));

    var _token = $('.js_token').val();
    $('.dd-button').on('click', function() {
        var id = $(this).attr("data-id");
        $("#saveProductAnchor").attr('data-id',id);
        // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var urlShowRoute = '{!! action("Admin\\CategoryController@showData") !!}';
        $.ajax({
            url: urlShowRoute,
            type: 'GET',
            data: {
                _token: _token,
                id: id
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (!returnData) {
                    layer.msg('无法读取到分类信息');
                }
               
                var categoryObject = returnData.data;
                var categoryID              = categoryObject.id;
                var categoryName            = categoryObject.name;
                var categoryDesc            = categoryObject.cate_desc;
                var categoryStatus          = categoryObject.status;
                var categoryParentId        = categoryObject.parent_id;
                var image                   = categoryObject.img;
                var brandProducts           = categoryObject.brand_product;
                var categoryWeight          = categoryObject.weight;

                $('#name').val(categoryName);
                $('#id').val(id);
                $('#cate_desc').val(categoryDesc);
                $('#weight').val(categoryWeight);
                setRadio('status', categoryStatus);
                $('#parent_id').val(categoryParentId);
                $('#showImage').attr('src',image);
                if(!categoryObject.brand_product){
                    var tbody = $('#brand_tbody');
                    tbody.empty();
                    return false;
                }
                loadBrandCategorys(brandProducts, categoryName, categoryID);
                //插入分类品牌
            }
        });

    });

    // 添加品牌分类列表
    function loadBrandCategorys(brandProducts, categoryName, categoryID){
        var tbody = $('#brand_tbody');
        tbody.empty();
        var html  = '';
        if(brandProducts == null){
            html = '<tr></tr>';
        }else{
            var brand = brandProducts;
            var len = brandProducts.length;

            for (var i = 0 ; i < len; i++) {
                console.log(brand[i]['status']);
                if(brand[i]['status'] == 0){
                    continue;
                }
                if (brand[i]['brand']) {
                    html += '<tr>'
                         +  '<td class="number">' + (i+1) + '</td>'
                         +  '<td class="categoryName">' + categoryName + '</td>'
                         +  '<td class="brandCategory_id">' + brand[i]['id'] + '</td>'
                         +  '<td class="BrandName">' + brand[i]['brand']['name']+ '</td>'
                         +  '<td class="BrandCataImage"><img src="' + brand[i]['image'] + '" style="width: 125px; height: 78px;" /></td>'
                         +  '<td><a href="/admin/category/CateProduct?id='+categoryID+'&brand_id='+brand[i]['brand_id']+'" class="btn btn-sm ink-reaction btn-info btn-add" data-id="'+brand[i]['brand_id'] +'" >查看分类品牌商品</a>&nbsp;'
                         +  '<a href="/admin/productCategory/show?category_id='+categoryID+'&brand_id='+brand[i]['brand_id']+'" class="btn btn-sm ink-reaction btn-success btn-add" data-id="'+brand[i]['brand_id'] +'" >添加分类品牌商品</a>&nbsp;'
                         +  '<a  class="btn btn-sm ink-reaction btn-primary btn-add updateBrandCateImg" data-toggle = "modal" data-target = "#myModal" data-id="'+brand[i]['id'] +' " >修改品牌分类头图</a></td>'
                         +  '</tr>';
                }

            }
        }

        tbody.append(html);
    }

    // 显示模态框
    $(document).on('click', '.updateBrandCateImg', function(){
        var brandCateId = $(this).attr('data-id');
        $('#brandCateId').val(brandCateId);
    })
    // 发送品牌头图分类数据
   $(document).on('click', '#submitBrandCateImg', function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var urlSaveRoute = '{!! action("Admin\\CategoryController@updateBrandCateImg") !!}';

        var formData = new FormData();

        if($('#newcategoryImage')[0].files[0]){
             formData.append('files', $('#newcategoryImage')[0].files[0]);
        }
        formData.append('_token',CSRF_TOKEN);
        formData.append('brandCateId', $('#brandCateId').val());

        $.ajax({
            url: urlSaveRoute,
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('新增成功', {icon: 1});
                    setTimeout(function(){
                       window.location.href = '{!! action("Admin\\CategoryController@index") !!}';
                    },1000)
                }else{
                    layer.msg(returnData.message,{icon: 0});
                }

            }
        });

    })

     // 新增分类
    $('#addAnchor').click(function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var urlSaveRoute = '{!! action("Admin\\CategoryController@add") !!}';

        var formData = new FormData();

        if($('#newcategoryImage')[0].files[0]){
             formData.append('img', $('#newcategoryImage')[0].files[0]);
        }
        formData.append('_token',CSRF_TOKEN);
        formData.append('name',$('#newname').val());
        formData.append('cate_desc',$('#newcate_desc').val());
        formData.append('weight',$('#newweight').val());
        formData.append('parent_id',$('#newparent_id').val());
        formData.append('status',$("input[name='sta']:checked").val());

        $.ajax({
            url: urlSaveRoute,
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('新增成功', {icon: 1});
                    setTimeout(function(){
                       window.location.href = '{!! action("Admin\\CategoryController@index") !!}';
                    },1000)
                }else{
                    layer.msg(returnData.message,{icon: 0});
                }

            }
        });
    });

    // 删除分类
    $('#deleteAnchor').click(function(){
        var id  = $('#id').val();
        if( id  == -1){
            layer.msg('删除失败,未选中任何分类',{icon: 0});
            return false;
        }
        layer.confirm('您确定删除 "'+$('#name').val()+'" 这个分类？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                id  = id;
                // var _token = $('meta[name="csrf-token"]').attr('content');
                var urlDeleteRoute = '{!! action("Admin\\CategoryController@delete") !!}';
                $.ajax({
                    url: urlDeleteRoute,
                    type: 'POST',
                    data: {
                        _token: _token,
                        id: id
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

    // 编辑分类
    $('#saveAnchor').click(function(){
        var id  = $('#id').val();
        if( id  == -1){
            layer.msg('保存失败,未选中任何分类',{icon: 0});
            return false;
        }
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var urlSaveRoute = '{!! action("Admin\\CategoryController@save") !!}';
        var formData = new FormData();

        if($('#categoryImage')[0].files[0]){
             formData.append('img', $('#categoryImage')[0].files[0]);
        }
        formData.append('_token',CSRF_TOKEN);
        formData.append('id',id);
        formData.append('cate_desc',$('#cate_desc').val());
        formData.append('weight',$('#weight').val());
        formData.append('status',$("input[name='status']:checked").val());
        $.ajax({
            url: urlSaveRoute,
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('保存成功', {icon: 1});
                    setTimeout(function(){
                       window.location.reload();
                    },1000)
                }else{
                    layer.msg(returnData.message,{icon: 0});
                }

            }
        });
    });

     // 分类商品
    $('#productAnchor').click(function(){
        var id  = $('#id').val();
        if( id  == -1){
            layer.msg('未选中任何分类',{icon: 0});
            return false;
        }
        $url = '{!! url("admin/category_brand/product") !!}'+'/'+id;
        window.location.href = $url;
    });

  // radio
    function setRadio(radioName, value)
    {
        $('input:radio[name='+radioName+']')[value].checked = true;
    }

    //多选框选择时，显示删除按钮
    var checkbox_value = new Array();
    $('.checkbox').click(function(){
        if($(this).prop('checked') == false){
            checkbox_value.pop($(this).val());
            if(checkbox_value.length == 0){
                 $('#deleteCategoryAnchor').hide();
            }
        }else{
            checkbox_value.push($(this).val());
            $('#deleteCategoryAnchor').show();
        }
    })

    $('#deleteCategoryAnchor').click(function(){
        var categories;
        if( checkbox_value.length == 0){
            layer.msg('删除失败,未选中任何分类',{icon: 0});
            return false;
        }
        layer.confirm('您确定删除已选中的分类？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                categories = checkbox_value;
                // var _token = $('meta[name="csrf-token"]').attr('content');
                var urlDeleteRoute = '{!! action("Admin\\CategoryController@delete") !!}';
                $.ajax({
                    url: urlDeleteRoute,
                    type: 'POST',
                    data: {
                        _token: _token,
                        categories: checkbox_value
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    })
</script>
@stop

