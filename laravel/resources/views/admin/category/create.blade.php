
@extends('admin.widget.body')

@section('style_link')

<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">

@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加分类品牌商品</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
        <form action='{!! action("Admin\\ProductCategoryController@search") !!}' method="GET" class="form floating-label">
            <div class="card-body">
                 <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                    <div class='col-md-12'>
                    <div class="row">
                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '12',
                            'id'    => 'brandId',
                            'name'  => 'brand_id',
                            'title' => '请选择品牌',
                            'brand' =>  $brand,
                        ])
                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '12',
                            'id'    => 'categoryId',
                            'name'  => 'category_id',
                            'title' => '请选择品牌下的分类',
                            'brand' => []
                        ])
                    </div>
                    <div class="col-lg-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">搜索该品牌商品</h3>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="card-body">
                                    <div class="input-group">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入商品名称或产品编号">
                                            @if(isset($brand_category) && isset($is_showOne) && $is_showOne == 1 )
                                                <input type="hidden" class="form-control" name="brand_id" value="{{$brand_category['brand_id']}}" >
                                                <input type="hidden" class="form-control" name="category_id" value="{{$brand_category['category_id']}}" >
                                                <input type="hidden" class="form-control" name="is_showOne" value="1" >
                                            @endif

                                            <div class="form-control-line"></div>
                                        </div>
                                        <div class="input-group-btn">
                                            <button class="btn btn-floating-action btn-default-bright" type="submit" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>商品名称</th>
                                                <th>商品编号</th>
                                                <th>商品图片</th>
                                                <th style="width: 80px;">操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id="search_tbody">
                                        @if(isset($products))
                                            @foreach($products as $key => $product)
                                                <tr>
                                                  <td class="number"><label class="checkbox-inline checkbox-styled"><input type="checkbox" class="checkbox" name="checkeds[]" value="{{ $product->id }}"/><span class=""></span></label> {{ $key+1 }}</td>
                                                 <td class="productName">{{ $product->name }}</td>
                                                 <td class="productSnumber">{{ $product->snumber }}</td>
                                                 <td class="productMainImage"><img src="{{  \App\Helpers\ImageHelper::getImageUrl($product->main_image,'product')}}" style="width: 50px;" />
                                                 </td>
                                                 <td><a class="btn btn-sm ink-reaction btn-info btn-add" data-id="{{ $product->id }}" >添加</a></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
s
                                </div><!-- ./card-body -->
                                <div class="text-center paginate">
                                    @if(isset($products))
                                          {{$products->render()}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row text-right">
                            <div class="col-md-12">
                              <ul class="list-inline addShow">
                                <li class="next"><button  class="btn ink-reaction btn-primary btn-md btn-add-some">
                                 批量提交
                                </button></li>

                                <li class="next"><button  class="btn ink-reaction btn-info btn-md btn-add-all">
                                 全部提交
                                </button></li>
                            </ul>
                            </div>
                        </div>
                    </div><!-- ./col-lg-6 -->
                   </div><!-- ./row -->
            </div>
        </form>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    // 单个添加
    $(document).on('click','.btn-add',function(){
        addProduct($(this));
    });

    @if(isset($products) && !$products->isEmpty())
        $('.addShow').show();
    @else
        $('.addShow').hide();
    @endif

   @if(isset($brandId))
        $('#brandId').val("{{$brandId}}");
    @endif
    // 添加指定品牌分类的情况
   @if(isset($brand_category) && $brand_category['brand_id'])
     var brandId = $('#brandId');
        brandId.find('option[value='+{{$brand_category['brand_id']}}+']' )
            .attr({'selected' : 'true'})
            .change();
        brandId.prop('disabled', 'true');
    @endif

    $('#brandId').change(function(){
       brandSelectChange();
    });

    $('#categoryId').change(function(){
        var tbody = $('#search_tbody');
        tbody.empty();
        $('.paginate').empty();
        $('.addShow').hide();
    });

    function labelStyle(_this){
        _this.css('top',0);
        _this.css('fontSize','12px');
    }

    function brandSelectChange(){
        var id = $('#brandId').val();
        var url = '{!! action("Admin\\BrandController@brandCategory") !!}';
        // var tbody = $('#search_tbody');
        // tbody.empty();
        $.ajax({
          type: 'GET',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'brand_id':id},
        }).done(function(data) {
            if(data.status=='success'){
                loadCategory(data.data.categorys);
            }else{
                 $('#search_tbody').empty();
                 toastr.error('查询不到该商品.', '失败!')
            }
        }).fail(function(data) {
            toastr.error('查询不到该商品.', '失败!')
        });
    }

     // 添加分类
    brandSelectChange();
    //加载品牌分类
    function loadCategory(category){
        if(typeof category == 'undefined'){
            return false;
        }
        var len  = category.length;
        var html = '';
        var categoryId = $('#categoryId');
        for (var i = 0 ; i < len; i++) {
            var disabled = '';
            var selected = '';
            // 显示指定品牌分类情况判断
            @if(isset($brand_category) && $brand_category['category_id'])
                if(category[i]['id'] == {{$brand_category['category_id']}}){
                    categoryId.prop('disabled','true');
                    selected = 'selected = true';
                }
            @endif

            // 将指定的分类设置为被选择状态
            @if(isset($categoryId) && $categoryId)
                if(category[i]['id'] == {{$categoryId}}){
                    var selected = 'selected = true';
                }
            @endif
            html += '<option '+ selected +' value="'+category[i]['id']+'">'+category[i]['name']+'</option>'
        }
        $('#categoryId').empty();
        $('#categoryId').append(html);

        labelStyle($('#categoryId').next());
    }

    //添加商品到分类
    function addProduct(_this){
        var product_id  = _this.attr('data-id');
        var brand_id = $('#brandId').val();
        var category_id = $('#categoryId').val();
        var url = '{!! action("Admin\\ProductCategoryController@store") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'product_id':product_id,'category_id':category_id,'brand_id':brand_id},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('添加成功', '成功!');
                _this.text('已添加');
                _this.css('border','none');
                _this.css('backgroundColor','#ccc');
                _this.removeClass('btn-add');
            }else{
                 toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            toastr.error('添加商品到分类失败.', '失败!')
        });
    }

    // 批量插入
    $('body').on('click','.btn-add-some',function(){
        var checkbox = $('#search_tbody').find('.checkbox:checked');
        var productIds = [];
        $.each(checkbox,function(){
            productIds.push($(this).val());
        })
        // 批量提交
        sendCategoryProduct(productIds);
    });

    // 插入全部
    $('body').on('click','.btn-add-all',function(){
        var product = $('#search_tbody').find('.btn-add');
        var productIds = [];
        $.each(product,function(){
            productIds.push($(this).attr('data-id'));
        })
        // 提交全部
        sendCategoryProduct(productIds);
    });

    // 提交全部或批量提交分类商品
    function sendCategoryProduct(productIds){
        if(productIds == ''){
            toastr.error('批量添加失败,当前未筛选出商品', '失败!')
            return false;
        }
        var brand_id = $('#brandId').val();
        var category_id = $('#categoryId').val();
        var url = '{!! action("Admin\\ProductCategoryController@insertAll") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'productIds':productIds,'category_id':category_id,'brand_id':brand_id},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('批量添加成功', '成功!');
                setTimeout(function(){
                     window.location.reload();
                 }, 1000);
            }else{
                 toastr.error('批量添加失败', '失败!')
            }
        }).fail(function(data) {
            toastr.error('添加商品到分类失败.', '失败!')
        });
        return false;
    }
</script>
@stop
