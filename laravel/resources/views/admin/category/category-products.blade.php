@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">商品列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>商品名</th>
                            <th>商品编号</th>
                            <th>商品主图</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td >{{ $product->snumber }}</td>
                            <td>
                            @if($product->main_image)
                            <img src="{{$product->main_image}}" width="50px">
                            @else
                            暂无图片
                            @endif
                            </td>
                            <td>
                            <a class="btn btn-xs ink-reaction btn-danger btn-delete" data-id="{{$product->productCategoryWithoutBrand->id}}" >移除</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $products->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //移除
    $('.btn-delete').click(function(){
        var url = '{!! action("Admin\\ProductCategoryController@delete") !!}';
        var _this = $(this);
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'id':_this.attr('data-id')},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('移除成功', '成功!')
                window.location.reload();
            }else{
                 toastr.error('移除失败', '失败!')
            }
        }).fail(function(data) {
            toastr.error('移除失败', '失败!')
        });
    })
</script>
@stop
