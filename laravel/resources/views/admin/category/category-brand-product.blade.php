@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">商品列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="col-md-12 col-lg-12 text-right" style="margin-top: 22px">
                    <a href="javascript:void(0);" class="ink-reaction btn-primary btn-sm selectAll" type="button">全选</a>
                    <a href="javascript:void(0);" class="ink-reaction btn-danger btn-sm deleteProduct" type="button">移除</a>
                </div>
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>id</th>
                            <th>商品名</th>
                            <th>商品编号</th>
                            <th>商品主图</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                             <td>
                              <label class="checkbox-inline checkbox-styled" >
                                <input name="product[]"  type="checkbox" value="{{$product->productCategories[0]->id}}" class=" js-product"><span></span>
                              </label>
                            </td>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td >{{ $product->snumber }}</td>
                            <td>
                            @if($product->main_image)
                            <img src="{{$product->main_image}}" width="50px">
                            @else
                            暂无图片
                            @endif
                            </td>
                            <td>
                            <a class="btn btn-xs ink-reaction btn-danger btn-delete" data-id="{{$product->productCategories[0]->id}}" >移除</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $products->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //移除
    $('.btn-delete').click(function(){
        var url = '{!! action("Admin\\ProductCategoryController@delete") !!}';
        var _this = $(this);
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'id':_this.attr('data-id')},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('移除成功', '成功!')
                window.location.reload();
            }else{
                 toastr.error('移除失败', '失败!')
            }
        }).fail(function(data) {
            toastr.error('移除失败', '失败!')
        });
    })

    //全选
    $('.selectAll').live('click',function(){
        selectAll();
    });

    var selectAllStatus = 1;
    function selectAll()
    {
        if(selectAllStatus == 1){
            $.each($('.js-product'),function(){
                $(this).attr('checked','checked');
            })
            selectAllStatus = 0;
        }else{
            $.each($('.js-product'),function(){
                $(this).removeAttr('checked');
            })
            selectAllStatus = 1;
        }
    }

    $('.deleteProduct').click(function(){
        var products = $('.js-product');
        var ids = [];
        $.each(products,function(){
            if($(this).attr('checked') == 'checked'){
                ids.push($(this).val());
            }
        });

        if(ids.length <= 0){
            layer.msg('批量删除失败,请先选择商品',{icon: 0});
            return false;
        }
        $.ajax({
            url: '{!! action("Admin\\ProductCategoryController@deleteAll") !!}',
            type: 'POST',
            data: {
                 ids : ids,
                 _token :"{!! csrf_token() !!}",
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('批量删除成功',{icon:1});
                    window.location.reload();
                }else{
                   layer.msg('批量删除失败,请刷新重试',{icon: 0});
                }
            }
        });
    })
</script>
@stop
