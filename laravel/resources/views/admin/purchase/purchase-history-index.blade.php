@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">首单购商品历史列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th>商品名</th>
                            <th>商品图片</th>
                            <th>商品市场价</th>
                            <th>赠品已送人数</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($purchases as $purchase)
                         <tr >
                            <td>{{ $purchase->sellProduct->product->name or ''}}</td>
                            <td>
                            @if(isset($purchase->sellProduct->all_image[0]))
                            <img src="{{$purchase->sellProduct->all_image[0]}}" width="50px" height="50px">
                            @endif
                            </td>
                            <td>
                            {{$purchase->sellProduct->market_price or ''}}
                            </td>
                            <td>{{$purchase->count or ''}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $purchases->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var _token = $('#_token').val();
    //商品上下架
    $('.js_text').live('click', function() {
        deleteText($(this));
    });

    //delete 
    function deleteText(_this)
    {
        $.ajax({
            url: '{!! action("Admin\\FirstPurchaseTextController@delete") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-id'),
                _token      :  _token,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success('成功', '删除成功');
                    window.location.reload();
                }else{
                     toastr.error('失败', '删除失败!')
                }
            }
        });

    }
</script>
@stop
