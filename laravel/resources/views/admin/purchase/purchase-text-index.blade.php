@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">首单购文本列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                 <form class="form floating-label" autocomplete="off">
                    <div class="form-group">
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'title',
                            'title' => '首单购标题',
                            'value' => isset($activity->title)?$activity->title:''
                        ])
                    </div>
                </form>
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th >id</th>
                            <th >文本</th>
                            <th >操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($purchaseText as $text)
                        <tr >
                            <td>{{ $text->id }}</td>
                            <td>{{ $text->text }}</td>
                            <td>
                            <a href="{{ action('Admin\FirstPurchaseTextController@show', ['id' => $text->id])}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                          
                            <a href="javascript:void(0);" class="btn ink-reaction btn-xs btn-danger js_text" type="button" data-id = "{{$text->id}}">删除</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $purchaseText->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var _token = $('#_token').val();
    //商品上下架
    $('.js_text').live('click', function() {
        deleteText($(this));
    });

    //delete 
    function deleteText(_this)
    {
        $.ajax({
            url: '{!! action("Admin\\FirstPurchaseTextController@delete") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-id'),
                _token      :  _token,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success('成功', '删除成功');
                    window.location.reload();
                }else{
                     toastr.error('失败', '删除失败!')
                }
            }
        });

    }
</script>
@stop
