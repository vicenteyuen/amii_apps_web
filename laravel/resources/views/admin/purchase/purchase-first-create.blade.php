     
@extends('admin.widget.body')

@section('style_link')

<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加首单购商品</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">  
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                     <form class="form floating-label" autocomplete="off">
                    @include('admin.widget.input',[
                                'name' => 'payMoney', 
                                'id' => 'payMoney',
                                'title' => '首单购实付金额',
                                'colsm' => '4',
                                'collg' => '4',
                                'value' => isset($money) ? $money: ''
                          ])
                    @include('admin.widget.button', [
                            'id' => 'pay',
                            'text' => '保存',
                            'colsm' => '4',
                            'collg' => '4',
                        ])
                    </form>
                    </div>
                </div> 
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">搜索商品</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="card-body">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入">
                                        <label for="search">请输入商品名称或产品编号</label>
                                        <div class="form-control-line"></div>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-floating-action btn-default-bright" type="button" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>商品名称</th>
                                            <th>商品编号</th>
                                            <th style="width: 80px;">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody id="search_tbody">

                                    </tbody>
                                </table>
                            </div><!-- ./card-body -->
                        </div>
                    </div>
                </div><!-- ./col-lg-6 -->
            </div><!-- ./row -->
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).on('click','.btn-add',function(){
        addProduct($(this));
    });

    $(document).on('click','#pay',function(){
        pay($(this));
    });

    //search 
    $('#search-button').click(function(){
        var tbody = $('#search_tbody');
        tbody.empty();
        var url = '{!! action("Admin\\FirstPurchaseController@search") !!}';
        $.ajax({
          type: 'GET',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'key':$('#key').val()},
        }).done(function(data) {
            if(data.status=='success'){
                if(!data.data){
                    toastr.error('查询不到该的商品.', '失败!')
                    return false;
                }
                loadProduct(data.data);
            }else{
                 toastr.error('查询不到该的商品.', '失败!')
            }
        }).fail(function(data) {
            toastr.error('查询不到该的商品.', '失败!')
        });

    });

    //添加商品
    function addProduct(_this){
        var sell_product_id  = _this.attr('data-id');
        var url = '{!! action("Admin\\FirstPurchaseController@store") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'sell_product_id':sell_product_id},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('添加成功', '成功!');
                window.location.href = '{!! action("Admin\\FirstPurchaseController@index") !!}';
            }else{
                 toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            toastr.error('添加失败,请刷新重试.', '失败!')
        });
    }

    function pay(_this){
        var money  = $('#payMoney').val();
        var url = '{!! action("Admin\\FirstPurchaseController@saveMoney") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'money':money},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('操作成功', '成功!');
            }else{
                 toastr.error('操作失败,请刷新重试.', '失败!')
            }
        }).fail(function(data) {
            toastr.error('操作失败,请刷新重试.', '失败!')
        });
    }


    // 添加到搜索商品列表
    function loadProduct(product){
        var tbody = $('#search_tbody');
        var html  = '';
        var len   = product.length;
        for (var i = 0 ; i < len; i++) {
            html += '<tr>'
                 +  '<td class="number">' + (i+1) + '</td>'
                 +  '<td class="productName">' + product[i]['name'] + '</td>'
                 +  '<td class="productSnumber">' + product[i]['snumber'] + '</td>'
                 +  '<td><a class="btn btn-sm ink-reaction btn-info btn-add" data-id="'+product[i]['sell_product'][0]['id'] +'" >添加</a></td>' 
                 +  '</tr>';
        }
        tbody.append(html);
    }

</script>
@stop