@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">

@stop
@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">首单购管理</li>
        </ol>
    </div>

     <div class="section-body">
        <div class="card">
            <div class="card-body">
             <div class="card-head">
                        <div class="btn-group">
                            <a href="javascript:void(0);" class="btn ink-reaction btn-default btn-sm pull-left js-purchase" data-status = "{{$purchase->status or ''}}" data-id = "{{$purchase->id or ''}}">
                                @if(isset($purchase->status) && $purchase->status == 1)
                                已开启
                                @else
                                已关闭
                                @endif
                            </a>

                        </div>
                    <div class="tools">
                        
                        <!-- <div class="btn-group">
                            <a href="{!! action('Admin\FirstPurchaseController@history') !!}" class="btn ink-reaction btn-default btn-sm pull-right">
                               首单商品记录
                            </a>

                        </div> -->
                        <div class="btn-group">
                            <a href="{!! action('Admin\FirstPurchaseController@create') !!}" class="btn ink-reaction btn-default btn-sm pull-right">
                                @if(isset($purchase->sellProduct))
                                    更换首单购礼品
                                @else
                                    添加首单购礼品
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>商品名</th>
                            <th>商品图片</th>
                            <th>商品市场价</th>
                            <th>首单金额</th>
                            <th>赠品已送总人数</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td>{{ $purchase->sellProduct->product->name or ''}}</td>
                            <td>
                            @if(isset($purchase->sellProduct->all_image[0]))
                            <img src="{{$purchase->sellProduct->all_image[0]}}" width="50px" height="50px">
                            @endif
                            </td>
                            <td>
                            {{$purchase->sellProduct->market_price or ''}}
                            </td>
                            <td>
                            {{$purchase->money or ''}}
                            </td>
                            <td>{{$purchase->count or ''}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).on('click','.js-purchase',function(){
        changeStatus($(this));
    });

    function changeStatus(_this){
        var id      = _this.attr('data-id');
        var status  = _this.attr('data-status');
        if(status == 1){
            successMessage = '关闭成功';
            errorMessage = '关闭失败';
            status = 0;
        }else{
            successMessage = '开启成功';
            errorMessage = '开启失败';
            status = 1;
        }
        var url = '{!! action("Admin\\FirstPurchaseController@update") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'id':id,'status':status},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success(successMessage, '成功!');
                window.location.href = '{!! action("Admin\\FirstPurchaseController@index") !!}';
            }else{
                 toastr.error(errorMessage, '失败!')
            }
        }).fail(function(data) {
            toastr.error('操作失败,请刷新重试.', '失败!')
        });
    }
</script>
@stop
