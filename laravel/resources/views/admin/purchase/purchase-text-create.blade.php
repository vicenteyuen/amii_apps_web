@extends('admin.widget.body')

@section('content')

<section>
    <!--head-->
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加首单购文本</li>
        </ol>
    </div>
    <!--end head-->
    <div class="section-body">
        <div class="card">
            <form method="post" action="{{$url}}" class="form floating-label">
            <div class="card-body tab-content">
                <!-- 基本信息 -->
                <input type="hidden" name="id" value="{{$id or ''}}">
                <div class="tab-pane active" id="base-info">
                    <div class="form-group">
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'title',
                            'title' => '首单购标题',
                            'value' => isset($activity->title)?$activity->title:''
                        ])

                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'order_description',
                            'title' => '订单描述',
                            'value' => isset($activity->order_description)?$activity->order_description:''
                        ])

                        @include('admin.widget.textarea', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'text',
                            'title' => '首单购简介',
                            'value' => isset($purchaseText->text)?$purchaseText->text:''
                        ])
                    </div>
                </div>
                <!-- 基本信息结束 -->

            </div><!-- ./.card-body -->

            <div class="card-actionbar">
                <div class="card-actionbar-row">
                    <button class="btn ink-reaction btn-primary" type="submit">@if (isset($id))编辑@else添加@endif
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
@stop

