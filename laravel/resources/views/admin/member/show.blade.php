@extends('admin.widget.body')

@section('style_link')
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')

<section>

    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">会员基本信息</li>
        </ol>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="col-sm-12">
                <div class="card card-underline">
                    <div class="card-head">
                        <header>基本信息</header>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <span class="col-sm-2">
                                    <div class="comment-avatar">
                                        <img src="{{ $user['avatar'] }}" width="100" height="100" class="img-circle">
                                    </div>
                                    <input type="text" hidden name="id" value="{{$user['id']}}">
                                </span>
                                <span class="col-sm-10 padding">
                                    昵称：{{ $user['nickname'] }} <br>
                                    手机: {{ $user['phone'] }} <br>
                                    性别: {{ \App\Helpers\EnumHelper::getGender($user['gender']) }}<br>
                                    年龄: {{ $user['age'] ? $user['age'] : '' }} <br>
                                    <div>出生日期: </div>
                                    @include('admin.widget.datetimepicker', [
                                            'colsm' => '12',
                                            'collg' => '6',
                                            'name' => 'birthday',
                                            'class' => 'birthday',
                                            'id' => 'birthday',
                                            'value' => $user['birthday'] ? $user['birthday']: '',
                                    ])
                                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group text-left">
                                            <a id="changeBirthday" class='btn btn-success'>修改</a>
                                        </div>
                                    </div>
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0;">

                                    注册时间: {{ $user['created_at'] }} <br>
                                    登录时间: {{ $user['last_log_time'] }} <br>
                                    部落上级:
                                    <a
                                    @if(isset($parentTribe['user']))
                                    href="{{action('Admin\MemberController@show',['show'=>$parentTribe['user']['id']])}}"
                                     @endif>
                                    {{ $parentTribe['user']['nickname'] or '暂无'}}
                                    </a>
                                   </div>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card card-underline">
                    <div class="card-head">
                        <header> 收货地址 </header>
                    </div>
                        <div class="card-body">
                            @if (!empty($addresses))
                                <table class="table table-striped table-condensed no-margin" id="address-table">
                                    <thead>
                                        <tr>
                                            <th width="50">默认</th>
                                            <th width="120">收件人</th>
                                            <th width="160">手机</th>
                                            <th width="240">所在地区</th>
                                            <th>详细地址</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($addresses as $address)
                                        <tr>
                                            <td>{{ $address['status'] == 1 ? '是' : '否' }}</td>
                                            <td>{{ $address['realname'] }}</td>
                                            <td>{{ $address['phone'] }}</td>
                                            <td></td>
                                            <td>{{ $address['detail'] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                该会员目前尚未填写收货地址
                            @endif
                        </div>
                </div>


            </div>

            <div class="col-sm-12">

                <!-- 一级部落 -->
                <div class="card card-underline">
                    <div class="card-head">
                        <header>一级部落</header>
                    </div>
                    <div class="card-body">
                        @if ($firstTribes->count())
                            <table class="table table-striped table-condensed no-margin" id="member-table">
                                <thead>
                                    <tr>
                                        <th width="40px">@sortablelink('id', '#')</th>
                                        <th width="50px">性别</th>
                                        <th>头像</th>
                                        <th>@sortablelink('nickname', '昵称') </th>
                                        <th>手机号码</th>
                                        <th>@sortablelink('created_at', '注册时间')</th>
                                        <!-- <th>@sortablelink('created_at', '最后登录时间')</th> -->
                                        <th width="150px">操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($firstTribes as $k=>$v)
                                    <?php $userUrl = action('Admin\MemberController@show', $v['user_id']); ?>
                                    <tr>
                                        <td><a href="{{ $userUrl }}">{{ $v['user_id'] }}</a></td>
                                        <td>{{ \App\Helpers\EnumHelper::getGender($v['user']['gender']) }}</td>
                                        <td>
                                            @if(isset($v['user']['avatar']))
                                                    <a href="{{ $userUrl }}">
                                                        <img src="{{ $v['user']['avatar'] }}" width="50" height="50">
                                                    </a>
                                                @else
                                                    暂无
                                            @endif
                                        </td>
                                        <td><a href="{{ $userUrl }}">{{ $v['user']['nickname'] }}</a></td>
                                        <td>{{ $v['user']['phone'] }}</td>
                                        <td>{{ date('Y-m-d', strtotime($v['user']['created_at'])) }}</td>
                                        <td >
                                            <a href="{{ action('Admin\MemberController@show', $v['user_id'])}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">查看</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">
                                {!! $firstTribes->appends(['spage' => $secondTribes->currentPage() ])->render() !!}
                            </div>
                        @else

                            暂无部落

                        @endif
                    </div>
                </div><!-- end card -->


                <!-- 二级部落 -->
                <div class="card card-underline">
                    <div class="card-head">
                        <header>二级部落</header>
                    </div>
                    <div class="card-body">
                        @if ($secondTribes->count())
                            <table class="table table-striped table-condensed no-margin" id="member-table">
                                <thead>
                                    <tr>
                                        <th width="40px">@sortablelink('id', '#')</th>
                                        <th width="50px">性别</th>
                                        <th>头像</th>
                                        <th>@sortablelink('nickname', '昵称') </th>
                                        <th>手机号码</th>
                                        <th>@sortablelink('created_at', '注册时间')</th>
                                        <!-- <th>@sortablelink('created_at', '最后登录时间')</th> -->
                                        <th width="150px">操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($secondTribes as $k=>$v)
                                    <?php $userUrl = action('Admin\MemberController@show', $v['user_id']); ?>
                                    <tr>
                                        <td><a href="{{ $userUrl }}">{{ $v['user_id'] }}</a></td>
                                        <td>{{ \App\Helpers\EnumHelper::getGender($v['user']['gender']) }}</td>
                                        <td>
                                            @if(isset($v['user']['avatar']))
                                                <a href="{{ $userUrl }}">
                                                    <img src="{{ $v['user']['avatar'] }}" width="50" height="50">
                                                </a>
                                            @else
                                                暂无
                                            @endif
                                        </td>
                                        <td><a href="{{ $userUrl }}">{{ $v['user']['nickname'] }}</a></td>
                                        <td>{{ $v['user']['phone'] }}</td>
                                        <td>{{ date('Y-m-d', strtotime($v['user']['created_at'])) }}</td>
                                        <td >
                                            <a href="{{ action('Admin\MemberController@show', $v['user_id'])}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">查看</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">
                                {!! $secondTribes->appends(['fpage' => $firstTribes->currentPage() ])->render() !!}
                            </div>
                        @else
                            暂无部落
                        @endif
                    </div><!-- end card-body -->
                </div><!-- end card -->

                  <!-- 余额记录 -->
                <div class="card card-underline">
                    <div class="card-head">
                        <header>余额记录</header>
                    </div>
                    <div class="card-body">
                        @if ($balances->count())
                            <table class="table table-striped table-condensed no-margin" id="member-table">
                                <thead>
                                    <tr>
                                        <th>类型</th>
                                        <th>金额</th>
                                        <th>收益来源</th>
                                        <th>订单号</th>
                                        <th>类型</th>
                                        <th>部落收益状态</th>
                                        <th>时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($balances as $balance)
                                    @php
                                         if($balance->value == 0){
                                            continue;
                                         }
                                    @endphp
                                    <tr>
                                       <td>{{$balance->type_str}}</td>
                                       <td>{{$balance->value}}</td>
                                       <td>
                                           @if(!empty($balance->user))
                                            {{$balance->user->nickname}}
                                           @else
                                            无
                                           @endif
                                       </td>
                                       <td>
                                           @if(!empty($balance->order))
                                            {{$balance->order->order_sn}}
                                           @else
                                            无
                                           @endif
                                       </td>
                                       <td>
                                            @if($balance->value >= 0)
                                                <span style="color: green">收入</span>
                                            @else
                                                <span style="color: red">支出</span>
                                            @endif

                                       </td>
                                       <td>
                                           @if($balance->type == 1)
                                                @if($balance->status == 1)
                                                    <span style="color: green">已返</span>
                                                @else
                                                <span style="color: red">待返</span>
                                                @endif
                                           @else
                                              无
                                           @endif
                                       </td>
                                       <td>{{$balance->admin_created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">
                               {{$balances->render()}}
                            </div>
                        @else
                            暂无记录
                        @endif
                    </div>
                </div><!-- end card -->



            </div><!-- end col-sm-6 -->
        </div>
    </div>
</section>


@stop

@section('script_link')
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop

@section('script')
<script type="text/javascript">
    // 生日事件插件
    $(".birthday").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 2,
        forceParse: 0,
    });

    $(document).on('click', '#changeBirthday', function(){
        var value = $('input[name=birthday]').val();
        var id = $('input[name=id]').val();
        $.ajax({
            type: 'POST',
            url: '{!! action("Admin\\MemberController@changeBirthday") !!}',
            data: {
                    _token : "{!! csrf_token() !!}",
                    userId: id,
                    birthday: value,
                },
            dataType: 'JSON',
            success: function (returnData) {
                    if (returnData.status == 'success') {
                        layer.msg('修改成功',{icon:1});
                        window.location.reload();
                    }else{
                       layer.msg(returnData.message,{icon: 0});
                    }
                }
        })
    });

</script>
@stop

