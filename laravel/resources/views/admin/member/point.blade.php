@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">积分列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="card-body">
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                            <tr>
                                <th>积分类型</th>
                                <th>积分值</th>
                                <th>赠送时间</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($points as $point)
                            <tr>
                                <td>{{ $point->type_str }}</td>
                                <td>{{ $point->points}}</td>
                                <td>{{ $point->updated_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    {!! $points->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
@stop
