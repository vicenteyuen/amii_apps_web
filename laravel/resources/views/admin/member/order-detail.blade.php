@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">订单详情</li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i> 订单信息</header>
                    </div>
                    <div class="card-body">
                        <dl class="dl-horizontal">
                            <dt>订单号</dt>
                            <dd>{{ $order->order_sn }}</dd>
                            <dt>订单状态</dt>
                            <dd>{{\App\Helpers\OrderHelper::statusStr($order->status_str)}}</dd>
                            <dt>订单类型</dt>
                            <dd> @if($order->provider == 'standard')
                                    普通订单
                                @else
                                    积分订单
                                @endif
                            </dd>
                            <dt>运费</dt>
                            <dd>{{ $order->shipping_fee or '0.00' }}￥</dd>
                            <dt>优惠</dt>
                            <dd>{{$order->coupon_discount or '0.00' }}￥</dd>
                            <dt>商品总额</dt>
                            <dd>{{$order->product_amount}}
                            @if($order->provider == 'standard')￥
                            @endif
                            </dd>
                            <dt>实付金额</dt>
                            <dd>{{ $order->fee }}
                            @if($order->provider == 'standard')￥
                            @endif
                            </dd>
                            <dt>下单时间</dt>
                            <dd>{{ $order->created_at}}</dd>
                            <dt>购买者</dt>
                            <dd>{{ ($order->shipping_address)['name']}}</dd>
                            <dt>手机</dt>
                            <dd>{{ ($order->shipping_address)['phone']}}</dd>
                            <dt>快递地址</dt>
                            <dd>{{ ($order->shipping_address)['address']}}</dd>
                        </dl>
                    </div>

                </div>
            </div>

            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i> 商品信息</header>
                    </div>
                    <div class="card-body">
                            @foreach($order->orderProducts as $orderProduct)
                            <dl class="dl-horizontal">
                                <dt>商品名</dt>
                                <dd>{{ $orderProduct->sellProduct->product->name}}</dd>
                                <dt>商品图片</dt>
                                <dd><img width="50px" height="50px" src="{{$orderProduct->sellProduct->all_image[0]}}"></dd>
                                <dt>商品属性</dt>
                                <dd>
                                    @foreach($orderProduct->inventory->sku as $sku)
                                        <span>{{$sku->skuValue->attribute->name}}</span> 
                                        <span> {{$sku->skuValue->value}}</span>
                                    @endforeach
                                </dd>
                                @if($orderProduct->sellProduct->provider == 1)
                                    <dt>商品价格</dt>
                                    <dd>{{$orderProduct->sellProduct->market_price}}￥</dd>
                                @else
                                     <dt>商品积分</dt>
                                     <dd>{{$orderProduct->sellProduct->product_point}}</dd>
                                @endif
                                <dt>商品数量</dt>
                                <dd>{{$orderProduct->number}}件</dd>
                            <!-- 售后 -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="row text-right btn-commit">
                                        <div class="col-md-12">
                                        <ul class="list-unstyled">
                                            <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md js-detail" data-id = "{{$orderProduct->sell_product_id}}">售后详情</button></li>
                                        </ul>
                                        </div>
                                    </div>
                                    <div class="row js-after-sell-{{$orderProduct->sell_product_id}}" style="display: none;" >
                                        @if(isset($orderProduct->refund))
                                        <dl class="dl-horizontal">
                                          <dt>申请类型</dt>  
                                              @if($orderProduct->refund->provider == 'pay')
                                                 <dd>退款</dd>
                                              @else
                                                <dd>退货退款</dd>
                                              @endif
                                          <dt>申请状态</dt>
                                             @if($orderProduct->refund->status == 0)
                                                <dd>申请中</dd>
                                                <dt>@if($order->provider == 'standard')
                                                    申请退款金额
                                                    @else
                                                    申请退积分数
                                                    @endif
                                                </dt>
                                                <dd>{{$orderProduct->refund->pay}}</dd>
                                                <dt>原因</dt>
                                                <dd>{{$orderProduct->refund->applied_data['reason'] or '暂无'}}</dd>
                                                <dt>图片</dt>
                                                <dd>
                                                    @if(isset($orderProduct->refund->logs[0]->images) && !empty($orderProduct->refund->logs[0]->images))
                                                        @foreach($orderProduct->refund->logs as $log)
                                                            @foreach($log->images as $image)
                                                                <img  src="{{$image['value']}}" width="50px" height="50px">
                                                            @endforeach
                                                        @endforeach
                                                    @else
                                                        暂无
                                                    @endif
                                                </dd>
                                                <dt>
                                                    @if($order->provider == 'standard')
                                                    最高可退金额
                                                    @else
                                                    最高可退积分
                                                    @endif
                                                </dt>
                                                <dd>{{$orderProduct->refund->pay}}</dd>
                                                <!-- 申请中 -->
                                                <!-- 已同意 -->
                                                    @if($orderProduct->refund->refund_status == 1)
                                                    <dt>商家</dt>
                                                    <dd>已同意</dd>
                                                        @if($orderProduct->refund->provider == 'product')
                                                        <button data-id="{{$orderProduct->refund->id}}"  class="btn ink-reaction btn-primary btn-xs js-received" style="margin-left: 100px">确认收货</button>
                                                        @endif
                                                    @endif
                                             @elseif($orderProduct->refund->status == 1)
                                                <dd>完成</dd>
                                             @elseif($orderProduct->refund->status == 2)
                                                <dd>已拒绝</dd>
                                             @endif
                                        </dl>
                                        @else
                                            <span>暂无售后申请</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            </dl>
                            @endforeach

                    </div>

                </div>
            </div>
        </div>
        

        @if(isset($express->info))
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i>物流信息</header>
                    </div>
                    <div class="card-body">
                                <dt>订单号</dt>
                                <dd>{{$express['info']['nu']}}</dd>
                        @foreach($express['info']['data'] as $value)
                            <dl class="dl-horizontal">
                                <dt>内容</dt>
                                <dd>{{ is_array($value['context']) ? $value['context']['date']:$value['context']  }}</dd>
                                <dt>时间</dt>
                                <dd>{{ is_array($value['time']) ? $value['time']['date']:$value['time']  }}</dd>
                            </dl>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>   
        @endif  

        @if($order->status_str == 'refunding')
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i>订单售后</header>
                    </div>
                    <div class="card-body">
                         <dl class="dl-horizontal">
                            <dt>订单号</dt>
                            <dd>{{ $order->order_sn }}</dd>
                            <dt>订单状态</dt>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        @endif


    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $('.js-detail').click(function(){
        $id = $(this).attr('data-id');
        $('.js-after-sell-'+$id).css('display','block');
    })

</script>
@stop
