@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">订单列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="card-body">
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                            <tr>
                                <th>订单号</th>
                                <th>订单类型</th>
                                <th>订单状态</th>
                                <th>购买者</th>
                                <th>下单时间</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->order_sn}}</td>
                                <td>
                                    @if($order->provider == 'standard')
                                        普通订单
                                    @else
                                        积分订单
                                    @endif
                                </td>
                                <td> {{\App\Helpers\OrderHelper::statusStr($order->status_str)}}
                                </td>
                                <td>{{ $order->shipping_address['name'] }}</td>
                                <td>{{ $order->created_at}}</td>
                                <td>
                                <a href="{{ action('Admin\OrderController@userOrderDetail', ['orderSn' => $order->order_sn])}}" class="btn ink-reaction btn-success btn-xs" type="button">详情</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    {!! $orders->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
@stop
