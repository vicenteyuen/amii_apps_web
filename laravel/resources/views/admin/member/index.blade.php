@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">会员列表</li>
        </ol>
    </div>
    <div class="card">
        <div class="card-head">
            <header></header>
            <div class="tools">
                <div class="btn-group col-md-6 col-lg-6 pull-right">
                    <form method="GET" action="" accept-charset="UTF-8" id="search-form">
                        <div class="input-group input-group-lg pull-right" >
                            <div class="input-group-content" >
                                <input type="text" placeholder="请输入姓名或手机号码" id="search" name="search" class="form-control" value="" style="text-indent: 10px;">
                                <div class="form-control-line"></div>
                               @include('admin.widget.datetimepicker', [
                                    'colsm' => '6',
                                    'collg' => '6',
                                    'title' => '开始时间',
                                    'name' => 'start',
                                    'class' => 'startDate',
                                    'style' => 'height:46px',
                                    'value' => old('start') ? old('start') : ''
                                ])
                                @include('admin.widget.datetimepicker', [
                                    'colsm' => '6',
                                    'collg' => '6',
                                    'title' => '结束时间',
                                    'name' => 'end',
                                    'class' => 'endDate',
                                    'style' => 'height:46px',
                                    'value' => old('end') ? old('end') : ''
                                ])
                            </div>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-floating-action btn-default-bright"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form> <!-- ./form -->
                </div>
            </div>
        </div><!--end .card-head -->

        <div class="card-body">
            <table class="table table-striped table-condensed no-margin" id="member-table">
                <thead>
                    <tr>
                        <th width="40px">@sortablelink('id', '#')</th>
                        <th width="50px">性别</th>
                        <th>头像</th>
                        <th>@sortablelink('nickname', '昵称') </th>
                        <th>手机号码</th>
                        <th>@sortablelink('created_at', '注册时间')</th>
                        <!-- <th>@sortablelink('created_at', '最后登录时间')</th> -->
                        <th>余额</th>
                        <th>累积积分</th>
                        <th>可用积分</th>
                        <th width="200px">操作</th>
                    </tr>
                </thead>
                <tbody>

                @foreach ($members as $k=>$v)
                    <?php $userUrl = action('Admin\MemberController@show', $v['id']); ?>
                    <tr>
                        <td><a href="{{ $userUrl }}">{{ $v['id'] }}</a></td>
                        <td>{{ \App\Helpers\EnumHelper::getGender($v['gender']) }}</td>
                        <td>
                        @if(isset($v['avatar']))
                        <a href="{{ $userUrl }}">
                        <img src="{{ $v['avatar'] }}" width="50" height="50">
                        </a>
                        @else
                        暂无
                        @endif
                        </td>
                        <td><a href="{{ $userUrl }}">{{ $v['nickname'] }}</a></td>
                        <td>{{ $v['phone'] }}</td>
                        <td>{{ date('Y-m-d H:i:s', strtotime($v['created_at'])) }}</td>
                        <td>{{ $v['cumulate_profit']}}</td>
                        <td>{{ $v['sum_points']}}</td>
                        <td>{{ $v['points']}}</td>
                        <td >
                            <a href="{{ action('Admin\MemberController@show', $v['id'])}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">查看</a>
                            <a href="{{ action('Admin\OrderController@userOrder', $v['id'])}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">订单</a>
                            <a href="{{ action('Admin\PointReceivedController@show', $v['id'])}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">积分详情</a>
                            @if($v['status'] == 1)
                                   <a href="javascript:void(0);" class="btn ink-reaction btn-danger btn-xs js_user_status" type="button" data-type = "1" data-user-id = "{{$v['id']}}" data-status = "0">屏蔽</a>
                            @else
                                  <a href="javascript:void(0);" class="btn ink-reaction btn-success btn-xs js_user_status" type="button" data-type = "1" data-user-id = "{{$v['id']}}" data-status = "1">取消屏蔽</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
           @if($members->toArray()['data'])
            <div class="col-md-12 col-lg-12">
                <label style="color: black;font-size: 16px">当前页数量: {{$members->toArray()['to'] - $members->toArray()['from'] + 1}}</label>
                <label style="color: black;font-size: 16px">总数量: {{$members->toArray()['total']}}</label>
            </div>
            @endif
            <div class="text-center">
                {!! $members->render() !!}
            </div>
        </div><!-- ./card-body -->
    </div><!-- /.card -->
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript">

    $(".startDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
    $(".endDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });

     //屏蔽用户
    $('.js_user_status').live('click', function() {
        userStatus($(this));
    });

    // 屏蔽用户
    function userStatus(_this)
    {
        var status = _this.attr('data-status');
        $.ajax({
            url: '{!! action("Admin\\MemberController@updateUser") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-user-id'),
                status      :  _this.attr('data-status'),
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success('操作成功', '成功');
                    if(status == 1){
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-success');
                        _this.attr('data-status',0);
                        _this.text('屏蔽');
                    }else{
                        _this.addClass('btn-success');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',1);
                        _this.text('取消屏蔽');
                    }
                }else{
                     toastr.error('操作失败', '失败!')
                }
            }
        });
    }

</script>
@stop
