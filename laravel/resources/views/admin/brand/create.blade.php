@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2-bootstrap.css">
@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加品牌</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="fromProduct" enctype="multipart/form-data" method="POST">
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                       <input type="hidden" name="id"  value="{{$brand->id or ''}}">
                        <div class="tab-content clearfix">
                           <div class="row">
                                @include('admin.widget.input', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'name',
                                    'id'    => 'brandName',
                                    'title' => '品牌名称' ,
                                    'value' => isset($brand->name )? $brand->name : ''
                                ])

                                @include('admin.widget.input', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'hot',
                                    'id'    => 'hot',
                                    'title' => '品牌热度',
                                    'value' => isset($brand->hot) ? $brand->hot : ''
                                ])

                               
                                <div class="col-xs-12 col-md-12 col-lg-12" >
                                    <div class="form-group floating-label">
                                    <label>添加分类</label>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 20px">
                                @foreach($categories as $category)
                                    <div class="col-xs-2 col-md-2 col-lg-2 col-sm-2 " style="margin-top: 10px;margin-left: 50px;">
                                        <label class="checkbox-inline checkbox-styled">
                                            <input name="categories[]"  type="checkbox" value="{{$category->id}}"
                                            @if(isset($brandCategory))
                                                @if(in_array($category->id,$brand->brandCategory->pluck('category_id')->toArray()))
                                                    checked="checked"
                                                    class="js-danger-delete"
                                                @endif
                                            @endif
                                             ><span class="">{{$category->name}}</span>
                                        </label>

                                    </div><!--end .col -->
                                @endforeach
                               </div>

                            <div style="margin-left: 10px; width: 50%; float:left ;color: rgba(204, 204, 204, 0.94)" class="" class="col-xs-6 col-sm-6 col-md-6 col-lg-collg-6"  >
                            <label >品牌logo(750*470)</label>
                            </div>
                                @include('admin.widget.image-upload', [
                                    'colsm' => '6',
                                    'collg' => '6',
                                    'id'    => 'productImages',
                                    'label' => '',
                                    'style' => 'width:375px; height:235px;',
                                    'name'  => 'logo',
                                ])

                                @include('admin.widget.radio', [
                                'name' => 'recommend',
                                'id' => 'status',
                                'title' => '是否推荐',
                                'value' => isset($brand->recommend) ? $brand->recommend : 1])
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md">
                                提交
                                </button></li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop

@section('script')
<script type="text/javascript">
    $('#categories_select').select2();
    $("#productImages").change(function(){
        Helper.readURL(this);
    });

    @if(@isset($brand))
        Helper.setDefaultImg($('#uploadImg'),"{{$brand->logo}}");
        var url = "{!! action('Admin\\BrandController@edits') !!}"
        var msg = '编辑成功';
    @else
        Helper.setDefaultImg($('#uploadImg'));

        var url = "{!! action('Admin\\BrandController@store') !!}"
        var msg = '新增成功'
    @endif

    // wizard on tab next 提交表单
     $('#fromProduct').ajaxForm({
        url: url,
        type: 'POST',
        dataType: 'json',
        async:false,
        success: function (returnData) {
            if (returnData.status == 'success') {
                layer.msg(msg, {icon: 1});
                setTimeout(function(){
                    window.location.href = "{!! action('Admin\\BrandController@index') !!}"
                },1000)
            }else{
                layer.msg(returnData.message,{icon: 0});
            }
        }
    });

     $('.js-danger-delete').click(function(){
        var _this = $(this);
        layer.confirm('危险操作,可能会影响App首页分类商品跳转', {
                btn: ['确定','取消'],//按钮
        }, function(index){
            layer.close(index);
            if(_this.attr('checked') == 'checked'){
                 _this.removeAttr('checked');
             }else{
                _this.attr('checked','checked');
             }
           
        },function(index){
            layer.close(index); 
        });
        return false;

     })
</script>
@stop
