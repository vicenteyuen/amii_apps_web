@extends('admin.widget.body')

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">品牌列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>品牌名称</th>
                                    <th>品牌logo</th>
                                    <th>品牌热度</th>
                                    <th>是否推荐</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($brands as $brand)
                                    <tr>
                                        <td>{{ $brand->id }}</td>
                                        <td>{{ $brand->name }}</td>
                                        <td>
                                            @if (!empty($brand->logo))
                                            <img src="{{  $brand->logo }}" width="100px" height="50px" >
                                            @endif
                                        </td>
                                        <td>
                                            {{ $brand->hot }}
                                        </td>
                                        <td>{{ $brand->recommend == 1 ? '是' : '否' }}</td>
                                        <td>
                                            <a href="{{ action('Admin\BrandController@showEdit', ['id' => $brand->id])}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                                            <a href="{{ action('Admin\BrandController@productShow', ['id' => $brand->id])}}" class="btn ink-reaction btn-primary btn-xs" type="button">品牌商品</a>
                                            <a href="{{ action('Admin\BrandController@add', ['id' => $brand->id])}}" class="btn btn-sm ink-reaction btn-xs btn-primary"  type="button">添加品牌商品</a>
                                            <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-xs btn-danger js-delete" data-id="{{$brand->id}}" data-name="{{$brand->name}}" data-toggle="modal" data-target="#confirmModal">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                         @if($brands->toArray()['data'])
                        <div class="col-md-12 col-lg-12">
                            <label style="color: black;font-size: 16px">当前页数量: {{$brands->toArray()['to'] - $brands->toArray()['from'] + 1}}</label>
                            <label style="color: black;font-size: 16px">总数量: {{$brands->toArray()['total']}}</label>
                        </div>
                        @endif
                        <div class="text-center">
                            {!! $brands->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')

<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop

@section('script')
<script type="text/javascript">
      // 删除分类
    $('.js-delete').click(function(){
        var _this = $(this);
        layer.confirm('您确定删除 "'+_this.attr('data-name')+'" 这个品牌？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\BrandController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

</script>
@stop
