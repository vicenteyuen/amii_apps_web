
@extends('admin.widget.body')

@section('style_link')

<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">

@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加品牌商品</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class='col-md-12'>
                <div class="row">
                  <form action='{!! action("Admin\\BrandController@brandSearch") !!}' method="GET" class="form floating-label">
                     @include('admin.widget.input',[
                        'name' => 'name',
                        'id' => 'brandId',
                        'title' => '品牌名' ,
                        'value' => isset($brand) && $brand->name ? $brand->name : '',
                        'attribute' => 'disabled',
                        'dataValue' => $brand->id
                        ])
                    </form>
                </div>
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">搜索商品</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="card-body">
                                <form action='{!! action("Admin\\BrandController@brandSearch") !!}' method="GET" class="form floating-label">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入商品名称或产品编号">
                                        <input type="hidden" class="form-control" id="brand_id" name="brand_id" value="{{$brand->id}}" placeholder="请输入商品名称或产品编号">

                                        <div class="form-control-line"></div>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-floating-action btn-default-bright" type="submit" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                    </div>
                                </div>
                            </from>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>商品名称</th>
                                            <th>商品编号</th>
                                            <th>商品图片</th>
                                            <th style="width: 80px;">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody id="search_tbody">
                                    @if(isset($products))
                                        @foreach($products as $key => $product)
                                            <tr>
                                              <td class="number"><label class="checkbox-inline checkbox-styled"><input type="checkbox" class="checkbox" name="checkeds[]" value="{{ $product->id }}"/><span class=""></span></label> {{ $key+1 }}</td>
                                             <td class="productName">{{ $product->name }}</td>
                                             <td class="productSnumber">{{ $product->snumber }}</td>
                                             <td class="productMainImage"><img src="{{ $product->main_image }}" style="width: 50px;" /></td>
                                             <td><a class="btn btn-sm ink-reaction btn-info btn-add" data-id="{{ $product->id }}" >添加</a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- ./card-body -->
                            <div class="text-center">
                            @if(isset($products))
                                  {{$products->render()}}
                            @endif
                             </div>

                        </div>
                    </div>
                         <div class="row text-right btn-commit">
                            <div class="col-md-12">
                                <ul class="list-inline addShow">
                                    <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md btn-add-some">
                                     批量提交
                                    </button></li>

                                    <li class="next"><button type="submit" class="btn ink-reaction btn-info btn-md btn-add-all">
                                     全部提交
                                    </button></li>
                                </ul>
                            </div>
                        </div>
                </div><!-- ./col-lg-6 -->
            </div><!-- ./row -->
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).on('click','.btn-add',function(){
        addProduct($(this));
    });

    @if(isset($products) && !$products->isEmpty())
        $('.addShow').show();
    @else
        $('.addShow').hide();
    @endif

    //添加商品到分类
    function addProduct(_this){
        var product_id  = _this.attr('data-id');
        var brand_id =  $('#brandId').attr('data-value');
        var url = '{!! action("Admin\\BrandProductController@store") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'product_id':product_id,'brand_id':brand_id},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('添加成功', '成功!');
                _this.text('已添加');
                _this.css('border','none');
                _this.css('backgroundColor','#ccc');
            }else{
                 toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            toastr.error('添加商品到品牌失败.', '失败!')
        });
    }

    // 添加到搜索商品列表
    function loadProduct(product){
        var tbody = $('#search_tbody');
        tbody.empty();
        var html  = '';
        var len   = product.length;
        if (len > 0) {
            $('.addShow').show();
        }
        for (var i = 0 ; i < len; i++) {
            html += '<tr>'
                 +  '<td class="number"><label class="checkbox-inline checkbox-styled"><input type="checkbox" class="checkbox" name="checkeds[]" value="'+ product[i]['id'] +'"/><span class=""></span></label> ' + (i+1) + '</td>'
                 +  '<td class="productName">' + product[i]['name'] + '</td>'
                 +  '<td class="productSnumber">' + product[i]['snumber'] + '</td>'
                 +  '<td class="productMainImage"><img src="' + product[i]['main_image'] + '" style="width: 50px;" /></td>'
                 +  '<td><a class="btn btn-sm ink-reaction btn-info btn-add" data-id="'+product[i]['id'] +'" >添加</a></td>'
                 +  '</tr>';
        }

        tbody.append(html);
    }

    // 批量插入
    $('body').on('click','.btn-add-some',function(){
        var checkbox = $('#search_tbody').find('.checkbox:checked');
        var productIds = [];
        $.each(checkbox,function(){
            productIds.push($(this).val());
        })
        // 批量提交
        sendCategoryProduct(productIds);
    });

     // 插入全部
    $('body').on('click','.btn-add-all',function(){
        var product = $('#search_tbody').find('.btn-add');
        var productIds = [];
        $.each(product,function(){
            productIds.push($(this).attr('data-id'));
        })
        // 提交全部
        sendCategoryProduct(productIds);
    });


    // 提交全部或批量提交分类商品
    function sendCategoryProduct(productIds){
        if(productIds == ''){
            toastr.error('批量添加失败,当前未筛选出商品', '失败!')
            return false;
        }
        var brand_id =  $('#brandId').attr('data-value');
        var url = '{!! action("Admin\\BrandProductController@insertAll") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'productIds':productIds,'brand_id':brand_id},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('批量添加成功', '成功!');
                setTimeout(function(){
                     window.location.reload();
                 }, 1000);
            }else{
                 toastr.error('批量添加失败', '失败!')
            }
        }).fail(function(data) {
            toastr.error('添加商品到分类失败.', '失败!')
        });
    }

</script>
@stop
