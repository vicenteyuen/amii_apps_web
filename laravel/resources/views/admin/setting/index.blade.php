@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">系统参数设置</li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i> 社区模块</header>
                    </div>
                    <div class="card-body">
                        @foreach($setting as $community)
                        <dl class="dl-horizontal">
                            @if($community->key == 'subject')
                                @php
                                    $text = '专题状态'
                                @endphp
                            @elseif($community->key == 'video')
                                @php
                                    $text = '视频状态'
                                @endphp
                            @elseif($community->key == 'store')
                                @php
                                    $text = '实体店状态'
                                @endphp
                            @endif
                                <dt>{{$text}}</dt>
                                <dd>
                                    @if($community->value == 1)
                                        已开启
                                        @php
                                            $btn = '关闭'
                                        @endphp
                                    @else
                                        已关闭
                                        @php
                                            $btn = '开启'
                                        @endphp
                                    @endif
                                    <button style="margin-left: 20px" class="btn ink-reaction btn-primary text-left btn-xs js-setting" data-key="{{$community->key}}" data-value="{{$community->value ? 0 : 1}}">{{$btn}}</button>
                                </dd>
                        </dl>
                       @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    
    $(".js-setting").click(function(){
        var key = $(this).attr('data-key');
        var value = $(this).attr('data-value');
        $.ajax({
            url:"{{action('Admin\\SettingController@update')}}",
            type: 'POST',
            dataType: 'JSON',
            data:{'key' : key, 'value' : value},
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('操作成功', {icon: 1});
                    setTimeout(function(){
                        window.location.reload();
                    },1000)
                }else{
                    layer.msg('操作失败,请刷新重试',{icon: 0});
                }
            }
        });
    })
</script>
@stop
