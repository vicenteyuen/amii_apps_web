@extends('admin.widget.body')

@section('content')
    <section>
        <div class="section-header">
            <ol class="breadcrumb">
                <li class="active">商品服务列表</li>
            </ol>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                        <tr>
                            <th>名称</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($services as $service)
                            <tr>
                                <td>{{ $service->name }}</td>
                                <td>@if($service->status == 1)
                                    开启
                                    @else
                                    禁用
                                    @endif
                                </td>
                                <td>
                                     @if($service->status == 1)
                                       <a href="javascript:void(0);" class="btn ink-reaction btn-danger btn-xs js-status" data-id="{{$service->id}}" data-status="{{$service->status ? 0 :1}}" type="button">禁用</a>
                                     @else
                                          <a href="javascript:void(0);" class="btn ink-reaction btn-primary btn-xs js-status" data-id="{{$service->id}}" data-status="{{$service->status ? 0 :1}}" type="button">开启</a>
                                     @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop

@section('script')
    <script type="text/javascript">
    $('.js-status').click(function(){
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        $.ajax({
            url: "{{action('Admin\\ProductServiceController@update')}}",
            type: 'POST',
            dataType: 'JSON',
            data:{'id':id,'status':status},
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('操作成功', {icon: 1});
                    setTimeout(function(){
                        window.location.reload();
                    },1000)
                }else{
                    layer.msg('操作失败,请刷新重试',{icon: 0});
                }

            }
        });
    })
    </script>
@stop
