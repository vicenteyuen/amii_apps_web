@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/bootstrap-tagsinput.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">库存管理</li>
        </ol>
    </div>

    <div class="section-body">
        <div class="card">
            <div class="card-body">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <label>库存商品展示主图</label>
                    <div class="row">
                        @include('admin.widget.image-upload', [
                            'colsm' => '6',
                            'collg' => '4',
                            'id'    => 'productImages',
                            'name'  => 'main_image',
                            'descript' => '列表展示图(300x300)',
                            'style' => 'width:300px;height:300px;',
                        ])

                        @if(isset($edit) && $sellProduct->product->main_image)
                        <div class="col-xs-3 col-sm-3 col-md-3 imageTable" style="display: none;margin-top: -30px">
                            <label>商品主图</label>
                            <div>
                                <table class="table table-bordered no-margin table-striped table-hover Imagelist">
                                    <tr align="center">
                                    <th>商品主图</th>
                                    <th>操作</th>
                                    </tr>
                                          <tr align="center">
                                            <td><img src="{{$sellProduct->product->main_image}}" width="50px" height="50px"></td>
                                            <td>
                                            <a data-id ="{{$sellProduct->product->id}}" class="btn ink-reaction btn-success btn-sm changeProductImage">替换</a>
                                            <a data-id ="{{$sellProduct->product->id}}" class="btn ink-reaction btn-danger btn-sm deleteProductImage">删除</a>
                                            </td>
                                        </tr>
                                </table>
                            </div>
                        </div>
                        @endif

                        @if(isset($edit) && !$productImages->isEmpty())
                        <div class="col-xs-3 col-sm-3 col-md-3 imageTable" style="display: none;margin-top: -30px">
                              <label>商品图片</label>
                              <div>
                                  <table class="table table-bordered no-margin table-striped table-hover Imagelist">
                                      <tr align="center">
                                      <th>商品列表图</th>
                                      <th>操作</th>
                                      </tr>

                                      @foreach($productImages as $key=>$val)
                                          <tr align="center">
                                              <td><img src="{{$val->image}}" width="50px" height="50px"></td>
                                              <td>
                                              <a data-id ="{{$val->id}}" class="btn ink-reaction btn-success btn-sm changeImage">替换</a>
                                              <a data-id ="{{$val->id}}" class="btn ink-reaction btn-danger btn-sm deleteImage">删除</a>
                                              </td>
                                          </tr>
                                      @endforeach

                                  </table>
                              </div>
                        </div>
                         @endif
                         @include('admin.widget.image-select-button', [
                            'id' => 'idValue',
                            'text' => '从图片库中选择图片',

                        ])
                         @include('admin.widget.image-select-button', [
                            'id' => 'uploadInventoryImage',
                            'text' => '上传',
                            'marginTop' => '-36px',
                            'marginLeft' => '196px'
                        ])
                        @include('admin.widget.image-select')
                        @include('admin.widget.image-select-tmpl')
                        @include('admin.widget.input', ['name' => 'selectImage', 'id' => 'selectImage','type'=>'hidden'])
                         @include('admin.widget.input', ['name' => 'uploadType', 'id' => 'uploadType','type'=>'hidden'])
                    </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <form class="form floating-label" autocomplete="off">
                        @include('admin.widget.input', [
                            'colsm' => '8',
                            'collg' => '8',
                            'name' => 'sell_point',
                            'id' => 'sell_point',
                            'title' => '商品卖点',
                            'value' => isset($sellProduct->product->sell_point)?$sellProduct->product->sell_point:''
                        ])
                        @include('admin.widget.button', [
                            'id' => 'saveSellPoint',
                            'text' => '保存',
                            'colsm' => '4',
                            'collg' => '4',
                        ])
                    </form>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                     <form class="form floating-label" autocomplete="off">
                      @if($_GET['provider'] == 1)
                          @include('admin.widget.input',[
                                'name' => 'sell_product_price',
                                'id' => 'productPrice',
                                'title' => '商品基本价格(展示价格)',
                                'colsm' => '4',
                                'collg' => '4'
                          ])
                          @include('admin.widget.input',[
                                'name' => 'sell_product_mark_price',
                                'id' => 'productMarkPrice',
                                'title' => '商品基本市场价格(划去价格)',
                                'colsm' => '4',
                                'collg' => '4'
                          ])
                      @elseif($_GET['provider'] == 2)
                          @include('admin.widget.input',[
                                'name' => 'product_point',
                                'id' => 'productPoint',
                                'title' => '商品积分',
                                'colsm' => '4',
                                'collg' => '4'
                          ])
                          @include('admin.widget.input',[
                                'name' => 'sell_product_mark_price',
                                'id' => 'productMarkPrice',
                                'title' => '商品基本市场价格(划去价格)',
                                'colsm' => '4',
                                'collg' => '4'
                          ])
                      @endif
                      @include('admin.widget.button', [
                            'id' => 'baseProductPrice',
                            'text' => '保存',
                            'colsm' => '4',
                            'collg' => '4',
                        ])

                        </form>
                    </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 50px">
                    <div class="form-group">
                        <label class="col-sm-2 text-l control-label" style="margin-top: 10px">选择属性：</label>
                        <input type="hidden" id="product_id"  value="{{$_GET['product_id']}}">
                        <input type="hidden" id="sell_product_id" value="{{$sellPro->id}}">
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        <div class="col-sm-10">
                            <div class="row">
                                 <div class="col-sm-2 ">
                                    <select id="attribute-select" name="attribute-select" class="static dirty pull-left form-control">
                                        <?php echo \App\Helpers\AdminViewHelper::objectSelectHtml($attributes, 0);  ?>
                                    </select>
                                </div>
                                <button class="col-sm-2 btn ink-reaction btn-success" type="button" id="attr_add_button">添加属性</button>
                                <div class="form-inline col-sm-8"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-2 text-l control-label" style="margin-top: 30px">属性值：</label>
                        <div class="col-sm-10" id="attr_value_div" style="margin-top: 26px">
                        </div>
                    </div>
              </div>


              <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 20px">
                <button class=" btn ink-reaction btn-success btn-xs addSellAttributeValue" type="button">生成属性值表</button>
              </div>

              <div  class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 20px">
                  <div class="form-group">
                      <label class="col-sm-2 text-l control-label" style="margin-bottom: 10px" >属性值图：</label>
                      <div id="sell-attribute-table" style="margin-top: 10px">
                            @if(isset($edit) && !empty($edit))
                                <table class="table table-bordered no-margin table-striped table-hover"><tr><th>属性值名称</th><th>图片</th><th>操作</th></tr>
                                @foreach($sellAttributeValue as $value)
                                    <tr>
                                        <td>{{$value->attributeValue->value}}</td>
                                        <td>
                                        <div class="thumbnail upload-display" style="width:80px;height:80px;margin: auto;" >
                                            <input id="images_{{$value->id}}" type="file">
                                            <label for="images_{{$value->id}}"></label>
                                            <img id="attributeImage_{{$value->id}}" data-src="holder.js/100%x180" src="{{$value->sell_product_attribute_value_image}}" alt="error img"/>
                                        </div>
                                        </td>
                                        <td>
                                            <button class="btn ink-reaction btn-success attribute-value-image-save-button" type="button" value_id="{{$value->id}}">保存</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                            @endif
                      </div>
                  </div>
              </div>

               <div  class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 20px">
                  <div class="form-group">
                      <label class="col-sm-2 text-l control-label" style="margin-bottom: 10px" >库存属性：</label>
                      <div id="inventory-table" style="margin-top: 10px">
                      </div>
                  </div>
              </div>

              <div class="row text-right">
                <div class="col-xs-12 col-sm-12 col-md-12">
                     <button class=" btn ink-reaction btn-success btn-md addInventory" type="button">生成库存</button>
                </div>
              </div>
                <!-- 游戏商品 -->
                @if($_GET['provider'] != 3)
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                         <button class=" btn ink-reaction btn-success btn-md" type="button" id="pushProduct"  data-status="1" @if($sellStatus == 1) disabled @endif>上架</button>
                         <button class=" btn ink-reaction btn-danger btn-md" type="button" id="popProduct" data-status='0'
                         @if($sellStatus == 0) disabled @endif>下架</button>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div><!--end ./card-body -->
</section>
@stop
@section('script')

<!-- 属性模板 -->
<script type="text/x-tmpl" id="tmpl-attr-value">
<div class="row rowAttribute">
    <label class="col-sm-1" style="font-size:150%;">{%=o.attribute.name%}</label>
    <div class="col-sm-2 ">
        <select id="attributeValue-select" name="attributeValue-select" class="static dirty pull-left form-control js-select-attribute" data-attribute-id ={%=o.attribute_id%} data-id ={%=o.id%}>
             {% for (var i=0; i<o.attribute.attribute_value.length; i++) { %}
                <option id="{%=o.attribute.attribute_value[i].id%}">{%=o.attribute.attribute_value[i].value%}</option>
             {% } %}
        </select>
    </div>
    <button class="col-sm-1 btn ink-reaction btn-success value-add-button" type="button" style="margin-top:0px;">
        添加值
    </button>
    <button class="col-sm-1 btn ink-reaction btn-danger attribute-del-button" type="button" style="margin:0px 0 0 4px;">
        删除
    </button>
     <div class="form-inline col-sm-6 attribute-values">
     </div>
</div>
</script>

<!-- 属性值模板 -->
<script type="text/x-tmpl" id="tmpl-attr-values">
    <div class="bootstrap-tagsinput" style="border-bottom:none;">
      {% for (var i=0; i<o.length; i++) { %}
      <span class="tag label label-info editable-click" id=""
       style="display: inline-block;
              text-align: center;height: 26px;font-size: 16px;padding-left: 0px;margin: 5px">
          <a value="{%=o[i]['id']%}" class="'value-label'" data-type="text">{%=o[i]['attribute_value']['value']%}</a>
          @if(empty($edit))<span class="tag-close" idvalue="{%=o[i]['id']%}" data-role="remove"></span>@endif
      </span>
    {% } %}
    </div>
</script>


<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/tmpl/tmpl.min.js" type="text/javascript"></script>

 <!-- 从图片库中选择 -->
<script type="text/javascript">
     var page = 1;
     var object ={};
     var provider = 'product';
    $('#idValue').on('click', function () {
        $.ajax({
            url: "{!! action('Admin\\ImageController@pageImage') !!}",
            type: 'GET',
            data:{page:page, provider:provider}
        })
        .done(function (data) {
            object.data = data;
            $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
                    Helper.modalToggle($('#modal-image-select'));
            });
     });

    // 上一页
     $('body').on('click','#prePage',function() {
        if(page >=1)--page;
        $.ajax({
            url: "{!! action('Admin\\ImageController@pageImage') !!}",
            type: 'GET',
            data:{page:page, provider:provider}
        })
        .done(function (data) {
            object.data = data;
            if(data.length == 0){
                layer.msg('已是首页',{icon: 0});
                page++;
                return false;
            }
            $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
        });
    })

    // 下一页
        $('body').on('click','#nextPage',function() {
        $.ajax({
            url: "{!! action('Admin\\ImageController@pageImage') !!}",
            type: 'GET',
            data:{page:++page, provider:provider}
        })
        .done(function (data) {
            object.data = data;
            if(data.length ==0){
                --page;
                layer.msg('已是最后一页',{icon: 0});
                return false;
            }
            $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
            // Helper.modalToggle($('#modal-image-select'));
        });
    })
</script>

<script type="text/javascript">
    $('#modal-image-select').on('click', '#imageSelectSubmit', function() {
        if(imageSelected !== undefined){
            var start = imageSelected.lastIndexOf('/')+1;
            var imageName = imageSelected.substring(start);
            $('#selectImage').val(imageName);
            Helper.setDefaultImg($('#uploadImg'),imageSelected);
        }
        //图片库
        $('#uploadType').val('imagesFolder');
        Helper.modalToggle($('#modal-image-select'));
    });
</script>

<script type="text/javascript">

    //STATUS 值不为-1当前页面为编辑状态
    var STATUS = {{ $edit or '-1'}};
    var ADD_STATUS = -1;
    var EDIT_STATUS = 1;
    var uploadUrl = "{!! action('Admin\\ProductImageController@store') !!}";
    var _token = $('#_token').val();

    // 点击添加属性按钮,触发添加值按钮，只加载属性值，不添加属性值
    var flag = true;
    Helper.setDefaultImg($('#uploadImg'));
    // 商品展示图(主图)
    $("#productImages").change(function(){
        $('#uploadType').val('inputFile');
        Helper.readURL(this);
    });

    //编辑显示主图
    @if(!empty($sellProduct->image))
         Helper.setDefaultImg($('#uploadImg'),"{{$sellProduct->image}}");
    @endif

    //上传主图
    $('#uploadInventoryImage').live('click',function(){
        uploadImage();
    });

     //添加属性
    $('#attr_add_button').live('click', function() {
        addAttribute();
    });

    // 添加属性值
    $('.value-add-button').live('click', function() {
        loadAttributeValues($(this));

        // 判断是否有属性值内容
        var attribute_value = $(this).next().next().html();
        if (attribute_value == '') {
            $(this).next().hide();
        } else {
           $(this).next().show();
        }
    });

    //保存无属性库存
    $('#inventory-single-save-button').live('click',function(){
        saveInventory('0_0',$(this));
    });

    //保存无属性库存
    $('.attribute-value-image-save-button').live('click',function(){
        var valueId = $(this).attr('value_id');
        saveAttributeValueImage(valueId,$(this));
    });

    //保存商品卖点
    $('#saveSellPoint').live('click', function() {
        saveSellPoint($(this));
    });

    //保存基本价格
    $('#baseProductPrice').live('click', function() {
        savePrice($(this));
    });

    // 删除属性值
    $('.tag-close').live('click', function() {
        var val = $(this).attr('idvalue');
        var _this = $(this).parent().parent().parent();
        deleteValue(val,_this);
    });

    // 删除product_image
    $('.deleteImage').live('click',function(){
        removeImage($(this));
    });

    // 删除product main_image
    $('.deleteProductImage').live('click',function(){
        removeProductImage($(this));
    });

    // 生成库存
    $('.addInventory').live('click',function(){
        loadInventory();
    });

    //生成属性值图
    $('.addSellAttributeValue').live('click',function(){
        loadSellAttributeValue();
    });

    // 更换商品图片
    $('.changeImage').live('click',function(){
      changeImageValue($(this));
    });

    $('.changeProductImage').live('click',function(){
      changeProductImageValue($(this));
    });


    //商品上架
    $('#pushProduct').live('click', function() {
        productStatus($(this));
    });

    //商品下架
    $('#popProduct').live('click', function() {
        productStatus($(this));
    });

    @if($_GET['provider'] == 2)
        var strPrice = '积分';
    @else
        var strPrice = '市场价';
    @endif


    //上传主图
    function uploadImage(){
        var formData = new FormData();
        formData.append('_token',_token);
        formData.append('id',"{{$sellPro->id}}");
        formData.append('main_image', $('#productImages')[0].files[0]);
        formData.append('selectImage', $('#selectImage').val());
        formData.append('uploadType',  $('#uploadType').val());
        $.ajax({
            type: 'POST',
            url: "{!! action('Admin\\SellProductController@upload') !!}",
            processData: false,
            contentType: false,
            cache: false,
            async:false,
            data: formData
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('上传图片成功!', '成功')
            }else{
                toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            toastr.error('上传图片失败.', '失败!')
        });
    }
    // 保存商品卖点
    function saveSellPoint(_this)
    {
        var url = '{!! action("Admin\\SellProductController@saveSellPoint") !!}';
        var successMsg = '商品卖点保存成功!';
        var errorMsg = '商品卖点保存失败.';
        var saveData = {'id':"{{$sellPro->id}}",'sell_point':$('#sell_point').val(),'_token':_token};
        $.ajax({
            type: 'POST',
            url: url,
            data: saveData,
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success(successMsg, '成功')
            }else{
                if(STATUS == ADD_STATUS){
                     toastr.error(data.message, '失败!')
                }
            }
        }).fail(function(data) {
            toastr.error(errorMsg, '失败!')
        });
    }

    // 保存基本价格
    function savePrice(_this)
    {
        var url = '{!! action("Admin\\SellProductController@savePrice") !!}';
         @if($_GET['provider'] == 1)
            var successMsg = '基本价格保存成功!';
            var errorMsg = '基本价格保存失败.';
            var saveData = {'id':"{{$sellPro->id}}",'base_price':$('#productPrice').val(),'_token':_token,'base_mark_price':$('#productMarkPrice').val(),'provider':'1'};

         @elseif($_GET['provider'] == 2)
            var successMsg = '商品积分保存成功!';
            var errorMsg = '商品积分保存失败.';
            var saveData = {'id':"{{$sellPro->id}}",'_token':_token,'product_point':$('#productPoint').val(),'base_mark_price':$('#productMarkPrice').val(),'provider':'2'};
         @else
            toastr.error('保存失败，请刷新重试', '失败!')
            return false;
         @endif
        $.ajax({
            type: 'POST',
            url: url,
            data: saveData,
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success(successMsg, '成功')
            }else{
                if(STATUS == ADD_STATUS){
                     toastr.error(data.message, '失败!')
                }
            }
        }).fail(function(data) {
            toastr.error(errorMsg, '失败!')
        });
    }

    //没有库存，但有属性或属性值的判断情况，编辑库存时加载
    @if (isset($hasSellAttr) && $hasSellAttr == 1 )
         loadAttributeValue();
    @endif

    // 添加属性
    function addAttribute(){
        if(STATUS == EDIT_STATUS){
             loadAttributeValue();
             return false;
        }
        var attribute_id  = $('#attribute-select').val();
        var sell_id = $('#sell_product_id').val();
        var url = '{!! action("Admin\\SellAttributeController@store") !!}';
        $.ajax({
            type: 'POST',
            url: url,
            async:false,
            data: {'product_sell_attribute_id':sell_id,'attribute_id':attribute_id,'_token':_token},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('属性添加成功!', '成功')
            }else{
                if(STATUS == ADD_STATUS){
                     toastr.error(data.message, '失败!')
                }
            }
            loadAttributeValue();
        }).fail(function(data) {
            toastr.error('属性添加失败.', '失败!')
        });
    }


    // 加载属性的所有属性值(select option)
     function loadAttributeValue(){
        var sell_id = $('#sell_product_id').val();
        var url = '{!! action("Admin\\SellAttributeController@index") !!}';
        $.ajax({
          type: 'GET',
          url: url,
          async:false,
          data: {'_token':_token,'id':sell_id},
        }).done(function(data) {
            if (data.status=='success') {
                updateAttributeForm(data.data);
                clickAttributeLoadValues();
            }
            // else {
            //     if(STATUS == ADD_STATUS){
            //         toastr.error(data.message, '失败!')
            //     }
            // }
        }).fail(function(data) {
            toastr.error('属性获取失败.', '失败!')
        });

    }

    //获取商品的属性值
    function loadValue(_this){
        var sell_attribute_id = _this.parent().find('.js-select-attribute').attr('data-id');
        var attribute_id = _this.parent().find('.js-select-attribute').attr('data-attribute-id');
        var url = '{!! action("Admin\\SellAttributeValueController@index") !!}';
        $.ajax({
            type: 'GET',
            url: url,
            data: {'sell_attribute_id':sell_attribute_id,'attribute_id':attribute_id}
        }).done(function(data) {
            _this.parent().find('.attribute-values').empty();
            if(data.status=='success'){
                if ( _this.parent().find('.attribute-del-button')) {
                    _this.parent().find('.attribute-del-button').remove();
                }
                _this.parent().find('.attribute-values').append(tmpl("tmpl-attr-values", data.data));
                // toastr.success('属性值获取成功!', '成功')
            }else{
                // toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            // toastr.error('属性值获取失败.', '失败!')
        });
    }

    // 点击添加属性时加载各属性的属性值(模拟添加值按钮)
    function clickAttributeLoadValues()
    {
        var attributeBtn = $('.value-add-button');
        flag = false;
        $.each(attributeBtn,function(key,value){
            $(this).click();
        });
        flag = true;
    }

    // 添加属性值
    function loadAttributeValues(_this){
        var attribute_value_id = _this.parent().find('.js-select-attribute option:selected').attr('id');
        var sell_attribute_id  = _this.parent().find('.js-select-attribute').attr('data-id');
        var attribute_id       = _this.parent().find('.js-select-attribute').attr('data-attribute-id');
        var url = '{!! action("Admin\\SellAttributeValueController@store") !!}';
        if(!flag){
            loadValue(_this);
            return false;
        }
        $.ajax({
            type: 'POST',
            url: url,
            data: {'_token':_token,'attribute_value_id':attribute_value_id,'sell_attribute_id':sell_attribute_id,'attribute_id':attribute_id}
        }).done(function(data) {
            loadValue(_this);
            if(data.status == 'success'){
                toastr.success('属性值添加成功!', '成功');

            }else{
                toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            toastr.error('属性值添加失败.', '失败!')
        });
    };

    // 删除属性
    var deleteAttribute = $('.attribute-del-button');
    deleteAttribute.live('click', function(){

        var _this = $(this).parent();
        var sell_attribute_id = $(this).parent().find('.js-select-attribute').attr('data-id');
        var attribute_id = $(this).parent().find('.js-select-attribute').attr('data-attribute-id');

        $.ajax({
            type: 'post',
            url: '{!! action("Admin\\SellAttributeValueController@delete") !!}',
            data: {'_token':_token, sell_attribute_id:sell_attribute_id, },
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('属性删除成功!', '成功')
                // attribute ==1 ,该属性没有属性值
                if(data.data.attribute == 1){
                    _this.remove();
                }
            }else{
                toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            toastr.error('属性删除失败.', '失败!')
        });
    });


    // 删除属性值
    function deleteValue(valueId,_this){
        $.ajax({
            type: 'post',
            url: '{!! action("Admin\\SellAttributeValueController@delete") !!}',
            data: {'_token':_token,id:valueId},
        }).done(function(data) {
            if(data.status=='success'){
                loadValue(_this);
                toastr.success('属性值删除成功!', '成功')
                // attribute ==1 ,该属性没有属性值
                if(data.data.attribute == 1){
                    _this.parent().remove();
                }
            }else{
                toastr.error(data.message, '失败!')
            }
        }).fail(function(data) {
            toastr.error('属性值删除失败.', '失败!')
        });
    }

    // 加载属性模板(拼接javascript字符串)
    function updateAttributeForm(attributes){
        $('#attr_value_div').empty();
        for (var i = 0, len = attributes.length; i < len; i++) {
            $('#attr_value_div').append(tmpl("tmpl-attr-value", attributes[i]));
        }
    }

    //获取生成库存的数据
    function loadInventory(){
        var sell_product_id = {{ $sellPro->id}} ;
        $.ajax({
            type: 'GET',
            url: '{!! action("Admin\\SellAttributeValueController@makeInventory") !!}',
            data: {id:sell_product_id},
            async: false,
        }).done(function(data) {
            if(data.status=='success'){
              var res =  updateAttrValues(data.data.create);
              // 缺少属性或属性值，提示
              if (res == false) {
                toastr.error('缺少属性或属性值，请添加！', '失败!')
              }
                // 填充库存数据
                updateInventory(data.data.inventory);
            }else{
                // 没有属性性的库存
                makeTabel();
                if(STATUS ==ADD_STATUS){
                    toastr.success('生成没有属性的库存,若要添加属性,请先添加属性再生成库存', '成功!');
                }else{
                    getInventoryNumber();
                }
            }
        }).fail(function(data) {
            toastr.error('生成库存失败.', '失败!')
        });
    }

    // 生成没有属性的库存
    function makeTabel(){
        $('#inventory-table').empty();
        var elementId = '0_0'
        var tableHtml = '';
        tableHtml +=  '<table class="table table-bordered no-margin table-striped table-hover" >'
                     +'<tr><th>#</th><th>'+strPrice+'</th></th><th>库存</th><th>库存警告阀值</th><th>商品编号</th><th>操作</th></tr>'
                     +'<tr align="center" class="addValueData" data-key="0_0">'
                     +'<td>1</td>'
                     +'<td><input id="markPrice_'+elementId+'" type="text"></td>'
                     +'<td><input id="number_'+elementId+'" type="text"></td>'
                     +'<td><input id="warnNumber_'+elementId+'" type="text"></td>'
                     +'<td><input id="skuCode_'+elementId+'" type="text"></td>'
                     +'<td><button class="btn ink-reaction btn-success" type="button" id="inventory-single-save-button" >保存</button></td>'
                     +'</tr>';
                     +'</table>';

        $('#inventory-table').append(tableHtml);
        // 编辑获取无属性的库存
    }

    //获取生成属性值数据的数据
    function loadSellAttributeValue(){
        var sell_product_id = {{ $sellPro->id}} ;
        $.ajax({
            type: 'GET',
            url: '{!! action("Admin\\SellAttributeValueController@makeSellAttributeValue") !!}',
            data: {id:sell_product_id},
        }).done(function(data) {
            if(data.status=='success'){
                // 填充库存数据
                loadAttributeValueImage(data.data);
            }
        }).fail(function(data) {
            toastr.error('生成属性值表失败.', '失败!')
        });
    }

    //生成属性值表格
    function loadAttributeValueImage(data){
        $('#sell-attribute-table').empty();
        var imageArr = [],image = [];
        var tableHtml = '<table class="table table-bordered no-margin table-striped table-hover"><tr><th>属性值名称</th><th>图片</th><th>操作</th></tr>';
        console.log(data);
        for (var i = 0; i < data.length; i++) {
             for (var j =  0; j < data[i]['sell_attribute_value'].length; j++) {
                tableHtml +=   "<tr>"
                             + "<td>" + data[i]['sell_attribute_value'][j]['attribute_value']['value'] + "</td>"
                             + '<td><div class="thumbnail upload-display" style="width:80px;height:80px;margin: auto;" ><input id="images_'+data[i]['sell_attribute_value'][j]['id']+'" type="file">'+'<label for="images_'+data[i]['sell_attribute_value'][j]['id']+'" ></label>'+
                             '<img id="attributeImage_'+data[i]['sell_attribute_value'][j]['id']+'" data-src="holder.js/100%x180" src="" alt="error img"/></div></td>'
                             +'<td><button class="btn ink-reaction btn-success attribute-value-image-save-button" type="button" value_id="'+data[i]['sell_attribute_value'][j]['id']+'">保存</button></td>'
                            +'</tr>';
                 imageArr.push("#attributeImage_"+data[i]['sell_attribute_value'][j]['id']);
                 image.push("#images_"+data[i]['sell_attribute_value'][j]['id']);

            }
        }
        tableHtml += "</table>";
        $('#sell-attribute-table').append(tableHtml);
        setDefaultImage(imageArr,image);
    }


    // 编辑获取无属性的库存
    function getInventoryNumber(){
         $.ajax({
            type: 'GET',
            url: "{{ action('Admin\\InventoryController@index')}}",
            async:false,
            data: {'_token':_token,'id':"{!! $sellPro->id !!}",'product_id':"{!! $_GET['product_id'] !!}"}
        }).done(function(data) {
            if(data.status == 'success'){
                var elementId = '0_0';
                $("#markPrice_"+elementId).val(data.data.mark_price);
                $("#number_"+elementId).val(data.data.number);
                $("#warnNumber_"+elementId).val(data.data.warn_number);
                $("#skuCode_"+elementId).val(data.data.sku_code);
            }
        }).fail(function(data) {
        });
    }

    // 生成库存属性表
    function updateAttrValues(attributes){
        var valueArr = [];
        var lines = 1 ;
        // 库存没有属性或有属性没有属性值
        for (var i = 0; i < attributes.length; i++) {
            // console.log(attributes[i].attribute.attribute_value.length);
            // console.log('存有属性没有属性值或没有属性');
            if (attributes[i].attribute.attribute_value) {
                lines = lines * attributes[i].attribute.attribute_value.length;
                valueArr.push(attributes[i].attribute.attribute_value.length);
            }
        }

        $('#inventory-table').empty();

        var htmlBuffer = [];
        htmlBuffer.push('<table class="table table-bordered no-margin table-striped table-hover">');
        htmlBuffer.push('<tr> ');
        htmlBuffer.push('<th>#</th>');
        for (var i = 0; i < attributes.length; i++) {
             htmlBuffer.push('<th>' + attributes[i].attribute.name + '</th>');
        }
        htmlBuffer.push('<th>图片</th><th>'+strPrice+'</th><th>库存</th><th>库存警告阀值</th><th>商品编号</th><th>操作</th> </tr>');
        //  var tableHtml= htmlBuffer.join('\n');
        // $('#inventory-table').append(tableHtml);

        var indexArr =[];
        var linenum = 0;
        var elementId = "";
        var imageArr = []; //存图片不同属性值图片id数组
        var image = [];

        for (var j = 0; j < lines; j++) {
            indexArr = getAttrIndexArr(valueArr,j)

            var valueIds = [];
            for (var i = 0; i < attributes.length; i++) {
                if (attributes[i].attribute && attributes[i].attribute.attribute_value[indexArr[i]]) {
                    valueIds.push (parseInt(attributes[i].attribute.attribute_value[indexArr[i]].id));
                }
            }
            valueIds.sort(sortNumber);
            // console.log(valueIds);

            elementId = valueIds.join('_');

            linenum = j + 1;
            htmlBuffer.push('<tr class="addValueData" data-key="'+elementId+'">');
            htmlBuffer.push('<td>'+linenum+'</td>');
            for (var i = 0; i < attributes.length; i++) {
                htmlBuffer.push(' <td>'+attributes[i].attribute.attribute_value[indexArr[i]].value+'</td>');
            }

            htmlBuffer.push('<td><div class="thumbnail upload-display" style="width:80px;height:80px; margin: auto;" ><input id="image_'+elementId+'" type="file">'+'<label for="image_'+elementId+'" ></label>'+
                '<img id="uploadImage_'+elementId+'" data-src="holder.js/100%x180" src="" alt="error img"/></div></td>'); //图片

            htmlBuffer.push('<td><input id="markPrice_'+elementId+'" type="text"></td>');
            htmlBuffer.push('<td><input id="number_'+elementId+'" type="text"></td>');
            htmlBuffer.push('<td><input id="warnNumber_'+elementId+'" type="text"></td>');

            htmlBuffer.push('<td><input id="skuCode_'+elementId+'" type="text"></td>');

            htmlBuffer.push('<td><button class="btn ink-reaction btn-success inventory-save-button" type="button" id="inventory-save-button" style="margin-top:0px;" value_ids="'+elementId+'">保存</button></td>');
            htmlBuffer.push('</tr>');
            imageArr.push("#uploadImage_"+elementId);
            image.push("#image_"+elementId);

        }

        htmlBuffer.push('</table>');
        var tableHtml= htmlBuffer.join('\n');
        $('#inventory-table').append(tableHtml);
        setDefaultImage(imageArr,image);

        // 存有属性没有属性值或没有属性,返回提示
        if (lines == 0) {
            return false;
        }
    }


    //设置库存图片
    // arr 设置默认图片 image 设置上传的图片
    function setDefaultImage(arr,image,elseParam = [])
    {
        var len = arr.length;
        for(var i=0;i<len;i++){
            if(!elseParam[i]){
                Helper.setDefaultImg($(arr[i]));
            }
           (function(i){
                $(image[i]).on('change', function() {
                   Helper.readURL(this,arr[i]);
                })
            })(i);
        }
    }

     // 保存库存
     $('#inventory-save-button').live( "click", function() {
        var valueIds = $(this).attr('value_ids');
        saveInventory(valueIds,$(this));
    });

    //保存库存
    function saveInventory(valueIds,_this){
        if(STATUS != ADD_STATUS){
            url = '{!! action("Admin\\InventoryController@edits") !!}';
        }else{
            url = '{!! action("Admin\\InventoryController@store") !!}';
        }
        var formData = new FormData();

        var productId = {{$_GET['product_id']}};
        var productSellId = {{$sellPro->id}};
        var markPrice = $('#markPrice_'+valueIds).val();
        var number    = $('#number_'+valueIds).val();
        var warnNumber= $('#warnNumber_'+valueIds).val();
        var skuCode   = $('#skuCode_'+valueIds).val();

        formData.append('_token',_token);
        formData.append('product_id',"{{$_GET['product_id']}}");
        formData.append('product_sell_id', "{{$sellPro->id}}");
        formData.append('mark_price', markPrice);
        formData.append('number',number);
        formData.append('warn_number', warnNumber);
        formData.append('sku_code', skuCode);
        formData.append('value_key', valueIds);
        if($('#image_'+valueIds).length>0){
             if(typeof $('#image_'+valueIds)[0].files[0] != 'undefined'){
                formData.append('image', $('#image_'+valueIds)[0].files[0]);
             }
        }
        $.ajax({
            type: 'POST',
            url: url,
            processData: false,
            contentType: false,
            cache: false,
            async:false,
            data: formData
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('库存保存成功!', '成功');
                  //禁用添加属性，添加属性值按钮,删除属性
                $('#attr_add_button').attr('disabled','disabled');
                $('.addInventory').attr('disabled','disabled');
                $('.value-add-button').attr('disabled','disabled');
                $('.tag-close').remove();
            }else{
                var msg =  JSON.parse(data.message);
                $.each(msg, function(i) {
                    var info = msg[i];
                    for (var i = 0; i < info.length; i++) {
                        layer.msg(info[i],{icon: 0});
                    }
                });
            }
        }).fail(function(data) {
            toastr.error('库存保存失败.', '失败!')
        });
    }

    //保存库存
    function saveAttributeValueImage(valueIds){
        url = '{!! action("Admin\\SellAttributeValueController@updateImage") !!}';
        var formData = new FormData();

        formData.append('_token',_token);
        formData.append('id',valueIds);


        if($('#attributeImage_'+valueIds).length>0){
             if(typeof $('#images_'+valueIds)[0].files[0] != 'undefined'){
                formData.append('sell_product_attribute_value_image', $('#images_'+valueIds)[0].files[0]);
             }
        }
        $.ajax({
            type: 'POST',
            url: url,
            processData: false,
            contentType: false,
            cache: false,
            async:false,
            data: formData
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('属性值图片保存成功!', '成功');
            }else{
                $.each(JSON.parse(data.message), function(key, item){
                    toastr.error(item, '失败!');
                })
            }
        }).fail(function(data) {
            toastr.error('属性值图片保存失败.', '失败!')
        });
    }

    function getAttrIndexArr(valueArr,total){
        // check overflow
        var max = 1;
        // get array
        var indexArr = [];
        var current = total;
        for (var i = 0; i < valueArr.length; i++) {
            var size = 1;
            for (var j = i+1; j < valueArr.length; j++) {
                size = size * valueArr[j];
            }
            indexArr[i] = parseInt( current / size) ;
            current = current - indexArr[i] * size;

            // console.log(indexArr[i]);
        }
        return indexArr;
    }

    function sortNumber(a,b) {
        return a - b;
    }

    // 编辑库存
    if(STATUS == EDIT_STATUS){
         $('#attr_add_button').click();
         $('.addInventory').click();
         $('#productPrice').val({{$sellProduct->base_price or ''}});
         $('#productMarkPrice').val({{$sellProduct->market_price or ''}});
         $('#productPoint').val({{$sellProduct->product_point or ''}});
         $('#attr_add_button').attr('disabled','disabled');
         $('.addInventory').attr('disabled','disabled');
         $('.value-add-button').attr('disabled','disabled');
         $('.addSellAttributeValue').attr('disabled','disabled'); //生成属性值表按钮为不可点击
         @if(!empty($productImages) && !$productImages->isEmpty())
             $('.imageTable').css('display','block');
         @endif

         var imageArr = [],image = [],elseParam = [];
         // 关联上传图片显示
         @if(isset($sellAttributeValue))
         @foreach($sellAttributeValue as $value)
            @if(!$value->sell_product_attribute_value_image)
                elseParam.push(0);
            @else
                elseParam.push(1);
            @endif
            imageArr.push("#attributeImage_"+{{$value->id}});
            image.push("#images_"+{{$value->id}});
         @endforeach
        setDefaultImage(imageArr,image,elseParam);
        @endif
    }

    // 删除图片
    function removeImage(_this){
        $.ajax({
            url: '{!! action("Admin\\ProductImageController@delete") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-id'),
                _token      :  _token
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success('商品图片删除成功!', '成功');
                    _this.parent().parent().remove();
                }else{
                    toastr.error('商品图片删除失败!', '失败');
                }
            }
        });
    }

     // 删除图片
    function removeProductImage(_this){
        $.ajax({
            url: '{!! action("Admin\\ProductController@deleteMainImage") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-id'),
                _token      :  _token
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success('商品图片删除成功!', '成功');
                    _this.parent().parent().remove();
                }else{
                    toastr.error('商品图片删除失败!', '失败');
                }
            }
        });
    }

    // 填充库存数据
    function updateInventory(inventories){

        var inventory;
        var elementId = "";
        for (var i = 0; i < inventories.length; i++) {
            inventory = inventories[i];
            elementId = inventory.value_key;
            $("#markPrice_"+elementId).val(inventory.mark_price);
            $("#number_"+elementId).val(inventory.number);
            $("#warnNumber_"+elementId).val(inventory.warn_number);
            $("#skuCode_"+elementId).val(inventory.sku_code);
            Helper.setDefaultImg($('#uploadImage_'+elementId),inventory.image);
        }
    }

        //上下架商品
    function productStatus(_this)
    {
        var status = _this.attr('data-status');
        if(status == 1){
            var success = '上架成功';
            var error   = '上架失败';
        }else{
            var success = '下架成功';
            var error   = '下架失败';
        }
        $.ajax({
            url: '{!! action("Admin\\SellProductController@status") !!}',
            type: 'POST',
            data: {
                 id         :  "{{$sellPro->id}}",
                _token      :  _token,
                status      :  status,
                provider    :  {{$_GET['provider']}}
            },
            dataType: 'JSON',
            success: function (returnData) {
                console.log(returnData);
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');
                    if(status == 1){
                        $('#pushProduct').attr('disabled','disabled');
                        $('#popProduct').removeAttr('disabled');
                    }else{
                        $('#popProduct').attr('disabled','disabled');
                        $('#pushProduct').removeAttr('disabled');
                    }
                }else{
                     toastr.error(returnData.message, '失败!')
                }
            }
        });

    }

    // 更换商品图片
    function changeImageValue(that)
    {
      var productImage = that.parent().parent().find('img').attr('src');
      var sellProductImage = $('#uploadImg').attr('src');
      var productImageId = that.attr('data-id');
      console.log(productImage);
      console.log(sellProductImage);
      $.ajax({
            url: '{!! action("Admin\\ProductImageController@change") !!}',
            type: 'POST',
            data: {
                 sell_product_id     :  "{{$sellPro->id}}",
                product_image_id     :  that.attr('data-id')
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    that.parent().parent().find('img').attr('src',sellProductImage);
                    $('#uploadImg').attr('src',productImage);
                    toastr.success('替换成功', '成功');
                }else{
                     toastr.error('操作失败,请刷新重试', '失败!')
                }
            }
        });
    }

    function changeProductImageValue(that)
    {
      var productImage = that.parent().parent().find('img').attr('src');
      var sellProductImage = $('#uploadImg').attr('src');
      var productImageId = that.attr('data-id');
      $.ajax({
            url: '{!! action("Admin\\ProductController@change") !!}',
            type: 'POST',
            data: {
                 sell_product_id     :  "{{$sellPro->id}}",
                product_id     :  that.attr('data-id')
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    that.parent().parent().find('img').attr('src',sellProductImage);
                    $('#uploadImg').attr('src',productImage);
                    toastr.success('替换成功', '成功');
                }else{
                     toastr.error('操作失败,请刷新重试', '失败!')
                }
            }
        });
    }
</script>
@stop
