@extends('admin.widget.body')

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">关于AMII商城系统</li>
        </ol>
    </div>
    <div class="card card-underline">
        <div class="card-head">
            <header></header>

        </div><!--end .card-head -->

        <div class="card-body">
            <p>AMII - 自营商城系统</p>
            <p>极简 - 不动声色的美</p>
            <p><a href="http://www.amii.com">http://www.amii.com</a></p>
            <p>版本：1.0</p>
        </div><!-- ./card-body -->
    </div><!-- /.card -->
</section>
@stop
