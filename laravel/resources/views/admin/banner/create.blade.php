@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">
                @if (!isset($showBanner))
                    添加首页轮播图
                @else
                    编辑首页轮播图
                @endif
            </li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                @if (!isset($showBanner))
                    {!! Form::open(['action' => 'Admin\BannerController@store', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                @else
                    {!! Form::open(['action' => 'Admin\BannerController@edits', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                    <input type="hidden" id="id" name="id" value="{{isset($showBanner) && $showBanner->id ? $showBanner->id : ''}}">
                @endif
                        @include('admin.widget.select', [
                            'colsm'  => '12',
                            'collg'  => '6',
                            'id'     => 'status',
                            'name'   => 'status',
                            'title'  => '启用状态',
                            'selected' => old('status') ? old('status') : (isset($showBanner) && $showBanner->status ? $showBanner->status : ''),
                            'values' => [
                                '关闭' => '0',
                                '启用' => '1',
                            ]
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'weight',
                            'title' => '排序权重（降序）',
                            'type'  => 'text',
                            'value' => old('weight') ? old('weight') : (isset($showBanner->weight) ? $showBanner->weight : '0')
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name'  => 'title',
                            'id'    => 'title',
                            'title' => '标题',
                            'type'  => 'text',
                            'value' => old('title') ? old('title') : (isset($showBanner->title) ? $showBanner->title : ''),
                        ])

                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id'    => 'provider',
                            'name'  => 'provider',
                            'title' => '跳转方式',
                            'selected' => old('provider') ? old('provider') : (isset($showBanner) && $showBanner->provider_value ? $showBanner->provider_value : '5'),
                           'values' => [
                                '品牌分类列表' => '5',
                                '分类商品列表' => '1',
                                '品牌商品列表' => '2',
                                '商品详情'    => '3',
                                '跳转到页面'  => '4',
                            ],
                        ])

                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'resource',
                            'id' => 'resource',
                            'title' => '跳转资源标识',
                            'type'  => 'text',
                            'value' => old('resource') ? old('resource') : (isset($showBanner->resource) ? $showBanner->resource : ''),
                            'attribute' => "placeholder=请填写品牌分类ID",
                        ])

                        @include('admin.widget.image-upload', [
                            'colsm' => '12',
                            'collg' => '4',
                            'class' => 'puload-display-lg-banner-app',
                            'id' => 'bannerAppImg',
                            'imgId' => 'bannerAppImgId',
                            'name' => 'appImage',
                            'descript' => '移动应用配图(690x930)',
                            'defaultImg' => old('appImage') ? old('appImage') : (isset($showBanner) ? $showBanner->app_image : ''),
                        ])
                        <input type="hidden" name="banImg" id="banImg" value="" src="" />
                        @include('admin.widget.image-upload', [
                            'colsm' => '12',
                            'collg' => '4',
                            'class' => 'puload-display-lg-banner-app',
                            'id' => 'bannerMobileImg',
                            'imgId' => 'bannerMobileImgId',
                            'name' => 'mobileImage',
                            'descript' => '移动网页配图(690x930)',
                            'defaultImg' => old('mobileImage') ? old('mobileImage') : (isset($showBanner) ? $showBanner->mobile_image : ''),
                        ])
                         <input type="hidden" name="mobileImg" id="mobileImg" value="" src="" />
                        @include('admin.widget.image-upload', [
                            'colsm' => '12',
                            'collg' => '4',
                            'class' => 'puload-display-lg-weapp-mobile',
                            'id'    => 'popularWeappImg',
                            'imgId' => 'popularWeappImgId',
                            'name'  => 'weappImage',
                            'descript' => '移动小程序配图(705x560)',
                            'style' => 'width: 344px;height: 238px;',
                            'defaultImg' => old('weappImage') ? old('weappImage') : (isset($showBanner) ? $showBanner->weapp_image : ''),
                        ])
                         <input type="hidden" name="weappImg" id="weappImg" value="" src="" />
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group text-right">
                                <button type="submit" class='btn btn-success'>保存</button>
                            </div>
                        </div>

                        <!-- 模态框（Modal） -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content" style="max-height:calc(100vh - 140px); overflow-y: scroll;">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">
                                            添加跳转资源标识
                                        </h4>
                                    </div>
                                    <div class="modal-body">


                                        <div class="card-body">
                                           <form class="form floating-label"  >
                                                <div class="brand hiddenAll"  >
                                                 @include('admin.widget.select', [
                                                    'colsm' => '12',
                                                    'collg' => '12',
                                                    'id'    => 'brandId',
                                                    'name'  => 'brand_id',
                                                    'title' => '请选择品牌',
                                                    'values' =>  $brandData,
                                                ])
                                                </div>
                                                <div class="category hiddenAll"  >
                                                    @include('admin.widget.select', [
                                                        'colsm' => '12',
                                                        'collg' => '12',
                                                        'id'    => 'categoryId',
                                                        'name'  => 'category_id',
                                                        'title' => '请选择品牌下的分类',
                                                        'brand' => []
                                                    ])
                                                </div>
                                                <!-- 用户分类商品搜索 -->
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hiddenAll cate">
                                                        <div class="dd nestable-list">

                                                            @foreach($categories as $category)
                                                            <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 " style="margin-top: 10px;margin-left: 30px;">
                                                                <label class="checkbox-inline checkbox-styled">
                                                                    <input name="categories"   type="radio" value="{{$category->id}}"
                                                                    @if(isset($brandCategory))
                                                                        @if(in_array($category->id,$brand->brandCategory->pluck('category_id')->toArray()))
                                                                            checked="checked"
                                                                            class="js-danger-delete"
                                                                        @endif
                                                                    @endif
                                                                     ><span class="">{{$category->name}}</span>
                                                                </label>

                                                            </div><!--end .col -->
                                                            @endforeach
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hiddenAll cateBtn">
                                                    <div class="input-group text-right">
                                                     <div class="input-group-btn">
                                                        <button class="btn  btn-success btn-md" type="button" id="Catebtn">添加</button>
                                                    </div>
                                                    </div>
                                                </div>

                                            </form>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 searchPorduct hiddenAll">
                                            <div class="input-group">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入">
                                                    <label for="search">请输入商品名称或产品编号</label>
                                                    <div class="form-control-line"></div>
                                                </div>
                                                <div class="input-group-btn">
                                                    <button class="btn btn-floating-action btn-default-bright" type="button" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                                </div>
                                            </div>
                                            </div>

                                            <table class="table table-striped table-hover dataTable no-footer searchPorduct hiddenAll">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>商品名称</th>
                                                        <th>商品编号</th>
                                                        <th style="width: 80px;">操作</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="search_tbody">

                                                </tbody>
                                            </table>
                                       </div>
                                        <!-- ./card-body -->

                                    </div>

                                </div><!-- /.modal-content -->
                                <div class="text-right hiddenAll pagebtn" style="margin: 6px 20px 20px 0;" >
                                    <button type="button" id="prePage" class="btn btn-sm btn-default">上一页</button>
                                    <button type="button"  id="nextPage" class="btn btn-sm btn-default" >下一页</button>
                                </div>
                            </div><!-- /.modal -->
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    //移动应用配图
    @if (old('banImg'))
        var newSrc = "{!!old('banImg')!!}";
        $('#bannerAppImgId').attr({'src': newSrc });
        $('#banImg').attr({'value': newSrc});
    @else
        Helper.setDefaultImg($('#bannerAppImg~img'));
    @endif

    $("#bannerAppImg").change(function(){
        Helper.readURL(this, '#bannerAppImgId');

        var obj = $('#bannerAppImg~img');
        var hidden =  $('#banImg');
        getPrevImgSrc($(this), obj, hidden);
    });

    //移动网页配图
    @if (old('mobileImg'))
        var newSrc = "{!!old('mobileImg')!!}";
        $('#bannerMobileImgId').attr({'src': newSrc });
        $('#mobileImg').attr({'value': newSrc});
    @else
        Helper.setDefaultImg($('#bannerMobileImg~img'));
    @endif
    $("#bannerMobileImg").change(function(){
        Helper.readURL(this, '#bannerMobileImgId');

        var obj = $('#bannerMobileImg~img');
        var hidden = $('#mobileImg');
        getPrevImgSrc($(this), obj, hidden);
    });

    //移动小程序网页配图
    @if (old('weappImg'))
        var newSrc = "{!!old('weappImg')!!}";
        $('#popularWeappImgId').attr({'src': newSrc });
        $('#weappImg').attr({'value': newSrc});
    @else
        Helper.setDefaultImg($('#popularWeappImg~img'));
    @endif

    $("#popularWeappImg").change(function(){
        Helper.readURL(this, '#popularWeappImgId');

        var obj = $('#popularWeappImg~img');
        var hidden = $('#weappImg');
        getPrevImgSrc($(this), obj, hidden);
    });

    // 获取预览图片src
    function getPrevImgSrc(_this, obj, hidden){
         _this.click(function(){
            return false;
         })
        var t = setTimeout(function(){
            var newSrc = obj.attr('src');
           hidden.attr({'value': newSrc});
        },10);
    }


    //编辑移动应用配图
    @if(!empty($showBanner->app_image))
         Helper.setDefaultImg($('#bannerAppImg~img'),"{{old('appImage') ? old('appImage') : $showBanner->app_image}}");
    @endif
    //编辑移动网页配图
    @if(!empty($showBanner->mobile_image))
         Helper.setDefaultImg($('#bannerMobileImg~img'),"{{old('mobileImage') ? old('mobileImage') : $showBanner->mobile_image}}");
    @endif
    //编辑移动小程序网页配图
    @if(!empty($showBanner->weapp_image))
         Helper.setDefaultImg($('#popularWeappImg~img'),"{{old('weappImage') ? old('weappImage') : $showBanner->weapp_image}}");
    @endif

    // 提示跳转资源标识
    $("#provider").change(function(){
        $('#resource').val('');
        var value = $(this).val();
        switch(value ){
            case '1':
                $placeholder = '请填写分类ID';
            break;
            case '2':
                $placeholder = '请填写品牌ID';
            break;
            case '3':
                $placeholder = '请填写商品ID';
            break;
            case '4':
                $placeholder = '请填写跳转网址';
            break;
            case '5':
                $placeholder = '请填写品牌分类ID';
            break;
            default:
                $placeholder = '';
            break;
        }
        return $("#resource").attr('placeholder', $placeholder);
    })

       // 模态框弹框
    $('#resource').focus(function(){
        var hiddenAll = $('.hiddenAll');
        var brand     = $('.brand');
        var category  = $('.category');
        var cate      = $('.cate');
        var cateBtn   = $('.cateBtn');
        var searchPorduct = $('.searchPorduct');
        var provider  = $('#provider').val();
        var dataTarget = "#myModal";

        clearSearchDate();
        switch(provider ){
            case '1':
                hiddenAll.hide();
                cate.show();
                cateBtn.show()
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
            break;
            case '2':
                hiddenAll.hide();
                brand.show();
                cateBtn.show()
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
                $("#brandId").find('option:nth-of-type(2)').prop('selected','selected').change();
                brandSelectChange();
            break;
            case '3':
                hiddenAll.hide();
                searchPorduct.show();
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
            break;
            case '4':
                 $(this).removeAttr("data-toggle data-target");
            break;
            case '5':
                hiddenAll.hide();
                brand.show();
                cateBtn.show();
                category.show();
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
                $("#brandId").find('option:nth-of-type(2)').prop('selected','selected').change();
                brandSelectChange();
            break;
            default:
                $(this).removeAttr("data-toggle data-target");
            break;
        }

    })

    // tbody/搜索清空、
    function clearSearchDate(){
        var tbody = $('#search_tbody');
        tbody.empty();
        $('#key').val('');
        $('#brandId').val('');
        $('#categoryId').val('');
    }

    var current_page = 1;
    var prev_page_url = 1;
    var nextPage_url;
    var pagebtn = $('.pagebtn');
    // search
    $('#search-button').click(function(){
        var tbody = $('#search_tbody');
        tbody.empty();
        var url = "{!! action('Admin\\PopularController@search', ['page' => "+ current_page +"]) !!}";
        sendSearchRequire(url);
    })

    // 上一页
    $('body').on('click','#prePage',function() {
        if(prev_page_url == null){
            toastr.error('已是首页', '失败!')
            return false;
        }
        var url = prev_page_url;
        sendSearchRequire(url);
    })

    // 下一页
    $('body').on('click','#nextPage',function() {
        if(nextPage_url == null){
            toastr.error('已是最后一页', '失败!')
            return false;
        }
        var url = nextPage_url;
        sendSearchRequire(url);
    })

    // 获取数据
    function sendSearchRequire(url){

         $.ajax({
            url: url,
            type: 'GET',
            data: {'_token': "{{ csrf_token() }}", 'key': $('#key').val()},
        }).done(function (data) {
            if(data.status == 'success'){
                if(data.data.data == 0){
                    toastr.error('查询不到该商品.', '失败!')
                    return false;
                }
                // 加载
                loadProduct(data);
                // 下一页url
                nextPage_url = data.data.next_page_url;
                // 上一页url
                prev_page_url = data.data.prev_page_url;
                // 判断是否有下一页
                if(nextPage_url != null ){
                    pagebtn.show();
                }
            }else{
                toastr.error('查询不到该商品', '失败！');
            }
        }).fail(function (data) {
            toastr.error('查询不到该商品', '失败！')
        });
    }

    // 添加搜索商品到列表
    function loadProduct(data){
        var tbody = $('#search_tbody');
        tbody.empty();
        var html = "";
        var product = data.data.data;
        var len = product.length;

        for (var i = 0; i < len; i++) {
            html += '<tr>'
                 +  '<td class="number" >' + (i+1) + '</td>'
                 +  '<td class="productName" >' + product[i]['name'] + '</td>'
                 +  '<td class="productSnumber" >' + product[i]['snumber'] + '</td>'
                 +  '<td><a class="btn btn-sm ink-reaction btn-info btn-add" data-id="' + product[i]['id']+ ' ">选择</a></td>'
                 +  '<tr>';
        }
        tbody.append(html);
    }

    // 选择商品
    $(document).on('click', '.btn-add', function(){
        var product_id = $(this).attr('data-id');
        $('#resource').val(product_id);
        $('button[data-dismiss=modal]').click();
    })

    // 选择品牌/分类
    $(document).on('click', '#Catebtn', function(){
        var provider  = $('#provider').val();
        var resource_value;

        if ( provider == 1) {
            resource_value =  $("input:radio[name='categories']:checked").val();
        } else if(provider == 2){
            resource_value = $('#brandId').val();
        } else if (provider == 5) {
            resource_value = $('#categoryId').val();
        }
        $('#resource').val(resource_value);
        $('button[data-dismiss=modal]').click();
    })

    // 添加分类
    // brandSelectChange();
    $('#brandId').change(function(){
       brandSelectChange();
    });

    $('#categoryId').change(function(){
        var tbody = $('#search_tbody');
        tbody.empty();
    });

    function labelStyle(_this){
        _this.css('top',0);
        _this.css('fontSize','12px');
    }

    function brandSelectChange(){
        var id = $('#brandId').val();
        var url = '{!! action("Admin\\PopularController@brandCategory") !!}';
        var tbody = $('#search_tbody');
        tbody.empty();
        $.ajax({
          type: 'GET',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'brand_id':id},
        }).done(function(data) {
            if(data.status=='success'){
                loadCategory(data.data.categorys, data.data.brand_category);
            }else{
                 $('#search_tbody').empty();
                 toastr.error('查询不到该商品.', '失败!')
            }
        }).fail(function(data) {
            toastr.error('查询不到该商品.', '失败!')
        });
    }

    //加载品牌分类
    function loadCategory(category, brand_category){
        if(typeof category == 'undefined'){
            return false;
        }
        var len  = category.length;
        var html = '';
        for (var i = 0 ; i < len; i++) {
            html += '<option value="'+brand_category[i]['id']+'">'+category[i]['name']+'</option>'
        }
        $('#categoryId').empty();
        $('#categoryId').append(html);
        labelStyle($('#categoryId').next());
    }

    // 隐藏选择框默认空选项
    $(function(){
        $('#provider').find('option:first').prop('style', 'display:none');
        $('#brandId').find('option:first').prop('style', 'display:none');
    })



</script>
@stop
