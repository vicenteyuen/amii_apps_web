@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/summernote/summernote.css">
<style type="text/css">
    .square {
    position: absolute;
    margin: auto;
    min-height: 100%;
    min-width: 100%;

    /* For the following settings we set 100%, but it can be higher if needed
    See the answer's update */
    left: -100%;
    right: -100%;
    top: -100%;
    bottom: -100%;
}
</style>
@stop

@section('content')
<section>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @php
                    $activityRule_exist = isset($activityRule) ? true : false;
                    @endphp
                    @if($activityRule_exist)
                        {!! Form::open(['action' => ['Admin\ActivityRuleComtroller@update', $activityRule->id], 'class' => 'form',
                        'id' => 'activityRule-form', 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
                    @else
                        {!! Form::open(['action' => 'Admin\ActivityRuleComtroller@store', 'class' => 'form', 'id' =>
                        'activityRule-form', 'enctype' => 'multipart/form-data']) !!}
                    @endif
                    @include('admin.widget.input', [
                        'name' => 'editIden',
                        'id'   => 'editIden',
                        'type' => 'hidden'
                    ])
                    @include('admin.widget.image-upload', [
                        'colsm' => '12',
                        'collg' => '12',
                        'id'    => 'activityRule-image',
                        'name'  => 'image',
                        'style' => 'width:690px;height:300px;',
                        'descript' => '活动主图(690x300)'
                    ])

                   @include('admin.widget.radio', [
                        'name' => 'is_addLink',
                        'id' => 'is_addLink',
                        'title' => '是否添加链接',
                        'value' => old('is_addLink') ? old('is_addLink') : (isset($activityRule->is_addLink) ? $activityRule->is_addLink : 0 ),
                    ])
                    <div id="showSummernote">
                        <div class="row">
                            <div class="col-lg-10 col-sm-12">
                                @include('admin.widget.textarea', [
                                'colsm' => '12',
                                'collg' => '12',
                                'name'  => "summernoteCont",
                                'id'    => 'summernote',
                                'title' => '',
                                'value' => '',
                                ])
                            </div>
                        </div>
                    </div>
                    <div id="showLink">
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'linkContent',
                            'id'   => 'linkContent',
                            'type' => 'text',
                            'title' => '活动链接',
                            'value' => '',
                        ])
                    </div>

                    @include('admin.widget.input', [
                        'name' => 'content',
                        'id'   => 'hiddenCont',
                        'type' => 'hidden',
                        'value' => old('content') ? old('content'): (isset($activityRule) && $activityRule->content ? $activityRule->content : ''),

                    ])
                    @include('admin.widget.input', [
                        'colsm' => '12',
                        'collg' => '8',
                        'name'  => 'weight',
                        'title' => '排序权重（降序）',
                        'type'  => 'text',
                        'value' => old('weight') ? old('weight') : (isset($activityRule)? $activityRule->weight : '0')
                    ])
                    @include('admin.widget.editor-preview')
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group text-right">
                        <button type="submit" id="save" class='btn btn-success'>保存</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
</section>
@stop

@section('script_link')
<script src="/assets/lib/summernote/summernote.min.js"></script>
<script src="/assets/lib/summernote/lang/summernote-zh-CN.js"></script>
<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/tmpl/tmpl.min.js"></script>
@stop

@section('script')
<script>
    var product_wrapper_style = "margin:13px 0;display: flex;border: 1px solid #e1e1e1;border-radius: 5px;";
    var product_img_style = "width:30%; max-width:30%;position:relative;overflow: hidden;width: 200px;height: 200px;";
    var product_img_img_style = "width:100%;height:100%; border-top-left-radius:5px;border-bottom-left-radius: 5px; ";
    var img_content_style = "flex-grow: 1;padding: 10px 13px;display: flex;justify-content: space-between;flex-direction: column;";
    var product_price_content_style = "display: flex;justify-content: space-between; align-items:center;";
    var product_price_style = "color: #dca678; font-size: 15px;";
    var buy_button_style = "height: 25px;width: 90px;background: black;border-radius: 5px;color: white;text-align: center;font-size: 13px;line-height: 25px;";

    // 初始化隐藏
     $('#showLink').hide();

     //当出现表单信息提交不成功的处理
     @if ( old('content') )
        var value = $('input[name = is_addLink]:checked').val();
        if(value == 1) {
            $('#showSummernote').hide();
            $('#showLink').show();
            $('#linkContent').val($('#hiddenCont').val());
        } else {
            $('#showSummernote').show();
            $('#showLink').hide();
            $('#summernote').summernote('code', $('#hiddenCont').val());
        }
     @endif

     //点击提交订单时，赋值给中间隐藏着input标签
     $(document).on('click', '#save',function(){
        var value = $('input[name = is_addLink]:checked').val();
        // console.log($('#hiddenCont').val());
        // console.log(value);
        // 富文本有值
        if (value == 1) {
            $('#hiddenCont').val($('#linkContent').val());
        } else {
            $('#hiddenCont').val($('#summernote').val());
        }
        // console.log($('#hiddenCont').val());
        // return false;
     })

    // 编辑状态
    @if (isset($activityRule))
        var value = "{{ $activityRule->is_addLink }}";
        if(value == 1){
            $('#showSummernote').hide();
            $('#showLink').show();

            $('#linkContent').val($('#hiddenCont').val());
        } else {
            $('#showSummernote').show();
            $('#showLink').hide();

            $('#summernote').summernote('code', $('#hiddenCont').val());
        }

    @endif

    // 判断时候显示添加链接
    $(document).on('click', 'input[name = is_addLink]', function(){
        var value = $(this).val();
        showLink(value);
    })
    // 显示或隐藏
    function showLink(value)
    {
        if(value == 1){
            $('#showSummernote').hide();
            $('#linkContent').val('');
            $('#showLink').show();
        } else {
            $('#showSummernote').show();
             $('#summernote').val('');
            $('#showLink').hide();
        }
    }

    //初始化富文本
    $summernote = $('#summernote');
    $summernote.summernote({
        lang: "zh-CN",
        height: "500",
        callbacks: {
            onImageUpload: function (files) {
                var length = files.length;
                for (var i = 0; i < length; i++) {
                    sendFile(files[i]);
                }
            },
            onInit: function() {
                addPreviewButton();
            }
        }
    });

    // add preview button to editor
    function addPreviewButton(){
        var noteBtn = '<button id="previewEditor" type="button" class="btn btn-default btn-sm btn-small" title="预览" data-event="" tabindex="-1"><i class="fa fa-eye"></i></button>';
        var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
        $(fileGroup).appendTo($('.note-toolbar'));
        // Button tooltips
        $('#previewEditor').tooltip({container: 'body', placement: 'bottom'});
        // Button events
        $('#previewEditor').click(function(event) {
            $('#editor-preview-tmpl').empty().append($('#summernote').val());
            Helper.modalToggle($('#modal-editor-preview'));
        });
    }

    //upload
    function sendFile(file, editor, welEditable) {
        var data = new FormData();
        data.append('file', file);
        data.append('_token', "{{ csrf_token() }}");
        data.append('provider', 'activityRule');
        $.ajax({
            url: '{!! action("Admin\\HelperController@summernoteImageUpload") !!}',
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false
        }).done(function (url) {
            $('#summernote').summernote('insertImage', url, file.name);
        }).fail(function () {
            console.log("error");
        }).always(function () {
            console.log("complete");
        });
    }

    //上传主图
    @if (isset($activityRule))
        Helper.setDefaultImg($('#uploadImg'), "{{$activityRule->mediumImage}}");
    @else
        Helper.setDefaultImg($('#uploadImg'));
    @endif
    $("#activityRule-image").change(function () {
        Helper.readURL(this);
        $('#editIden').val('-1');
    });


    $('#modal-image-select').on('click', '#imageSelectSubmit', function () {
        if (imageSelected !== undefined) {
            $summernote.summernote('restoreRange');
            $summernote.summernote('insertImage', imageSelected)
        }
        Helper.modalToggle($('#modal-image-select'));
    });

    // editor preview
    $('#editor-preview').on('click', function () {
        $('#editor-preview-tmpl').empty().append($('#summernote').val());
        Helper.modalToggle($('#modal-editor-preview'));

        // 调整富文本插入图片显示尺寸
        $("#editor-preview-tmpl").find('p > img').prop('style', 'width:100%');
    });

    $("#previewEditor").on('click', function(){
        $("#editor-preview-tmpl").find('p > img').prop('style', 'width:100%');
    })
</script>
@stop
