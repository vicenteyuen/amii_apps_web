<script type="text/x-tmpl" id="tmpl-image-select">
    {%if (!o.data.length) { %}
        <div class="">
           <p>图片管理暂无商品图片，请先上传</p>
        </div>
    {% } %}
    {% for(var i = 0; i < o.data.length; i++) { %}
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 imageSelect">
        <div class="image-contain">
            <img src="{%=o.data[i].url%}" data-id="{%=o.data[i].id%}">
            <label class="position-ab font-0 images-select"></label>
        </div>
    </div>
    {% } %}
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button type="button" id="imageSelectSubmit" class="btn btn-primary">确定</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <div style="float: right;" >
        <button type="button" id="prePage" class="btn btn-sm btn-default">上一页</button>
        <button type="button"  id="nextPage" class="btn btn-sm btn-default" >下一页</button>
        </div>
    </div>


</script>
    <script type="text/x-tmpl" id="tmpl-image-select-view">
    {% for(img in o) { %}
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <div class="image-contain">
            <img src="{%=o[img]%}">
        </div>
    </div>
    {% } %}
</script>

{{--
    @include('admin.widget.image-select-tmpl')
--}}
