<div class="modal fade" id="modal-editor-preview">
    <div class="modal-dialog">
        <div class="modal-content">
            <section>
                <div class="row" id="editor-preview-tmpl" style="width: 576px;">
                </div>
                <div class="row">
                    <div class="col-lg-12" align="right">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
