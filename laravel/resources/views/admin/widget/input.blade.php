<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }} {{$class or ''}}">
    <div class="form-group">
        <input type="{{$type or 'text'}}" name="{{ $name or '' }}" id="{{ $id or '' }}" class="form-control" {{ $attribute or '' }} value="{{$value or ''}}" {{ $readonly or '' }} data-value="{{$dataValue or ''}}">
        <label for="{{ $id or '' }}" class="control-label">{{ $title or '' }}</label>
    </div>
</div>
{{--
    @include('admin.widget.input', [
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'name' => 'name',
        'id' => 'name',
        'title' => '商品名',
        'type'  => 'hidden'
        'attribute' => 'attributeValue',
        'value'  => 'value'
    ])
--}}
