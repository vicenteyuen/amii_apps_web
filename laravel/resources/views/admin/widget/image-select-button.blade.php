<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '12' }} col-lg-{{ $collg or '12' }} {{$position or ''}}" style="margin-top: {{$marginTop or '0px'}};margin-left: {{$marginLeft or '0px'}}">
    <button type="button" id="{{ $id or '' }}" class="btn btn-primary ink-reaction btn-md" data-toggle="modal">
        <i class="fa fa-file-photo-o"></i>&nbsp;{{ $text or '上传' }}
    </button>
</div>
{{--
    @include('admin.widget.image-select-button', [
        'id' => 'idValue',
        'text' => '上传',
    ])
    <div class="row">
        @include('admin.widget.image-select-button', [
            'id' => 'idValue',
            'text' => '选择图片',
        ])
    </div>
    <script src="/assets/lib/tmpl/tmpl.min.js"></script>
    <script type="text/javascript">
        $('#idValue').on('click', function () {
            $.ajax({
                url: "{!! action('Admin\\ImageController@index') !!}",
                type: 'GET'
            })
            .done(function (data) {
                $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', data));
                Helper.modalToggle($('#modal-image-select'));
            });
        });
    </script>
--}}
