<div class="row">
    <div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}">
        <div class="modal fade" id="modal-file-upload">
            <div class="modal-dialog">
                <div class="modal-content">
                    {{ $form or Form::open([]) }}
                    <!-- <form class="form-inline" enctype="multipart/form-data" method="post" action=""> -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                ×
                            </button>
                            <h4 class="modal-title">上传文件</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="file" class="col-sm-3 control-label">
                                    文件
                                </label>
                                <div class="col-sm-8">
                                    <input type="file" name="xml">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                取消
                            </button>
                            <button type="submit" class="btn btn-primary">
                                上传
                            </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
{{--
    @include('admin.widget.file-upload', [
        'form' => Form::open(['action' => 'Admin\ProductController@store'])
    ])
--}}
