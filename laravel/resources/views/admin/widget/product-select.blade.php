<div class="modal fade" id="modal-product-select">
    <div class="modal-dialog">
        <div class="modal-content">
            <section>
                <div class="row" id="product-select-tmpl">
                    <div class="card-header form form-inline">
                        <div class="form-group">
                            <input type="text" name="keyword" class="form-control" placeholder="输入商品id或名称">
                        </div>
                        <button type="button" class="btn btn-sm btn-default-bright" id="search-product">提交</button>
                    </div>
                    <div class="card-body no-padding" style="height:400px;overflow-y: scroll">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th width="100">编号</th>
                                    <th>名称</th>
                                    <th width="100">操作</th>
                                </tr>
                            </thead>
                            <tbody id="product-table">
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>