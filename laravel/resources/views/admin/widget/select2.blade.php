<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}">
    <div class="form-group">
            <select id="{{ $id or ''}}" name="{{$name or ''}}" class="form-control select2-list" data-placeholder="{{$placeholder or '请选择'}}" multiple>
                <?php
                $brandCategory = isset($brand->brandCategory)? $brand->brandCategory: $categories;
                echo \App\Helpers\AdminViewHelper::categorySelectHtml($categories, '', $brandCategory);
                ?>
            </select>
        <!-- <span class="fa fa-unsorted form-control-feedback"></span> -->
    </div>
</div>
{{--
    @include('admin.widget.select2', [
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'attribute' => 'attributeValue',
        'id' => 'idValue',
        'name' => 'nameValue',
        'title' => 'titleValue',
      
    ])
--}}
