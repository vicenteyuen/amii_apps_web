<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}">
    <div class="form-group floating-label">
        <select id="{{ $id or '' }}"  name="{{ $name or '' }}" class="form-control" {{ $attribute or '' }}>
                @foreach ($value as $key => $value)
                <option value="{{ $value->id }}" 
                @if(!empty($expressId))
                    @if($value->id == $expressId)
                        selected
                    @endif
                @endif
                >{{ $value->name }}</option>
                @endforeach
        </select>
        <label for="{{ $id or '' }}">{{ $title or '' }}</label>
        <p class="help-block">{{ $text or '' }}</p>
    </div>
</div>
{{--
    @include('admin.widget.select', [
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'attribute' => 'attributeValue',
        'id' => 'idValue',
        'name' => 'nameValue',
        'title' => 'titleValue',
        'values' => [
            'name' => 'value',
        ]
    ])
--}}
