@extends('admin.widget.base')
@section('body')
    <header id="header">
        <div class="headerbar">
            <div class="headerbar-left">
                <ul class="header-nav header-nav-options">
                    <li class="header-nav-brand" >
                        <div class="brand-holder">
                            <a href="/admin">
                                <span class="text-lg text-bold text-primary">AMII商城系统</span>
                            </a>
                        </div>
                    </li>
                    <li>
                        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="headerbar-right">
                <ul class="header-nav header-nav-profile">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                            <span class="profile-info">
                                {{ Auth('admin')->user()->email }}
                                <small>{{ Auth('admin')->user()->nickname }}</small>
                            </span>
                        </a>
                        <ul class="dropdown-menu animation-dock">
                            <li><a href="">重置密码</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ action('Admin\LoginController@logout') }}"><i class="fa fa-fw fa-power-off text-danger" aria-hidden="true"></i> 退出</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div id="base">
        <div id="content">
            @yield('content')
        </div>
        <div id="menubar" class="menubar-inverse">
            <div class="menubar-scroll-panel">
                <ul id="main-menu" class="gui-controls">
                    @include('admin.widget.sidebar')
                </ul>
            </div>
        </div>
    </div>
@stop
