<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}" style="margin-top: {{ $marginTop or 0 }}">
    <button type="button" class="btn btn-primary ink-reaction btn-md" data-toggle="modal" data-target="#modal-file-upload">
        <i class="fa fa-upload"></i>{{ $text or '上传' }}
    </button>
</div>
{{--
    @include('admin.widget.file-upload-button', [
        'text' => '上传',
    ])
--}}
