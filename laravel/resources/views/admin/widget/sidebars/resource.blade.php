<!-- resource -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-file-image-o" aria-hidden="true"></i></div>
        <span class="title">素材模块</span>
    </a>
    <ul>
        <!-- image -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">图片管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\ImageController@index')  }}"><span class="title">添加图片</span></a></li>
                <li><a href="{{ action('Admin\ImageFileController@showFileImage')  }}"><span class="title">图片列表</span></a></li>
                <li><a href="{{ action('Admin\ImageFileController@index')  }}"><span class="title">文件列表</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
    </ul>
</li>