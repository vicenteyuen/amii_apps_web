<!-- member -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
        <span class="title">会员模块</span>
    </a>
    <ul>
        <li>
            <a href="{{ action('Admin\MemberController@index') }}"><span class="title">会员列表</span></a>
        </li>
    </ul>
</li>
