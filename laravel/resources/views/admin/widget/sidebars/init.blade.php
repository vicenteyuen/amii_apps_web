<!-- init -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-cogs" aria-hidden="true"></i></div>
        <span class="title">系统配置模块</span>
    </a>
    <ul>
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">物流管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\ExpressController@create') }}"><span class="title">新增快递公司</span></a></li>
                <li><a href="{{ action('Admin\ExpressController@index') }}"><span class="title">快递公司列表</span></a></li>
                <li><a href="{{ action('Admin\ExpressTemplateController@create') }}"><span class="title">新增运费模版</span></a></li>
                <li><a href="{{ action('Admin\ExpressTemplateController@index') }}"><span class="title">运费模版列表</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">淘宝同步管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\TaobaoSyncController@category') }}"><span class="title">淘宝分类映射系统分类</span></a></li>
                <li><a href="{{ action('Admin\TaobaoSyncController@index') }}"><span class="title">淘宝商品同步</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
        <li>
            <a href="{{ action('Admin\ProductServiceController@index') }}"><span class="title">折扣服务列表</span></a>
        </li>
        <li>
            <a href="{{ action('Admin\VersionController@index') }}"><span class="title">版本管理</span></a>
        </li>
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">公告功能</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\PointAnnounceController@index') }}"><span class="title">积分排名公告</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">App下载渠道管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\ChannelController@index') }}"><span class="title">渠道列表</span></a></li>
                <li><a href="{{ action('Admin\ChannelController@create') }}"><span class="title">添加渠道</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">分享文案管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\ShareController@index') }}"><span class="title">分享文案列表</span></a></li>
                <li><a href="{{ action('Admin\ShareController@create') }}"><span class="title">添加分享文案</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
    </ul>
</li>
