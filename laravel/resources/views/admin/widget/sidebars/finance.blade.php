<!-- finance -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-jpy" aria-hidden="true"></i></div>
        <span class="title">财务模块</span>
    </a>
    <ul>
        <!-- coupon -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">优惠券管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\CouponController@index') }}"><span class="title">优惠券列表</span></a></li>
                <li><a href="{{ action('Admin\CouponController@create') }}"><span class="title">添加优惠券</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->

        <!-- withdraw -->
        <li>
            <a href="{{ action('Admin\WithdrawController@index') }}"><span class="title">提现管理</span></a>
        </li>
    </ul>
</li>
