<!-- shop -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></div>
        <span class="title">商城模块</span>
    </a>
    <ul>
        <!-- product -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">商品管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\ProductController@index') }}"><span class="title">商品列表</span></a></li>
                <li><a href="{{ action('Admin\ProductController@create') }}"><span class="title">添加商品</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->

        <!-- brand -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">品牌管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\BrandController@index') }}"><span class="title">品牌列表</span></a></li>
                <li><a href="{{ action('Admin\BrandController@create') }}"><span class="title">添加品牌</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->

        <!-- category -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">分类管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\CategoryController@index') }}"><span class="title">分类管理</span></a></li>
                <li><a href="{{ action('Admin\ProductCategoryController@categoryShow') }}"><span class="title">添加分类品牌商品</span></a></li>
                <li><a href="{{ action('Admin\ProductCategoryController@categoryProductShow') }}"><span class="title">添加分类商品</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->

        <!-- attribute -->
        <li>
            <a href="{{ action('Admin\AttributeController@index') }}"><span class="title">属性管理</span></a>
        </li><!--end /submenu-li -->
    </ul>
</li>