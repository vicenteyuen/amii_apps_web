<!-- message -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></div>
        <span class="title">消息模块</span>
    </a>
    <ul>
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">通知消息管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\NotifyMessageController@index')}}"><span class="title">物流消息列表</span></a></li>
                <li><a href="{{ action('Admin\NotifyMessageController@editNotify')}}"><span class="title">发送系统消息</span></a></li>
                <li><a href="{{ action('Admin\NotifyMessageController@notifyIndex')}}"><span class="title">系统消息列表</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
        <li>
            <a href="{{ action('Admin\FeedBackController@index') }}"><span class="title">用户反馈列表</span></a>
        </li>
    </ul>
</li>
