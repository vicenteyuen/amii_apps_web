<!-- home -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i></div>
        <span class="title">App首页模块</span>
    </a>
    <ul>
        <!-- banner -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">App首页轮播管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\BannerController@index') }}"><span class="title">首页轮播列表</span></a></li>
                <li><a href="{{ action('Admin\BannerController@create') }}"><span class="title">添加首页轮播</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->

        <!-- popular -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">App首页推广管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\PopularController@index') }}"><span class="title">推广列表</span></a></li>
                <li><a href="{{ action('Admin\PopularController@create') }}"><span class="title">添加推广</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
    </ul>
</li>
