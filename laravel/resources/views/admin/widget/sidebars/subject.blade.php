<!-- subject -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-university" aria-hidden="true"></i></div>
        <span class="title">社区模块</span>
    </a>
    <ul>
        <li><a href="{{ action('Admin\ActivityRuleComtroller@index') }}"><span class="title">活动列表</span></a></li>
        <li><a href="{{ action('Admin\SubjectController@index') }}"><span class="title">专题列表</span></a></li>
        <li><a href="{{ action('Admin\VideoController@index') }}"><span class="title">视频列表</span></a></li>
        <li><a href="{{ action('Admin\PhysicalStoreController@index') }}"><span class="title">实体店列表</span></a></li>
    </ul>
</li>