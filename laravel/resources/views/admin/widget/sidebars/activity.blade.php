<!-- activity -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-gamepad" aria-hidden="true"></i></div>
        <span class="title">活动模块</span>
    </a>
    <ul>
        <!-- first purchase -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">首单购管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\FirstPurchaseController@index') }}"><span class="title">首单购商品</span></a></li>
                <li><a href="{{ action('Admin\FirstPurchaseTextController@index') }}"><span class="title">首单购文本列表</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->

        <!-- game -->
        <li class="gui-folder">
            <a href="javascript:void(0);">
                <span class="title">拼图送衣管理</span>
            </a>
            <!--start submenu -->
            <ul>
                <li><a href="{{ action('Admin\UserGameController@manager')}}"><span class="title">游戏数据统计</span></a></li>
                <li><a href="{{ action('Admin\GamePushDocumentController@index')}}"><span class="title">游戏推送文案管理</span></a></li>
                <li><a href="{{ action('Admin\UserGameController@index')}}"><span class="title">游戏商品列表</span></a></li>
            </ul><!--end /submenu -->
        </li><!--end /submenu-li -->
    </ul>
</li>
