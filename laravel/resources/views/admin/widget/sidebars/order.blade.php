<!-- order -->
<li class="gui-folder">
    <a>
        <div class="gui-icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
        <span class="title">订单模块</span>
    </a>
    <ul>
        <!-- order -->
        <li>
            <a href="{{ action('Admin\OrderController@index') }}"><span class="title">订单列表</span></a>
        </li><!--end /submenu-li -->

        <!-- comment -->
        <li>
            <a href="{{ action('Admin\UserCommentController@index') }}"><span class="title">评论列表</span></a>
        </li><!--end /submenu-li -->
    </ul>
</li>
