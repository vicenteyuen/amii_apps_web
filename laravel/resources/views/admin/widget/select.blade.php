<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}">
    <div class="form-group floating-label">
        <select id="{{ $id or '' }}"  name="{{ $name or '' }}" class="form-control" {{ $attribute or '' }}>
            @if (isset($selectArr))
                @foreach ($values as $value => $show)
                    @if (intval($value) == intval($selectArr))
                        <option selected="selected" value="{{ $value }}">{{ $show }}</option>
                    @else
                        <option value="{{ $value }}">{{ $show }}</option>
                    @endif
                @endforeach
            @elseif(isset($brand))
                @foreach ($brand as $key => $value)
                <option value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach
            @elseif(isset($expressIndex))
                @foreach ($expressIndex as $key => $value)
                <option value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach
            @elseif(isset($selectValues))
                @foreach ($selectValues as $key => $value)
                <option value="{{ $value->id }}">{{ $value->name }}</option>
                @endforeach
            @else
                <option value="">&nbsp;</option>
                @foreach ($values as $key => $value)

                    @if (isset($selected) && $value == $selected)
                    <option selected="selected" value="{{ $value }}">{{ $key }}</option>
                    @else
                    <option value="{{ $value }}">{{ $key }}</option>
                    @endif

                @endforeach
            @endif
        </select>
        <label for="{{ $id or '' }}">{{ $title or '' }}</label>
        <p class="help-block">{{ $text or '' }}</p>
    </div>
</div>
{{--
    @include('admin.widget.select', [
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'attribute' => 'attributeValue',
        'id' => 'idValue',
        'name' => 'nameValue',
        'title' => 'titleValue',
        'values' => [
            'key' => 'value',
        ]
    ])
--}}
