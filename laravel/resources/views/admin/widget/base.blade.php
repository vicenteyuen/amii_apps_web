<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta name="renderer" content="webkit">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="https://mobileapps.amii.com">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/materialadmin1.min.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/nestable.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/material-design-iconic-font.min.css">
        @yield('style_link')
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/admin.css">

        <!--[if lt IE 9]>
        <script type="text/javascript" src="assets/js/html5shiv.min.js"></script>
        <script type="text/javascript" src="assets/js/respond.min.js"></script>
        <![endif]-->
        @yield('style')
    </head>
    <body class="menubar-hoverable header-fixed menubar-pin">
        @yield('body')
        <script src="/assets/admin/js/ganguo-admin.min.js"></script>
        <script src="/assets/lib/layer/layer.js"></script>
        <script src="/assets/admin/js/jquery.cookie.min.js"></script>
        <script src="/assets/admin/js/admin.js"></script>
        @yield('script_link')
        @yield('script')
        @if (! empty($errors))
            <script type="text/javascript">
                $.each({!! json_encode($errors) !!}, function(k,v){
                    if(typeof v == 'object'){
                        v = v[0];
                    }
                    layer.msg(v, {
                        icon: 2
                    });
                });
            </script>
        @endif
        @if (old('success_msg'))
            <script type="text/javascript">
                layer.msg('{{ old('success_msg') }}', {
                    icon: 1
                });
            </script>
        @endif
    </body>
</html>
