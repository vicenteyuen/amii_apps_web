<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '12' }} col-lg-{{ $collg or '12' }}">
    <div class="form-group floating-label">
        <label>{{ $title or '' }}</label>
        <div id="{{ $id or '' }}" style="margin-top: 20px">
            <label class="radio-inline radio-styled radio-success">
                <input type="radio" @if( empty($value) ||$value == 1) checked @endif name="{{ $name or '' }}" value="1"><span>是</span>
            </label>
            <label class="radio-inline radio-styled radio-danger">
                <input type="radio" @if(isset($value) && $value == 0) checked @endif name="{{ $name or '' }}" value="0"><span>否</span>
            </label>
        </div>
    </div>
</div>
{{--
    @include('admin.widget.radio', [
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'name' => 'name',
        'id' => 'name',
        'title' => '商品名',
    ])
--}}