<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}">
	<div class="form-group">
    <button type="button" class="btn ink-reaction btn-primary btn-md" id={{ $id or ''}} style="margin-top: {{$marginTop or '0px'}}">
        <i class="fa"></i>{{ $text or '新增属性' }}
    </button>
    </div>
</div>
{{--
    @include('admin.widget.button', [
        'text' => '新增属性',
    ])
--}}