<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}">
    <div class="form-group floating-label">
        <textarea name="{{ $name or '' }}" id="{{ $id or '' }}" class="form-control" {{ $readonly or '' }} placeholder="{{ $placeholder or '' }}" {{ $attribute or '' }} >{{$value or ''}}</textarea>
        <label for="{{ $id or '' }}">{{ $title or '' }}</label>
    </div>
</div>
{{--
    @include('admin.widget.textarea', [
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'name' => 'name',
        'id' => 'name',
        'title' => '商品简介',
        'placeholder' => 'placeholderValue',
        'attribute' => 'attributeValue'
    ])
--}}
