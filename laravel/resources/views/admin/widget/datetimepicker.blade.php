<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}">
    <div class="form-group floating-label input-group date {{ $class or '' }}">
        <input class="form-control" size="16" style="{{$style or '' }}" type="text" value="{{ $value or '' }}" readonly="" data-link-field="{{ $name }}">
        <span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
        <label for="{{ $name }}">{{ $title or '' }}</label>
        <input type="hidden" id="{{ $name }}" name="{{ $name  }}" value="" /><br/>
    </div>
</div>
{{--
    @include('admin.widget.datetimepicker', [
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'name' => 'name',
        'class' => 'datetimepicker',
        'value' => '2013-02-21T15:25:00Z',
    ])

    $(".class").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 0,
        forceParse: 0,
    });
--}}
