@if (isset($row) && $row)
<div class="row">
@endif

<div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '6' }} col-lg-{{ $collg or '6' }}  ">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.image.css">
    <div class="thumbnail upload-display {{ $class or '' }} resizable" style="{{ $style or '' }}">
        <input type='file' id="{{ $id or 'uploadImgInput' }}" name="{{ $name or '' }}" accept=".png,.gif,.jpg"/>
        <label for="{{ $id or 'uploadImgInput' }}" style="margin-left: 10px;color: #969c9c">{{$label or ''}}</label>
        <img id="{{$imgId or 'uploadImg'}}" data-src="holder.js/100%x180" src="{{ $defaultImg or '' }}" alt="error img" />
    </div>
    <p class="text-left">{{ $descript or '' }}</p>
</div>
@if (isset($row) && $row)
</div>
@endif
{{--
    @include('admin.widget.image-upload', [
        'row' => true,      #是否单独使用row
        'colsm' => '12',    #小屏幕显示宽度
        'collg' => '6',     ＃大屏幕显示宽度
        'id' => 'idValue',
        'imgId' => 'uploadImg',
        'name' => 'nameValue',
    ])
    Helper.setDefaultImg($('#idValue~img'));
    $("#idValue~img").change(function(){
        Helper.readURL(this);
    });
--}}
