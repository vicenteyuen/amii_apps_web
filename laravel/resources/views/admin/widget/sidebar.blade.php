<li>
    <a href="{{ action('Admin\DashboardController@getIndex') }}">
        <div class="gui-icon"><i class="md md-home"></i></div>
        <span class="title">信息面板</span>
    </a>
</li>

@include('admin.widget.sidebars.home')

@include('admin.widget.sidebars.shop')

@include('admin.widget.sidebars.order')

@include('admin.widget.sidebars.finance')

@include('admin.widget.sidebars.resource')

@include('admin.widget.sidebars.subject')

@include('admin.widget.sidebars.activity')

@include('admin.widget.sidebars.member')

@include('admin.widget.sidebars.message')

@include('admin.widget.sidebars.report')

@include('admin.widget.sidebars.init')
