@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
@stop

@section('content')
<section>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                    <form class="form floating-label" autocomplete="off">
                        <div class="form-wizard-nav">
                            <div class="progress" style="width: 75%;"><div class="progress-bar progress-bar-primary" style="width: 0%;"></div></div>
                            <ul class="nav nav-justified nav-pills">
                                <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">商品属性</span></a></li>
                                <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">商品库存</span></a></li>
                                <li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">商品图片</span></a></li>
                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            @include('admin.inventory.tab1')
                            @include('admin.inventory.tab2')
                            @include('admin.inventory.tab3')
                        </div><!--end .tab-content -->
                        <ul class="pager wizard">
                            <li class="previous disabled"><a class="btn-raised button-previous" href="javascript:void(0);">Previous</a></li>
                            <li class="next"><a class="btn-raised button-next" href="javascript:void(0);">Next</a></li>
                        </ul>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@include('admin.widget.file-upload')
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
@stop

@section('script')
<script type="text/javascript">
    $('#rootwizard').bootstrapWizard({
        'withVisible': true,
        'tabClass': 'nav nav-pills',
        'nextSelector': '.button-next',
        'previousSelector': '.button-previous',
        'onTabClick': function() {
            return false;
        },
        'onTabShow': onTabShow
    });

    // wizard on tab show
    function onTabShow(tab, navigation, index){
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        $('#rootwizard').find('.progress-bar').css({width: percent + '%'});
        $('.form-wizard-horizontal').find('.progress').css({'width': percentWidth});
    }

    $('#attr_add_button').click(function(){
        
        var attribute = $("#attribute-select").find("option:selected").text();
         layer.msg('23');
         layer.prompt({title: '请输入属性值'},function(value, index, elem){
             
            layer.close(index);
         });
    })

</script>
@stop
