<div class="tab-pane active" id="tab1">
    <br><br>
    <div class="row">

    </div>
    <div class="form-group">
        <input type="text" name="Address" id="Address" class="form-control" value="{{$name}}">
        <label for="Address" class="control-label">商品名称</label>
    </div>

    <div class="form-group">
    <label class="col-sm-5 text-l control-label" style="margin-left: -14px">选择属性：</label>
        <div class="col-sm-11">
            <div class="row">
                <div class="col-sm-2">
                    <select id="attribute-select" name="attribute-select" class="static dirty pull-left form-control">
                        <?php echo \App\Helpers\AdminViewHelper::objectSelectHtml($attributes, 0);  ?>
                    </select>
                </div>
                <button class="col-sm-2 btn ink-reaction btn-success" type="button" id="attr_add_button">添加属性</button>
                <div class="form-inline col-sm-8"></div>
            </div>
        </div>
    </div>
    
</div><!--end #tab1 -->
