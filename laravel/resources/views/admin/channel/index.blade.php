@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">渠道列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <dir class="row">
                    <a href="{{ action('Admin\\ChannelController@create') }}" class="btn btn-primary ink-reaction btn-sm" >
                        添加
                     </a>
                </dir>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if ($channels->isEmpty())
                            @include('admin.channel.null')
                        @else
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>渠道名称</th>
                                    <th>渠道二维码</th>
                                    <th>下载量</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($channels as $channel)
                                <tr>
                                    <td>{{ $channel->id }}</td>
                                    <td>{{ $channel->name }}</td>
                                    <td>
                                        <a href="{{ action('Admin\\ChannelController@downloadQrcode',['provider' => $channel->code]) }}"  >
                                            <img id="downloadQrcode"  width="100px" src="{{ url('/channelQrcode/'.$channel->code.'.png') }}"/>
                                        </a>
                                    </td>
                                     <td>{{ $channel->downloads }}</td>
                                    <td>
                                        <a href="{{ action('Admin\ChannelController@showEdits', ['id' => $channel->id])}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>

                                        <a class="btn ink-reaction btn-danger btn-xs js-delete" data-id="{{$channel->id }}" data-name="{{$channel->name }}">
                                            删除
                                        </a>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if($channels->toArray()['data'])
                        <div class="col-md-12 col-lg-12">
                            <label style="color: black;font-size: 16px">当前页数量: {{$channels->toArray()['to'] - $channels->toArray()['from'] + 1}}</label>
                            <label style="color: black;font-size: 16px">总数量: {{$channels->toArray()['total']}}</label>
                        </div>
                         @endif
                        <div class="text-center">
                            {!! $channels->render() !!}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

  // 删除
    $('.js-delete').click(function(){
        var _this = $(this);
        layer.confirm('您确定删除 "'+_this.attr('data-name')+'" 这个渠道？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\ChannelController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });
</script>
@stop






