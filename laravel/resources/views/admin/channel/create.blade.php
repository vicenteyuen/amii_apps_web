@extends('admin.widget.body')

@section('style_link')
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">
                @if(!isset($channel))
                    添加渠道
                @else
                    编辑渠道
                @endif
            </li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @if(!isset($channel))
                        {!! Form::open(['action' => 'Admin\ChannelController@store', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                    @else
                       {!! Form::open(['action' => 'Admin\ChannelController@edits', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                       <input type="hidden" name="id" value="{{$channel->id}}">
                    @endif

                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'name',
                            'id' => 'name',
                            'title' => '渠道名称',
                            'type'  => 'text',
                            'value' => isset($channel) ? $channel->name : ''
                        ])

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group text-right">
                                <button type="submit" class='btn btn-success'>保存</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
@stop

@section('script')
<script type="text/javascript">

</script>
@stop
