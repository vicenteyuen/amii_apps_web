@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">优惠券标识码列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th width="20%">优惠券标识码</th>
                                    <th width="20%">领用状态</th>
                                    <th width="25%">开始时间</th>
                                    <th width="25%">结束时间</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($couponGrants as $couponGrant)
                                <tr>
                                    <td>{{ $couponGrant->id }}</td>
                                    <td>{{ $couponGrant->code }}</td>
                                    @if ($couponGrant->status == 1 )
                                        <td class="text-green"> 已领用</td>
                                    @else
                                        <td class="text-red"> 未领用</td>
                                    @endif
                                    <td>{{ date('Y-m-d H:m:s', strtotime($couponGrant->coupon->start))  }}</td>
                                    <td>{{ date('Y-m-d H:m:s', strtotime($couponGrant->coupon->end))  }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $couponGrants->appends(['id' => $coupon_id])->render() !!}
                        </div>
                    </div>
                </div>
                <div class="card-head text-right">
                    <a href="{!! action('Admin\\CouponController@index') !!}" class="btn ink-reaction btn-primary btn-md" type="button" >返回</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
@stop






