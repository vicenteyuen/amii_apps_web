@extends('admin.widget.body')

@section('content')
    <section>
        <div class="section-body">
            <div class="row">

                <!-- BEGIN ALERT - REVENUE -->
                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>--}}
                                <strong class="text-xl">{{$numbers->orders_count}}</strong><br/>
                                <span class="opacity-50">总订单数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - REVENUE -->

                <!-- BEGIN ALERT - VISITS -->
                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-warning text-lg">0,01% <i class="md md-swap-vert"></i></strong>--}}
                                <strong class="text-xl">¥ {{$numbers->orders_total_amount}}</strong><br/>
                                <span class="opacity-50">销售总额</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - VISITS -->

                <!-- BEGIN ALERT - TIME ON SITE -->
                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>--}}
                                <strong class="text-xl">{{$numbers->refund_count}}</strong><br/>
                                <span class="opacity-50">售后产品数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - TIME ON SITE -->

                <!-- BEGIN ALERT - BOUNCE RATES -->
                <div class="col-md-3 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-danger text-lg">0,18% <i class="md md-trending-down"></i></strong>--}}
                                <strong class="text-xl">{{$numbers->user_count}}</strong><br/>
                                <span class="opacity-50">总客户数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - BOUNCE RATES -->

            </div><!--end .row -->

            <div class="card">
                <div class="card-head">
                    <header>每月数量统计</header>
                </div><!--end .card-head -->
                <div class="card-body">
                    <canvas id="history"></canvas>
                </div><!--end .card-body -->
            </div><!--end .card -->

        </div>
    </section>
@stop

@section('script_link')
    <script src="//cdn.bootcss.com/Chart.js/2.5.0/Chart.bundle.min.js"></script>
@stop

@section('script')
    <script>
        new Chart(document.getElementById('history'), {
            type: 'line',
            data: {
                labels: {!! json_encode($history['labels']) !!},
                datasets: [
                    {
                        label: "注册数量",
                        fill: false,
                        backgroundColor: "rgba(255, 99, 132, 0.4)",
                        borderColor: "rgba(255, 99, 132,1)",
                        data: {!! json_encode($history['registration']) !!},
                    },
                    {
                        label: "订单数量",
                        fill: false,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        data: {!! json_encode($history['orders']) !!},
                    }
                ]
            },
            options: {

            }
        });
    </script>
@stop
