@extends('admin.widget.base')

@section('body')
<section class="section-account">
    <!-- 公司标志 -->
    <div class="spacer">
		<div class="logo">

        </div>
	</div>
    <div class="card contain-sm style-transparent">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <center><img src="/static/admin/amii_logo.jpg" alt="" height="150" width="300"></center>
                    <br/>
                    <span class="text-lg text-bold text-primary">AMII商城系统</span>
                    <br/><br/>
                    {!! Form::open(['action' => 'Admin\LoginController@store', 'method' => 'POST', 'class' => 'form', 'autocomplete' => 'off']) !!}
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" name="email">
                            <label for="email">邮箱</label>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password">
                            <label for="password">密码</label>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-xs-6 text-left">
                                <div class="checkbox checkbox-inline checkbox-styled">
                                    <label>
                                        <input type="checkbox" name="remember"> <span>记住我</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6 text-right">
                                <button class="btn btn-primary btn-raised" type="submit">登录</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
    @if (config('app.debug'))
        <script type="text/javascript">
            $('#email').val('admin@admin.com');
            $('#password').val('123456');
        </script>
    @endif
@stop
