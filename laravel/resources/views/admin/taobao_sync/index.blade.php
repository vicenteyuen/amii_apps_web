@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-body">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="alert alert-callout alert-success no-margin">
                            <strong class="text-xl">Amii淘宝商品同步</strong><br/>
                            <div class="form-group text-left">
                                <a href="{{ action('Admin\TaobaoSyncController@searchView', ['provider' => 'amii']) }}" class='btn btn-success'>搜索</a>
                                <button onclick="sync('amii')" class='btn btn-success'>开始同步</button>
                            </div>
                        </div>
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="alert alert-callout alert-success no-margin">
                            <strong class="text-xl">AmiiR淘宝商品同步</strong><br/>
                            <div class="form-group text-left">
                                <a href="{{ action('Admin\TaobaoSyncController@searchView', ['provider' => 'amiiR']) }}" class='btn btn-success'>搜索</a>
                                <button onclick="sync('amiiR')" class='btn btn-success'>开始同步</button>
                            </div>
                        </div>
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="alert alert-callout alert-success no-margin">
                            <strong class="text-xl">AmiiM淘宝商品同步</strong><br/>
                            <div class="form-group text-left">
                                <a href="{{ action('Admin\TaobaoSyncController@searchView', ['provider' => 'amiiM']) }}" class='btn btn-success'>搜索</a>
                                <button onclick="sync('amiiM')" class='btn btn-success'>开始同步</button>
                            </div>
                        </div>
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="alert alert-callout alert-success no-margin">
                            <strong class="text-xl">AmiiC淘宝商品同步</strong><br/>
                            <div class="form-group text-left">
                                <a href="{{ action('Admin\TaobaoSyncController@searchView', ['provider' => 'amiiC']) }}" class='btn btn-success'>搜索</a>
                                <button onclick="sync('amiiC')" class='btn btn-success'>开始同步</button>
                            </div>
                        </div>
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function sync(provider) {
        $.ajax({
            type: 'POST',
            url: "{{ action('Admin\TaobaoSyncController@sync') }}",
            data: {
                "provider": provider
            },
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('成功');
            }else{
                if(STATUS == ADD_STATUS){
                    toastr.error(data.message, '失败!');
                }
            }
        }).fail(function(data) {
            toastr.error('失败!');
        });
    }
</script>
@stop
