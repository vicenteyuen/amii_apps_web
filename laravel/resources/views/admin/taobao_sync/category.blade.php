@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">淘宝分类映射系统分类</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th>淘宝分类名</th>
                            <th>已映射系统分类名</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>
                                <div class="form-group floating-label">
                                    <select id="" name="" class="form-control category-select" data-taobao-cate-id="{{ $item->id }}">
                                        <option>不映射系统分类</option>
                                        @foreach ($categorys as $key => $value)
                                            @if ($key == $item->category_id)
                                            <option value="{{ $key }}" selected>{{ $value }}</option>
                                            @else
                                            <option value="{{ $key }}">{{ $value }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var originCateId = 0;
    $('.category-select').on('click', function() {
        originCateId = $(this).val();
    });
    $('.category-select').on('change', function() {
        $('.category-select').prop('disabled', true);
        var cateId = $(this).val();
        var taobaoCateId = $(this).attr('data-taobao-cate-id');
        $.ajax({
            type: 'POST',
            url: "{{ action('Admin\TaobaoSyncController@mapCate') }}",
            data: {
                "taobaoCateId": taobaoCateId,
                "cateId": cateId
            },
        }).done(function(data) {
            $('.category-select').prop('disabled', false);
            if(data.status=='success'){
                layer.msg('映射成功',{icon:1});
            }else{
                layer.msg('映射失败',{icon:0});
                $(".category-select option[value='" + originCateId + "']").prop('selected', true);
            }
            originCateId = 0;
        }).fail(function(data) {
            $('.category-select').prop('disabled', false);
            layer.msg('映射失败',{icon:0});
            $(".category-select option[value='" + originCateId + "']").prop('selected', true);
            originCateId = 0;
        });
    });
</script>
@stop
