@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<style type="text/css">
    /*.on-sale-items {
        display: none;
    }*/
</style>
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">搜索{{ $merchant }}淘宝商品</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group">
                            <div class="input-group-content">
                                <input type="text" class="form-control" id="key" name="search" value="" placeholder="请输入商品在淘宝名称">
                                <div class="form-control-line"></div>
                            </div>
                            <div class="input-group-btn">
                                <button class="btn btn-floating-action btn-default-bright" onclick="searchTaobaoProducts('{{ $merchant }}')"><i class="fa fa-search" id="search-sub"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-head">
                <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                    <li class="active"><a href="#">在售商品</a></li>
                </ul>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin on-sale-items">
                            <thead>
                                <tr>
                                    <th>商品主图</th>
                                    <th>商品编号</th>
                                    <th>商品名称</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-head">
                <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                    <li class="active"><a href="#">仓库商品</a></li>
                </ul>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin inventory-items">
                            <thead>
                                <tr>
                                    <th>商品主图</th>
                                    <th>商品编号</th>
                                    <th>商品名称</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // 显示条目
    function showItems(type) {
        $('.' + type + '-items').show();
    }
    // 隐藏条目
    function hideItems(type) {
        $('.' + type + '-items').hide();
    }
    // 清空条目
    function clearItems(type) {
        var selector = '.' + type + '-items tbody';
        $(selector).empty();
    }
    // 插入条目
    function appendItems(type, html) {
        var selector = '.' + type + '-items tbody';
        $(selector).append(html);
    }
    // 搜索淘宝在售商品
    function searchOnSale(merchant, str) {
        // 清空当前搜索条目
        clearItems('on-sale');
        $.ajax({
            type: 'POST',
            url: "{{ action('Admin\TaobaoSyncController@search') }}",
            data: {
                "provider": 'onSale',
                "merchant": merchant,
                'search': str
            },
        }).done(function(data) {
            if(data.status=='success'){
                searchData2Html('on-sale', data.data);
            }else{
                toastr.error(data.message, '失败!');
            }
        }).fail(function(data) {
            toastr.error('失败!');
        });
    }
    // 搜索淘宝仓库商品
    function searchInventory(merchant, str) {
        // 清空当前搜索条目
        clearItems('inventory');
        $.ajax({
            type: 'POST',
            url: "{{ action('Admin\TaobaoSyncController@search') }}",
            data: {
                "provider": 'inventory',
                "merchant": merchant,
                'search': str
            },
        }).done(function(data) {
            if(data.status=='success'){
                searchData2Html('inventory', data.data);
            }else{
                toastr.error(data.message, '失败!');
            }
        }).fail(function(data) {
            toastr.error('失败!');
        });
    }
    // 搜索淘宝商品
    function searchTaobaoProducts(merchant) {
        var search = $('input[name="search"]').val();
        if (search == '') {
            toastr.error('搜索数据不能为空');
            return;
        }
        searchOnSale(merchant, search);
        searchInventory(merchant, search);
    }
    // 拼接html片段
    function searchData2Html(type, data) {
        if (typeof data.items.items === 'undefined') {
            return;
        }
        if (typeof data.items.items.item === 'undefined') {
            return;
        }
        var items = data.items.items.item;
        var htmlStr = '';
        for (var i = 0; i < items.length; i++) {
            htmlStr += '<tr>';
            htmlStr += tdImgHtml(items[i].pic_url)
                + '<td>' + items[i].outer_id + '</td>'
                + '<td>' + items[i].title + '</td>'
                + tdButtonHtml(items[i].num_iid);
            htmlStr += '</tr>';
        }
        appendItems(type, htmlStr);
    }
    // 图片html片段拼接
    function tdImgHtml(url) {
        return '<td><img src="' + url + '" width="100px"></td>';
    }
    // 操作html片段拼接
    function tdButtonHtml(id) {
        return '<td><button data-num-iid="' + id + '" class="btn ink-reaction btn-primary btn-xs sync-taobao-product" type="button">同步此商品</button></td>';
    }
    $(document).on('click', '.sync-taobao-product', function() {
        var numIid = $(this).attr('data-num-iid'),
        merchant = '{{ $merchant }}';
        $.ajax({
            type: 'POST',
            url: "{{ action('Admin\TaobaoSyncController@syncSearch') }}",
            data: {
                "merchant": merchant,
                "numIid": numIid
            },
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('已加入商品同步队列，请稍等');
            }else{
                toastr.error(data.message);
            }
        }).fail(function(data) {
            toastr.error('网络请求失败!');
        });
    });
</script>
@stop
