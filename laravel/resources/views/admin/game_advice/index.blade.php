@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">用户游戏反馈列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
               <div class="card-head">
                    <div class="tools">
                        <div class="btn-group">
                            <a href="{!! action('Admin\UserGameController@index') !!}" class="btn ink-reaction btn-primary btn-sm pull-right">
                                游戏商品列表
                             </a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>用户昵称</th>
                            <th>用户头像</th>
                            <th>游戏类型</th>
                            <th>信息反馈状态</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($UserGameAdvices as $UserGameAdvice)
                        <tr>
                            <td>{{ $UserGameAdvice->user['id'] }}</td>
                            <td>{{ $UserGameAdvice->user['nickname'] }}</td>
                            <td>
                            @if(isset($UserGameAdvice->user['avatar']))
                            <img src="{{$UserGameAdvice->user['avatar']}}" width="50px">
                            @else
                            暂无图片
                            @endif
                            </td>
                            <td>{{$UserGameAdvice->provider}}</td>
                           <td>
                                @if($UserGameAdvice->status == 0)
                                    @php
                                        $btnType = 'text-danger';
                                        $btnName = '未处理';
                                    @endphp
                                @elseif($UserGameAdvice->status == 1)
                                    @php
                                     $btnType = 'text-primary';
                                     $btnName = '已处理,未采纳';
                                    @endphp
                                @elseif($UserGameAdvice->status == 2)
                                    @php
                                     $btnType = 'text-success';
                                     $btnName = '已采纳';
                                    @endphp
                                @endif
                                <span class=" {{$btnType}} " type="text" >{{$btnName}}</span>
                            </td>
                            <td>
                            <a href="{{ action('Admin\\UserGameAdviceController@details', ['adviceId' => $UserGameAdvice->id ]) }}" class="btn ink-reaction btn-success btn-xs" type="button">查看</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($UserGameAdvices->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$UserGameAdvices->toArray()['to'] - $UserGameAdvices->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$UserGameAdvices->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                    {!! $UserGameAdvices->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop
