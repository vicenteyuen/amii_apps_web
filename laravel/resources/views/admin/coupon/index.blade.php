@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">优惠券列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if ($coupons->isEmpty())
                            @include('admin.coupon.null')
                        @else
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <!-- <th>图片</th> -->
                                    <th>优惠券类型</th>
                                    <th>数量</th>
                                    <th>起步金额</th>
                                    <th>最多使用额</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($coupons as $coupon)
                                <tr>
                                    <td>{{ $coupon->id }}</td>
                                   {{--
                                    <td class="image-width-full" style="width: 192px;">
                                        @if ($coupon->image)
                                        <img src="{{ $coupon->image }}">
                                        @else
                                        无图
                                        @endif
                                    </td>
                                    --}}
                                    <td>{{ $coupon->provider }}</td>
                                    <td>{{ $coupon->number }}</td>
                                    <td>¥ {{ $coupon->lowest}}</td>
                                    <td>¥ {{ $coupon->value }}</td>
                                    <td>
                                        {!! \HtmlFuncs::statusAdmin($coupon->status) !!}
                                    </td>
                                    <td>
                                        @if($coupon->status == 1)
                                             @php
                                                 $btnType = 'btn-danger';
                                                 $btnName = '停用';
                                             @endphp
                                        @else
                                             @php
                                                 $btnType = 'btn-primary';
                                                 $btnName = '启用';
                                             @endphp
                                        @endif

                                        <a href="javascript:void(0);" class="btn ink-reaction  {{$btnType}} btn-xs js_coupon_status" type="button" data-status = "{{ $coupon->status }}" data-id = "{{ $coupon->id }}">{{$btnName}}</a>

                                        <a href="{{ action("Admin\\CouponController@show", ['id' => $coupon->id ]) }}" class="btn  btn-primary btn-xs " type="" > 查看券码</a>
                                            </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if($coupons->toArray()['data'])
                        <div class="col-md-12 col-lg-12">
                            <label style="color: black;font-size: 16px">当前页数量: {{$coupons->toArray()['to'] - $coupons->toArray()['from'] + 1}}</label>
                            <label style="color: black;font-size: 16px">总数量: {{$coupons->toArray()['total']}}</label>
                        </div>
                         @endif
                        <div class="text-center">
                            {!! $coupons->render() !!}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // 优惠卷停用
    $('.js_coupon_status').live('click', function(){
        couponStatus($(this));
    })
    // 优惠卷停用
    function couponStatus(_this)
    {
        var status = _this.attr('data-status');
        if(status == '1'){
            var success      = '停用成功';
            var error        = '停用失败';
            var updataStatus = 0;
        }else{
            var success      = '启用成功';
            var error        = '启用失败';
            var updataStatus = 1;
        }
        $.ajax({
            url: '{{ action("Admin\\CouponController@status") }}',
            type: 'POST',
            data: {
                id     : _this.attr('data-id'),
                status : updataStatus,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');
                    if(status == 1){
                        // 可启用状态
                        _this.parents('tr').find('i').addClass('text-red fa-close');
                        _this.parents('tr').find('i').removeClass('text-green fa-check');
                        _this.addClass('btn-primary');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',0);
                        _this.text('启用');
                    }else{
                        // 可停用状态
                        _this.parents('tr').find('i').addClass('text-green fa-check');
                        _this.parents('tr').find('i').removeClass('text-red fa-close');
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-primary');
                        _this.attr('data-status',1);
                        _this.text('停用');
                    }
                } else {
                    toastr.error(error, '失败');
                }
            }
        });

    };

</script>
@stop






