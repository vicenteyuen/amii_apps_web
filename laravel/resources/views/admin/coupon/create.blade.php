@extends('admin.widget.body')

@section('style_link')
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加优惠券</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    {!! Form::open(['action' => 'Admin\CouponController@store', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                        {{--
                        @include('admin.widget.image-upload', [
                            'colsm' => '12',
                            'collg' => '12',
                            'class' => 'puload-display-popular',
                            'id' => 'couponImg',
                            'imgId' => 'couponImgId',
                            'name' => 'image',
                            'descript' => '优惠券展示图片'
                        ])
                        --}}
                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id' => 'status',
                            'name' => 'status',
                            'title' => '启用状态',
                            'values' => [
                                '关闭' => '0',
                                '启用' => '1',
                            ]
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'title',
                            'id' => 'title',
                            'title' => '标题',
                            'type'  => 'text'
                        ])
                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id' => 'provider',
                            'name' => 'provider',
                            'title' => '选择优惠券类型',
                            'selectArr' => 1,
                            'values' => $providers
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'value',
                            'id' => 'value',
                            'title' => '面额',
                            'type'  => 'text'
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'discount',
                            'id' => 'discount',
                            'title' => '折扣',
                            'type'  => 'text'
                        ])
                        @include('admin.widget.datetimepicker', [
                            'colsm' => '12',
                            'collg' => '6',
                            'title' => '开始时间',
                            'name' => 'start',
                            'class' => 'startDate',
                        ])
                        @include('admin.widget.datetimepicker', [
                            'colsm' => '12',
                            'collg' => '6',
                            'title' => '结束时间',
                            'name' => 'end',
                            'class' => 'endDate',
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'number',
                            'id' => 'number',
                            'title' => '数量',
                            'type'  => 'text'
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'lowest',
                            'id' => 'lowest',
                            'title' => '起步金额',
                            'type'  => 'text'
                        ])
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group text-right">
                                <button type="submit" class='btn btn-success'>保存</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
@stop

@section('script')
<script type="text/javascript">
    $('#discount').hide();
    $('#provider').on('change', function() {
        if ($(this).val() == 3) {
            $('#discount').show();
        } else {
            $('#discount').val('').hide();
        }
    });
    Helper.setDefaultImg($('#couponImg~img'));
    $("#couponImg").change(function(){
        Helper.readURL(this, '#couponImgId');
    });
    $(".startDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
    $(".endDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
</script>
@stop
