@extends('admin.widget.body')

@section('content')
    <section>
        <div class="section-header">
            <ol class="breadcrumb">
                <li class="active">视频 添加/修改</li>
            </ol>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        @php
                            $video_exist = isset($video) ? true : false;
                        @endphp
                        @if($video_exist)
                            {!! Form::open(['action' => ['Admin\VideoController@update', $video->id], 'class' => 'form', 'id' => 'video-form', 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
                        @else
                            {!! Form::open(['action' => 'Admin\VideoController@store', 'class' => 'form', 'id' => 'video-form', 'enctype' => 'multipart/form-data']) !!}
                        @endif

                        @include('admin.widget.image-upload', [
                            'colsm' => '6',
                            'collg' => '4',
                            'id'    => 'video-image',
                            'name'  => 'image',
                            'defaultImg' => old('image') ? old('image') : '',
                            'descript'=>'尺寸: 360*240',
                            'style'=>'width:360px;height:240px;',

                        ])

                        @include('admin.widget.input', [
                            'name' => 'editIden',
                            'id'   => 'editIden',
                            'type' => 'hidden'
                        ])

                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name'  => 'title',
                            'id'    => 'title',
                            'title' => '标题',
                            'type'  => 'text',
                            'value' => old('title') ? old('title') : ($video_exist ? $video->title : '')
                        ])
                        @include('admin.widget.textarea', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'description',
                            'title' => '描述',
                            'value' => old('description') ? old('description') : ($video_exist ? $video->description : '')
                        ])

                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '8',
                            'name'  => 'weight',
                            'title' => '排序权重（降序）',
                            'type'  => 'text',
                            'value' => old('weight') ? old('weight') : (isset($video->weight)? $video->weight : '0')
                        ])

                        <div class="col-xs-12 col-sm-12 col-md-12" id="upload-container">
                            <div class="form-group">
                            <label>
                                @if (isset($video))
                                    更换视频
                                @else
                                    上传视频
                                @endif
                            </label><br>
                            <div id="fileupload-container">
                                <a id="pickfiles" href="javascript:;" class="btn btn-sm btn-primary add-operation">选择文件</a>
                                <a id="uploadfiles" href="javascript:;" class="btn btn-sm btn-primary add-operation">上传文件</a>
                            </div>
                            <a id="deletefiles" href="javascript:;" class="btn btn-sm btn-danger delete-operation hide">删除文件</a>
                            <br>
                            <span id="filelist">你的浏览器或浏览器版本不支持flash, silverlight 或HTML5，无法上传</span>
                            <div id="upload-proccess"></div>
                            <br>
                            </div>
                        </div>
                        @if (isset($video))
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label>视频</label><br>
                                <video src="{{ $video->video }}" controls="controls" poster="{{ $video->image }}" height="300px">您的浏览器不支持 video 标签。</video>
                            </div>
                        </div>
                        @endif


                        <input type="hidden" class="hide" value="{{ $video->original_video or null }}" name="video">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group text-right">
                            <button type="submit" class='btn btn-success'>保存</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </section>
@stop

@section('script_link')
    <script src="/assets/lib/plupload/plupload.full.min.js"></script>
    <script src="/assets/lib/plupload/i18n/zh_CN.js"></script>
@stop

@section('script')
    <script type="text/javascript">
        //上传主图
        @if(isset($video))
            Helper.setDefaultImg($('#uploadImg'),"{{$video->image}}");
        @else
            Helper.setDefaultImg($('#uploadImg'));
        @endif
        $("#video-image").change(function(){
            Helper.readURL(this);
            $('#editIden').val('-1');
        });

        $(document).ready(function() {
            var uploader = new plupload.Uploader({
                runtimes : 'html5,browserplus,silverlight,flash,gears,html4',
                browse_button : 'pickfiles',
                container: 'fileupload-container',
                multi_selection: false,
                chunk_size: '1mb',
                unique_names: true,
                url: "{{ action('Admin\VideoController@upload') }}",
                flash_swf_url : '/assets/lib/plupload/Moxie.swf',
                silverlight_xap_url : '/assets/lib/plupload/Moxie.xap',
                filters : [
                    {
                        title : "Video files",
                        extensions : "mpg,m4v,mp4,flv,3gp,mov,avi,rmvb,mkv,wmv"
                    }
                ],

                init: {
                    PostInit: function() {
                        $('#filelist').html('');

                        $('#uploadfiles').click(function() {
                            uploader.start();
                            return false;
                        });
                    },

                    FilesAdded: function(up, files) {
                        $.each(up.files, function (i, file) {
                            if (up.files.length <= 1) {
                                return;
                            }
                            up.removeFile(file);
                        });
                        plupload.each(files, function(file) {
                            $('#filelist').html('<div>' + file.name + ' (' + plupload.formatSize(file.size) + ')</div>');
                        });
                    },

                    UploadProgress: function(up, file) {
                        $('#upload-proccess').html('<div style="height:20px;"><div style="border-radius:2px;background-color:#5cb85c;height: 100%;width:' + file.percent +'%"></div></div>');
                    },

                    Error: function(up, err) {
                        console.log('错误码: ' + err.code + ', 错误信息: ' + err.message);
                    },

                    FileUploaded: function (uploader, files, result) {
                        var response = JSON.parse(result.response);
                        if (response.status == 'error') {
                            layer.msg(response.message);
                        } else{
                            $('#fileupload-container').hide();
                            $('.delete-operation').removeClass('hide');
                            $('input[name="video"]').val(response.data.file_name);
                            layer.msg('上传成功');
                        }
                    }
                }
            });

            uploader.init();
        });

        $('.delete-operation').click(function () {
            $(this).addClass('hide');
            $('#fileupload-container').show();
            $('#filelist').html('');
            $('#upload-proccess').html('');
            $('input[name="video"]').val('');
        })
    </script>
@stop
