@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">用户评论内容</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-head text-right">
                <div class="col-md-12">
                <p></p>
                <div class="btn-group">
                    @if($commentDetail['comment_status'] == 0)
                         @php
                             $btnType = 'btn-danger';
                             $btnName = '屏蔽';
                         @endphp
                    @else
                         @php
                             $btnType = 'btn-primary';
                             $btnName = '开放';
                         @endphp
                    @endif

                    <a href="javascript:void(0);" class="btn  ink-reaction  {{$btnType}} btn-md js_comment_status" type="button" data-status = "{{ $commentDetail['comment_status'] }}" data-id = "{{ $commentDetail['id'] }}">{{$btnName}}</a>
                     <header></header>
                </div>
                </div>
            </div><!--end .card-head -->
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                        <div class="tab-content clearfix">

                           <div class="row">
                                @include('admin.widget.textarea', [
                                    'colsm'    => '48',
                                    'collg'    => '12',
                                    'name'     => 'content',
                                    'id'       => 'content',
                                    'title'    => '评论内容' ,
                                    'type'     => 'text',
                                    'readonly' => 'readonly',
                                    'value'    => isset($commentDetail) && $commentDetail['content'] ? $commentDetail['content'] : '暂无评论内容'
                                ])

                                <div class="col-md-12">
                                    <div class="form-group floating-label clearfix">
                                        <label for="commentImage">评论图片</label>
                                        @if ( isset($commentImages) && count($commentImages) > 0)
                                            <ul class="list-inline" name = 'commentImage'>
                                                @foreach ($commentImages as $commentImage)
                                                    <li class = "next image-width-full" >
                                                     <img class="img-rounded" style = "width: 192px" src="{{ $commentImage->image }}">
                                                </li>
                                                @endforeach
                                            </ul>
                                        @else
                                           <p name="commentImage">暂无评论图片</p>
                                        @endif

                                    </div>
                                </div>

                            </div>
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next">
                                    <a href="#" onClick="javascript :history.back(-1)"; class="btn ink-reaction btn-primary btn-md" type="button" >返回</a>
                               </li>
                            </ul>
                            </div>
                        </div>

                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //用户评论屏蔽开放
    $('.js_comment_status').live('click', function(){
        commentStatus($(this));
    })
    // 用户评论屏蔽开放
    function commentStatus(_this)
    {
        var status = _this.attr('data-status');
        if(status == '0'){
            var success      = '屏蔽成功';
            var error        = '屏蔽失败';
            var updateStatus = 1;
        }else{
            var success      = '开放成功';
            var error        = '开放失败';
            var updateStatus = 0;
        }
        $.ajax({
            url: '{{ action("Admin\\UserCommentController@status") }}',
            type: 'POST',
            data: {
                id     : _this.attr('data-id'),
                status : updateStatus,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');

                    if(status == 0){
                        // 可启用状态
                        _this.addClass('btn-primary');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',1);
                        _this.text('开放');
                    }else{
                        // 可停用状态
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-primary');
                        _this.attr('data-status',0);
                        _this.text('屏蔽');
                    }
                } else {
                    toastr.error(error, '失败');
                }
            }
        });

    };
</script>
@stop


