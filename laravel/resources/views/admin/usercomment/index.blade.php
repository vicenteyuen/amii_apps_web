@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">商品评论列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <div class="card-body">
                 <form action="{{action('Admin\UserCommentController@index')}}" method="GET">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                                   @if(isset($_GET['rank']))
                                        @if($_GET['rank'] == '1')
                                            一星
                                        @elseif($_GET['rank'] == '2')
                                            二星
                                        @elseif($_GET['rank'] == '3')
                                            三星
                                        @elseif($_GET['rank'] == '4')
                                            四星
                                        @elseif($_GET['rank'] == '5')
                                            五星
                                        @endif
                                  @else
                                   全部
                                  @endif
                                <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                         <li><a href="{{action('Admin\UserCommentController@index',['product_id' => $product_id, 'provider' => $provider])}}">
                                            全部
                                         </a></li>
                                    @foreach($ranks as $rank)
                                         <li><a href="{{action('Admin\UserCommentController@index',['product_id' => $product_id, 'rank' => $rank->rank,  'provider' => $provider])}}">{{$rank->desc}}</a></li>
                                    @endforeach
                                  </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">

                            <div class="input-group pull-right">
                               <form method="GET" action="" accept-charset="UTF-8" id="search-form">
                                   <div class="input-group input-group-lg pull-left" style="width: 600px;">
                                       <div class="input-group-content" >
                                            <input type="text" placeholder="请输入商品编号" id="search" name="search" class="form-control" value="" style="text-indent: 10px;width:600px; font-size: 16px;">
                                           <div class="form-control-line"></div>
                                            @include('admin.widget.datetimepicker', [
                                                'colsm' => '6',
                                                'collg' => '6',
                                                'title' => '开始时间',
                                                'name' => 'start',
                                                'class' => 'startDate',
                                                'style' => 'height:46px',
                                                'value' => old('start') ? old('start') : ''
                                            ])
                                            @include('admin.widget.datetimepicker', [
                                                'colsm' => '6',
                                                'collg' => '6',
                                                'title' => '结束时间',
                                                'name' => 'end',
                                                'class' => 'endDate',
                                                'style' => 'height:46px',
                                                'value' => old('end') ? old('end') : ''
                                            ])
                                       </div>
                                       <div class="input-group-btn">
                                           <button type="submit" class="btn btn-floating-action btn-default-bright"><i class="fa fa-search"></i></button>
                                       </div>
                                   </div>
                               </form>
                            </div>

                        </div>
                    </div>
                </form>
                </div>

                <div class="card-body">
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                            <tr>
                                <th>商品编号</th>
                                <th>评论内容</th>
                                <th>评论时间</th>
                                <th>星级</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($commentLists as $commentList)

                             <tr>
                                <td>{{ $commentList['sellProduct']['product']['snumber']}}</td>

                                <td>
                                    {{ str_limit( $commentList['content'], 60)  }}
                                </td>
                                <td>{{ $commentList['created_at'] }}</td>
                                <td>
                                    @foreach ($ranks as $rank)
                                        @if ($commentList['comment_rank_id'] == $rank->rank)
                                         {{ $rank->desc }}
                                        @endif
                                    @endforeach
                                </td>
                                <td>

                                <a href="{!! action('Admin\\UserCommentController@showDetail', ['sell_product_id' => $commentList['sellProduct']['id'],
                                'comment_id' => $commentList['id']]) !!}" class="btn ink-reaction btn-success btn-xs" type="button">查看评论详情</a>

                                 @if($commentList['comment_status'] == 0)
                                       @php
                                           $btnType = 'btn-danger';
                                           $btnName = '屏蔽';
                                       @endphp
                                  @else
                                       @php
                                           $btnType = 'btn-primary';
                                           $btnName = '开放';
                                       @endphp
                                  @endif

                                  <a href="javascript:void(0);" class="btn ink-reaction  {{$btnType}} btn-xs js_comment_status" type="button" data-status = "{{ $commentList['comment_status'] }}" data-id = "{{ $commentList['id'] }}">{{$btnName}}</a>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                 <div class="text-center">
                    @if ($provider && $ranks)
                        {!! $commentLists->appends(['product_id' => $product_id,
                                'provider' => $provider, 'rank' => $comment_rank_id])->render() !!}
                    @elseif ($ranks)
                        {!! $commentLists->appends(['rank' => $comment_rank_id])->render() !!}
                    @else
                        {!! $commentLists->render() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript">
    //用户评论屏蔽开放
    $('.js_comment_status').live('click', function(){
        commentStatus($(this));
    })
    // 用户评论屏蔽开放
    function commentStatus(_this)
    {
        var status = _this.attr('data-status');
        if(status == '0'){
            var success      = '屏蔽成功';
            var error        = '屏蔽失败';
            var updateStatus = 1;
        }else{
            var success      = '开放成功';
            var error        = '开放失败';
            var updateStatus = 0;
        }
        $.ajax({
            url: '{{ action("Admin\\UserCommentController@status") }}',
            type: 'POST',
            data: {
                id     : _this.attr('data-id'),
                status : updateStatus,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');

                    if(status == 0){
                        // 可启用状态
                        _this.addClass('btn-primary');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',1);
                        _this.text('开放');
                    }else{
                        // 可停用状态
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-primary');
                        _this.attr('data-status',0);
                        _this.text('屏蔽');
                    }
                } else {
                    toastr.error(error, '失败');
                }
            }
        });

    };

    $(".startDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
    $(".endDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
</script>
@stop
