@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active"> 物流消息列表</li>
        </ol>
    </div>
    <div class="section-body">

        <div class="card">
            <div class="card-head">
                <header></header>
                <div class="tools">
                    <div class="btn-group">
                        <form method="GET" action="" accept-charset="UTF-8" id="search-form">
                            <div class="input-group input-group-lg pull-right" style="width: 240px;">
                                <div class="input-group-content" >
                                    <input type="text" placeholder="请输入姓名或手机号码" id="search" name="search" class="form-control" value="" style="width:220px">
                                    <div class="form-control-line"></div>
                                </div>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-floating-action btn-default-bright"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form> <!-- ./form -->
                    </div>
                </div>
            </div><!--end .card-head -->
            <div class="card-body">

                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th >id</th>
                            <th >姓名</th>
                            <th >手机号码</th>
                            <th >消息标题</th>
                            <!-- <th >消息类型</th> -->
                            <th >消息内容</th>
<!--                             <th >跳转类型</th>
                            <th >跳转资源标识</th> -->
                            <th >消息状态</th>
                            <th >创建时间</th>
                            <th >操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($notifymessages as $notifymessage)

                        <tr>
                            <td>{{ $notifymessage['id'] }}</td>
                            <td>{{ $notifymessage['user']['realname'] }}</td>
                            <td>{{ $notifymessage['user']['phone'] }}</td>
                            <td>{{ $notifymessage['title'] }}</td>
                            <td>{{ $notifymessage['content'] }}</td>
                            <td>
                                @if ($notifymessage['status'] == 0)
                                    未读
                                @elseif ($notifymessage['status'] == 1)
                                    已读
                                @else
                                    无效
                                @endif
                            </td>
                            <td>{{ $notifymessage['created_at'] }}</td>
                            <td>
                                <a href="{{ action('Admin\NotifyMessageController@showDetail', ['id' => $notifymessage['id'] ])}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">查看</a>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @if($notifymessages->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$notifymessages->toArray()['to'] - $notifymessages->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$notifymessages->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                      {!! $notifymessages->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var _token = $('#_token').val();

</script>
@stop
