@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2-bootstrap.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">

@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">系统消息详情</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="formNotifyMessage"  method="POST">
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">

                        <div class="tab-content clearfix">

                           <div class="row">

                                @include('admin.widget.input', [
                                    'colsm'    => '12',
                                    'collg'    => '12',
                                    'name'     => 'title',
                                    'id'       => 'title',
                                    'title'    => '消息标题' ,
                                    'type'     => 'text',
                                    'readonly' => 'readonly',
                                    'value'    => isset($notifymessage->title) ? $notifymessage->title : ''
                                ])

                                @include('admin.widget.textarea', [
                                    'colsm' => '12',
                                    'collg' => '6',
                                    'name'  => 'content',
                                    'id'    => 'content',
                                    'title' => '系统消息内容',
                                    'placeholder' => '点击此处编辑自定义消息内容',
                                    'attribute'   => 'attributeValue',
                                    'readonly' => 'readonly',
                                    'value' => isset($notifymessage->content) ? $notifymessage->content : ''
                                ])

                            </div>
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">

                                <li class="next">
                                <a href="{!! action('Admin\\NotifyMessageController@notifyIndex') !!}" class="btn ink-reaction btn-primary btn-md" type="button" >返回</a>

                               </li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop

@section('script')
<script type="text/javascript">

    var url  = "{!! action('Admin\\NotifyMessageController@notifyPlush') !!}";
    var message  = '发送成功';

    // wizard on tab next 提交表单
     $('#formNotifyMessage').ajaxForm({
        url: url,
        type: 'POST',
        dataType: 'json',
        async: false,
        success: function (returnData) {
            if (returnData.status == 'success') {

                layer.msg(message, {icon: 1});
                setTimeout(function(){
                    // 返回上一页
                    window.location.reload();
                },1000)
            }else{
                layer.msg(returnData.message,{icon: 0});
            }
        }
    });

</script>
@stop
