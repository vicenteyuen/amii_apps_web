@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active"> 系统消息列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="card-head">
                    <div class="tools">
                        <div class="btn-group">
                            <a href="{!! action('Admin\NotifyMessageController@editNotify') !!}" class="btn ink-reaction btn-default btn-sm pull-right">
                                新建
                            </a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th >id</th>
                            <th >消息标题</th>
                            <th >消息内容</th>
                            <th >创建时间</th>
                            <th >操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($notifymessages as $notifymessage)
                        <tr>
                            <td>{{ $notifymessage->id }}</td>
                            <td>{{ $notifymessage->title }}</td>
                            <td>{{ $notifymessage->content }}</td>
                            <td>{{ $notifymessage->created_at }}</td>
                            <td>
                                <a href="{{ action('Admin\NotifyMessageController@showNotify', ['id' => $notifymessage->id ])}}" class="btn ink-reaction btn-flat btn-primary btn-xs" type="button">查看</a>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>

                </table>
                @if($notifymessages->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$notifymessages->toArray()['to'] - $notifymessages->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$notifymessages->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                      {!! $notifymessages->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var _token = $('#_token').val();

</script>
@stop
