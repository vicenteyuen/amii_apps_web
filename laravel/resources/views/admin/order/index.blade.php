@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">订单列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <div class="card-body">
                <form action="{{action('Admin\OrderController@index')}}" method="GET">
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                           <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                                  @if(isset($_GET['status']))
                                    @if($_GET['status'] == 'all')
                                        全部
                                    @elseif($_GET['status'] == 'paying')
                                        待支付
                                    @elseif($_GET['status'] == 'sending')
                                        待发货
                                    @elseif($_GET['status'] == 'receiving')
                                        待收货
                                    @elseif($_GET['status'] == 'appraising')
                                        待评价
                                    @elseif($_GET['status'] == 'refunding')
                                        售后中
                                    @elseif($_GET['status'] == 'cancel')
                                        交易关闭
                                    @elseif($_GET['status'] == 'success')
                                        交易完成
                                    @endif
                                  @else
                                    筛选订单
                                  @endif
                                <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @foreach($status as $orderStatus)
                                         <li><a href="{{action('Admin\OrderController@index',['status' => $orderStatus,'provider' =>$request->provider])}}">{{\App\Helpers\OrderHelper::statusStr($orderStatus)}}</a></li>
                                    @endforeach
                                  </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                           <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                                  @if(isset($_GET['provider']))
                                    @if($_GET['provider'] == 'standard')
                                        普通订单
                                    @elseif($_GET['provider'] == 'point')
                                        积分订单
                                    @elseif($_GET['provider'] == 'game')
                                        游戏订单
                                    @endif
                                  @else
                                    订单类型
                                  @endif
                                <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @foreach($providers as $provider)
                                         <li><a href="{{action('Admin\OrderController@index',['status' => $request->status,'provider' =>$provider])}}">
                                         @if($provider == 'standard')
                                            普通订单
                                         @elseif($provider == 'point')
                                            积分订单
                                         @elseif($provider == 'game')
                                            游戏订单
                                         @endif
                                         </a></li>
                                    @endforeach
                                  </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group">
                                <div class="input-group-content">
                                    <input type="text" class="form-control" id="key" name="search" value="" placeholder="请输入订单号或手机号码" style="text-indent: 10px;">
                                    <div class="form-control-line"></div>
                                      @include('admin.widget.datetimepicker', [
                                                'colsm' => '6',
                                                'collg' => '6',
                                                'title' => '开始时间',
                                                'name' => 'start',
                                                'class' => 'startDate',
                                                'style' => 'height:46px',
                                                'value' => old('start') ? old('start') : ''
                                            ])
                                      @include('admin.widget.datetimepicker', [
                                            'colsm' => '6',
                                            'collg' => '6',
                                            'title' => '结束时间',
                                            'name' => 'end',
                                            'class' => 'endDate',
                                            'style' => 'height:46px',
                                            'value' => old('end') ? old('end') : ''
                                      ])
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-floating-action btn-default-bright submit" type="submit" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                </div>

                <div class="card-body">
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                            <tr>
                                <th>订单号</th>
                                <th>订单类型</th>
                                <th>订单状态</th>
                                <th>购买者</th>
                                <th>下单时间</th>
                                <th>支付完成时间</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->order_sn}}</td>
                                <td>
                                    @if($order->provider == 'standard')
                                        普通订单
                                    @elseif($order->provider == 'point')
                                        积分订单
                                    @elseif($order->provider == 'game')
                                        游戏订单
                                    @endif
                                </td>
                                <td> {{\App\Helpers\OrderHelper::statusStr($order->status_str)}}
                                </td>
                                <td>{{ $order->user['phone'] }}</td>
                                <td>{{ $order->created_at }}</td>
                                <td>
                                    {{ $order->pay_end_time }}
                                </td>
                                <td>

                                 @if($order->status_str == 'receiving')
                                    @if($order->can_refund == 1)
                                       <a href="{{ action('Admin\OrderController@canRefund', ['orderSn' => $order->order_sn, 'can_refund' => 'false' ])}}" class="btn ink-reaction btn-danger btn-xs" type="button" disable="disable">关闭售后入口</a>
                                    @else
                                        @if($order->provider != 'game')
                                        <a href="{{ action('Admin\OrderController@canRefund', ['orderSn' => $order->order_sn, 'can_refund' => 'true'])}}" class="btn ink-reaction btn-success btn-xs" type="button">开启售后</a>
                                        @endif
                                    @endif

                                 @endif
                                 <a href="{{ action('Admin\OrderController@show', ['orderSn' => $order->order_sn])}}" class="btn ink-reaction btn-success btn-xs" type="button">详情</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                 @if($orders->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$orders->toArray()['to'] - $orders->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$orders->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                    {!! $orders->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript">

    $(".startDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
    $(".endDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
</script>
@stop
