@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.image.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">订单详情</li>
        </ol>
    </div>
    <div class="section-body contain-lg">

        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i> 订单信息</header>
                    </div>
                    <div class="card-body">
                        <dl class="dl-horizontal">
                            <dt>订单号</dt>
                            <dd>{{ $order->order_sn }}</dd>
                            <dt>订单状态</dt>
                            <dd>{{\App\Helpers\OrderHelper::statusStr($order->status_str)}}</dd>
                            <dt>订单类型</dt>
                            <dd> @if($order->provider == 'standard')
                                    普通订单
                                @elseif($order->provider == 'point')
                                    积分订单
                                @elseif($order->provider == 'game')
                                    游戏订单
                                @else
                                    其他订单
                                @endif
                            </dd>
                            <dt>运费</dt>
                            <dd>￥{{ $order->shipping_fee or '0.00' }}</dd>
                            <dt>优惠</dt>
                            <dd>￥{{$order->discount or '0.00' }}</dd>
                            <dt>vip优惠</dt>
                            <dd>￥{{$order->vip_discount or '0.00' }}</dd>
                            <dt>优惠券优惠</dt>
                            <dd>￥{{$order->coupon_discount or '0.00' }}</dd>
                            <dt>余额抵现</dt>
                            <dd>￥{{$order->balance_discount or '0.00' }}</dd>
                            <dt>积分抵扣</dt>
                            <dd>￥{{$order->point_discount or '0.00' }}</dd>
                            <dt>商品总额</dt>
                            <dd>￥{{$order->product_amount}}
                            @if($order->provider == 'standard')
                            @endif
                            </dd>
                            <dt>实付金额</dt>
                            <dd>￥{{ $order->order_fee }}
                            @if($order->provider == 'standard')
                            @endif
                            </dd>
                            <dt>下单时间</dt>
                            <dd>{{ $order->created_at}}</dd>
                            <dt>支付完成时间</dt>
                            <dd>{{ $order->pay_end_time }}</dd>
                            <dt>购买者</dt>
                            <dd>{{ ($order->shipping_address)['name']}}</dd>
                            <dt>手机</dt>
                            <dd>{{ ($order->shipping_address)['phone']}}</dd>
                            <dt>快递地址</dt>
                            <dd>{{ ($order->shipping_address)['address']}}</dd>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i>支付信息</header>
                    </div>
                    <div class="card-body">
                      @foreach($order->payments as $payment)
                        <dl class="dl-horizontal">
                                <dt>支付状态</dt>
                                <dd>
                                    @if($payment->status == 0)
                                        未支付
                                    @elseif($payment->status ==1)
                                        支付完成
                                    @elseif($payment->status ==2)
                                        支付失败
                                    @else
                                        未知
                                    @endif
                                </dd>
                                <dt>支付类型</dt>
                                <dd>
                                    @if($payment->provider == 0)
                                        支付宝
                                    @elseif($payment->provider =='alipay')
                                        微信支付
                                    @elseif($payment->provider =='wechat')
                                        小程序支付
                                    @elseif($payment->provider =='weapp')
                                        支付完成
                                    @elseif($payment->provider =='weweb')
                                        微信网页支付
                                    @else
                                        未知
                                    @endif
                                </dd>
                                <dt>支付交易订单号</dt>
                                <dd>
                                    {{$payment->trade_no ?:'暂无'}}
                                </dd>
                                <dt>付款金额</dt>
                                <dd>
                                    ￥{{$payment->total_fee}}
                                </dd>
                                <dt>订单名称</dt>
                                <dd>
                                    {{$payment->subject ?:'暂无'}}
                                </dd>
                                <dt>支付完成时间</dt>
                                <dd>
                                     {{$payment->paid_time or '暂无'}}
                                </dd>
                                <dt>支付退款记录</dt>
                                <dd>
                                  @if($payment->refund == 0)
                                    未退款
                                  @elseif($payment->refund == 1)
                                    已退款
                                  @else
                                    未知
                                  @endif
                                </dd>
                                <dt>退款时间</dt>
                                <dd>
                                    {{$payment->refund_time or '暂无'}}
                                </dd>
                        </dl>
                      @endforeach
                    </div>

                </div>
            </div>

            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i> 商品信息</header>
                    </div>
                    <div class="card-body">
                            @foreach($order->orderProducts as $orderProduct)
                                <dl class="dl-horizontal">
                                    <dt>商品名</dt>
                                    <dd>{{ $orderProduct->sellProduct->product->name}}</dd>
                                    <dt>商品图片</dt>
                                    <dd>
                                        <div class="img-container">
                                            <img class="img" src="{{$orderProduct->sellProduct->all_image[0]}}">
                                        </div>
                                    </dd>
                                    <dt>商品属性</dt>
                                    <dd>
                                        @foreach($orderProduct->inventory->sku as $sku)
                                            <span>{{$sku->skuValue->attribute->name}}</span>
                                            <span> {{$sku->skuValue->value}}</span>
                                        @endforeach
                                    </dd>
                                    @if($orderProduct->sellProduct->provider == 2)
                                        <dt>商品积分</dt>
                                         <dd>{{$orderProduct->inventory->mark_price * 100}}</dd>
                                    @else
                                         <dt>商品价格</dt>
                                        <dd>
                                            ￥{{$orderProduct->inventory->mark_price}}
                                        </dd>
                                    @endif
                                    <dt>商品数量</dt>
                                    <dd>{{$orderProduct->number}}件</dd>
                                <!-- 售后 -->
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row text-left ">
                                            <ul class="list-unstyled">
                                                <li class="next text-right">
                                                @if($order->status_str == 'receiving')
                                                    @if($order->can_refund == 1)
                                                       <a href="{{ action('Admin\OrderController@canRefund', ['orderSn' => $order->order_sn, 'can_refund' => 'false' ])}}" class="btn ink-reaction btn-danger btn-xs" type="button" disable="disable">关闭售后入口</a>
                                                    @else
                                                        @if($order->provider != 'game')
                                                        <a href="{{ action('Admin\OrderController@canRefund', ['orderSn' => $order->order_sn, 'can_refund' => 'true'])}}" class="btn ink-reaction btn-success btn-xs" type="button">开启售后</a>
                                                        @endif
                                                    @endif

                                                @endif
                                                </li>
                                               <li class="next"><h3 class="text-primary text-md" style="margin-left: 80px;color: black" >售后详情</h3></li>
                                            </ul>
                                        </div>
                                        <div class="row js-after-sell-{{$orderProduct->sell_product_id}}" style="" >
                                            @if(isset($orderProduct->refund))
                                                <dl class="dl-horizontal">
                                                  <dt>申请时间</dt>
                                                  <dd>{{$orderProduct->refund->created_at}}</dd>
                                                  <dt>申请类型</dt>
                                                      @if($orderProduct->refund->provider == 'pay')
                                                         <dd>退款</dd>
                                                      @else
                                                        <dd>退货退款</dd>
                                                      @endif
                                                  <dt>申请状态</dt>
                                                     @if($orderProduct->refund->status == 0)
                                                        <dd>申请中</dd>
                                                        <dt>@if($order->provider == 'standard')
                                                            申请退款金额
                                                            @else
                                                            申请退积分数
                                                            @endif
                                                        </dt>

                                                        <dd>
                                                        @if($order->provider == 'standard')
                                                            {{$orderProduct->refund->pay}}
                                                        @else
                                                            {{(int)$orderProduct->refund->pay}}
                                                        @endif
                                                        </dd>

                                                        <dt>
                                                            @if($order->provider == 'standard')
                                                            最高可退金额
                                                            @else
                                                            最高可退积分
                                                            @endif
                                                        </dt>
                                                        <dd>
                                                            @if($order->provider == 'standard')
                                                                {{$orderProduct->refund->pay}}
                                                            @else
                                                                {{(int)$orderProduct->refund->pay}}
                                                            @endif
                                                        </dd>

                                                        <dt>图片</dt>
                                                        <dd>
                                                            @if(isset($orderProduct->refund->logs[0]->images) && !empty($orderProduct->refund->logs[0]->images))
                                                                @foreach($orderProduct->refund->logs as $log)
                                                                    @foreach($log->images as $image)
                                                                    <div class="resizable">
                                                                      <img class="img" src="{{$image['value']}}">
                                                                    </div>
                                                                    @endforeach
                                                                @endforeach
                                                            @else
                                                                暂无
                                                            @endif
                                                        </dd>

                                                        <!-- 申请中 -->
                                                            @if($orderProduct->refund->refund_status == 0)
                                                            <div class="" style="margin-left: 80px;margin-top: 10px">
                                                                <button style="" data-id='{{$orderProduct->id}}'  class="btn ink-reaction btn-primary btn-xs js-agress">同意</button>
                                                                <!-- 退货退款 -->
                                                                 @if($orderProduct->refund->provider == 'product')
                                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                        <div class="form-group">
                                                                            <input type='text' name="content" id="js-content" class="form-control"  value="">
                                                                            <label for="js-content" class="control-label" style="top: -10px">退货地址</label>
                                                                        </div>
                                                                    </div>
                                                                 @endif
                                                                 @if($orderProduct->refund->provider == 'product')
                                                                <button data-id="{{$orderProduct->refund->id}}" style="display: none;margin-top: 20px" class="btn ink-reaction btn-primary btn-xs js-received">确认收货</button>
                                                                 @endif
                                                            </div>

                                                            <div class="row js-refuse-reason" style="margin-left: 80px;margin-top: 40px">
                                                                <button class="btn ink-reaction btn-primary text-left btn-xs js-refuse" data-id="{{$orderProduct->refund->id}}"  >拒绝</button>


                                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                    <div class="form-group">
                                                                        <input type='text' name="refuse" id="js-reason" class="form-control"  value="">
                                                                        <label for="js-reason" class="control-label" style="top: -10px">拒绝原因</label>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            @endif
                                                        <!-- 已同意 -->
                                                            @if($orderProduct->refund->refund_status == 1)
                                                            <dt>商家</dt>
                                                            <dd>已同意</dd>
                                                                @if($orderProduct->refund->provider == 'product')
                                                                <button data-id="{{$orderProduct->refund->id}}"  class="btn ink-reaction btn-primary btn-xs js-received" style="margin-left: 100px">确认收货</button>
                                                                @endif
                                                            @endif
                                                     @elseif($orderProduct->refund->status == 1)
                                                        <dd>完成</dd>
                                                     @elseif($orderProduct->refund->status == 2)
                                                        <dd>已拒绝</dd>
                                                     @endif
                                                    <!-- 售后记录-->
                                                    @if($orderProduct->refund->status == 1 || $orderProduct->refund->status==2 )
                                                        <dt>@if($order->provider == 'standard')
                                                            申请退款金额
                                                            @else
                                                            申请退积分数
                                                            @endif
                                                        </dt>
                                                        <dd>{{$orderProduct->refund->pay}}</dd>
                                                        <dt>图片</dt>
                                                        <dd>
                                                            @if(isset($orderProduct->refund->logs[0]->images) && !empty($orderProduct->refund->logs[0]->images))
                                                                @foreach($orderProduct->refund->logs as $log)
                                                                    @foreach($log->images as $image)
                                                                    <div class="resizable">
                                                                      <img class="img" src="{{$image['value']}}">
                                                                    </div>
                                                                    @endforeach
                                                                @endforeach
                                                            @else
                                                                暂无
                                                            @endif
                                                        </dd>

                                                    @endif
                                            @else
                                                <dt>售后状态</dt>
                                                @if ($order->can_refund == 0 && $order->shipping_status == 1)
                                                    <dd>售后入口已关闭</dd>
                                                @else
                                                    <dd>暂无售后申请</dd>
                                                @endif
                                            @endif

                                            @if(isset($orderProduct->refund->logs))
                                            <dt>售后记录</dt>
                                                <dd>
                                                    <table class="table no-margin">
                                                    <thead>
                                                        <tr>
                                                            <th>标题</th>
                                                            <th>内容</th>
                                                            <th>创建时间</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($orderProduct->refund->logs as $log)
                                                        <tr>
                                                            <td>{{$log->title}}</td>
                                                            <td>{{$log->content}}</td>
                                                            <td>{{$log->created_at}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                    </table>
                                                </dd>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                </dl>
                            @endforeach
                    </div>

                </div>
            </div>
        </div>
        @if($order->status_str == 'sending')
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i>订单发货</header>
                    </div>
                    <div class="card-body">
                    <form class="form floating-label" action="{{action('Admin\OrderController@send')}}" method="POST">
                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id'    => 'express_id',
                            'name'  => 'express_id',
                            'title' => '请选择快递公司',
                            'values' => $expressIndex,
                        ])
                         @include('admin.widget.input', ['name' => 'shipping_sn','title' => '快递单号'])
                         <input name="orderSn" hidden="hidden" value="{{$orderSn}}">
                         <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md">发货</button></li>
                            </ul>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endif

        @if(isset($express->info))
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i>物流信息</header>
                    </div>
                    <div class="card-body">
                                <span style="font-size: 16px;font-weight: 500">快递公司</span>
                                <span>{{$express['info']['com']}}</span>
                                <span style="font-size: 16px;font-weight: 500;margin-left: 20px">订单号</span>
                                <span>{{$express['info']['nu']}}</span>
                        @foreach($express['info']['data'] as $value)
                            <dl class="dl-horizontal">
                                <dt>内容</dt>
                                <dd>{{ is_array($value['context']) ? $value['context']['date']:$value['context']  }}</dd>
                                <dt>时间</dt>
                                <dd>{{ is_array($value['time']) ? $value['time']['date']:$value['time']  }}</dd>
                            </dl>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        @endif

        @if($order->status_str == 'refunding')
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i>订单售后</header>
                    </div>
                    <div class="card-body">
                         <dl class="dl-horizontal">
                            <dt>订单号</dt>
                            <dd>{{ $order->order_sn }}</dd>
                            <dt>订单状态</dt>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
@include('admin.widget.modal-show')
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $('.js-detail').click(function(){
        $id = $(this).attr('data-id');
        $('.js-after-sell-'+$id).css('display','block');
    })
    // 同意申请
    $('.js-agress').click(function(){
        var  flag = false;
        var id = $(this).attr('data-id');
        var content = $('#js-content').val();
        var _this = $(this);
        var received = _this.parent().find('.js-received');
        $('.js-agress').attr('disabled','disabled');
        layer.confirm('您确定同意本次售后申请？', {
                btn: ['确定','取消'] //按钮
        }, function(){
            if(flag){
                return flag;
            }
            flag = true;
            $.ajax({
                url: '{!! action("Admin\\OrderProductRefundController@agree") !!}',
                type: 'POST',
                data: {
                     id : id,
                     content:content,
                     _token :"{!! csrf_token() !!}"
                },
                dataType: 'JSON',
                success: function (returnData) {
                    if (returnData.status == 'success') {
                        if(received.length>0){
                            received.css('display','block');
                        }

                        $('.js-agress').attr('disabled','disabled');
                        $('.js-agress').text('已同意');
                        $('.js-refuse-reason').css('display','none');
                        layer.msg('操作成功',{icon:1});
                    }else{
                       layer.msg(returnData.message,{icon: 0});
                       $('.js-agress').removeAttr('disabled');
                    }
                }
            });
        },function(){
            $('.js-agress').removeAttr('disabled');
        });
    })

    // 拒绝申请
    $('.js-refuse').click(function(){
        var id = $(this).attr('data-id');
        var _this = $(this);
        layer.confirm('您确定拒绝本次售后申请？', {
                btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url: '{!! action("Admin\\OrderProductRefundController@refuse") !!}',
                type: 'POST',
                data: {
                    id  : id,
                    reason: $('#js-reason').val(),
                    _token :"{!! csrf_token() !!}"
                },
                dataType: 'JSON',
                success: function (returnData) {
                    if (returnData.status == 'success') {
                        layer.msg('拒绝成功',{icon:1});
                        window.location.reload();
                    }else{
                       layer.msg(returnData.message,{icon: 0});
                    }
                }
            });
        });
    })

    // 确认收货
    $('.js-received').click(function(){
        var id = $(this).attr('data-id');
        $.ajax({
            url: '{!! action("Admin\\OrderProductRefundController@receive") !!}',
            type: 'POST',
            data: {
                 id : id,
                 _token :"{!! csrf_token() !!}"
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('确认成功',{icon:1});
                    window.location.reload();
                }else{
                   layer.msg('操作失败,请刷新重试',{icon: 0});
                }
            }
        });
    })

    // product 预览
    $(document).on("hidden.bs.modal", function (e) {
        $(e.target).removeData("bs.modal").find(".modal-content").empty();
    });

    $( ".resizable" ).resizable({aspectRatio: .8});
</script>
@stop
