@extends('admin.widget.body')

@section('content')

<section>
    <!--head-->
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">属性基本信息</li>
        </ol>
    </div>
    <!--end head-->
    <div class="section-body">
        <div class="card">
            <div class="card-body tab-content">
                <!-- 基本信息 -->
                <div class="tab-pane active" id="base-info">

                    <div class="form-group">

                        <div class="form-group">
                            <label>属性名称</label>
                            <input class="form-control" type="text" id="name" name="name" value="{{ $attribute->name or ''}}">
                        </div>

                        <div class="form-group">
                            <label>权重(越大要靠前)</label>
                            <input class="form-control" type="text" id="weight" name="weight" value="{{ $attribute->weight or ''}}">
                        </div>

                        <div class="form-group">
                            <label for="has_img">是否有图片</label>
                            {!! \App\Helpers\AdminViewHelper::radioHtml('has_img', ['0'=>'无','1'=>'有'], isset($attribute->has_img) ? $attribute->has_img : 1 ) !!}
                        </div>

                        <div class="form-group">
                            <label for="status">是否开启</label>
                            {!! \App\Helpers\AdminViewHelper::radioHtml('status', ['0'=>'关闭','1'=>'打开'], isset($attribute->status) ? $attribute->status : 1 ) !!}
                        </div>

                    </div>

                </div>
                <!-- 基本信息结束 -->

            </div><!-- ./.card-body -->


            <div class="card-actionbar">
                <div class="card-actionbar-row">
                    <button class="btn ink-reaction btn-primary addAnchor">@if (isset($_GET['id']))编辑@else添加@endif
                    </button>
                    <a href="{!! action('Admin\AttributeController@index') !!}" title="取消" class='btn btn-default'>取消</a>
                </div>
            </div>

        </div>
    </div>
</section>
@stop


@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>

<script type="text/javascript">
      // 新增属性
    $('.addAnchor').click(function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        @if (isset($_GET['id']))
            var urlSaveRoute = '{!! action("Admin\\AttributeController@edits") !!}';
            var id  = {{$_GET['id']}}
            var msg = '编辑属性成功';
        @else 
            var urlSaveRoute = '{!! action("Admin\\AttributeController@add") !!}'; 
            var id  = -1; 
            var msg = '新增属性成功';
        @endif
        $.ajax({
            url: urlSaveRoute,
            type: 'POST',
            data: {
                _token: CSRF_TOKEN,
                id        : id,
                name      : $("#name").val(),
                weight    : $("#weight").val(),
                has_img   : $("input[name='has_img']:checked").val(),
                status    : $("input[name='status']:checked").val(),
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg(msg, {icon: 1});
                    setTimeout(function(){
                       window.location.href = "{!! action('Admin\\AttributeController@index') !!}";
                    },1000)
                }else{
                    layer.msg(returnData.message,{icon: 0});
                }
           
            }
        });
    });
</script>
@stop

