@extends('admin.widget.body')

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">属性列表</li>
        </ol>
    </div>

    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="card-head">
                    <div class="tools">
                        <div class="btn-group">
                            <a href="{!! action('Admin\AttributeController@addShow') !!}" class="btn ink-reaction btn-default btn-sm pull-right">
                                添加
                            </a>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
            <table class="table table-striped table-condensed no-margin" align="right">
                <thead>
                    <tr>
                        <th style="text-align: center;">#</th>
                        <th style="text-align: center;">属性名称</th>
                        <th style="text-align: center;">属性值</th>
                        <th style="text-align: center;">权重(越高越靠前)</th>
                        <th style="text-align: center;">是否有图片</th>
                        <th style="text-align: center;">是否启用</th>
                        <th style="width: 400px;text-align: center;">操作</th>
                    </tr>
                </thead>
                <tbody>
                <input type="hidden" name="_token" class="_token" value="{{csrf_token()}}">
                @foreach ($attributes as $attribute)
                    <tr style="text-align: center;"> 
                        <td>{{ $attribute->id }}</td>
                        <td>{{ $attribute->name}}</td>
                         <td>
                            @if(!$attribute->attributeValue->isEmpty())
                            <select class="form-control js-select">
                             @foreach ($attribute->attributeValue as $value)
                              <option value="{{$value->id}}">{{$value->value}}</option>
                             @endforeach
                            </select>
                            @else
                                暂无属性值
                            @endif
                        </td>
                        <td align="center">{{ $attribute->weight }}</td>
                        <td>
                            {!! \App\Helpers\AdminViewHelper::enableHtml($attribute->has_img) !!}
                        </td>
                        <td>
                            {!! \App\Helpers\AdminViewHelper::enableHtml($attribute->status) !!}
                        </td>
                        <td>
                            <a href="{{ action('Admin\AttributeController@editShow', ['id' => $attribute->id]) }}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                            <a href="{{ action('Admin\AttributeValueController@addShow', ['id' => $attribute->id]) }}" class="btn ink-reaction btn-primary btn-xs" type="button">添加属性值</a>
                             @if(!$attribute->attributeValue->isEmpty())
                             <a href="{{ action('Admin\AttributeValueController@editShow', ['id' => $attribute->attributeValue[0]->id]) }}" class="btn ink-reaction btn-primary btn-xs EditValue" type="button">编辑属性值</a>
                             <a class="btn ink-reaction btn-danger btn-xs deleteValue" type="button">删除属性值</a>
                             @endif
                             @if($attribute->id != 2)
                                <a data-id ="{{$attribute->id}}" data-name="{{$attribute->name}}" class="btn ink-reaction btn-danger btn-xs deleteAnchor" type="button">删除</a>
                             @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
             @if($attributes->toArray()['data'])
            <div class="col-md-12 col-lg-12">
                <label style="color: black;font-size: 16px">当前页数量: {{$attributes->toArray()['to'] - $attributes->toArray()['from'] + 1}}</label>
                <label style="color: black;font-size: 16px">总数量: {{$attributes->toArray()['total']}}</label>
            </div>
            @endif
            <div class="text-center">
                {!! $attributes->render() !!}
            </div>
                </div>
            </div>
        </div>  
    </div><!--end ./card-body -->
</section>
@stop

@section('script')
<script type="text/javascript">
 // 删除属性
    $('.deleteAnchor').click(function(){
        var id = $(this).attr('data-id');
        layer.confirm('您确定删除 "'+$(this).attr('data-name')+'" 这个属性？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                var urlDeleteRoute = '{!! action("Admin\\AttributeController@delete") !!}';
                $.ajax({
                    url: urlDeleteRoute,
                    type: 'POST',
                    data: {
                        _token: $('._token').val(),
                        id: id
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }
                   
                    }
                });
        });
    });

    // 删除属性值
    $('.deleteValue').click(function(){
        var id = $(this).parent().parent().find('.js-select').val();
        var name = $(this).parent().parent().find('.js-select option:selected').text();
        layer.confirm('您确定删除 "'+name+'" 这个属性值？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var urlDeleteRoute = '{!! action("Admin\\AttributeValueController@delete") !!}';
                $.ajax({
                    url: urlDeleteRoute,
                    type: 'POST',
                    data: {
                        _token: $('._token').val(),
                        id: id
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }
                   
                    }
                });
        });
    });
    // select动态改变标签a属性值的id
    $(".js-select").change(function(){
        var id = $(this).val();
        var edit = $(this).parents().find('.EditValue');
        var href = edit.attr('href').split('?');
        edit.attr('href',href[0]+"?id="+id);
    })
</script>
@stop
