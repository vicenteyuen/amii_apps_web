@extends('admin.widget.body')

@section('content')

<section>
    <!--head-->
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加属性值</li>
        </ol>
    </div>
    <!--end head-->
    <div class="section-body">
        <div class="card">
            <div class="card-body tab-content">
                <!-- 基本信息 -->
                <div class="tab-pane active" id="base-info">

                    <div class="form-group">

                        <div class="form-group">
                            <label>属性名称</label>
                            <input class="form-control" type="text" id="name" name="name" value="{{$attributeName}}" readonly="readonly">
                        </div>

                        <div class="form-group">
                            <label>属性值</label>
                            <input class="form-control" type="text" id="value" name="value" value="{{$attributeValue->value or ''}}">
                        </div>

                        <div class="form-group">
                            <label>权重(越大要靠前)</label>
                            <input class="form-control" type="text" id="weight" name="weight" value="{{$attributeValue->weight or ''}}">
                        </div>

                        @if(isset($attributeValue))
                        <div class="form-group">
                            <label>分类图片</label>
                            <div>
                                <img id="showImage" src="{{$attributeValue->image}}" id="img" class="img mgt-10" style="max-width=200px; max-height:200px;">
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label>上传分类图片(尺寸: 750x470)</label>
                            <input type="file" class="form-control" name="img" id="categoryImage">
                        </div>


                        <div class="form-group">
                            <label for="status">是否开启</label>
                            {!! \App\Helpers\AdminViewHelper::radioHtml('status', ['0'=>'关闭','1'=>'打开'], isset($attributeValue->status) ? $attributeValue->status : 1 ) !!}
                        </div>

                    </div>

                </div>
                <!-- 基本信息结束 -->

            </div><!-- ./.card-body -->


            <div class="card-actionbar">
                <div class="card-actionbar-row">
                    <button class="btn ink-reaction btn-primary addAnchor">@if(isset($attributeValue))编辑@else添加@endif
                    </button>
                    <a href="{!! action('Admin\AttributeController@index') !!}" title="取消" class='btn btn-default'>取消</a>
                </div>
            </div>

        </div>
    </div>
</section>
@stop


@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>

<script type="text/javascript">
      // 新增属性值
    $('.addAnchor').click(function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        @if(isset($attributeValue))
            var urlSaveRoute = '{!! action("Admin\\AttributeValueController@edits") !!}';
            var msg = '编辑成功';
        @else
            var urlSaveRoute = '{!! action("Admin\\AttributeValueController@add") !!}';
            var msg = '添加成功';
        @endif

        var formData = new FormData();
        if($('#categoryImage')[0].files[0]){
             formData.append('image', $('#categoryImage')[0].files[0]);
        }

        formData.append('_token',CSRF_TOKEN);
        formData.append('attribute_id',"{{ $_GET['id'] }}");
        formData.append('value',$("#value").val());
        formData.append('weight',$("#weight").val());
        formData.append('status',$("input[name='status']:checked").val());

        $.ajax({
            url: urlSaveRoute,
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg(msg, {icon: 1});
                    setTimeout(function(){
                       window.location.href = "{!! action('Admin\\AttributeController@index') !!}";
                    },1000)
                }else{
                    layer.msg(returnData.message,{icon: 0});
                }

            }
        });
    });
</script>
@stop

