@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">积分公告列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-head">
                <header>
                    <a href="{{ action('Admin\PointAnnounceController@createOrShow') }}" class="btn ink-reaction btn-primary btn-md download">创建公告</a>
                </header>
            </div>
            <div class="card-body">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th>公告标题</th>
                            <th>奖励类型</th>
                            <th>人数</th>
                            <th>状态</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>
                            @if ($item->provider == 1)
                            月度奖励
                            @elseif ($item->provider == 2)
                            年度奖励
                            @endif
                            </td>
                            <td>{{ $item->number }}</td>
                            <td>
                            @if (! $item->status)
                                <button class="btn ink-reaction btn-default-dark btn-xs" disabled="disabled">结算中</button>
                            @else
                                @if ($item->is_publish)
                                    <a href="{{ action('Admin\PointAnnounceController@unPublish', ['id' => $item->id]) }}" class="btn ink-reaction btn-danger btn-xs">取消公示</a>
                                @else
                                    <a href="{{ action('Admin\PointAnnounceController@publish', ['id' => $item->id]) }}" class="btn ink-reaction btn-success btn-xs">公示</a>
                                @endif
                            @endif
                                <a href="{{ action('Admin\PointAnnounceController@createOrShow', ['id' => $item->id]) }}" class="btn ink-reaction btn-primary btn-xs">详情</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {!! $items->render() !!}
            </div>
        </div>
    </div>
</section>
@stop
