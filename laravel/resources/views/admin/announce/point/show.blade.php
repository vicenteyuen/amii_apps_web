@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">{{ $item->id ? '编辑' : '添加' }}积分公告</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    {!! Form::open(['action' => 'Admin\PointAnnounceController@update', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    @include('admin.widget.input', [
                        'name' => 'name',
                        'id' => 'name',
                        'title' => '公告名',
                        'colsm' => '12',
                        'collg' => '12',
                        'value' => $item->name
                    ])
                    @include('admin.widget.select', [
                        'colsm' => '12',
                        'collg' => '12',
                        'id' => 'provider',
                        'name' => 'provider',
                        'title' => '公告类型',
                        'selected' => $item->provider,
                        'values' => [
                            '月度奖励' => '1',
                            '年度奖励' => '2',
                        ]
                    ])
                    @include('admin.widget.input', [
                        'name' => 'number',
                        'id' => 'number',
                        'title' => '人数',
                        'colsm' => '12',
                        'collg' => '12',
                        'value' => $item->number
                    ])
                    @include('admin.widget.datetimepicker', [
                        'colsm' => '12',
                        'collg' => '6',
                        'title' => '开始时间',
                        'name' => 'start',
                        'class' => 'startDate',
                        'value' => $item->start_time
                    ])
                    @include('admin.widget.datetimepicker', [
                        'colsm' => '12',
                        'collg' => '6',
                        'title' => '结束时间',
                        'name' => 'end',
                        'class' => 'endDate',
                        'value' => $item->end_time
                    ])
                    @if ($item->id)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group text-right">
                            @if (! $item->status)
                            <button type="button" class='btn btn-success'>结算中</button>
                            @else
                                @if ($item->is_publish)
                                <a href="{{ action('Admin\PointAnnounceController@unPublish', ['id' => $item->id]) }}" class='btn btn-success'>取消公示</a>
                                @else
                                <a href="{{ action('Admin\PointAnnounceController@publish', ['id' => $item->id]) }}" class='btn btn-success'>公示</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    @else
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group text-right">
                            <button type="submit" class='btn btn-success'>保存</button>
                        </div>
                    </div>
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>
            @if ($item->id)
            <div class="card-body">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr >
                            <th>名次</th>
                            <th>月度积分</th>
                            <th>用户名</th>
                            <th>手机号</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($item->users as $user)
                        <tr>
                            <td>{{ $user->placed }}</td>
                            <td>{{ $user->points }}</td>
                            <td>{{ $user->user->nickname }}</td>
                            <td>{{ $user->user->phone }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</section>
@stop

@section('script_link')
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
@stop

@section('script')
<script type="text/javascript">
    $('.form').on('submit', function() {
        $('button[type="submit"]').disable();
    });
    $(".startDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
    $(".endDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
</script>
@stop
