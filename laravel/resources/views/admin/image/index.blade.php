@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">
<!-- <link rel="stylesheet" type="text/css" href="/assets/admin/css/bootstrap.min.css"> -->
@stop


@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">图片列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-head">
                <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                    <li class="active"><a href="#index">图片列表</a></li>
                    <li><a href="#create">添加图片</a></li>
                </ul>
            </div>
            <div class="card-body tab-content">
                <!-- index -->
                <div class="tab-pane active" id="index">
                    <div class="row">
                    @each ('admin.image.contain', $images, 'image', 'admin.image.null')
                    </div>
                    <div class="row text-right">
                        <div class="col-md-6">
                            <button class="btn ink-reaction btn-success btn-sm selectAll">全选</button>
                            <button class="btn ink-reaction btn-danger btn-sm deleteImage">删除</button>
                        </div>
                    </div>
                    <div class="text-center">
                        {!! $images->render() !!}
                    </div>
                </div>
                <!-- create -->
                <div class="tab-pane" id="create">
                    {!! Form::open(['class' => 'form floating-label']) !!}
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                    <div class="row">
                    @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '12',
                            'id' => 'imageFileId',
                            'name' => 'imageFileId',
                            'title' => '选文件夹',
                            'type'  => 'image',
                            'selectValues' => $files
                        ])
                    @include('admin.widget.select', [
                        'colsm' => '12',
                        'collg' => '12',
                        'id' => 'imageType',
                        'name' => 'imageType',
                        'title' => '选择上传图片类型',
                        'type'  => 'image',
                        'values' => [
                            '商品图片(750x750)' => 'product',
                            '首页轮播App图片(710x1043)' => 'bannerApp',
                            '首页轮播移动端图片(710x1043)' => 'bannerMobile',
                            '首页轮播小程序图片(705x560)' => 'bannerWeapp',
                            '推广图片(710x960)' => 'popular',
                            '推广图片小程序图片(344x238)' => 'popularWeapp',
                            '分类图片(375x235)' => 'category',
                            '品牌图片(375x235)' => 'brand',
                            '优惠卷图片(300x300)' => 'coupon',
                            '专题图片(750x358)' => 'subject',
                            '专题内容图片(原图尺寸)' => 'subjectCont',
                            '实体店国片(700x330)' => 'physicalStore',
                            '视频图片(360x240)' => 'video',
                            '头像图片(300x300)' => 'avatar',
                            '售后图片(300x300)' => 'refund',
                        ]
                    ])
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div id="uploader">
                                <p>你的浏览器不支持Flash或者HTML 5.</p>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')

<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
<script src="/assets/lib/plupload/i18n/zh_CN.js"></script>

@stop

@section('script')
<script type="text/javascript">
    var imageUploadUrl = "{!! action('Admin\ImageController@store') !!}/";
    $('#uploader').plupload({
        url : '',
        filters : [
            {title : "Image files", extensions : "jpg,gif,png"}
        ],
        rename: true,
        sortable: true,
        flash_swf_url : '/assets/lib/plupload/Moxie.swf',
        silverlight_xap_url : '/assets/lib/plupload/Moxie.xap'
    });
    $('#uploader').on('ready', function() {
        $('#uploader').plupload('disable');
        return false;
    });
    $('#imageType').on('change', function() {
        if ($(this).val()) {
            if($('#imageFileId').val() == null){
                layer.msg('请先选择文件夹', {icon: 4});
                return false;
            }
            $('#uploader').plupload({
                url: imageUploadUrl + $(this).val()+'_'+$('#imageFileId').val(),
            });
            $('#uploader').plupload('enable');
        } else {
            layer.msg('请选择上传图片类型', {icon: 4});
            $('#uploader').plupload('disable');
        }
    });


    $(".nav-tabs").live('click',function(){
        var href = $(this).find('.active').find('a').attr('href');
        if(href == '#index'){
            window.location.reload();
        }
    })

    $('.deleteImage').live('click',function(){
        removeImage($(this));
    });

    $('.selectAll').live('click',function(){
        selectAll();
    });

    var selectAllStatus = 1;
    function selectAll()
    {
        if(selectAllStatus == 1){
            $.each($('.js-images'),function(){
                $(this).attr('checked','checked');
            })
            selectAllStatus = 0;
        }else{
            $.each($('.js-images'),function(){
                $(this).removeAttr('checked');
            })
            selectAllStatus = 1;
        }
         
    }

    // 删除图片
    function removeImage(_this)
    {
        var ImageIds = [];
        $.each($('.js-images'),function(){
            if($(this).attr('checked') == 'checked'){
                ImageIds.push($(this).val());
            }
            
        })
        if(ImageIds.length <= 0){
             layer.msg('请选择图片', {icon: 0});
             return false;
        }
        $.ajax({
            url: '{!! action("Admin\\ImageController@deleteAll") !!}',
            type: 'POST',
            async: false,
            data: {
                _token: $('#_token').val(),
                imageIds: ImageIds,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('删除成功', {icon: 1});
                    window.location.reload();
                }else{
                    layer.msg('删除失败,请刷新重试',{icon: 0});
                }

            }
        });

    }
</script>
@stop
