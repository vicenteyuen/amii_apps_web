@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">文件夹列表</li>
        </ol>
    </div>
        <div class="row">    
               <div class="col-md-12">
                    <div class="card tabs-left style-default-light">
                        <ul class="card-head nav nav-tabs">
                            @foreach($imageFiles as $key => $file)
                            <li class= @if($index == $file->id) "active" @endif><a href="{{action('Admin\ImageFileController@selectFile',$file->id)}}">{{$file->name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="card-body tab-content style-default-bright">
                            <div class="tab-pane active" id="index">
                                <div class="row">
                                @each ('admin.image.contain', $images, 'image', 'admin.image.null')
                                </div>
                                <div class="row text-right">
                                    <div class="col-md-6">
                                        <button class="btn ink-reaction btn-success btn-sm selectAll">全选</button>
                                        <button class="btn ink-reaction btn-danger btn-sm deleteImage">删除</button>
                                    </div>
                                </div>
                                <div class="text-center">
                                    {{ empty($images)?'':$images->render() }}
                                </div>
                          </div>                       
                        </div><!--end .card-body -->
                    </div><!--end .card -->
               </div>           
        </div>
    </div>
</section>
@stop

@section('script_link')
<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
<script src="/assets/lib/plupload/i18n/zh_CN.js"></script>
@stop

@section('script')
<script type="text/javascript">

    $('.deleteImage').live('click',function(){
        removeImage($(this));
    });

    $('.selectAll').live('click',function(){
        selectAll();
    });

    var selectAllStatus = 1;
    function selectAll()
    {
        if(selectAllStatus == 1){
            $.each($('.js-images'),function(){
                $(this).attr('checked','checked');
            })
            selectAllStatus = 0;
        }else{
            $.each($('.js-images'),function(){
                $(this).removeAttr('checked');
            })
            selectAllStatus = 1;
        }
         
    }

    // 删除图片
    function removeImage(_this)
    {
        var ImageIds = [];
        $.each($('.js-images'),function(){
            if($(this).attr('checked') == 'checked'){
                ImageIds.push($(this).val());
            }
            
        })
        if(ImageIds.length <= 0){
             layer.msg('请选择图片', {icon: 0});
             return false;
        }
        $.ajax({
            url: '{!! action("Admin\\ImageController@deleteAll") !!}',
            type: 'POST',
            async: false,
            data: {
                _token: $('#_token').val(),
                imageIds: ImageIds,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('删除成功', {icon: 1});
                    window.location.reload();
                }else{
                    layer.msg('删除失败,请刷新重试',{icon: 0});
                }

            }
        });

    }
</script>
@stop
