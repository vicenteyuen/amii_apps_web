<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
    <div class="image-contain">
        <img src="{{ $image->url }}">
    </div>
    <div>
      <span style="position: absolute;left: 10px;top: -15px;font-size: 30px;" >
		  <label class="checkbox-inline checkbox-styled" >
		    <input name="imageIds[]"  type="checkbox" value="{{$image->id}}" class="removeImage js-images"><span></span>
		  </label>
      </span>
      <!-- <span data-id="{{$image->id}}" class="glyphicon glyphicon-remove-circle removeImage" style="position: absolute;right: 0px;top: -10px;font-size: 30px;color: #cc4210;cursor: pointer;" >
      </span> -->
    </div>
</div>
