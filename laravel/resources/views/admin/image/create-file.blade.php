     
@extends('admin.widget.body')

@section('style_link')

<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">

@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加文件夹</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">  
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                    <form action="{{$url}}" method="POST" class="form floating-label" autocomplete="off">
                    <input type="hidden" name="id" value="{{isset($file)?$file->id:0}}"> 
                    @include('admin.widget.input',[
                                'name' => 'name', 
                                'title' => '文件夹名称',
                                'colsm' => '12',
                                'collg' => '12',
                                'value' => isset($file) ? $file->name: ''
                    ])
                    @include('admin.widget.input',[
                                'name' => 'desc', 
                                'title' => '简介',
                                'colsm' => '12',
                                'collg' => '12',
                                'value' => isset($file) ? $file->desc: ''
                    ])
                     <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md">
                                提交
                                </button></li>
                            </ul>
                            </div>
                        </div>
                    </form>
                    </div>
                </div> 
            </div><!-- ./row -->
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop