@extends('admin.widget.body')

@section('style_link')

<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">

@stop
@section('content')
    <section>
        <div class="section-header">
            <ol class="breadcrumb">
                <li class="active">文件夹列表</li>
            </ol>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="card-head">
                        <div class="tools">
                            <div class="btn-group">
                                <a href="{!! action('Admin\ImageFileController@create') !!}" class="btn ink-reaction btn-primary btn-sm pull-right">
                                    添加
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                        <tr>
                            <th>名称</th>
                            <th>简介</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($imageFiles as $imageFile)
                            <tr>
                                <td>{{ $imageFile->name}}</td>
                                <td>{{ $imageFile->desc }}</td>
                                <td>                                     
                                    <a href="{{ action('Admin\ImageFileController@showEdit', $imageFile->id)}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                                    <a href="javascript:;" data-url="{{ action('Admin\ImageFileController@delete', $imageFile->id)}}" class="btn ink-reaction btn-danger btn-xs file-delete" type="button">删除</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop

@section('script')
    <script type="text/javascript">
        $('.file-delete').click(function () {
            var url = $(this).attr('data-url');
            layer.confirm('确定删除?', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                                window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
            });
        })
    </script>
@stop
