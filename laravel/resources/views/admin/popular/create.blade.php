@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">
                @if (!isset($showPopular))
                    添加推广
                @else
                    编辑推广
                @endif

            </li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @if (!isset($showPopular))
                        {!! Form::open(['action' => 'Admin\PopularController@store', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                    @else
                        {!! Form::open(['action' => 'Admin\PopularController@edits', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                        <input type="hidden" id="id" name="id" value="{{isset($showPopular) && $showPopular->id ? $showPopular->id : ''}}">
                    @endif

                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id' => 'status',
                            'name' => 'status',
                            'title' => '启用状态',
                            'selected' => isset($showPopular->status) ? $showPopular->status : '',
                            'values' => [
                                '关闭' => '0',
                                '启用' => '1',
                            ]
                        ])
                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id' => 'parentId',
                            'name' => 'parentId',
                            'title' => '选择父级',
                            'selectArr' => isset($showPopular->parent_id) ? $showPopular->parent_id : 0,
                            'values' => $parents,

                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name'  => 'title',
                            'id'    => 'title',
                            'title' => '标题',
                            'type'  => 'text',
                            'value' => isset($showPopular->title) ? $showPopular->title : '',
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'descript',
                            'id' => 'descript',
                            'title' => '描述',
                            'type'  => 'text',
                            'value' => isset($showPopular->descript) ? $showPopular->descript : '',
                        ])

                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name' => 'weight',
                            'title' => '排序权重（降序）',
                            'type'  => 'text',
                            'value' => isset($showPopular->weight) ? $showPopular->weight : '0'
                        ])
                        <div id="showIsClick"  >
                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id' => 'isClick',
                            'name' => 'isClick',
                            'title' => '是否可点击跳转',
                            'selected' => isset($showPopular->is_click) ? $showPopular->is_click : '1',
                            'values' => [
                                '可跳转' => '1',
                                '不可跳转' => '0',
                            ]
                        ])
                        </div>

                        <div id = 'showJump' style="{{ isset($showPopular) && $showPopular->is_click == 0 ? 'display: none' : 'display:block' }}">

                        @include('admin.widget.select', [
                            'colsm' => '12',
                            'collg' => '6',
                            'id' => 'provider',
                            'name' => 'provider',
                            'title' => '跳转方式',
                            'selected' => isset($showPopular->provider_value) ? $showPopular->provider_value : '5',
                            'values' => [
                                '品牌分类列表' => '5',
                                '分类商品列表' => '1',
                                '品牌商品列表' => '2',
                                '商品详情'    => '3',
                                '跳转到页面'  => '4',
                            ],

                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '6',
                            'name'  => 'resource',
                            'id'    => 'resource',
                            'title' => '跳转资源标识',
                            'attribute' => "placeholder=请填写品牌分类ID",
                            'type'  => 'text',
                            'value' => isset($showPopular->resource) ? $showPopular->resource : '',
                        ])
                        </div>

                        <div class = 'showImg' style="{{ isset($showPopular) && $showPopular->parent_id != 0 ? 'display: none' : 'display:block' }}">
                            @include('admin.widget.image-upload', [
                                'colsm' => '12',
                                'collg' => '6',
                                'class' => 'puload-display-popular',
                                'id'    => 'popularImg',
                                'imgId' => 'popularImgId',
                                'name'  => 'image',
                                'style'=>'width:335px;height:480px;',
                                'descript' => '图片(分辨率: 690x930 )',

                            ])
                            @include('admin.widget.image-upload', [
                                'colsm' => '12',
                                'collg' => '6',
                                'class' => 'puload-display-md-weapp-mobile',
                                'id'    => 'popularWeappImg',
                                'imgId' => 'popularWeappImgId',
                                'name'  => 'weapp_image',
                                'style'=>'width:344px;height:238px;',
                                'descript' => '小程序图片(分辨率: 705x560 )',

                            ])
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group ">
                                @if (isset($showPopular))
                                    <a href="{{ Action('Admin\PopularController@create') }}" class="btn ink-reaction btn-success btn-md text-left" type="button">添加推广</a>
                                    @if (isset($is_child))
                                        @php
                                            $is_child = 'is_childCreate=1';
                                        @endphp
                                    @else
                                        @php
                                            $is_child = '';
                                        @endphp
                                    @endif
                                    <a href="{{ Action('Admin\PopularController@create', ['id' => $showPopular->id, $is_child ]) }}" class="btn ink-reaction btn-success btn-md text-left" type="button">添加推广下级</a>
                                @endif
                                <button type="submit" class='btn btn-success pull-right'>保存</button>
                            </div>
                        </div>

                        @if(isset($showPopular) && count($childs) > 0)
                            <div class="col-md-12 col-lg-12 text-right" style="margin-top: 22px">
                                <a href="javascript:void(0);" class="ink-reaction btn-primary btn-sm selectAll" type="button">全选</a>
                                <a href="javascript:void(0);" class="ink-reaction btn-danger btn-sm deleteProduct" type="button">删除</a>
                            </div>
                        @endif

                        <!-- 推广下级start-->
                        <div class='showImg' style="{{ isset($showPopular) && $showPopular->parent_id != 0 ? 'display: none' : 'display:block' }}">
                            <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="form-group">

                            @if(isset($showPopular))
                            <table id = "tablePopuler" class="table table-hover table-condensed table-striped no-margin">
                                <leble for = "tablePopuler" class="control-label">{{$showPopular->title}}推广的下级</lable>
                                <thead >
                                    <tr >
                                        <th>#</th>
                                        <th >id</th>
                                        <th >标题</th>
                                        <th >添加其下级</th>
                                        <th >是否可跳转</th>
                                        <th >跳转方式</th>
                                        <th >跳转资源标识</th>
                                        <th >排序权重（降序）</th>
                                        <th >操作</th>
                                    </tr>
                                </thead>
                                @if (count($childs) > 0)
                                <tbody>
                                    @foreach($childs as $child)
                                        <tr>
                                        <td>
                                          <label class="checkbox-inline checkbox-styled" >
                                            <input name="product[]"  type="checkbox" value="{{$child->id}}" class=" js-product"><span></span>
                                          </label>
                                        </td>
                                        <td>{{$child->id}}</td>
                                        <td>{{$child->title}}</td>
                                        <td>
                                            @if (isset($is_child))
                                                @php
                                                    $is_child = 'is_childCreate=1';
                                                @endphp
                                            @else
                                                @php
                                                    $is_child = '';
                                                @endphp
                                            @endif
                                           <a href="{{ Action('Admin\PopularController@create', ['id' => $child->id, $is_child]) }}" class="btn ink-reaction btn-success btn-xs" type="button">添加</a>
                                        </td>

                                        <td>
                                            @if ($child->is_click == 1)
                                                可跳转
                                            @else
                                                不可跳转
                                            @endif
                                        </td>
                                        <td>
                                            @if ($child->provider == 'category')
                                                分类商品列表
                                            @elseif ($child->provider == 'brand')
                                                品牌商品列表
                                            @elseif ($child->provider == 'product')
                                                商品详情
                                            @elseif ($child->provider == 'html')
                                                跳转到页面
                                            @elseif ($child->provider == 'brandcate')
                                                品牌分类列表
                                            @else
                                                暂无
                                            @endif
                                        </td>
                                        <td>
                                            @if ($child->resource)
                                                {{$child->resource}}
                                            @else
                                                暂无
                                            @endif
                                        </td>
                                        <td>{{$child->weight}}</td>
                                        <td>
                                            {!! \HtmlFuncs::statusAdmin($child->status) !!}
                                        </td>
                                        <td>
                                            @if($child->status == 1)
                                                @php
                                                    $btnType = 'btn-danger';
                                                    $btnName = '停用';
                                                @endphp
                                           @else
                                                @php
                                                    $btnType = 'btn-primary';
                                                    $btnName = '启用';
                                                @endphp
                                           @endif

                                           <a href="javascript:void(0);" class="btn ink-reaction  {{$btnType}} btn-xs js_child_status" type="button" data-status = "{{ $child->status }}" data-id = "{{ $child->id }}">{{$btnName}}</a>

                                            <a href="{{ Action('Admin\PopularController@showDetail', ['id' => $child->id, 'is_child=1']) }}" class="btn ink-reaction btn-success btn-xs" type="button">编辑</a>

                                            <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-xs btn-danger js-delete" data-id="{{$child->id}}" data-name="{{$child->name}}" data-toggle="modal" data-target="#confirmModal">删除</a>
                                        </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                @else

                                @endif
                            </table>
                            @endif
                            </div>
                            </div>
                        </div>
                        <!-- 推广下级end -->

                        <!-- 模态框（Modal） -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content" style="max-height:calc(100vh - 140px); overflow-y: scroll;">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">
                                            添加跳转资源标识
                                        </h4>
                                    </div>
                                    <div class="modal-body">


                                        <div class="card-body">
                                           <form class="form floating-label"  >
                                                <div class="brand hiddenAll"  >
                                                 @include('admin.widget.select', [
                                                    'colsm' => '12',
                                                    'collg' => '12',
                                                    'id'    => 'brandId',
                                                    'name'  => 'brand_id',
                                                    'title' => '请选择品牌',
                                                    'values' =>  $brandData,
                                                ])
                                                </div>
                                                <div class="category hiddenAll"  >
                                                    @include('admin.widget.select', [
                                                        'colsm' => '12',
                                                        'collg' => '12',
                                                        'id'    => 'categoryId',
                                                        'name'  => 'category_id',
                                                        'title' => '请选择品牌下的分类',
                                                        'brand' => []
                                                    ])
                                                </div>
                                                <!-- 用户分类商品搜索 -->
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hiddenAll cate">
                                                        <div class="dd nestable-list">

                                                            @foreach($categories as $category)
                                                            <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 " style="margin-top: 10px;margin-left: 30px;">
                                                                <label class="checkbox-inline checkbox-styled">
                                                                    <input name="categories"   type="radio" value="{{$category->id}}"
                                                                    @if(isset($brandCategory))
                                                                        @if(in_array($category->id,$brand->brandCategory->pluck('category_id')->toArray()))
                                                                            checked="checked"
                                                                            class="js-danger-delete"
                                                                        @endif
                                                                    @endif
                                                                     ><span class="">{{$category->name}}</span>
                                                                </label>

                                                            </div><!--end .col -->
                                                            @endforeach
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hiddenAll cateBtn">
                                                    <div class="input-group text-right">
                                                     <div class="input-group-btn">
                                                        <button class="btn  btn-success btn-md" type="button" id="Catebtn">添加</button>
                                                    </div>
                                                    </div>
                                                </div>

                                            </form>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 searchPorduct hiddenAll">
                                            <div class="input-group">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入">
                                                    <label for="search">请输入商品名称或产品编号</label>
                                                    <div class="form-control-line"></div>
                                                </div>
                                                <div class="input-group-btn">
                                                    <button class="btn btn-floating-action btn-default-bright" type="button" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                                </div>
                                            </div>
                                            </div>

                                            <table class="table table-striped table-hover dataTable no-footer searchPorduct hiddenAll">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>商品名称</th>
                                                        <th>商品编号</th>
                                                        <th style="width: 80px;">操作</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="search_tbody">

                                                </tbody>
                                            </table>
                                       </div>
                                        <!-- ./card-body -->

                                    </div>

                                </div><!-- /.modal-content -->
                                    <div class="text-right hiddenAll pagebtn" style="margin: 6px 20px 20px 0;" >
                                        <button type="button" id="prePage" class="btn btn-sm btn-default">上一页</button>
                                        <button type="button"  id="nextPage" class="btn btn-sm btn-default" >下一页</button>
                                    </div>
                            </div><!-- /.modal -->
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    // 推广下级编辑时，添加下级处理
    @if(isset($is_childCreate))
        $('#showIsClick').hide();
        $('#isClick').val(0);
        $('#showJump').hide();
    @endif

    // 推广下级添加推广
    @if(isset($parent_id))
       $('#parentId').val({{$parent_id}});
    @endif
    // 后台主图
    Helper.setDefaultImg($('#popularImg~img'));
    $("#popularImg").change(function(){
        Helper.readURL(this, '#popularImgId');
    });
    // 小程序主图
    Helper.setDefaultImg($('#popularWeappImg~img'));
    $("#popularWeappImg").change(function(){
        Helper.readURL(this, '#popularWeappImgId');
    });

    //编辑显示主图
    @if(!empty($showPopular->image))
         Helper.setDefaultImg($('#popularImg~img'),"{{$showPopular->image}}");
    @endif
    //编辑显示主图
    @if(!empty($showPopular->weapp_image))
         Helper.setDefaultImg($('#popularWeappImg~img'),"{{$showPopular->weapp_image}}");
    @endif

    // 父级选择
    $(document).ready(function(){
        var value = $(parentId).val();
        showImage(value);

        $(parentId).change(function(){
            var value = $(this).val();
            showImage(value);

        })
        // 是否可跳转
        $("#isClick").change(function(){
            var value = $(this).val();
            if (value == 1 ) {
                $("#showJump").show();
                $("#provider").focus().find('option:nth-of-type(2)').prop('selected','selected');
            } else {
                $("#showJump").hide();
            }

        })

        // 提示跳转资源标识
        $("#provider").change(function(){
            $('#resource').val('');
            var value = $(this).val();
            switch(value ){
                case '1':
                    $placeholder = '请填写分类ID';
                break;
                case '2':
                    $placeholder = '请填写品牌ID';
                break;
                case '3':
                    $placeholder = '请填写商品ID';
                break;
                case '4':
                    $placeholder = '请填写跳转网址';
                break;
                case '5':
                    $placeholder = '请填写品牌分类ID';
                break;
                default:
                    $placeholder = '';
                break;
            }
            return $("#resource").attr('placeholder', $placeholder);
        })
    })


    // check is parent ? show image
    function showImage(value){
       if (value == 0) {
            $(".showImg").show();
        } else {
            $(".showImg").hide();
        }
    }

    // 优惠卷停用
    $('.js_child_status').live('click', function(){
        popularStatus($(this));
    })
    // 优惠卷停用
    function popularStatus(_this)
    {
        var status = _this.attr('data-status');
        if(status == '1'){
            var success      = '停用成功';
            var error        = '停用失败';
            var updateStatus = 0;
        }else{
            var success      = '启用成功';
            var error        = '启用失败';
            var updateStatus = 1;
        }
        $.ajax({
            url: '{{ action("Admin\\PopularController@status") }}',
            type: 'POST',
            data: {
                id     : _this.attr('data-id'),
                status : updateStatus,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');
                    if(status == 1){
                        // 可启用状态
                        _this.parents('tr').find('i').addClass('text-red fa-close');
                        _this.parents('tr').find('i').removeClass('text-green fa-check');
                        _this.addClass('btn-primary');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',0);
                        _this.text('启用');
                    }else{
                        // 可停用状态
                        _this.parents('tr').find('i').addClass('text-green fa-check');
                        _this.parents('tr').find('i').removeClass('text-red fa-close');
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-primary');
                        _this.attr('data-status',1);
                        _this.text('停用');
                    }
                } else {
                    toastr.error(error, '失败');
                }
            }
        });

    };

    // 删除推广
    $('.js-delete').click(function(){
        var _this = $(this);
        layer.confirm('您确定删除 "'+_this.attr('data-name')+'" 这个推广？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\PopularController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

    // 模态框弹框
    $('#resource').focus(function(){
        var hiddenAll = $('.hiddenAll');
        var brand     = $('.brand');
        var category  = $('.category');
        var cate      = $('.cate');
        var cateBtn   = $('.cateBtn');
        var searchPorduct = $('.searchPorduct');
        var provider  = $('#provider').val();
        var dataTarget = "#myModal";

        clearSearchDate();

        switch(provider ){
            case '1':
                hiddenAll.hide();
                cate.show();
                cateBtn.show()
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
            break;
            case '2':
                hiddenAll.hide();
                brand.show();
                cateBtn.show()
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
                $("#brandId").find('option:nth-of-type(2)').prop('selected','selected').change();
                // brandSelectChange();
            break;
            case '3':
                hiddenAll.hide();
                searchPorduct.show();
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
            break;
            case '4':
                 $(this).removeAttr("data-toggle data-target");
            break;
            case '5':
                hiddenAll.hide();
                brand.show();
                cateBtn.show();
                category.show();
                $(this).attr({'data-toggle': 'modal', 'data-target': dataTarget});
                $("#brandId").find('option:nth-of-type(2)').prop('selected','selected').change();
                // brandSelectChange();
            break;
            default:
                $(this).removeAttr("data-toggle data-target");
            break;
        }

    })

    // tbody/搜索清空、
    function clearSearchDate(){
        var tbody = $('#search_tbody');
        tbody.empty();
        $('#key').val('');
        $('#brandId').val('');
        $('#categoryId').val('');
    }

    var current_page = 1;
    var prev_page_url = 1;
    var nextPage_url;
    var pagebtn = $('.pagebtn');
    // search
    $('#search-button').click(function(){
        var tbody = $('#search_tbody');
        tbody.empty();
        var url = "{!! action('Admin\\PopularController@search', ['page' => "+ current_page +"]) !!}";
        sendSearchRequire(url);
    })

    // 上一页
    $('body').on('click','#prePage',function() {
        if(prev_page_url == null){
            toastr.error('已是首页', '失败!')
            return false;
        }
        var url = prev_page_url;
        sendSearchRequire(url);
    })

    // 下一页
    $('body').on('click','#nextPage',function() {
        if(nextPage_url == null){
            toastr.error('已是最后一页', '失败!')
            return false;
        }
        var url = nextPage_url;
        sendSearchRequire(url);
    })

    // 获取数据
    function sendSearchRequire(url){

         $.ajax({
            url: url,
            type: 'GET',
            data: {'_token': "{{ csrf_token() }}", 'key': $('#key').val()},
        }).done(function (data) {
            if(data.status == 'success'){
                if(data.data.data == 0){
                    toastr.error('查询不到该商品.', '失败!')
                    return false;
                }
                // 加载
                loadProduct(data);
                // 下一页url
                nextPage_url = data.data.next_page_url;
                // 上一页url
                prev_page_url = data.data.prev_page_url;
                // 判断是否有下一页
                if(nextPage_url != null ){
                    pagebtn.show();
                }
            }else{
                toastr.error('查询不到该商品', '失败！');
            }
        }).fail(function (data) {
            toastr.error('查询不到该商品', '失败！')
        });
    }

    // 添加搜索商品到列表
    function loadProduct(data){
        var tbody = $('#search_tbody');
        tbody.empty();
        var html = "";
        var product = data.data.data;
        var len = product.length;

        for (var i = 0; i < len; i++) {
            html += '<tr>'
                 +  '<td class="number" >' + (i+1) + '</td>'
                 +  '<td class="productName" >' + product[i]['name'] + '</td>'
                 +  '<td class="productSnumber" >' + product[i]['snumber'] + '</td>'
                 +  '<td><a class="btn btn-sm ink-reaction btn-info btn-add" data-id="' + product[i]['id']+ ' ">选择</a></td>'
                 +  '<tr>';
        }
        tbody.append(html);
    }

    // 选择商品
    $(document).on('click', '.btn-add', function(){
        var product_id = $(this).attr('data-id');
        $('#resource').val(product_id);
        $('button[data-dismiss=modal]').click();
    })

    // 选择品牌/分类
    $(document).on('click', '#Catebtn', function(){
        var provider  = $('#provider').val();
        var resource_value;

        if ( provider == 1) {
            resource_value =  $("input:radio[name='categories']:checked").val();
        } else if(provider == 2){
            resource_value = $('#brandId').val();
        } else if (provider == 5) {
            resource_value = $('#categoryId').val();
        }
        $('#resource').val(resource_value);
        $('button[data-dismiss=modal]').click();
    })

    // 添加分类

    $('#brandId').change(function(){
        brandSelectChange();
    });

    $('#categoryId').change(function(){
        var tbody = $('#search_tbody');
        tbody.empty();
    });

    function labelStyle(_this){
        _this.css('top',0);
        _this.css('fontSize','12px');
    }

    function brandSelectChange(){
        var id = $('#brandId').val();
        var url = '{!! action("Admin\\PopularController@brandCategory") !!}';
        var tbody = $('#search_tbody');
        tbody.empty();
        if (! id) {
            return;
        }
        $.ajax({
          type: 'GET',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'brand_id':id},
        }).done(function(data) {
            if(data.status=='success'){
                loadCategory(data.data.categorys, data.data.brand_category);
            }else{
                 $('#search_tbody').empty();
                 toastr.error('查询不到该商品.', '失败!')
            }
        }).fail(function(data) {
            toastr.error('查询不到该商品.', '失败!')
        });
    }

    //加载品牌分类
    function loadCategory(category, brand_category){
        if(typeof category == 'undefined'){
            return false;
        }
        var len  = category.length;
        var html = '';

        for (var i = 0; i < brand_category.length; i++) {
            for (var j = 0; j < category.length; j++) {
                if (brand_category[i]['category_id'] == category[j]['id']) {
                    html += '<option value="'+brand_category[i]['id']+'">'+category[j]['name']+'</option>';
                }
            }
        }
        $('#categoryId').empty();
        $('#categoryId').append(html);

        labelStyle($('#categoryId').next());
    }

    // 隐藏选择框默认空选项
    $(function(){
        $('#provider').find('option:first').prop('style', 'display:none');
        $('#brandId').find('option:first').prop('style', 'display:none');
    })

    //全选
    $('.selectAll').live('click',function(){
        selectAll();
    });

    var selectAllStatus = 1;
    function selectAll()
    {
        if(selectAllStatus == 1){
            $.each($('.js-product'),function(){
                $(this).attr('checked','checked');
            })
            selectAllStatus = 0;
        }else{
            $.each($('.js-product'),function(){
                $(this).removeAttr('checked');
            })
            selectAllStatus = 1;
        }

    }

    // 批量移除
    $('.deleteProduct').click(function(){
        var products = $('.js-product');
        var ids = [];
        $.each(products,function(){
            if($(this).attr('checked') == 'checked'){
                ids.push($(this).val());
            }
        });

        if(ids.length <= 0){
            layer.msg('批量删除失败,请先选择类别',{icon: 0});
            return false;
        }
         layer.confirm('您确定删除这些推广？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                url: '{!! action("Admin\\PopularController@deleteAll") !!}',
                type: 'POST',
                data: {
                     ids : ids,
                     _token :"{!! csrf_token() !!}",
                },
                dataType: 'JSON',
                success: function (returnData) {
                    if (returnData.status == 'success') {
                        layer.msg('批量删除成功',{icon:1});
                        window.location.reload();
                    }else{
                       layer.msg('批量删除失败,请刷新重试',{icon: 0});
                    }
                }
             });
        });
    })

</script>
@stop
