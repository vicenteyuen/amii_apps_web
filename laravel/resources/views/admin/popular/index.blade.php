@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">推广列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @if ($populars->isEmpty())
                            @include('admin.popular.null')
                        @else
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>标题</th>
                                    <th>图片</th>
                                    <th>小程序图片</th>
                                    <th>链接类型</th>
                                    <th>排序权重（降序）</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($populars as $popular)
                                <tr>
                                    <td>{{ $popular->id }}</td>
                                    <td>{{ $popular->title }}</td>
                                    <td class="image-width-full" style="width: 192px;">
                                        @if ($popular->image)
                                        <img src="{{ $popular->image }}">
                                        @else
                                        无图
                                        @endif
                                    </td>
                                    <td class="image-width-full" style="width: 182px;">
                                        @if ($popular->weapp_image)
                                        <img src="{{ $popular->weapp_image }}">
                                        @else
                                        无图
                                        @endif
                                    </td>
                                    <td>
                                        @if($popular->provider == 'category')
                                            分类商品列表
                                        @elseif($popular->provider == 'brand')
                                            品牌商品列表
                                        @elseif($popular->provider == 'product')
                                            商品详情
                                        @elseif($popular->provider == 'html')
                                            跳转到页面
                                        @elseif($popular->provider == 'brandcate')
                                            品牌分类列表
                                        @else
                                            未定义
                                        @endif
                                    </td>
                                    <td>{{ $popular->weight }}</td>
                                    <td>
                                        {!! \HtmlFuncs::statusAdmin($popular->status) !!}
                                    </td>
                                    <td>
                                       @if($popular->status == 1)
                                            @php
                                                $btnType = 'btn-danger';
                                                $btnName = '停用';
                                            @endphp
                                       @else
                                            @php
                                                $btnType = 'btn-primary';
                                                $btnName = '启用';
                                            @endphp
                                       @endif

                                       <a href="javascript:void(0);" class="btn ink-reaction  {{$btnType}} btn-xs js_popular_status" type="button" data-status = "{{ $popular->status }}" data-id = "{{ $popular->id }}">{{$btnName}}</a>

                                       <a href="{{ Action('Admin\PopularController@showDetail', ['id' => $popular->id]) }}" class="btn ink-reaction btn-success btn-xs" type="button">编辑</a>

                                        <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-xs btn-danger js-delete" data-id="{{$popular->id}}" data-name="{{$popular->name}}" data-toggle="modal" data-target="#confirmModal">删除</a>

                                       </td>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if($populars->toArray()['data'])
                        <div class="col-md-12 col-lg-12">
                            <label style="color: black;font-size: 16px">当前页数量: {{$populars->toArray()['to'] - $populars->toArray()['from'] + 1}}</label>
                            <label style="color: black;font-size: 16px">总数量: {{$populars->toArray()['total']}}</label>
                        </div>
                        @endif
                        <div class="text-center">
                            {!! $populars->render() !!}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // 优惠卷停用
    $('.js_popular_status').live('click', function(){
        popularStatus($(this));
    })
    // 优惠卷停用
    function popularStatus(_this)
    {
        var status = _this.attr('data-status');
        if(status == '1'){
            var success      = '停用成功';
            var error        = '停用失败';
            var updateStatus = 0;
        }else{
            var success      = '启用成功';
            var error        = '启用失败';
            var updateStatus = 1;
        }
        $.ajax({
            url: '{{ action("Admin\\PopularController@status") }}',
            type: 'POST',
            data: {
                id     : _this.attr('data-id'),
                status : updateStatus,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');
                    console.log(status);
                    if(status == 1){

                        // 可启用状态
                        _this.parents('tr').find('i').addClass('text-red fa-close');
                        _this.parents('tr').find('i').removeClass('text-green fa-check');
                        _this.addClass('btn-primary');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',0);
                        _this.text('启用');
                    }else{
                        // 可停用状态
                        _this.parents('tr').find('i').addClass('text-green fa-check');
                        _this.parents('tr').find('i').removeClass('text-red fa-close');
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-primary');
                        _this.attr('data-status',1);
                        _this.text('停用');
                    }
                } else {
                    toastr.error(error, '失败');
                }
            }
        });

    };

    // 删除推广
    $('.js-delete').click(function(){
        var _this = $(this);
        layer.confirm('您确定删除 "'+_this.attr('data-name')+'" 这个推广？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\PopularController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

</script>
@stop

