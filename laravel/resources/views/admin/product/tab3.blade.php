<div class="tab-pane" id="tab3">
    <br><br>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label" >商品图片</label>
                <div id="uploader" style="margin-top: 20px">
                    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 imageTable" style="@if(empty($productImage) || $productImage->isEmpty())display: none;@endif">
            <div class="form-group">
                <label class="control-label" >商品图片列表：</label>
                <div style="margin-top: 20px">
                    <table class="table table-bordered no-margin table-striped table-hover Imagelist">
                        <tr align="center">
                        <th>#</th>
                        <th>图片</th>
                        <th>操作</th>
                        </tr>
                        @if(isset($productImage) &&!$productImage->isEmpty())
                            @foreach($productImage as $key => $image)
                                <tr align="center">
                                    <td class="key">{{$key+1}}</td>
                                    <td><img src="{{$image->image}}" width="50px" height="50px"></td>
                                    <td><a data-id ="{{$image->id}}" class="btn ink-reaction btn-danger btn-sm removeImage">删除</a></td>
                                <tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if(empty($product))
        <div class="row" style="margin-top: 10px;margin-left: 0px">
         <a href="javascript:void(0)" class="btn ink-reaction btn-success btn-sm addInvenotry text-right" type="button">新增库存</a>
        <!-- <a href="javascript:void(0)" class="btn ink-reaction btn-success btn-sm addScoreInvenotry text-right" type="button">新增积分库存</a> -->
        </div>
    @else 
        @foreach($product->sellProduct as $key => $value)
            <div class="row">
                <div class="col-xs-{{ $colsm or '12' }} col-sm-{{ $colsm or '12' }} col-md-{{ $collg or '12' }} col-lg-{{ $collg or '12' }}" id="inventory-table_{{$key}}" >
                </div>
            </div>
            <div class="row" style="margin-top: 10px;margin-bottom: 10px">
             <a style="margin-left: 10px" href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => $value->provider ])}}" class="btn ink-reaction btn-success btn-sm text-right" type="button">
             @if($value->provider == '1')
             编辑库存
             @else
             编辑积分库存
             @endif
             </a>
            </div>
        @endforeach
    @endif

</div><!--end #tab3 -->

