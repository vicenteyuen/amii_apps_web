<div class="tab-pane" id="tab2">
    <br><br>
    <div class="row">
    <input type="hidden" name='product_id' value="{{ $_GET['product_id'] or ''}}" id="product_id"> 
    <input type="hidden" name='sell_product_id' value="" id="sell_product_id">
    @include('admin.widget.textarea', [
            'colsm' => '12',
            'collg' => '12',
            'name' => 'detail',
            'id' => 'summernote',
            'title' => '',
            'value' => isset($product->detail)?$product->detail:''
        ])
    </div>    
</div><!--end #tab2 -->
