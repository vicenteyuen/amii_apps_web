<div class="tab-pane active" id="tab1">
    <br><br>
    <div class="row">
        @include('admin.widget.input', ['name' => 'name', 'id' => 'productName', 'title' => '商品名' ,'value' => isset($product->name )? $product->name : ''])
        @include('admin.widget.input', ['name' => 'snumber', 'id' => 'snumber', 'title' => '商品编号(不填系统自动分配)','value' => isset($product->snumber) ? $product->snumber : '' ] )
        @include('admin.widget.input', ['name' => 'unit', 'id' => 'unit', 'title' => '计量单位','value' => isset($product->unit) ? $product->unit: ''])
        @include('admin.widget.select3', [
            'colsm' => '12',
            'collg' => '6',
            'id'    => 'express_template',
            'name'  => 'express_template_id',
            'title' => '选择运费模版',
            'type'  => 'image',
            'value' =>  $expressTemplates,
        ])
        @include('admin.widget.input', ['name' => 'uploadType', 'id' => 'uploadType','type'=>'hidden'])
        @include('admin.widget.input', ['name' => 'selectImage', 'id' => 'selectImage','type'=>'hidden'])
        @include('admin.widget.image-upload', [
            'colsm'    => '6',
            'collg'    => '6',
            'position' => 'text-right',
            'id'       => 'productImages',
            'name'     => 'main_image',
            'descript' => '商品主图(尺寸:300x300)',
            'style'    => 'width:300px;height:300px'
        ])
        @include('admin.widget.textarea', [
            'colsm' => '12',
            'collg' => '6',
            'name' => 'brief',
            'id' => 'brief',
            'title' => '商品简介',
            'value' => isset($product->brief)?$product->brief:''
        ])

         @include('admin.widget.image-select-button', [
            'id' => 'idValue',
            'text' => '从图片库中选择图片',
        ])

        @include('admin.widget.radio', ['name' => 'status', 'id' => 'status', 'title' => '是否上架','value' => isset($product->status) ? $product->status : 1])

        @include('admin.widget.image-select')
        @include('admin.widget.image-select-tmpl')


    </div>
</div><!--end #tab1 -->
