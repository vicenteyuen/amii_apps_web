@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">商品列表</li>
        </ol>
    </div>

    <div class="section-body">
        <div class="card">
        <form action="{{action('Admin\ProductController@index')}}" method="GET" class="form floating-label">
            <div class="card-body">
             <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="col-md-8 col-lg-8">
                        @foreach($providers as $provider)
                            <div class="col-xs-2 col-md-2 col-lg-2 col-sm-2">
                                <label class="checkbox-inline checkbox-styled">
                                    <input class="providerCheckbox" name="provider[]" type="checkbox"
                                      @if($request->provider && in_array($provider['id'],$request->provider))
                                        checked='checked'
                                      @endif
                                     value="{{$provider['id']}}"><span class="">{{$provider['name']}}</span>
                                </label>
                            </div>
                          @endforeach
                    </div>

                    @if($request->provider)
                    <div class="col-md-4 col-lg-4">
                       <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                            @if(isset($request->status))
                                @if($request->status == 1)
                                    上架
                                @elseif($request->status == 0)
                                    下架
                                @elseif($request->status == 2)
                                    全部商品
                                @endif
                              @else
                                选择商品状态
                              @endif
                            <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                @foreach($status as $value)
                                     <li><a class="addProviderParam" href="{{action('Admin\ProductController@index',['provider' => $request->provider,'status' => $value,'sort' => $request->sort,'brandId' => $request->brandId,'page_number' => $request->page_number,'templateId' => $request->templateId,'search' => $request->search,'start' => $request->start,'end' => $request->end])}}">
                                         @if($value == 2)
                                            全部商品
                                         @elseif($value == 1)
                                            上架
                                         @elseif($value == 0)
                                            下架
                                         @endif
                                     </a></li>
                                @endforeach
                              </ul>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                       <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                            @if(isset($request->sort))
                                @if($request->sort == 'asc')
                                    升序
                                @elseif($request->sort == 'desc')
                                    降序
                                @endif
                              @else
                                按数量排序
                              @endif
                            <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                @foreach($sorts as $value)
                                     <li><a class="addProviderParam" href="{{action('Admin\ProductController@index',['provider' => $request->provider,'status' => $request->status,'sort' => $value,'brandId' => $request->brandId,'page_number' => $request->page_number,'templateId' => $request->templateId,'search' => $request->search ,'start' => $request->start,'end' => $request->end])}}">
                                         @if($value == 'asc')
                                            升序
                                         @elseif($value == 'desc')
                                            降序
                                         @endif
                                     </a></li>
                                @endforeach
                              </ul>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="col-md-6 col-lg-6" >
                    <div class="col-md-4 col-lg-4" >
                        <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                                @if(isset($request->templateId))
                                   @foreach($expressTemplates as $value)
                                        @if($request->templateId == $value->id)
                                            {{$value->name}}
                                            @break
                                        @endif
                                   @endforeach
                                  @else
                                    选择运费模版
                                  @endif
                                <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @foreach($expressTemplates as $value)
                                         <li><a class="addProviderParam" href="{{action('Admin\ProductController@index',['provider' => $request->provider,'status' => $request->status,'sort' => $request->sort,'templateId' => $value->id,'brandId' => $request->brandId,'page_number' => $request->page_number,'search' => $request->search,'start' => $request->start,'end' => $request->end])}}">
                                             {{$value->name}}
                                         </a></li>
                                    @endforeach
                                </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                                @if(isset($request->brandId))
                                   @foreach($brands as $value)
                                        @if($request->brandId == $value->id)
                                            {{$value->name}}
                                            @break
                                        @endif
                                   @endforeach
                                  @else
                                    选择品牌
                                  @endif
                                <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @foreach($brands as $value)
                                         <li><a class="addProviderParam" href="{{action('Admin\ProductController@index',['provider' => $request->provider,'status' => $request->status,'sort' => $request->sort,'templateId' => $request->templateId,'brandId' => $value->id,'page_number' => $request->page_number,'search' => $request->search,'start' => $request->start,'end' => $request->end])}}">
                                             {{$value->name}}
                                         </a></li>
                                    @endforeach
                                </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                                @if(isset($request->page_number))
                                   @foreach($pages as $value)
                                        @if($request->page_number == $value)
                                            {{$value}}
                                            @break
                                        @endif
                                   @endforeach
                                  @else
                                    选择分页商品数量
                                  @endif
                                <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @foreach($pages as $value)
                                         <li><a class="addProviderParam" href="{{action('Admin\ProductController@index',['provider' => $request->provider,'status' => $request->status,'sort' => $request->sort,'templateId' => $request->templateId,'brandId' => $request->brandId,'page_number' => $value,'search' => $request->search,'start' => $request->start,'end' => $request->end])}}">
                                             {{$value}}
                                         </a></li>
                                    @endforeach
                                </ul>
                        </div>
                    </div>
                </div>
                 <div class="col-md-6 col-lg-6" >
                    <div class="col-md-4 col-lg-4" >
                        <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 190px">
                                @if(isset($request->mainImage))
                                   @foreach($mainImage as $value)
                                        @if($request->mainImage == $value['id'])
                                            {{$value['name']}}
                                            @break
                                        @endif
                                   @endforeach
                                  @else
                                    主图筛选
                                  @endif
                                <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @foreach($mainImage as $value)
                                         <li><a class="addProviderParam" href="{{action('Admin\ProductController@index',['provider' => $request->provider,'status' => $request->status,'sort' => $request->sort,'mainImage' => $value['id'],'brandId' => $request->brandId,'page_number' => $request->page_number,'search' => $request->search,'start' => $request->start,'end' => $request->end,'templateId' => $request->templateId])}}">
                                             {{$value['name']}}
                                         </a></li>
                                    @endforeach
                                </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12" style="margin-top: 10px">
                      @include('admin.widget.select3', [
                          'colsm' => '4',
                          'collg' => '4',
                          'id'    => 'express_template',
                          'name'  => 'express_template_id',
                          'title' => '选择运费模版',
                          'type'  => 'image',
                          'value' =>  $expressTemplates,
                      ])
                      <div class="col-md-2 col-lg-2" style="margin-top: 22px">
                          <a href="javascript:void(0);" class="ink-reaction btn-primary btn-sm selectAll" type="button">全选</a>
                          <a href="javascript:void(0);" class="ink-reaction btn-success btn-sm addTemplate" type="button">提交</a>
                      </div>

                      <div class="col-md-6 col-lg-6">
                          <div class="input-group">
                              <div class="input-group-content">
                                  <input type="text" class="form-control" id="key" name="search" value="{{$request->search}}" placeholder="请输入商品名称或编号" style="text-indent: 10px">
                                  <div class="form-control-line" ></div>
                                  @include('admin.widget.datetimepicker', [
                                      'colsm' => '6',
                                      'collg' => '6',
                                      'title' => '开始时间',
                                      'name' => 'start',
                                      'class' => 'startDate',
                                      'style' => 'height:36px',
                                      'value' => $request->start?:''
                                  ])
                                  @include('admin.widget.datetimepicker', [
                                      'colsm' => '6',
                                      'collg' => '6',
                                      'title' => '结束时间',
                                      'name' => 'end',
                                      'class' => 'endDate',
                                      'style' => 'height:36px',
                                      'value' => $request->end?:''
                                  ])
                              </div>
                              <div class="input-group-btn">
                                  <button class="btn btn-floating-action btn-default-bright submit" type="submit" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                              </div>
                          </div>
                      </div>
                      @if(isset($request->provider) && in_array(1,$request->provider))
                      <div class="col-md-2 col-lg-2" style="margin-top: -10px;margin-bottom: 10px">
                          <a href="javascript:void(0);" class="ink-reaction btn-primary btn-sm selectAll" type="button">全选</a>
                          <a href="javascript:void(0);" class="ink-reaction btn-success btn-sm js-status" type="button" data-status="1">上架</a>
                           <a href="javascript:void(0);" class="ink-reaction btn-danger btn-sm js-status" type="button" data-status="0">下架</a>
                      </div>
                      @endif
                </div>
            </div>
        </form>
            @if($products->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$products->toArray()['to'] - $products->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$products->toArray()['total']}}</label>
                </div>
            @endif
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>id</th>
                            <th>商品名</th>
                            <th>商品编号</th>
                            <th>商品卖点</th>
                            <th>商品主图</th>
                            <th>运费模版名称</th>
                            <th>库存</th>
                            @if(isset($request->provider) && $request->provider)
                            <th>库存数量</th>
                            @endif
                            <th>更新时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        @php
                            // init vars
                            $hasStandardInventory =  0;
                            $hasPointInventory    =  0;
                            $hasGameInventory     =  0;
                            $hasGiftInventory     =  0;
                            $standardClass = 'btn-danger';
                            $pointClass = 'btn-danger';
                            $gameClass = 'btn-danger';
                            $giftClass = 'btn-danger';
                            $showTr = 0;
                            $sellProductNumber = '';
                            $sellProductImage = '';
                            $showInvetoryArr = [];
                            $singleInventoryClass = '';
                            $inventoryStatus = 0;
                            if(!$product->sellProduct->isEmpty()){
                                foreach($product->sellProduct as $value){
                                    $sellProductImage = '';
                                    if($value->provider == 1 && $hasStandardInventory == 0){
                                        $hasStandardInventory = 1;
                                        $sellProductImage = $value->smallImage;
                                        if($value->status == 1){
                                            $standardClass = 'btn-success';
                                        }
                                        if(isset($request->provider) && in_array(1,$request->provider)){
                                            $showTr = 1;
                                            $sellProductNumber .= '  '.$value->number;
                                            $showInvetoryArr['standard'] = 1;
                                            $singleInventoryClass = 1;
                                            $inventoryStatus = $value->status;
                                        }
                                    }
                                    if($value->provider == 2 && $hasPointInventory == 0){
                                        $hasPointInventory = 1;
                                        if($value->status == 1){
                                            $pointClass = 'btn-success';
                                        }
                                        if(isset($request->provider) && in_array(2,$request->provider)){
                                            $showTr = 1;
                                            $sellProductNumber .= '  '.$value->number;
                                            $showInvetoryArr['point'] = 2;
                                            $singleInventoryClass = 2;
                                            $inventoryStatus = $value->status;
                                        }
                                    }
                                    if($value->provider == 3 && $hasGameInventory == 0){
                                        $hasGameInventory = 1;
                                        if($value->status == 1){
                                            $gameClass = 'btn-success';
                                        }
                                        if(isset($request->provider) && in_array(3,$request->provider)){
                                            $showTr = 1;
                                            $sellProductNumber .= '  '.$value->number;
                                            $showInvetoryArr['game'] = 3;
                                            $singleInventoryClass = 3;
                                            $inventoryStatus = $value->status;
                                        }
                                    }
                                    if($value->provider == 4 && $hasGiftInventory == 0){
                                        $hasGiftInventory = 1;
                                        if($value->status == 1){
                                            $giftClass = 'btn-success';
                                        }
                                        if(isset($request->provider) && in_array(3,$request->provider)){
                                            $showTr = 1;
                                            $sellProductNumber .= '  '.$value->number;
                                            $showInvetoryArr['gift'] = 4;
                                            $singleInventoryClass = 4;
                                            $inventoryStatus = $value->status;
                                        }
                                    }
                                }
                            }

                        @endphp

                        @if(!isset($request->provider)|| $showTr)
                        <tr>
                            <td>
                              <label class="checkbox-inline checkbox-styled" >
                                <input name="product[]"  type="checkbox" value="{{$product->id}}" class=" js-product"><span></span>
                              </label>
                            </td>
                            <td>{{ $product->id }}</td>
                            <td width='18%'>{{ $product->name }}</td>
                            <td>{{ $product->snumber }}</td>
                            <td width='10%'>{{ $product->sell_point }}</td>
                            <td>
                            @if(isset($sellProductImage))
                                <img src="{{\App\Helpers\ImageHelper::getImageUrl($sellProductImage, 'product', 'small')}}" width="100px">
                            @else
                                无图片
                            @endif
                            </td>

                            <td>{{isset($product->expressTemplate->name)?$product->expressTemplate->name:'暂无'}}</td>
                            <td>

                            @if(!isset($request->provider))
                            <div class="btn-group">
                                <button type="button" class="btn ink-reaction btn-icon-toggle btn-primary" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-chevron-down"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    @if(!$hasStandardInventory)
                                            <li><a href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 1])}}" class="btn ink-reaction btn-xs" type="button">库存管理</a></li>

                                    @endif
                                    @if(!$hasPointInventory)
                                            <li> <a href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 2])}}" class="btn ink-reaction btn-xs" type="button">库存积分管理</a></li>

                                    @endif

                                    @if(!$hasGiftInventory)
                                            <li> <a href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 4])}}" class="btn ink-reaction btn-xs" type="button">礼品库存管理</a></li>

                                    @endif

                                    @php
                                        $puzzles = '';
                                        $gamePuzzles = '';
                                        foreach($product->sellProduct as $sellPro){
                                            if($sellPro->provider == 1){
                                                $gamePuzzles = $sellPro->game_puzzles;
                                                $marketPrice =  $sellPro->market_price;
                                                break;
                                            }
                                        }
                                    if($gamePuzzles){
                                        if(in_array($gamePuzzles,[4,6,9,12,16,25])){
                                            $puzzles =  $gamePuzzles.'适用拼图游戏';
                                        }
                                    }
                                    @endphp

                                    @if($puzzles)
                                         @if(!$hasGameInventory)
                                        <li> <a href="{{ action('Admin\\UserGameController@storeByGetMethod', ['product_id' => $product->id,'market_price' => $marketPrice,'puzzles' => $gamePuzzles])}}" class="btn ink-reaction btn-xs" type="button">游戏库存管理</a></li>
                                        @endif
                                    @endif

                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)" class="btn ink-reaction btn-xs" type="button">拼图块数
                                    {{$puzzles or "$gamePuzzles(不适用拼图游戏)"}}
                                    </a></li>

                                </ul>
                            </div>
                            @endif

                            @if($hasStandardInventory &&  (!isset($request->provider) || in_array(1,$request->provider)))
                                 <a href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 1])}}" class="btn ink-reaction {{$standardClass}} btn-xs" type="button">库存管理</a>

                            @endif

                            @if($hasPointInventory && (!isset($request->provider) || in_array(2,$request->provider)))
                                    <a href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 2])}}" class="btn ink-reaction  {{$pointClass}} btn-xs" type="button">库存积分管理</a>
                            @endif

                            @if($hasGameInventory && (!isset($request->provider) || in_array(3,$request->provider)))
                                 <a href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 3])}}" class="btn ink-reaction btn-xs {{$gameClass}}" type="button">游戏库存管理</a>
                            @endif

                            @if($hasGiftInventory && (!isset($request->provider) || in_array(4,$request->provider)))
                                    <a href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 4])}}" class="btn ink-reaction btn-xs {{$giftClass}} " type="button">礼品库存管理</a>

                            @endif

                            </td>
                            @if(isset($request->provider) && $request->provider)
                            <td>
                                {{$sellProductNumber}}
                            </td>
                            @endif
                            <td>
                                {{$product->updated_at}}
                            </td>
                            <td>
                            <a href="{{ action('Admin\ProductController@showEdit', ['product_id' => $product->id])}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                            @if(count($showInvetoryArr) == 1 && $singleInventoryClass == 1)
                              @if($inventoryStatus == 1)
                                <a href="javascript:void(0);" class="btn ink-reaction btn-danger btn-xs js-single-status" type="button" data-type = "{{$singleInventoryClass}}" data-product = "{{$product->id}}" data-status = "0">下架</a>
                              @else
                               <a href="javascript:void(0);" class="btn ink-reaction btn-success btn-xs js-single-status" type="button" data-type = "{{$singleInventoryClass}}" data-product = "{{$product->id}}" data-status = "1">上架</a>
                              @endif
                            @elseif(count($showInvetoryArr) > 1 && array_key_exists('standard',$showInvetoryArr) && in_array(1,$request->provider))
                              @if($standardClass == 'btn-success')
                                   <a href="javascript:void(0);" class="btn ink-reaction btn-danger btn-xs js-single-status" type="button" data-type = "1" data-product = "{{$product->id}}" data-status = "0">下架</a>
                              @else
                                  <a href="javascript:void(0);" class="btn ink-reaction btn-success btn-xs js-single-status" type="button" data-type = "1" data-product = "{{$product->id}}" data-status = "1">上架</a>
                              @endif
                            @elseif($hasStandardInventory &&  (!isset($request->provider) || in_array(1,$request->provider)))
                              @if($standardClass == 'btn-success')
                                   <a href="javascript:void(0);" class="btn ink-reaction btn-danger btn-xs js-single-status" type="button" data-type = "1" data-product = "{{$product->id}}" data-status = "0">下架</a>
                              @else
                                  <a href="javascript:void(0);" class="btn ink-reaction btn-success btn-xs js-single-status" type="button" data-type = "1" data-product = "{{$product->id}}" data-status = "1">上架</a>
                              @endif
                            @endif
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $products->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@include('admin.widget.modal-show')
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript">
    var _token = $('#_token').val();
    //商品上下架
    $('.js_product_status').live('click', function() {
        productStatus($(this));
    });

    //全选
    $('.selectAll').live('click',function(){
        selectAll();
    });

    var selectAllStatus = 1;
    function selectAll()
    {
        if(selectAllStatus == 1){
            $.each($('.js-product'),function(){
                $(this).attr('checked','checked');
            })
            selectAllStatus = 0;
        }else{
            $.each($('.js-product'),function(){
                $(this).removeAttr('checked');
            })
            selectAllStatus = 1;
        }

    }

    // 批量添加商品运费模版
    $('.addTemplate').click(function(){
        var products = $('.js-product');
        var ids = [];
        $.each(products,function(){
            if($(this).attr('checked') == 'checked'){
                ids.push($(this).val());
            }
        });

        if( !($('#express_template').val())){
            layer.msg('批量添加失败,请先选择运费模版',{icon: 0});
            return false;
        }
        if(ids.length <= 0){
            layer.msg('批量添加失败,请先选择商品',{icon: 0});
            return false;
        }
        $.ajax({
            url: '{!! action("Admin\\ProductController@updateTemplate") !!}',
            type: 'POST',
            data: {
                 ids : ids,
                 templateId :$('#express_template').val(),
                 _token :"{!! csrf_token() !!}",
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('批量添加成功',{icon:1});
                    window.location.reload();
                }else{
                   layer.msg('操作失败,请刷新重试',{icon: 0});
                }
            }
        });
    })


    //批量上下架
    $('.js-status').click(function(){
        @if(isset($request->provider) && in_array(1,$request->provider))
          var provider = 1;
        @elseif(count($request->provider) == 1)
          var provider = {{$request->provider[0]}};
        @else
          var provider = '';
        @endif
      
        if(!provider){
            layer.msg('批量上下架失败,当前商品不是单个类型或不包括普通商品',{icon: 0});
            return false;
        }
        var products = $('.js-product');
        var ids = [];
        var status = $(this).attr('data-status');
        $.each(products,function(){
            if($(this).attr('checked') == 'checked'){
                ids.push($(this).val());
            }
        });

        if(ids.length <= 0){
            layer.msg('批量上下架失败,请先选择商品',{icon: 0});
            return false;
        }
        $.ajax({
            url: '{!! action("Admin\\SellProductController@muliSellproductStatus") !!}',
            type: 'POST',
            data: {
                 ids : ids,
                 provider :provider,
                 status :status,
                 _token :"{!! csrf_token() !!}",
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('操作成功',{icon:1});
                    window.location.reload();
                }else{
                   layer.msg('操作失败,请刷新重试',{icon: 0});
                }
            }
        });
    })
    

    //单个商品上下架
    $('.js-single-status').click(function(){
        var _this = $(this);
        var status = _this.attr('data-status');
        if(status == '0'){
            var success = '下架成功';
            var error   = '下架失败';
        }else{
            var success = '上架成功';
            var error   = '上架失败';
        }
        $.ajax({
            url: '{!! action("Admin\\SellProductController@sellProductStatus") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-product'),
                _token      :  _token,
                status      :  _this.attr('data-status'),
                provider    :  _this.attr('data-type')
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');
                    if(status == 0){
                        // 下架操作
                        _this.addClass('btn-primary');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',1);
                        _this.text('上架');
                    }else{
                        // 上架操作
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-primary');
                        _this.attr('data-status',0);
                        _this.text('下架');
                    }
                }else{
                     toastr.error(error, '失败!')
                }
            }
        });

    })

    // 拼接url链接,加上provider
    $('.addProviderParam').click(function(){
        var _this = $(this);
        var href = _this.attr('href');
        var hrefArr = href.split('?');

        if( hrefArr.length <= 1 ){
            return true;
        }

        hrefArr[0] += '?';
        var hrefParaArr = hrefArr[1].split('&');
        for(var i = 0,j=0; i < hrefParaArr.length; i++){
            if(hrefParaArr[i].indexOf('provider') != -1){
                continue;
            }
            j++;
            if(j != 1){
              hrefArr[0] += '&';
            }
            hrefArr[0] += hrefParaArr[i];
        }

        $('input[name="provider[]"]:checked').each(function(){
           hrefArr[0]  += '&provider[]='+$(this).val();
        })
        _this.attr('href',hrefArr[0]);
        return true;
    })

    $(".startDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
    $(".endDate").datetimepicker({
        language:  'zh-CN',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: 1,
        todayBtn:  1,
        startDate: 0,
        minuteStep: 5,
        weekStart: 1,
        startView: 2,
        todayHighlight: 1,
        minView: 1,
        forceParse: 0,
    });
</script>
@stop
