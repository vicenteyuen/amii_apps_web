@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2-bootstrap.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop

@section('content')
<section>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="fromProduct" enctype="multipart/form-data" method="POST">
                      <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                      <input type="hidden" name="type" id="type" value="1">
                        <div class="form-wizard-nav">
                            <div class="progress" style="width: 75%;"><div class="progress-bar progress-bar-primary" style="width: 0%;"></div></div>
                            <ul class="nav nav-justified nav-pills">
                                <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">商品基本信息</span></a></li>
                                <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">商品详情</span></a></li>
                                <li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">商品库存</span></a></li>
                            </ul>
                        </div><!--end .form-wizard-nav -->
                        <div class="tab-content clearfix">
                            @include('admin.product.tab1')
                            @include('admin.product.tab2')
                            @include('admin.product.tab3')
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md">提交</button></li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>
<script src="/assets/lib/summernote/summernote.min.js"></script>
<script src="/assets/lib/summernote/lang/summernote-zh-CN.js"></script>
<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/plupload/i18n/zh_CN.js"></script>

<script src="/assets/lib/tmpl/tmpl.min.js"></script>
@stop

@section('script')

    <!-- 从图片库中选择 -->
    <script type="text/javascript">
         var page = 1;
         var object ={};
         var provider = 'product';
        $('#idValue').on('click', function () {
            $.ajax({
                url: "{!! action('Admin\\ImageController@pageImage') !!}",
                type: 'GET',
                data:{page:page, provider:provider,}
            })
            .done(function (data) {
                object.data = data;
                $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
                        Helper.modalToggle($('#modal-image-select'));
                });
         });


        // 上一页
         $('body').on('click','#prePage',function() {
            if(page >=1)--page;
            $.ajax({
                url: "{!! action('Admin\\ImageController@pageImage') !!}",
                type: 'GET',
                data:{page:page, provider:provider,}
            })
            .done(function (data) {
                object.data = data;
                if(data.length == 0){
                    layer.msg('已是首页',{icon: 0});
                    page++;
                    return false;
                }
                $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
            });
        })

        // 下一页
            $('body').on('click','#nextPage',function() {
            $.ajax({
                url: "{!! action('Admin\\ImageController@pageImage') !!}",
                type: 'GET',
                data:{page:++page, provider:provider,}
            })
            .done(function (data) {
                object.data = data;
                if(data.length ==0){
                    --page;
                    layer.msg('已是最后一页',{icon: 0});
                    return false;
                }
                $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
                // Helper.modalToggle($('#modal-image-select'));
            });
        })
    </script>

    <script type="text/javascript">
    $('#modal-image-select').on('click', '#imageSelectSubmit', function() {
        if(imageSelected !== undefined){
            var start = imageSelected.lastIndexOf('/')+1;
            var imageName = imageSelected.substring(start);
            $('#selectImage').val(imageName);
            Helper.setDefaultImg($('#uploadImg'),imageSelected);
        }
        //图片库
        $('#uploadType').val('imagesFolder');
        Helper.modalToggle($('#modal-image-select'));
    });
    </script>

<script type="text/javascript">
    var formUrl;
    var msg;
    var uploadUrl = "{!! action('Admin\\ProductImageController@store') !!}";
    var key = 0; //多张库存
    @if(isset($_GET['product_id']))
        formUrl = '{!! action("Admin\\ProductController@edits") !!}';
        msg = '编辑成功';
    @else
        formUrl = '{!! action("Admin\\ProductController@store") !!}';
        msg = '新增成功';
    @endif
    // 分类选择
    $('#categories_select').select2();
    // 上传主图
    @if(isset($_GET['product_id']))
        Helper.setDefaultImg($('#uploadImg'),"{{$product->main_image}}");
    @else
        Helper.setDefaultImg($('#uploadImg'));
    @endif

    // 点击输入框文件上传
    $("#productImages").change(function(){
        //点击上传
        Helper.readURL(this);
        $('#uploadType').val('inputFile');
    });

    // summnernote editor 商品详情
    $('#summernote').summernote({
        lang: 'zh-CN',
        height: 500,
        callbacks: {
            onImageUpload: function(files) {
                var length = files.length;
                for(var i = 0; i < length; i++ ){
                    sendFile(files[i]);
                }
            }
        }
    });

    $('#rootwizard').bootstrapWizard({
        'withVisible': true,
        'tabClass': 'nav nav-pills',
        'nextSelector': '.button-next',
        'previousSelector': '.button-previous',
        'onTabClick': function() {
            return false;
        },
        'onTabShow': onTabShow
    });

    // 上传商品图片
    function show(){
            $('#uploader').plupload({
            url :uploadUrl+'?_token='+_token+'&product_id=' + $('#product_id').val(),
            filters : [
                {title : "Image files", extensions : "jpg,gif,png"}
            ],
            rename: true,
            sortable: true,
            flash_swf_url : '/assets/lib/plupload/Moxie.swf',
            silverlight_xap_url : '/assets/lib/plupload/Moxie.xap',
            init:{
                //上传成功回调
                'FileUploaded':function(up,file,info){
                    $('.imageTable').css('display','block');
                    setUploadImag(info.response);
                },
            }
        });
    }

     // 删除库存图片
    $('.removeImage').live('click',function(){
        removeImage($(this));
    });

      // 删除图片
    function removeImage(_this){
        $.ajax({
            url: '{!! action("Admin\\ProductImageController@delete") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-id')
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                   layer.msg('图片删除成功',{icon: 1});
                    _this.parent().parent().remove();
                }else{
                   layer.msg(returnData.message,{icon: 0});
                }
            }
        });
    }

    // wizard on tab next
     $('#fromProduct').ajaxForm({
        url: formUrl,
        type: 'POST',
        dataType: 'json',
        async:false,
        success: function (returnData) {
            if (returnData.status == 'success') {
                layer.msg(msg, {icon: 1});
                if($('#product_id').val() == ''){
                     $('#product_id').val(returnData.data.id);
                     show();
                }
                $('#type').val(returnData.data.type);
                setTimeout(function(){
                    $('#rootwizard').bootstrapWizard('next');
                    if($('#tab3').hasClass('active')){
                        $(".btn-commit").css('display','none')
                    }else{
                        $(".btn-commit").css('display','block')
                    }
                },1000)
            }else{
                layer.msg(returnData.message,{icon: 0});
            }
        }
    });

    // wizard on tab show
    function onTabShow(tab, navigation, index){
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        $('#rootwizard').find('.progress-bar').css({width: percent + '%'});
        $('.form-wizard-horizontal').find('.progress').css({'width': percentWidth});
    }

    //upload
    function sendFile(file, editor, welEditable){
        var data = new FormData();
        data.append('file', file);
        data.append('_token', "{{ csrf_token() }}")
        $.ajax({
            url: '{!! action("Admin\\ProductController@upload") !!}',
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false
        })
        .done(function(url) {
            $('#summernote').summernote('insertImage', url, file.name);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

    @if(isset($product))
        show(); //显示上传插件
        @foreach($product->sellProduct as $key => $value)
            requestInventoryData("{{$value->id}}","{{$key}}");
        @endforeach
    @endif

    //库存数据
    function requestInventoryData($id,provider)
    {
        var sell_product_id = $id ;
        $.ajax({
            type: 'GET',
            url: '{!! action("Admin\\SellAttributeValueController@makeInventory") !!}',
            data: {id:sell_product_id},
            async: false
        }).done(function(data) {
            if(data.status=='success'){
                updateAttrValues(data.data.create,provider);
                updateInventory(data.data.inventory,provider);
            }else{
                 // 无属性值库存
                 makeTabel(provider);
                 getInventoryNumber($id,provider);
            }
        }).fail(function(data) {
            toastr.error('生成库存失败.', '失败!');
        });
    }

      // 生成没有属性的库存
    function makeTabel(provider){
        $('#inventory-table_'+provider).empty();
        var elementId = '0_0'
        var tableHtml = '';
        tableHtml +=  '<table class="table table-bordered no-margin table-striped table-hover" >'
                     +'<tr><th>#</th><th>市场价</th><th>会员价</th><th>库存</th><th>库存警告阀值</th><th>商品编号</th></tr>'
                     +'<tr align="center" class="addValueData js-add-data" data-key="0_0">'
                     +'<td>1</td>'
                     +'<td><input id="markPrice'+provider+'_'+elementId+'" type="text"></td>'
                     +'<td><input id="shopPrice'+provider+'_'+elementId+'" type="text"></td>'
                     +'<td><input id="number'+provider+'_'+elementId+'" type="text"></td>'
                     +'<td><input id="warnNumber'+provider+'_'+elementId+'" type="text"></td>'
                     +'<td><input id="skuCode'+provider+'_'+elementId+'" type="text"></td>'
                     +'</tr>';
                     +'</table>';

        $('#inventory-table_'+provider).append(tableHtml);
        $('.js-add-data').find('input').attr('readOnly','readOnly');
        // 编辑获取无属性的库存
    }

    // 编辑获取无属性的库存
    function getInventoryNumber($id,provider){
         $.ajax({
            type: 'GET',
            url: "{{ action('Admin\\InventoryController@index')}}",
            data: {'id':$id,'product_id':"{{ $_GET['product_id'] or '0' }}"}
        }).done(function(data) {
            if(data.status == 'success'){
                var elementId = '0_0';
                $("#markPrice"+provider+'_'+elementId).val(data.data.mark_price);
                $("#shopPrice"+provider+'_'+elementId).val(data.data.shop_price);
                $("#number"+provider+'_'+elementId).val(data.data.number);
                $("#warnNumber"+provider+'_'+elementId).val(data.data.warn_number);
                $("#skuCode"+provider+'_'+elementId).val(data.data.sku_code);
            }
        }).fail(function(data) {
        });
    }

    // 编辑时新增图片更新到列表
    function setUploadImag(info){
        info = JSON.parse(info);
        var lastkey = $('tr').last().find('.key');
        var html = '';
        if(lastkey.length == 0 ){
            lastkey = 1;
        }else{
            lastkey = parseInt(lastkey.text())+1;
        }
        html = '<tr align="center">'
                +'<td class="key">'+lastkey+'</td>'
                +'<td><img src="'+info.image+'" width="50px" height="50px"></td>'
                +'<td><a data-id ="'+info.id+'" class="btn ink-reaction btn-danger btn-sm removeImage">删除</a></td>'
                +'</tr>';

        $('.Imagelist').append(html);

    }
    // 生成库存属性表
    function updateAttrValues(attributes,provider){
        var valueArr = [];
        var lines = 1 ;

        for (var i = 0; i < attributes.length; i++) {
            // console.log(attributes[i].attribute_value.length);
            if (attributes[i].attribute_value) {
                lines = lines * attributes[i].attribute_value.length;
                valueArr.push(attributes[i].attribute_value.length);
            }
        }

        $('#inventory-table_' + key).empty();

        var htmlBuffer = [];
        htmlBuffer.push('<table class="table table-bordered no-margin table-striped table-hover">');
        htmlBuffer.push('<tr> ');
        htmlBuffer.push('<th>#</th>');
        for (var i = 0; i < attributes.length; i++) {
             htmlBuffer.push('<th>' + attributes[i].name + '</th>');
        }
        htmlBuffer.push('<th>图片</th><th>市场价</th><th>会员价</th><th>库存</th><th>库存警告阀值</th><th>商品编号</th></tr>');

        var indexArr =[];
        var linenum = 0;
        var elementId = "";
        var sell_attribute_values_id = [];
        for (var j = 0; j < lines; j++) {
            indexArr = getAttrIndexArr(valueArr,j)

            var valueIds = [];
            for (var i = 0; i < attributes.length; i++) {
                if (attributes[i] && attributes[i].attribute_value[indexArr[i]]) {
                    valueIds.push (parseInt(attributes[i].attribute_value[indexArr[i]].id));
                }
            }
            valueIds.sort(sortNumber);
            // console.log(valueIds);

            elementId = valueIds.join('_');

            linenum = j + 1;
            htmlBuffer.push('<tr class="addValueData" data-key="'+elementId+'">');
            htmlBuffer.push('<td>'+linenum+'</td>');
            for (var i = 0; i < attributes.length; i++) {
                htmlBuffer.push(' <td>'+attributes[i].attribute_value[indexArr[i]].value+'</td>');
            }

             htmlBuffer.push('<td><div class="thumbnail upload-display" style="width:80px;height:80px" ><input id="image'+provider+'_'+elementId+'" type="file">'+'<label for="image'+provider+'_'+elementId+'" ></label>'+
                '<img class="defaultInventoryImage_'+provider+'" id="uploadImage'+provider+'_'+elementId+'" data-src="holder.js/100%x180" src="" alt="error img"/></div></td>'); //图片

            htmlBuffer.push('<td><input id="markPrice'+provider+'_'+elementId+'" type="text"></td>');
            htmlBuffer.push('<td><input id="shopPrice'+provider+'_'+elementId+'" type="text"></td>');

            htmlBuffer.push('<td><input id="number'+provider+'_'+elementId+'" type="text"></td>');
            htmlBuffer.push('<td><input id="warnNumber'+provider+'_'+elementId+'" type="text"></td>');

            htmlBuffer.push('<td><input id="skuCode'+provider+'_'+elementId+'" type="text"></td>');

            htmlBuffer.push('</tr>');
        }

        htmlBuffer.push('</table>');

        var tableHtml= htmlBuffer.join('\n');
        $('#inventory-table_' + key).append(tableHtml);
        Helper.setDefaultImg($('.defaultInventoryImage_'+provider)); //设置图片缺省图
        $('#inventory-table_' + key).find('input').attr('disabled','disabled');
        key++;
    }

    // 填充库存数据
    function updateInventory(inventories,provider){
        // console.log(inventories);
        var inventory;
        var elementId = "";

        for (var i = 0; i < inventories.length; i++) {
            inventory = inventories[i];

            var valueIds = [];
            if (inventory.sku_value) {
                for (var j = 0; j < inventory.sku_value.length; j++) {
                    valueIds.push (parseInt(inventory.sku_value[j].id));
                }
            }
            valueIds.sort(sortNumber);
            elementId = valueIds.join('_');
            $("#markPrice"+provider+'_'+elementId).val(inventory.mark_price);
            $("#shopPrice"+provider+'_'+elementId).val(inventory.shop_price);
            $("#number"+provider+'_'+elementId).val(inventory.number);
            $("#warnNumber"+provider+'_'+elementId).val(inventory.warn_number);
            $("#skuCode"+provider+'_'+elementId).val(inventory.sku_code);
            Helper.setDefaultImg($('#uploadImage'+provider+'_'+elementId),inventory.image);
        }

    }

    // 新增库存按钮跳转
    if($('.addInvenotry').length > 0){
        $('.addInvenotry').click(function(){
            var product_id = $('#product_id').val();
            addSellProduct(product_id,1);
        });
    }

     // 新增积分库存按钮跳转
    // if($('.addScoreInvenotry').length > 0){
    //     $('.addScoreInvenotry').click(function(){
    //         var product_id = $('#product_id').val();
    //         addSellProduct(product_id,2);
    //     });
    // }

    //普通库存，积分库存
    function addSellProduct(product_id,provider){
        $.ajax({
            type: 'POST',
            url: '{!! action("Admin\\SellProductController@addSellType") !!}',
            data: {product_id:product_id,provider:provider},
            async: false
        }).done(function(data) {
            if(data.status=='success'){
                 window.location.href = '{!! action("Admin\\SellController@index") !!}'+'?product_id='+product_id+'&provider='+provider;
            }else{
                toastr.error('跳转页面失败,请刷新重试', '失败!');
            }
        }).fail(function(data) {
            toastr.error('跳转页面失败,请刷新重试', '失败!');
        });
    }

    function getAttrIndexArr(valueArr,total){
        // check overflow
        var max = 1;
        // get array
        var indexArr = [];
        var current = total;
        for (var i = 0; i < valueArr.length; i++) {
            var size = 1;
            for (var j = i+1; j < valueArr.length; j++) {
                size = size * valueArr[j];
            }
            indexArr[i] = parseInt( current / size) ;
            current = current - indexArr[i] * size;

            // console.log(indexArr[i]);
        }
        return indexArr;
    }

    function sortNumber(a,b) {
        return a - b;
    }
</script>
@stop
