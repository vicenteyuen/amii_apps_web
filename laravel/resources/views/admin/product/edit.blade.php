@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2-bootstrap.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop

@section('content')
<section>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="fromProduct" enctype="multipart/form-data" method="POST">
                      <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                      <input type="hidden" name="type" id="type" value="1">
                        <div class="tab-content clearfix">
                            <div class="row">
                                @include('admin.widget.input', ['name' => 'name', 'id' => 'productName', 'title' => '商品名' ,'value' => isset($product->name )? $product->name : ''])
                                @include('admin.widget.input', ['name' => 'snumber', 'id' => 'snumber', 'title' => '商品编号(不填系统自动分配)','value' => isset($product->snumber) ? $product->snumber : '' ] )
                                @include('admin.widget.input', ['name' => 'unit', 'id' => 'unit', 'title' => '计量单位','value' => isset($product->unit) ? $product->unit: ''])
                                @include('admin.widget.select3', [
                                    'colsm' => '12',
                                    'collg' => '6',
                                    'id'    => 'express_template',
                                    'name'  => 'express_template_id',
                                    'title' => '选择运费模版',
                                    'type'  => 'image',
                                    'value' =>  $expressTemplates,
                                    'expressId' => isset($product->express_template_id)?$product->express_template_id:''
                                ])
                                @include('admin.widget.input', ['name' => 'uploadType', 'id' => 'uploadType','type'=>'hidden'])
                                @include('admin.widget.input', ['name' => 'selectImage', 'id' => 'selectImage','type'=>'hidden'])
                                @include('admin.widget.image-upload', [
                                    'colsm'    => '6',
                                    'collg'    => '6',
                                    'position' => 'text-right',
                                    'id'       => 'productImages',
                                    'name'     => 'main_image',
                                    'descript' => '商品主图(尺寸:300x300)',
                                    'style'    => 'width:300px;height:300px'
                                ])
                                @include('admin.widget.textarea', [
                                    'colsm' => '12',
                                    'collg' => '6',
                                    'name' => 'brief',
                                    'id' => 'brief',
                                    'title' => '商品简介',
                                    'value' => isset($product->brief)?$product->brief:''
                                ])

                                 @include('admin.widget.image-select-button', [
                                    'id' => 'idValue',
                                    'text' => '从图片库中选择图片',
                                ])

                                @include('admin.widget.radio', ['name' => 'status', 'id' => 'status', 'title' => '是否上架','value' => isset($product->status) ? $product->status : 1])

                                @include('admin.widget.image-select')
                                @include('admin.widget.image-select-tmpl')


                            </div>
                            <div class="row">
                                <input type="hidden" name='product_id' value="{{ $_GET['product_id'] or ''}}" id="product_id"> 
                                <input type="hidden" name='sell_product_id' value="" id="sell_product_id">
                                @include('admin.widget.textarea', [
                                        'colsm' => '12',
                                        'collg' => '12',
                                        'name' => 'detail',
                                        'id' => 'summernote',
                                        'title' => '',
                                        'value' => isset($product->detail)?$product->detail:''
                                    ])
                                </div>    
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md">提交</button></li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>
<script src="/assets/lib/summernote/summernote.min.js"></script>
<script src="/assets/lib/summernote/lang/summernote-zh-CN.js"></script>
<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script src="/assets/lib/plupload/i18n/zh_CN.js"></script>

<script src="/assets/lib/tmpl/tmpl.min.js"></script>
@stop

@section('script')

    <!-- 从图片库中选择 -->
    <script type="text/javascript">
         var page = 1;
         var object ={};
         var provider = 'product';
        $('#idValue').on('click', function () {
            $.ajax({
                url: "{!! action('Admin\\ImageController@pageImage') !!}",
                type: 'GET',
                data:{page:page, provider:provider,}
            })
            .done(function (data) {
                object.data = data;
                $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
                        Helper.modalToggle($('#modal-image-select'));
                });
         });

         // summnernote editor 商品详情
        $('#summernote').summernote({
            lang: 'zh-CN',
            height: 500,
            callbacks: {
                onImageUpload: function(files) {
                    var length = files.length;
                    for(var i = 0; i < length; i++ ){
                        sendFile(files[i]);
                    }
                }
            }
        });

        //upload
        function sendFile(file, editor, welEditable){
            var data = new FormData();
            data.append('file', file);
            data.append('_token', "{{ csrf_token() }}")
            $.ajax({
                url: '{!! action("Admin\\ProductController@upload") !!}',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            })
            .done(function(url) {
                $('#summernote').summernote('insertImage', url, file.name);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        }

        // 上一页
         $('body').on('click','#prePage',function() {
            if(page >=1)--page;
            $.ajax({
                url: "{!! action('Admin\\ImageController@pageImage') !!}",
                type: 'GET',
                data:{page:page, provider:provider,}
            })
            .done(function (data) {
                object.data = data;
                if(data.length == 0){
                    layer.msg('已是首页',{icon: 0});
                    page++;
                    return false;
                }
                $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
            });
        })

        // 下一页
            $('body').on('click','#nextPage',function() {
            $.ajax({
                url: "{!! action('Admin\\ImageController@pageImage') !!}",
                type: 'GET',
                data:{page:++page, provider:provider,}
            })
            .done(function (data) {
                object.data = data;
                if(data.length ==0){
                    --page;
                    layer.msg('已是最后一页',{icon: 0});
                    return false;
                }
                $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
                // Helper.modalToggle($('#modal-image-select'));
            });
        })
    </script>

    <script type="text/javascript">
    $('#modal-image-select').on('click', '#imageSelectSubmit', function() {
        if(imageSelected !== undefined){
            var start = imageSelected.lastIndexOf('/')+1;
            var imageName = imageSelected.substring(start);
            $('#selectImage').val(imageName);
            Helper.setDefaultImg($('#uploadImg'),imageSelected);
        }
        //图片库
        $('#uploadType').val('imagesFolder');
        Helper.modalToggle($('#modal-image-select'));
    });
    </script>

<script type="text/javascript">
    var formUrl;
    var msg;
    var uploadUrl = "{!! action('Admin\\ProductImageController@store') !!}";
    var key = 0; //多张库存
    @if(isset($_GET['product_id']))
        formUrl = '{!! action("Admin\\ProductController@edits") !!}';
        msg = '编辑成功';
    @else
        formUrl = '{!! action("Admin\\ProductController@store") !!}';
        msg = '新增成功';
    @endif
    // 分类选择
    $('#categories_select').select2();
    // 上传主图
    @if(isset($_GET['product_id']))
        Helper.setDefaultImg($('#uploadImg'),"{{$product->main_image}}");
    @else
        Helper.setDefaultImg($('#uploadImg'));
    @endif

    // 点击输入框文件上传
    $("#productImages").change(function(){
        //点击上传
        Helper.readURL(this);
        $('#uploadType').val('inputFile');
    });

    // summnernote editor 商品详情
    $('#summernote').summernote({
        lang: 'zh-CN',
        height: 500,
        callbacks: {
            onImageUpload: function(files) {
                var length = files.length;
                for(var i = 0; i < length; i++ ){
                    sendFile(files[i]);
                }
            }
        }
    });

    // wizard on tab next
     $('#fromProduct').ajaxForm({
        url: formUrl,
        type: 'POST',
        dataType: 'json',
        async:false,
        success: function (returnData) {
            if (returnData.status == 'success') {
                layer.msg('编辑成功', {icon: 1});
                setTimeout(function(){
                    window.location.href = "{{action('Admin\\ProductController@index')}}";
                },1000)
            }else{
                layer.msg('编辑失败',{icon: 0});
            }
        }
    });

     // 上传商品图片
    function show(){
            $('#uploader').plupload({
            url :uploadUrl+'?_token='+_token+'&product_id=' + $('#product_id').val(),
            filters : [
                {title : "Image files", extensions : "jpg,gif,png"}
            ],
            rename: true,
            sortable: true,
            flash_swf_url : '/assets/lib/plupload/Moxie.swf',
            silverlight_xap_url : '/assets/lib/plupload/Moxie.xap',
            init:{
                //上传成功回调
                'FileUploaded':function(up,file,info){
                    $('.imageTable').css('display','block');
                    setUploadImag(info.response);
                },
            }
        });
    }

    @if(isset($product))
        show(); //显示上传插件
        @foreach($product->sellProduct as $key => $value)
            requestInventoryData("{{$value->id}}","{{$key}}");
        @endforeach
    @endif

</script>
@stop
