@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">游戏商品列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                 <form action="" method="GET" class="form floating-label">
                    <div class="input-group">
                        <div class="input-group-content">
                            <input type="text" class="form-control" id="key" name="search" value="" placeholder="请输入商品名称或编号">
                            <label for="search"></label>
                            <div class="form-control-line"></div>
                        </div>
                        <div class="input-group-btn">
                            <button class="btn btn-floating-action btn-default-bright submit" type="submit" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                        </div>
                    </div>
                </form>
                <div class="form-group floating-label">
                   <a href="{{ action('Admin\UserGameController@create')}}" class="btn ink-reaction btn-success btn-sm pull-right">
                    添加游戏商品
                 </a>
                  <a style="margin-right: 20px" href="{!! action('Admin\UserGameAdviceController@index') !!}" class="btn ink-reaction btn-primary btn-sm pull-right">
                    用户游戏反馈
                 </a>
                </div>
                <div class="form-group floating-label">
                    <label for="name">活动商品数量限制/日</label>
                    <input type="text"  class="form-control" id="rule" name="rule" value="{{isset($activity->rule)? $activity->rule : ''}}">
                </div>
                <a href="javascript:void(0);" class="btn ink-reaction btn-primary btn-sm js-game-rule">
                    确认
                </a>
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>商品名</th>
                            <th>商品编号</th>
                            <th>商品主图</th>
                            <th>拼图个数</th>
                            <th>正在玩游戏人数</th>
                            <th>已完成人数</th>
                            <th>总库存</th>
                            <th>剩余库存</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($sellProducts as $sellProduct)
                        <tr>
                            <td>
                              <label class="checkbox-inline checkbox-styled" >
                                <input name="sellProduct[]"  type="checkbox" value="{{$sellProduct->id}}" class=" js-product"><span></span>
                              </label>
                            </td>
                            <td>{{ $sellProduct->product->name }}</td>
                             <td>{{ $sellProduct->product->snumber}}</td>
                            <td>
                            @if(isset($sellProduct->all_image[0]))
                            <img src="{{$sellProduct->all_image[0]}}" width="50px">
                            @else
                            暂无图片
                            @endif
                            </td>
                            <td>{{$sellProduct->puzzles}}</td>
                            <td>{{$sellProduct->game_number}}</td>
                            <td>{{$sellProduct->success_number}}</td>
                            <td>{{$sellProduct->inventory_number}}</td>
                            <td>
                                {{$sellProduct->number}}
                            </td>
                            <td>
                           @if($sellProduct->status == 1)
                                @php
                                    $btnType = 'btn-danger';
                                    $btnName = '下架';
                                @endphp
                           @else
                                @php
                                    $btnType = 'btn-primary';
                                    $btnName = '上架';
                                @endphp
                           @endif
                            <a href="javascript:void(0);" class="btn ink-reaction {{$btnType}} btn-xs js_product_status" type="button" data-status = "{{$sellProduct->status}}" data-id = "{{$sellProduct->id}}">{{$btnName}}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                 <div class="col-md-3 col-lg-3" style="margin-top: 22px">
                        <a href="javascript:void(0);" class="ink-reaction btn-primary btn-sm selectAll" type="button">全选</a>
                        <a href="javascript:void(0);" class="ink-reaction btn-danger btn-sm js-product-status" type="button" data-status='0'>下架</a>
                        <a href="javascript:void(0);" class="ink-reaction btn-success btn-sm js-product-status" type="button" data-status='1'>上架</a>
                </div>
                @if($sellProducts->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$sellProducts->toArray()['to'] - $sellProducts->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$sellProducts->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                    {!! $sellProducts->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var _token = $('#_token').val();
    //商品上下架
    $('.js_product_status').live('click', function() {
        productStatus($(this));
    });

    //全选
    $('.selectAll').live('click',function(){
        selectAll();
    });

    var selectAllStatus = 1;
    function selectAll()
    {
        if(selectAllStatus == 1){
            $.each($('.js-product'),function(){
                $(this).attr('checked','checked');
            })
            selectAllStatus = 0;
        }else{
            $.each($('.js-product'),function(){
                $(this).removeAttr('checked');
            })
            selectAllStatus = 1;
        }

    }

    //product上下架
    function productStatus(_this)
    {
        var status = _this.attr('data-status');
        if(status == '1'){
            var success = '下架成功';
            var error   = '下架失败';
            var updataStatus = 0;
        }else{
            var success = '上架成功';
            var error   = '上架失败';
            var updataStatus = 1;
        }
        $.ajax({
            url: '{!! action("Admin\\SellProductController@gameStatus") !!}',
            type: 'POST',
            data: {
                 id         :  _this.attr('data-id'),
                _token      :  _token,
                status      :  updataStatus
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success(success, '成功');
                    if(status == 1){
                        // 可上架状态
                        _this.addClass('btn-primary');
                        _this.removeClass('btn-danger');
                        _this.attr('data-status',0);
                        _this.text('上架');
                    }else{
                        // 可下架状态
                        _this.addClass('btn-danger');
                        _this.removeClass('btn-primary');
                        _this.attr('data-status',1);
                        _this.text('下架');
                    }
                }else{
                     toastr.error(error, '失败!')
                }
            }
        });
    }

    $('.js-game-rule').click(function(){
         $.ajax({
            url: '{!! action("Admin\\ActivityController@update") !!}',
            type: 'POST',
            data: {
                provider : 'game',
                rule     :  $('#rule').val(),
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    toastr.success('修改成功', '成功');
                    window.location.reload();
                }else{
                     toastr.error('修改失败', '失败!');
                }
            }
        });
    })

    // 批量上下架商品
    $('.js-product-status').click(function(){
        var products = $('.js-product');
        var ids = [];
        var status = $(this).attr('data-status');
        $.each(products,function(){
            if($(this).attr('checked') == 'checked'){
                ids.push($(this).val());
            }
        });

        if(ids == ''){
            layer.msg('操作失败,请先选择游戏商品',{icon: 0});
            return false;
        }
        $.ajax({
            url: '{!! action("Admin\\SellProductController@muliGameStatus") !!}',
            type: 'POST',
            data: {
                 ids : ids,
                 status :status,
                 _token :"{!! csrf_token() !!}",
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('操作成功',{icon:1});
                    window.location.reload();
                }else{
                   layer.msg('操作失败,请刷新重试',{icon: 0});
                }
            }
        });
    })
</script>
@stop
