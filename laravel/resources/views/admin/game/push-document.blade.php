@extends('admin.widget.body')

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">游戏推送方案</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>游戏文案</th>
                                    <th>游戏状态</th>
                                    <th>倒计时</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($documents as $document)
                                    <tr>
                                        <td>{{ $document->id }}</td>
                                        <td>{{ $document->desc }}</td>
                                        <td>
                                        @if($document->provider == 1)
                                            进行中(好友激活)
                                        @elseif($document->provider == 2)
                                            进行中，未完成
                                        @elseif($document->provider == 3)
                                            未合成
                                        @elseif($document->provider == 4)
                                            完成需合成
                                        @elseif($document->provider == 5)
                                            超时(未完成)
                                        @elseif($document->provider == 6)
                                            超时(未合成)
                                        @else
                                            无
                                        @endif
                                        </td>
                                        <td>
                                        @if($document->hours)
                                            {{$document->hours}}小时
                                        @else
                                            无
                                        @endif
                                        </td>
                                        <td>
                                            <a href="{{ action('Admin\GamePushDocumentController@showEdit', ['id' => $document->id])}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')

<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop
