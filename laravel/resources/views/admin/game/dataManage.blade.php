@extends('admin.widget.body')

@section('style_link')
<link type="text/css" rel="stylesheet" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css">
@stop

@section('content')
    <section>
        <div class="section-body">
            <div class="row">
             <form class="form floating-label" autocomplete="off" id="fromProduct" action = "{{action('Admin\UserGameController@manager')}}" enctype="multipart/form-data" method="get">
             @include('admin.widget.datetimepicker', [
                'colsm' => '6',
                'collg' => '6',
                'title' => '筛选时间',
                'name' => 'date',
                'class' => 'startDate',
            ])
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin-top: 10px">
                <div class="form-group text-left">
                    <button type="submit" class='btn btn-primary btn-sm'>确认</button>
                </div>
            </div>
            </form>
            </div>
            <div class="row">
                <!-- BEGIN ALERT - REVENUE -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>--}}
                                <strong class="text-xl">{{$data['joinGamePerson']}}</strong><br/>
                                <span class="opacity-50">发起人数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - REVENUE -->

                <!-- BEGIN ALERT - VISITS -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-warning text-lg">0,01% <i class="md md-swap-vert"></i></strong>--}}
                                <strong class="text-xl">{{$data['gameCollectionPreson']}}</strong><br/>
                                <span class="opacity-50">点亮人数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - VISITS -->

                <!-- BEGIN ALERT - TIME ON SITE -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>--}}
                                <strong class="text-xl">{{$data['takePartIn']}}</strong><br/>
                                <span class="opacity-50">总参与人数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - TIME ON SITE -->
            </div><!--end .row -->

             <div class="row">
                <!-- BEGIN ALERT - REVENUE -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>--}}
                                <strong class="text-xl">{{$data['sendPerson']}}</strong><br/>
                                <span class="opacity-50">送出人数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - REVENUE -->

                <!-- BEGIN ALERT - VISITS -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-warning text-lg">0,01% <i class="md md-swap-vert"></i></strong>--}}
                                <strong class="text-xl">{{$data['sendGameTime']}}</strong><br/>
                                <span class="opacity-50">送出件数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - VISITS -->

                <!-- BEGIN ALERT - TIME ON SITE -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>--}}
                                <strong class="text-xl">{{$data['personPerGetTime']}}</strong><br/>
                                <span class="opacity-50">人均获得件数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - TIME ON SITE -->
            </div><!--end .row -->

             <div class="row">
                <!-- BEGIN ALERT - REVENUE -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>--}}
                                <strong class="text-xl">{{$data['joinGameTime']}}</strong><br/>
                                <span class="opacity-50">已发起次数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - REVENUE -->

                <!-- BEGIN ALERT - VISITS -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<strong class="pull-right text-warning text-lg">0,01% <i class="md md-swap-vert"></i></strong>--}}
                                <strong class="text-xl">{{$data['personPerGetTime']}}</strong><br/>
                                <span class="opacity-50">人均发起次数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - VISITS -->

                <!-- BEGIN ALERT - TIME ON SITE -->
                <div class="col-md-4 col-sm-4">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                {{--<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>--}}
                                <strong class="text-xl">{{$data['unCommit']}}</strong><br/>
                                <span class="opacity-50">待合成次数</span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div><!--end .col -->
                <!-- END ALERT - TIME ON SITE -->
            </div><!--end .row -->

            <div class="card">
                <div class="card-head">
                    <header>每日数量统计</header>
                </div><!--end .card-head -->
                <div class="card-body">
                    <canvas id="history"></canvas>
                </div><!--end .card-body -->
            </div><!--end .card -->

        </div>
    </section>
@stop

@section('script_link')
    <script src="//cdn.bootcss.com/Chart.js/2.5.0/Chart.bundle.min.js"></script>
    <script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/assets/lib/datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
@stop

@section('script')
    <script>

        $(".startDate").datetimepicker({
            language:  'zh-CN',
            format: 'yyyy-mm-dd hh:ii',
            autoclose: 1,
            todayBtn:  1,
            startDate: 0,
            minuteStep: 5,
            weekStart: 1,
            startView: 2,
            todayHighlight: 1,
            minView: 1,
            forceParse: 0,
        });
        new Chart(document.getElementById('history'), {
            type: 'line',
            data: {
                labels: {!! json_encode($perData['labels']) !!},
                datasets: [
                    {
                        label: "每日发起次数",
                        fill: false,
                        backgroundColor: "rgba(255, 99, 132, 0.4)",
                        borderColor: "rgba(255, 99, 132,1)",
                        data: {!! json_encode($perData['perTime']) !!},
                    },
                    {
                        label: "每日发起人数",
                        fill: false,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        data: {!! json_encode($perData['perPerson']) !!},
                    },
                    {
                        label: "每日送出件数",
                        fill: false,
                        backgroundColor: "rgba(75,50,50,0.4)",
                        borderColor: "rgba(75,50,50,1)",
                        data: {!! json_encode($perData['sendProduct']) !!},
                    }
                ]
            },
            options: {

            }
        });
    </script>
@stop
