@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">
                游戏
                @if($document->provider == 1)
                    进行中(好友激活)
                @elseif($document->provider == 2)
                    进行中，未完成
                @elseif($document->provider == 3)
                    未合成
                @elseif($document->provider == 4)
                    完成需合成
                @elseif($document->provider == 5)
                    超时(未完成)
                @elseif($document->provider == 6)
                    超时(未合成)
                @endif
                文案编辑
            </li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
            <div class="row">
            {!! Form::open(['action' => 'Admin\GamePushDocumentController@edit', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                @if(in_array($document->provider,[2,3]))
                    @include('admin.widget.input', [
                        'colsm' => '12',
                        'collg' => '12',
                        'name'  => 'hours',
                        'id'    => 'hours',
                        'title' => '倒计时',
                        'type'  => 'text',
                        'value' => isset($document->hours) ? $document->hours : '',
                    ])
                @endif
                @include('admin.widget.textarea', [
                    'colsm' => '12',
                    'collg' => '12',
                    'name'  => 'desc',
                    'id'    => 'desc',
                    'title' => '文案内容',
                    'placeholder' => '点击此处编辑自定义文案内容',
                    'value' => isset($document->desc) ? $document->desc : '',
                 ])
                 <input type="hidden" name="id" value="{{$document->id}}">
                 <input type="hidden" name="provider" value="{{$document->provider}}">
                </div>
                 <div style="background: #ccc;padding: 20px;margin-bottom: 20px;font-size:18px" >
                    <p>系统预留变量</p>
                    <p style="color: red">
                        @if($document->provider == 1)
                            <span>{name}</span> <span style="color: black">激活用户</span><br/>
                            <span>{number}</span><span style="color: black">游戏剩余块数</span><br/>
                        @elseif($document->provider == 2)
                             <span>{productName}</span> <span style="color: black">商品名称</span><br/>
                             <span>{productSnumber}</span> <span style="color: black">款号</span><br/>
                             <span>{remainTime}</span> <span style="color: black">剩余时间</span><br/>
                             <span>{number}</span><span style="color: black">游戏剩余块数</span><br/>
                        @elseif($document->provider == 3)
                             <span>{productName}</span> <span style="color: black">商品名称</span><br/>
                             <span>{productSnumber}</span> <span style="color: black">款号</span><br/>
                             <span>{remainTime}</span> <span style="color: black">剩余时间</span><br/>
                        @elseif(in_array($document->provider,[4,5,6]))
                            <span>{productName}</span> <span style="color: black">商品名称</span><br/>
                            <span>{productSnumber}</span> <span style="color: black">款号</span><br/>
                        @endif
                    </p>
                </div>
                <div class="row text-right btn-commit">
                    <div class="col-md-12">
                    <ul class="list-unstyled">
                        <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md js-commit">
                         提交
                        </button></li>
                    </ul>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop
