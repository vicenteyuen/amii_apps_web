@extends('admin.widget.body')

@section('style_link')

<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">

@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加游戏商品</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">搜索商品</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="card-body">
                             <form action="" method="GET" class="form floating-label">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入商品名称或产品编号">
                                        <label for="search"></label>
                                        <div class="form-control-line"></div>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-floating-action btn-default-bright submit" type="submit" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                    </div>
                                </div>
                            </from>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>商品名称</th>
                                            <th>商品编号</th>
                                            <th>市场价格</th>
                                            <th>游戏格数</th>
                                            <th style="width: 80px;">操作</th>
                                        </tr>

                                    </thead>
                                    <tbody id="search_tbody">
                                        @foreach($products as $key => $product)
                                            <tr>
                                               <td class="number">{{$key+1}}</td>
                                               <td class="productName">{{$product->name}}</td>
                                                <td class="productSnumber">{{$product->snumber}}</td>
                                               <td class="productMarketPrice">{{$product->sellProductOne->market_price}}</td>
                                               <td class="puzzle"><input type="name" value="{{
                                              $product->sellProductOne->game_puzzles}}"></td>
                                                   @if($product->sellProductGame)
                                                    <td><a class="btn btn-sm ink-reaction btn-info" data-id="{{$product->id}}" href="{{ action('Admin\\SellController@edits', ['product_id' => $product->id,'provider' => 3])}}" >库存管理</a></td>
                                                   @else
                                                    <td><a  class="btn btn-sm ink-reaction btn-info btn-add" data-id="{{$product->id}}" >添加</a></td>
                                                   @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- ./card-body -->
                          <div class="text-center">
                            {{$products->render()}}
                        </div>
                        </div>
                    </div>
                </div><!-- ./col-lg-6 -->
            </div><!-- ./row -->
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).on('click','.btn-add',function(){
        addProduct($(this));
    });

    //添加游戏商品
    function addProduct(_this){
        var product_id  = _this.attr('data-id');
        var market_price = _this.parent().parent().find('.productMarketPrice').text();
        var puzzles = _this.parent().parent().find('.puzzle').find('input').val();
        var check = checkPuzzles(puzzles);
        if(!check){
            toastr.error('游戏商品格数不是在4,6,9,12,16,20,25之中', '失败!')
            return false;
        }

        var url = '{!! action("Admin\\UserGameController@store") !!}';
        $.ajax({
          type: 'POST',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'product_id':product_id,'market_price':market_price,'base_price':market_price,'puzzles':puzzles},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('添加成功', '成功!');
                window.location.href = data.data.url;
            }else{
                 toastr.error('添加失败', '失败!')
            }
        }).fail(function(data) {
            toastr.error('添加失败,请刷新重试.', '失败!')
        });
    }

    function checkPuzzles(number)
    {
        if(number == 4 || number == 6 || number == 9 ||number == 12 || number == 16 || number == 20 || number == 25){
            return true;
        }
        return false;

    }

</script>
@stop
