@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">
                @if (!isset($showDetail))
                    添加文案
                @else
                    编辑文案
                @endif

            </li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @if (!isset($showDetail))
                        {!! Form::open(['action' => 'Admin\ShareController@store', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                    @else
                        {!! Form::open(['action' => 'Admin\ShareController@edits', 'class' => 'form', 'enctype' => 'multipart/form-data']) !!}
                        <input type="hidden" id="id" name="id" value="{{isset($showDetail) && $showDetail->id ? $showDetail->id : ''}}">
                    @endif

                    @include('admin.widget.select', [
                        'colsm' => '12',
                        'collg' => '6',
                        'id' => 'provider',
                        'name' => 'provider',
                        'title' => '分享端口位置',
                        'selected' => old('provider') ? old('provider') : (isset($showDetail->provider_value) ? $showDetail->provider_value : ''),
                        'values' => [
                            '单品页' => '1',
                            '社区专题' => '2',
                            '社区视频' => '3',
                            '用户二维码' => '4',
                            '用户部落' => '5',
                            '首单7折活动' => '6',
                            '拼图送衣游戏' => '7',
                            '拼图送衣游戏首页' => '8',
                            '红包分享' => '9',
                            '首页分享' => '10',
                        ],

                    ])

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <p></p>
                            <div class="input-group ">
                             <div class="input-group-btn">
                                <a id="addShare" class='btn btn-success pull-left' >选择分享对象</a>
                            </div>
                            </div>
                        </div>
                         @if(isset($showDetail))
                           @include('admin.widget.input', [
                                'colsm' => '12',
                                'collg' => '6',
                                'id' => 'hiddenProvider',
                                'name' => 'hiddenProvider',
                                'type'  => 'hidden',
                                'value' => isset($showDetail->provider_value) ? $showDetail->provider_value : '',
                            ])
                        @endif

                         <div id="showShareAll">
                        @include('admin.widget.radio', [
                            'name' => 'is_shareAll',
                            'id' => 'is_shareAll',
                            'title' => '是否应用到分享端口所以对象',
                            'value' => old('is_shareAll') ? old('is_shareAll') : (isset($showDetail->is_shareAll)? $showDetail->is_shareAll : 0 ),
                        ])
                        </div>
                        @include('admin.widget.radio', [
                            'name' => 'is_defaultTitle',
                            'id' => 'is_defaultContent',
                            'title' => '是否使用默认标题',
                            'value' => old('is_defaultTitle') ? old('is_defaultTitle') : (isset($showDetail->is_defaultTitle)? $showDetail->is_defaultTitle :0 ),
                        ])
                        <div id="showTitle">
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name'  => 'title',
                            'id'    => 'title',
                            'title' => '标题',
                            'type'  => 'text',
                            'value' => old('title') ? old('title') : (isset($showDetail->title) ? $showDetail->title : ''),
                        ])
                        </div>

                        @include('admin.widget.radio', [
                            'name' => 'is_defaultContent',
                            'id' => 'is_defaultContent',
                            'title' => '是否使用默认内容',
                            'value' => old('is_defaultContent') ? old('is_defaultContent') : (isset($showDetail->is_defaultContent)? $showDetail->is_defaultContent :0 ),
                        ])
                        <div id="showTextarea">
                        @include('admin.widget.textarea', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name'  => 'content',
                            'id'    => 'content',
                            'title' => '文案内容',
                            'placeholder' => '点击此处编辑自定义文案内容',
                            'attribute'   => 'attributeValue',
                            'value' => old('content') ? old('content') : (isset($showDetail->content) ? $showDetail->content : ''),
                         ])
                        </div>
                        @include('admin.widget.radio', [
                            'name' => 'is_defaultImg',
                            'id' => 'is_defaultImg',
                            'title' => '是否使用默认图片',
                            'value' => old('is_defaultImg') ? old('is_defaultImg') : (isset($showDetail->is_defaultImg)? $showDetail->is_defaultImg :0 ),
                        ])
                        <div id="showImg">
                        @include('admin.widget.image-upload', [
                            'colsm' => '12',
                            'collg' => '6',
                            'class' => 'puload-display-md-weapp-mobile',
                            'id'    => 'shareImg',
                            'imgId' => 'shareImgId',
                            'name'  => 'image',
                            'style'=>'width:344px;height:238px;',
                            'descript' => '文案内容图片(分辨率: 300x300 )',
                        ])
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <input type="text" id="shareSelectIds" hidden name="shareSelectIds" value="{{ old('shareSelectIds') ? old('shareSelectIds') : (isset($showDetail) && $showDetail->shareSelectIds ? $showDetail->shareSelectIds : '') }}"/>

                            <div class="input-group text-right">
                             <div class="input-group-btn">
                                <button type="submit" class='btn btn-success pull-right'>保存</button>
                            </div>
                            </div>
                        </div>
                        <!--选择分享对象start-->
                          <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="form-group tableShare">
                            <table id = "tableShare" class="table table-hover table-condensed table-striped no-margin">
                                <leble for = "tableShare"  class="control-label">已选择分享对象列表</lable>
                                <thead >
                                    <tr >
                                        <th >#id</th>
                                        <th class="tdName">名称</th>
                                        <th class="tdNumber">商品编号</th>
                                        <th >操作</th>
                                    </tr>
                                </thead>
                                <tbody id="Selected_tbody">

                                </tbody>
                            </table>

                            </div>
                            </div>
                        </div>
                        <!--选择分享对象end -->

                        <!-- 模态框（Modal） -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content" style="max-height:calc(100vh - 140px); overflow-y: scroll;">
                                    <div class="modal-header">
                                        <button type="button" id='modalClose' class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">
                                            选择分享对象
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-body">

                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 searchPorduct ">
                                            <div class="input-group">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入">
                                                    <label for="search" id="searchLable">请输入商品名称或产品编号</label>
                                                    <div class="form-control-line"></div>
                                                </div>
                                                <div class="input-group-btn">
                                                    <button class="btn btn-floating-action btn-default-bright" type="button" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                                </div>
                                            </div>
                                            </div>

                                            <table class="table table-striped table-hover dataTable no-footer searchPorduct">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th class="tdName">商品名称</th>
                                                        <th class="tdNumber">商品编号</th>
                                                        <th style="width: 80px;">操作</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="search_tbody">

                                                </tbody>
                                            </table>
                                       </div>
                                        <!-- ./card-body -->

                                    </div>

                                </div><!-- /.modal-content -->

                                    <div class="text-right" style="margin: 6px 20px 20px 0;" >
                                        <button type="button" id="prePage" class="btn btn-sm btn-default  pagebtn">上一页</button>
                                        <button type="button"  id="nextPage" class="btn btn-sm btn-default  pagebtn" >下一页</button>
                                        <a class="btn ink-reaction btn-primary btn-md btn-add-some">
                                         批量选择
                                        </a>
                                        <a class="btn ink-reaction btn-info btn-md btn-add-all">
                                         全部选择
                                        </a>
                                    </div>
                            </div><!-- /.modal -->
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // 隐藏选择框默认空选项
    $(function(){
        $('#provider').find('option:first').prop('style', 'display:none')
        @if(!isset($showDetail))
            $('#provider').find('option[value=1]').attr('selected', true).change();
        @endif
        // 编辑时，判断is_shareAll
        @if(isset($showDetail) && $showDetail->is_shareAll == 1)
            $('#addShare').hide();
            $('.tableShare').hide();
        @endif

        // 编辑时，标题is_defaultTitle
        @if(isset($showDetail) && $showDetail->is_defaultTitle == 1)
            $('#showTitle').hide();
        @endif

        // 编辑时，判断is_defaultContent
        @if(isset($showDetail) && $showDetail->is_defaultContent == 1 )
            $('#showTextarea').hide();
        @endif

        Helper.setDefaultImg($('#shareImg~img'));

         // 编辑时，判断is_defaultImg
        @if(isset($showDetail) && $showDetail->is_defaultImg == 1 )
            $('#showImg').hide();
            Helper.setDefaultImg($('#shareImg~img'));
        @else
            @if(isset($showDetail) )
            Helper.setDefaultImg($('#shareImg~img'),"{{$showDetail->image}}");
            @endif
        @endif

        // 分享主图
        $("#shareImg").change(function(){
            Helper.readURL(this, '#shareImgId');
        });


        // 是否一键应用到分享端口所有所有对象
        $('input[name=is_shareAll]').click(function(){
            //判断privder值是否在在指定运行范围内
            var value = $('#provider').val();
            var arr = ['1','2','3','7'];

            var radioStatus = $(this);
            if( radioStatus.val() == 1 ){
                $('#addShare').hide();
                $('.tableShare').hide();
            }else{
                // 若是在如许范围内则显示
                if( arr.indexOf(value) != -1){
                    $('#addShare').show();
                    $('.tableShare').show();
                }
            }

        })


        // 是否使用默认标题或内容
        $('input[name=is_defaultTitle]').click(function(){
            var radioStatus = $(this);
            if(radioStatus.val() == 1 ){
                $('#showTitle').hide();
                // 若为系统默认，赋值为2
                $('input[name=title]').val('2');
            }else{
                $('#showTitle').show();
                $('input[name=title]').val('');
            }
        })


        $('input[name=is_defaultContent]').click(function(){
            var radioStatus = $(this);
            if(radioStatus.val() == 1 ){
                $('#showTextarea').hide();
                // 若为系统默认，赋值为2
                $('textarea[name=content]').text('2');
            }else{
                $('#showTextarea').show();
                $('textarea[name=content]').text('');
            }
        })


        $('input[name=is_defaultImg]').click(function(){
            var radioStatus = $(this);
            if(radioStatus.val() == 1 ){
                $('#showImg').hide();
                // 若为系统默认，赋值为2
                Helper.setDefaultImg($('#shareImg~img'));
            }else{
                $('#showImg').show();
                Helper.setDefaultImg($('#shareImg~img'));
            }
        })

    })


    // 删除
    $('.js-delete').click(function(){
        var _this = $(this);

        layer.confirm('您确定删除这个文案？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\ShareController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

    //关闭模态框
     $('#modalClose').click(function(){
        $('#provider').removeAttr("data-toggle data-target");
     })

     // 切换选择分享点
    var searchLable;
    var tdName;
    var tdNumber;
    $('#provider').change(function(){
        clearSearchDate();

        // 清空加载表格
        $('#Selected_tbody').empty();
        //设置一键分享对口所有对象
        $('input[name=is_shareAll]').attr('checked', true);

        var value = $(this).val();
        //判断显示相应文本内容
        showText(value);
    })

    // 编辑时 ，显示相应的文本
    @if (isset($showDetail))
        var value = $('#provider').val();
        showText(value);
    @endif

    // 获取制定显示文本内容和弹模态框
    function showText(value)
    {
         $('#addShare').show();
         $('.tableShare').show();

         switch(value){
            case '1':
                searchLable = '请输入商品名称或产品编号';
                tdName = '商品名称';
                tdNumber = '产品编号';
            break;
            case '2':
                searchLable = '请输入专题标题';
                tdName = '专题标题';
                tdNumber = '专题副标题';
            break;
            case '3':
                searchLable = '请输入视频标题';
                tdName = '视频标题';
                tdNumber = '视频描述';
            break;
            case '4':
                setNotShareToObjStyle();
            break;
            case '5':
                setNotShareToObjStyle();
            break;
            case '6':
                setNotShareToObjStyle();
            break;
            case '7':
                searchLable = '请输入游戏商品名称或商品编号';
                tdName = '游戏商品名称';
                tdNumber = '游戏产品编号';
            break;
            case '8':
                setNotShareToObjStyle();
            break;
            case '9':
                setNotShareToObjStyle();
            break;
            default:
            break;
        }

        $('#searchLable').text(searchLable);
        $('.tdName').text(tdName);
        $('.tdNumber').text(tdNumber);

    }

    // 设置不需细分到分享端口样式
    function setNotShareToObjStyle()
    {
        $('#addShare').hide();
        $('#showShareAll').hide();
        $('input[name=is_shareAll][value=1]').attr('checked', true);
        $('.tableShare').hide();
    }

    // 编辑状态时，添加select disabled
    @if(isset($showDetail))
        $('#provider').attr({'disabled': 'true'});
    @endif

    // 选择分享选择分享对象
     $(document).on('click','#addShare',function(){
        // clearSearchDate();
        // 显示模态框和对应内容
        value = $('#provider').val();
        var arr = ['1','2','3','7'];
        //显示模态框
        var attribute = {'data-toggle': 'modal', 'data-target': '#myModal'};
        if( arr.indexOf(value) != -1){
            // 编辑状态时，加disabled
            @if(isset($showDetail))
                $('#provider').attr(attribute)
                              .removeAttr('disabled')
                              .click()
                              .attr({'disabled': 'true'});
            @endif
                $('#provider').attr(attribute)
                              .click().removeAttr("data-toggle data-target");
        };
    })

     // tbody/搜索清空、
    function clearSearchDate(){
        $('#search_tbody').empty();
        $('#key').val('');
        // 编辑状态不清空隐藏input值
        @if (!isset($showDetail))
            $('#shareSelectIds').val('');
        @endif

        $('.pagebtn').hide();
        $('.btn-add-all').hide();
        $('.btn-add-some').hide();
    }

    // search
    var current_page = 1;
    var prev_page_url = 1;
    var nextPage_url;
    var pagebtn = $('.pagebtn');
    $('#search-button').click(function(){
        var tbody = $('#search_tbody');
        tbody.empty();

        // 清空存储被选择对象的id
        @if (!isset($showDetail))
            $('#shareSelectIds').val('');
        @endif

        var url = "{!! action('Admin\\ShareController@search', ['page' => "+ current_page +"]) !!}";
        sendSearchRequire(url);

    })

    // 单个选择
    $(document).on('click','.btn-add',function(){
        var sellproduct_id = $(this).attr('data-id');

        // 获取选择分享对象数据
        getSelected(sellproduct_id, $('#provider').val() );

        // 保存隐藏input[#shareSelectIds]的值
        saveSelectedIds(sellproduct_id);

        $(this).text('已选择')
               .css('border','none')
               .css('backgroundColor','#ccc')
               .removeClass('btn-add')
               .parents('tr').find('input[type="checkbox"]').attr({'checked': true, 'disabled':true});

    });

    // 批量选择
   $('body').on('click', '.btn-add-some', function () {
        var checkbox = $('#search_tbody').find('.checkbox:checked');
        var msg = '批量选择成功';
        selectSomeOrAll(checkbox, msg);
   });

   $('body').on('click', '.checkbox', function () {
        if($(this).prop('checked') == false){
            $(this).parents('tr').find('td:last a')
                       .text('添加')
                       .css('border','')
                       .css('backgroundColor','#2196f3')
                       .addClass('btn-add');
        }else{
            $(this).parents('tr').find('td:last a')
                            .text('已选择')
                            .css('border','none')
                            .css('backgroundColor','#ccc')
                            .removeClass('btn-add');
        }
   })

    // 全部选择
    $('body').on('click', '.btn-add-all', function () {

        var shareProduct = $('#search_tbody').find('.btn-add');
        var msg = '全部选择成功';
        selectSomeOrAll(shareProduct, msg);
   });


    // 批量、全部选择
    function selectSomeOrAll(shareProduct, msg)
    {
        var sellproduct_id;
        var array_id = [];
         var id = [];
        $.each(shareProduct,function(){
            var sellproduct_id = $(this).attr('data-id');
            // 获取选择对象数据
            getSelected(sellproduct_id, $('#provider').val() );
             // 保存隐藏input[#shareSelectIds]的值
            $(this).parents('tr').find('td:last a')
                   .text('已选择')
                   .css('border','none')
                   .css('backgroundColor','#ccc')
                   .removeClass('btn-add');

                array_id.push(Number(sellproduct_id));

        });
            id = getSelectedIds();
            // 合并数组
            var resArr =$.merge(id,array_id);
            // 去重
            resArr = $.unique(resArr);
            id = getSelectedIds();

            //隐藏的input填值
            $('#shareSelectIds').val(resArr);
        setTimeout(function(){
             toastr.success(msg, '成功!');
            $('button[data-dismiss=modal]').click();
        }, 500);

    }

    // 给隐藏input 赋值 已选项的ID
    function saveSelectedIds(sellproduct_id){
        // 获取所有已选列表的对象id
        var array_id = getSelectedIds();
        // 加入当前点击项id
        array_id.push(Number(sellproduct_id));
        //隐藏的input填值
        $('#shareSelectIds').val(array_id);
    }
    // 获取已选择列表的选的id
    function getSelectedIds(){
        var arr = [];
        $('.removeSelected').each(function(){
            var selectId = $(this).attr('data-id');
            //  string类型需转Number类型
            arr.push(Number(selectId));
        })
        return arr;
    }

      // 上一页
    $('body').on('click','#prePage',function() {
        if(prev_page_url == null){
            toastr.error('已是首页', '失败!')
            return false;
        }
        var url = prev_page_url;
        sendSearchRequire(url);
    })

    // 下一页
    $('body').on('click','#nextPage',function() {
        if(nextPage_url == null){
            toastr.error('已是最后一页', '失败!')
            return false;
        }
        var url = nextPage_url;
        sendSearchRequire(url);
    })

     // 搜索获取数据
    function sendSearchRequire(url){
        var provider_id = $('#provider').val();
         $.ajax({
            url: url,
            type: 'GET',
            data: {'_token': "{{ csrf_token() }}", 'key': $('#key').val(), 'provider_id': provider_id},
        }).done(function (data) {
            if(data.status == 'success'){
                if(data.data.data == 0){
                    toastr.error('查询不到该商品.', '失败!')
                    return false;
                }
                // 加载
                loadProduct(data);
                // 下一页url
                nextPage_url = data.data.next_page_url;
                // 上一页url
                prev_page_url = data.data.prev_page_url;
                // 判断是否有下一页
                if(nextPage_url != null ){
                    //显示
                    $('.pagebtn').show();
                }
                $('.btn-add-some').show();
                $('.btn-add-all').show();
            }else{
                toastr.error('查询不到该商品', '失败！');
            }
        }).fail(function (data) {
            toastr.error('查询不到该商品', '失败！')
        });
    }
    // 选择分享点获取数据
    function getSelected(id , provider)
    {
        var url = '{!! action("Admin\\ShareRelationController@getSelected" )!!}';
        $.ajax({
          type: 'GET',
          url: url,
          data: {'_token':"{{ csrf_token() }}",'sell_product_id':id, 'provider': provider},
        }).done(function(data) {
            if(data.status=='success'){
                // console.log(data.data);
                // 页面加载获取数据
                loadSelectedData(data.data);
            }else{
                 $('#Selected_tbody').empty();
                 toastr.error('获取数据失败！.', '失败!')
            }
        }).fail(function(data) {
            toastr.error('获取数据失败！.', '失败!')
        });
    }

    // 选择搜索商品到列表
    function loadProduct(data){
        var tbody = $('#search_tbody');
        tbody.empty();
        var html = "";
        var product = data.data.data;
        var name, snumber;
        var provider = $('#provider').val();

        var len = product.length;
        for (var i = 0; i < len; i++) {

            switch (provider) {
                case '1':
                    name  = product[i]['product']['name'];
                    snumber  = product[i]['product']['snumber'] ;
                break;
                case '2':
                    name  = product[i]['title'];
                    snumber  = product[i]['subtitle'] ;
                break;
                case '3':
                    name  = product[i]['title'];
                    snumber  = product[i]['description'] ;
                case '4':
                break;
                case '5':
                    break;
                case '6':
                    break;
                case '7':
                    name  = product[i]['sell_product']['product']['name'];
                    snumber  = product[i]['sell_product']['product']['snumber'] ;
                break;
                case '8':
                break;
                default:
                    return false;
                break;
            }
            // 获取已选择列表的选的id
            var arr = getSelectedIds();
             // 只加载不已选的项
            if ( arr.indexOf(Number(product[i]['id'])) == -1 ) {
                html  += '<tr class="tr">'
                       + '<td class="number" width="12%"><label class="checkbox-inline checkbox-styled"><input type="checkbox" class="checkbox" name="checkeds[]" data-id="' + product[i]['id']+ ' " value="'+ product[i]['id'] +'"/><span class=""></span></label> ' + (i+1) + '</td>'
                        +  '<td class="productName" >' + name + '</td>'
                        +  '<td class="productSnumber" >' + snumber + '</td>'
                        +  '<td><a class="btn btn-sm ink-reaction btn-info btn-add" data-id="' + product[i]['id']+ ' ">选择</a></td>'
                         +  '<tr>';
            }

        }
        tbody.append(html);
    }

    // 加载获取选择数据
    function loadSelectedData(data)
    {
        var tbody = $('#Selected_tbody');
            var html = "";
            var product = data;
            var name, snumber;
            var provider = $('#provider').val();
            switch (provider) {
                case '1':
                    name  = product['product']['name'];
                    snumber  = product['product']['snumber'] ;
                break;
                case '2':
                    name  = product['title'];
                    snumber  = product['subtitle'] ;
                break;
                case '3':
                    name  = product['title'];
                    snumber  = product['description'] ;
                break;
                case '4':
                    break;
                case '5':
                    break;
                case '6':
                    break;
                case '7':
                    name  = product['sell_product']['product']['name'];
                    snumber  = product['sell_product']['product']['snumber'] ;
                break;
                case '8':
                break;
                default:
                    return false;
                break;
            }
            // 获取已选择列表的选的id
            var arr = getSelectedIds();
             // 只加载不已选的项
            if ( arr.indexOf(Number(product['id'])) == -1 ) {
                    html  += '<tr class="tr">'
                             +  '<td class="number" >' + product['id'] + '</td>'
                             +  '<td class="productName" >' + name + '</td>'
                             +  '<td class="productSnumber" >' + snumber + '</td>'
                             +  '<td><a class="btn btn-sm ink-reaction btn-info btn-danger removeSelected" data-id="' + product['id']+ ' ">移除</a></td>'
                             +  '<tr>';
                }
            tbody.append(html);
    }

    // 移除已选分享对象
   $('body').on('click','.removeSelected',function() {
        var value = $(this).attr('data-id');
        var selectedIds = $('#shareSelectIds').val();
        var arr = selectedIds.split(',');
        var newArr = [];

        $(this).parents('tr').remove();
        // 将隐藏的input标签值中移除
        for(var i = 0; i < arr.length; i++){
                // console.log(arr[i]);
            if( Number(arr[i]) !== Number(value) ){
                // console.log(value);
                newArr.push(arr[i]);
            }
        }
        // 隐藏的input标签值中重新赋值
        $('#shareSelectIds').val('');
        $('#shareSelectIds').val(newArr);
    })

    // 编辑时，加载数据
    @if(isset($showDetail) && $showDetail->shareSelectIds )
        var selectedIds = $('#shareSelectIds').val();
        var arr = selectedIds.split(',');
        for(var i = 0; i < arr.length; i++ ){
            // 获取数据,加载出来
            getSelected(arr[i],  $('#provider').val());
        }
    @endif


</script>
@stop
