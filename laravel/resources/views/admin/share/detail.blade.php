@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">文案详情</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                 <div class="card-head text-right">
                    <div class="col-md-12">
                        <div class="btn-group">
                            <a href="{{ Action('Admin\ShareController@showDetail', ['type' => '0', 'id' => $showDetail->id]) }}" class="btn ink-reaction btn-primary btn-md" type="button">编辑</a>
                        </div>
                    </div>
                </div>

                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                 <form class="form floating-label" autocomplete="off" id=""  method="POST">
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                        <div class="tab-content clearfix">
                           <div class="row">

                                @include('admin.widget.input', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'title',
                                    'id'    => 'title',
                                    'title' => '标题',
                                    'type'  => 'text',
                                    'readonly' => 'readonly',
                                    'value' => isset($showDetail->title) ? ($showDetail->title == 2 ? '系统默认内容' : $showDetail->title)  : '暂无文案标题',
                                ])
                                @include('admin.widget.textarea', [
                                    'colsm'    => '48',
                                    'collg'    => '12',
                                    'name'     => 'content',
                                    'id'       => 'content',
                                    'title'    => '文案内容' ,
                                    'type'     => 'text',
                                    'readonly' => 'readonly',
                                    'value'    => isset($showDetail) && $showDetail->content ? ($showDetail->content == 2 ? '系统默认内容' : $showDetail->content) : '暂无文案内容'
                                ])

                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next">
                                    <a href="#" onClick="javascript :history.back(-1)"; class="btn ink-reaction btn-primary btn-md" type="button" >返回</a>
                               </li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
@stop


