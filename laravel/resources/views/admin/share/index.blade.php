@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">分享文案列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <dir class="row">
                    <a href="{{ action('Admin\\ShareController@create') }}" class="btn btn-primary ink-reaction btn-sm" >
                        添加
                     </a>
            </dir>
            <div class="card-body">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

                <div class="card-body">
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>文案标题</th>
                                <th>文案图片</th>
                                <th>分享端口类型</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($shareDatas as $shareData)
                             <tr>
                                 <td>{{ $shareData->id }}</td>
                                 <td>
                                    @if ( $shareData->is_defaultTitle == 1)
                                         系统默认标题
                                    @else
                                        {{ $shareData->title }}
                                    @endif
                                 </td>
                                 <td>
                                  @if ( $shareData->is_defaultImg == 1)
                                         系统默认图片
                                    @else
                                        <image src="{{ $shareData->image }}" width="100px" /></td>
                                    @endif

                                <td>
                                   @foreach ($shareTypes as $shareType)
                                        @if ($shareData->provider_value  == $shareType->provider_id)
                                         {{ $shareType->desc }}
                                        @endif
                                    @endforeach
                                </td>

                                 <td>{{ $shareData->created_at }}</td>
                                 <td>

                                    <a href="{{ Action('Admin\ShareController@showDetail', ['type' => '1', 'id' => $shareData->id]) }}" class="btn ink-reaction btn-success  btn-xs" type="button">查看文案详情</a>

                                    <a href="{{ Action('Admin\ShareController@showDetail', ['type' => '0', 'id' => $shareData->id]) }}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>

                                    <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-xs btn-danger js-delete" data-id="{{$shareData->id}}" data-name="{{$shareData->title}}" data-toggle="modal" data-target="#confirmModal">删除</a>

                                 </td>
                             </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                @if($shareDatas->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$shareDatas->toArray()['to'] - $shareDatas->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$shareDatas->toArray()['total']}}</label>
                </div>
                @endif
                 <div class="text-center">
                    {!! $shareDatas->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">

   // 删除文案
    $('.js-delete').click(function(){
        var _this = $(this);

        layer.confirm('您确定删除这个文案？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\ShareController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });
</script>
@stop
