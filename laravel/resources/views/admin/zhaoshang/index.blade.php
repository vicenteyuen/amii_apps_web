@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">招商管理列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>商家姓名</th>
                            <th>联系电话</th>
                            <th>信息状态</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($zhaoshangs as $zhaoshang)
                        <tr onclick="window.location.href='{{ Action('Admin\ZhaoShangController@showDetail', ['user_id' => $zhaoshang->id]) }}'"  style="cursor: pointer;">
                            <td>{{ $zhaoshang->id }}</td>
                            <td>{{ $zhaoshang->name }}</td>
                            <td>{{ $zhaoshang->phone }}</td>
                            <td>
                           <!--  <a href="" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a> -->
                           @if($zhaoshang->status == 1)
                                @php
                                    $btnType = 'text-primary';
                                    $btnName = '已处理';
                                @endphp
                           @else
                                @php
                                    $btnType = 'text-danger';
                                    $btnName = '未处理';
                                @endphp
                           @endif

                            <span class=" {{$btnType}} " type="text" >{{$btnName}}</span>
                            </td>
                            <td>
                            @if($zhaoshang->status == 1)
                                <a href="{{ Action('Admin\ZhaoShangController@showDetail', ['user_id' => $zhaoshang->id]) }}" class="btn ink-reaction btn-success btn-xs" type="button">查看</a>
                                </td>
                            @else
                                <a href="{{ Action('Admin\ZhaoShangController@showDetail', ['user_id' => $zhaoshang->id]) }}" class="btn ink-reaction btn-success btn-xs" type="button">编辑</a>
                                </td>
                            @endif
                        </tr>

                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {!! $zhaoshangs->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop
@section('script')
<script type="text/javascript">



</script>
@stop
