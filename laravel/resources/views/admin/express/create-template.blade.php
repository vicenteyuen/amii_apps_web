@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/admin.sell.create.css">
@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加运费模版</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="fromProduct" action = @if(isset($template))
                     "{{action('Admin\ExpressTemplateController@save')}}"
                   @else
                     "{{action('Admin\ExpressTemplateController@store')}}"
                   @endif

                    enctype="multipart/form-data" method="POST">
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                     <input type="hidden" name="id"  value="{{$id or ''}}">
                  
                        <div class="tab-content clearfix">
                           <div class="row">
                                @include('admin.widget.input', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'name',
                                    'title' => '模版名称',
                                    'value' => old('name') ? old('name') : (isset($template->name)?$template->name :''),
                                ])
                                @include('admin.widget.radio', [
                                    'name' => 'free',
                                    'id' => 'free',
                                    'title' => '是否包邮',
                                    'value' => old('free') ? old('free') : (isset($template->free)?$template->free:0 ),
                                ])
                                @include('admin.widget.input', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'default_fee',
                                    'id'    => 'default_fee',
                                    'class' => 'default_fee',
                                    'title' => '默认价格',
                                    'value' => old('default_fee') ? old('default_fee') : (isset($template->expressFee->default_fee)?$template->expressFee->default_fee:''),
                                ])
                                @include('admin.widget.input', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'free_fee',
                                    'id'    => 'free_fee',
                                    'class' => 'free_fee',
                                    'title' => '包邮条件(单位件)',
                                    'value' => old('free_fee') ? old('free_fee') : (isset($template->expressFee->free_fee)?$template-> expressFee->free_fee:''),
                                ])
                                @include('admin.widget.input', [
                                    'colsm' => '12',
                                    'collg' => '12',
                                    'name'  => 'free_money',
                                    'id'    => 'free_money',
                                    'class' => 'free_money',
                                    'title' => '包邮金额(单位元)',
                                    'value' => old('free_money') ? old('free_money') : (isset($template->expressFee->free_money)?$template-> expressFee->free_money:''),
                                ])
                            </div>
                            <div class="row js-choice-provice" >
                                <div class="form-group">
                                <div class="row text-left js-template-btn" style="margin-left: 0px">
                                    <div class="col-md-12">
                                    <ul class="list-unstyled">
                                        <li class="next"><a class="btn ink-reaction btn-primary btn-md">
                                         添加地区模版
                                        </a></li>
                                    </ul>
                                    </div>
                               </div>
                                @foreach($provice as $pro)

                                    <div class="col-xs-2 col-md-2 col-lg-2 col-sm-2 js-template-area" style="margin-top: 20px;margin-left: 50px;display: none">
                                        <label class="checkbox-inline checkbox-styled">
                                            <input name="region[]" type="checkbox" value="{{$pro->province_id}}"  text = "{{$pro->name}}" class="js-template"><span class="">{{$pro->name}}</span>
                                        </label>

                                    </div><!--end .col -->
                                @endforeach
                                </div>
                            </div>
                        </div><!--end .tab-content -->

                        <div class="col-xs-12 col-sm-12 col-md-12 js-table-template" style="display: none;margin-bottom: 20px;margin-top: 20px">
                            <div class="form-group">
                                <div>
                                    <table class="js-table table table-bordered no-margin table-striped table-hover ">
                                        <tr>
                                            <th>地区</th>
                                            <th>默认邮费</th>
                                        </tr>
                                        @if(isset($template->expressCondition))
                                            @foreach($template->expressCondition as $condition)
                                            <tr data-value ="{{$condition->region_code}}">
                                               <td>{{$condition->province->name}}</td>
                                               <td><input  name="default_fees[]" value="{{$condition->default_fee}}"><input name="province[]" type="hidden" value="{{$condition->region_code}}"></td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md js-commit">
                                 提交
                                </button></li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop

@section('script')
<script type="text/javascript">

    $('.js-commit').click(function(){
        if($('input[name=free]:checked').val() == 0){
            if($('#default_fee').val() == ''){
                layer.msg('默认价格不能为空', {icon: 0});
                return false;
            }
        }
    })
    $('input[name=free]').click(function(){
        var defult = $(".default_fee");
        var free = $(".free_fee");
        var freeMoney = $(".free_money");
        var choiceProvice = $(".js-choice-provice");
        var templatBtn = $(".js-template-btn");
        var table = $(".js-table-template");
        var radioStatus = $(this);
        if(radioStatus.val() == 1){
            defult.css('display','none');
            free.css('display','none');
            freeMoney.css('display','none');
            choiceProvice.css('display','none');
            templatBtn.css('display','none');
            table.css('display','none');
        }else{
            defult.css('display','block');
            free.css('display','block');
            freeMoney.css('display','block');
            choiceProvice.css('display','block');
            templatBtn.css('display','block');
            table.css('display','block');
        }
    })

    $('.js-template-btn').click(function(){
        $('.js-template-area').css('display','block');
    })

    //编辑
    @if(isset($template->free) && $template->free == 1)
        var defult = $(".default_fee");
        var free = $(".free_fee");
        var freeMoney = $(".free_money");
        var choiceProvice = $(".js-choice-provice");
        var templatBtn = $(".js-template-btn");
        var table = $(".js-table-template");
        defult.css('display','none');
        free.css('display','none');
        freeMoney.css('display','none');
        choiceProvice.css('display','none');
        templatBtn.css('display','none');
        table.css('display','none');
    @endif

    @if(isset($template->expressCondition) && !empty($template->expressCondition))
        $('.js-table-template').css('display','block');
        //编辑时勾选上选择的地区
        var templateArea = $('.js-template');
        var exitsArea = $('input[name^="province"');
        $.each(exitsArea,function(key1,value1){
            var _this = $(this);
            $.each(templateArea,function(key,value){
                if($(this).attr('value') == _this.val()){
                    $(this).attr('checked','checked');
                }
            })
        })
    @endif

    $('body').on('click','.js-template',function(){
        var template = $('.js-table-template');
        var table = template.find('table');
        var tr = table.find('tr');
        var _this = $(this);
        var htmlBuffer = [];
        var tableHtml;
        var removeTr;
        if(_this.attr('checked') == 'checked'){   //add
            template.css('display','block');
            htmlBuffer.push('<tr data-value="'+_this.val()+'">');
            htmlBuffer.push('<td>'+_this.attr('text')+'</td>');
            htmlBuffer.push('<td><input  name="default_fees[]"> <input name="province[]" type="hidden" value="'+_this.val()+'">'+'</td>');
            htmlBuffer.push('</tr>');
            tableHtml= htmlBuffer.join('\n');
            table.append(tableHtml);
        }else{
            $.each(tr,function(key,value){
                if($(this).attr('data-value') == _this.val()){
                    $(this).remove();
                    return false;
                }
            })
        }
    })

</script>
@stop
