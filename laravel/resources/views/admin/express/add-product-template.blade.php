
@extends('admin.widget.body')

@section('style_link')

<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">

@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">批量添加运费模版商品</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class='col-md-12'>
                <div class="row">
                  <form action='' method="GET"  class="form floating-label">
                     @include('admin.widget.input',[
                        'name' => 'name',
                        'id' => 'template',
                        'title' => '模版名' ,
                        'colsm' => '12',
                        'collg' => '12',
                        'value' => isset($template->name )? $template->name : '',
                        'attribute' => 'disabled',
                        'dataValue' => $template->id
                        ])
                    </form>
                </div>
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">搜索商品</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="card-body">
                            <form action='' method="GET" class="form floating-label">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="key" name="key" value="" placeholder="请输入商品名称或产品编号">
                                         <input type="hidden" class="form-control" id="" name="id" value="{{$template->id}}" placeholder="">

                                        <label for="search"></label>
                                        <div class="form-control-line"></div>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-floating-action btn-default-bright" type="submit" id="search-button"><i class="fa fa-search" id="search-sub"></i></button>
                                    </div>
                                </div>
                            </form>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>商品名称</th>
                                            <th>商品编号</th>
                                        </tr>
                                    </thead>
                                    <tbody id="search_tbody">
                                         @foreach($products as $key => $product)
                                            <tr>
                                                <td><label class="checkbox-inline checkbox-styled"> <input  name="product[]" type="checkbox" class="js-check" value="{{$product->id}}"><span></span></label></td>
                                                <td>{{$product->name}}</td>
                                                <td class="productSnumber">{{$product->snumber}}</td>
                                            </tr>
                                         @endforeach
                                    </tbody>
                                </table>
                                <div class="row text-right">
                                     <div class="col-xs-12 col-sm-12 col-md-12">
                                        <button class=" btn ink-reaction btn-success btn-md " id="btn1">全选</button>
                                        <button class=" btn ink-reaction btn-danger btn-md " id="btn2" >取消全选</button>
                                        <button class=" btn ink-reaction btn-success btn-md addProduct" type="button">添加</button>
                                     </div>
                                </div>
                            </div><!-- ./card-body -->
                            <div class="text-center">
                                {{$products->render()}}
                            </div>
                        </div>
                    </div>
                </div><!-- ./col-lg-6 -->
            </div><!-- ./row -->
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/admin/js/jquery-1.11.2.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // 全选、取消全选
    $("#btn1").on('click', function () {
        $("input[type=checkbox]").prop("checked", true);
    })
    $("#btn2").click(function () {
        $("input[type=checkbox]").prop("checked", false);
    })


    $('body').on('click','.addProduct',function(){
        var productIds = [];
        $.each($("input[name='product[]']:checked"), function() {
            productIds.push($(this).val());
        })
        if(productIds == ''){
             toastr.error('请先选择商品', '失败!');
             return false;
        }
        $.ajax({
          type: 'POST',
          url: "{{action('Admin\\ExpressTemplateController@storeTemplateProduct')}}",
          data: {'_token':"{{ csrf_token() }}",'productIds':productIds,'templateId':$('#template').attr('data-value')},
        }).done(function(data) {
            if(data.status=='success'){
                toastr.success('添加成功', '成功!');
            }else{
                 toastr.error('添加失败', '失败!')
            }
        }).fail(function(data) {
            toastr.error('添加商品到运费失败.', '失败!')
        });

    })



</script>
@stop
