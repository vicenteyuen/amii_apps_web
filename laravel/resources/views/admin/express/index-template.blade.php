@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">运费模版列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>模版名称</th>
                                    <th>是否包邮</th>
                                    <th>默认价格</th>
                                    <th>滿件包邮(件)</th>
                                    <th>包邮金额(元)</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($templates as $template)
                                    <tr>
                                        <td>{{ $template->name }}</td>
                                        <td>
                                            @if($template->free == 1)
                                                是
                                            @else
                                                否
                                            @endif
                                        </td>
                                        <td>
                                            @if($template->free == 1)
                                                无
                                            @else
                                                {{isset($template->expressFee->default_fee)?$template->expressFee->default_fee:''}}
                                            @endif
                                        </td>
                                        <td>
                                        @if($template->free == 1)
                                                无
                                            @else
                                                {{$template->expressFee->free_fee?$template->expressFee->free_fee:'无'}}
                                        @endif

                                        </td>
                                        <td>
                                        @if($template->free == 1)
                                                无
                                            @else
                                                {{$template->expressFee->free_money?$template->expressFee->free_money:'无'}}
                                        @endif

                                        </td>
                                        <td>

                                           
                                            <a href="{{action('Admin\ExpressTemplateController@templateProductIndex',['id' =>$template->id])}}" class="btn btn-sm ink-reaction btn-xs btn-success data-toggle="modal" data-target="#confirmModal">运费模版商品</a>
                                            <a href="{{action('Admin\ExpressTemplateController@showEdit',['id' =>$template->id])}}" class="btn btn-sm ink-reaction btn-xs btn-success data-toggle="modal" data-target="#confirmModal">编辑</a>
                                            <a href="{{action('Admin\ExpressTemplateController@createTemplateProduct',['id' =>$template->id])}}" class="btn btn-sm ink-reaction btn-xs btn-success data-toggle="modal" data-target="#confirmModal">添加商品</a>
                                            <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-xs btn-success js-add-product" data-id="{{$template->id}}"  data-toggle="modal" data-target="#confirmModal">设为默认运费模版</a>
                                            <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-xs btn-danger js-delete" data-id="{{$template->id}}" data-name="{{$template->name}}" data-toggle="modal" data-target="#confirmModal">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')

<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop

@section('script')
<script type="text/javascript">
      // 删除分类
    $('.js-delete').click(function(){
        var _this = $(this);
        layer.confirm('您确定删除 "'+_this.attr('data-name')+'" 这个运费模版？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\ExpressTemplateController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

    
    $('.js-add-product').click(function(){
        var _this = $(this);
        $.ajax({
            url: '{!! action("Admin\\ExpressTemplateController@setAllProductExpressTemplate") !!}',
            type: 'POST',
            data: {
                _token: "{{csrf_token()}}",
                id    : _this.attr('data-id')
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('设置成功', {icon: 1});
                    setTimeout(function(){
                    },1000)
                }else{
                    layer.msg('设置失败,请刷新重试',{icon: 0});
                }

            }
        });
       
    });
</script>
@stop
