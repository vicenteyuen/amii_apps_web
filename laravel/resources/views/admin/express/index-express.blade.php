@extends('admin.widget.body')

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">运费模版列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>公司名称</th>
                                    <th>代号</th>
                                    <th>是否禁用</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($expresses as $express)
                                    <tr>
                                        <td>{{ $express->name }}</td>
                                         <td>{{ $express->com }}</td>
                                        <td>
                                            @if($express->status == 1)
                                                否
                                                @php
                                                    $opration = '禁用';
                                                    $class = "btn-danger";
                                                @endphp
                                            @else
                                                是
                                                @php
                                                   $opration = '开启';
                                                   $class = "btn-success";
                                                @endphp
                                            @endif
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-xs {{$class}} js-delete" data-id="{{$express->id}}" data-status="{{$express->status}}" data-toggle="modal" data-target="#confirmModal">
                                            {{$opration}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')

<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>

@stop

@section('script')
<script type="text/javascript">
      // 删除分类
    $('.js-delete').click(function(){
        var _this = $(this);
        var status = _this.attr('data-status');
        if(status == 1){
            status = 0;
        }else{
            status = 1;
        }
        $.ajax({
            url: '{!! action("Admin\\ExpressController@save") !!}',
            type: 'POST',
            data: {
                _token: "{{csrf_token()}}",
                id    : _this.attr('data-id'),
                status: status,
            },
            dataType: 'JSON',
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('操作成功', {icon: 1});
                    setTimeout(function(){
                       window.location.reload();
                    },1000)
                }else{
                    layer.msg('操作失败,请刷新重试',{icon: 0});
                }
            }
        });
       
    });

</script>
@stop
