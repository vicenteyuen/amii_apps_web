@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">运费模版商品列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>模版名称</th>
                            <th>商品名</th>
                            <th>商品编号</th>
                            <th>商品主图</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{$template->name}}</td>
                            <td>{{ $product->name }}</td>
                            <td >{{ $product->snumber }}</td>
                            <td>
                            @if($product->image)
                            <img src="{{$product->image}}" width="50px">
                            @else
                            暂无图片
                            @endif
                            </td>
                            <td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($products->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$products->toArray()['to'] - $products->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$products->toArray()['total']}}</label>
                </div>
                 @endif
                <div class="text-center">
                    {!! $products->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
@stop
