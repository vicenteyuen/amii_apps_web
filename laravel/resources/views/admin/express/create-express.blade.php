@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/wizard.css">
@stop

@section('content')

<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">添加快递公司</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div id="rootwizard" class="form-wizard form-wizard-horizontal">
                   <form class="form floating-label" autocomplete="off" id="fromProduct" action = "{{action('Admin\ExpressController@store')}}" enctype="multipart/form-data" method="POST">
                     <input type="hidden" name="_token" class="js_token" id="_token" value="{{csrf_token()}}">
                        <div class="tab-content clearfix">
                           <div class="row">
                                @include('admin.widget.input', [
                                    'colsm' => '12',    
                                    'collg' => '12',   
                                    'name'  => 'name', 
                                    'title' => '快递公司名称' ,                        
                                ])

                                @include('admin.widget.input', [
                                    'colsm' => '12',    
                                    'collg' => '12',   
                                    'name'  => 'com', 
                                    'title' => '公司代号',
                                ])

                            </div>
                        </div><!--end .tab-content -->
                        <div class="row text-right btn-commit">
                            <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li class="next"><button type="submit" class="btn ink-reaction btn-primary btn-md">
                                 提交
                                </button></li>
                            </ul>
                            </div>
                        </div>
                    </form>
                </div><!--end #rootwizard -->
            </div>
        </div>
    </div>
</section>
@stop

@section('script_link')
@if (config('app.debug'))
<!-- 使用 https://github.com/VinceG/twitter-bootstrap-wizard -->
@endif
<script src="/assets/admin/js/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/admin/js/jquery.form.js" type="text/javascript"></script>
@stop

@section('script')

@stop
