@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop
@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">运费模版详情</li>
        </ol>
    </div>
    <div class="section-body contain-lg">

        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i> 模版基本信息</header>
                    </div>
                    <div class="card-body">
                        <dl class="dl-horizontal">
                            <dt>模版名称</dt>
                            <dd>{{$template->name }}</dd>
                            <dt>是否包邮</dt>
                            <dd>@if($template->free == 1)
                                    是
                                @else
                                    否
                                @endif
                            </dd>
                            @if($template->free == 0)
                            <dt>默认价格</dt>
                            <dd>{{$template->expressFee->default_fee}}￥</dd>
                            <dt>满件包邮</dt>
                            <dd>{{$template->expressFee->free_fee ? $template->expressFee->free_fee.'件' : '无'}}</dd>
                            @endif
                        </dl>
                        <div class=" pull-right">
                          <a href="javascript:void(0);" class="btn btn-sm ink-reaction btn-sm btn-danger js-delete" data-id="{{$template->id}}" data-name="{{$template->name}}" data-toggle="modal" data-target="#confirmModal">删除</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(!$template->expressCondition->isEmpty())
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-head card-bordered style-primary">
                    <header><i class="fa fa-fw fa-tag"></i> 地区模版</header>
                    </div>
                    <div class="card-body">
                        @foreach($template->expressCondition as $condition)
                        <dl class="dl-horizontal">
                            <dt>地区</dt>
                            <dd>{{$condition->province->name}}</dd>
                            <dt>默认价格</dt>
                            <dd>{{$condition->default_fee}}￥</dd>
                        </dl>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>

<script type="text/javascript">
      // 删除分类
    $('.js-delete').click(function(){
        var _this = $(this);
        layer.confirm('您确定删除 "'+_this.attr('data-name')+'" 这个运费模版？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: '{!! action("Admin\\ExpressTemplateController@delete") !!}',
                    type: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        id    : _this.attr('data-id')
                    },
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                               window.history.go(-1);
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
        });
    });

</script>
@stop

