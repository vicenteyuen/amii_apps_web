@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/toastr/toastr.min.css">
@stop

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">提现记录列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                @if ($request->status !== null)
                                    @if ($request->status == 0)
                                    申请中
                                    @elseif ($request->status == 1)
                                    已同意
                                    @elseif ($request->status == 2)
                                    已支付
                                    @elseif ($request->status == 3)
                                    已拒绝
                                    @endif
                                @else
                                选择筛选条件
                                @endif
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="{{ action('Admin\WithdrawController@index') }}">全部</a></li>
                                <li><a href="{{ action('Admin\WithdrawController@index', ['status' => 0]) }}">申请中</a></li>
                                <li><a href="{{ action('Admin\WithdrawController@index', ['status' => 1]) }}">已同意</a></li>
                                <li><a href="{{ action('Admin\WithdrawController@index', ['status' => 2]) }}">已支付</a></li>
                                <li><a href="{{ action('Admin\WithdrawController@index', ['status' => 3]) }}">已拒绝</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-hover table-condensed table-striped no-margin">
                            <thead>
                                <tr>
                                    <th>用户</th>
                                    <th>提现账号信息</th>
                                    <th>提现金额</th>
                                    <th>提现申请时间</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->channel->user->nickname }}</td>
                                    <td>
                                        账号类型:
                                        @if ($item->channel->type == 'alipay')
                                        支付宝
                                        @else
                                        微信
                                        @endif
                                        <br/>
                                        账号: {{ $item->channel->account_number }}<br/>
                                        账号名: {{ $item->channel->account_name }}
                                    </td>
                                    <td>{{ $item->amount }}元</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>
                                        @if ($item->state == 0)
                                        申请中
                                        @elseif ($item->state == 1)
                                        已同意
                                        @elseif ($item->state == 2)
                                        已支付
                                        @elseif ($item->state == 3)
                                        已拒绝
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->state == 0)
                                        <a class="btn ink-reaction btn-success btn-xs withdraw-agree-pay" type="button" data-id="{{ $item->id }}">支付</a>
                                        <a class="btn btn-sm ink-reaction btn-xs btn-danger withdraw-refuse-pay" type="button" data-id="{{ $item->id }}">拒绝</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                        </div>
                    </div>
                    <div class="text-center">
                        {!! $items->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/lib/toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
$('.withdraw-agree-pay').on('click', function() {
    $.ajax({
        url: '{{ action("Admin\WithdrawController@agree") }}',
        type: 'POST',
        data: {
            id     : $(this).attr('data-id'),
        },
        dataType: 'JSON',
        success: function (returnData) {
            console.log(returnData);
            if (returnData.status == 'success') {
                toastr.success('支付', '成功');
            } else {
                toastr.error('支付', '失败');
            }
        }
    });
});
$('.withdraw-refuse-pay').on('click', function() {
    $.ajax({
        url: '{{ action("Admin\WithdrawController@refuse") }}',
        type: 'POST',
        data: {
            id     : $(this).attr('data-id'),
        },
        dataType: 'JSON',
        success: function (returnData) {
            console.log(returnData);
            if (returnData.status == 'success') {
                toastr.success('拒绝', '成功');
            } else {
                toastr.error('拒绝', '失败');
            }
        }
    });
});
</script>
@stop

