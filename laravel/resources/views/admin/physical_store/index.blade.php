@extends('admin.widget.body')

@section('content')
    <section>
        <div class="section-header">
            <ol class="breadcrumb">
                <li class="active">实体店列表</li>
            </ol>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="card-head">
                        <div class="tools">
                            <div class="btn-group">
                                <a href="{!! action('Admin\PhysicalStoreController@create') !!}" class="btn ink-reaction btn-primary btn-sm pull-right">
                                    添加
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="table table-hover table-condensed table-striped no-margin">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>名称</th>
                            <th>主图</th>
                            <th>排序权重（降序）</th>
                            <th>联系电话</th>
                            <th width="200">详细地址</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($stores as $store)
                            <tr>
                                <td>{{ $store->id }}</td>
                                <td>{{ $store->name }}</td>
                                <td>
                                    <img src="{{ $store->image }}" height="40">
                                </td>
                                <td>{{ $store->weight }}</td>

                                <td>{{ $store->phone }}</td>
                                <td>{{ $store->address . $store->detail_address }}</td>
                                <td>

                                 @if($store->status == 1)
                                     <a href="javascript:void(0);" class="btn ink-reaction btn-danger btn-xs js-status" data-id="{{$store->id}}" data-status="{{$store->status ? 0 :1}}" type="button">
                                        禁用
                                    </a>
                                 @else
                                     <a href="javascript:void(0);" class="btn ink-reaction btn-primary btn-xs js-status" data-id="{{$store->id}}" data-status="{{$store->status ? 0 :1}}" type="button">
                                        开启
                                    </a>
                                 @endif
                                     
                                    <a href="{{ action('Admin\PhysicalStoreController@edit', $store->id)}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                                    <a href="javascript:;" data-url="{{ action('Admin\PhysicalStoreController@destroy', $store->id)}}" class="btn ink-reaction btn-danger btn-xs store-delete" type="button">删除</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if($stores->toArray()['data'])
                    <div class="col-md-12 col-lg-12">
                        <label style="color: black;font-size: 16px">当前页数量: {{$stores->toArray()['to'] - $stores->toArray()['from'] + 1}}</label>
                        <label style="color: black;font-size: 16px">总数量: {{$stores->toArray()['total']}}</label>
                    </div>
                    @endif
                    <div class="text-center">
                        {!! $stores->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('script')
    <script type="text/javascript">
        $('.store-delete').click(function () {
            var url = $(this).attr('data-url');
            layer.confirm('确定删除?', {
                btn: ['确定','取消'] //按钮
            }, function(){
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: 'JSON',
                    success: function (returnData) {
                        if (returnData.status == 'success') {
                            layer.msg('删除成功', {icon: 1});
                            setTimeout(function(){
                                window.location.reload();
                            },1000)
                        }else{
                            layer.msg('删除失败,请刷新重试',{icon: 0});
                        }

                    }
                });
            });
        })

    $('.js-status').click(function(){
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        $.ajax({
            url: "{{action('Admin\\PhysicalStoreController@status')}}",
            type: 'POST',
            dataType: 'JSON',
            data:{'id':id,'status':status},
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('操作成功', {icon: 1});
                    setTimeout(function(){
                        window.location.reload();
                    },1000)
                }else{
                    layer.msg('操作失败,请刷新重试',{icon: 0});
                }

            }
        });
    })
    </script>
@stop
