@extends('admin.widget.body')

@section('style')
    <style>
        #map-container {
            min-width:100%;
            min-height:50%;
        }
        #infoDiv {
            padding-left: 30px;
        }
        .result-list {
            display: block;
            height:3em;
            line-height: 3em;
            text-decoration: none;
        }
    </style>
@stop

@section('style_link')
    <link rel="stylesheet" type="text/css" href="/assets/lib/summernote/summernote.css">
@stop

@section('content')
    <section>
       <div class="section-header">
            <ol class="breadcrumb">
                <li class="active">实体店 添加/修改</li>
            </ol>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        @php
                            $store_exist = isset($store) ? true : false;
                        @endphp
                        @if($store_exist)
                            {!! Form::open(['action' => ['Admin\PhysicalStoreController@update', $store->id], 'class' => 'form', 'id' => 'store-form', 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
                        @else
                            {!! Form::open(['action' => 'Admin\PhysicalStoreController@store', 'class' => 'form', 'id' => 'store-form', 'enctype' => 'multipart/form-data']) !!}
                        @endif
                        @include('admin.widget.image-upload', [
                            'colsm' => '12',
                            'collg' => '12',
                            'id' => 'store-image',
                            'name' => 'image',
                            'descript'=>'尺寸: 700*330',
                            'style'=>'width:700px;height:330px;',
                            'defaultImg' => old('image') ? old('image') : '',
                        ])
                        @include('admin.widget.input', [
                            'colsm' => 0,
                            'collg' => 0,
                            'name' => 'editIden',
                            'id' => 'editIden',
                            'type'=>'hidden'
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'name',
                            'title' => '名称',
                            'type'  => 'text',
                            'value' => old('name') ? old('name') : ($store_exist ? $store->name : '')
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'weight',
                            'title' => '排序权重（降序）',
                            'type'  => 'text',
                            'value' => old('weight') ? old('weight') : ($store_exist ? $store->weight : '0')
                        ])
                        @include('admin.widget.input', [
                            'colsm' => 0,
                            'collg' => 0,
                            'name' => 'name_initial',
                            'id' => 'name_initial',
                            'type'=>'hidden',
                            'value' => old('name_initial') ? old('name_initial') : ($store_exist ? $store->name_initial : ''),
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'phone',
                            'title' => '联系电话',
                            'type'  => 'text',
                            'value' => old('phone') ? old('phone') : ($store_exist ? $store->phone : ''),
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '6',
                            'collg' => '6',
                            'name' => 'address',
                            'title' => '地址',
                            'type'  => 'text',
                            'value' => old('address') ? old('address') : ($store_exist ? $store->address : ''),
                           
                        ])

                        @include('admin.widget.button', [
                            'colsm' => '6',
                            'collg' => '6',
                            'id'    => 'mapSearch',
                            'text' =>  '地图搜索',
                            ])


                        @include('admin.widget.input', [
                            'colsm' => 0,
                            'collg' => 0,
                            'name' => 'longitude',
                            'id' => 'longitude',
                            'value' => old('longitude') ? old('longitude') : ($store_exist ? $store->longitude : ''),
                            'type'=>'hidden'
                        ])
                        @include('admin.widget.input', [
                            'colsm' => 0,
                            'collg' => 0,
                            'name' => 'latitude',
                            'id' => 'latitude',
                            'value' => old('latitude') ? old('latitude') : ($store_exist ? $store->latitude : ''),
                            'type'=>'hidden'
                        ])
                        @include('admin.widget.input', [
                            'colsm' => 0,
                            'collg' => 0,
                            'name' => 'city_name',
                            'id' => 'city_name',
                            'value' => old('city_name') ? old('city_name') : ($store_exist ? $store->city_name : ''),
                            'type'=>'hidden'
                        ])
                        @include('admin.widget.input', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'detail_address',
                            'title' => '补充地址',
                            'type'  => 'text',
                            'value' => old('detail_address') ? old('detail_address') : ($store_exist ? $store->detail_address : ''),
                        ])
                        @include('admin.widget.textarea', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name' => 'introduction',
                            'id' => 'summernote',
                            'value' => old('introduction') ? old('introduction') : ($store_exist ? $store->introduction : ''),
                        ])
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group text-right">
                            <button type="submit" class='btn btn-success'>保存</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    @include('admin.widget.editor-preview')

@stop

@section('script_link')
    <script src="assets/lib/summernote/summernote.min.js"></script>
    <script src="assets/lib/summernote/lang/summernote-zh-CN.js"></script>
    <script src="assets/lib/tmpl/tmpl.min.js"></script>
    <script src="assets/admin/js/makeby.js"></script>
    <script charset="utf-8" src="assets/admin/js/lbs.qqmap.js"></script>
@stop

@section('script')
    <script type="text/javascript">

        //初始化富文本
        $summernote = $('#summernote');
        $summernote.summernote({
            lang: "zh-CN",
            height: "500",

            callbacks: {
                onImageUpload: function (files) {
                    var length = files.length;
                    for (var i = 0; i < length; i++) {
                        sendFile(files[i]);
                    }
                },
                onInit: function() {
                    addPreviewButton();
                }
            }

        });


        // add preview button to editor
        function addPreviewButton(){
            var noteBtn = '<button id="previewEditor" type="button" class="btn btn-default btn-sm btn-small" title="预览" data-event="" tabindex="-1"><i class="fa fa-eye"></i></button>';
            var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
            $(fileGroup).appendTo($('.note-toolbar'));
            // Button tooltips
            $('#previewEditor').tooltip({container: 'body', placement: 'bottom'});
            // Button events
            $('#previewEditor').click(function(event) {
                $('#editor-preview-tmpl').empty().append($('#summernote').val());
                Helper.modalToggle($('#modal-editor-preview'));
            });
        }

        //上传主图
        @if(isset($store))
            Helper.setDefaultImg($('#uploadImg'), "{{$store->image}}");
        @else
            Helper.setDefaultImg($('#uploadImg'));
        @endif
        $("#store-image").change(function () {
            Helper.readURL(this);
            $('#editIden').val('-1');
        });

        //upload
        function sendFile(file, editor, welEditable) {
            var data = new FormData();
            data.append('file', file);
            data.append('_token', "{{ csrf_token() }}");
            data.append('provider', 'physicalStore');
            $.ajax({
                url: '{!! action("Admin\\HelperController@summernoteImageUpload") !!}',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (url) {
                    $('#summernote').summernote('insertImage', url, file.name);
            }).fail(function () {
                    console.log("error");
            }).always(function () {
                    console.log("complete");
            });
        }

        //获取门店名称首字母
        $(function(){
            $('input[name="name"]').on('blur', function(event) {
                //取拼音大写
                var str = makePy($(this).val());
                //截取首字母
                str = str[0].substr(0,1);
                //判断是否为小写，是就转为大写
                if (/^[a-z]+$/.test( str )) {
                    str = str.toUpperCase()
                }
                //判断是否为空值
                if(str != ''){
                    //不是就赋值
                    $('input[name="name_initial"]').val(str).addClass('dirty');
                }else{
                    //是就清空
                    $('input[name="name_initial"]').val('').removeClass('dirty');
                }
            });
        });

        //腾讯地图
        var map = null;
        var searchService = null;
        var geocoder = null;
        var init = function () {
            var center = new qq.maps.LatLng(39.916527,116.397128);
            map = new qq.maps.Map(document.getElementById('map-container'),{
                center: center,
                zoom: 13
            });

            //调用Poi检索类
            searchService = new qq.maps.SearchService({
                map : map,
                complete:function(result) {
                    if (result.type != 'POI_LIST') {
                        layer.msg('地址信息不全或有误，请重填')
                        return;
                    }
                    var list = result.detail.pois;
                    var list_str = '';
                    for (var x = 0; x < list.length; x++) {
                        list_str += '<a href="javascript:;" class="result-list" onclick="selectAddress(this)" data-lat="' + list[x].latLng.lat
                                + '" data-lng="' + list[x].latLng.lng +'" data-address="' + (list[x].address? list[x].address:'') + list[x].name
                                + '"><i class="md md-location-on"></i> ' + (list[x].address? list[x].address:'') + list[x].name + '</a>';
                    }
                    document.getElementById('infoDiv').innerHTML = list_str;
                }
            });

            //初始化用户地址
            citylocation = new qq.maps.CityService({
                complete : function(result){
                    map.setCenter(result.detail.latLng);
                }
            });
            citylocation.searchLocalCity();
        };

        $('#mapSearch').click(function () {
            map_layer_index = layer.open({
                type: 1,
                maxmin: true,
                area: ['100%', '100%'],
                content: '<div id="map-container"></div><div>区域 <input type="text" value="" id="regionText" placeholder="城市级别"/>地点 <input type="text" value="" id="poiText"/><input type="button" value="Search" onclick="getResult()"/><div id="pageIndexLabel" style="width: 100%;"></div></div><div style="width: 100%; height: 180px" id="infoDiv"></div>'
            });
            init();
        });

        //搜索输入地址
        function getResult() {
            //设置searchRequest
            var poiText = document.getElementById("poiText").value;
            var regionText = document.getElementById("regionText").value;

            searchService.setLocation(regionText);
            searchService.search(poiText);
        }

        //选择地址，更新input信息
        function selectAddress(e) {
            var lat = $(e).attr('data-lat');
            var lng = $(e).attr('data-lng');
            var address = $(e).attr('data-address');
            var latLng = new qq.maps.LatLng(lat, lng);

            geocoder = new qq.maps.Geocoder();
            geocoder.getAddress(latLng);
            //设置服务请求成功的回调函数
            geocoder.setComplete(function(result) {
                console.log(result)
                $('input[name="address"]').val(address);
                $('input[name="longitude"]').val(lng);
                $('input[name="latitude"]').val(lat);
                $('input[name="city_name"]').val(result.detail.addressComponents.city);

            });

            if (map_layer_index) {
                layer.close(map_layer_index);
            }
        }
    </script>
@stop
