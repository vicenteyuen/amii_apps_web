@extends('admin.widget.body')

@section('style_link')
<link rel="stylesheet" type="text/css" href="/assets/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">
<link rel="stylesheet" type="text/css" href="/assets/lib/summernote/summernote.css">
<style type="text/css">
    .square {
    position: absolute;
    margin: auto;
    min-height: 100%;
    min-width: 100%;

    /* For the following settings we set 100%, but it can be higher if needed
    See the answer's update */
    left: -100%;
    right: -100%;
    top: -100%;
    bottom: -100%;
}
</style>
@stop

@section('content')
<section>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @php
                    $subject_exist = isset($subject) ? true : false;
                    @endphp
                    @if($subject_exist)
                        {!! Form::open(['action' => ['Admin\SubjectController@update', $subject->id], 'class' => 'form',
                        'id' => 'subject-form', 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
                    @else
                        {!! Form::open(['action' => 'Admin\SubjectController@store', 'class' => 'form', 'id' =>
                        'subject-form', 'enctype' => 'multipart/form-data']) !!}
                    @endif
                    @include('admin.widget.image-upload', [
                        'colsm' => '12',
                        'collg' => '12',
                        'id'    => 'subject-image',
                        'name'  => 'image',
                        'style' => 'width:750px;height:358px;',
                        'descript' => '专题主图(750x358)'
                    ])
                    @include('admin.widget.input', [
                        'name' => 'editIden',
                        'id'   => 'editIden',
                        'type' => 'hidden'
                    ])
                    @include('admin.widget.input', [
                        'colsm' => '12',
                        'collg' => '8',
                        'name'  => "title",
                        'id'    => 'title',
                        'title' => '标题',
                        'type'  => 'text',
                        'value' => old('title') ? old('title') : (isset($subject) && $subject->title ? $subject->title : '')
                    ])
                    @include('admin.widget.input', [
                        'colsm' => '12',
                        'collg' => '8',
                        'name'  => "subtitle",
                        'id'    => 'subtitle',
                        'title' => '子标题',
                        'type'  => 'text',
                        'value' => old('subtitle') ? old('subtitle') : (isset($subject) && $subject->subtitle ? $subject->subtitle : ''),
                    ])

                    @include('admin.widget.input', [
                        'colsm' => '12',
                        'collg' => '8',
                        'name'  => 'weight',
                        'title' => '排序权重（降序）',
                        'type'  => 'text',
                        'value' => old('weight') ? old('weight') : (isset($subject)? $subject->weight : '0')
                    ])

                    <div class="row">
                        <div class="col-lg-10 col-sm-12">
                            @include('admin.widget.textarea', [
                            'colsm' => '12',
                            'collg' => '12',
                            'name'  => "content",
                            'id'    => 'summernote',
                            'title' => '',
                            'value' => old('content') ? old('content'): (isset($subject) && $subject->content ? $subject->content : ''),
                            ])
                        </div>
                        <div class="col-lg-2 col-sm-12 col-xs-12 form-group">
                            <div class="card">
                                <div class="card-body no-padding">
                                    <ul class="list divider-full-bleed">
                                        <li class="tile">
                                            <a class="tile-content ink-reaction" href="javascript:;"
                                               id="editor-preview">
                                                <div class="tile-icon">
                                                    <i class="fa fa-eye"></i>
                                                </div>
                                                <div class="tile-text">
                                                    预览
                                                </div>
                                            </a>
                                        </li>
                                        @include('admin.widget.editor-preview')


                                        <li class="tile">
                                            <a class="tile-content ink-reaction" href="javascript:;"
                                               id="insert-image">
                                                <div class="tile-icon">
                                                    <i class="fa fa-image"></i>
                                                </div>
                                                <div class="tile-text">
                                                    插入图片
                                                </div>
                                            </a>
                                        </li>
                                        @include('admin.widget.image-select')
                                        @include('admin.widget.image-select-tmpl')
                                        <li class="tile">
                                            <a class="tile-content ink-reaction" href="javascript:;"
                                               id="insert-product">
                                                <div class="tile-icon">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </div>
                                                <div class="tile-text">
                                                    插入商品
                                                </div>
                                            </a>
                                        </li>
                                        @include('admin.widget.product-select')
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-8">
                        <div class="form-group floating-label">
                            <label class="col-sm-3 control-label">图文关联商品</label>
                            <div class="col-sm-9" id="contact-products">
                                @if($subject_exist && $subjectProducts)
                                @foreach($subjectProducts as $product)
                                <label class="checkbox-inline checkbox-styled checkbox-primary">
                                    <input type="checkbox" value="{{ $product->id }}" name="product_ids[]" checked="">
                                    <span>{{ $product->name }}</span>
                                </label>
                                @endforeach
                                @endif
                                <div class="help-block">* 在编辑器中删除商品图文时需在此取消打勾</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group text-right">
                        <button type="submit" class='btn btn-success'>保存</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    </div>
</section>
@stop

@section('script_link')
<script src="/assets/lib/summernote/summernote.min.js"></script>
<script src="/assets/lib/summernote/lang/summernote-zh-CN.js"></script>
<script src="/assets/lib/plupload/plupload.full.min.js"></script>
<script src="/assets/lib/tmpl/tmpl.min.js"></script>
@stop

@section('script')
<script>
    var product_wrapper_style = "margin:13px 0;display: flex;border: 1px solid #e1e1e1;border-radius: 5px;";
    var product_img_style = "width:30%; max-width:30%;position:relative;overflow: hidden;width: 200px;height: 200px;";
    var product_img_img_style = "width:100%;height:100%; border-top-left-radius:5px;border-bottom-left-radius: 5px; ";
    var img_content_style = "flex-grow: 1;padding: 10px 13px;display: flex;justify-content: space-between;flex-direction: column;";
    var product_price_content_style = "display: flex;justify-content: space-between; align-items:center;";
    var product_price_style = "color: #dca678; font-size: 15px;";
    var buy_button_style = "height: 25px;width: 90px;background: black;border-radius: 5px;color: white;text-align: center;font-size: 13px;line-height: 25px;";

    //初始化富文本
    $summernote = $('#summernote');
    $summernote.summernote({
        lang: "zh-CN",
        height: "500",
        callbacks: {
            onImageUpload: function (files) {
                var length = files.length;
                for (var i = 0; i < length; i++) {
                    sendFile(files[i]);
                }
            },
            onInit: function() {
                addPreviewButton();
            }
        }
    });

    // add preview button to editor
    function addPreviewButton(){
        var noteBtn = '<button id="previewEditor" type="button" class="btn btn-default btn-sm btn-small" title="预览" data-event="" tabindex="-1"><i class="fa fa-eye"></i></button>';
        var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
        $(fileGroup).appendTo($('.note-toolbar'));
        // Button tooltips
        $('#previewEditor').tooltip({container: 'body', placement: 'bottom'});
        // Button events
        $('#previewEditor').click(function(event) {
            $('#editor-preview-tmpl').empty().append($('#summernote').val());
            Helper.modalToggle($('#modal-editor-preview'));
        });
    }

    //upload
    function sendFile(file, editor, welEditable) {
        var data = new FormData();
        data.append('file', file);
        data.append('_token', "{{ csrf_token() }}");
        data.append('provider', 'subjectCont');
        $.ajax({
            url: '{!! action("Admin\\HelperController@summernoteImageUpload") !!}',
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false
        }).done(function (url) {
            $('#summernote').summernote('insertImage', url, file.name);
        }).fail(function () {
            console.log("error");
        }).always(function () {
            console.log("complete");
        });
    }

    //上传主图
    @if (isset($subject))
        Helper.setDefaultImg($('#uploadImg'), "{{$subject->largeImage}}");
    @else
        Helper.setDefaultImg($('#uploadImg'));
    @endif
    $("#subject-image").change(function () {
        Helper.readURL(this);
        $('#editIden').val('-1');
    });

    //图片库选择图片，插入富文本中
    var page = 1;
    var object ={};
    var provider = 'subjectCont';
    $('#insert-image').on('click', function () {
        $summernote.summernote('saveRange');
        $.ajax({
            url: "{!! action('Admin\\ImageController@pageImage') !!}",
            type: 'GET',
            data:{page:page, provider:provider,}
        })
        .done(function (data) {
            object.data = data;
            $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
            Helper.modalToggle($('#modal-image-select'));
        });
    });

    // 上一页
     $('body').on('click','#prePage',function() {
        if(page >=1)--page;
        $.ajax({
            url: "{!! action('Admin\\ImageController@pageImage') !!}",
            type: 'GET',
            data:{ page:page, provider:provider,}
        })
        .done(function (data) {
            object.data = data;
            if(data.length == 0){
                layer.msg('已是首页',{icon: 0});
                page++;
                return false;
            }
            $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
        });
    });

    // 下一页
    $('body').on('click','#nextPage',function() {
        $.ajax({
            url: "{!! action('Admin\\ImageController@pageImage') !!}",
            type: 'GET',
            data:{page:++page, provider:provider,}
        })
        .done(function (data) {
            object.data = data;
            if(data.length ==0){
                --page;
                layer.msg('已是最后一页',{icon: 0});
                return false;
            }
            $('#image-select-tmpl').empty().append(tmpl('tmpl-image-select', object));
            // Helper.modalToggle($('#modal-image-select'));
        });
    });

    $('#modal-image-select').on('click', '#imageSelectSubmit', function () {
        if (imageSelected !== undefined) {
            $summernote.summernote('restoreRange');
            $summernote.summernote('insertImage', imageSelected)
        }
        Helper.modalToggle($('#modal-image-select'));
    });

    //商品库选择商品，插入富文本中
    $('#insert-product').click(function () {
        // $('#product-table').empty();
        $summernote.summernote('saveRange');
        Helper.modalToggle($('#modal-product-select'));
    });
    $('#search-product').click(function () {
        var keyword = $('input[name="keyword"]').val();
        if (!keyword.trim()) {
            return;
        }
        $.ajax({
            url: "{!! action('Admin\ProductController@search') !!}",
            type: 'post',
            data: {
                "keyword": keyword
            }
        }).done(function (data) {
            if (data.status == 'success') {
                if (!data.data) {
                    $('#product-table').empty().append('没有搜到相关信息');
                } else {
                    var tr = "";
                    $.each(data.data.data, function (index, item) {
                        tr += '<tr><td>' + item.snumber + '</td><td>' + item.name
                            + '</td><td><div class="btn-group"><a href="javascript:;" class="btn btn-xs btn-default-bright select-product"'
                            + '" data-json=\'' + JSON.stringify(item) + '\'>选择</a></div></td></tr>';
                    });
                    $('#product-table').empty().append(tr);
                }
            } else if (data.status == 'error') {
                layer.msg(data.message)
            }
        });
    });
    $('#product-table').on('click', '.select-product', function () {
        $summernote.summernote('saveRange');
        var product = $(this).data('json');
        Helper.modalToggle($('#modal-product-select'));

        //商品信息插入富文本
        if (!product.main_image) {
            product.main_image = "{!! config('amii.image') !!}"
        }

        var node = '<div style=" ' + product_wrapper_style + '"><div style="' + product_img_style + '"><img class="square" src="' + product.main_image + '" style="' + product_img_img_style + '"></div><div style="' + img_content_style + '"><div style="overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;line-height: 18px;font-size: 12px;color: #333333;">' + product.name + '</div><div style="' + product_price_content_style + '"><div style="' + product_price_style + '">¥ ' + product.product_price + '</div><a href="javascript:;" onclick="install_app(' + product.id + ')"><div style="' + buy_button_style + '" id="' + product.id + '">立即购买</div></a></div></div></div><p><br></p>';

        //加入关联商品checkbox
        var product_input_node = '<label class="checkbox-inline checkbox-styled checkbox-primary"><input type="checkbox" value="' + product.id
            + '" name="product_ids[]" checked=""><span>' + product.name + '</span></label>';

        $('#contact-products').append(product_input_node);
        $summernote.summernote('restoreRange');
        $summernote.summernote('insertNode', $(node)[0]);
        $summernote.summernote('removeFormat');
        $summernote.summernote('restoreRange');
    });

    // editor preview
    $('#editor-preview').on('click', function () {
        $('#editor-preview-tmpl').empty().append($('#summernote').val());
        Helper.modalToggle($('#modal-editor-preview'));

        // 调整富文本插入图片显示尺寸
        $("#editor-preview-tmpl").find('p > img').prop('style', 'width:100%');
    });

    $("#previewEditor").on('click', function(){
        $("#editor-preview-tmpl").find('p > img').prop('style', 'width:100%');
    })
</script>
@stop
