@extends('admin.widget.body')

@section('content')
<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">专题列表</li>
        </ol>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="card-head">
                    <div class="tools">
                        <div class="btn-group">
                            <a href="{!! action('Admin\SubjectController@create') !!}" class="btn ink-reaction btn-primary btn-sm pull-right">
                                添加
                            </a>

                        </div>
                    </div>
                </div>
                <table class="table table-hover table-condensed table-striped no-margin">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>标题</th>
                            <th>子标题</th>
                            <th>专题主图</th>
                            <th>排序权重（降序）</th>
                            <th>浏览数</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($subjects as $subject)
                        <tr>
                            <td>{{ $subject->id }}</td>
                            <td>{{ $subject->title }}</td>
                            <td >{{ $subject->subtitle }}</td>
                            <td>
                                <img src="{{ $subject->smallImage }}" height="40">
                            </td>
                            <td>{{ $subject->weight }}</td>
                            <td >{{ $subject->scan }}</td>
                            <td>
                                <!-- <button type="button" class="btn btn-primary btn-xs copybtn" data-clipboard-action="copy" data-clipboard-text="{{ $subject->id }}">复制链接</button> -->
                                @if($subject->status == 1)
                                    <a href="javascript:void(0);" class="btn ink-reaction btn-danger btn-xs js-status" data-id="{{$subject->id}}" data-status="{{$subject->status ? 0 :1}}" type="button">
                                      禁用
                                     </a>

                                @else
                                    <a href="javascript:void(0);" class="btn ink-reaction btn-primary btn-xs js-status" data-id="{{$subject->id}}" data-status="{{$subject->status ? 0 :1}}" type="button">
                                      开启
                                     </a>
                                @endif
                                <a href="{{ action('Admin\SubjectController@edit', $subject->id)}}" class="btn ink-reaction btn-primary btn-xs" type="button">编辑</a>
                                <a href="javascript:;" data-url="{{ action('Admin\SubjectController@destroy', $subject->id)}}" class="btn ink-reaction btn-danger btn-xs subject-delete" type="button">删除</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($subjects->toArray()['data'])
                <div class="col-md-12 col-lg-12">
                    <label style="color: black;font-size: 16px">当前页数量: {{$subjects->toArray()['to'] - $subjects->toArray()['from'] + 1}}</label>
                    <label style="color: black;font-size: 16px">总数量: {{$subjects->toArray()['total']}}</label>
                </div>
                @endif
                <div class="text-center">
                    {!! $subjects->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('script')
<script type="text/javascript" src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/assets/lib/clipboard/clipboard.min.js"></script>
<script type="text/javascript">

    // 复制链接
    var copybtn = new Clipboard('.copybtn');
    copybtn.on('success', function(e) {
         layer.msg('复制成功', {icon: 1});
    });
    copybtn.on('error', function(e) {
        layer.msg('复制失败', {icon: 0});
    });


    $('.subject-delete').click(function () {
        var url = $(this).attr('data-url');
        layer.confirm('确定删除?', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'JSON',
                success: function (returnData) {
                    if (returnData.status == 'success') {
                        layer.msg('删除成功', {icon: 1});
                        setTimeout(function(){
                            window.location.reload();
                        },1000)
                    }else{
                        layer.msg('删除失败,请刷新重试',{icon: 0});
                    }

                }
            });
        });
    })

    $('.js-status').click(function(){
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        $.ajax({
            url: "{{action('Admin\\SubjectController@status')}}",
            type: 'POST',
            dataType: 'JSON',
            data:{'id':id,'status':status},
            success: function (returnData) {
                if (returnData.status == 'success') {
                    layer.msg('操作成功', {icon: 1});
                    setTimeout(function(){
                        window.location.reload();
                    },1000)
                }else{
                    layer.msg('操作失败,请刷新重试',{icon: 0});
                }

            }
        });
    })
</script>
@stop
