<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="x5-page-mode" content="app"/>
    <meta name="browsermode" content="application">
    <meta name="description" content="AMII商城">
    <meta name="format-detection" content="telephone=no" />

    <title>A+</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    {{--<base href="/">--}}

    {{--<script src="//cdn.bootcss.com/eruda/1.2.2/eruda.min.js"></script>--}}
    <script>typeof eruda !== 'undefined' && eruda.init();</script>

    {{--<link rel="stylesheet" href="//at.alicdn.com/t/font_q0nwiq733c2qpvi.css">--}}
    <link rel="stylesheet" href="{{ mix('/assets/wechat/css/app.css') }}">
    <script src="https://jic.talkingdata.com/app/h5/v1?appid=0B5A973A679F491D82BCD798B10681DC&vn=amii&vc=1.0"></script>
</head>
<body class="gray-bg">
<div id="app"></div>

<script src="//map.qq.com/api/js?v=2.exp&key=756BZ-YB6WW-HB5RZ-RBERN-LASC5-XDBMB&libraries=convertor"></script>
@if($wechat_config)
    <script src="{{ url('assets/lib/jweixin-1.2.0.js') }}"></script>
@endif
{{--<script src="//cdn.bootcss.com/es6-shim/0.35.3/es6-shim.min.js"></script>--}}
{{--<script src="//cdn.bootcss.com/babel-polyfill/6.23.0/polyfill.min.js"></script>--}}
<script src="//cdn.bootcss.com/fastclick/1.0.6/fastclick.min.js"></script>
<script src="{{ mix('/assets/wechat/js/manifest.js') }}"></script>
<script src="{{ mix('/assets/wechat/js/vendor.js') }}"></script>
<script src="{{ mix('/assets/wechat/js/app.js') }}"></script>
</body>
</html>
