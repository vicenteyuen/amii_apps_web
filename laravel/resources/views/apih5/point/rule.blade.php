@extends('apih5.widget.base',['head_titile' => '积分规则'])

@section('content')
    <style type="text/css">
        .rule-cont{
            padding: 14px 15px;
        }
        .rule-cont h1{
            margin: 0 0 10px 0;
            font-size: 16px;
        }
        .rule-cont .mt{
            margin-top: 10px;
        }
        .rule-cont table{
            text-align: center;
            border-collapse:collapse;
            border: 1px solid rgb(230, 230, 230);
        }
        .rule-cont table td{
            border: 1px solid rgb(230, 230, 230);
            font-size: 9px;
            padding: 9px 2%;
            color: #333333;
        }
        .rule-cont table thead td{
            background-color: #e0af5b;
            font-size: 10px;
            color: #fff;
        }
        .award-content{
            margin-bottom: 34px;
        }
        .award-content-items{
            vertical-align: middle;
            line-height: 2;
            font-size: 12px;
        }
        .award-content-items::before{
            content: '';
            display: inline-block;
            width: 4px;
            height: 4px;
            border-radius: 100%;
            background: rgb(215, 149, 35);
            vertical-align: middle;
            margin: 0 7.5px 0 5px;
        }
        .award-content-items span{
            color: red;
        }
        iframe{
            border: 0;
            width: 100%;
        }
    </style>
    <div class="rule-cont">
        <h1>积分有什么用？ </h1>
        <div class="award-content">
            <div class="award-content-items">在消费时可抵现金使用（100积分=1元，最多抵扣实付金额的50%）；</div>
            <div class="award-content-items">在积分中心兑换各种超值好礼；</div>
            <div class="award-content-items">试手气去抽奖，赢得超级大奖；<span>（敬请期待）</span></div>
            <div class="award-content-items">积分可转赠给朋友。<span>（敬请期待）</span></div>
        </div>
        <h1>如何赚积分？ </h1>
        <table>
            <thead>
                <td width="20%">积分获取</td>
                <td width="40%">获取方式</td>
                <td width="40%">获得积分</td>
            </thead>
            <tbody>
                <tr>
                    <td>注册</td>
                    <td>成功注册APP账号，验证手机， 成为专属会员</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td rowspan="2">个人资料</td>
                    <td>完善信息1 ：生日/收货地址</td>
                    <td>5/每条</td>
                </tr>
                <tr>
                    <td>完善信息2 ：绑定微信、QQ、 微博等账号</td>
                    <td>10/每个账号</td>
                </tr>
                <tr>
                    <td rowspan="2">形象管理特权</td>
                    <td>设置性别/身高/体重/肩宽/胸围/ 腰围/臀围/大腿围/小腿围/ AMII购买上装尺码/下装尺码 </td>
                    <td>5/每条</td>
                </tr>
                <tr>
                    <td>设置正面全身照/侧面全身照</td>
                    <td>25/每条</td>
                </tr>
                <tr>
                    <td>邀请</td>
                    <td>发出邀请链接，成功邀请朋友注册</td>
                    <td>200/人</td>
                </tr>
                <tr>
                    <td>购物</td>
                    <td>购买商品</td>
                    <td>按照实付金额取整计算 </td>
                </tr>
                <tr>
                    <td>天数达标<br/>（额外奖励）</td>
                    <td>签到</td>
                    <td>第一天签到5积分，后续签到每天增加5积分，最多10积分；若断开，则从5积分开始。</td>
                </tr>
                <tr>
                    <td rowspan="2">购买评论</td>
                    <td>普通评论<br/>（纯文字）</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>图片评论<br/>（图片+20字以上文字）</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td rowspan="2">图文专区参与<br/>（最多5次/天）</td>
                    <td>点赞</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>评论</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td rowspan="4">分享<br/>（最多5次/天）</td>
                    <td>活动分享（与游戏链接合并）</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>图文分享（专题、视频）</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>商品链接</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>商品链接+个人评价（买家秀）</td>
                    <td>15</td>
                </tr>
                <tr>
                    <td>客户建议 </td>
                    <td>客户填写反馈建议</td>
                    <td>每次反馈得5积分，最多50积分/月，一旦采纳建议另外赠送200积分</td>
                </tr>
            </tbody>
        </table>
        <iframe src="/apih5/point/award"
            id="iFrame2"
            name="iFrame2"
            onload="this.height=iFrame2.document.body.scrollHeight;">
        </iframe>
    </div>
@stop
