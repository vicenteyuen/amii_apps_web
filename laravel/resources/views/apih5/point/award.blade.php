@extends('apih5.widget.base',['head_titile' => '积分奖励'])

@section('content')
    <style type="text/css">
        .award-cont{
            padding: 0 10px;
        }

        .award-items{
            padding: 22px 0;
            font-size: 12px;
        }

        .award-title{
            display: inline-block;
            padding: 10px;
            font-size: 14px;
            color: #fff;
            background: #d79523;
            line-height: 1;
            margin-bottom: 13px;
            border-radius: 2px;
        }

        .award-content-items{
            vertical-align: middle;
            line-height: 2;
        }
        .award-content-items::before{
            content: '';
            display: inline-block;
            width: 4px;
            height: 4px;
            border-radius: 100%;
            background: rgb(215, 149, 35);
            vertical-align: middle;
            margin: 0 7.5px 0 5px;
        }

        .award-content{
            line-height: 2;
        }

        .award-content-span{
            font-size: 11px;
            color: #999999;
        }

        .award-content table{
            width: 100%;
            text-align: center;
            border-collapse:collapse;
            border: 1px solid rgb(204, 204, 204);
            font-size: 12px;
        }
        .award-content table thead td{
            padding: 8px 0;
        }
        .award-content table img{
            width: 55px;
        }
        .award-content table td{
            border: 1px solid rgb(204, 204, 204);
        }
        .award-content table tbody td{
            padding: 15px 0;
        }
        .award-content-number{
            text-indent: 17px;
        }
        .award-content-number span{
            color: rgb(215, 149, 35);
        }
    </style>
    <div class="award-cont">
        <div class="award-items">
            <div class="award-title">奖励规则</div>
            <div class="award-content">
                <div class="award-content-items">月度积分排名前三/年度排名前十可获得积分兑换奖品的机会；</div>
                <div class="award-content-items">月度/年度奖励方式：</div>
                <div class="award-content-number"><span>使用可用积分兑换礼品</span>或下一级礼品，奖品可自主选择（除了平台提供的参考奖品，获奖用户可自主提供价值范围内的意向奖品的购买链接，我们尽量满足您的需求喔~）；</div>
                <div class="award-content-items">出现积分排名并列时，月度/年度成交金额高的名次排前。</div>
                <div class="award-content-span">*6个月内最多获得2次月度兑换奖品机会，希望给其他用户更多获奖机会，当然你还可以竞选年度大奖喔</div>
            </div>
        </div>
        <div class="award-items">
            <div class="award-title">7月月度奖励</div>
            <div class="award-content">
                <table>
                    <thead>
                        <td>月度排名</td>
                        <td>7月奖品价值</td>
                        <td>兑奖所需积分</td>
                        <td>图片<br/>（仅供参考）</td>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>300元以内<br/>（参考奖品：500GB移动硬盘）</td>
                            <td>15000</td>
                            <td>
                                <img src="assets/images/point/point-img1.png">
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>200元以内<br/>（参考奖品：蕉下小黑伞）</td>
                            <td>10000</td>
                            <td>
                                <img src="assets/images/point/point-img2.png">
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>100元以内<br/>（参考奖品：JAYJUN面膜10片）</td>
                            <td>5000</td>
                            <td>
                                <img src="assets/images/point/point-img3.png">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- <div class="notice">7月月度积分排名奖品待定，敬请期待~</div> -->
            </div>
        </div>
        <div class="award-items">
            <div class="award-title">2017年度奖励</div>
            <div class="award-content">
                <table>
                    <thead>
                        <td>年度排名</td>
                        <td>奖品</td>
                        <td>兑奖所需积分</td>
                        <td>图片<br/>（仅供参考）</td>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>银行50g黄金纪念币</td>
                            <td>375000</td>
                            <td>
                                <img src="assets/images/point/point-img4.png">
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>携程礼品卡</td>
                            <td>250000</td>
                            <td>
                                <img src="assets/images/point/point-img5.png">
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>iPhone 256G<br/>（最新版）</td>
                            <td>200000</td>
                            <td>
                                <img src="assets/images/point/point-img6.png">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="4">4</td>
                            <td rowspan="4">周大福首饰4件套<br/> (戒指+项链+<br/>手链+耳钉)</td>
                            <td rowspan="4">150000</td>
                            <td>
                                <img src="assets/images/point/point-img7.png">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="assets/images/point/point-img8.png">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="assets/images/point/point-img9.png">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="assets/images/point/point-img10.png">
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>手表(施华洛世奇)</td>
                            <td>112500</td>
                            <td>
                                <img src="assets/images/point/point-img11.png">
                            </td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>护肤套装：SK2</td>
                            <td>75000</td>
                            <td>
                                <img src="assets/images/point/point-img12.png">
                            </td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Calvin Klein：包包</td>
                            <td>50000</td>
                            <td>
                                <img src="assets/images/point/point-img13.png">
                            </td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>新秀丽：行李箱</td>
                            <td>37500</td>
                            <td>
                                <img src="assets/images/point/point-img14.png">
                            </td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>dior：香水</td>
                            <td>25000</td>
                            <td>
                                <img src="assets/images/point/point-img15.png">
                            </td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>沃尔玛超市礼品卡</td>
                            <td>12500</td>
                            <td>
                                <img src="assets/images/point/point-img16.png">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
