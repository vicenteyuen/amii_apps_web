@extends('apih5.widget.base', ['head_titile' => '全民推荐，佣金轻松享'])

@section('content')
    <style type="text/css">
        .sales-home{
            width: 100%;
            min-height: 100vh;
            background: url(/assets/images/sales-bg.png) no-repeat top center;
            -webkit-background-size: 100% 100%;
            -moz-background-size: 100% 100%;
            -ms-background-size: 100% 100%;
            -o-background-size: 100% 100%;
            background-size: 100% 100%;
        }
    </style>
    <div class="sales-home"></div>
@stop
