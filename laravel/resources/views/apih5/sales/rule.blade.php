@extends('apih5.widget.base', ['head_titile' => '邀请好友，礼券佣金立即享'])

@section('content')
    <style type="text/css">
        .sales-rule{
            background: #fed773;
            padding: 25px 10px 60px;
            font-size: 12px;
            color: #560d18;
        }

        .sales-items{
            position: relative;
            margin-bottom: 20px;
            padding: 0 0 20px;
            line-height: 2;
            border-radius: 2px;
            background: #fff;
        }

        .sales-title{
            display: inline-block;
            position: relative;
            padding: 8px 26px 12px 15px;
            font-size: 14px;
            line-height: 1;
            color: #fff;
            left: -11px;
            top: -6px;
            background: url(/assets/images/sales-title.png) no-repeat left center;
            -webkit-background-size: contain;
            -moz-background-size: contain;
            -ms-background-size: contain;
            -o-background-size: contain;
            background-size: contain;
        }
        .sales-items:last-child .sales-title{
            padding: 8px 15px 12px;
        }

        .sales-items:last-child{
            padding-bottom: 0;
        }

        .sales-ul{
            padding: 15px 15px 15px 35px;
            list-style-type: decimal;
        }
        .sales-content{
            margin-top: -15px;
        }
        .sales-notice{
            font-size: 10px;
            color: #999999;
            padding: 0 15px;
        }
        .sales-content-items{
            padding: 0 15px;
        }
        .sales-content-items .sales-notice{
            padding: 0;
        }
        .sales-content-items .title{
            color: #ff3f00;
            font-size: 13px;
            margin: 20px 0 12px;
            line-height: 1;
            font-weight: 500;
        }
        .sales-content-items .sales-text{
            text-indent: 10px;
        }

        .sales-images{
            width: 100%;
            margin: 20px auto 26px;
        }

        .example .title{
            color: #560d18;
            font-weight: normal;
        }
        .example .sales-text{
            text-indent: 0;
        }

    </style>
    <div class="sales-rule">
        <div class="sales-items">
            <div class="sales-title">分佣规则</div>
            <div class="sales-content">
                <div class="sales-content-items">
                    <div class="title">成为部落成员</div>
                    <div class="sales-text">一级部落：您发起分享→好友通过您的分享成功注册→成为您的一级部落;</div>
                    <div class="sales-text">二级部落：一级部落发起分享→他人通过其分享成功注册→成为您的二级部落</div>
                    <div class="sales-notice">
                        温馨提示：<br/>
                        您的一级及二级部落通过您所分享内容中带上的唯一识别码予以永久性绑定，不可取消或反向绑定。
                    </div>
                </div>
                <div class="sales-content-items">
                    <div class="title">佣金分享机制</div>
                    <div class="sales-text">您将获得一二级部落交易订单实付金额的10%返佣，图示：</div>
                </div>
                <div class="sales-images">
                    <img src="/assets/images/sales-rule.png" alt="">
                </div>
                <div class="sales-notice">
                    温馨提示：<br/>
                    1. 部落规模最大化的要点是尽可能扩大您的一级部落！<br/>
                    2. 部落成员只要在规定的渠道成功下单购买，您都将获得返佣！目前AMII微商城、小程序和APP已打通返佣机制，线上线下全渠道即将打通，敬请期待！<br/>
                    3. 2017年首年年度返佣排名第一名，平台额外奖励一二级部落实付交易额的5%返佣，即享受一级实付金额的15%佣金，享受二级实付金额的15%佣金，品牌返佣高达30%！
                </div>
                <div class="sales-content-items">
                    <div class="title">佣金发放规则</div>
                    <div class="sales-text">一二级部落人员下单购买→发货→收到货确认收货→7天后系统自动返佣到个人账户</div>
                </div>
                <div class="sales-content-items example">
                    <div class="title">例子：</div>
                    <div class="sales-text">您邀请好友小美注册为会员，小美成功下单实付了100元美衣，您将获得10元佣金；小美邀请好友小丽注册成为会员，小丽成功下单实付了100元美衣，您还将获得10元佣金；日后小美、小丽在平台的所有交易您都将获得10%的佣金！</div>
                </div>
            </div>
        </div>
        <div class="sales-items">
            <div class="sales-title">邀新红包规则</div>
            <ul class="sales-content sales-ul">
                <li class="sales-text">邀请好友新加入注册成功后，您将获得10元优惠券（满100减10），好友获得20优惠券（满100减20）；</li>
                <li class="sales-text">本活动最终解释权归平台所有。</li>
            </ul>
        </div>
    </div>
@stop
