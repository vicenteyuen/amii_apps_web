<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="{{ url('/') }}">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $head_titile or '' }}</title>
    <style>
        body{
            margin: 0;
        }
        img {
            max-width: 100% !important;
            vertical-align: middle;
        }
        video {
            max-width: 100%;
            height: auto !important;
        }
        video {
            max-width: 100%;
            height: auto !important;
        }
        div {
            max-width: 100%;
            height: auto !important;
        }
        a{
            text-decoration: none;
        }
    </style>
</head>
<body>
@yield('content')
<script>
    var q, i;
    q = document.querySelectorAll("img,video");
    for (i = 0; i < q.length; i++) {
        q[i].removeAttribute("style");
        q[i].removeAttribute("width");
        q[i].removeAttribute("height")
    }
</script>
</body>
</html>
