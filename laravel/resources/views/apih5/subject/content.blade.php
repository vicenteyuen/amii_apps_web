@extends('apih5.widget.base')

@section('content')
    {!! $subject->content !!}
    @foreach ($subject->products as $product)
    <script type="text/javascript">
        function install_app(sign) {
            document.getElementById("application-" + sign).src = "amii://product?id={{ $product->product_id }}";
        }
    </script>
    <iframe id="application-{{ $product->product_id }}" width="1" height="1" style="visibility:hidden"></iframe>
    @endforeach
@stop
