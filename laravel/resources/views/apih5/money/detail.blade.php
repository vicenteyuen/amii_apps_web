@extends('apih5.widget.base',['head_titile' => '邀请好友，礼券佣金立即享'])

@section('content')
    <style type="text/css">
        .money-detail{
            width: 100%;
            min-height: 100vh;
            background: url(/assets/images/money/detail-bg.png) no-repeat top center;
            -webkit-background-size: 100% 100%;
            -moz-background-size: 100% 100%;
            -ms-background-size: 100% 100%;
            -o-background-size: 100% 100%;
            background-size: 100% 100%;
        }

        .side-button{
            position: absolute;
            right: 0;
            top: 40px;
        }
        .rule-button{
            background-color: rgb(253, 156, 1);
            box-shadow: 0px 5px 5px 0px rgba(69, 0, 96, 0.3);
            padding: 9px 6px;
            text-align: center;
            color: #fff;
            font-size: 12px;
            writing-mode: vertical-rl;
            line-height: 1.2;
            margin-bottom: 9px;
            cursor: pointer;
            border-radius: 2px;
        }
        .qrcode-button{
            background-color: rgb(253, 156, 1);
            box-shadow: 0px 5px 5px 0px rgba(69, 0, 96, 0.3);
            padding: 3px 0 4px;
            width: 26px;
            text-align: center;
            cursor: pointer;
            border-radius: 2px;
        }

        .qrcode-button img{
            width: 14px;
            height: 14px;
        }

        .money-detail .detail-content{
            position: absolute;
            top: 34%;
            left: 50%;
            width: 80%;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%);
        }
        .money-detail .items{
            position: relative;
            margin: 19px 0 4%;
            padding: 0 10px 10px 17.5px;
            background: #fff3de;
            border: 2px dashed rgb(255, 148, 0);
            border-radius: 10px;
            text-align: center;
        }
        .items .title{
            position: absolute;
            left: 50%;
            top: -15px;
            display: inline-block;
            width: 57px;
            text-align: center;
            padding: 5px 0;
            font-size: 11px;
            background: #ff9400;
            color: #fff;
            border-radius: 120px;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%);
        }

        .money-share .title{
            position: relative;
            text-align: center;
            padding: 0 2.5px;
            margin-bottom: 6%;
        }
        .money-share .title span{
            display: inline-block;
            width: 67px;
            padding: 6px 0;
            font-size: 11px;
            color: #fff;
            background: #f53344;
            border-radius: 120px;
            position: absolute;
            top: 0;
            left: 50%;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%);
        }
        .money-share .title::before{
            content: '';
            display: inline-block;
            border-bottom: 2px dashed rgb(245, 51, 68);
            width: 100%;
        }
        .share-items{
            display: inline-block;
            width: 25%;
            float: left;
            text-align: center;
        }
        .share-items .text{
            font-size: 11px;
            color: rgba(0, 0, 0, 0.502);
            padding-top: 5px;
        }
        .share-items img {
            width: 60%;
        }

        .shadow-box{
            display: none;
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            background: rgba(0, 0, 0, 0.6);
            padding-top: 62px;
        }
        .shadow-box .content{
            position: relative;
            width: 75%;
            background: #fff;
            margin: 0 auto;
            border-radius: 7px;
            padding: 0 15px 20px;
        }
        .shadow-box .border{
            border: 3px solid rgb(254, 60, 76);
        }
        .shadow-box .title{
            text-align: center;
        }
        .shadow-box .title img{
            position: relative;
            top: -35px;
            width: 70%;
        }
        .shadow-box ul{
            margin-top: -10px;
            font-size: 12px;
            padding: 0 0 0 20px;
        }
        .shadow-box ul li {
            margin-bottom: 27.5px;
            list-style-type: decimal
        }
        .shadow-box .close{
            position: absolute;
            bottom: -70px;
            left: 0;
            text-align: center;
            width: 100%;
        }
        .shadow-box .close img {
            width: 50px;
            height: 50px;
        }

        .qrcode-title{
            text-align: center;
            font-size: 15px;
            font-weight: bold;
            color: #fe3c4c;
            padding: 20px 0;
        }
        .qrcode-content{
            text-align: center;
        }
        .qrcode-images{
            position: relative;
            width: 180px;
            height: 180px!important;
            overflow: hidden;
            margin: 0 auto;
        }
        .qrcode-images img{
            width: 100%;
        }
        .qrcode-content .notice{
            font-size: 11px;
            color: rgba(0, 0, 0, 0.502);
            margin-top: 20px;
        }
        .share-box{
            display: none;
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            background: rgba(0, 0, 0, .6);
            color: #fff;
            text-align: center;
            font-size: 16px;
            z-index: 9999;
        }
        .share-icon{
            display: block;
            text-align: right;
            margin: 15px 0 11px 0;
            width: 100%;
            height: 61px;
            background: url('/assets/images/arrow.png') no-repeat 95% center;
            -webkit-background-size: contain;
            -moz-background-size: contain;
            -ms-background-size: contain;
            -o-background-size: contain;
            background-size: contain;
        }
        .share-box .notice{
            margin: 110px 0;
            line-height: 1.6;
            font-size: 15px;
        }

        img{
            width: 130px;
        }

        .items-invite{
            padding-top: 20px;
            font-size: 10px;
            color: #f13540;
            font-weight: bold;
            line-height: 1.5;
        }
        .items-invite ul{
            padding: 0 0 0 20px;
            display: inline-block;
            width: 57%;
            vertical-align: middle;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
            text-align: left;
            margin: 0;
        }
        .items-invite ul li {
            list-style-type: decimal;
        }
        .items-invite-img{
            display: inline-block;
            width: 40%;
            vertical-align: middle;
        }
        .items-content{
            padding: 22.4px 0 10px;
            text-align: left;
            font-size: 10px;
            color: #f13540;
            font-weight: bold;
            line-height: 1.5;
            overflow: hidden;
        }
        .items-content img{
            display: inline-block;
            width: 40%;
            vertical-align: middle;
        }
        .coupon-title{
            display: inline-block;
            width: 50%;
            vertical-align: middle;
            padding-left: 12px;
        }

        @media screen and (max-width: 361px) {
            .money-detail{
                padding-bottom: 13%;
            }
            .detail-content{
                top: 34.5%!important;
            }
        }
        @media screen and (min-width: 362px){
            .money-detail{
                padding-bottom: 8px;
            }
        }
    </style>
    <div class="money-detail">
        <div class="side-button">
            <div class="rule-button" id="open-rule">活动规则</div>
            <div class="qrcode-button" id="open-qrcode">
                <img src="/assets/images/money/qrcode.png" alt="">
            </div>
        </div>
        <div class="detail-content">
            <div class="items">
                <div class="title">邀请者</div>
                <div class="items-invite">
                    <ul>
                        <li>邀请好友您将获得10元优惠券</li>
                        <li>好友下单您可获得10%佣金，好友邀请他人下单您还可再获得10%佣金。</li>
                    </ul>
                    <div class="items-invite-img">
                        <img src="/assets/images/money/coupon.png" alt="">
                    </div>
                </div>
            </div>
            <div class="items">
                <div class="title">被邀请者</div>
                <div class="items-content">
                    <img src="/assets/images/money/coupon1.png" alt="">
                    <div class="coupon-title">获得20元优惠券</div>
                </div>
            </div>
            <div class="money-share">
                <div class="title"><span>立即分享</span></div>
                <div class="share-items" data-type="wechat">
                    <img src="/assets/images/money/wechat.png" alt="">
                    <div class="text">微信</div>
                </div>
                <div class="share-items" data-type="moments">
                    <img src="/assets/images/money/moments.png" alt="">
                    <div class="text">朋友圈</div>
                </div>
                <div class="share-items" data-type="qq">
                    <img src="/assets/images/money/qq.png" alt="">
                    <div class="text">QQ</div>
                </div>
                <div class="share-items" data-type="weibo">
                    <img src="/assets/images/money/weibo.png" alt="">
                    <div class="text">微博</div>
                </div>
            </div>
        </div>
        <div class="shadow-box" id="rule">
            <div class="content border">
                <div class="title">
                    <img src="/assets/images/money/rule-title.png" alt="">
                </div>
                <ul>
                    <li>
                        邀请好友新加入注册成功后，您将获得10元优惠券（满100减10），好友获得20优惠券（满100减20）；
                    </li>
                    <li>
                        好友成功下单后您可获得实付金额10%的佣金，且获得好友再邀请他人成功下单实付金额的10%佣金；
                    </li>
                    <li>
                        本活动最终解释权归平台所有。
                    </li>
                </ul>
                <div class="close">
                    <img src="/assets/images/money/close.png" alt="">
                </div>
            </div>
        </div>
        <div class="shadow-box" id="qrcode">
            <div class="content">
                <div class="qrcode-title">
                    我的邀请二维码
                </div>
                <div class="qrcode-content">
                    <div class="qrcode-images">
                        <img src="" alt="">
                    </div>
                    <div class="notice">长按二维码保存并分享</div>
                </div>
                <div class="close">
                    <img src="/assets/images/money/close.png" alt="">
                </div>
            </div>
        </div>
        <div class="share-box" id="wechat-share">
            <i class="share-icon"></i>
            <span>请点击右上角发送给朋友</span>
        </div>
        <div class="share-box" id="share">
            <div class="notice">请使用浏览器<br/>自带分享功能发送给好友  </div>
            <img src="/assets/images/share-button.png">
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function (){
            var rule = document.getElementById('rule'),
                qrcode = document.getElementById('qrcode'),
                close = document.querySelectorAll('.close'),
                open_rule = document.getElementById('open-rule'),
                open_qrcode = document.getElementById('open-qrcode'),
                items = document.querySelectorAll('.share-items'),
                shareBox = document.getElementById('share'),
                wechat_share = document.getElementById('wechat-share');

                open_rule.onclick = function () {
                    rule.style.display = 'block';
                }
                open_qrcode.onclick = function () {
                    qrcode.style.display = 'block';
                }

                for (var i = 0; i < close.length; i++) {
                    close[i].onclick = function (){
                        this.parentNode.parentNode.style.display = 'none';
                    }
                }
                for (var i = 0; i < items.length; i++) {
                    items[i].onclick = function (){
                        if (navigator.userAgent.toLowerCase().match(/amii\/app/i) == 'amii/app') {
                            window.location.href='amii://share?provider=red_packet&method='+this.getAttribute("data-type");
                            return false;
                        }
                        if (navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger') {
                            wechat_share.style.display = 'block';
                            shareBox.style.display = 'none';
                        } else {
                            wechat_share.style.display = 'none';
                            shareBox.style.display = 'block';
                        }
                    }
                }
                shareBox.onclick = function (){
                    this.style.display = 'none';
                }
                wechat_share.onclick = function (){
                    this.style.display = 'none';
                }
        }
    </script>
@stop
