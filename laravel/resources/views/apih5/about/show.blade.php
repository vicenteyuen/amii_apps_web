@extends('apih5.widget.base',['head_titile' => '关于我们'])

@section('content')
    <style type="text/css">
        .settings-about{
            height: 100vh!important;
            color: #FFF;
            text-align: center;
            background: url("/assets/images/about.png") no-repeat center center;
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -ms-background-size: cover;
            -o-background-size: cover;
        }
        .about-logo{
            padding: 65px 0 52px;
        }
        .about-logo img{
            width: 90px;
        }
        .about-info{
            line-height: 2;
            font-size: 12px;
        }
        .about-foot{
            position: fixed;
            bottom: 0.3rem;
            left: 0;
            width: 100%;
            font-size: 11px;
        }
        .about-phone{
            display: inline-block;
            font-size: 12px;
            margin-bottom: 0.29rem;
        }
        .about-foot img{
            width: 80%;
            margin-bottom: 18px;
        }
    </style>
    <div class="settings-about">
        <div class="about-logo">
            <img src="/assets/images/logo.png">
        </div>
        <div class="about-info">
            西伍集团成立于2008年，产品线涵盖<br/>时尚女装、童装、首饰、鞋类、箱包，<br/>目前以服饰为主导，电子商务营销为主体，<br/>逐渐发展成为网络与实体营销一体化的集团企业。<br/>西伍集团主张极简美学的原创设计风格，<br/>是一家互联网TOP10的品牌企业，<br/>旗下主推品牌有“AMII”、“AMII Redefine”、<br/>“amii童装”、”摩尼菲格”、“AMII REX”。
        </div>
        <div class="about-foot">
            <img src="/assets/images/about-logo.png" alt="">
            <span class="about-phone">中国区客服热线4006-839-776</span><br/>
        </div>
    </div>
@stop
