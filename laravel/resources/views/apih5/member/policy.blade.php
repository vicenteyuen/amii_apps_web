@extends('apih5.widget.base',['head_titile' => '用户注册协议'])

@section('content')
    <style type="text/css">
        .policy-cont{
            padding: 15px;
        }
        .policy-cont h3{
            text-align: center;
        }
        .policy-cont pre {
            white-space: pre-wrap;
        }
    </style>
    <div class="policy-cont">
        <h3>用户注册协议</h3>
        <!-- 7.17 -->
        <pre>
    本协议详述用户在使用佛山市西伍服饰有限公司（以下简称“西伍”）旗下的应用服务平台时所须遵守的条款和条件。如用户有任何疑问，请及时联系我们。
本平台在此提醒用户：在注册前，请用户务必仔细阅读用户注册协议，一旦用户在注册时点击“同意”本协议，即视为用户确认自己具有享受本平台服务、下单购物等相应的权利能力和行为能力，能够独立承担法律责任。如果用户未满18周岁，请用户在父母或监护人的监护下使用本站，否则，相关的法律后果仍由用户和用户的监护人承担。用户在申请注册流程中点击同意本协议之前，应当认真阅读本协议。请用户务必审慎阅读、充分理解各条款内容，特别是免除或者限制责任的条款、法律适用和争议解决条款。免除或者限制责任的条款将以粗体标识，用户应重点阅读。如用户对协议有任何疑问，可向平台客服咨询。成功注册成为本平台用户意味着用户已仔细阅读并自愿接受
本用户协议中所含的所有条款和条件的约束，本用户注册协议对双方均具有法律效力。
用户知晓、清晰并确认：随着国家法律法规及平台运营变化，本用户注册协议内容并未能全面覆盖并罗列用户与平台间的所有权利与义务，因此本平台有权不时地更新用户注册协议条款，更新后的用户注册协议一旦在平台上发布即生效，并代替原协议内容。用户可随时并有义务不时地登录平台，关注并查阅最新版的用户注册协议；如用户不同意更新后的用户注册协议，可以且应立即停止接受本平台依据协议提供的服务；如用户继续使用本平台提供的服务的，即视为同意更新后的用户注册协议，如用户有任何疑问，欢迎及时反馈。
一、名词释义
二、用户注册
三、交易
四、配送
五、收货
六、用户依法使用平台义务
七、隐私政策
八、通知
九、所有权及知识产权声明
十、服务终止
十一、免责声明

一、名词释义
1.1用户注册协议：指用户与平台当下订立的或平台提供服务过程中旨在约定用户登录、使用本平台，以及通过本平台下达订单、购买商品、支付货款、收取商品等整个网络购物过程中，用户与平台之间的权利、义务的书面合同。用户理解并同意前述书面合同所指包括但不限于用户注册协议、平台规则、法律声明、隐私政策、补充协议等随国家法律法规变化及平台运营需要产生的相关协议、声明、公告、规则，该相关协议、声明、公告、规则为本用户注册协议的有效组成部分，具有同等法律效力，以下统称“协议”。
1.2应用服务平台：指西伍旗下的微商城、APP、小程序、PC官网等应用服务程序、软件，以下简称“本平台”，用户理解并同意协议在前述平台下的任一程序、软件所发布则该协议均适用于本平台项下的所有程序、软件，该发布的相应协议另有明确除外。
1.3 商品：指西伍通过本平台向用户推荐并进行销售的具体商品、服务的统称。
1.4购物车：是模拟线下实体商店提供给顾客放置商品的“购物车”的一个概念，指本平台向用户展示用户有意向下达订单、准备购买但尚未支付相应购买价款的商品信息的网页区域， 又称“虚拟购物车”。
1.5 订单：指由本平台结帐程序生成的记录用户通过本平台所购买商品的名称、品牌、价款、尺寸、数量等交易信息的表格。这份文件将被用作所有可能发生的本平台与用户购买有关的询问、请求和争议的参考。
1.6送货单：指本平台附着于用户订购商品的包裹上或包裹里的、供收货人签收的商品销售凭证，如发生退换货必须寄回。
1.7 送货地址：指用户在下达订单时向本平台提供的要求商品送达的地址，如果没有特别说明，即为用户填写订单时提供的收货地址为送货地址。
1.8 用户账号：指用户按本平台要求注册获得并可据以登录本平台的账号。
1.9恶意评论：是指个人或组织对本平台或西伍对外销售的商品没有兴趣，却组织（包括但不限于人力、计算机技术等手段）用户注册在本平台上大规模发表用户评论，特别发表对本平台或西伍销售的商品负面的评论乃至违反国家有关法律法规的评论。
1.10不正当利益：用户通过歪曲或利用平台活动、规则的漏洞所获取的本不应由用户获得的利益。
1.11恶意拍单：用户恶意大量下单后又取消、恶意下单发货后申请退款、恶意下单后拒收包裹等。
二、用户注册
   2.1本平台通过实名绑定的手机号快捷注册或第三方软件直接登录使用。本平台用户注册时需填写真实、有效、合法的个人资料，以便用于账户操作或验证核实使用。如因用户填写虚假信息导致无法正常享受相应服务或账号被盗，无法申诉夺回账户，用户需自行承担相应的法律后果。如用户提供的注册资料不合法、不真实、不准确、不详尽的，用户需承担因此引起的相应责任及后果，并且本平台保留立即终止向该用户提供服务的权利。
2.2一个手机号只能注册一个账户，用户一旦注册成功，将会得到一个密码和账号（以下简称“账户”），用户应谨慎合理的保存、使用账户。必要时，用户可以根据个人需求更改账户密码或其他信息，但需自行对账户中的所有活动和事件承担法律责任。因为用户个人管理不善造成帐号、密码等被复制或被盗用，相应损失由用户自行承担。
2.3用户理解并同意：用户若发现任何非法使用其帐户的行为或存在安全漏洞的情况的，请及时告知本平台，并给予本平台充足的时间对前述非法使用行为或安全漏洞进行调查并采取相应的措施。
   2.4用户同意，本平台拥有通过邮件、短信、电话等形式，向在本平台注册、购物用户、收货人发送订单信息、促销活动等告知信息的权利。
2.5用户不得将在本平台注册获得的账户借给他人使用，否则用户应承担由此产生的全部责任，并与实际使用人承担连带责任，除非有法律规定或司法裁定，且征得本平台的同意，否则，用户的账户不得以任何方式转让、赠与或继承。
   2.6用户同意，本平台有权使用用户的注册信息、用户名、密码等信息，登录进入用户的注册账户，进行证据保全，包括但不限于公证、见证等。
三、交易
   3.1用户在本平台购买物品时请务必注意以下条款：
在本平台的一切操作应遵守所有适用的中国法律、法规、条
例和地方性法律要求。
由于手机/电脑显示差异性、互联网等现有技术水平的限制，
用户在手机/电脑上查阅的商品形状、细节、规格、颜色可能因前述客观原因的存在而存在误差。用户在下单前，请仔细确认所购商品的名称、价格、数量、型号、规格、尺寸等商品信息，如有任何疑问，请向客服确认后再下单购买。
除有明确的书面标示，本平台上显示的所有价格均以人民币
为计价单位，商品价格仅包含相应税费，不包含运费；提交订单/结账前，运费会自动计算包含在订单总价之内。在商品详情页面里用户可以找到所有的相关信息，支付价格为下订单时的有效价格。
   3.2因市场或商业环境的变化，互联网信息传递的滞后性等客观因素，用户理解并同意：
  （1）本平台将会尽最大努力向用户提供尽可能准确的商品价格、数量、是否有货、促销活动等信息，但因上述客观原因，本平台上前述商品信息随时都有可能发生变动，本平台不再另行通知用户；
  （2）如本平台发现用户订购的商品价格有误，将会尽快通知用户，用户有权选择重新确认或取消订单；24小时内如未能与该用户取得联系的，将按用户已取消该订单处理；如用户已经付款的，将按用户已取消该订单，全额退款处理，但用户同意给予本平台合理时间处理退款事宜。
  （3）如本平台发现用户已订购商品缺货的，将及时通知用户协商退换处理，24小时内协商不成或无法联系用户的，按订单取消，全额退款处理，但用户同意给予本平台合理时间处理退款事宜。
3.3为向用户提供公平的交易环境，用户理解与知悉：本平台保留对每位用户订购商品数量的限制权；这些限制可能针对来自同一帐号，同一收货地址，同一个手机号、同一IP地址的订单，如西伍认为用户有恶意订购商品的行为，本平台有权取消订单，无息退还用户的实付金额。
3.4用户在下订单时，有义务再次对所购商品的名称、价格、数量、型号、规格、尺寸、联系地址、电话、收货人等信息进行确认，并对前述信息的真实性负责；收货人与用户本人不一致的，收货人的行为和意思表示视为用户的行为和意思表示，用户应对收货人的行为及意思表示的法律后果承担连带责任。
3.5用户应当知道：用户在付款前有权修改或取消订单；用户付款后，即表示用户已经同意该商品描述所含的一切出售条件（只要物品出售条件不违反协议并合法），除该订单出现协议规定的除外情形外，订单无法修改或取消。如因用户提供错误或不完整的信息导致无法确认订单或无法配送或无正当理由拒绝完成在本平台的订单交易的，如无理拒收等，用户需承担因上述行为产生的额外或损失费用，并且本平台保留对用户当次及之后订单拒绝发货的权利。
   3.6用户通过本平台下达订单后，仅表示系统接收到了用户下单的订单，只有本平台将订单上的商品向用户或用户指定收货人发出时，双方之间的买卖合同才成立。而且，如果用户在一份订单里订购了多种商品但本平台只发出了部分商品时，用户与本平台或西伍之间仅仅就本平台或西伍已经发出的那一部分商品成立买卖合同。
四、配送
   4.1西伍将会把商品送到用户指定的收货地址，所有在本平台上列出的送货时间仅为参考时间，参考时间的计算是根据库存状况、正常的处理过程和送货时间、送货地点的基础上估计得出的，用户理解并允许出现误差。
   4.2因如下情况造成订单延迟或无法配送等，西伍不承担延迟配送的责任：
（1）用户提供的信息错误、地址不详细等原因导致的；
（2）货物送达后无人签收/拒绝签收，导致无法配送或延迟配送的；
（3）情势变更因素导致的；
（4）不可抗力因素导致的，例如：自然灾害、交通戒严、突发
战争、政府行为、疫情等。
五、收货
5.1用户订购的商品将被送至用户下单时提供的送货地址。无论什么原因商品不能送达到送货地址的，本平台将会尽快与用户取得联系。假如本平台工作人员自第一次试图与用户联系之日起7天内仍未取得用户答复的，本平台有权取消该订单。
5.2用户收到订购商品的同时，请认真检查送达商品并书面签收，
书面签收证明为对商品验收的有效凭证。
   5.3本平台商品支持7天（以用户/收货人签收送达凭证之日起计）无理由退换货，用户需承担非因商品质量问题退换货产生的费用；超过7天的非因商品质量问题提出的退换货申请，本平台有权拒绝。
   5.4商品质量问题引起的争议由用户和平台协商解决，协商不成的，商品质量问题以生效法律文书的认定为准。
六、用户依法使用平台义务
   6.1用户依法拥有言论自由，有权在本平台留言、评论、进行社区互动、参与平台活动等活动，用户同意依据国家相关法律法规规章制定，严格遵守以下义务：
  （1）不传输或发表：煽动抗拒、破坏宪法和法律、行政法规实施的言论，煽动颠覆国家政权，推翻社会主义制度的言论，煽动分裂国家、破坏国家统一的的言论，煽动民族仇恨、民族歧视、破坏民族团结的言论；
  （2）从中国大陆向境外传输资料信息时必须符合中国有关法律法规；
  （3）不利用本平台从事洗钱、窃取商业秘密、窃取个人信息等违法犯罪活动；
  （4）不干扰本平台的正常运转，不得侵入本平台及国家计算机信息系统；
  （5）不传输或发表任何违法犯罪的、骚扰性的、中伤他人的、辱骂性的、恐吓性的、伤害性的、庸俗的，淫秽的、不文明的等信息资料；
（6）不使用谩骂、侮辱、诽谤等不雅言语以及攻击其他用户或本平台；
  （7）不传输或发表损害国家社会公共利益和涉及国家安全的信息资料或言论；
（8） 不教唆他人从事本条所禁止的行为；
（9） 不利用在平台注册的账户进行牟利性经营活动；
(10)不发布任何侵犯他人著作权、商标权、专利权、商业秘密等知识产权或合法权利的内容；
(11)不利用平台漏洞谋取利益，发现漏洞应及时反馈给平台修
复；
(12)不利用虚假信息或其他手段恶意赚取平台积分、恶意变现
用户积分，不得利用积分进行营利性活动；
  （13）不歪曲或恶意利用平台的促销、推广等活动、规则获取不正当利益；
  （14）不采取其他任何不被平台认可或允许的行为获取不正当利益。
   6.2本平台保留对用户实施包括但不限于以下列举的恶意行为依法追究相应法律责任的权利，用户不能为了任何目的使用任何机器人、蜘蛛软件、刷屏软件或其他自动方式进入或攻击平台，进行以下操作：
（1） 进行对本平台造成或可能造成（按本平台自行酌情确定）不
合理或不合比例重大负荷的行为；
（2）未经本平台授权，对本平台的内容（除用户自身个人信息以
外）制作拷贝、进行复制、修改、制作衍生作品、分发或公开展示等的侵权行为；
（3）干扰或试图干扰本平台的正常工作或活动；
（4）在本平台中使用可能视为被禁止或可能被禁止的任何内容；
（5）对本平台使用不良数据，包含可能破坏、改变、删除、不利
影响、秘密截取、未经授权而接触，或征用任何系统、数据、个人资料的病毒、特洛依木马、蠕虫、定时炸弹、删除蝇、复活节彩蛋、间谍软件、其他电脑程序等。
6.3用户应不时关注并遵守本平台不时公布或修改的各类合法规则规定，并应对自己在网上的言论和行为承担法律责任。
6.4本平台保留删除平台内各类不符合法律法规规定或不真实的信息内容而无须通知用户的权利。
6.5若用户未遵守以上规定的，本平台有权依据独立判断对用户实施限制权限、暂停或关闭用户帐号等措施。
七、隐私政策
7.1用户注册时，本平台为帮助用户完成注册、登录、申诉以及订阅信息推送等，存在读取用户手机信息或第三方账号数据的可能，但请用户放心，用户提交的个人资料信息未经用户本人同意，本平台不会将用户个人信息向第三方泄露，法律有强制性规定除外；
7.2本平台会保留用户的浏览历史记录、交易记录、收货地址或发表过的言论等个人信息，以便能更好地为用户提供个性化的服务与优化会员权益；或需在用户手机上储存部分离线信息，提高访问的速度，优化用户的访问，提升用户体验等，前述相关信息未经用户同意，西伍不会将前述信息向第三方泄露，法律有强制性规定或司法机关依法调取除外；
7.3在不违反上述隐私政策的前提下，西伍及本平台有权对平台所载的用户数据进行抓取、检索、收集、整理、分析，并对整个用户数据库进行分析研究，且有权对分析、研究结果进行商业性的使用。
八、通知
8.1用户理解并同意，本平台无需通知用户即可自行决定以下事宜：
修改、暂停或停止本平台或任何和本平台相关的服务、功能
和所售商品；
（2）对本平台的个性化特殊模块的使用是否收费；
（3）调整或者免除和本平台相关的任何费用；
（4）对部分或全部用户提供机会、权利；
（5）修改、发布涉及平台与用户之间权利义务的协议。
8.2本平台不排除存在物品描述、价格、促销、可供性相关的误，
不准确或者遗漏的信息；用户同意随时给予本平台充足的时间机会纠正此类错误，并无需另行通知用户。
九、所有权及知识产权声明
9.1用户一旦接受本协议，即表明该用户主动将其在任何时间段在本平台发表的任何形式的信息内容（包括但不限于客户评价、客户咨询、各类话题文章等信息内容）的财产性权利等任何可转让的权利，如著作权财产权（包括但不限于：复制权、发行权、出租权、展览权、表演权、放映权、广播权、信息网络传播权、摄制权、改编权、翻译权、汇编权以及应当由著作权人享有的其他可转让权利），全部独家且不可撤销地转让给本平台及西伍所有，用户同意西伍有权就任何主体的侵权行为而单独提起诉讼。
9.2本平台所刊登的资料信息（包括但不限于编码、按钮图标、商标、服务标志、肖像、文字、图表、标识、图像、声音文件片段、视频、数码下载、数据编辑、计算机软件等），均是西伍或其内容提供者的财产，受国内法律和国际知识产权相关约定的保护。本平台或西伍有权对本平台上所有内容进行汇编并具有排他性的权利，汇编成果受国内法律和国际知识产权约定的保护。
9.3本平台所使用的包括但不限于体现西伍品牌、字号、企业名称、企业简介等企业或集团信息、资料的文案、编码、按钮图标、商标、服务标志、肖像、文字、图表、标识、按钮图标、图像、声音文件片段、视频、数码下载、数据编辑、计算机软件、设计、元素、用户信息以及平台数据库、数据分析结果等知识产权（包括但不限于商标、专利、著作权、包装装潢、字号）归佛山市西伍服饰有限公司所有，任何人未经授权不得以任何理由、任何形式出售或进行商业性使用，用户因私人原因下载、复制前述信息不构成西伍或本平台向该用户转让前述相关内容及权益。
9.4本协议已经构成《中华人民共和国著作权法》[中华人民共和国主席令（第二十六号）]及相关法律规定的著作权的财产权等可转让权利书面转让协议，其效力及于用户在平台上发布的任何受著作权法保护的作品内容，无论该等内容形成于本协议订立前还是本协议订立后。
9.5除法律另有强制性规定外，未经西伍书面许可,任何单位或个人不得以任何方式非法地全部或部分复制、转载、引用、链接、抓取或以其他方式使用本平台的信息内容，否则，西伍有权追究其法律责任。
9.6同时，本平台尊重他人的知识产权，如用户认为自身的知识产权受到侵犯，请及时联系本平台，并同意给予本平台合理时间采取措施。
十、服务终止
10.1本平台可随时根据实际情况中断一项或多项服务并不需对任何个人或第三方负责。用户若反对任何服务条款或对后来修改的条款有异议，或对本平台服务不满，可以行使如下权利：
申请注销用户账户，不再使用本平台服务。
书面通知本平台停止对其账户提供服务。
本平台收到用户送达的停止提供服务书面通知后，用户根据协议对本平台享有的服务立即中止。即时起，本平台也不再对该用户承担任何义务。
10.2出现以下行为的用户注册将被取消用户资格
违反本协议、规则或其他注册条款；
（2）提供虚假个人资料；
（3）恶意拍单申请退款，影响平台的正常运营；
散播恶意病毒，影响平台正常运行；
通过作弊或不正当手段牟利，邀请虚假用户，如非实名制手机用户、虚拟运营商手机号、空号等，恶意获取邀新积分；
利用虚假用户注册会员牟取利益，如拼图送衣等平台推出的一系列活动、游戏等；
利用平台漏洞恶意牟利，或散播煽动他人参与牟利型活动中去，造成平台损失；
邀请的一级部落成员手机号中，手机号为空号的成员数为5个或以上；
有违反中国的法律、法规、违反互联网相关规定的行为；
将注册账号当作商品进行交易的行为，如出售账户、拍卖;
不恰当行使用户权利的行为，如出借、转让注册账号给其他用户等；
通过盗取密码、诈骗等不正当手段获得用户账号的行为；
在留言、评论中多次或大量使用谩骂，侮辱，诽谤等不雅言语的行为；
采取任何不被平台认可或允许的行为获取不正当利益的行为；
10.3用户采取或实施了协议规定的其他应被取消用户资格的行为。如对平台作出的处罚有疑问，请用户在7天内提出申诉，超过申诉期后，默认用户接受本平台处罚。
十一、免责声明
11.1除另有明确的书面说明外,本平台及其所包含的或以其它方式通过本平台向用户提供的全部信息、内容、材料、产品（包括软件）和服务，均是在“按现状”和“按现有”的基础上提供的。
11.2除另有明确的书面说明外,西伍不对本平台的运营及其包含在本网站上的信息、内容、材料、产品（包括软件）或服务作任何形式的、明示或默示的声明或担保（法律另有强制性规定除外）。
11.3西伍不担保本平台所包含的或以其它方式通过平台向用户提供的全部信息、内容、材料、产品（包括软件）和服务、其服务器或从本站发出的电子信件、信息没有病毒或其他有害成分。
11.4西伍将尽力在技术上确保平台的正常运行，避免服务中断或将中断时间限制在最短时间内，保证用户网上交易活动的顺利进行。如因不可抗力或其他本平台无法控制的因素使平台崩溃或无法正常使用，导致网上交易无法完成或丢失相关信息、记录等，西伍及本平台不承担责任；任何用户向本平台提供错误、不完整、不实信息等造成不能正常使用本平台服务或遭受任何其他损失，概与本平台无关。
11.5本平台保留在中华人民共和国大陆地区法施行之法律允许的范围内独自决定拒绝服务、关闭用户账户、清除或编辑内容或取消订单的权利。
十二、其它
12.1本协议的订立、执行、解释和争议的解决，均应适用中华人民共和国法律。本平台及用户注册双方如对本协议在履行中发生争执，应尽力协商解决，协商不成的，交佛山仲裁委员会仲裁裁决。
12.2在法律允许的最大限度范围内，西伍对本协议及本平台内容拥有解释权。
12.3本协议条款具有可分性，部分无效不影响其它部分效力。

        </pre>
    </div>
@stop
