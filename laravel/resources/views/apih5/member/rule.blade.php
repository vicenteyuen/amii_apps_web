@extends('apih5.widget.base',['head_titile' => '会员规则'])

@section('content')
    <style type="text/css">
        .rule-cont{
            padding: 14px 15px;
        }
        .rule-cont h1{
            margin: 0 0 10px 0;
            font-size: 16px;
        }
        .rule-cont .mt{
            margin-top: 10px;
        }
        .rule-cont table{
            width: 100%;
            text-align: center;
            border-collapse:collapse;
            border: 1px solid rgb(230, 230, 230);
        }
        .rule-cont table td{
            border: 1px solid rgb(230, 230, 230);
            font-size: 9px;
            padding: 9px 2%;
            color: #333333;
        }
        .rule-cont table thead td{
            background-color: #e0af5b;
            font-size: 10px;
            color: #fff;
        }
        .rule-cont table td .img {
            display: inline-block;
            width: 15px;
            height: 15px!important;
            background: url('/assets/images/confirm.png') no-repeat center center;
            -webkit-background-size: contain;
            -moz-background-size: contain;
            -ms-background-size: contain;
            -o-background-size: contain;
            background-size: contain;
        }
        .notice{
            font-size: 12px;
            margin-top: 10px;
            color: #666;
        }
    </style>
    <div class="rule-cont">
        <h1>A.会员等级</h1>
        <table>
            <thead>
                <tr>
                    <td rowspan="2">会员等级</td>
                    <td rowspan="2">积分门槛</td>
                    <td colspan="3">条件(同时满足)</td>
                </tr>
                <tr>
                    <td>验证手机号</td>
                    <td>身份验证</td>
                    <td>购物金额＋部落佣金</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>V0</td>
                    <td>0</td>
                    <td>— —</td>
                    <td>— —</td>
                    <td>— —</td>
                </tr>
                <tr>
                    <td>V1</td>
                    <td>1000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>— —</td>
                </tr>
                <tr>
                    <td>V2</td>
                    <td>5000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>— —</td>
                </tr>
                <tr>
                    <td>V3</td>
                    <td>10000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>100元</td>
                </tr>
                <tr>
                    <td>V4</td>
                    <td>20000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>300元</td>
                </tr>
                <tr>
                    <td>V5</td>
                    <td>50000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>500元</td>
                </tr>
                <tr>
                    <td>V6</td>
                    <td>150000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>1000元</td>
                </tr>
                <tr>
                    <td>V7</td>
                    <td>400000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>2000元</td>
                </tr>
                <tr>
                    <td>V8</td>
                    <td>800000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>5000元</td>
                </tr>
                <tr>
                    <td>V9</td>
                    <td>1500000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>10000元</td>
                </tr>
                <tr>
                    <td>V10</td>
                    <td>3000000</td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>
                        <div class="img"></div>
                    </td>
                    <td>20000元</td>
                </tr>
            </tbody>
        </table>
        <div class="notice">PS：购物金额为实付金额，购物金额和部落佣金仅统计真实购物情况（不包含退换情况）</div>
        <h1 class="mt">B.会员特权</h1>
        <table>
            <thead>
                <td>会员等级</td>
                <td>会员专享价</td>
                <td>极速退款</td>
                <td>生日积分</td>
                <td>生日折扣卡<br/>1个月有效</td>
            </thead>
            <tbody>
                <tr>
                    <td>V0</td>
                    <td>— —</td>
                    <td>— —</td>
                    <td>— —</td>
                    <td>— —</td>
                </tr>
                <tr>
                    <td>V1</td>
                    <td>— —</td>
                    <td>— —</td>
                    <td>500</td>
                    <td rowspan="3">95折</td>
                </tr>
                <tr>
                    <td>V2</td>
                    <td>— —</td>
                    <td>— —</td>
                    <td rowspan="2">1000</td>
                </tr>
                <tr>
                    <td>V3</td>
                    <td>尊享99折</td>
                    <td>额度100元</td>
                </tr>
                <tr>
                    <td>V4</td>
                    <td rowspan="2">尊享98折</td>
                    <td>额度300元</td>
                    <td rowspan="3">2000</td>
                    <td rowspan="3">9折</td>
                </tr>
                <tr>
                    <td>V5</td>
                    <td>额度500元</td>
                </tr>
                <tr>
                    <td>V6</td>
                    <td>尊享97折</td>
                    <td>额度1000元</td>
                </tr>
                <tr>
                    <td>V7</td>
                    <td>尊享96折</td>
                    <td>额度2000元</td>
                    <td rowspan="2">3000</td>
                    <td rowspan="2">85折</td>
                </tr>
                <tr>
                    <td>V8</td>
                    <td>尊享95折</td>
                    <td>额度5000元</td>
                </tr>
                <tr>
                    <td>V9</td>
                    <td>尊享93折</td>
                    <td>额度10000元</td>
                    <td rowspan="2">5000</td>
                    <td rowspan="2">8折</td>
                </tr>
                <tr>
                    <td>V10</td>
                    <td>尊享9折</td>
                    <td>额度20000元</td>
                </tr>
            </tbody>
        </table>
         <br>
        <table>
            <thead>
            <td>会员等级</td>
            <td>尊享包邮卡<br>（年度次数）</td>
            <td>退货保障卡<br>（年度次数）</td>
            <td>积分加倍<br>（用于限时活动）</td>

            </thead>
            <tbody>
            <tr>
                <td>V0</td>
                <td>— —</td>
                <td>— —</td>
                <td>— —</td>
            </tr>
            <tr>
                <td>V1</td>
                <td>1</td>
                <td>1</td>
                <td>— —</td>
            </tr>
            <tr>
                <td>V2</td>
                <td>2</td>
                <td>2</td>
                <td rowspan="2">1.1倍</td>
            </tr>
            <tr>
                <td>V3</td>
                <td>3</td>
                <td>3</td>
            </tr>
            <tr>
                <td>V4</td>
                <td>4</td>
                <td>4</td>
                <td rowspan="2">1.2倍</td>
            </tr>
            <tr>
                <td>V5</td>
                <td>5</td>
                <td>5</td>
            </tr>
            <tr>
                <td>V6</td>
                <td>6</td>
                <td>6</td>
                <td>1.3倍</td>
            </tr>
            <tr>
                <td>V7</td>
                <td>8</td>
                <td>8</td>
                <td>1.4倍</td>
            </tr>
            <tr>
                <td>V8</td>
                <td>10</td>
                <td>10</td>
                <td>1.5倍</td>
            </tr>
            <tr>
                <td>V9</td>
                <td>20</td>
                <td>20</td>
                <td>1.6倍</td>
            </tr>
            <tr>
                <td>V10</td>
                <td>年度包邮</td>
                <td>无理由退货</td>
                <td>1.8倍</td>
            </tr>
            </tbody>
        </table>
    </div>
@stop
