@extends('apih5.widget.base', ['head_titile' => '首单7折活动'])

@section('content')
    <style type="text/css">
        .adv-index{
            padding: 20px 20px 20px 0;
            text-align: center;
        }
        .adv-index .gift{
            width: 111px;
            margin-bottom: 20px;
        }
        .adv-items{
            text-align: left;
            margin-bottom: 20px;
        }
        .adv-items .title{
            display: inline-block;
            position: relative;
            padding: 6px 30px 6px 20px;
            background: #d79523;
            color: #fff;
            font-size: 14px;
            margin-bottom: 15px;
        }
        .adv-items .title::before{
            content: '';
            display: inline-block;
            position: absolute;
            right: -15px;
            top: 0;
            width: 0;
            height: 0;
            border: 15px solid #d79523;
            border-top-color: transparent;
            border-right-color: transparent;
            border-left-color: transparent;
            -webkit-transform: rotate(-180deg);
            -moz-transform: rotate(-180deg);
            -ms-transform: rotate(-180deg);
            -o-transform: rotate(-180deg);
            transform: rotate(-180deg);
        }
        .adv-items .title::after{
            content: '';
            display: inline-block;
            position: absolute;
            right: -15px;
            bottom: 0;
            width: 0;
            height: 0;
            border: 15px solid #d79523;
            border-top-color: transparent;
            border-right-color: transparent;
            border-left-color: transparent;
        }
        .adv-items .content{
            padding: 0 0 0 20px;
            font-size: 13px;
            color: #000000;
        }
        .adv-items .content .text{
            position: relative;
            margin-bottom: 15px;
        }
        .adv-items .content .text span{
            display: block;
            padding: 0 0 0 8px;
            margin-left: 8px;
            line-height: 1.923;
            text-align: justify;
        }
        .adv-items .content a{
            color: #d79523;
        }
        .adv-items .content .text:before{
            content: '';
            display: inline-block;
            position: absolute;
            top: 10px;
            width: 4px;
            height: 4px;
            border-radius: 50%;
            background: #d79523;
            vertical-align: middle;
        }
        .adv-index img {
            margin-bottom: 14px;
            width: 100%;
        }
        .adv-notice{
            text-align: left;
            color: #959595;
            font-size: 11px;
            line-height: 1.636;
            padding-left: 20px;
        }
        .banner{
            padding: 0 0 0 20px;
        }
    </style>
    <div class="adv-index">
        <!-- <img src="assets/images/gift.png" class="gift"> -->
        <div class="adv-items">
            <div class="title"><span>活动</span></div>
            <div class="content">
                <div class="text"><span>新用户成功注册AMII账号之后，首次在APP端下单，即可享受首单7折的优惠。</span></div>
                <!-- <div class="text"><span>首单实付金额<a>满300</a>元，即<a>赠送价值299元的极简风格手表</a>，数量有限，先到先得。</span></div> -->
            </div>
        </div>
       <!--  <div class="banner">
            <img src="assets/images/watch.png">
        </div> -->
        <div class="adv-items">
            <div class="title"><span>规则</span></div>
            <div class="content">
                <div class="text"><span>用户通过各渠道（APP市场、邀请链接、二维码等）成功注册成为AMII会员，首次下单即可享受7折优惠，首单金额的30%将于用户确认收货后的第8天返还到余额账户。用户账号中的余额可累积，用于购买AMII产品，也可用于余额提现。</span></div>
                <div class="text"><span>该活动仅用于有效注册的新用户，一个有效手机号码仅能注册一个AMII账号。</span></div>
                <!-- <div class="text"><span>用户首单金额满300元，赠送礼品将与产品一起寄给用户。若用户需要换货，赠品可保留，仅需寄回产品即可；若用户需要退货，需赠品和产品一起寄回，才能完成退款。否则只能退还扣除赠品售价后的实付金额（退款金额=实付金额-299）。</span></div> -->
            </div>
        </div>
        <div class="adv-notice">
            *本活动仅限真实用户参与，如发现违规行为，本平台有权取消其活动资格，并保留追究相关法律责任的权利，最终解释权归本平台所有。
        </div>
    </div>
@stop
