<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta name="renderer" content="webkit">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="http://amii.lo">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/materialadmin1.min.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/nestable.css">
        <link type="text/css" rel="stylesheet" href="/assets/admin/css/material-design-iconic-font.min.css">
                <link type="text/css" rel="stylesheet" href="/assets/admin/css/admin.css">

        <!--[if lt IE 9]>
        <script type="text/javascript" src="assets/js/html5shiv.min.js"></script>
        <script type="text/javascript" src="assets/js/respond.min.js"></script>
        <![endif]-->
            </head>
    <body class="menubar-hoverable header-fixed menubar-pin">


<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">上传图片测试</li>
        </ol>
    </div>
    <div class="card card-underline">
        <div class="card-head">
            <header></header>

        </div><!--end .card-head -->

        <div class="card-body">
            <form id="formImage" enctype="multipart/form-data" method="POST"
                action="{{ action('Test\TestGimageController@store') }}">
                <input type="file" name="file" />
                <button type="submit" class="btn ink-reaction btn-primary btn-md">提交</button>
            </form>
        </div><!-- ./card-body -->
    </div><!-- /.card -->
</section>


    </body>
</html>


