<?php
/**
 * Created by PhpStorm.
 * User: stevie
 * Date: 5/24/16
 * Time: 16:07
 */

return [
    'check' => [
        'wait' => '待审核',
        'failure' => '审核失败',
    ],
    'transaction' => [
        'no_contact' => '未联系',
        'processing' => '处理中',
        'closed' => '关闭',
        'completed' => '完成'
    ],
    'role' => [
        'Headquartered' => '总部',
        'Store' => '分店',
        'Consumer' => '消费者',
        'Shipping' => '跑男',
    ],
    'product' => [
        1 => '待审核',
        2 => '通过',
        3 => '失败',
    ],
    'product_day_type' => [
        'today' => '当日达',
        'tomorrow' => '次日达',
        'express' => '快递'
    ]
];
