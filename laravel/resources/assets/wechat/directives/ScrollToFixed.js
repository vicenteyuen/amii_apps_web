/**
 * Created by stevie on 2017/1/20.
 * 参考 https://github.com/bigspotteddog/ScrollToFixed
 */
export default {
    bind(el) {
        let fixed_tool = null;
        let original_offsetTop = null;
        let placeholder_el = null;
        if(navigator.userAgent.indexOf('Android') >= 0){
            window.onscroll = ()=>{
                if(!fixed_tool){
                    fixed_tool = el;
                    fixed_tool.insertAdjacentHTML('afterend', '<div style="width: 100%;height: ' + fixed_tool.offsetHeight + 'px; display: none"></div>');
                    placeholder_el = fixed_tool.nextElementSibling;
                    original_offsetTop = fixed_tool.offsetTop
                }
                if(fixed_tool.offsetTop > original_offsetTop){
                    original_offsetTop = fixed_tool.offsetTop
                }
                let top = document.documentElement.scrollTop || document.body.scrollTop;
                let has_style = fixed_tool.getAttribute('style');
                if (original_offsetTop < top && !has_style) {
                    fixed_tool.setAttribute('style', 'position:fixed;top:0;left:0;width:100%;z-index:1;');
                    placeholder_el.style.display = 'block'
                } else if (original_offsetTop > top && has_style) {
                    fixed_tool.removeAttribute('style');
                    placeholder_el.style.display = 'none'
                }
            };
        }else{
            el.setAttribute('style', 'position:-webkit-sticky;position:sticky;top:0;left:0;width:100%;z-index:1;')
        }
    },
    unbind(){
        window.onscroll = null;
    }
}
