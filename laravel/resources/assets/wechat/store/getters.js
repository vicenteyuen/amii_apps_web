/**
 * http://vuex.vuejs.org/zh-cn/getters.html
 * 有时候我们需要从 store 中的 state 中派生出一些状态，例如对列表进行过滤并计数：
 */
export default {
    positioncity(state){
        if(!state.position_data || !state.position_data.addressComponents) return null;
        return state.position_data.addressComponents.city || null
    },
    positionlocation(state){
        if(!state.position_data) return null;
        return state.position_data.location || null
    },
}
