export default {
    temp: {},
    user_info: {},
    search_history: [],
    token: null,
    shopping_cart_count: 0,
    position_data: null,
    one_display: {
        advertisement: false,
        download_prompt: false,
        advertisement_notice: false
    },
    game: {
        userId: []
    },
    prevLink: null,
    award_point: 0,
    is_app: navigator.userAgent.toLowerCase().match(/amii\/app/i) == 'amii/app',
    is_wechat: navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == 'micromessenger' && typeof wx != 'undefined',
    is_safari: navigator.userAgent.toLowerCase().match(/safari/i) == 'safari'
}
