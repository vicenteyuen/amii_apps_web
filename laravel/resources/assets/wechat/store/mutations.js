/**
 * http://vuex.vuejs.org/zh-cn/mutations.html
 * 更改 Vuex 的 store 中的状态的唯一方法是提交 mutation。
 * Vuex 中的 mutations 非常类似于事件：每个 mutation 都有一个字符串的 事件类型 (type) 和 一个 回调函数 (handler)。
 * 这个回调函数就是我们实际进行状态更改的地方，并且它会接受 state 作为第一个参数：
 */
export default {
    updateToken(state, payload){
        if (!payload) {
            payload = null;
            state.user = {};
        }
        state.token = payload;

        const key = 'token';
        if (payload) {
            sessionStorage.setItem(key, payload)
        } else {
            sessionStorage.removeItem(key);
        }
    },
    commitTemp(state, payload){
        if (!payload) {
            payload = {};
        }
        state.temp = payload;
    },
    cacheGame(state, payload){
        if (!payload) {
            payload = {};
        }
        state.game = payload;
    },
    cacheGameUserId(state, payload){
        state.game.userId.push(payload);
    },
    cacheUserInfo(state, payload){
        if (!payload) {
            payload = {};
        }
        state.user_info = payload;
    },
    updatePositionData(state, payload = null){
        state.position_data = payload;
    },
    setOneDisplay(state, payload){
        state.one_display[payload] = true;
    },
    addOneDisplay(state, payload){
        state.one_display[payload] = false;
    },
    addPrevLink(state, payload){
        state.prevLink = payload;
    },
    /**
     * 获取连续登陆积分提示
     * @param state
     * @param payload
     */
    fetchLoginPointDialog(state, payload){
        if (state.award_point && payload) return;
        state.award_point = payload;
    },
    addSearchKeyword(state, payload){
        if (!payload) {
            return;
        }
        for (let index in state.search_history) {
            if (state.search_history[index] == payload) {
                state.search_history.splice(index, 1);
            }
        }
        state.search_history.splice(0, 0, payload);
        if (state.search_history.length > 10) {
            state.search_history.splice(state.search_history.length - 1, 1);
        }
    }
}
