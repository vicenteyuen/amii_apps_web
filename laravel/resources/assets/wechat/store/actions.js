/**
 * http://vuex.vuejs.org/zh-cn/actions.html
 * Action 提交的是 mutation，而不是直接变更状态。
 * Action 可以包含任意异步操作。
 */
export default {
    login({commit}, token){
        commit('updateToken', token);
    },
    addToCart({dispatch}, payload){
        RequestAPI.http.post('cart', payload).then((r) => {
            payload.id = r.id;
            dispatch('getShoppingCartProducts');
            Mint.Toast('添加成功，在购物车等亲');
        });
    },
    getShoppingCartProducts({state, commit}){
        RequestAPI.http.get('helper/notify/number').then((r) => {
            if (!state.token && parseInt(r.is_login)) {
                commit('updateToken', true);
            }else if(!parseInt(r.is_login)){
                commit('updateToken', null);
            }
            state.shopping_cart_count = Number(r.cart)
        })
    },
    /**
     * payload -> {inventory_id:1, number: 1}
     * @param commit
     * @param payload
     */
    nowCheckout({commit}, payload){
        RequestAPI.http.post('order/buy', payload).then((r) => {
            Vue.$router.push({
                name: 'Mall.SubmitOrder',
                params: {id: r.orderSn}
            })
        })
    },
    getMyData({commit}){
        RequestAPI.http.get('user').then((r) => {
            commit('cacheUserInfo', r);
        })
    }
}
