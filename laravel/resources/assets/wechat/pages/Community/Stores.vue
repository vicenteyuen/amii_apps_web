<template>
    <div class="community-stores">
        <div class="main"
             v-infinite-scroll="getData"
             infinite-scroll-disabled="loading"
             infinite-scroll-distance="10">
            <div class="current-location">
                <span @click="openCityPicker">当前地区: {{ form.cityName || '全国' }} <span class="iconfont icon-down"></span></span>
                <div class="right" @click="onClickMap">
                    <span> 地图模式</span>
                    <img src="~/assets/images/location-ico.png"/>
                </div>
            </div>
            <div class="store" v-for="i in list">
                <div class="store-image">
                    <img v-lazy="i.image">
                </div>
                <div class="store-attr">
                    <div class="store-name">{{ i.name }} <span class="right" v-if="i.distance">{{ i.distance | distanceFormat }}</span>
                    </div>
                    <div class="store-contact">
                        {{ i.address }}{{ i.detail_address }} <br>
                        {{ i.phone }}
                        <div class="iconfont icon-daohang right" @click="onClickMap(i)"></div>
                    </div>
                </div>
            </div>
            <empty-view type="15" content="当前城市暂无实体店" v-if="(!list || !list.length) && this.form.cityName"></empty-view>
            <empty-view type="16" :moreContent="['为搜索您附近的店铺', '请允许AMII使用您的位置功能']"
                        v-if="!this.form.cityName && !list.length"></empty-view>
        </div>
        <store-map @close="store_map.show=false" v-if="store_map.show" :list="list" :position="store_map.position" :center="store_map.center"></store-map>
        <div class="city-picker address-popup-bottom city-picker" :class="!city_picker.show||'addressShow'">
            <div class="address-handle right-align">
                <div class="address-handle-close" @click="closeCityPicker"><i class="iconfont icon-close"></i></div>
            </div>
            <div class="address-content">
                <picker :slots="city_picker.slots" @change="changeCityPicker"></picker>
            </div>
        </div>
        <fixed-bottom-bar></fixed-bottom-bar>
    </div>
</template>

<script>
    import FixedBottomBar from '/components/FixedBottomBar.vue'
    import StoreMap from '/modules/StoreMap.vue'
    import EmptyView from '/modules/EmptyView.vue'
    import {Picker, InfiniteScroll} from 'mint-ui';

    export default{
        components: {
            FixedBottomBar,
            StoreMap,
            EmptyView,
            Picker
        },
        directive: {
            InfiniteScroll
        },
        filters: {
            distanceFormat(distance){
                if (!distance) return '';
                if (distance < 1000) {
                    return distance + "m";
                } else if (distance > 1000) {
                    return (distance / 1000) + "km"
                }
            }
        },
        data(){
            return {
                list: [],
                form: {
                    page: 0,
                    cityName: null,
                    longitude: null,
                    latitude: null,
                    is_paginate: 1
                },
                last_page: false,
                loading: false,

                store_map: {
                    show: false,
                    position: null,
                    center: null,
                },

                city_picker: {
                    area: '',
                    list: [],
                    show: false,
                    slots: [
                        {
                            flex: 1,
                            values: [],
                            textAlign: 'center'
                        }, {
                            flex: 1,
                            values: [],
                            textAlign: 'center'
                        }, {
                            flex: 1,
                            values: [],
                            textAlign: 'center'
                        },
                    ],
                },
            }
        },
        watch: {
//            '$store.getters.positioncity'(val){
//                this.form.cityName = val;
//            },
//            '$store.getters.positionlocation'(val){
//                this.form.longitude = val.lng;
//                this.form.latitude = val.lat;
//            },
            'form.cityName'(val){
                this.getData(true);
            },
//            '$store.state.position_data'(val){
//                if (!val) return;
//                this.store_map.position = val.location;
//                this.form.cityName = val.addressComponents.city;
//            }
        },
        created(){
//            if(this.$store.getters.positioncity){
//                this.form.cityName = this.$store.getters.positioncity;
//            }
//            let positionlocation = this.$store.getters.positionlocation;
//            if(positionlocation && positionlocation.lng && positionlocation.lat){
//                this.form.longitude = positionlocation.lng;
//                this.form.latitude = positionlocation.lat;
//                this.store_map.position = positionlocation;
//            }
        },
        mounted(){
            this.fetchCityPickerData();
            if (!this.$store.state.position_data) this.navigatorGeoLocation();
        },
        methods: {
            getData(is_init_page){
                if (this.loading || (!is_init_page && this.last_page)) return;

                this.loading = true;

                if (is_init_page || !this.form.page) {
                    this.form.page = 0;
                    this.last_page = false;
                    window.scrollTo(0, 0);
                }
                this.form.page++;

                let form = this.form;
                if (form.cityName == '全国') form.cityName = null;

                Mint.Indicator.open();
                RequestAPI.http.get('physical-store', {params: form})
                    .then((response) => {
                        let list = response.paginate_data.stores;
                        if (this.form.page <= 1) {
                            this.list = list;
                        } else {
                            for (let i of list) {
                                this.list.push(i)
                            }
                        }

                        if (response.pagination.current_page === response.pagination.last_page || !parseInt(response.pagination.last_page)) {
                            this.last_page = true;
                        }
                    })
                    .finally(() => {
                        Mint.Indicator.close();
                        this.$nextTick(()=>{
                            this.loading = false;
                        })
                    })
            },
            fetchCityPickerData(){
                RequestAPI.http.get('address/info/list')
                    .then((response) => {
                        this.city_picker.list = response;
                        let nationwide = {
                            id: 1,
                            lat: '',
                            lng: '',
                            name: '全国',
                            province_id: 0,
                            city: [],
                        };
                        this.city_picker.list.unshift(nationwide);
                        for (let i of this.city_picker.list) {
                            this.city_picker.slots[0].values.push(i.name);
                        }
                    })
                    .finally(() => {
                        Mint.Indicator.close();
                    })
            },
            onClickMap(item = null){
                if(item.latitude && item.longitude){
                    this.store_map.center = {
                        lat: item.latitude,
                        lng: item.longitude,
                    };
                }else{
                    this.store_map.center = null;
                }

                this.store_map.show = true;
            },
            openCityPicker(){
                if (!this.city_picker.list.length) return;
                this.city_picker.show = true;
            },
            closeCityPicker(){
                this.city_picker.show = false;
                this.form.cityName = this.city_picker.area;
            },
            changeCityPicker(picker, values) {
                let citys = [], areas = [];
                for (let i of this.city_picker.list) {
                    if (i.name == values[0]) {
                        if (!i.city.length) {
                            picker.setSlotValues(1, []);
                            picker.setSlotValues(2, []);
                            this.form.region_id = i.province_id;
                            continue;
                        }
                        for (let ii of i.city) {
                            citys.push(ii.name);
                            if (ii.name == values[1]) {
                                if (!ii.area.length) {
                                    picker.setSlotValues(2, []);
                                    this.form.region_id = ii.city_id;
                                    continue;
                                }
                                for (let iii of ii.area) {
                                    areas.push(iii.name);
                                    if (iii.name == values[2]) {
                                        this.form.region_id = iii.area_id;
                                    }
                                }
                                picker.setSlotValues(2, areas);
                            }
                        }
                        picker.setSlotValues(1, citys);
                    }
                }
                if (values.length == 3 && values[2]) {
                    this.city_picker.area = values[1];
                } else {
                    this.city_picker.area = values[0];
                }
            },
            navigatorGeoLocation(){
                if (!navigator.geolocation) return;
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        if (typeof qq == 'undefined')return;
                        let latlng = new qq.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        qq.maps.convertor.translate(latlng, 1, (res) => {
                            this.handlePosition(res[0]);
                        });
                    },
                    (err) => {
                        if (typeof qq == 'undefined')return;
                        new qq.maps.CityService({
                            complete: (result) => {
                                this.handlePosition(result.detail.latLng);
                            }
                        }).searchLocalCity();

                    });
            },
            handlePosition(position){
                new qq.maps.Geocoder({
                    complete: (result) => {
                        delete result.detail.nearPois;
                        this.$store.commit('updatePositionData', result.detail);
                    }
                }).getAddress(new qq.maps.LatLng(position.lat, position.lng));
            }
        }
    }
</script>

<style lang="scss">
    .community-stores {
        .current-location {
            background: #fff;
            height: .77rem;
            line-height: .77rem;
            padding: 0 .2rem;

            img {
                width: .4rem;
            }
        }

        .store {
            margin: .2rem;
            background: #fff;
            overflow: hidden;
            border-radius: .1rem;

            &-attr {
                padding: .1rem;
            }

            &-name {
                font-size: .3rem;
                font-weight: bold;
                margin-bottom: .1rem;

                .right {
                    line-height: 1.5;
                    color: #636363;
                    font-size: .26rem;
                }
            }

            &-contact {
                line-height: 1.5;
                color: #636363;
                font-size: .26rem;
            }
            img {
                width: 100%;
            }
        }
        .addressArea {
            width: 100%;
        }
        .city-picker {
            position: fixed;
            background: #fff;
            width: 100%;
            top: 50%;
            left: 0;
            transform: translate3d(-50%, 100%, 0);
            backface-visibility: hidden;
            transition: .2s ease-out;
            z-index: 9999;

            &.address-popup-bottom {
                top: auto;
                right: auto;
                bottom: 0;
                left: 0;
                transform: translate3d(0%, 100%, 0);
            }
            .address-handle {
                border-bottom: solid 1px #eaeaea;
                color: #26a2ff;
                line-height: 40px;
                font-size: 16px;
                .address-handle-close {
                    display: inline-block;
                    padding: 0 0.22rem;
                }
            }
            .address-content {
                font-size: 24px;
            }
            &.addressShow {
                transform: translate3d(0, 0, 0);
            }
        }
    }
</style>
