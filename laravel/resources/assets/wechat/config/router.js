const lazyLoading = (name, index = false) => () => System.import(`../pages/${name}${index ? (name?'/':'')+'index' : ''}.vue`);
// const Loading = (name, index = false) => require(`../pages/${name}${index ? (name?'/':'')+'index' : ''}.vue`);

import DefaultIndex from '../pages/index.vue'

export default {
    base: '/wechat/',
    mode: 'history', //history,hash
    linkActiveClass: 'router-link-active',
    routes: [
        {
            path: '/mall',
            component: DefaultIndex,
            redirect: {
                name: 'Mall.Home',
            },
            children: [
                {
                    name: 'Mall.Home',
                    path: '/',
                    component: lazyLoading('Mall/Home'),
                },
                {
                    name: 'Mall.Advertisement',
                    path: 'advertisement',
                    component: lazyLoading('Mall/Advertisement'),
                    meta: {
                        title: '首单7折活动'
                    }
                },
                {
                    name: 'Mall.Sales',
                    path: 'sales',
                    component: lazyLoading('Mall/Sales'),
                    meta: {
                        title: '分佣规则'
                    }
                },
                {
                    name: 'Mall.SalesHome',
                    path: 'sales-home',
                    component: lazyLoading('Mall/SalesHome'),
                    meta: {
                        title: '全民推荐，佣金轻松享'
                    }
                },
                {
                    name: 'Mall.ProductsList',
                    path: 'products-list',
                    component: lazyLoading('Mall/ProductsList'),
                },
                {
                    name: 'Mall.ProductDetail',
                    path: 'product/:id',
                    component: lazyLoading('Mall/Product'),
                    meta: {
                        title: '商品详情'
                    }
                },
                {
                    name: 'Mall.ShoppingCart',
                    path: 'shopping-cart',
                    component: lazyLoading('Mall/ShoppingCart'),
                    meta: {
                        title: '购物车'
                    },
                },
                {
                    name: 'Mall.SubmitOrder',
                    path: 'submit-order/:id',
                    component: lazyLoading('Mall/SubmitOrder'),
                    meta: {
                        title: '提交订单'
                    }
                },
                {
                    name: 'Mall.WechatShare',
                    path: 'wechat-share',
                    component: lazyLoading('Mall/WechatShare'),
                }
            ]
        },
        {
            name: 'Community.index',
            path: '/community',
            component: lazyLoading('Community', true),
            redirect: {
                name: 'Community.Games',
            },

            children: [
                {
                    name: 'Community.Topics',
                    path: 'topics',
                    component: lazyLoading('Community/Topics'),
                    meta: {
                        title: '专题'
                    },
                },
                {
                    name: 'Community.Topic',
                    path: 'topic/:id',
                    component: lazyLoading('Community/Topic'),
                    meta: {
                        title: '专题详情'
                    }
                },
                {
                    name: 'Community.Videos',
                    path: 'videos',
                    component: lazyLoading('Community/Videos'),
                    meta: {
                        title: '视频'
                    },
                },
                {
                    name: 'Community.VideoList',
                    path: 'video-list/:type',
                    component: lazyLoading('Community/VideoList'),
                    meta: {
                        title: '最新视频'
                    }
                },
                {
                    name: 'Community.Video',
                    path: 'video/:id',
                    component: lazyLoading('Community/Video'),
                    meta: {
                        title: '视频'
                    },
                },
                {
                    name: 'Community.Stores',
                    path: 'stores',
                    component: lazyLoading('Community/Stores'),
                    meta: {
                        title: '实体店'
                    },
                },
                {
                    name: 'Community.Games',
                    path: 'games',
                    component: lazyLoading('Community/Games'),
                    meta: {
                        title: '活动'
                    }
                },
            ]
        },
        {
            path: '/game',
            component: lazyLoading('Game', true),
            redirect: {
                name: 'Game.Home',
            },
            children: [
                {
                    name: 'Game.Home',
                    path: '/',
                    component: lazyLoading('Game/Home'),
                    meta: {
                        title: '拼图送衣'
                    }
                },
                {
                    name: 'Game.Rule',
                    path: 'rule',
                    component: lazyLoading('Game/Rule'),
                    meta: {
                        title: '游戏规则'
                    }
                },
                {
                    name: 'Game.Register',
                    path: 'register',
                    component: lazyLoading('Game/Register'),
                    meta: {
                        title: '注册或登录'
                    }
                },
                {
                    name: 'Game.Product',
                    path: 'product/:id',
                    component: lazyLoading('Game/Product'),
                    meta: {
                        title: '游戏商品详情'
                    }
                },
                {
                    name: 'Game.Puzzle',
                    path: 'puzzle',
                    component: lazyLoading('Game/Puzzle'),
                    meta: {
                        title: '我的拼图-进行中'
                    }
                },
                {
                    name: 'Game.Heartbeat',
                    path: 'heartbeat',
                    component: lazyLoading('Game/Heartbeat'),
                    meta: {
                        title: '我的拼图-心动的'
                    }
                },
                {
                    name: 'Game.Order',
                    path: 'order',
                    component: lazyLoading('Game/Order'),
                    meta: {
                        title: '我的拼图-已到手'
                    }
                },
                {
                    name: 'Game.Invalid',
                    path: 'invalid',
                    component: lazyLoading('Game/Invalid'),
                    meta: {
                        title: '我的拼图-已失效'
                    }
                },
                {
                    name: 'Game.Show',
                    path: 'show/:id',
                    component: lazyLoading('Game/Show'),
                    meta: {
                        title: '拼图详情'
                    }
                },
                {
                    name: 'Game.Submit',
                    path: 'submit/:id',
                    component: lazyLoading('Game/Submit'),
                    meta: {
                        title: '提交成功'
                    }
                },
                {
                    name: 'Game.SubmitOrder',
                    path: 'submit-order/:id',
                    component: lazyLoading('Game/SubmitOrder'),
                    meta: {
                        title: '提交订单'
                    }
                },
            ]
        },
        {
            path: '/points',
            component: DefaultIndex,
            redirect: {
                name: 'Points.Home',
            },
            children: [
                {
                    name: 'Points.Home',
                    path: '/',
                    component: lazyLoading('Points/Home'),
                    meta: {
                        title: '积分商城'
                    }
                },
                {
                    name: 'Points.Task',
                    path: 'task',
                    component: lazyLoading('Points/Task'),
                    meta: {
                        title: '今日任务'
                    }
                },
                {
                    name: 'Points.Award',
                    path: 'award',
                    component: lazyLoading('Points/Award'),
                    meta: {
                        title: '获奖名单'
                    }
                },
                {
                    name: 'Points.AwardRank',
                    path: 'award-rank',
                    component: lazyLoading('Points/AwardRank'),
                    meta: {
                        title: '获奖排行'
                    }
                },
                {
                    name: 'Points.Rank',
                    path: 'rank',
                    component: lazyLoading('Points/Rank'),
                    meta: {
                        title: '排行榜'
                    }
                },
                {
                    name: 'Points.Recording',
                    path: 'recording',
                    component: lazyLoading('Points/Recording'),
                    meta: {
                        title: '积分明细'
                    }
                },
                {
                    name: 'Points.Rule',
                    path: 'rule',
                    component: lazyLoading('Points/Rule'),
                    meta: {
                        title: '积分规则'
                    }
                },
                {
                    name: 'Points.Product',
                    path: 'product/:id',
                    component: lazyLoading('Points/Product'),
                    meta: {
                        title: '商品详情'
                    }
                },
                {
                    name: 'Points.SubmitOrder',
                    path: 'submit-order/:id',
                    component: lazyLoading('Points/SubmitOrder'),
                    meta: {
                        title: '提交订单'
                    }
                },
            ]
        },
        {
            path: '/my',
            component: DefaultIndex,
            redirect: {
                name: 'My.Home',
            },
            meta: {
            },
            children: [
                {
                    name: 'My.Home',
                    path: '/',
                    component: lazyLoading('My/Home'),
                    meta: {
                        title: '我的'
                    }
                },
                {
                    name: 'My.Collect',
                    path: 'collect',
                    component: lazyLoading('My/Collect'),
                    meta: {
                        title: '我的收藏'
                    }
                },
                {
                    name: 'My.Money',
                    path: 'money',
                    component: lazyLoading('My/Money'),
                    meta: {
                        title: '邀请好友，礼券佣金立即享'
                    }
                },
                {
                    name: 'My.Share',
                    path: 'share',
                    component: lazyLoading('My/Share'),
                    meta: {
                        title: '领取新人专享券'
                    }
                },
                {
                    name: 'My.ShareSuccess',
                    path: 'share-success',
                    component: lazyLoading('My/ShareSuccess'),
                    meta: {
                        title: '领取新人专享券'
                    }
                },

                {
                    name: 'My.Footprint',
                    path: 'footprint',
                    component: lazyLoading('My/Footprint'),
                    meta: {
                        title: '浏览足迹'
                    }
                },
                {
                    name: 'My.Member',
                    path: 'member',
                    component: lazyLoading('My/Member'),
                    meta: {
                        title: '会员中心'
                    }
                },
                {
                    name: 'My.Qrcode',
                    path: 'qrcode',
                    component: lazyLoading('My/Qrcode'),
                    meta: {
                        title: '我的二维码'
                    }
                },
                 {
                    name: 'My.Propert',
                    path: 'property',
                    component: lazyLoading('My/Property'),
                    meta: {
                        title: '资产管理'
                    }
                },
                 {
                    name: 'My.Coupons',
                    path: 'coupons',
                    component: lazyLoading('My/Coupons'),
                    meta: {
                        title: '优惠券'
                    }
                },
                {
                    name: 'My.Withdraw.index',
                    path: 'withdraw',
                    component: DefaultIndex,
                    redirect: {
                        name: 'My.Withdraw.Home',
                    },
                    children: [
                        {
                            name: 'My.Withdraw.Home',
                            path: 'home',
                            component: lazyLoading('My/Withdraw/Home'),
                            meta: {
                                title: '余额'
                            }
                        },
                        {
                            name: 'My.Withdraw.Withdraw',
                            path: 'withdraw',
                            component: lazyLoading('My/Withdraw/Withdraw'),
                            meta: {
                                title: '提现'
                            }
                        },
                        {
                            name: 'My.Withdraw.WithdrawChannel',
                            path: 'withdrawchannel',
                            component: lazyLoading('My/Withdraw/WithdrawChannel'),
                            meta: {
                                title: '提现账号'
                            }
                        },
                         {
                            name: 'My.Withdraw.AlipayBinding',
                            path: 'alipaybinding',
                            component: lazyLoading('My/Withdraw/AlipayBinding'),
                            meta: {
                                title: '绑定支付宝账号'
                            }
                        },
                    ]
                },
                {
                    name: 'My.Tribe',
                    path: 'tribe',
                    component: lazyLoading('My/Tribe'),
                    meta: {
                        title: '部落'
                    }
                },
                {
                    name: 'My.Income',
                    path: 'income',
                    component: lazyLoading('My/Income'),
                    meta: {
                        title: '收益明细'
                    }
                },
                {
                    name: 'My.TribeDetail',
                    path: 'tribe-detail',
                    component: lazyLoading('My/TribeDetail'),
                    meta: {
                        title: '佣金排行榜'
                    }
                },
                {
                    path: 'messages',
                    component: DefaultIndex,
                    redirect: {
                        name: 'My.Messages',
                    },
                    children: [
                        {
                            name: 'My.Messages',
                            path: '/',
                            component: lazyLoading('My/Messages/Home'),
                            meta: {
                                title: '消息中心'
                            }

                        },
                        {
                            name: 'My.Messages.DeliveryList',
                            path: 'deliverylist',
                            component: lazyLoading('My/Messages/DeliveryList'),
                            meta: {
                                title: '物流信息'
                            }
                        },
                        {
                            name: 'My.Messages.NotificationList',
                            path: 'notificationlist',
                            component: lazyLoading('My/Messages/NotificationList'),
                            meta: {
                                title: '通知信息'
                            }
                        },
                        {
                            name: 'My.Messages.ActivityList',
                            path: 'activitylist',
                            component: lazyLoading('My/Messages/ActivityList'),
                            meta: {
                                title: '活动信息'
                            }
                        },
                    ]
                },
                {
                    name: 'My.Orders.Index',
                    path: 'orders',
                    component: DefaultIndex,
                    redirect: {
                        name: 'My.Orders',
                    },
                    children: [
                        {
                            name: 'My.Orders',
                            path: ':status(\\w+)',
                            component: lazyLoading('My/Orders/Home'),
                            meta: {
                                title: '我的订单'
                            }
                        },
                        {
                            name: 'My.Orders.Show',
                            path: ':id(\\d+)',
                            component: lazyLoading('My/Orders/Show'),
                            meta: {
                                title: '订单详情'
                            }
                        },
                        {
                            name: 'My.Orders.Express',
                            path: 'express/:id',
                            component: lazyLoading('My/Orders/Express'),
                            meta: {
                                title: '物流详情'
                            }
                        },
                        {
                            name: 'My.Orders.Comment',
                            path: 'comment/:order',
                            component: lazyLoading('My/Orders/Comment'),
                            meta: {
                                title: '评价'
                            }
                        },
                        {
                            name: 'My.Orders.Aftermarket',
                            path: 'aftermarket/:order/:type',
                            component: lazyLoading('My/Orders/Aftermarket'),
                            meta: {
                                title: '申请售后'
                            }
                        },
                        {
                            name: 'My.Orders.AftermarketDetail',
                            path: 'aftermarketDetail/:order',
                            component: lazyLoading('My/Orders/AftermarketDetail'),
                            meta: {
                                title: '售后'
                            }
                        },
                        {
                            name: 'My.Orders.ExpressInfo',
                            path: 'expressInfo/:order',
                            component: lazyLoading('My/Orders/ExpressInfo'),
                            meta: {
                                title: '退货物流'
                            }
                        },
                        {
                            name: 'My.Orders.AftermarketList',
                            path: 'aftermarketList/:status',
                            component: lazyLoading('My/Orders/AftermarketList'),
                            meta: {
                                title: '售后订单'
                            }
                        },
                    ]
                },
            ]
        },
        {
            path: '/settings',
            component: lazyLoading('Settings', true),
            redirect: {
                name: 'Settings.Home',
            },
            meta: {
            },
            children: [
                {
                    name: 'Settings.Home',
                    path: '/',
                    component: lazyLoading('Settings/Home'),
                    meta: {
                        title: '设置'
                    }
                },
                {
                    name: 'Settings.InfoEdit',
                    path: 'infoEdit',
                    component: lazyLoading('Settings/InfoEdit'),
                    meta: {
                        title: '个人信息'
                    }
                },
                {
                    name: 'Settings.ModifyNickname',
                    path: 'modifyNickname',
                    component: lazyLoading('Settings/ModifyNickname'),
                    meta: {
                        title: '昵称'
                    }
                },
                {
                    name: 'Settings.ModifyPhone',
                    path: 'modifyPhone/:form',
                    component: lazyLoading('Settings/ModifyPhone'),
                    meta: {
                        title: '绑定新手机'
                    },
                },
                {
                    name: 'Settings.VerifyPhone',
                    path: 'verifyPhone',
                    component: lazyLoading('Settings/VerifyPhone'),
                    meta: {
                        title: '验证手机'
                    },
                },
                {
                    name: 'Settings.BindPhone',
                    path: 'bind-phone',
                    component: lazyLoading('Settings/BindPhone'),
                    meta: {
                        title: '绑定手机'
                    }
                },
                {
                    path: 'address',
                    component: lazyLoading('Settings', true),
                    redirect: {
                        name: 'Settings.Address',
                    },
                    children: [
                        {
                            name: 'Settings.Address',
                            path: '/',
                            component: lazyLoading('Settings/Address'),
                            meta: {
                                title: '地址管理'
                            }
                        },
                        {
                            name: 'Settings.AddressEdit',
                            path: 'edit/:addressId?',
                            component: lazyLoading('Settings/AddressEdit'),
                            meta: {
                                title: '编辑地址'
                            }
                        },
                    ]
                },
                {
                    path: 'figure',
                    component: lazyLoading('Settings', true),
                    redirect: {
                        name: 'Settings.Figure',
                    },
                    children: [
                        {
                            name: 'Settings.Figure',
                            path: '/',
                            component: lazyLoading('Settings/Figure'),
                            meta: {
                                title: '身材管理'
                            }
                        },
                        {
                            name: 'Settings.FigureEdit',
                            path: 'edit/:id?',
                            component: lazyLoading('Settings/FigureEdit'),
                            meta: {
                                title: '编辑身材'
                            }
                        },
                    ]
                },
                {
                    path: 'password',
                    component: lazyLoading('Settings', true),
                    redirect: {
                        name: 'Settings.Password',
                    },
                    children: [
                        {
                            name: 'Settings.Password',
                            path: '/',
                            component: lazyLoading('Settings/Password/Home'),
                            meta: {
                                title: '密码设置'
                            },
                        },
                        {
                            path: 'withdraw',
                            component: lazyLoading('Settings', true),
                            redirect: {
                                name: 'Settings.Withdraw',
                            },
                            children: [
                                {
                                    name: 'Settings.Withdraw',
                                    path: '/',
                                    component: lazyLoading('Settings/Password/Withdraw'),
                                    meta: {
                                        title: '修改提现密码'
                                    }
                                },
                                {
                                    name: 'Settings.Forget',
                                    path: 'forget',
                                    component: lazyLoading('Settings/Password/Forget'),
                                    meta: {
                                        title: '手机验证'
                                    }
                                },
                                {
                                    name: 'Settings.OldWithdraw',
                                    path: 'oldWithdraw',
                                    component: lazyLoading('Settings/Password/OldWithdraw'),
                                    meta: {
                                        title: '输入原提现密码'
                                    }
                                },
                                {
                                    name: 'Settings.ResetWithdraw',
                                    path: 'resetWithdraw',
                                    component: lazyLoading('Settings/Password/ResetWithdraw'),
                                    meta: {
                                        title: '重置提现密码'
                                    }
                                },
                                {
                                    name: 'Settings.SetWithdraw',
                                    path: 'set-withdraw',
                                    component: lazyLoading('Settings/Password/SetWithdraw'),
                                    meta: {
                                        title: '设置提现密码'
                                    }
                                }

                            ]
                        },
                    ]
                },
                {
                    path: 'binding',
                    component: lazyLoading('Settings', true),
                    redirect: {
                        name: 'Settings.Binding',
                    },
                    children: [
                        {
                            name: 'Settings.Binding',
                            path: '/',
                            component: lazyLoading('Settings/Binding'),
                            meta: {
                                title: '绑定账号'
                            }
                        },
                    ]
                },
                {
                    name: 'Settings.About',
                    path: 'about',
                    component: lazyLoading('Settings/About'),
                    meta: {
                        title: '关于我们'
                    }
                },
                {
                    name: 'Settings.Feedback',
                    path: 'feedback',
                    component: lazyLoading('Settings/Feedback'),
                    meta: {
                        title: '意见反馈'
                    }
                },
            ]
        },
        {
            path: '/auth',
            component: lazyLoading('Auth', true),
            redirect: {
                name: 'Auth.Login',
            },
            children: [
                {
                    name: 'Auth.Login',
                    path: '/',
                    component: lazyLoading('Auth/Login'),
                    meta: {
                        title: '登录账号'
                    }
                },
                {
                    name: 'Auth.Register',
                    path: 'register',
                    component: lazyLoading('Auth/Register'),
                    meta: {
                        title: '创建账号'
                    }
                },
                {
                    name: 'Auth.Forget',
                    path: 'forget',
                    component: lazyLoading('Auth/Forget'),
                    meta: {
                        title: '忘记密码'
                    }
                },
                {
                    name: 'Auth.Reset',
                    path: 'reset',
                    component: lazyLoading('Auth/Reset'),
                    meta: {
                        title: '重置密码'
                    }
                },
                {
                    name: 'Auth.DynamicLogin',
                    path: 'dynamicLogin',
                    component: lazyLoading('Auth/DynamicLogin'),
                    meta: {
                        title: '动态密码登录'
                    }
                },
            ]
        },

        {
            path: '*',
            redirect: '/mall'
        }
    ]
}
