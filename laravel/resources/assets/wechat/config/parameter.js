export const sort_order = [
    {
        name: '综合排序',
        value: '',
        is_drop_down: true
    },
    {
        name: '新品',
        value: 'new_desc',
        is_drop_down: true
    },
    {
        name: '价格由高到低',
        value: 'price_desc',
        is_drop_down: false
    },
    {
        name: '价格由低到高',
        value: 'price_asc',
        is_drop_down: false
    },
    {
        name: '销量优先',
        value: 'sell_desc',
        is_drop_down: false
    },
];

export const order_status = {
    paying: '待付款',
    close: '交易关闭',
    sending: '待发货',
    receiving: '待签收',
    appraising: '待评价',
    success: '交易成功',
    refunding: '售后中',
    closerefundedpay: '仅退款成功交易完成',
    refundpaysuccess: '仅退款成功',
    failrefunded: '退款拒绝交易成功',
    refundfail: '退款拒绝',
    invalid: '已失效',
};

export const app_rules = {
    login() {
        location.href = 'amii://user/signin'
    },
    complete() {
        location.href = 'amii://game/complete'
    },
    share(provider, id, game_id, method){
        let url = 'amii://share?provider='+ provider
        if (id) {
            url += '&id='+ id
        }
        if (game_id) {
            url += '&game_id='+ game_id
        }
        if (method) {
            url += '&method='+ method
        }
        console.log(url);
        window.location.href = url;
    },
};
