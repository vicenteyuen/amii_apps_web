require('promise.prototype.finally').shim();
require('array.prototype.find').shim();
require('array.prototype.findindex').shim();

/* ============
 * fastclick
 * ============
 *
 * Polyfill to remove click delays on browsers with touch UIs
 *
 * https://github.com/ftlabs/fastclick
 */
// import FastClick from 'fastclick'
// FastClick.attach(document.body);

if ('addEventListener' in document && window.FastClick) {
    document.addEventListener('DOMContentLoaded', (e)=>{
        FastClick.attach(document.body);
    }, false);
}
/* ============
 * Vue
 * ============
 *
 * Vue.js is a library for building interactive web interfaces.
 * It provides data-reactive components with a simple and flexible API.
 *
 * http://vuejs.org/guide/
 */
import Vue from 'vue'

Vue.filter('placeholder', (value, type) => {
    if (!value) {
        switch (type) {
            case 'avatar':
                return require('/assets/images/avatar-empty.png');
            default:
                return require('/assets/images/placeholder.png');
        }
    }
    return value;
});

let letter_number = [];
for (let i = 48; i <= 57; i++) {
    letter_number.push(i);
}
for (let i = 96; i <= 105; i++) {
    letter_number.push(i);
}
for (let i = 65; i <= 90; i++) {
    letter_number.push(i);
}
Vue.config.keyCodes = {
    letter_number: letter_number
};
if (process.env.NODE_ENV !== 'production') {
    Vue.config.silent = true;
    Vue.config.devtools = true;
}

/* ============
 * Vuex Store
 * ============
 *
 * The store of the application
 *
 * http://vuex.vuejs.org/en/index.html
 */
import Vuex from 'vuex'

Vue.use(Vuex);
const store = new Vuex.Store(require('../store').default);

/* ============
 * Vue Router
 * ============
 *
 * The official Router for Vue.js. It deeply integrates with Vue.js core
 * to make building Single Page Applications with Vue.js a breeze.
 *
 * http://router.vuejs.org/en/index.html
 */
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const router = new VueRouter(require('./router').default);

router.beforeEach(async (to, from, next) => {
    if (to.name.indexOf('Auth.Login') < 0) {
        store.commit('addPrevLink', to.fullPath)
    }
    if(from.path === '/' && !from.name){
        Mint.Indicator.open();
        let config = {headers:{}};
        if (store.state.token) {
            config.headers.token = store.state.token;
        } else if (sessionStorage.getItem('token')) {
            store.commit('updateToken', sessionStorage.getItem('token'));
            config.headers.token = sessionStorage.getItem('token');
        } else if (localStorage.getItem('token')) {
            store.commit('updateToken', localStorage.getItem('token'));
            config.headers.token = localStorage.getItem('token');
        }
        let r = await axios.get(RequestAPI.base_url+'user', config)
        if (!store.state.token && !r.data.code) store.commit('updateToken', true);
        Mint.Indicator.close();
    }
    next();
    // if (to.matched.some(m => m.meta.auth) && !store.state.token) {
    //     Mint.MessageBox.confirm('请登录后操作', '')
    //         .then(action => {
    //             next({
    //                 name: 'Auth.Login',
    //             });
    //         }, action => {
    //             if (!from.name) {
    //                 next({
    //                     path: '/',
    //                 });
    //             }
    //         });
    // } else {
    //     next()
    // }
});
import {wxConfig} from '/api/WechatShare'

router.afterEach((to, from) => {
    if (to.name != from.name) {
        window.scrollTo(0, 0);
    }
    if (to.meta.title) {
        document.title = to.meta.title + ' - AMII';
    } else if (document.title != 'AMII') {
        document.title = 'AMII';
    }

    wxConfig();

    if (to.name.indexOf('Game.Home') >= 0) {
        if (store.state.is_app) {
            window.location.href = "amii://appsetting?allow_share=1&page_share_provider=puzzle_index";
        }
    } else if (to.name.indexOf('My.Money') >= 0){
        if (store.state.is_app) {
            window.location.href = "amii://appsetting?allow_share=1&page_share_provider=red_packet";
        }
    } else if (to.name.indexOf('Mall.SalesHome') >= 0){
        if (store.state.is_app) {
            window.location.href = "amii://appsetting?allow_share=1&page_share_provider=tribe";
        }
    } else if (to.name.indexOf('Mall.Advertisement') >= 0){
        if (store.state.is_app) {
            window.location.href = "amii://appsetting?allow_share=1&page_share_provider=activity";
        }
    } else {
        if (store.state.is_app) {
            window.location.href = "amii://appsetting?allow_share=0";
        }
    }
});

/* ============
 * axios
 * ============
 *
 * Promise based HTTP client for the browser and node.js
 *
 * https://github.com/mzabriskie/axios
 */
window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
};

/* ============
 * mint-ui
 * ============
 *
 * Mobile UI elements for Vue.js
 *
 * http://mint-ui.github.io/#!/zh-cn
 */
import {Toast, Indicator, MessageBox, Lazyload} from 'mint-ui'
window.Mint = {Toast, Indicator, MessageBox};

let img = require('/assets/images/placeholder_03.png');

Vue.use(Lazyload, {
    preLoad: 1.3,
    error: img,
    loading: img,
    //try: 3 // default 1
});

/* ============
 * 注册全局api
 * ============
 */
import API from '../api'
window.RequestAPI = new API();

import ScrollToFixed from '/directives/ScrollToFixed'
Vue.directive('scroll-to-fixed', ScrollToFixed);

window.shareComplete = (id) => {
    window.Vue.$router.push({name: 'Game.Show', params:{id: id}})
}

export default {
    router,
    store
}
