# Directory Structure

- api 接口调用
- assets 静态资源
    - images 图片
    - scss 样式表
- components 组件，不包含其他组件
- directives 指令
- modules 模块，复用组件
- pages 路由展示的页面
- app.js 入口
- app.vue 入口
