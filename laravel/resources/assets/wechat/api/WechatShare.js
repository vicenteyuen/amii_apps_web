export const shareAction = {
    init(title, desc, type, dataUrl, shareUrl,imgUrl, successFn, cancelFn) {
        let url = location.href;
        if (shareUrl) {
            url = shareUrl;
        }
        if (Vue.$store.state.user_info && Vue.$store.state.user_info.uuid && url.indexOf('uuid=') < 0) {
            if (url.indexOf('?') >= 0) {
                url += '&'
            } else {
                url += '?'
            }
            url += 'uuid=' + Vue.$store.state.user_info.uuid
        }
        if (!imgUrl && imgUrl == '') {
            imgUrl = location.protocol + '//' + location.host + '/assets/images/AMII-xl.png';
        }
        let data = {
            title: title, //分享标题
            desc: desc, //分享描述
            type: type, //分享类型,music、video或link，不填默认为link
            dataUrl: dataUrl, //如果type是music或video，则要提供数据链接，默认为空
            link: url, //分享链接
            imgUrl: imgUrl,
            // imgUrl: location.protocol + '//' + location.host + '/assets/images/AMII-xl.png', //分享图标
            success: function () {
                if (successFn) successFn();
            },
            cancel: function () {
                if (cancelFn) cancelFn();
            }
        };

        if (typeof wx === 'undefined') return false;

        wx.ready(() => {
            wx.onMenuShareTimeline(data);
            wx.onMenuShareAppMessage(data);
            wx.onMenuShareQQ(data);
            wx.onMenuShareWeibo(data);
            wx.onMenuShareQZone(data);
        });
    }
};

export const wxConfig = (url = null) => {
    if (typeof wx === 'undefined') return null;
    url = url || location.protocol + '//' + location.host + '/wechat' + Vue.$route.fullPath;

    RequestAPI.http.get('oath2/wemp/config', {
        params:{
            url: url
        }
    }).then((r)=>{
        wx.config(r);
    })
};
