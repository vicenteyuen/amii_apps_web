import ErrorHandle from './ErrorHandle'

export default class {
    constructor(prefix = '') {
        if(window.location.host.indexOf('amii') >= 0){
            this.base_url = '/api/';
        }else{
            // this.base_url = 'https://mobileapps.amii.com/api/';
            this.base_url = 'https://amii.dev.ganguo.hk/api/';
            // this.base_url = '/api/';
        }
        this.base_url+=prefix;
        this.http = axios.create({
            baseURL: this.base_url,
            withCredentials: true,
        });
        this.http.interceptors.request.use(
            (config) => {
                if (Vue.$store.state.token) {
                    config.headers.token = Vue.$store.state.token;
                } else if (sessionStorage.getItem('token')) {
                    Vue.$store.commit('updateToken', sessionStorage.getItem('token'));
                    config.headers.token = sessionStorage.getItem('token');
                } else if (localStorage.getItem('token')) {
                    Vue.$store.commit('updateToken', localStorage.getItem('token'));
                    config.headers.token = localStorage.getItem('token');
                }
                return config;
            });
        this.http.interceptors.response.use(
            (response) => {
                if (response.data.status == 'success') {
                    return response.data.data;
                }
                if (response.data.code) {
                    new ErrorHandle(response.data);
                    return Promise.reject(response.data);
                }
                if (response.data.message) {
                    Mint.Toast(response.data.message);
                    return Promise.reject(response.data);
                }
            },
            (error) => {
                if (error.response && error.response.status > 200) {
                    let msg = error.response.statusText;
                    switch (error.response.status){
                        case 429:
                            msg = '您的操作过于频繁！';
                            break;
                    }
                    msg && Mint.Toast(msg);
                    return Promise.reject(error);
                }
                Mint.Toast(error.message);
                return Promise.reject(error);
            });
    }
}
