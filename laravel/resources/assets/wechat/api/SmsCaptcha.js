import Index from './index'
import HmacSha512 from 'crypto-js/hmac-sha512'

export default class extends Index {
    send(url, data = {a:1}) {
        Mint.Indicator.open();
        return this.http.get('helper/xpublic')
            .then((response) => {
                let config = {
                    headers: {
                        'X-Hash': HmacSha512(JSON.stringify(data), response.public).toString(),
                        'X-Public': response.public
                    }
                };
                return this.http.post(url, data, config)
            })
            .finally(() => Mint.Indicator.close());
    }

    validator(el, type) {
        let phoneExp = new RegExp(/^1(3[0-9]|4[01234689]|5[0135789]|8[0-9]|7[2-9])\d{8}$/);
        let emailExp = new RegExp(/^[\w\+\-]+(\.[\w\+\-]+)*@[a-z\d\-]+(\.[a-z\d\-]+)*\.([a-z]{2,4})$/i);
        let usernameExp = new RegExp(/^\w{3,12}$/);
        let passwordExp = new RegExp(/^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]+$/);
        let numberExp = new RegExp(/\d{6}/);
        switch (type) {
            case 'phone':
                return phoneExp.test(el);
            case 'email':
                return emailExp.test(el);
            case 'username':
                return usernameExp.test(el);
            case 'password':
                return passwordExp.test(el);
            case 'number':
                return numberExp.test(el);
        }
    }
}
