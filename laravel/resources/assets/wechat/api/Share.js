export const share = {
    init(privoder, id) {
        Mint.Indicator.open();
        return RequestAPI.http.get('share/'+ privoder +'?id='+ id)
        .finally(() => Mint.Indicator.close());
    }
};
