import {app_rules} from '/config/parameter'
export default class {
    constructor(data) {
        this.error = data;
        for (let error of this.errors()) {
            if (error.code != data.code) {
                continue
            }
            if (error.message) {
                Mint.Toast(error.message);
                continue
            }
            if (typeof error.method == 'function') {
                error.method();
            }
        }
    }

    errors() {
        return [
            {
                code: 10000,
                method: () => {
                    for (let k in this.error.data) {
                        let error = this.error.data[k];
                        Mint.Toast(error[0]);
                        break;
                    }
                }
            },
            {
                code: 10001,
                message: '该用户已注册'
            },
            {
                code: 30001,
                message: '此商品不存在'
            },
            {
                code: 10003,
                method: () => {
                    // Mint.MessageBox.confirm('请登录后操作', '')
                    //     .then(action => {
                    //         Vue.$router.push({
                    //             name: 'Auth.Login',
                    //         });
                    //     }, action => {
                    //     });
                    if (Vue.$store.state.is_app) {
                        app_rules.login();
                    } else {
                        Vue.$router.push({
                            name: 'Auth.Login',
                        });
                    }
                    if (Vue.$store.state.token) {
                        Vue.$store.commit('updateToken', null);
                    }
                }
            },
            {
                code: 10066,
                method: () => {
                    if (Vue.$store.state.is_app) {
                        location.href = 'amii://user/bindphone';
                        return false;
                    } else {
                        Mint.MessageBox.confirm('当前账号未绑定手机，前往绑定？', '')
                        .then(action => {
                            Vue.$router.push({
                                name: 'Settings.BindPhone',
                            });
                        }, action => {
                        });
                    }
                }
            },
            {
                code: 500001,
                message: '用户未注册'
            },
        ]
    }
}
