import Index from './index'

export default class extends Index {
    constructor() {
        super();
    }

    getProfile() {
        return this.http.get('user');
    }
}
